﻿using System;
using MMInterfaces;

namespace MMFactory
{
    public sealed class MMFactory
    {
        private static readonly MMFactory instance = new MMFactory();
        IAnnotationReader annotationReader = null;
        ICasePropertyEvaluator casePropertyEvaluator = null;
        IDocumentPropertyEvaluator docPropertyEvaluator = null;
        IAnnotationPropertyEvaluator annPropertyEvaluator = null;
        ITaskPropertyEvaluator taskPropertyEvaluator = null;
        ICaseCommentReader caseCommentReader = null;
        IDocumentCommentReader docCommentReader = null;
        ITaskCommentReader taskCommentReader = null;
        ICaseDocGenerator caseDocGenerator = null;
        IDocumentDocGenerator docDocGenerator = null;
        ITaskDocGenerator taskDocGenerator = null;
        ICaseTypeSelector caseTypeSelector = null;
        IDocumentTypeSelector docTypeSelector = null;
        IAnnotationTypeSelector annTypeSelector = null;
        ITaskTypeSelector taskTypeSelector = null;

        // constructor
        private MMFactory()
        {
        }
        
        public static MMFactory Instance
        {
            get 
            {
                return instance; 
            }
        }

        private ICustomizationPoint SetCustomizationPoint(string appSetting, string path)
        {
            ICustomizationPoint icp = null;

            if (appSetting != null)
            {
                string[] settings = appSetting.Split(',');
                if (settings.Length >= 2)
                {
                    string type = settings[0].Trim();
                    string assembly = settings[1].Trim();
                    string assemblyPath = path + "\\" + assembly;

                    icp = System.Reflection.Assembly.LoadFrom(assemblyPath).CreateInstance(type) as ICustomizationPoint;
                    if (icp == null)
                    {
                        throw new ArgumentException("Could not set customization point from " + appSetting);
                    }
                    if (settings.Length > 2)
                    {
                        string[] args = new string[settings.Length - 2];
                        for (int i = 0; i < settings.Length - 2; i++)
                        {
                            args[i] = settings[i + 2];
                        }
                        icp.SetContext(args);
                    }
                }
            }
            return icp;
        }

        public void SetAnnotationReader(string appSetting, string path)
        {
            if (annotationReader == null)
            {
                annotationReader = (IAnnotationReader)SetCustomizationPoint(appSetting, path);
            }
        }

        public void SetCasePropertyEvaluator(string appSetting, string path)
        {
            if (casePropertyEvaluator == null)
            {
                casePropertyEvaluator = (ICasePropertyEvaluator)SetCustomizationPoint(appSetting, path);
            }
        }

        public void SetDocumentPropertyEvaluator(string appSetting, string path)
        {
            if (docPropertyEvaluator == null)
            {
                docPropertyEvaluator = (IDocumentPropertyEvaluator)SetCustomizationPoint(appSetting, path);
            }
        }

        public void SetAnnotationPropertyEvaluator(string appSetting, string path)
        {
            if (annPropertyEvaluator == null)
            {
                annPropertyEvaluator = (IAnnotationPropertyEvaluator)SetCustomizationPoint(appSetting, path);
            }
        }

        public void SetTaskPropertyEvaluator(string appSetting, string path)
        {
            if (taskPropertyEvaluator == null)
            {
                taskPropertyEvaluator = (ITaskPropertyEvaluator)SetCustomizationPoint(appSetting, path);
            }
        }

        public void SetCaseCommentReader(string appSetting, string path)
        {
            if (caseCommentReader == null)
            {
                caseCommentReader = (ICaseCommentReader)SetCustomizationPoint(appSetting, path);
            }
        }

        public void SetDocumentCommentReader(string appSetting, string path)
        {
            if (docCommentReader == null)
            {
                docCommentReader = (IDocumentCommentReader)SetCustomizationPoint(appSetting, path);
            }
        }

        public void SetTaskCommentReader(string appSetting, string path)
        {
            if (taskCommentReader == null)
            {
                taskCommentReader = (ITaskCommentReader)SetCustomizationPoint(appSetting, path);
            }
        }

        public void SetCaseNotesDocGenerator(string appSetting, string path)
        {
            if (caseDocGenerator == null)
            {
                caseDocGenerator = (ICaseDocGenerator)SetCustomizationPoint(appSetting, path);
            }
        }

        public void SetDocumentNotesDocGenerator(string appSetting, string path)
        {
            if (docDocGenerator == null)
            {
                docDocGenerator = (IDocumentDocGenerator)SetCustomizationPoint(appSetting, path);
            }
        }

        public void SetTaskNotesDocGenerator(string appSetting, string path)
        {
            if (taskDocGenerator == null)
            {
                taskDocGenerator = (ITaskDocGenerator)SetCustomizationPoint(appSetting, path);
            }
        }

        public void SetCaseTypeSelector(string appSetting, string path)
        {
            if(caseTypeSelector == null)
            {
                caseTypeSelector = (ICaseTypeSelector)SetCustomizationPoint(appSetting, path);
            }
        }

        public void SetDocumentTypeSelector(string appSetting, string path)
        {
            if (docTypeSelector == null)
            {
                docTypeSelector = (IDocumentTypeSelector)SetCustomizationPoint(appSetting, path);
            }
        }

        public void SetAnnotationTypeSelector(string appSetting, string path)
        {
            if (annTypeSelector == null)
            {
                annTypeSelector = (IAnnotationTypeSelector)SetCustomizationPoint(appSetting, path);
            }
        }

        public void SetTaskTypeSelector(string appSetting, string path)
        {
            if (taskTypeSelector == null)
            {
                taskTypeSelector = (ITaskTypeSelector)SetCustomizationPoint(appSetting, path);
            }
        }

        public IAnnotationReader GetAnnotationReader()
        {
            return annotationReader;
        }

        public ICasePropertyEvaluator GetCasePropertyEvaluator()
        {
            return casePropertyEvaluator;
        }

        public IDocumentPropertyEvaluator GetDocumentPropertyEvaluator()
        {
            return docPropertyEvaluator;
        }

        public IAnnotationPropertyEvaluator GetAnnotationPropertyEvaluator()
        {
            return annPropertyEvaluator;
        }

        public ITaskPropertyEvaluator GetTaskPropertyEvaluator()
        {
            return taskPropertyEvaluator;
        }

        public ICaseCommentReader GetCaseCommentReader()
        {
            return caseCommentReader;
        }

        public IDocumentCommentReader GetDocumentCommentReader()
        {
            return docCommentReader;
        }

        public ITaskCommentReader GetTaskCommentReader()
        {
            return taskCommentReader;
        }

        public ICaseDocGenerator GetCaseDocGenerator()
        {
            return caseDocGenerator;
        }

        public IDocumentDocGenerator GetDocumentDocGenerator()
        {
            return docDocGenerator;
        }

        public ITaskDocGenerator GetTaskDocGenerator()
        {
            return taskDocGenerator;
        }

        public ICaseTypeSelector GetCaseTypeSelector()
        {
            return caseTypeSelector;
        }

        public IDocumentTypeSelector GetDocumentTypeSelector()
        {
            return docTypeSelector;
        }

        public IAnnotationTypeSelector GetAnnotationTypeSelector()
        {
            return annTypeSelector;
        }

        public ITaskTypeSelector GetTaskTypeSelector()
        {
            return taskTypeSelector;
        }
    }
}
