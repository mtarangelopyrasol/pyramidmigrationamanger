﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMInterfaces
{
    public enum P8AnnotationType
    {
        Arrow,
        Highlight,
        HighlightPolygon,
        Line,
        None,
        OpenPolygon,
        Oval,
        Pen,
        Polygon,
        Rectangle,
        SolidText,
        Stamp,
        StickyNote,
        TransparentText   
    }

    public interface ICustomizationPoint
    {
        void SetContext(string[] args);
    }

    public interface IAnnotationReader : ICustomizationPoint
    {
        Boolean SetAnnotationFile(string fileName, PageInformation docInfo);
        long GetAnnotationCount();
        P8AnnotationType GetTargetAnnotationType(int i);
        string GetLegacyName(int i);
        string GetClassName(int i);
        double GetHeight(int i);
        double GetWidth(int i);
        double GetTop(int i);
        double GetLeft(int i);
        string GetCreator(int i);
        string GetEntryDate(int i);
        string GetModifyDate(int i);
        string GetPageNumber(int i);

        string GetArrowHeadSize(int i);
        string GetLineBackMode(int i);
        string GetLineColor(int i);
        string GetLineStartX(int i);
        string GetLineEndX(int i);
        string GetLineStartY(int i);
        string GetLineEndY(int i);
        string GetLineStyle(int i);
        string GetLineWidth(int i);
        string GetMultiPageTiffPageNumber(int i);
        string GetTextBackMode(int i);
        string GetBrushColor(int i);
        string GetSubClass(int i);
        string GetForeColor(int i);
        string GetBackColor(int i);

        string GetBorderBackMode(int i);
        string GetBorderColor(int i);
        string GetBorderStyle(int i);
        string GetBorderWidth(int i);
        string GetFontBold(int i);
        string GetFontItalic(int i);
        string GetFontName(int i);
        string GetFontSize(int i);
        string GetFontStrikethrough(int i);
        string GetFontUnderline(int i);
        string GetHasBorder(int i);
        string GetRotation(int i);

        byte[] GetText(int i);
        string GetPoints(int i);
        List<string> GetHistory(int i);
    }

    public interface IPropertyEvaluator : ICustomizationPoint
    {
        List<string> Evaluate(string targetName, string targetType, SourceProperties srcProperties);
    }

    public interface ICasePropertyEvaluator : IPropertyEvaluator
    {
    }

    public interface IDocumentPropertyEvaluator : IPropertyEvaluator
    {
        List<string> Evaluate(string targetName, string targetType, DocumentPages docPages);
    }

    public interface IAnnotationPropertyEvaluator : IPropertyEvaluator
    {
    }

    public interface ITaskPropertyEvaluator : IPropertyEvaluator
    {
    }

    public interface ICommentReader : ICustomizationPoint
    {
        Boolean SetCommentFile(string fileName, SourceProperties srcProperties);
        long GetCommentCount();
        string GetTargetCommentType(int i);
        string GetClassName(int i);
        string GetActionCode(int i);
        string GetText(int i);
    }

    public interface ICaseCommentReader : ICommentReader
    {
    }

    public interface IDocumentCommentReader : ICommentReader
    {
        Boolean SetCommentFile(string fileName, DocumentPages docPages);
    }

    public interface ITaskCommentReader : ICommentReader
    {
    }

    public interface IDocGenerator : ICustomizationPoint
    {
        GeneratedDoc Generate(string srcFilePath, string stagingPath, SourceProperties srcProperties);
    }

    public interface ICaseDocGenerator : IDocGenerator
    {
    }

    public interface IDocumentDocGenerator : IDocGenerator
    {
        GeneratedDoc Generate(string srcFilePath, string stagingPath, DocumentPages docPages);
    }

    public interface ITaskDocGenerator : IDocGenerator
    {
    }

    public interface ITypeSelector : ICustomizationPoint
    {
        string Select(SourceProperties srcProperties);
    }

    public interface ICaseTypeSelector : ITypeSelector
    {
    }

    public interface IDocumentTypeSelector : ITypeSelector
    {
        string Select(DocumentPages docPages);
    }

    public interface IAnnotationTypeSelector : ITypeSelector
    {
    }

    public interface ITaskTypeSelector : ITypeSelector
    {
    }
}
