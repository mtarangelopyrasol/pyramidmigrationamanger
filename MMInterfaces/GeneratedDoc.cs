﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMInterfaces
{
    public class GeneratedDoc
    {
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public string ContainmentName { get; set; }
        public int PageNumber { get; set; }
    }
}
