﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMInterfaces
{
    public class PageInformation
    {
        public int RotationAngle { get; set; }
        public int PreAnnotationRotation { get; set; }
        public double PageWidth { get; set; }
        public double PageHeight { get; set; }
        public int XResolution { get; set; }
        public int YResolution { get; set; }

        public PageInformation()
        {
            RotationAngle = 0;
            PreAnnotationRotation = 0;
            PageWidth = 0;
            PageHeight = 0;
            XResolution = 0;
            YResolution = 0;
        }
    }
}
