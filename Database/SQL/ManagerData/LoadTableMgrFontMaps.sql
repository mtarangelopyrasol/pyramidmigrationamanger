
USE [MIGRATIONDB]

INSERT INTO [dbo].[MgrFontMaps] (FontName, InputSize, OutputSize, HeightFactor, WidthFactor) VALUES
	('Arial', '12', '8', 1.0, 1.0),
	('Arial', 'ANY', 'ANY', 1.0, 1.18),
	('Arial Rounded MT', '12', '8', 1.0, 1.0),
	('Bradley Hand ITC', '16', '4', 1.0, 3.0),
	('Candara', '13', '8', 1.0, 1.0),
	('Century Gothic', '12', '4', 1.0, 3.0),
	('Comic Sans MS', '11', '4', 0.2, 0.5),
	('Comic Sans MS', '12', '4', 0.2, 0.3),
	('Microsoft Sans Serif', '10', '8', 1.0, 1.0),
	('Microsoft Sans Serif', '12', '8', 1.0, 1.0),
	('Microsoft Sans Serif', '13', '8', 1.0, 1.0),
	('Microsoft Sans Serif', '14', '6', 1.0, 6.0),
	('Microsoft Sans Serif', '16', '8', 1.0, 1.0),
	('Nimrod', '12', '8', 1.0, 1.0),
	('Nimrod', '16', '8', 1.0, 1.0),
	('Tahoma', '9', '8', 1.0, 1.0),
	('Tempus Sans ITC', '12', '8', 1.0, 1.0),
	('Times New Roman', '12', '10', 1.0, 1.0)
GO

				
				

				
