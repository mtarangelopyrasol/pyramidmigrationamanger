
USE [MIGRATIONDB]

INSERT INTO [dbo].[MgrLeadFormat] (Format, Compression, LeadFormat) VALUES
	('BMP', 'NONE', 'Bmp'),
	('GIF', 'NONE', 'Gif'),
	('JPG', 'NONE', 'Jpeg'),
	('JPEG', 'NONE', 'Jpeg'),
	('PDF', 'NONE', 'RasPdfLzw'),
	('TIF', 'NONE', 'CcittGroup4'),
	('TIFF', 'NONE', 'CcittGroup4'),
	('TXT', 'NONE', 'Txt'),
	('ANY', 'TIF', 'Tif'),
	('ANY', 'TIFABIC', 'TifAbic'),
	('ANY', 'TIFCMYK', 'TifCmyk'),
	('ANY', 'TIFYCC', 'TifYcc'),
	('ANY', 'TIFPACKBITS', 'TifPackBits'),
	('ANY', 'TIFPACKBITSCMYK', 'TifPackBitsCmyk'),
	('ANY', 'TIFPACKBITSYCC', 'TifPackbitsYcc'),
	('ANY', 'TIFLZW', 'TifLzw'),
	('ANY', 'TIFLZWCMYK', 'TifLzwCmyk'),
	('ANY', 'TIFLZWYCC', 'TifLzwYcc'),
	('ANY', 'TIFJPEG', 'TifJpeg'),
	('ANY', 'TIFJPEG422', 'TifJpeg422'),
	('ANY', 'TIFJPEG411', 'TifJpeg411'),
	('ANY', 'TIFCMP', 'TifCmp'),
	('ANY', 'TIFJBIG', 'TifJbig'),
	('ANY', 'TIFJBIG2', 'TifJbig2'),
	('ANY', 'TIFJ2K', 'TifJ2k'),
	('ANY', 'TIFCMW', 'TifCmw'),
	('ANY', 'GEOTIFF', 'GeoTiff'),
	('ANY', 'EXIF', 'Exif'),
	('ANY', 'EXIFYCC', 'ExifYcc'),
	('ANY', 'EXIFJPEG422', 'ExifJpeg422'),
	('ANY', 'EXIFJPEG411', 'ExifJpeg411'),
	('ANY', 'CCITT', 'Ccitt'),
	('ANY', 'CCITTGROUP31DIM', 'CcittGroup31Dim'),
	('ANY', 'CCITTGROUP32DIM', 'CcittGroup32Dim'),
	('ANY', 'CCITTGROUP4', 'CcittGroup4'),
	('ANY', 'TIFABC', 'TifAbc'),
	('ANY', 'TIFMRC', 'TifMrc'),
	('ANY', 'TIFLEADMRC', 'TifLeadMrc'),
	('ANY', 'RASPDF', 'RasPdf'),
	('ANY', 'RASPDFCMYK', 'RasPdfCmyk'),
	('ANY', 'RASPDFG31DIM', 'RasPdfG31Dim'),
	('ANY', 'RASPDFG32DIM', 'RasPdfG32Dim'),
	('ANY', 'RASPDFG4', 'RasPdfG4'),
	('ANY', 'RASPDFLZW', 'RasPdfLzw'),
	('ANY', 'RASPDFLZWCMYK', 'RasPdfLzwCmyk'),
	('ANY', 'RASPDFJPEG', 'RasPdfJpeg'),
	('ANY', 'RASPDFJPEG422', 'RasPdfJpeg422'),
	('ANY', 'RASPDFJPEG411', 'RasPdfJpeg411'),
	('ANY', 'RASPDFJBIG2', 'RasPdfJbig2'),
	('ANY', 'CMP', 'Cmp'),
	('ANY', 'JPEG', 'Jpeg'),
	('ANY', 'JPEG411', 'Jpeg411'),
	('ANY', 'JPEG422', 'Jpeg422'),
	('ANY', 'JPEGLAB', 'JpegLab'),
	('ANY', 'JPEGLAB411', 'JpegLab411'),
	('ANY', 'JPEGLAB422', 'JpegLab422'),
	('ANY', 'JPEGRGB', 'JpegRgb'),
	('ANY', 'J2K', 'J2k'),
	('ANY', 'JP2', 'Jp2'),				
	('ANY', 'CMW', 'Cmw'),
	('ANY', 'GIF', 'Gif')		
GO
