
USE [MIGRATIONDB]

INSERT INTO [dbo].[PromotionRules] (DocFormat, PageFormat, PageCompression, RuleResult) VALUES
	('CcittGroup4', 'ANY', 'JPEG', 'TifJpeg'),
	('RasPdfLzw', 'Jpeg411', 'JPEG', 'RasPdfJpeg411'),
	('RasPdfLzw', 'JpegLab411', 'JPEG', 'RasPdfJpeg411'),
	('RasPdfLzw', 'TifJpeg411', 'JPEG', 'RasPdfJpeg411'),
	('RasPdfLzw', 'Jpeg422', 'JPEG', 'RasPdfJpeg422'),
	('RasPdfLzw', 'JpegLab422', 'JPEG', 'RasPdfJpeg422'),
	('RasPdfLzw', 'TifJpeg422', 'JPEG', 'RasPdfJpeg422'),
	('RasPdfLzw', 'ANY', 'JPEG', 'RasPdfJpeg')
GO

				
				

				
