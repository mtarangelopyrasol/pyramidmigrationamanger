
USE [MIGRATIONDB]

INSERT INTO [dbo].[MgrConvMaps] (SourceFormat, TargetFormat, Exempt, Restricted) VALUES
	('ASF', 'ASF', 1, 1),
	('AVI','AVI', 1, 1),
	('bin', 'bin', 1, 1),
	('emz', 'emz', 1, 1),
	('hpd', 'hpd', 1, 1),
	('lnk', 'lnk', 1, 1),
	('MDI', 'MDI', 1, 1),
	('MOV', 'MOV', 1, 1),
	('mp3', 'mp3', 1, 1),
	('mp4', 'mp4', 1, 1),
	('mpeg4', 'mpeg4', 1, 1),
	('MSG', 'MSG', 1, 1),
	('odt', 'odt', 1, 1),
	('OFT', 'OFT', 1, 1),
	('pub', 'pub', 1, 1),
	('qbb', 'qbb', 1, 1),
	('rdf', 'rdf', 1, 1),
	('WAV', 'WAV', 1, 1),
	('WMA', 'WMA', 1, 1),
	('WMV', 'WMV', 1, 1),
	('WPS', 'WPS', 1, 1),
	('xml', 'xml', 1, 1),
	('ALL', 'tif', 0, 0)
GO


 