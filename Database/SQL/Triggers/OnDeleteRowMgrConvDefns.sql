/***************************************************************************
* File: OnDeleteRowMgrConvDefns.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnDeleteRowMgrConvDefns Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/21/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnDeleteRowMgrConvDefns]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnDeleteRowMgrConvDefns]
GO

CREATE TRIGGER OnDeleteRowMgrConvDefns ON dbo.MgrConvDefns
FOR DELETE 
AS
BEGIN

DECLARE @EvtDesc varchar(4096)

	SELECT @EvtDesc = 'ID: ' + CAST(d.ID AS varchar(2)) + '; Name: ' + d.ConvDefnName + '; Marshal To Single Doc: ' + CAST(d.MarshalToSingleDoc AS varchar(2))
	FROM dbo.MgrConvDefns cd
	JOIN deleted d
	ON d.ID = cd.ID
	
	EXECUTE LogHistory @Evt = 'CONFIG_DELETE', @Desc = @EvtDesc
	
END
GO
