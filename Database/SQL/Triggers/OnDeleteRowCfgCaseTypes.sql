/***************************************************************************
* File: OnDeleteRowCfgCaseTypes.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnDeleteRowCfgCaseTypes Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/21/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnDeleteRowCfgCaseTypes]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnDeleteRowCfgCaseTypes]
GO

CREATE TRIGGER OnDeleteRowCfgCaseTypes ON dbo.CfgCaseTypes
FOR DELETE 
AS
BEGIN

DECLARE @CaseID int
DECLARE @EvtDesc varchar(4096)

	SELECT @CaseID = d.ID, @EvtDesc = 'ID: ' + CAST(d.ID AS varchar(2)) + '; Name: ' + d.CaseName + '; Default Document Class: ' + d.DefaultDocClass
	FROM deleted d
	
	DELETE CfgCaseProperties
	WHERE CaseTypeID = @CaseID

	DELETE CfgCaseFolders
	WHERE CaseTypeID = @CaseID

	DELETE CfgTaskTypes
	WHERE CaseID = @CaseID
	
	IF(@EvtDesc IS NOT NULL)
	BEGIN
		EXECUTE LogHistory @Evt = 'CASE_DELETE', @Desc = @EvtDesc
	END
END
GO
