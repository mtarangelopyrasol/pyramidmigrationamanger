/***************************************************************************
* File: OnInsertRowCfgCaseProperties.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnInsertRowCfgCaseProperties Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/14/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnInsertRowCfgCaseProperties]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnInsertRowCfgCaseProperties]
GO

CREATE TRIGGER OnInsertRowCfgCaseProperties ON dbo.CfgCaseProperties
FOR INSERT 
AS
BEGIN

DECLARE @ConfigID int
DECLARE @CaseID int
DECLARE @Config varchar(64)
DECLARE @CaseName varchar(64)
DECLARE @Required int;
DECLARE @Mapped int;
DECLARE @Computed int;
DECLARE @MapName varchar(64)
DECLARE @EvtDesc varchar(4096)

	SELECT @ConfigID = i.ConfigID, @CaseID = i.CaseTypeID, @EvtDesc = i.Name + '(Source: ' + i.Source + '; Data Type: ' + i.DataType + ')', 
		@Required = i.Required, @Mapped = i.Mapped, @Computed = i.Computed, @MapName = i.MappingName
	FROM inserted i

	SELECT @Config = ConfigName
	FROM MgrConfigurations
	WHERE ID = @ConfigID

	SELECT @CaseName = CaseName
	FROM CfgCaseTypes
	WHERE ID = @CaseID

	SET @EvtDesc = 'Configuration: ' + @Config + '; Case: ' + @CaseName

	IF(@Required = 1)
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Required'
	END

	IF(@Computed = 1)
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Computed'
	END

	IF(@Mapped = 1)
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Mapped using mapping ' + @MapName
	END
	
	EXECUTE LogHistory @Evt = 'CASEPROP_ADD', @Desc = @EvtDesc
END
GO
