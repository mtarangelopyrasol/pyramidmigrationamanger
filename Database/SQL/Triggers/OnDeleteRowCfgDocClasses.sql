/***************************************************************************
* File: OnDeleteRowCfgDocClasses.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnDeleteRowCfgDocClasses Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/21/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnDeleteRowCfgDocClasses]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnDeleteRowCfgDocClasses]
GO

CREATE TRIGGER OnDeleteRowCfgDocClasses ON dbo.CfgDocClasses
FOR DELETE 
AS
BEGIN

DECLARE @DocClassID int
DECLARE @EvtDesc varchar(4096)

	SELECT @DocClassID = d.ID, @EvtDesc = 'ID: ' + CAST(d.ID AS varchar(2)) + '; Name: ' + d.DocClassName
	FROM deleted d
	
	DELETE CfgDocClassProperties
	WHERE DocClassID = @DocClassID

	IF(@EvtDesc IS NOT NULL)
	BEGIN
		EXECUTE LogHistory @Evt = 'DOC_DELETE', @Desc = @EvtDesc
	END
END
GO
