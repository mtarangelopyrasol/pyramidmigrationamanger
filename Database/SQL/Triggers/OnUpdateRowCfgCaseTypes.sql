/***************************************************************************
* File: OnUpdateRowCfgCaseTypes.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnUpdateRowCfgCaseTypes Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/10/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnUpdateRowCfgCaseTypes]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnUpdateRowCfgCaseTypes]
GO

CREATE TRIGGER OnUpdateRowCfgCaseTypes ON dbo.CfgCaseTypes
FOR UPDATE 
AS
BEGIN

DECLARE @CfgID int
DECLARE @Config varchar(64)
DECLARE @OldName varchar(64)
DECLARE @NewName varchar(64)
DECLARE @OldClass varchar(64)
DECLARE @NewClass varchar(64)
DECLARE @EvtDesc varchar(4096)

	SELECT  @CfgID = d.ConfigID,
		@OldName = d.CaseName, @NewName = i.CaseName, 
		@OldClass = d.DefaultDocClass, @NewClass = i.DefaultDocClass
	FROM inserted i
	FULL OUTER JOIN deleted d
	ON i.ID = d.ID

	SELECT @Config = ConfigName
	FROM MgrConfigurations
	WHERE ID = @CfgID

	IF((@OldName != @NewName) OR (@OldClass != @NewClass))
	BEGIN
		SET @EvtDesc = 'Configuration: ' + @Config

		IF (@OldName != @NewName)
		BEGIN
			SET @EvtDesc = @EvtDesc + '; Name: ' + @OldName + ' changed to ' + @NewName + '; '
		END
		ELSE
		BEGIN
			SET @EvtDesc = @EvtDesc + '; Name: ' + @OldName + ' '
		END

		IF (@OldClass != @NewClass)
		BEGIN
			SET @EvtDesc = @EvtDesc + '; Default Document Class: ' + @OldClass + ' changed to ' + @NewClass + '; '
		END

		EXECUTE LogHistory @Evt = 'CASE_UPDATE', @Desc = @EvtDesc
	END
END
GO
