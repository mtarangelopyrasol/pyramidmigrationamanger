/***************************************************************************
* File: OnUpdateRowCfgDocClassProperties.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnUpdateRowCfgDocClassProperties Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/10/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnUpdateRowCfgDocClassProperties]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnUpdateRowCfgDocClassProperties]
GO

CREATE TRIGGER OnUpdateRowCfgDocClassProperties ON dbo.CfgDocClassProperties
FOR UPDATE 
AS
BEGIN

DECLARE @ConfigID int
DECLARE @DocClassID int
DECLARE @Config varchar(64)
DECLARE @DocClassName varchar(64)
DECLARE @OldName varchar(64)
DECLARE @OldSource varchar(64)
DECLARE @OldRequired int;
DECLARE @OldMapped int;
DECLARE @OldComputed int;
DECLARE @OldMapName varchar(64)
DECLARE @NewName varchar(64)
DECLARE @NewSource varchar(64)
DECLARE @NewRequired int;
DECLARE @NewMapped int;
DECLARE @NewComputed int;
DECLARE @NewMapName varchar(64)
DECLARE @EvtDesc varchar(4096)

	SELECT  @ConfigID = d.ConfigID, @DocClassID = d.DocClassID,
		@OldName = d.Name, @NewName = i.Name, 
		@OldSource = d.Source, @NewSource = i.Source,
		@OldRequired = d.Required, @NewRequired = i.Required,
		@OldMapped = d.Mapped, @NewMapped = i.Mapped,
		@OldComputed = d.Computed, @NewComputed = i.Computed,
		@OldMapName = d.MappingName, @NewMapName = i.MappingName
	FROM inserted i
	FULL OUTER JOIN deleted d
	ON i.ID = d.ID

	SELECT @Config = ConfigName
	FROM MgrConfigurations
	WHERE ID = @ConfigID

	SELECT @DocClassName = DocClassName
	FROM CfgDocClasses
	WHERE ID = @DocClassID

	IF((@OldName != @NewName) OR (@OldSource != @NewSource) OR (@OldRequired != @NewRequired) OR (@OldMapped != @NewMapped) OR (@OldComputed != @NewComputed) OR (@OldMapName != @NewMapName))
	BEGIN
		SET @EvtDesc = 'Configuration: ' + @Config + '; DocClass: ' + @DocClassName

		IF (@OldName != @NewName)
		BEGIN
			SET @EvtDesc = @EvtDesc + '; Name: ' + @OldName + ' changed to ' + @NewName + '; '
		END

		IF (@OldSource != @NewSource)
		BEGIN
			SET @EvtDesc = @EvtDesc + '; Source: ' + @OldSource + ' changed to ' + @NewSource + '; '
		END

		IF (@OldRequired != @NewRequired)
		BEGIN
			SET @EvtDesc = @EvtDesc + '; Name: ' + CAST(@OldRequired AS varchar(2)) + ' changed to ' + CAST(@NewRequired AS varchar(2)) + '; '
		END

		IF (@OldMapped != @NewMapped)
		BEGIN
			SET @EvtDesc = @EvtDesc + '; Mapped: ' + CAST(@OldMapped AS varchar(2)) + ' changed to ' + CAST(@NewMapped AS varchar(2)) + '; '
		END

		IF (@OldComputed != @NewComputed)
		BEGIN
			SET @EvtDesc = @EvtDesc + '; Computed: ' + CAST(@OldComputed AS varchar(2)) + ' changed to ' + CAST(@NewComputed AS varchar(2)) + '; '
		END

		IF (@OldMapName != @NewMapName)
		BEGIN
			SET @EvtDesc = @EvtDesc + '; MapName: ' + @OldMapName + ' changed to ' + @NewMapName + '; '
		END
		EXECUTE LogHistory @Evt = 'DocClass_UPDATE', @Desc = @EvtDesc
	END
END
GO
