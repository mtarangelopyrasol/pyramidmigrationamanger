/***************************************************************************
* File: OnInsertRowCfgTaskTypes.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnInsertRowCfgTaskTypes Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/21/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnInsertRowCfgTaskTypes]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnInsertRowCfgTaskTypes]
GO

CREATE TRIGGER OnInsertRowCfgTaskTypes ON dbo.CfgTaskTypes
FOR INSERT 
AS
BEGIN

DECLARE @CaseID int
DECLARE @CaseName varchar(64)
DECLARE @Name varchar(64)
DECLARE @DocInit varchar(1)
DECLARE @InitDocClass varchar(64)
DECLARE @InitDocSource varchar(64)
DECLARE @Attach varchar(64)
DECLARE @EvtDesc varchar(4096)

	SELECT @CaseID = i.CaseID, @Name = i.TaskName, @DocInit = i.IsDocInitiated, @InitDocClass = i.InitiatingDocClass, @InitDocSource = i.InitiatingDocIdSource, @Attach = i.AttachingProperty
	FROM inserted i
	
	SELECT @CaseName = CaseName
	FROM CfgCaseTypes
	WHERE ID = @CaseID

	SET @EvtDesc = 'Task ' + @Name + ' added to Case ' + @CaseName

	IF(@DocInit = 1)
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Document Initiated; Initiating Document Class: ' + @InitDocClass + '; Initiating Doc Id Source: ' + @InitDocSource + '; Attaching Property: ' + @Attach
	END

	EXECUTE LogHistory @Evt = 'TASK_ADD', @Desc = @EvtDesc
END
GO
