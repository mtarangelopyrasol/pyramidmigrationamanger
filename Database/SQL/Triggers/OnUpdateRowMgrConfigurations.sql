/***************************************************************************
* File: OnUpdateRowMgrConfigurations.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnUpdateRowMgrConfigurations Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	11/05/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnUpdateRowMgrConfigurations]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnUpdateRowMgrConfigurations]
GO

CREATE TRIGGER OnUpdateRowMgrConfigurations ON dbo.MgrConfigurations
AFTER UPDATE 
AS
BEGIN

DECLARE @OldName varchar(64)
DECLARE @NewName varchar(64)
DECLARE @OldComment varchar(64)
DECLARE @NewComment varchar(64)
DECLARE @EvtDesc varchar(4096)

	SET NOCOUNT ON;

	UPDATE t SET LastUpdated = CURRENT_TIMESTAMP
	FROM MgrConfigurations t
	WHERE EXISTS (SELECT 1 FROM inserted WHERE ID = t.ID);

	SELECT @EvtDesc = 'ID: ' + CAST(d.ID AS varchar(2)), @OldName = d.ConfigName, @NewName = i.ConfigName, @OldComment = d.Comment, @NewComment = i.Comment
	FROM inserted i
	FULL OUTER JOIN deleted d
	ON i.ID = d.ID
	
	IF(@OldName is null)
	BEGIN
		SET @OldName = 'null'
	END

	IF (@OldName != @NewName)
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Name: ' + @OldName + ' changed to ' + @NewName
	END

	IF(@OldComment is null)
	BEGIN
		SET @OldComment = 'null'
	END

	IF (@OldComment != @NewComment)
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Comment: ' + @OldComment + ' changed to ' + @NewComment
	END

	EXECUTE LogHistory @Evt = 'CONFIG_UPDATE', @Desc = @EvtDesc
	
END
GO
