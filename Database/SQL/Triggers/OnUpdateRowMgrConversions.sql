/***************************************************************************
* File: OnUpdateRowMgrConversions.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnUpdateRowMgrConversions Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/21/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnUpdateRowMgrConversions]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnUpdateRowMgrConversions]
GO

CREATE TRIGGER OnUpdateRowMgrConversions ON dbo.MgrConversions
FOR UPDATE 
AS
BEGIN

DECLARE @Defn varchar(64)
DECLARE @OldMapID int
DECLARE @NewMapID int
DECLARE @Source varchar(16)
DECLARE @Target varchar(16)
DECLARE @Exempt varchar(16)
DECLARE @OldMap varchar(256)
DECLARE @NewMap varchar(256)
DECLARE @EvtDesc varchar(4096)

	SELECT @OldMapID = d.ConvMapID, @NewMapID = i.ConvMapID
	FROM inserted i
	FULL OUTER JOIN deleted d
	ON i.ID = d.ID

	IF(@OldMapID != @NewMapID)
	BEGIN
		SELECT @Defn = cd.ConvDefnName
		FROM MgrConvDefns cd, inserted i
		WHERE cd.ID = i.ConvDefnID

		SELECT @Source = SourceFormat, @Target = TargetFormat, @Exempt = Exempt
		FROM MgrConvMaps
		WHERE ID = @OldMapID

		SET @EvtDesc = 'Conversion ' + @Defn + ': Map ' + @Source + '->' + @Target + ', Marshaling Exempt: ' + @Exempt + '  changed to '

		SELECT @Source = SourceFormat, @Target = TargetFormat, @Exempt = Exempt
		FROM MgrConvMaps
		WHERE ID = @NewMapID

		SET @EvtDesc = @EvtDesc + @Source + '->' + @Target + ', Marshaling Exempt: ' + @Exempt

		EXECUTE LogHistory @Evt = 'CONV_UPDATE', @Desc = @EvtDesc
	END
END
GO
