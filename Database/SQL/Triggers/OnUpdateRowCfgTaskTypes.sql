/***************************************************************************
* File: OnUpdateRowCfgTaskTypes.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnUpdateRowCfgTaskTypes Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/10/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnUpdateRowCfgTaskTypes]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnUpdateRowCfgTaskTypes]
GO

CREATE TRIGGER OnUpdateRowCfgTaskTypes ON dbo.CfgTaskTypes
FOR UPDATE 
AS
BEGIN

DECLARE @CaseID int
DECLARE @CaseName varchar(64)
DECLARE @OldName varchar(64)
DECLARE @NewName varchar(64)
DECLARE @OldInit int
DECLARE @NewInit int
DECLARE @OldClass varchar(64)
DECLARE @NewClass varchar(64)
DECLARE @OldSource varchar(64)
DECLARE @NewSource varchar(64)
DECLARE @OldAttach varchar(64)
DECLARE @NewAttach varchar(64)
DECLARE @EvtDesc varchar(4096)

	SELECT  @CaseID = d.CaseID,
		@OldName = d.TaskName, @NewName = i.TaskName, 
		@OldInit = d.IsDocInitiated, @NewInit = i.IsDocInitiated,
		@OldClass = d.InitiatingDocClass, @NewClass = i.InitiatingDocClass,
		@OldSource = d.InitiatingDocIdSource, @NewSource = i.InitiatingDocIdSource,
		@OldAttach = d.AttachingProperty, @NewAttach = i.AttachingProperty
	FROM inserted i
	FULL OUTER JOIN deleted d
	ON i.ID = d.ID

	SELECT @CaseName = CaseName
	FROM CfgCaseTypes
	WHERE ID = @CaseID

	IF((@OldName != @NewName) OR (@OldInit != @NewInit) OR (@OldClass != @NewClass) OR (@OldSource != @NewSource) OR (@OldAttach != @NewAttach))
	BEGIN
		SET @EvtDesc = 'Case: ' + @CaseName

		IF (@OldName != @NewName)
		BEGIN
			SET @EvtDesc = @EvtDesc + '; Name: ' + @OldName + ' changed to ' + @NewName + '; '
		END
		ELSE
		BEGIN
			SET @EvtDesc = @EvtDesc + '; Name: ' + @OldName + ' '
		END

		IF (@OldInit != @NewInit)
		BEGIN
			SET @EvtDesc = @EvtDesc + '; Is Document Initiated: ' + @OldInit + ' changed to ' + @NewInit + '; '
		END

		IF (@OldClass != @NewClass)
		BEGIN
			SET @EvtDesc = @EvtDesc + '; Initiating Document Class: ' + @OldClass + ' changed to ' + @NewClass + '; '
		END

		IF (@OldSource != @NewSource)
		BEGIN
			SET @EvtDesc = @EvtDesc + '; Initiating Document Id Source: ' + @OldSource + ' changed to ' + @NewSource + '; '
		END

		IF (@OldAttach != @NewAttach)
		BEGIN
			SET @EvtDesc = @EvtDesc + '; Attaching Property: ' + @OldAttach + ' changed to ' + @NewAttach + '; '
		END

		EXECUTE LogHistory @Evt = 'TASK_UPDATE', @Desc = @EvtDesc
	END
END
GO
