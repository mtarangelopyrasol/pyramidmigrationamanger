/***************************************************************************
* File: OnInsertRowCfgTaskProperties.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnInsertRowCfgTaskProperties Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/14/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnInsertRowCfgTaskProperties]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnInsertRowCfgTaskProperties]
GO

CREATE TRIGGER OnInsertRowCfgTaskProperties ON dbo.CfgTaskProperties
FOR INSERT 
AS
BEGIN

DECLARE @ConfigID int
DECLARE @TaskID int
DECLARE @Config varchar(64)
DECLARE @TaskName varchar(64)
DECLARE @Required int;
DECLARE @Mapped int;
DECLARE @Computed int;
DECLARE @MapName varchar(64)
DECLARE @EvtDesc varchar(4096)

	SELECT @ConfigID = i.ConfigID, @TaskID = i.TaskTypeID, @EvtDesc = i.Name + '(Source: ' + i.Source + '; Data Type: ' + i.DataType + ')', 
		@Required = i.Required, @Mapped = i.Mapped, @Computed = i.Computed, @MapName = i.MappingName
	FROM inserted i

	SELECT @Config = ConfigName
	FROM MgrConfigurations
	WHERE ID = @ConfigID

	SELECT @TaskName = TaskName
	FROM CfgTaskTypes
	WHERE ID = @TaskID

	SET @EvtDesc = 'Configuration: ' + @Config + '; Task: ' + @TaskName

	IF(@Required = 1)
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Required'
	END

	IF(@Computed = 1)
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Computed'
	END

	IF(@Mapped = 1)
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Mapped using mapping ' + @MapName
	END
	
	EXECUTE LogHistory @Evt = 'TaskPROP_ADD', @Desc = @EvtDesc
END
GO
