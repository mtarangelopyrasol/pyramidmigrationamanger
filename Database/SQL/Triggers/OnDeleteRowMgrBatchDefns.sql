/***************************************************************************
* File: OnDeleteRowMgrBatchDefns.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnDeleteRowMgrBatchDefns Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	11/05/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnDeleteRowMgrBatchDefns]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnDeleteRowMgrBatchDefns]
GO

CREATE TRIGGER OnDeleteRowMgrBatchDefns ON dbo.MgrBatchDefns
FOR DELETE 
AS
BEGIN

DECLARE @EvtDesc varchar(4096)

	SELECT @EvtDesc = 'ID: ' + CAST(d.ID AS varchar(2)) + '; Batch Defn: ' + d.BatchDefnName + '; Size: ' + CAST(d.BatchSize AS varchar(6)) + '; Condition: ' + d.BatchCondition
	FROM deleted d
	
	EXECUTE LogHistory @Evt = 'BATCHDEFN_DELETE', @Desc = @EvtDesc

	SELECT @EvtDesc = 'ID: ' + CAST(d.ID AS varchar(2)) + '; Source Content Path Prefix: ' + d.SourceContentPathPrefix + '; Staging Path: ' + d.StagingPath
	FROM deleted d
	
	EXECUTE LogHistory @Evt = 'BATCHDEFN_DELETE', @Desc = @EvtDesc

END
GO
