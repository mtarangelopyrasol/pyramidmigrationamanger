/***************************************************************************
* File: OnDeleteRowCfgTaskTypes.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnDeleteRowCfgTaskTypes Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/21/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnDeleteRowCfgTaskTypes]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnDeleteRowCfgTaskTypes]
GO

CREATE TRIGGER OnDeleteRowCfgTaskTypes ON dbo.CfgTaskTypes
FOR DELETE 
AS
BEGIN

DECLARE @TaskID int
DECLARE @EvtDesc varchar(4096)

	SELECT @TaskID = d.ID, @EvtDesc = 'ID: ' + CAST(d.ID AS varchar(2)) + '; Name: ' + d.TaskName + '; Is Document Initiated: ' + CAST(d.IsDocInitiated AS VARCHAR(2)) + '; Initiating Doc Class: ' + d.InitiatingDocClass +
				'; Initiating Doc Id Source: ' + d.InitiatingDocIdSource + '; Attaching Property: ' + d.AttachingProperty
	FROM dbo.CfgTaskTypes cd
	JOIN deleted d
	ON d.ID = cd.ID
	
	DELETE CfgTaskProperties
	WHERE TaskTypeID = @TaskID

	IF(@EvtDesc IS NOT NULL)
	BEGIN
		EXECUTE LogHistory @Evt = 'TASK_DELETE', @Desc = @EvtDesc
	END
END
GO
