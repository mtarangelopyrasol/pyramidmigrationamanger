/***************************************************************************
* File: OnDeleteRowMgrJobs.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnDeleteRowMgrJobs Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	11/05/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnDeleteRowMgrJobs]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnDeleteRowMgrJobs]
GO

CREATE TRIGGER OnDeleteRowMgrJobs ON dbo.MgrJobs
FOR DELETE 
AS
BEGIN

DECLARE	@JobName varchar(100)
DECLARE @CfgNum int
DECLARE @Level varchar(12)
DECLARE @EvtDesc varchar(4096)

	SELECT @CfgNum = d.ConfigID, @JobName = d.JobName, @EvtDesc = 'ID: ' + CAST(d.ID AS varchar(2)) + '; Config: ' + CAST(d.ConfigID AS varchar(2)) + '; Name: ' + d.JobName + '; Comment: ' + d.Comment
	FROM deleted d

	SELECT @Level = mc.ConfigLevel
	FROM dbo.MgrConfigurations mc
	WHERE mc.ID = @CfgNum

	EXECUTE DropJobTables @Job = @JobName, @Level = @Level

	EXECUTE LogHistory @Evt = 'JOB_DELETE', @Desc = @EvtDesc
END
GO
