/***************************************************************************
* File: OnDeleteRowMgrConfigurations.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnDeleteRowMgrConfigurations Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	11/05/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnDeleteRowMgrConfigurations]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnDeleteRowMgrConfigurations]
GO

CREATE TRIGGER OnDeleteRowMgrConfigurations ON dbo.MgrConfigurations
FOR DELETE 
AS
BEGIN

DECLARE @ConfigNum int
DECLARE @EvtDesc varchar(4096)

	SELECT @ConfigNum = d.ID, @EvtDesc = 'ID: ' + CAST(d.ID AS varchar(2)) + '; Name: ' + d.ConfigName + '; Comment: ' + d.Comment
	FROM deleted d
	
	EXECUTE LogHistory @Evt = 'CONFIG_DELETE', @Desc = @EvtDesc
	
END
GO
