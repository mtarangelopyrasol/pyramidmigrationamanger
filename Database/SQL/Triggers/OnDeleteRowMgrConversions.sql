/***************************************************************************
* File: OnDeleteRowMgrConversions.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnDeleteRowMgrConversions Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/21/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnDeleteRowMgrConversions]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnDeleteRowMgrConversions]
GO

CREATE TRIGGER OnDeleteRowMgrConversions ON dbo.MgrConversions
FOR DELETE 
AS
BEGIN

DECLARE @DefnID int
DECLARE @MapID int
DECLARE @Source varchar(16)
DECLARE @Target varchar(16)
DECLARE @Exempt varchar(16)
DECLARE @NewMap varchar(256)
DECLARE @EvtDesc varchar(4096)

	SELECT @DefnID = d.ConvDefnID, @MapID = d.ConvMapID
	FROM dbo.MgrConversions cd
	JOIN deleted d
	ON d.ID = cd.ID

	SELECT @EvtDesc = 'Conversion: ' + ConvDefnName
	FROM MgrConvDefns
	WHERE ID = @DefnID

	SELECT @Source = SourceFormat, @Target = TargetFormat, @Exempt = Exempt
	FROM MgrConvMaps
	WHERE ID = @MapID

	SET @EvtDesc = @EvtDesc + ' Deleted map ' + @Source + '->' + @Target + ' Exempt: ' + @Exempt

	EXECUTE LogHistory @Evt = 'CONVMAP_DELETE', @Desc = @EvtDesc
	
END
GO
