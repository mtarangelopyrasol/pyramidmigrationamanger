/***************************************************************************
* File: OnInsertRowCfgAnnotationSource.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnInsertRowCfgAnnotationSource Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/10/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnInsertRowCfgAnnotationSource]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnInsertRowCfgAnnotationSource]
GO

CREATE TRIGGER OnInsertRowCfgAnnotationSource ON dbo.CfgAnnotationSource
FOR INSERT 
AS
BEGIN

DECLARE @EvtDesc varchar(4096)
DECLARE @DType varchar(16)
DECLARE @Fmt varchar(32)
DECLARE @MValued varchar(1)

	SELECT @EvtDesc = 'CfgAnnotationSource - Config ID: ' + CAST(i.ConfigID AS varchar(2)) + '; Column: ' + CAST(i.ColumnID AS varchar(3)) + '; Name: ' + i.Name  + '; Data Type: ' + i.DataType + '; Max Length: ' + CAST(i.MaxLength AS varchar(4)), 
			@DType = i.DataType, @Fmt = i.Format, @MValued = i.MultiValued
	FROM inserted i
	
	IF(@DType = 'DateTime')
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Format: ' + @Fmt
	END

	IF(@MValued = 'Y')
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Multi Valued'
	END

	IF(@EvtDesc IS NOT NULL)
	BEGIN
		EXECUTE LogHistory @Evt = 'COLUMN_INSERT', @Desc = @EvtDesc
	END
END
GO
