/***************************************************************************
* File: OnDeleteRowMgrMappings.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnDeleteRowMgrMappings Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/21/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnDeleteRowMgrMappings]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnDeleteRowMgrMappings]
GO

CREATE TRIGGER OnDeleteRowMgrMappings ON dbo.MgrMappings
FOR DELETE 
AS
BEGIN

DECLARE @EvtDesc varchar(4096)

	SELECT @EvtDesc = 'ID: ' + CAST(d.ID AS varchar(2)) + '; Name: ' + d.MappingName
	FROM dbo.MgrMappings m
	JOIN deleted d
	ON d.ID = m.ID
	
	EXECUTE LogHistory @Evt = 'VALUEMAP_DELETE', @Desc = @EvtDesc
	
END
GO
