/***************************************************************************
* File: OnUpdateRowMgrMappingValues.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnUpdateRowMgrMappingValues Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/21/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnUpdateRowMgrMappingValues]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnUpdateRowMgrMappingValues]
GO

CREATE TRIGGER OnUpdateRowMgrMappingValues ON dbo.MgrMappingValues
FOR UPDATE 
AS
BEGIN

DECLARE @Id int
DECLARE @Defn varchar(64)
DECLARE @OldSource varchar(64)
DECLARE @OldTarget varchar(64)
DECLARE @NewSource varchar(64)
DECLARE @NewTarget varchar(64)
DECLARE @EvtDesc varchar(4096)

	SELECT @Id = d.ID, @OldSource = d.OldValue, @OldTarget = d.NewValue, @NewSource = i.OldValue, @NewTarget = i.NewValue
	FROM inserted i
	FULL OUTER JOIN deleted d
	ON i.ID = d.ID

	SELECT @Defn = MappingName
	FROM MgrMappings
	WHERE ID = @Id

	IF((@OldSource != @NewSource) OR (@OldTarget != @NewTarget))
	BEGIN
		SET @EvtDesc = 'Mapping ' + @Defn + ': Map ' + @OldSource + '->' + @OldTarget + '  changed to ' + @NewSource + '->' + @NewTarget

		EXECUTE LogHistory @Evt = 'VALUEMAP_UPDATE', @Desc = @EvtDesc
	END
END
GO
