/***************************************************************************
* File: OnDeleteRowCfgTaskProperties.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnDeleteRowCfgTaskProperties Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/21/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnDeleteRowCfgTaskProperties]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnDeleteRowCfgTaskProperties]
GO

CREATE TRIGGER OnDeleteRowCfgTaskProperties ON dbo.CfgTaskProperties
FOR DELETE 
AS
BEGIN

DECLARE @ConfigID int
DECLARE @TaskID int
DECLARE @Config varchar(64)
DECLARE @TaskName varchar(64)
DECLARE @Required int;
DECLARE @Mapped int;
DECLARE @Computed int;
DECLARE @MapName varchar(64)
DECLARE @EvtDesc varchar(4096)

	SELECT @ConfigID = d.ConfigID, @TaskID = d.TaskTypeID, @EvtDesc = d.Name + '(Source: ' + d.Source + '; Data Type: ' + d.DataType + ')', 
		@Required = d.Required, @Mapped = d.Mapped, @Computed = d.Computed, @MapName = d.MappingName
	FROM deleted d

	SELECT @Config = ConfigName
	FROM MgrConfigurations
	WHERE ID = @ConfigID

	SELECT @TaskName = TaskName
	FROM CfgTaskTypes
	WHERE ID = @TaskID

	SET @EvtDesc = 'Configuration: ' + @Config + '; Task: ' + @TaskName
	
	IF(@Required = 1)
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Required'
	END

	IF(@Computed = 1)
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Computed'
	END
	
	IF(@Mapped = 1)
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Mapped using mapping ' + @MapName
	END
	
	IF(@EvtDesc IS NOT NULL)
	BEGIN
		EXECUTE LogHistory @Evt = 'TASKPROP_DEL', @Desc = @EvtDesc
	END
	
END
GO
