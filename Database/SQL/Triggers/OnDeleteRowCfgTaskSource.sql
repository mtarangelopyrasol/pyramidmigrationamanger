/***************************************************************************
* File: OnDeleteRowCfgTaskSource.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnDeleteRowCfgTaskSource Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/10/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnDeleteRowCfgTaskSource]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnDeleteRowCfgTaskSource]
GO

CREATE TRIGGER OnDeleteRowCfgTaskSource ON dbo.CfgTaskSource
FOR DELETE 
AS
BEGIN

DECLARE @EvtDesc varchar(4096)
DECLARE @CfgID int
DECLARE @DType varchar(16)
DECLARE @Fmt varchar(32)
DECLARE @MValued varchar(1)
DECLARE @Col int

	SELECT @EvtDesc = 'CfgTaskSource - Config ID: ' + CAST(d.ConfigID AS varchar(2)) + '; Column: ' + CAST(d.ColumnID AS varchar(3)) + '; Name: ' + d.Name  + '; Data Type: ' + d.DataType + '; Max Length: ' + CAST(d.MaxLength AS varchar(4)), 
			@DType = d.DataType, @Fmt = d.Format, @MValued = d.MultiValued, @CfgID = d.ConfigID, @Col = d.ColumnID
	FROM deleted d
	
	IF(@DType = 'DateTime')
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Format: ' + @Fmt
	END

	IF(@MValued = 'Y')
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Multi Valued'
	END

	IF(@EvtDesc IS NOT NULL)
	BEGIN
		EXECUTE LogHistory @Evt = 'COLUMN_DELETE', @Desc = @EvtDesc
	END
END
GO
