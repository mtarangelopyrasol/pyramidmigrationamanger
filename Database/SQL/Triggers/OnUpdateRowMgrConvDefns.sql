/***************************************************************************
* File: OnUpdateRowMgrConvDefns.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnUpdateRowMgrConvDefns Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/21/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnUpdateRowMgrConvDefns]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnUpdateRowMgrConvDefns]
GO

CREATE TRIGGER OnUpdateRowMgrConvDefns ON dbo.MgrConvDefns
FOR UPDATE 
AS
BEGIN

DECLARE @Id int
DECLARE @OldName varchar(64)
DECLARE @NewName varchar(64)
DECLARE @OldSingle varchar(512)
DECLARE @NewSingle varchar(512)
DECLARE @EvtDesc varchar(4096)

	SELECT @EvtDesc = 'ID: ' + CAST(i.ID AS varchar(2)) + '; ', @Id = d.ID, @OldName = d.ConvDefnName, @NewName = i.ConvDefnName, 
		@OldSingle = d.MarshalToSingleDoc, @NewSingle = i.MarshalToSingleDoc
	FROM inserted i
	FULL OUTER JOIN deleted d
	ON i.ID = d.ID

	IF((@OldName != @NewName) OR (@OldSingle != @NewSingle))
	BEGIN
		IF (@OldName != @NewName)
		BEGIN
			SET @EvtDesc = @EvtDesc + 'Conversion Definition Name: ' + @OldName + ' changed to ' + @NewName
		END

		IF (@OldSingle != @NewSingle)
		BEGIN
			SET @EvtDesc = @EvtDesc + 'Marshal To Single Doc: ' + @OldSingle + ' changed to ' + @NewSingle
		END

		EXECUTE LogHistory @Evt = 'CONVDEFN_UPDATE', @Desc = @EvtDesc
	END
END
GO
