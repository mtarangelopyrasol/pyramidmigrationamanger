/***************************************************************************
* File: OnDeleteRowCfgCaseFolders.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnDeleteRowCfgCaseFolders Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/21/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnDeleteRowCfgCaseFolders]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnDeleteRowCfgCaseFolders]
GO

CREATE TRIGGER OnDeleteRowCfgCaseFolders ON dbo.CfgCaseFolders
FOR DELETE 
AS
BEGIN

DECLARE @EvtDesc varchar(4096)

	SELECT @EvtDesc = 'ID: ' + CAST(d.ID AS varchar(2)) + '; Name: ' + d.FolderName + '; Parent: ' + d.Parent
	FROM deleted d
	
	IF(@EvtDesc IS NOT NULL)
	BEGIN
		EXECUTE LogHistory @Evt = 'CASEFOLDER_DELETE', @Desc = @EvtDesc
	END
END
GO
