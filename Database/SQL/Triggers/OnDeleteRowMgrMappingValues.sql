/***************************************************************************
* File: OnDeleteRowMgrMappingValues.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnDeleteRowMgrMappingValues Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/21/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnDeleteRowMgrMappingValues]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnDeleteRowMgrMappingValues]
GO

CREATE TRIGGER OnDeleteRowMgrMappingValues ON dbo.MgrMappingValues
FOR DELETE 
AS
BEGIN

DECLARE @DefnID int
DECLARE @Source varchar(16)
DECLARE @Target varchar(16)
DECLARE @EvtDesc varchar(4096)

	SELECT @DefnID = d.MapID, @Source = d.OldValue, @Target = d.NewValue
	FROM dbo.MgrMappingValues mv
	JOIN deleted d
	ON d.ID = mv.ID

	SELECT @EvtDesc = 'MappingValue: ' + MappingName
	FROM MgrMappings
	WHERE ID = @DefnID

	SET @EvtDesc = @EvtDesc + ' Deleted map ' + @Source + '->' + @Target

	EXECUTE LogHistory @Evt = 'CONVMAP_DELETE', @Desc = @EvtDesc
	
END
GO
