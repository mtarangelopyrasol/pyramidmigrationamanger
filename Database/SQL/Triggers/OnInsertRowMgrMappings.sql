/***************************************************************************
* File: OnInsertRowMgrMappings.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnInsertRowMgrMappings Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/21/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnInsertRowMgrMappings]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnInsertRowMgrMappings]
GO

CREATE TRIGGER OnInsertRowMgrMappings ON dbo.MgrMappings
FOR INSERT 
AS
BEGIN

DECLARE @EvtDesc varchar(4096)

	SELECT @EvtDesc = 'ID: ' + CAST(i.ID AS varchar(2)) + '; Name: ' + i.MappingName
	FROM dbo.MgrMappings m
	JOIN inserted i
	ON i.ID = m.ID

	EXECUTE LogHistory @Evt = 'VALUEMAP_ADD', @Desc = @EvtDesc
END
GO
