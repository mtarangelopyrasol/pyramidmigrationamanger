/***************************************************************************
* File: OnInsertRowCfgAnnClasses.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnInsertRowCfgAnnClasses Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	11/8/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnInsertRowCfgAnnClasses]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnInsertRowCfgAnnClasses]
GO

CREATE TRIGGER OnInsertRowCfgAnnClasses ON dbo.CfgAnnClasses
FOR INSERT 
AS
BEGIN

DECLARE @EvtDesc varchar(4096)
DECLARE @ConfigID int
DECLARE @ClassID int
DECLARE @Name varchar(64)

DECLARE @MValued varchar(1)

	SELECT @EvtDesc = 'CfgAnnClass - Config ID: ' + CAST(i.ConfigID AS varchar(2)) + '; Name: ' + i.AnnClassName, @ConfigID = i.ConfigID, @ClassID = i.ID, @Name = i.AnnClassName		
	FROM inserted i
	
	EXECUTE GenerateAnnProperties @Config = @ConfigID, @AnnClassID = @ClassID, @AnnClassName = @Name

	EXECUTE LogHistory @Evt = 'ANNCLASS_ADD', @Desc = @EvtDesc
END
GO
