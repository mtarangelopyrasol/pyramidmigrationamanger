/***************************************************************************
* File: OnInsertRowMgrMappingValues.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnInsertRowMgrMappingValues Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/21/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnInsertRowMgrMappingValues]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnInsertRowMgrMappingValues]
GO

CREATE TRIGGER OnInsertRowMgrMappingValues ON dbo.MgrMappingValues
FOR INSERT 
AS
BEGIN

DECLARE @DefnID int
DECLARE @Source varchar(16)
DECLARE @Target varchar(16)
DECLARE @EvtDesc varchar(4096)

	SELECT @DefnID = i.MapID, @Source = i.OldValue, @Target = i.NewValue
	FROM dbo.MgrMappingValues cd
	JOIN inserted i
	ON i.ID = cd.ID

	SELECT @EvtDesc = 'Mapping: ' + MappingName
	FROM MgrMappings
	WHERE ID = @DefnID

	SET @EvtDesc = @EvtDesc + ' Added map ' + @Source + '->' + @Target

	EXECUTE LogHistory @Evt = 'VALUEMAP_ADD', @Desc = @EvtDesc
END
GO
