/***************************************************************************
* File: OnUpdateRowMgrMappings.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnUpdateRowMgrMappings Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/21/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnUpdateRowMgrMappings]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnUpdateRowMgrMappings]
GO

CREATE TRIGGER OnUpdateRowMgrMappings ON dbo.MgrMappings
FOR UPDATE 
AS
BEGIN

DECLARE @Id int
DECLARE @OldValue varchar(64)
DECLARE @NewValue varchar(64)
DECLARE @EvtDesc varchar(4096)

	SELECT @EvtDesc = 'ID: ' + CAST(i.ID AS varchar(2)) + '; ', @Id = d.ID, @OldValue = d.MappingName, @NewValue = i.MappingName
	FROM inserted i
	FULL OUTER JOIN deleted d
	ON i.ID = d.ID

	IF(@OldValue != @NewValue)
	BEGIN
		IF (@OldValue != @NewValue)
		BEGIN
			SET @EvtDesc = @EvtDesc + 'Mapping Definiton Name: ' + @OldValue + ' changed to ' + @NewValue
		END

		EXECUTE LogHistory @Evt = 'VALUEMAP_UPDATE', @Desc = @EvtDesc
	END
END
GO
