/***************************************************************************
* File: OnUpdateRowCfgIdentifiers.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnUpdateRowCfgIdentifiers Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/07/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnUpdateRowCfgIdentifiers]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnUpdateRowCfgIdentifiers]
GO

CREATE TRIGGER OnUpdateRowCfgIdentifiers ON dbo.CfgIdentifiers
FOR UPDATE 
AS
BEGIN

DECLARE @IDName varchar(64)
DECLARE @ConfigID integer
DECLARE @OldValue varchar(64)
DECLARE @NewValue varchar(64)
DECLARE @EvtDesc varchar(4096)

	SELECT @EvtDesc = 'Configuration ID: ' + CAST(d.ConfigID AS varchar(2)), @ConfigID = d.ConfigID, @IDName = d.IdentifierName, @OldValue = d.FieldName, @NewValue = i.FieldName
	FROM inserted i
	FULL OUTER JOIN deleted d
	ON i.ID = d.ID

	IF(@OldValue is null)
	BEGIN
		SET @OldValue = 'null'
	END

	IF(@NewValue is null)
	BEGIN
		SET @NewValue = 'null'
	END

	IF (@OldValue != @NewValue)
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Identifier: ' + @IDName + ' changed from ' + @OldValue + ' to ' + @NewValue
		EXECUTE LogHistory @Evt = 'IDENTIFIER_SET', @Desc = @EvtDesc

		IF (@IDName = 'DocTraceIdentifier')
		BEGIN
			IF (@NewValue != 'null')
			BEGIN
				UPDATE CfgDocClassProperties
				SET Source = 'MMID', Mapped = 0, Computed = 1, MappingName = ''
				WHERE Name = @NewValue AND ConfigID = @ConfigID
			END

			IF (@OldValue != 'null')
			BEGIN
				UPDATE CfgDocClassProperties
				SET Source = ''
				WHERE Name = @OldValue AND ConfigID = @ConfigID
			END
		END
	END
END
GO
