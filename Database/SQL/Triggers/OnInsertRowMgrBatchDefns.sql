/***************************************************************************
* File: OnInsertRowMgrBatchDefns.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnInsertRowMgrBatchDefns Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	11/05/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnInsertRowMgrBatchDefns]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnInsertRowMgrBatchDefns]
GO

CREATE TRIGGER OnInsertRowMgrBatchDefns ON dbo.MgrBatchDefns
FOR INSERT 
AS
BEGIN

DECLARE @EvtDesc varchar(4096)

	SELECT @EvtDesc = 'ID: ' + CAST(i.ID AS varchar(2)) + '; Batch Defn: ' + i.BatchDefnName + '; Size: ' + CAST(i.BatchSize AS varchar(6)) + '; Condition: ' + i.BatchCondition
	FROM inserted i
	
	EXECUTE LogHistory @Evt = 'BATCHDEFN_ADD', @Desc = @EvtDesc

	SELECT @EvtDesc = 'ID: ' + CAST(i.ID AS varchar(2)) + '; Source Content Path Prefix: ' + i.SourceContentPathPrefix + '; Staging Path: ' + i.StagingPath
	FROM inserted i
	
	EXECUTE LogHistory @Evt = 'BATCHDEFN_ADD', @Desc = @EvtDesc
	
END
GO
