/***************************************************************************
* File: OnUpdateRowCfgCaseFolders.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnUpdateRowCfgCaseFolders Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/10/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnUpdateRowCfgCaseFolders]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnUpdateRowCfgCaseFolders]
GO

CREATE TRIGGER OnUpdateRowCfgCaseFolders ON dbo.CfgCaseFolders
FOR UPDATE 
AS
BEGIN

DECLARE @ConfigID int
DECLARE @Config varchar(64)
DECLARE @CaseID int
DECLARE @CaseName varchar(64)
DECLARE @OldName varchar(64)
DECLARE @OldParent varchar(64)
DECLARE @NewName varchar(64)
DECLARE @NewParent varchar(64)
DECLARE @EvtDesc varchar(4096)

	SELECT  @ConfigID = d.ConfigID, @CaseID = d.CaseTypeID,
		@OldName = d.FolderName, @NewName = i.FolderName, 
		@OldParent = d.Parent, @NewParent = i.Parent
	FROM inserted i
	FULL OUTER JOIN deleted d
	ON i.ID = d.ID

	SELECT @Config = ConfigName
	FROM MgrConfigurations
	WHERE ID = @ConfigID

	SELECT @CaseName = CaseName
	FROM CfgCaseTypes
	WHERE ID = @CaseID

	IF((@OldName != @NewName) OR (@OldParent != @NewParent))
	BEGIN
		SET @EvtDesc = 'Configuration: ' + @Config + '; Case: ' + @CaseName

		IF (@OldName != @NewName)
		BEGIN
			SET @EvtDesc = @EvtDesc + '; Name: ' + @OldName + ' changed to ' + @NewName + '; '
		END

		IF (@OldParent != @NewParent)
		BEGIN
			SET @EvtDesc = @EvtDesc + '; Parent: ' + @OldParent + ' changed to ' + @NewParent + '; '
		END

		EXECUTE LogHistory @Evt = 'CASEFOLDER_UPDATE', @Desc = @EvtDesc
	END
END
GO
