/***************************************************************************
* File: OnUpdateRowMgrJobs.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnUpdateRowMgrJobs Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	11/05/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnUpdateRowMgrJobs]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnUpdateRowMgrJobs]
GO

CREATE TRIGGER OnUpdateRowMgrJobs ON dbo.MgrJobs
AFTER UPDATE 
AS
BEGIN

DECLARE @OldConfig int
DECLARE @NewConfig int
DECLARE @OldConfigName varchar(64)
DECLARE @NewConfigName varchar(64)
DECLARE @OldLevel varchar(12)
DECLARE @NewLevel varchar(12)
DECLARE @OldConv int
DECLARE @NewConv int
DECLARE @OldName varchar(64)
DECLARE @NewName varchar(64)
DECLARE @OldComment varchar(64)
DECLARE @NewComment varchar(64)
DECLARE @EvtDesc varchar(4096)
DECLARE @NewTables int

	SET NOCOUNT ON
	SET @NewTables = 0

	UPDATE t SET LastUpdated = CURRENT_TIMESTAMP
	FROM MgrJobs t
	WHERE EXISTS (SELECT 1 FROM inserted WHERE ID = t.ID);

	SELECT @EvtDesc = 'ID: ' + CAST(i.ID AS varchar(2)), @OldConfig = d.ConfigID, @NewConfig = i.ConfigID, @OldConv = d.ConvDefnID, @NewConv = i.ConvDefnID, @OldName = d.JobName, @NewName = i.JobName, @OldComment = d.Comment, @NewComment = i.Comment 
	FROM inserted i
	FULL OUTER JOIN deleted d
	ON i.ID = d.ID

	SELECT @OldLevel = mc.ConfigLevel, @OldConfigName = mc.ConfigName
	FROM dbo.MgrConfigurations mc
	WHERE mc.ID = @OldConfig

	SELECT @NewLevel = mc.ConfigLevel, @NewConfigName = mc.ConfigName
	FROM dbo.MgrConfigurations mc
	WHERE mc.ID = @NewConfig

	IF(@OldConfig != @NewConfig)
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Configuration Definition: ' + @OldConfigName + ' changed to ' + @NewConfigName
		SET @NewTables = 1
	END

	IF (@OldConv != @NewConv)
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Conversion Definition: ' + CAST(@OldConv AS varchar(2)) + ' changed to ' + CAST(@NewConv AS varchar(2))
	END

	IF (@OldName != @NewName)
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Name: ' + @OldName + ' changed to ' + @NewName
		SET @NewTables = 1
	END

	IF (@OldComment != @NewComment)
	BEGIN
		SET @EvtDesc = @EvtDesc + '; Comment: ' + @OldComment + ' changed to ' + @NewComment
	END

	IF(@NewTables = 1)
	BEGIN
		EXECUTE DropJobTables @Job = @OldName, @Level = @OldLevel
		EXECUTE GenerateJobTables @Job = @NewName, @ConfigNum = @NewConfig, @Level = @NewLevel
	END

	EXECUTE LogHistory @Evt = 'JOB_UPDATE', @Desc = @EvtDesc

END
GO
