/***************************************************************************
* File: OnInsertRowCfgDocClasses.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnInsertRowCfgDocClasses Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/21/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnInsertRowCfgDocClasses]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnInsertRowCfgDocClasses]
GO

CREATE TRIGGER OnInsertRowCfgDocClasses ON dbo.CfgDocClasses
FOR INSERT 
AS
BEGIN

DECLARE @EvtDesc varchar(4096)
DECLARE @DType varchar(16)
DECLARE @Fmt varchar(32)
DECLARE @MValued varchar(1)
DECLARE @ConfigID int
DECLARE @ClassID int

	SELECT @EvtDesc = 'CfgDocClass - Config ID: ' + CAST(i.ConfigID AS varchar(2)) + '; Name: ' + i.DocClassName, @ConfigID = i.ConfigID, @ClassID = i.ID		
	FROM inserted i

	EXECUTE GenerateDocProperties @Config = @ConfigID, @DocClassID = @ClassID
	
	EXECUTE LogHistory @Evt = 'DOCCLASS_ADD', @Desc = @EvtDesc
END
GO
