/***************************************************************************
* File: OnUpdateRowCfgDocClasses.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnUpdateRowCfgDocClasses Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/10/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnUpdateRowCfgDocClasses]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnUpdateRowCfgDocClasses]
GO

CREATE TRIGGER OnUpdateRowCfgDocClasses ON dbo.CfgDocClasses
FOR UPDATE 
AS
BEGIN

DECLARE @CfgID int
DECLARE @Config varchar(64)
DECLARE @OldName varchar(64)
DECLARE @NewName varchar(64)
DECLARE @EvtDesc varchar(4096)

	SELECT  @CfgID = d.ConfigID,
		@OldName = d.DocClassName, @NewName = i.DocClassName
	FROM inserted i
	FULL OUTER JOIN deleted d
	ON i.ID = d.ID

	SELECT @Config = ConfigName
	FROM MgrConfigurations
	WHERE ID = @CfgID

	IF(@OldName != @NewName)
	BEGIN
		SET @EvtDesc = 'Configuration: ' + @Config + '; Name: ' + @OldName + ' changed to ' + @NewName + '; '
		
		EXECUTE LogHistory @Evt = 'DOCCLASS_UPDATE', @Desc = @EvtDesc
	END
END
GO
