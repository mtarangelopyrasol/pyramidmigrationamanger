/***************************************************************************
* File: OnInsertRowCfgCaseFolders.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnInsertRowCfgCaseFolders Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/14/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnInsertRowCfgCaseFolders]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnInsertRowCfgCaseFolders]
GO

CREATE TRIGGER OnInsertRowCfgCaseFolders ON dbo.CfgCaseFolders
FOR INSERT 
AS
BEGIN

DECLARE @ConfigID int
DECLARE @Config varchar(64)
DECLARE @CaseID int
DECLARE @CaseName varchar(64)
DECLARE @Name varchar(64)
DECLARE @Parent varchar(64)
DECLARE @EvtDesc varchar(4096)

	SELECT @ConfigID = i.ConfigID, @CaseID = i.CaseTypeID, @Name = i.FolderName, @Parent = i.Parent
	FROM inserted i

	SELECT @Config = ConfigName
	FROM MgrConfigurations
	WHERE ID = @ConfigID

	SELECT @CaseName = CaseName
	FROM CfgCaseTypes
	WHERE ID = @CaseID

	SET @EvtDesc = 'Configuration: ' + @Config + '; Case ' + @CaseName + '; Name: ' + @Name + '; Parent: ' + @Parent
	
	EXECUTE LogHistory @Evt = 'CASEFOLDER_ADD', @Desc = @EvtDesc
END
GO
