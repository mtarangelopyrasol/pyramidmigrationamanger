/***************************************************************************
* File: OnUpdateRowMgrBatchDefns.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnUpdateRowMgrBatchDefns Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	11/05/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnUpdateRowMgrBatchDefns]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnUpdateRowMgrBatchDefns]
GO

CREATE TRIGGER OnUpdateRowMgrBatchDefns ON dbo.MgrBatchDefns
FOR UPDATE 
AS
BEGIN

DECLARE @Id int
DECLARE @OldSize int
DECLARE @NewSize int
DECLARE @OldName varchar(64)
DECLARE @NewName varchar(64)
DECLARE @OldCondition varchar(512)
DECLARE @NewCondition varchar(512)
DECLARE @OldPrefix varchar(512)
DECLARE @NewPrefix varchar(512)
DECLARE @OldStaging varchar(512)
DECLARE @NewStaging varchar(512)
DECLARE @EvtDesc varchar(4096)

	SELECT @EvtDesc = 'ID: ' + CAST(i.ID AS varchar(2)) + '; ', @Id = d.ID, @OldSize = d.BatchSize, @NewSize = i.BatchSize, @OldName = d.BatchDefnName, @NewName = i.BatchDefnName, 
		@OldCondition = d.BatchCondition, @NewCondition = i.BatchCondition,
		@OldPrefix = d.SourceContentPathPrefix, @NewPrefix = i.SourceContentPathPrefix,
		@OldStaging = d.StagingPath, @NewStaging = i.StagingPath
	FROM inserted i
	FULL OUTER JOIN deleted d
	ON i.ID = d.ID

	IF((@OldName != @NewName) OR (@OldSize != @NewSize) OR (@OldCondition != @NewCondition))
	BEGIN
		IF (@OldName != @NewName)
		BEGIN
			SET @EvtDesc = @EvtDesc + 'Batch Definition Name: ' + @OldName + ' changed to ' + @NewName
		END

		IF (@OldSize != @NewSize)
		BEGIN
			SET @EvtDesc = @EvtDesc + 'Batch Size: ' + CAST(@OldSize AS varchar(12)) + ' changed to ' + CAST(@NewSize AS varchar(12))
		END

		
		IF (@OldCondition != @NewCondition)
		BEGIN
			SET @EvtDesc = @EvtDesc + 'Batch Condition: ' + @OldCondition + ' changed to ' + @NewCondition
		END

		EXECUTE LogHistory @Evt = 'BATCHDEFN_UPDATE', @Desc = @EvtDesc
	END

	IF (@OldPrefix != @NewPrefix)
	BEGIN
		SET @EvtDesc = 'ID: ' + CAST(@Id AS varchar(2)) + ' Source Content Path Prefix: ' + @OldPrefix + ' changed to ' + @NewPrefix
		EXECUTE LogHistory @Evt = 'BATCHDEFN_UPDATE', @Desc = @EvtDesc
	END

	IF (@OldStaging != @NewStaging)
	BEGIN
		SET @EvtDesc = 'ID: ' + CAST(@Id AS varchar(2)) + ' Staging Path: ' + @OldStaging + ' changed to ' + @NewStaging
		EXECUTE LogHistory @Evt = 'BATCHDEFN_UPDATE', @Desc = @EvtDesc
	END
END
GO
