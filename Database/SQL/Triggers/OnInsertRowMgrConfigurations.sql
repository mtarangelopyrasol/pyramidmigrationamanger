/***************************************************************************
* File: OnInsertRowMgrConfigurations.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnInsertRowMgrConfigurations Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	08/17/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnInsertRowMgrConfigurations]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnInsertRowMgrConfigurations]
GO

CREATE TRIGGER OnInsertRowMgrConfigurations ON dbo.MgrConfigurations
FOR INSERT 
AS
BEGIN

DECLARE @ConfigNum int
DECLARE @ConfigLevel varchar(12)
DECLARE @EvtDesc varchar(4096)

	SELECT @ConfigNum = i.ID, @ConfigLevel = i.ConfigLevel, @EvtDesc = 'ID: ' + CAST(i.ID AS varchar(2)) + '; Name: ' + i.ConfigName + '; Level: ' + i.ConfigLevel + '; Comment: ' + i.Comment
	FROM dbo.MgrConfigurations mc
	JOIN inserted i
	ON i.ID = mc.ID
	
	EXECUTE GenerateIdentifiers @Config = @ConfigNum, @Level = @ConfigLevel

	EXECUTE LogHistory @Evt = 'CONFIG_ADD', @Desc = @EvtDesc
	
END
GO
