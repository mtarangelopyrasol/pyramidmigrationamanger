/***************************************************************************
* File: OnInsertRowCfgCaseTypes.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnInsertRowCfgCaseTypes Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/14/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnInsertRowCfgCaseTypes]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnInsertRowCfgCaseTypes]
GO

CREATE TRIGGER OnInsertRowCfgCaseTypes ON dbo.CfgCaseTypes
FOR INSERT 
AS
BEGIN

DECLARE @ConfigID int
DECLARE @Config varchar(64)
DECLARE @CaseName varchar(64)
DECLARE @DocClass varchar(64)
DECLARE @EvtDesc varchar(4096)

	SELECT @ConfigID = i.ConfigID, @CaseName = i.CaseName, @DocClass = i.DefaultDocClass
	FROM inserted i

	SELECT @Config = ConfigName
	FROM MgrConfigurations
	WHERE ID = @ConfigID

	SET @EvtDesc = 'Case ' + @CaseName + ' added to Configuration: ' + @Config + '; Default Document Class: ' + @DocClass
	
	EXECUTE LogHistory @Evt = 'CASE_ADD', @Desc = @EvtDesc
END
GO
