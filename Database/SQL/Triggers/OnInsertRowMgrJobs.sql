/***************************************************************************
* File: OnInsertRowMgrJobs.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnInsertRowMgrJobs Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	08/13/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnInsertRowMgrJobs]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnInsertRowMgrJobs]
GO

CREATE TRIGGER OnInsertRowMgrJobs ON dbo.MgrJobs
FOR INSERT 
AS
BEGIN

DECLARE	@JobName varchar(100)
DECLARE @GenTabName varchar(100)
DECLARE @CfgNum int
DECLARE @EvtDesc varchar(4096)
DECLARE @Level varchar(12)

	SELECT @CfgNum = i.ConfigID, @JobName = i.JobName, @EvtDesc = 'ID: ' + CAST(i.ID AS varchar(2)) + '; Config: ' + CAST(i.ConfigID AS varchar(2)) + '; Name: ' + i.JobName + '; Comment: ' + i.Comment
	FROM dbo.MgrJobs mj
	JOIN inserted i
	ON i.ID = mj.ID

	SELECT @Level = mc.ConfigLevel
	FROM dbo.MgrConfigurations mc
	WHERE mc.ID = @CfgNum

	EXECUTE GenerateJobTables @Job = @JobName, @ConfigNum = @CfgNum, @Level = @Level

	EXECUTE LogHistory @Evt = 'JOB_ADD', @Desc = @EvtDesc
END
GO
