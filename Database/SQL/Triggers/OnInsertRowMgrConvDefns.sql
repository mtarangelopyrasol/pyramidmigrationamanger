/***************************************************************************
* File: OnInsertRowMgrConvDefns.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnInsertRowMgrConvDefns Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/20/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnInsertRowMgrConvDefns]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnInsertRowMgrConvDefns]
GO

CREATE TRIGGER OnInsertRowMgrConvDefns ON dbo.MgrConvDefns
FOR INSERT 
AS
BEGIN

DECLARE @DefnID int
DECLARE @EvtDesc varchar(4096)

	SELECT @DefnID = i.ID, @EvtDesc = 'ID: ' + CAST(i.ID AS varchar(2)) + '; Name: ' + i.ConvDefnName + '; Marshal To Single Doc: ' + CAST(i.MarshalToSingleDoc AS varchar(2))
	FROM dbo.MgrConvDefns cd
	JOIN inserted i
	ON i.ID = cd.ID

	EXECUTE AddRestrictedConvMaps @ConvDefnID = @DefnID

	EXECUTE LogHistory @Evt = 'CONVDEFN_ADD', @Desc = @EvtDesc
END
GO
