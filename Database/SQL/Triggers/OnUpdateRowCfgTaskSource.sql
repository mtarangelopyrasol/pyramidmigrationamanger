/***************************************************************************
* File: OnUpdateRowCfgTaskSource.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the OnUpdateRowCfgTaskSource Trigger 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/10/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[OnUpdateRowCfgTaskSource]')and OBJECTPROPERTY(id, N'IsTrigger') = 1
)
DROP TRIGGER [dbo].[OnUpdateRowCfgTaskSource]
GO

CREATE TRIGGER OnUpdateRowCfgTaskSource ON dbo.CfgTaskSource
FOR UPDATE 
AS
BEGIN

DECLARE @Id int
DECLARE @CfgId int
DECLARE @OldColumn int
DECLARE @NewColumn int
DECLARE @OldName varchar(64)
DECLARE @NewName varchar(64)
DECLARE @OldDataType varchar(16)
DECLARE @NewDataType varchar(16)
DECLARE @OldLength int
DECLARE @NewLength int
DECLARE @OldFormat varchar(32)
DECLARE @NewFormat varchar(32)
DECLARE @OldMultiValue varchar(1)
DECLARE @NewMultiValue varchar(1)
DECLARE @EvtDesc varchar(4096)

	SELECT @EvtDesc = 'CfgTaskSource - ID: ' + CAST(i.ID AS varchar(2)) + '; ', @Id = d.ID, @CfgId = d.ConfigID, @OldColumn = d.ColumnID, @NewColumn = i.ColumnID, @OldName = d.Name, @NewName = i.Name, 
		@OldDataType = d.DataType, @NewDataType = i.DataType,
		@OldLength = d.MaxLength, @NewLength = i.MaxLength,
		@OldFormat = d.Format, @NewFormat = i.Format
	FROM inserted i
	FULL OUTER JOIN deleted d
	ON i.ID = d.ID

	IF (@OldColumn != @NewColumn)
	BEGIN
		SET @EvtDesc = @EvtDesc + 'Column: ' + CAST(@OldColumn AS varchar(3)) + ' changed to ' + CAST(@NewColumn AS varchar(3)) + '; '
	END

	IF (@OldName != @NewName)
	BEGIN
		SET @EvtDesc = @EvtDesc + 'Name: ' + @OldName + ' changed to ' + @NewName + '; '
	END

	IF (@OldDataType != @NewDataType)
	BEGIN
		SET @EvtDesc = @EvtDesc + 'Data Type: ' + @OldDataType + ' changed to ' + @NewDataType + '; '
	END

	IF (@OldLength != @NewLength)
	BEGIN
		SET @EvtDesc = @EvtDesc + 'Length: ' + CAST(@OldLength AS varchar(12)) + ' changed to ' + CAST(@NewLength AS varchar(12)) + '; '
	END

	IF (@OldFormat != @NewFormat)
	BEGIN
		SET @EvtDesc = @EvtDesc + 'Format: ' + @OldFormat + ' changed to ' + @NewFormat + '; '
	END

	IF (@OldMultiValue != @NewMultiValue)
	BEGIN
		SET @EvtDesc = @EvtDesc + 'Multi Value: ' + @OldMultiValue + ' changed to ' + @NewMultiValue + '; '
	END

	EXECUTE LogHistory @Evt = 'COLUMN_UPDATE', @Desc = @EvtDesc	
END
GO
