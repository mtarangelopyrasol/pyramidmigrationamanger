/***************************************************************************
* File: SPGenerateIdentifiers.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the GenerateIdentifiers Stored Procedure 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	08/17/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[GenerateIdentifiers]')and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
DROP PROCEDURE [dbo].[GenerateIdentifiers]
GO

CREATE PROCEDURE GenerateIdentifiers
(
@Config int,
@Level varchar(12)
)
AS

BEGIN

DECLARE @Error int
DECLARE @Success int

	SELECT @Success = 0

BEGIN TRANSACTION

	
	INSERT INTO CfgIdentifiers (ConfigID, IdentifierName) VALUES
		(@Config, 'AnnotationIdentifier'),
		(@Config, 'AnnotatedObjTypeIdentifier'),
		(@Config, 'AnnotatedObjectIdentifier'), 
		(@Config, 'AnnotationAccountIdentifier'),
		(@Config, 'AnnotationClassIdentifier'),
		(@Config, 'AnnotationPageIdentifier'), 
		(@Config, 'AnnotationFileNameIdentifier'),
		(@Config, 'AnnotationTgtObjTypeIdentifier')

	IF (@Level = 'CASE')
	BEGIN
		INSERT INTO CfgIdentifiers (ConfigID, IdentifierName) VALUES 
			(@Config, 'CaseAccountIdentifier'), 
			(@Config, 'CaseTypeIdentifier')
	END

	INSERT INTO CfgIdentifiers (ConfigID, IdentifierName) VALUES 
			(@Config, 'DocAccountIdentifier'),
			(@Config, 'DocClassIdentifier'),
			(@Config, 'DocContNameIdentifier'),
			(@Config, 'DocDateIdentifier'),
			(@Config, 'DocFolderIdentifier'),
			(@Config, 'DocTraceIdentifier'),
			(@Config, 'DocTypeIdentifier'),
			(@Config, 'DocumentIdentifier'), 
			(@Config, 'DocMarshalingIdentifier'),
			(@Config, 'DocMimeTypeIdentifier'),
			(@Config, 'DocPageFileNameIdentifier'), 
			(@Config, 'DocPageIdentifier'),
			(@Config, 'DocPageNumberIdentifier'),
			(@Config, 'DocPageStatusIdentifier'),
			(@Config, 'DocPreAnnRotationIdentifier'),
			(@Config, 'DocPreAnnRotationDirectionIdentifier'),
			(@Config, 'DocRotationIdentifier'),
			(@Config, 'DocRotationDirectionIdentifier')

	IF (@Level = 'CASE')
	BEGIN
		INSERT INTO CfgIdentifiers (ConfigID, IdentifierName) VALUES 
			(@Config, 'TaskAccountIdentifier'),
			(@Config, 'TaskTypeIdentifier'),
			(@Config, 'TaskDescriptionIdentifier'),
			(@Config, 'AlwaysCreateFolders')
	END
	
	SET @Error = @@ERROR  
	IF @Error <> 0  
	BEGIN      
		GOTO LogError  
	END

COMMIT TRANSACTION
	SELECT @Success = 1
GOTO ProcEnd 

LogError:
ROLLBACK TRANSACTION

DECLARE @ErrMsg varchar(1000)
	
	SELECT @ErrMsg = [description] 
	FROM master.dbo.sysmessages  
	WHERE error = @Error
	
	INSERT INTO ErrorLog(Source, ErrMsg) VALUES ('spGenerateIdentifiers', @ErrMsg)  
	
ProcEnd:
END

GO

