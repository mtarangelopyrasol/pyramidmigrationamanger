/***************************************************************************
* File: SPAddRestrictedConvMaps.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the AddRestrictedConvMaps Stored Procedure 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/20/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[AddRestrictedConvMaps]')and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
DROP PROCEDURE [dbo].[AddRestrictedConvMaps]
GO

CREATE PROCEDURE AddRestrictedConvMaps
(
@ConvDefnID int
)
AS

BEGIN

DECLARE @Error int
DECLARE @Success int

	SELECT @Success = 0

BEGIN TRANSACTION

	INSERT INTO MgrConversions (ConvDefnID, ConvMapID) 
		SELECT @ConvDefnID, m.ID
		FROM MgrConvMaps m
		WHERE Restricted = 1

	INSERT INTO MgrConversions (ConvDefnID, ConvMapID) 
		SELECT @ConvDefnID, m.ID
		FROM MgrConvMaps m
		WHERE SourceFormat = 'ALL' AND TargetFormat = 'tif'

	SET @Error = @@ERROR  
	IF @Error <> 0  
	BEGIN      
		GOTO LogError  
	END

COMMIT TRANSACTION
	SELECT @Success = 1
GOTO ProcEnd 

LogError:
ROLLBACK TRANSACTION

DECLARE @ErrMsg varchar(1000)
	
	SELECT @ErrMsg = [description] 
	FROM master.dbo.sysmessages  
	WHERE error = @Error
	
	INSERT INTO ErrorLog(Source, ErrMsg) VALUES ('spAddRestrictedConvMaps', @ErrMsg)  
	
ProcEnd:
END

GO

