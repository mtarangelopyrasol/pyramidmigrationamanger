/***************************************************************************
* File: SPCreateAnnotationDataTable.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the CreateAnnotationDataTable Stored Procedure 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	08/13/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[CreateAnnotationDataTable]')and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
DROP PROCEDURE [dbo].[CreateAnnotationDataTable]
GO

CREATE PROCEDURE CreateAnnotationDataTable
(
@GenTable varchar(64),
@Config int
)
AS

BEGIN

DECLARE @Error int
DECLARE @Success int
DECLARE @SQL varchar(4000)
DECLARE @Fieldname varchar(64)
DECLARE @Fieldtype varchar(16)
DECLARE @Fieldlength int
DECLARE @FieldFormat varchar(32)
DECLARE @FieldMultiValued varchar(1)
DECLARE @FieldDesc varchar(64)
DECLARE @ValueTable varchar(64)
DECLARE @Valuetype varchar(16)
DECLARE @Valuelength int
DECLARE @ValueFormat varchar(32)
DECLARE @ValueDesc varchar(64)
DECLARE cur CURSOR LOCAL for
	SELECT Name, DataType, MaxLength, Format, MultiValued FROM CfgAnnotationSource WHERE ConfigID = @Config ORDER BY ColumnID

	SELECT @Success = 0

BEGIN TRANSACTION
	SET @SQL = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[@GenTable]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [dbo].[" + @GenTable + "]
	CREATE TABLE [dbo].[" + @GenTable + "] (
		[ID] [bigint] IDENTITY(1,1) NOT NULL,
		[Status][varchar](16) NOT NULL DEFAULT 'CONV_NEW',
		[BatchID] [int] NULL,
		PRIMARY KEY (ID)) ON [PRIMARY]"
	EXEC(@SQL)

	SET @Error = @@ERROR  
	IF @Error <> 0  
	BEGIN      
		GOTO LogError  
	END

	OPEN cur
	FETCH next FROM cur into @Fieldname, @Fieldtype, @Fieldlength, @FieldFormat, @FieldMultiValued

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @FieldMultiValued = 'Y'
		BEGIN
			SET @Valuetype = @Fieldtype
			SET @Valuelength = @Fieldlength
			SET @ValueFormat = @FieldFormat
			SET @Fieldtype = 'String'
			SET @Fieldlength = 36
		END

		SET @FieldDesc = 
		CASE 
			WHEN @Fieldtype = 'String' THEN @Fieldname + SPACE(1) + 'varchar' + '(' + CONVERT(varchar(16), @Fieldlength) + ') NULL'
			WHEN @Fieldtype = 'Integer' THEN @Fieldname + SPACE(1) + 'int' 
			WHEN @Fieldtype = 'DateTime' THEN @Fieldname + SPACE(1) + 'datetime NULL'
		END;
		
		SET @SQL = "ALTER TABLE [dbo].[" + @GenTable + "] ADD " + @FieldDesc
		EXEC(@SQL)

		SET @Error = @@ERROR  
		IF @Error <> 0  
		BEGIN      
			GOTO LogError  
		END

		IF @FieldMultiValued = 'Y'
		BEGIN
			SET @ValueTable = @GenTable + "_" + @Fieldname
			SET @SQL = "CREATE TABLE [dbo].[" + @ValueTable + "] (
				[ID] [bigint] IDENTITY(1,1) NOT NULL,
				[Link_Key] [varchar](36) NOT NULL,
				[Datatype] [varchar](32) NOT NULL DEFAULT '" + @Valuetype + "',
				[Format][varchar](32) NOT NULL DEFAULT '" + @ValueFormat + "',
				PRIMARY KEY (ID)) ON [PRIMARY]"
			EXEC(@SQL)

			SET @ValueDesc = 
			CASE 
				WHEN @Valuetype = 'String' THEN 'Value varchar' + '(' + CONVERT(varchar(16), @Valuelength) + ') NULL'
				WHEN @Valuetype = 'Integer' THEN 'Value int' 
				WHEN @Valuetype = 'DateTime' THEN 'Value datetime NULL'
			END;
			
			SET @SQL = "ALTER TABLE [dbo].[" + @ValueTable + "] ADD " + @ValueDesc
			EXEC(@SQL)

			SET @Error = @@ERROR  
			IF @Error <> 0  
			BEGIN      
				GOTO LogError  
			END
		END
		FETCH next FROM cur into @Fieldname, @Fieldtype, @Fieldlength, @FieldFormat, @FieldMultiValued

	END
	CLOSE cur
	DEALLOCATE cur

COMMIT TRANSACTION
	SELECT @Success = 1
GOTO ProcEnd 

LogError:
ROLLBACK TRANSACTION

DECLARE @ErrMsg varchar(1000)
	
	SELECT @ErrMsg = [description] 
	FROM master.dbo.sysmessages  
	WHERE error = @Error
	
	INSERT INTO ErrorLog(Source, ErrMsg) VALUES ('spCreateAnnotationDataTable', @ErrMsg)  
	
ProcEnd:
END

GO

