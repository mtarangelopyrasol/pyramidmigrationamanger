/***************************************************************************
* File: SPCreateCEAnnotationsTable.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the CreateCEAnnotationsTable Stored Procedure 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	08/13/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[CreateCEAnnotationsTable]')and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
DROP PROCEDURE [dbo].[CreateCEAnnotationsTable]
GO

CREATE PROCEDURE CreateCEAnnotationsTable
(
@GenTable AS varchar(64)
)
AS

BEGIN

DECLARE @Error int
DECLARE @Success int
DECLARE @SQL varchar(2048)

	SELECT @Success = 0
	
BEGIN TRANSACTION
	SET @SQL = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[@GenTable]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [dbo].[" + @GenTable + "]
	CREATE TABLE [dbo].[" + @GenTable + "] (
		[ID] [bigint] IDENTITY(1,1) NOT NULL,
		[BatchID] [int] NOT NULL,
		[AnnType] [varchar] (16) NOT NULL DEFAULT 'DOCUMENT',
		[AnnClass] [varchar] (64) NOT NULL,
		[PageID] [int] NOT NULL,
		[ObjectID] [int] NOT NULL,
		[FilePath] [varchar] (1024) NOT NULL,
		[CE_GUID] [varchar] (48) NULL,
		[MigrationID][int] NULL,
		[Status][varchar](16) NOT NULL DEFAULT 'ING_NEW',
		[MMAccountID] [varchar](64) NULL,
		[StartTime] [datetime] NULL,
		[EndTime] [datetime] NULL,
		PRIMARY KEY (ID)) ON [PRIMARY]"

EXEC(@SQL)

	SET @Error = @@ERROR  
	IF @Error <> 0  
	BEGIN      
		GOTO LogError  
	END

COMMIT TRANSACTION
	SELECT @Success = 1
GOTO ProcEnd 

LogError:
ROLLBACK TRANSACTION

DECLARE @ErrMsg varchar(1000)
	
	SELECT @ErrMsg = [description] 
	FROM master.dbo.sysmessages  
	WHERE error = @Error
	
	INSERT INTO ErrorLog(Source, ErrMsg) VALUES ('spCreateCEAnnotationsTable', @ErrMsg)  
	
ProcEnd:
END

GO

