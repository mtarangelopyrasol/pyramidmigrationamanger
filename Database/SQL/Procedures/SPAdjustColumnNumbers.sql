/***************************************************************************
* File: SPAdjustColumnNumbers.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the AdjusColumnNumbers Stored Procedure 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/11/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[AdjustColumnNumbers]')and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
DROP PROCEDURE [dbo].[AdjustColumnNumbers]
GO

CREATE PROCEDURE AdjustColumnNumbers
(
@ConfigID int,
@TableName varchar(64),
@OldPosition int,
@NewPosition int
)
AS

BEGIN

DECLARE @Error int
DECLARE @Success int
DECLARE @curSQL varchar(4000)
DECLARE @SQL varchar(4000)
DECLARE @Id int
DECLARE @ColumnId int
DECLARE @NextCol int
		
	SELECT @Success = 0

BEGIN TRANSACTION
	
	IF(@NewPosition = 0)
	BEGIN
		/* deletion */
		
		SET @curSQL = "DECLARE curDel CURSOR for SELECT ID, ColumnID FROM " + @TableName + " WHERE ConfigID = " + CAST(@ConfigID AS varchar(3)) + " AND ColumnID > " + CAST(@OldPosition AS varchar(4)) + " ORDER BY ColumnID"
		EXEC(@curSQL)
		
		OPEN curDel
		FETCH next FROM curDel into @Id, @ColumnId

		SET @NextCol = @OldPosition
		
		WHILE @@FETCH_STATUS = 0
		BEGIN	
			SET @SQL = "UPDATE [dbo].[" + @TableName + "] SET ColumnID = " + CAST(@NextCol AS varchar(3)) + " WHERE ID = " + CAST(@Id AS varchar(4))
			EXEC(@SQL)

			SET @NextCol = @NextCol + 1

			SET @Error = @@ERROR  
			IF @Error <> 0  
			BEGIN      
				GOTO LogError  
			END
			
			FETCH next FROM curDel into @Id, @ColumnId
		END
		CLOSE curDel
		DEALLOCATE curDel	
	END
	ELSE
	BEGIN
		/* move */
		IF(@NewPosition > @OldPosition)
		BEGIN
			SET @curSQL = "DECLARE curMoveDown CURSOR for SELECT ID, ColumnID FROM " + @TableName + " WHERE ConfigID = " + CAST(@ConfigID AS VARCHAR(2)) + " AND ColumnID > " + CAST(@OldPosition AS varchar(4)) + " AND ColumnID <= " + CAST(@NewPosition AS varchar(4)) + " ORDER BY ColumnID"
			EXEC(@curSQL)

			OPEN curMoveDown
			FETCH next FROM curMoveDown into @Id, @ColumnId

			SET @NextCol = @OldPosition
	
			WHILE @@FETCH_STATUS = 0
			BEGIN	
				SET @SQL = "UPDATE [dbo].[" + @TableName + "] SET ColumnID = " + CAST(@NextCol AS varchar(3)) + " WHERE ID = " + CAST(@Id AS varchar(4))
				EXEC(@SQL)

				SET @NextCol = @ColumnId

				SET @Error = @@ERROR  
				IF @Error <> 0  
				BEGIN      
					GOTO LogError  
				END
				
				FETCH next FROM curMoveDown into @Id, @ColumnId
			END
			CLOSE curMoveDown
			DEALLOCATE curMoveDown
		END
		ELSE
		BEGIN
			SET @curSQL = "DECLARE curMoveUp CURSOR for SELECT ID, ColumnID FROM " + @TableName + " WHERE ConfigID = " + CAST(@ConfigID AS varchar(2)) + " AND ColumnID >= " + CAST(@NewPosition AS varchar(4)) + " AND ColumnID < " + CAST(@OldPosition AS varchar(4)) + " ORDER BY ColumnID DESC"
			EXEC(@curSQL)

			OPEN curMoveUp
			FETCH next FROM curMoveUp into @Id, @ColumnId

			SET @NextCol = @OldPosition

			WHILE @@FETCH_STATUS = 0
			BEGIN	
				SET @SQL = "UPDATE [dbo].[" + @TableName + "] SET ColumnID = " + CAST(@NextCol AS varchar(3)) + " WHERE ID = " + CAST(@Id AS varchar(4))
				EXEC(@SQL)

				SET @NextCol = @ColumnId

				SET @Error = @@ERROR  
				IF @Error <> 0  
				BEGIN      
					GOTO LogError  
				END
				
				FETCH next FROM curMoveUp into @Id, @ColumnId
			END
			CLOSE curMoveUp
			DEALLOCATE curMoveUp
		END
	END
	
		
COMMIT TRANSACTION
	SELECT @Success = 1
GOTO ProcEnd 

LogError:
ROLLBACK TRANSACTION

DECLARE @ErrMsg varchar(1000)
	
	SELECT @ErrMsg = [description] 
	FROM master.dbo.sysmessages  
	WHERE error = @Error
	
	INSERT INTO ErrorLog(Source, ErrMsg) VALUES ('spAdjustColumnNumbers', @ErrMsg)  
	
ProcEnd:
END

GO
