/***************************************************************************
* File: SPCreateCEDocumentsTable.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the CreateCEDocumentsTable Stored Procedure 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	08/13/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[CreateCEDocumentsTable]')and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
DROP PROCEDURE [dbo].[CreateCEDocumentsTable]
GO

CREATE PROCEDURE CreateCEDocumentsTable
(
@GenTable AS varchar(64),
@CfgLevel AS varchar(12)
)
AS

BEGIN

DECLARE @Error int
DECLARE @Success int
DECLARE @SQL varchar(2048)

	SELECT @Success = 0
	
BEGIN TRANSACTION
	IF (@CfgLevel = 'CASE')
	BEGIN
		SET @SQL = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[@GenTable]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
		DROP TABLE [dbo].[" + @GenTable + "]
	
		CREATE TABLE [dbo].[" + @GenTable + "] (
			[ID] [bigint] IDENTITY(1,1) NOT NULL,
			[BatchID] [int] NOT NULL,
			[CaseID] [int] NOT NULL,
			[DocClass] [varchar] (64) NOT NULL,
			[MimeType] [varchar] (128) NULL,
			[Folder] [varchar] (256) NULL,
			[ContainmentName] [varchar] (256) NULL,
			[CE_GUID] [varchar] (48) NULL,
			[MigrationID][int] NULL,
			[Status][varchar](16) NOT NULL DEFAULT 'ING_NEW',
			[MMAccountID] [varchar](64) NULL,
			PRIMARY KEY (ID)) ON [PRIMARY]"
	END
	ELSE
	BEGIN
		SET @SQL = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[@GenTable]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
		DROP TABLE [dbo].[" + @GenTable + "]

		CREATE TABLE [dbo].[" + @GenTable + "] (
			[ID] [bigint] IDENTITY(1,1) NOT NULL,
			[BatchID] [int] NOT NULL,
			[DocClass] [varchar] (64) NOT NULL,
			[MimeType] [varchar] (128) NULL,
			[Folder] [varchar] (256) NULL,
			[ContainmentName] [varchar] (256) NULL,
			[CE_GUID] [varchar] (48) NULL,
			[MigrationID][int] NULL,
			[Status][varchar](16) NOT NULL DEFAULT 'ING_NEW',
			[MMAccountID] [varchar](64) NULL,
			PRIMARY KEY (ID)) ON [PRIMARY]"
	END

EXEC(@SQL)

	SET @Error = @@ERROR  
	IF @Error <> 0  
	BEGIN      
		GOTO LogError  
	END

COMMIT TRANSACTION
	SELECT @Success = 1
GOTO ProcEnd 

LogError:
ROLLBACK TRANSACTION

DECLARE @ErrMsg varchar(1000)
	
	SELECT @ErrMsg = [description] 
	FROM master.dbo.sysmessages  
	WHERE error = @Error
	
	INSERT INTO ErrorLog(Source, ErrMsg) VALUES ('spCreateCEDocumentsTable', @ErrMsg)  
	
ProcEnd:
END

GO

