/***************************************************************************
* File: SPCreateCEAnnotationHistoryTable.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the CreateCEAnnotationHistoryTable Stored Procedure 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	08/13/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[CreateCEAnnotationHistoryTable]')and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
DROP PROCEDURE [dbo].[CreateCEAnnotationHistoryTable]
GO

CREATE PROCEDURE CreateCEAnnotationHistoryTable
(
@GenTable AS varchar(64)
)
AS

BEGIN

DECLARE @Error int
DECLARE @Success int
DECLARE @SQL varchar(2048)

	SELECT @Success = 0
	
BEGIN TRANSACTION
	SET @SQL = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[@GenTable]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [dbo].[" + @GenTable + "]
	CREATE TABLE [dbo].[" + @GenTable + "] (
		[ID] [bigint] IDENTITY(1,1) NOT NULL,
		[AnnotationID] [int] NOT NULL,
		[EventDate] [datetime] NOT NULL,
		[EventCreator][varchar](64) NOT NULL,
		[EventDescription][varchar](64) NOT NULL,
		[Status][varchar](16) NOT NULL DEFAULT 'NEW',
		PRIMARY KEY (ID)) ON [PRIMARY]"

EXEC(@SQL)

	SET @Error = @@ERROR  
	IF @Error <> 0  
	BEGIN      
		GOTO LogError  
	END

COMMIT TRANSACTION
	SELECT @Success = 1
GOTO ProcEnd 

LogError:
ROLLBACK TRANSACTION

DECLARE @ErrMsg varchar(1000)
	
	SELECT @ErrMsg = [description] 
	FROM master.dbo.sysmessages  
	WHERE error = @Error
	
	INSERT INTO ErrorLog(Source, ErrMsg) VALUES ('spCreateCEAnnotationHistoryTable', @ErrMsg)  
	
ProcEnd:
END

GO

