/***************************************************************************
* File: SPGenerateAnnProperties.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the GenerateAnnProperties Stored Procedure 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	11/08/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[GenerateAnnProperties]')and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
DROP PROCEDURE [dbo].[GenerateAnnProperties]
GO

CREATE PROCEDURE GenerateAnnProperties
(
@Config int,
@AnnClassID int,
@AnnClassName varchar(64)
)
AS

BEGIN

DECLARE @Error int
DECLARE @Success int

	SELECT @Success = 0

BEGIN TRANSACTION

	if((@AnnClassName = 'Annotation') OR (@AnnClassName = 'CmAcmCaseComment') OR (@AnnClassName = 'CmAcmTaskComment') OR (@AnnClassName = 'CmAcmVersionSeriesComment'))
	BEGIN
		INSERT INTO CfgAnnClassProperties (ConfigID, AnnClassID, Name, Source, DataType, Required, Mapped, Computed, MappingName) VALUES 
			(@Config, @AnnClassID, 'AnnotatedObject', 'ANNOTATED_OBJECT_ID', 'SingletonString', 1, 0, 1, '')
	END

	if((@AnnClassName = 'CmAcmCaseComment') OR (@AnnClassName = 'CmAcmTaskComment') OR (@AnnClassName = 'CmAcmVersionSeriesComment'))
	BEGIN
		INSERT INTO CfgAnnClassProperties (ConfigID, AnnClassID, Name, Source, DataType, Required, Mapped, Computed, MappingName) VALUES 
			(@Config, @AnnClassID, 'CmAcmAction', 'ACTION_CODE', 'SingletonInteger', 1, 0, 1, '')
		INSERT INTO CfgAnnClassProperties (ConfigID, AnnClassID, Name, Source, DataType, Required, Mapped, Computed, MappingName) VALUES 
			(@Config, @AnnClassID, 'CmAcmCommentText', '', 'SingletonString', 1, 0, 0, '')
	END
		
	if(@AnnClassName = 'CmAcmTaskComment')
	BEGIN
		INSERT INTO CfgAnnClassProperties (ConfigID, AnnClassID, Name, Source, DataType, Required, Mapped, Computed, MappingName) VALUES 
			(@Config, @AnnClassID, 'CmAcmCommentedTask', 'ANNOTATED_TASK', 'SingletonString', 1, 0, 1, '')
	END

	if(@AnnClassName = 'CmAcmVersionSeriesComment')
	BEGIN
		INSERT INTO CfgAnnClassProperties (ConfigID, AnnClassID, Name, Source, DataType, Required, Mapped, Computed, MappingName) VALUES 
			(@Config, @AnnClassID, 'CmAcmCommentedVersionSeries', 'DOC_VERSION_SERIES', 'SingletonString', 1, 0, 1, '')
	END

	SET @Error = @@ERROR  
	IF @Error <> 0  
	BEGIN      
		GOTO LogError  
	END

COMMIT TRANSACTION
	SELECT @Success = 1
GOTO ProcEnd 

LogError:
ROLLBACK TRANSACTION

DECLARE @ErrMsg varchar(1000)
	
	SELECT @ErrMsg = [description] 
	FROM master.dbo.sysmessages  
	WHERE error = @Error
	
	INSERT INTO ErrorLog(Source, ErrMsg) VALUES ('spGenerateAnnProperties', @ErrMsg)  
	
ProcEnd:
END

GO

