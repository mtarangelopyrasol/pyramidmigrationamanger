/***************************************************************************
* File: SPGenerateJobTables.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the GenerateJobTables Stored Procedure 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/26/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[GenerateJobTables]')and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
DROP PROCEDURE [dbo].[GenerateJobTables]
GO

CREATE PROCEDURE GenerateJobTables
(
@Job varchar(100),
@ConfigNum int,
@Level varchar(12)
)
AS

BEGIN

DECLARE @GenTabName varchar(100)
DECLARE @Error int
DECLARE @Success int

	SELECT @Success = 0

BEGIN TRANSACTION

	SET @GenTabName = @Job + '_PreIngestLog'
	EXECUTE CreatePreIngestLog @GenTable = @GenTabName

	SET @GenTabName = @Job + '_IngestLog'
	EXECUTE CreateIngestLog @GenTable = @GenTabName

	IF (@Level = 'CASE')
	BEGIN
		SET @GenTabName = @Job + '_CECases'
		EXECUTE CreateCECasesTable @GenTable = @GenTabName

		SET @GenTabName = @Job + '_CECaseProperties'
		EXECUTE CreateCECasePropertiesTable @GenTable = @GenTabName

		SET @GenTabName = @Job + '_CECaseFolders'
		EXECUTE CreateCECaseFoldersTable @GenTable = @GenTabName
	END

	SET @GenTabName = @Job + '_CEDocuments'
	EXECUTE CreateCEDocumentsTable @GenTable = @GenTabName, @CfgLevel = @Level

	SET @GenTabName = @Job + '_CEDocumentProperties'
	EXECUTE CreateCEDocumentPropertiesTable @GenTable = @GenTabName

	SET @GenTabName = @Job + '_CEDocumentPages'
	EXECUTE CreateCEDocumentPagesTable @GenTable = @GenTabName

	SET @GenTabName = @Job + '_CEAnnotations'
	EXECUTE CreateCEAnnotationsTable @GenTable = @GenTabName

	SET @GenTabName = @Job + '_CEAnnotationProperties'
	EXECUTE CreateCEAnnotationPropertiesTable @GenTable = @GenTabName

	SET @GenTabName = @Job + '_CEAnnotationHistory'
	EXECUTE CreateCEAnnotationHistoryTable @GenTable = @GenTabName

	IF (@Level = 'CASE')
	BEGIN
		SET @GenTabName = @Job + '_CETasks'
		EXECUTE CreateCETasksTable @GenTable = @GenTabName

		SET @GenTabName = @Job + '_CETaskProperties'
		EXECUTE CreateCETaskPropertiesTable @GenTable = @GenTabName

		SET @GenTabName = @Job + '_CaseData'
		EXECUTE CreateCaseDataTable @GenTable = @GenTabName, @Config = @ConfigNum
	END

	SET @GenTabName = @Job + '_DocumentData'
	EXECUTE CreateDocumentDataTable @GenTable = @GenTabName, @Config = @ConfigNum

	SET @GenTabName = @Job + '_AnnotationData'
	EXECUTE CreateAnnotationDataTable @GenTable = @GenTabName, @Config = @ConfigNum

	IF (@Level = 'CASE')
	BEGIN
		SET @GenTabName = @Job + '_TaskData'
		EXECUTE CreateTaskDataTable @GenTable = @GenTabName, @Config = @ConfigNum
	END

	SET @GenTabName = @Job + '_MarshalingInfo'
	EXECUTE CreateMarshalingInfoTable @GenTable = @GenTabName

	SET @Error = @@ERROR  
	IF @Error <> 0  
	BEGIN      
		GOTO LogError  
	END

COMMIT TRANSACTION
	SELECT @Success = 1
GOTO ProcEnd 

LogError:
ROLLBACK TRANSACTION

DECLARE @ErrMsg varchar(1000)
	
	SELECT @ErrMsg = [description] 
	FROM master.dbo.sysmessages  
	WHERE error = @Error
	
	INSERT INTO ErrorLog(Source, ErrMsg) VALUES ('spGenerateJobTables', @ErrMsg)  
	
ProcEnd:
END

GO

