/***************************************************************************
* File: SPDropJobTables.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the DropJobTables Stored Procedure 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/26/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[DropJobTables]')and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
DROP PROCEDURE [dbo].[DropJobTables]
GO

CREATE PROCEDURE DropJobTables
(
@Job varchar(100),
@Level varchar(12)
)
AS

BEGIN

DECLARE @dropSQL varchar(256)
DECLARE @Error int
DECLARE @Success int
DECLARE @TableName varchar(64)
DECLARE cur CURSOR LOCAL for
	SELECT name FROM dbo.sysobjects  WHERE name LIKE @Job + '%' AND xtype = 'U'

	SELECT @Success = 0

BEGIN TRANSACTION

	SET @dropSQL = 'DROP TABLE ' + @Job + '_PreIngestLog'
	EXEC(@dropSQL)

	SET @dropSQL = 'DROP TABLE ' + @Job + '_IngestLog'
	EXEC(@dropSQL)

	IF (@Level = 'CASE')
	BEGIN
		SET @dropSQL = 'DROP TABLE ' + @Job + '_CECases'
		EXEC(@dropSQL)

		SET @dropSQL = 'DROP TABLE ' + @Job + '_CECaseProperties'
		EXEC(@dropSQL)

		SET @dropSQL = 'DROP TABLE ' + @Job + '_CECaseFolders'
		EXEC(@dropSQL)
	END

	SET @dropSQL = 'DROP TABLE ' + @Job + '_CEDocuments'
	EXEC(@dropSQL)

	SET @dropSQL = 'DROP TABLE ' + @Job + '_CEDocumentProperties'
	EXEC(@dropSQL)

	SET @dropSQL = 'DROP TABLE ' + @Job + '_CEDocumentPages'
	EXEC(@dropSQL)

	SET @dropSQL = 'DROP TABLE ' + @Job + '_CEAnnotations'
	EXEC(@dropSQL)

	SET @dropSQL = 'DROP TABLE ' + @Job + '_CEAnnotationProperties'
	EXEC(@dropSQL)

	SET @dropSQL = 'DROP TABLE ' + @Job + '_CEAnnotationHistory'
	EXEC(@dropSQL)

	IF (@Level = 'CASE')
	BEGIN
		SET  @dropSQL = 'DROP TABLE ' + @Job + '_CETasks'
		EXEC(@dropSQL)

		SET  @dropSQL = 'DROP TABLE ' + @Job + '_CETaskProperties'
		EXEC(@dropSQL)

		SET  @dropSQL = 'DROP TABLE ' + @Job + '_TaskData'
		EXEC(@dropSQL)
	END

	SET @dropSQL = 'DROP TABLE ' + @Job + '_DocumentData'
	EXEC(@dropSQL)

	SET @dropSQL = 'DROP TABLE ' + @Job + '_AnnotationData'
	EXEC(@dropSQL)

	IF (@Level = 'CASE')
	BEGIN
		SET  @dropSQL = 'DROP TABLE ' + @Job + '_CaseData'
		EXEC(@dropSQL)
	END

	SET @dropSQL = 'DROP TABLE ' + @Job + '_MarshalingInfo'
	EXEC(@dropSQL)

	OPEN cur
	FETCH next FROM cur into @TableName

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET  @dropSQL = 'DROP TABLE ' + @TableName
		EXEC(@dropSQL)
		FETCH next FROM cur into @TableName
	END
	CLOSE cur
	DEALLOCATE cur

	SET @Error = @@ERROR  
	IF @Error <> 0  
	BEGIN      
		GOTO LogError  
	END

COMMIT TRANSACTION
	SELECT @Success = 1
GOTO ProcEnd 

LogError:
ROLLBACK TRANSACTION

DECLARE @ErrMsg varchar(1000)
	
	SELECT @ErrMsg = [description] 
	FROM master.dbo.sysmessages  
	WHERE error = @Error
	
	INSERT INTO ErrorLog(Source, ErrMsg) VALUES ('spDropJobTables', @ErrMsg)  
	
ProcEnd:
END

GO

