/***************************************************************************
* File: SPAdjustMMAccountID.sql
*
* Copyright: (c) 2014 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the AdjustMMAccountID Stored Procedure 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	01/15/2014	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[AdjustMMAccountID]')and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
DROP PROCEDURE [dbo].[AdjustMMAccountID]
GO

CREATE PROCEDURE AdjustMMAccountID
(
@TableName varchar(64),
@FieldLength int
)
AS

BEGIN

DECLARE @Error int
DECLARE @Success int
DECLARE @SQL varchar(4000)
DECLARE @FieldDesc varchar(64)

	SELECT @Success = 0

BEGIN TRANSACTION
	
	SET @FieldDesc = 'MMAccountID varchar' + '(' + CONVERT(varchar(16), @FieldLength) + ') NULL'
	SET @SQL = "ALTER TABLE [dbo].[" + @TableName + "] ALTER COLUMN " + @FieldDesc
	EXEC(@SQL)

	SET @Error = @@ERROR  
	IF @Error <> 0  
	BEGIN      
		GOTO LogError  
	END		

COMMIT TRANSACTION
	SELECT @Success = 1
GOTO ProcEnd 

LogError:
ROLLBACK TRANSACTION

DECLARE @ErrMsg varchar(1000)
	
	SELECT @ErrMsg = [description] 
	FROM master.dbo.sysmessages  
	WHERE error = @Error
	
	INSERT INTO ErrorLog(Source, ErrMsg) VALUES ('spAdjustMMAccountID', @ErrMsg)  
	
ProcEnd:
END

GO

