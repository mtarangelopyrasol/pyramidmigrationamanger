/***************************************************************************
* File: SPLogHistory.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the LogHistory Stored Procedure 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	11/02/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[LogHistory]')and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
DROP PROCEDURE [dbo].[LogHistory]
GO

CREATE PROCEDURE LogHistory
(
@Evt varchar(32),
@Desc varchar(4096)
)
AS

BEGIN

DECLARE @Error int
DECLARE @Success int

	SELECT @Success = 0

BEGIN TRANSACTION

	INSERT INTO MgrHistory (Event, Description) VALUES (@Evt, @Desc)

	SET @Error = @@ERROR  
	IF @Error <> 0  
	BEGIN      
		GOTO LogError  
	END

COMMIT TRANSACTION
	SELECT @Success = 1
GOTO ProcEnd 

LogError:
ROLLBACK TRANSACTION

DECLARE @ErrMsg varchar(1000)
	
	SELECT @ErrMsg = [description] 
	FROM master.dbo.sysmessages  
	WHERE error = @Error
	
	INSERT INTO ErrorLog(Source, ErrMsg) VALUES ('spLogHistory', @ErrMsg)  
	
ProcEnd:
END

GO

