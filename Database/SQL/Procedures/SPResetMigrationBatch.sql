/***************************************************************************
* File: SPResetMigrationBatch.sql
*
* Copyright: (c) 2014 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Reset Migration Batch Stored Procedure 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	5/16/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[ResetMigrationBatch]')and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
DROP PROCEDURE [dbo].[ResetMigrationBatch]
GO

CREATE PROCEDURE ResetMigrationBatch
(
@BatchID int,
@JobName varchar(64),
@CaseLevel int,
@ErrorsOnly int
)
AS

BEGIN

DECLARE @Error int
DECLARE @Success int
DECLARE @curCasSQL varchar(4000)
DECLARE @curDocSQL varchar(4000)
DECLARE @SQL varchar(4000)
DECLARE @CaseId varchar(64)
DECLARE @DocId varchar(64)
DECLARE @CaseTable varchar(64)
DECLARE @DocTable varchar(64)
DECLARE @AnnTable varchar(64)
DECLARE @TaskTable varchar(64)
		
	SELECT @Success = 0

BEGIN TRANSACTION
	
	IF(@CaseLevel = 1)
	BEGIN
		
		SELECT @CaseTable = @JobName + "_CECases"
		SELECT @DocTable = @JobName + "_CEDocuments"
		SELECT @AnnTable = @JobName + "_CEAnnotations"
		SELECT @TaskTable = @JobName + "_CETasks"

		SET @curCasSQL = "DECLARE curCase CURSOR for SELECT ID FROM " + @CaseTable + " WHERE MigrationID = " + CAST(@BatchID AS varchar(8))
		if(@ErrorsOnly = 1)
		BEGIN
			SET @curCasSQL = @curCasSQL + " AND Status IN ('ING_ERRORED', 'ING_CANCELED')"
		END
		EXEC(@curCasSQL)
		
		OPEN curCase
		FETCH next FROM curCase into @CaseId

		WHILE @@FETCH_STATUS = 0
		BEGIN	
			SET @SQL = "UPDATE [dbo].[" + @AnnTable + "] SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE ObjectID = '" + @CaseId + "' AND AnnClass != 'Annotation'"
			EXEC(@SQL)

			SET @Error = @@ERROR  
			IF @Error <> 0  
			BEGIN      
				GOTO LogError  
			END
			
			SET @SQL = "UPDATE [dbo].[" + @TaskTable + "] SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE CaseID = '" + @CaseId + "'"
			EXEC(@SQL)

			SET @Error = @@ERROR  
			IF @Error <> 0  
			BEGIN      
				GOTO LogError  
			END

			SET @curDocSQL = "DECLARE curDoc CURSOR for SELECT ID FROM " + @DocTable + " WHERE CaseID = '" + @CaseId + "'"
			EXEC(@curDocSQL)

			OPEN curDoc
			FETCH next FROM curDoc into @DocId

			WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @SQL = "UPDATE [dbo].[" + @AnnTable + "] SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE ObjectID = '" + @DocId + "' AND AnnClass = 'Annotation'"
				EXEC(@SQL)

				SET @Error = @@ERROR  
				IF @Error <> 0  
				BEGIN      
					GOTO LogError  
				END
				FETCH next FROM curDoc into @DocId
			END
			CLOSE curDoc
			DEALLOCATE curDoc

			SET @SQL = "UPDATE [dbo].[" + @DocTable + "] SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE CaseID = '" + @CaseId + "'"
			EXEC(@SQL)

			SET @Error = @@ERROR  
			IF @Error <> 0  
			BEGIN      
				GOTO LogError  
			END

			FETCH next FROM curCase into @CaseId
		END
		CLOSE curCase
		DEALLOCATE curCase
		
		SET @SQL = "UPDATE [dbo].[" + @CaseTable + "] SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE MigrationID = " + CAST(@BatchID AS varchar(8))
		if(@ErrorsOnly = 1)
		BEGIN
			SET @SQL = @SQL + " AND Status IN ('ING_ERRORED', 'ING_CANCELED')"
		END
		EXEC(@SQL)

		SET @Error = @@ERROR  
		IF @Error <> 0  
		BEGIN      
			GOTO LogError  
		END
	END
	ELSE
	BEGIN
		
		SELECT @DocTable = @JobName + "_CEDocuments"
		SELECT @AnnTable = @JobName + "_CEAnnotations"

		SET @curDocSQL = "DECLARE curDoc CURSOR for SELECT ID FROM " + @DocTable + " WHERE MigrationID = " + CAST(@BatchID AS varchar(8))
		if(@ErrorsOnly = 1)
		BEGIN
			SET @curDocSQL = @curDocSQL + " AND Status IN ('ING_ERRORED', 'ING_CANCELED')"
		END
		EXEC(@curDocSQL)
		
		OPEN curDoc
		FETCH next FROM curDoc into @DocId

		WHILE @@FETCH_STATUS = 0
		BEGIN	
		
			SET @SQL = "UPDATE [dbo].[" + @AnnTable + "] SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE ObjectID = '" + @DocId + "' AND AnnClass = 'Annotation'"
			EXEC(@SQL)

			SET @Error = @@ERROR  
			IF @Error <> 0  
			BEGIN      
				GOTO LogError  
			END
				
			FETCH next FROM curDoc into @DocId
		END
		CLOSE curDoc
		DEALLOCATE curDoc
		
		SET @SQL = "UPDATE [dbo].[" + @DocTable + "] SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE MigrationID = " + CAST(@BatchID AS varchar(8))
		if(@ErrorsOnly = 1)
		BEGIN
			SET @SQL = @SQL + " AND Status IN ('ING_ERRORED', 'ING_CANCELED')"
		END
		EXEC(@SQL)

		SET @Error = @@ERROR  
		IF @Error <> 0  
		BEGIN      
			GOTO LogError  
		END
	END
		
COMMIT TRANSACTION
	SELECT @Success = 1
GOTO ProcEnd 

LogError:
ROLLBACK TRANSACTION

DECLARE @ErrMsg varchar(1000)
	
	SELECT @ErrMsg = [description] 
	FROM master.dbo.sysmessages  
	WHERE error = @Error
	
	INSERT INTO ErrorLog(Source, ErrMsg) VALUES ('spResetMigrationBatch', @ErrMsg)  
	
ProcEnd:
END

GO
