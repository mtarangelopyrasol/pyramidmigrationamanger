/***************************************************************************
* File: SPGenerateDocProperties.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the GenerateDocProperties Stored Procedure 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	12/06/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

IF EXISTS
(
	SELECT * 
	FROM dbo.sysobjects 
	WHERE id = object_id(N'[dbo].[GenerateDocProperties]')and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
DROP PROCEDURE [dbo].[GenerateDocProperties]
GO

CREATE PROCEDURE GenerateDocProperties
(
@Config int,
@DocClassID int
)
AS

BEGIN

DECLARE @Error int
DECLARE @Success int

	SELECT @Success = 0

BEGIN TRANSACTION

	INSERT INTO CfgDocClassProperties (ConfigID, DocClassID, Name, Source, DataType, Required, Mapped, Computed, MappingName) VALUES 
		(@Config, @DocClassID, 'DocumentTitle', '', 'SingletonString', 1, 0, 0, '')
	
	SET @Error = @@ERROR  
	IF @Error <> 0  
	BEGIN      
		GOTO LogError  
	END

COMMIT TRANSACTION
	SELECT @Success = 1
GOTO ProcEnd 

LogError:
ROLLBACK TRANSACTION

DECLARE @ErrMsg varchar(1000)
	
	SELECT @ErrMsg = [description] 
	FROM master.dbo.sysmessages  
	WHERE error = @Error
	
	INSERT INTO ErrorLog(Source, ErrMsg) VALUES ('spGenerateDocProperties', @ErrMsg)  
	
ProcEnd:
END

GO

