/***************************************************************************
* File: CreateTableCfgCaseTypes.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Configuration Table: CaseTypes 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	07/20/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'CfgCaseTypes')
   DROP TABLE CfgCaseTypes
GO

CREATE TABLE [dbo].[CfgCaseTypes](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [ConfigID] [int] NOT NULL FOREIGN KEY REFERENCES MgrConfigurations(ID),
      [CaseName] [varchar](64) NOT NULL,
      [DefaultDocClass] [varchar](64) NULL,
      PRIMARY KEY (ID),
      CONSTRAINT UniqueCaseTypeName UNIQUE (ConfigID, CaseName)
) ON [PRIMARY]
GO
