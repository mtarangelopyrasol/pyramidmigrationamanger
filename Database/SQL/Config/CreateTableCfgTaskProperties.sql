/***************************************************************************
* File: CreateTableCfgTaskProperties.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Configuration Table: TaskProperties 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	09/07/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'CfgTaskProperties')
   DROP TABLE CfgTaskProperties
GO

CREATE TABLE [dbo].[CfgTaskProperties](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [ConfigID] [int] NOT NULL FOREIGN KEY REFERENCES MgrConfigurations(ID),
      [TaskTypeID] [int] NOT NULL FOREIGN KEY REFERENCES CfgTaskTypes(ID),
      [Name] [varchar](64) NOT NULL,
      [Source] [varchar] (64) NOT NULL,
      [DataType] [varchar](64) NOT NULL,
      [Required] [bit] NOT NULL DEFAULT 0,
      [Mapped] [bit] NOT NULL DEFAULT 0,
      [Computed] [bit] NOT NULL DEFAULT 0,
      [MappingName] [varchar](64) NULL,
      PRIMARY KEY (ID),
      CONSTRAINT UniqueTaskPropertyName UNIQUE (TaskTypeID, Name)
) ON [PRIMARY]
GO
