/***************************************************************************
* File: CreateTableCfgAnnotationSource.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Configuration Table: CfgAnnotationSource 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	07/20/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'CfgAnnotationSource')
   DROP TABLE CfgAnnotationSource
GO

CREATE TABLE [dbo].[CfgAnnotationSource](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [ConfigID] [int] NOT NULL FOREIGN KEY REFERENCES MgrConfigurations(ID),
      [ColumnID] [int] NOT NULL,
      [Name] [varchar](64) NOT NULL,
      [DataType][varchar](16) NOT NULL DEFAULT 'String',
      [MaxLength] [int] NULL,
      [Format][varchar](32) NULL,
      [MultiValued] [varchar] (1) NOT NULL DEFAULT 'N',
      PRIMARY KEY (ID),
      CONSTRAINT UniqueAnnSourceName UNIQUE (ConfigID, Name)
) ON [PRIMARY]
GO
