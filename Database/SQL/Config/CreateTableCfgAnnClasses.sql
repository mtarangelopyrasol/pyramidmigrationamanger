/***************************************************************************
* File: CreateTableCfgAnnClasses.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Configuration Table: CfgAnnClasses 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	10/31/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'CfgAnnClasses')
   DROP TABLE CfgAnnClasses
GO

CREATE TABLE [dbo].[CfgAnnClasses](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [ConfigID] [int] NOT NULL FOREIGN KEY REFERENCES MgrConfigurations(ID),
      [AnnClassName] [varchar](64) NOT NULL,
      PRIMARY KEY (ID),
      CONSTRAINT UniqueAnnClassTypeName UNIQUE (ConfigID, AnnClassName)
) ON [PRIMARY]
GO
