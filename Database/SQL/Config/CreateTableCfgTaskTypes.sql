/***************************************************************************
* File: CreateTableCfgTaskTypes.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Configuration Table: TaskTypes 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	09/07/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'CfgTaskTypes')
   DROP TABLE CfgTaskTypes
GO

CREATE TABLE [dbo].[CfgTaskTypes](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [CaseID] [int] NOT NULL FOREIGN KEY REFERENCES CfgCaseTypes(ID),
      [TaskName] [varchar](64) NOT NULL,
      [IsDocInitiated] [bit] NOT NULL DEFAULT 0,
      [InitiatingDocClass] [varchar](64) NULL,
      [InitiatingDocIdSource] [varchar](64) NULL,
      [AttachingProperty] [varchar](64) NULL,
      PRIMARY KEY (ID),
      CONSTRAINT UniqueTaskTypeName UNIQUE (CaseID, TaskName)
) ON [PRIMARY]
GO
