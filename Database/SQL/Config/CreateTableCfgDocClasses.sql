/***************************************************************************
* File: CreateTableCfgDocClasses.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Configuration Table: CfgDocClasses 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	07/20/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'CfgDocClasses')
   DROP TABLE CfgDocClasses
GO

CREATE TABLE [dbo].[CfgDocClasses](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [ConfigID] [int] NOT NULL FOREIGN KEY REFERENCES MgrConfigurations(ID),
      [DocClassName] [varchar](64) NOT NULL,
      PRIMARY KEY (ID),
      CONSTRAINT UniqueDocClassTypeName UNIQUE (ConfigID, DocClassName)
) ON [PRIMARY]
GO
