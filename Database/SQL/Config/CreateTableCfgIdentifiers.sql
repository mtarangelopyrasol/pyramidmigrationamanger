/***************************************************************************
* File: CreateTableCfgIdentifiers.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Configuration Table: CfgIdentifiers 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	07/20/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'CfgIdentifiers')
   DROP TABLE CfgIdentifiers
GO

CREATE TABLE [dbo].[CfgIdentifiers](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [ConfigID] [int] NOT NULL FOREIGN KEY REFERENCES MgrConfigurations(ID),
      [IdentifierName] [varchar](64) NOT NULL,
      [FieldName] [varchar](64) NULL,
      PRIMARY KEY (ID),
      CONSTRAINT UniqueIdentifierName UNIQUE (ConfigID, IdentifierName)
) ON [PRIMARY]
GO
