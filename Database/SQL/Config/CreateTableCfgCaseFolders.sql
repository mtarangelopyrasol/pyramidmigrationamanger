/***************************************************************************
* File: CreateTableCfgCaseFolders.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Configuration Table: CaseFolders 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	08/10/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'CfgCaseFolders')
   DROP TABLE CfgCaseFolders
GO

CREATE TABLE [dbo].[CfgCaseFolders](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [ConfigID] [int] NOT NULL FOREIGN KEY REFERENCES MgrConfigurations(ID),
      [CaseTypeID] [int] NOT NULL FOREIGN KEY REFERENCES CfgCaseTypes(ID),
      [FolderName] [varchar](64) NOT NULL,
      [Parent] [varchar](64) NOT NULL,
      PRIMARY KEY (ID)
) ON [PRIMARY]
GO
