/***************************************************************************
* File: CreateTableMgrSessions.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Housekeeping Table: MgrSessions 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	07/24/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'MgrSessions')
   DROP TABLE MgrSessions
GO

CREATE TABLE [dbo].[MgrSessions](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [SessionType] [varchar](16) NOT NULL DEFAULT 'CONVERSION',
      [StartTime] [datetime] NOT NULL DEFAULT GETDATE(),
      [EndTime] [datetime] NULL,
      [UserName][varchar](64) NOT NULL DEFAULT '',
      [WorkStation][varchar](64) NOT NULL DEFAULT '',
      [Status][varchar](16) NOT NULL DEFAULT 'PROCESSING',
      PRIMARY KEY (ID)
) ON [PRIMARY]
GO

