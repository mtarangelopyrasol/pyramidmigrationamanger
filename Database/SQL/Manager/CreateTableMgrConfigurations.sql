/***************************************************************************
* File: CreateTableMgrConfigurations.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Configuration Table: MgrConfiurations
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	07/24/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'MgrConfigurations')
   DROP TABLE MgrConfigurations
GO

CREATE TABLE [dbo].[MgrConfigurations](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [ConfigName][varchar](64) NOT NULL,
      [ConfigLevel][varchar](12) NOT NULL,
      [Comment][varchar](64) NULL,
      [LastUpdated][datetime] NOT NULL DEFAULT GETDATE(),
      PRIMARY KEY (ID),
      CONSTRAINT UniqueConfigName UNIQUE (ConfigName)
) ON [PRIMARY]
GO
