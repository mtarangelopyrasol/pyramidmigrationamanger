/***************************************************************************
* File: CreateTableMgrMappings.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Configuration Table: MgrMappings
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	07/20/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'MgrMappings')
   DROP TABLE MgrMappings
GO

CREATE TABLE [dbo].[MgrMappings](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [MappingName] [varchar](64) NOT NULL,
      PRIMARY KEY (ID),
      CONSTRAINT UniqueMappingName UNIQUE (MappingName)
) ON [PRIMARY]
GO
