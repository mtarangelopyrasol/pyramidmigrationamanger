/***************************************************************************
* File: CreateTableMgrMinBitsPerPixel.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Manager Table: MgrMinBitsPerPixel
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/24/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'MgrMinBitsPerPixel')
   DROP TABLE MgrMinBitsPerPixel
GO

CREATE TABLE [dbo].[MgrMinBitsPerPixel](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [LeadFormat][varchar](32) NOT NULL,
      [MinBpp][int] NOT NULL,
      PRIMARY KEY (ID)
) ON [PRIMARY]
GO
