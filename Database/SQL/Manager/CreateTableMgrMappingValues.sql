/***************************************************************************
* File: CreateTableMgrMappingValues.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Configuration Table: MgrMappingValues
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	07/20/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'MgrMappingValues')
   DROP TABLE MgrMappingValues
GO

CREATE TABLE [dbo].[MgrMappingValues](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [MapID] [int] FOREIGN KEY REFERENCES MgrMappings(ID),
      [OldValue] [varchar](64) NOT NULL,
      [NewValue] [varchar](64) NULL,
      PRIMARY KEY (ID)
) ON [PRIMARY]
GO

