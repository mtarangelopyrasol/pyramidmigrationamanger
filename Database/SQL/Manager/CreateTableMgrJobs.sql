/***************************************************************************
* File: CreateTableMgrJobs.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Configuration Table: MgrJobs
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	07/24/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'MgrJobs')
   DROP TABLE MgrJobs
GO

CREATE TABLE [dbo].[MgrJobs](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [ConfigID] [int] NOT NULL FOREIGN KEY REFERENCES MgrConfigurations(ID),
      [JobName][varchar](64) NOT NULL,
      [ConvDefnID][int] NOT NULL FOREIGN KEY REFERENCES MgrConvDefns(ID),
      [Comment][varchar](64) NULL,
      [LastUpdated][datetime] NOT NULL DEFAULT GETDATE(),
      PRIMARY KEY (ID),
      CONSTRAINT UniqueJobName UNIQUE (JobName)
) ON [PRIMARY]
GO
