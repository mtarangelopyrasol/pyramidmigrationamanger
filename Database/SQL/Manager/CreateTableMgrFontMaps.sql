/***************************************************************************
* File: CreateTableMgrFontMaps.sql
*
* Copyright: (c) 2014 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Manager Table: MgrFontMaps
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	02/05/2014	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'MgrFontMaps')
   DROP TABLE MgrFontMaps
GO

CREATE TABLE [dbo].[MgrFontMaps](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [FontName][varchar](64) NOT NULL,
      [InputSize][varchar](4) NOT NULL,
      [OutputSize][varchar](4) NOT NULL,
      [HeightFactor][float] NOT NULL,
      [WidthFactor][float] NOT NULL,
      PRIMARY KEY (ID)
) ON [PRIMARY]
GO
