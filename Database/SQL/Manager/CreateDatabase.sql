/***************************************************************************
* File: CreateDatabase.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the MIGRATIONDB Database 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	07/20/2012	Initial verson
*
***************************************************************************/

USE master

GO

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT name FROM sys.databases
      WHERE name = 'MIGRATIONDB')
   DROP DATABASE MIGRATIONDB
GO

CREATE DATABASE MIGRATIONDB

GO

