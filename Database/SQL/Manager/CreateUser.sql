/***************************************************************************
* File: CreateUser.sql
*
*
* Description:
*	Script provided By Auto Owners DBA for creating the database user to 
*	use with Migration Manager
*
* Author:
*	Dustin Prescott, Auto Owners DBA
*
* History:
*	Version	Author	Date		Description
*	1.0	DP	10/12/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB] 
GO 

CREATE USER [DOMNT1\SG-ECM Admins] FOR LOGIN [DOMNT1\SG-ECM Admins] 
GO 

USE [MIGRATIONDB] 
GO 

EXEC sp_addrolemember N'db_owner', N'DOMNT1\SG-ECM Admins' 
GO
