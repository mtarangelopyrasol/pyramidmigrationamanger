/***************************************************************************
* File: CreateTableMgrConversions.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Manager Table: MgrConversions
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	07/24/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'MgrConversions')
   DROP TABLE MgrConversions
GO

CREATE TABLE [dbo].[MgrConversions](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [ConvDefnID][int] NOT NULL FOREIGN KEY REFERENCES MgrConvDefns(ID),
      [ConvMapID] [int] NOT NULL FOREIGN KEY REFERENCES MgrConvMaps(ID),
      PRIMARY KEY (ID),
) ON [PRIMARY]
GO
