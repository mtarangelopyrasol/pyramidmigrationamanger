/***************************************************************************
* File: CreateTableMgrConvMaps.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Manager Table: MgrConvMap
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	07/24/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'MgrConvMaps')
   DROP TABLE MgrConvMaps
GO

CREATE TABLE [dbo].[MgrConvMaps](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [SourceFormat][varchar](16) NOT NULL,
      [TargetFormat][varchar](16) NOT NULL,
      [Exempt] [bit] NOT NULL DEFAULT 0,
      [Restricted] [bit] NOT NULL DEFAULT 0,
      PRIMARY KEY (ID)
) ON [PRIMARY]
GO
