/***************************************************************************
* File: CreateTableMgrMigrations.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Housekeeping Table: MgrMigrations 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	10/01/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'MgrMigrations')
   DROP TABLE MgrMigrations
GO

CREATE TABLE [dbo].[MgrMigrations](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [SessionID] [int] NOT NULL FOREIGN KEY REFERENCES MgrSessions(ID),
      [BatchID] [int] NOT NULL FOREIGN KEY REFERENCES MgrBatches(ID),
      [StartTime] [datetime] NOT NULL DEFAULT GETDATE(),
      [EndTime] [datetime] NULL,
      [UserName][varchar](64) NOT NULL DEFAULT '',
      [WorkStation][varchar](64) NOT NULL DEFAULT '',
      [Status][varchar](16) NOT NULL DEFAULT 'PROCESSING',
      [Comment][varchar](128) NOT NULL DEFAULT '',
      PRIMARY KEY (ID)
) ON [PRIMARY]
GO

