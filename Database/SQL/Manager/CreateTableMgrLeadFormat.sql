/***************************************************************************
* File: CreateTableMgrLeadFormat.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Manager Table: MgrLeadFormat
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/17/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'MgrLeadFormat')
   DROP TABLE MgrLeadFormat
GO

CREATE TABLE [dbo].[MgrLeadFormat](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [Format][varchar](32) NOT NULL,
      [Compression][varchar](32) NOT NULL,
      [LeadFormat][varchar](32) NOT NULL,
      PRIMARY KEY (ID)
) ON [PRIMARY]
GO
