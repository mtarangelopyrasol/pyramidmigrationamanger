/***************************************************************************
* File: CreateTableMgrHistory.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Housekeeping Table: MgrHistory 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	11/02/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'MgrHistory')
   DROP TABLE MgrHistory
GO

CREATE TABLE [dbo].[MgrHistory](
      [ID] [bigint] IDENTITY(1,1) NOT NULL,
      [Event][varchar](32) NOT NULL DEFAULT 'AUDIT',
      [TimeStamp][datetime] NOT NULL DEFAULT GETDATE(),
      [Description][varchar](4096) NULL,
      PRIMARY KEY (ID)
) ON [PRIMARY]
GO

