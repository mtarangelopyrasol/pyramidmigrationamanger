/***************************************************************************
* File: CreateTableMgrMimeTypeExt.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Manager Table: MgrMimeTypeExt
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	12/26/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'MgrMimeTypeExt')
   DROP TABLE MgrMimeTypeExt
GO

CREATE TABLE [dbo].[MgrMimeTypeExt](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [P8MimeType][varchar](128) NOT NULL,
      [Extension][varchar](64) NOT NULL,
      PRIMARY KEY (ID)
) ON [PRIMARY]
GO
