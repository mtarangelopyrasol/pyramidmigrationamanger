/***************************************************************************
* File: CreateTableMgrBatches.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Housekeeping Table: MgrBatches 
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	07/24/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'MgrBatches')
   DROP TABLE MgrBatches
GO

CREATE TABLE [dbo].[MgrBatches](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [SessionID] [int] NOT NULL FOREIGN KEY REFERENCES MgrSessions(ID),
      [JobID] [int] NOT NULL FOREIGN KEY REFERENCES MgrJobs(ID),
      [BatchDefnID] [int] NOT NULL FOREIGN KEY REFERENCES MgrBatchDefns(ID),
      [ActualSize] [int] NULL,
      [StartTime] [datetime] NOT NULL DEFAULT GETDATE(),
      [EndTime] [datetime] NULL,
      [UserName][varchar](64) NOT NULL DEFAULT '',
      [WorkStation][varchar](64) NOT NULL DEFAULT '',
      [Status][varchar](16) NOT NULL DEFAULT 'PROCESSING',
      [Comment][varchar](64) NOT NULL DEFAULT '',
      PRIMARY KEY (ID)
) ON [PRIMARY]
GO

