/***************************************************************************
* File: CreateTableMgrBatchDefns.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Configuration Table: MgrBatchDefns
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	07/24/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'MgrBatchDefns')
   DROP TABLE MgrBatchDefns
GO

CREATE TABLE [dbo].[MgrBatchDefns](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [BatchDefnName][varchar](64) NOT NULL,
      [BatchSize] [int] NOT NULL DEFAULT 0,
      [BatchCondition][varchar](512) NULL,
      [SourceContentPathPrefix][varchar](512) NULL,
      [StagingPath][varchar](512) NULL,
      PRIMARY KEY (ID),
      CONSTRAINT UniqueBatchDefnName UNIQUE (BatchDefnName)
) ON [PRIMARY]
GO
