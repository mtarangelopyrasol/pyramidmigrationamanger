/***************************************************************************
* File: CreateTableMgrFileExtension.sql
*
* Copyright: (c) 2013 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Manager Table: MgrFileExtension
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	06/24/2013	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'MgrFileExtension')
   DROP TABLE MgrFileExtension
GO

CREATE TABLE [dbo].[MgrFileExtension](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [Format][varchar](32) NOT NULL,
      [Compression][varchar](32) NOT NULL,
      [Extension][varchar](32) NOT NULL,
      PRIMARY KEY (ID)
) ON [PRIMARY]
GO
