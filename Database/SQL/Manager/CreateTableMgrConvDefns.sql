/***************************************************************************
* File: CreateTableMgrConvDefns.sql
*
* Copyright: (c) 2012 Pyramid Solutions Incorporated.
* All rights reserved.  Reproduction, adaptation, or 
* translation without written permission is prohibited, 
* except as allowed under the copyright laws.
*
* Description:
*	Script for creating the Manager Table: MgrConvDefns
*
* Author:
*	Usha Menon
*
* History:
*	Version	Author	Date		Description
*	1.0	UM	07/24/2012	Initial verson
*
***************************************************************************/

USE [MIGRATIONDB]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = 'MgrConvDefns')
   DROP TABLE MgrConvDefns
GO

CREATE TABLE [dbo].[MgrConvDefns](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [ConvDefnName][varchar](64) NOT NULL,
      [MarshalToSingleDoc] [bit] NOT NULL DEFAULT 0,
      PRIMARY KEY (ID),
      CONSTRAINT UniqueConvDefnName UNIQUE (ConvDefnName)
) ON [PRIMARY]
GO
