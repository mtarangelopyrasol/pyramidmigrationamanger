:: Run from the SQL Server server using a DB account with sufficient privileges
::
@echo off
cls
::
:: Set variables

SET INSTANCE=192.168.50.139
SET USER=p8dba
SET PASSWD=p@ssw0rd

::
:: Create Database
::

echo CreateDatabase:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateDatabase.sql

::
:: Migration Manager Tables
::

echo ErrorLog:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableErrorLog.sql

echo Configurations:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrConfigurations.sql

echo ConvDefinitions:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrConvDefns.sql

echo ConvMaps:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrConvMaps.sql
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\ManagerData\LoadTableMgrConvMaps.sql

echo Conversions:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrConversions.sql

echo Jobs:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrJobs.sql

echo BatchDefinitions:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrBatchDefns.sql

echo Sessions:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrSessions.sql

echo Batches:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrBatches.sql

echo Migrations:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrMigrations.sql

echo History:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrHistory.sql

echo MgrLeadFormat:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrLeadFormat.sql
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\ManagerData\LoadTableMgrLeadFormat.sql

echo MgrMimeTypeExt:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrMimeTypeExt.sql
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\ManagerData\LoadTableMgrMimeTypeExt.sql

echo MgrFileExtension:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrFileExtension.sql
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\ManagerData\LoadTableMgrFileExtension.sql

echo MgrMinBitsPerPixel:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrMinBitsPerPixel.sql
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\ManagerData\LoadTableMgrMinBitsPerPixel.sql

echo PromotionRules:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTablePromotionRules.sql
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\ManagerData\LoadTablePromotionRules.sql

echo FontMaps:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrFontMaps.sql
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\ManagerData\LoadTableMgrFontMaps.sql

echo MgrMappings:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrMappings.sql

echo MgrMappingValue:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrMappingValues.sql

::
:: Stored Procedures
::

echo Procedure CreatePreIngestLog:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreatePreIngestLog.sql

echo Procedure CreateIngestLog:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreateIngestLog.sql

echo Procedure CreateCECasesTable:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreateCECasesTable.sql

echo Procedure CreateCECasePropertiesTable:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreateCECasePropertiesTable.sql

echo Procedure CreateCECaseFoldersTable:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreateCECaseFoldersTable.sql

echo Procedure CreateCEDocumentsTable:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreateCEDocumentsTable.sql

echo Procedure CreateCEDocumentPropertiesTable:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreateCEDocumentPropertiesTable.sql

echo Procedure CreateCEPagesTable:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreateCEDocumentPagesTable.sql

echo Procedure CreateCEAnnotationsTable:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreateCEAnnotationsTable.sql

echo Procedure CreateCEAnnotationPropertiesTable:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreateCEAnnotationPropertiesTable.sql

echo Procedure CreateCEAnnotationHistoryTable:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreateCEAnnotationHistoryTable.sql

echo Procedure CreateCETasksTable:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreateCETasksTable.sql

echo Procedure CreateCETaskPropertiesTable:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreateCETaskPropertiesTable.sql

echo Procedure CreateCaseDataTable:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreateCaseDataTable.sql

echo Procedure CreateDocumentDataTable:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreateDocumentDataTable.sql

echo Procedure CreateAnnotationDataTable:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreateAnnotationDataTable.sql

echo Procedure CreateTaskDataTable:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreateTaskDataTable.sql

echo Procedure CreateMarshalingInfoTable:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPCreateMarshalingInfoTable.sql

echo Procedure GenerateIdentifiers:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPGenerateIdentifiers.sql

echo Procedure GenerateDocProperties:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPGenerateDocProperties.sql

echo Procedure GenerateAnnProperties:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPGenerateAnnProperties.sql

echo Procedure LogHistory:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPLogHistory.sql

echo Procedure AdjustColumnNumbers:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPAdjustColumnNumbers.sql

echo Procedure AddRestrictedConvMaps:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPAddRestrictedConvMaps.sql

echo Procedure GenerateJobTables:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPGenerateJobTables.sql

echo Procedure DropJobTables:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPDropJobTables.sql

echo Procedure AdjustMMAccountID:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPAdjustMMAccountID.sql

echo Procedure ResetMigrationBatch:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPResetMigrationBatch.sql
::
:: Triggers on Manager tables
::

echo Trigger OnInsertRowMgrConfigurations:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowMgrConfigurations.sql

echo Trigger OnInsertRowMgrJobs:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowMgrJobs.sql

echo Trigger OnInsertRowMgrBatchDefns:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowMgrBatchDefns.sql

echo Trigger OnUpdateRowMgrConfigurations:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowMgrConfigurations.sql

echo Trigger OnUpdateRowMgrJobs:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowMgrJobs.sql

echo Trigger OnUpdateRowMgrBatchDefns:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowMgrBatchDefns.sql

echo Trigger OnDeleteRowMgrConfigurations:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowMgrConfigurations.sql

echo Trigger OnDeleteRowMgrJobs:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowMgrJobs.sql

echo Trigger OnDeleteRowMgrBatchDefns:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowMgrBatchDefns.sql

echo Trigger OnInsertRowMgrConvDefns:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowMgrConvDefns.sql

echo Trigger OnUpdateRowMgrConvDefns:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowMgrConvDefns.sql

echo Trigger OnDeleteRowMgrConvDefns:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowMgrConvDefns.sql

echo Trigger OnInsertRowMgrConversions:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowMgrConversions.sql

echo Trigger OnUpdateRowMgrConversions:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowMgrConversions.sql

echo Trigger OnDeleteRowMgrConversions:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowMgrConversions.sql

echo Trigger OnInsertRowMgrMappings:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowMgrMappings.sql

echo Trigger OnUpdateRowMgrMappings:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowMgrMappings.sql

echo Trigger OnDeleteRowMgrMappings:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowMgrMappings.sql

echo Trigger OnInsertRowMgrMappingValues:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowMgrMappingValues.sql

echo Trigger OnUpdateRowMgrMappingValues:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowMgrMappingValues.sql

echo Trigger OnDeleteRowMgrMappingValues:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowMgrMappingValues.sql

::
:: Source Tables
::

echo CaseSource:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Config\CreateTableCfgCaseSource.sql

echo DocumentSource:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Config\CreateTableCfgDocumentSource.sql

echo AnnotationSource:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Config\CreateTableCfgAnnotationSource.sql

echo TaskSource:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Config\CreateTableCfgTaskSource.sql

::
:: Configuration Tables
::

echo CfgCaseTypes:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Config\CreateTableCfgCaseTypes.sql

echo CfgCaseTypeProperties:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Config\CreateTableCfgCaseProperties.sql

echo CfgCaseFolders:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Config\CreateTableCfgCaseFolders.sql

echo CfgDocClasses:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Config\CreateTableCfgDocClasses.sql

echo CfgDocClassProperties:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Config\CreateTableCfgDocClassProperties.sql

echo CfgAnnClasses:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Config\CreateTableCfgAnnClasses.sql

echo CfgAnnClassProperties:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Config\CreateTableCfgAnnClassProperties.sql

echo CfgTaskTypes:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Config\CreateTableCfgTaskTypes.sql

echo CfgTaskTypeProperties:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Config\CreateTableCfgTaskProperties.sql

::
:: Identifiers Table
::

echo Identifiers:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Config\CreateTableCfgIdentifiers.sql

::
:: Triggers on Configuration Tables
::

echo Trigger OnUpdateRowCfgIdentifiers:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowCfgIdentifiers.sql

echo Trigger OnDeleteRowCfgCaseSource:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowCfgCaseSource.sql

echo Trigger OnDeleteRowCfgDocumentSource:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowCfgDocumentSource.sql

echo Trigger OnDeleteRowCfgAnnotationSource:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowCfgAnnotationSource.sql

echo Trigger OnDeleteRowCfgTaskSource:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowCfgTaskSource.sql

echo Trigger OnInsertRowCfgCaseSource:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowCfgCaseSource.sql

echo Trigger OnInsertRowCfgDocumentSource:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowCfgDocumentSource.sql

echo Trigger OnInsertRowCfgAnnotationSource:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowCfgAnnotationSource.sql

echo Trigger OnInsertRowCfgTaskSource:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowCfgTaskSource.sql

echo Trigger OnUpdateRowCfgCaseSource:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowCfgCaseSource.sql

echo Trigger OnUpdateRowCfgCaseSource:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowCfgDocumentSource.sql

echo Trigger OnUpdateRowCfgCaseSource:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowCfgAnnotationSource.sql

echo Trigger OnUpdateRowCfgCaseSource:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowCfgTaskSource.sql

echo Trigger OnInsertRowCfgCaseFolders:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowCfgCaseFolders.sql

echo Trigger OnUpdateRowCfgCaseFolders:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowCfgCaseFolders.sql

echo Trigger OnDeleteRowCfgCaseFolders:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowCfgCaseFolders.sql

echo Trigger OnInsertRowCfgCaseProperties:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowCfgCaseProperties.sql

echo Trigger OnUpdateRowCfgCaseProperties:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowCfgCaseProperties.sql

echo Trigger OnDeleteRowCfgCaseProperties:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowCfgCaseProperties.sql

echo Trigger OnInsertRowCfgTaskProperties:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowCfgTaskProperties.sql

echo Trigger OnUpdateRowCfgTaskProperties:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowCfgTaskProperties.sql

echo Trigger OnDeleteRowCfgTaskProperties:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowCfgTaskProperties.sql

echo Trigger OnInsertRowCfgDocClassProperties:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowCfgDocClassProperties.sql

echo Trigger OnUpdateRowCfgDocClassProperties:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowCfgDocClassProperties.sql

echo Trigger OnDeleteRowCfgDocClassProperties:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowCfgDocClassProperties.sql

echo Trigger OnInsertRowCfgCaseTypes:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowCfgCaseTypes.sql

echo Trigger OnUpdateRowCfgCaseTypes:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowCfgCaseTypes.sql

echo Trigger OnDeleteRowCfgCaseTypes:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowCfgCaseTypes.sql

echo Trigger OnInsertRowCfgTaskTypes:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowCfgTaskTypes.sql

echo Trigger OnUpdateRowCfgTaskTypes:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowCfgTaskTypes.sql

echo Trigger OnDeleteRowCfgTaskTypes:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowCfgTaskTypes.sql

echo Trigger OnInsertRowCfgDocClasses:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowCfgDocClasses.sql

echo Trigger OnUpdateRowCfgDocClasses:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnUpdateRowCfgDocClasses.sql

echo Trigger OnDeleteRowCfgDocClasses:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnDeleteRowCfgDocClasses.sql

echo Trigger OnInsertRowCfgAnnClasses:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Triggers\OnInsertRowCfgAnnClasses.sql