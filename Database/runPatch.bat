:: Run from the SQL Server server using a DB account with sufficient privileges
::
@echo off
cls
::
:: Set variables

SET INSTANCE=192.168.50.138
SET USER=p8dba
SET PASSWD=p@ssw0rd


:: echo Procedure AdjustMMAccountID:
:: sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Procedures\SPAdjustMMAccountID.sql

echo FontMaps:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrFontMaps.sql
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\ManagerData\LoadTableMgrFontMaps.sql

:: echo MgrFileExtension:
:: sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateTableMgrFileExtension.sql
:: sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\ManagerData\LoadTableMgrFileExtension.sql

:: echo Patch:
:: sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Patch\Patch.sql
