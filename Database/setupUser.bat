:: Run from the SQL Server server using a DB account with sufficient privileges
::
@echo off
cls
::
:: Set variables

SET INSTANCE=192.168.100.128
SET USER=p8dba
SET PASSWD=p@ssw0rd

::
:: Create User
::

echo CreateUser:
sqlcmd -S %INSTANCE% -U %USER% -P %PASSWD% -i SQL\Manager\CreateUser.sql

