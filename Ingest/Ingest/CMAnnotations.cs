﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml;
using FileNet.Api.Core;

namespace Ingest
{
    class CMAnnotations : ClassBase
    {
        protected List<CMAnnotation> annCollection = new List<CMAnnotation>();
        int totalCount = 0;
        int errorCount = 0;
        int successCount = 0;

        public CMAnnotations(BackgroundWorker bw, Logger logr, string dbCnxn, string batch, string job, Boolean caseLevel, CommunicationInfo info) : base(bw, logr, dbCnxn, batch, job, caseLevel, info)
        {
            ItemsProcessed = false;
        }

        public Boolean ItemsProcessed { get; set; }

        public IngestionResult PerformIngestion(string convBatch, Boolean depthFirst)
        {
            IngestionResult result = IngestionResult.ING_NEW;

            if (!depthFirst)
            {
                if(isCaseLevelConfig) result = PerformCaseAnnotationIngestion(convBatch, depthFirst);
                result = PerformDocAnnotationIngestion(convBatch, depthFirst);
                if(isCaseLevelConfig) result = PerformTaskAnnotationIngestion(convBatch, depthFirst);
            }
            return result;
        }

        public IngestionResult PerformCaseAnnotationIngestion(string convBatch, Boolean depthFirst)
        {
            IngestionResult result = IngestionResult.ING_NEW;

            XmlDocument xml = DBAdapter.DBAdapter.GetProcessingRecs(dbConnection, jobName + "_CECases", "MigrationID", convBatch, "BatchID = " + convBatch, "Status = 'ING_DONE'", false);
            foreach (XmlElement row in xml.SelectNodes("//row"))
            {
                IFolder caseFolder = Factory.Folder.GetInstance(ceInfo.GetObjectStore(), null, row.GetAttribute("CaseFolderPath").Trim());
                result = IngestCaseAnnotations(convBatch, row.GetAttribute("ID").Trim(), caseFolder, depthFirst);
            }
            return result;
        }

        public IngestionResult PerformDocAnnotationIngestion(string convBatch, Boolean depthFirst)
        {
            IngestionResult result = IngestionResult.ING_NEW;

            XmlDocument xml = DBAdapter.DBAdapter.GetProcessingRecs(dbConnection, jobName + "_CEDocuments", "MigrationID", convBatch, "BatchID = " + convBatch, "Status = 'ING_DONE'", false);
            foreach (XmlElement row in xml.SelectNodes("//row"))
            {
                IDocument doc = Factory.Document.GetInstance(ceInfo.GetObjectStore(), null, row.GetAttribute("CE_GUID").Trim());
                result = IngestDocAnnotations(convBatch, row.GetAttribute("ID").Trim(), doc, depthFirst);
            }
            return result;
        }

        public IngestionResult PerformTaskAnnotationIngestion(string convBatch, Boolean depthFirst)
        {
            IngestionResult result = IngestionResult.ING_NEW;

            XmlDocument xml = DBAdapter.DBAdapter.GetProcessingRecs(dbConnection, jobName + "_CETasks", "MigrationID", convBatch, "BatchID = " + convBatch, "Status = 'ING_DONE'", false);
            foreach (XmlElement row in xml.SelectNodes("//row"))
            {
                string sql = "SELECT CE_GUID FROM " + jobName + "_CECases WHERE ID = " + row.GetAttribute("CaseID").Trim();
                XmlDocument caseXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
                string ceCaseGUID = caseXml.SelectNodes("//row")[0].Attributes["CE_GUID"].Value;
                IFolder caseFolder = Factory.Folder.GetInstance(ceInfo.GetObjectStore(), null, row.GetAttribute("CaseFolderPath").Trim());
                result = IngestTaskAnnotations(convBatch, row.GetAttribute("ID").Trim(), row.GetAttribute("CE_GUID").Trim(), ceCaseGUID, depthFirst);
            }
            return result;
        }

        public IngestionResult IngestDocAnnotations(string convBatch, string annotatedObjectMMID, IDocument doc, Boolean depthFirst)
        {
            IngestionResult result = IngestionResult.ING_NEW;

            while (result != IngestionResult.ING_COMPLETE)
            {
                if (backgroundWorker.CancellationPending)
                {
                    result = IngestionResult.ING_CANCELED;
                    break;
                }
                XmlDocument docsXML = GetAnnotations(convBatch, "DOCUMENT", annotatedObjectMMID, depthFirst);
                if (docsXML.SelectNodes("//row").Count == 0)
                {
                    result = IngestionResult.ING_COMPLETE;
                    break;
                }
                ItemsProcessed = true;
                foreach (XmlElement rowNode in docsXML.SelectNodes("//row"))
                {
                    if (backgroundWorker.CancellationPending)
                    {
                        result = IngestionResult.ING_CANCELED;
                        break;
                    }
                    try
                    {
                        CMAnnotation cma = new CMAnnotation(backgroundWorker, logger, dbConnection, batchID, jobName, isCaseLevelConfig, ceInfo);
                        cma.SetSource(rowNode);
                        result = cma.IngestDocAnnotations(convBatch, doc, depthFirst);
                    }
                    catch (Exception ex)
                    {
                        LogError("Exception processing annotation: " + ex.Message);
                        result = IngestionResult.ING_ERRORED;
                    }
                    totalCount++;
                    if ((result == IngestionResult.ING_DONE) || (result == IngestionResult.ING_COMPLETE)) successCount++;
                    else if (result == IngestionResult.ING_ERRORED) errorCount++;
                }
            }
            return result;
        }

        public IngestionResult IngestCaseAnnotations(string convBatch, string annotatedObjectMMID, IFolder folder, Boolean depthFirst)
        {
            IngestionResult result = (isCaseLevelConfig) ? IngestionResult.ING_NEW : IngestionResult.ING_COMPLETE;

            while (result != IngestionResult.ING_COMPLETE)
            {
                if (backgroundWorker.CancellationPending)
                {
                    result = IngestionResult.ING_CANCELED;
                    break;
                }
                XmlDocument caseXML = GetAnnotations(convBatch, "CASE", annotatedObjectMMID, depthFirst);
                if (caseXML.SelectNodes("//row").Count == 0)
                {
                    result = IngestionResult.ING_COMPLETE;
                    break;
                }
                ItemsProcessed = true;
                foreach (XmlElement rowNode in caseXML.SelectNodes("//row"))
                {
                    if (backgroundWorker.CancellationPending)
                    {
                        result = IngestionResult.ING_CANCELED;
                        break;
                    }
                    try
                    {
                        CMAnnotation cma = new CMAnnotation(backgroundWorker, logger, dbConnection, batchID, jobName, isCaseLevelConfig, ceInfo);
                        cma.SetSource(rowNode);
                        result = cma.IngestCaseAnnotations(convBatch, folder, depthFirst);
                    }
                    catch (Exception ex)
                    {
                        LogError("Exception processing annotation: " + ex.Message);
                        result = IngestionResult.ING_ERRORED;
                    }
                    totalCount++;
                    if ((result == IngestionResult.ING_DONE) || (result == IngestionResult.ING_COMPLETE)) successCount++;
                    else if (result == IngestionResult.ING_ERRORED) errorCount++;
                }
            }
            return result;
        }

        public IngestionResult IngestTaskAnnotations(string convBatch, string annotatedObjectMMID, string ceTaskGUID, string caseGUID, Boolean depthFirst)
        {
            IngestionResult result = (isCaseLevelConfig) ? IngestionResult.ING_NEW : IngestionResult.ING_COMPLETE;

            while (result != IngestionResult.ING_COMPLETE)
            {
                if (backgroundWorker.CancellationPending)
                {
                    result = IngestionResult.ING_CANCELED;
                    break;
                }
                XmlDocument tasksXML = GetAnnotations(convBatch, "TASK", annotatedObjectMMID, depthFirst);
                if (tasksXML.SelectNodes("//row").Count == 0)
                {
                    result = IngestionResult.ING_COMPLETE;
                    break;
                }
                ItemsProcessed = true;
                foreach (XmlElement rowNode in tasksXML.SelectNodes("//row"))
                {
                    if (backgroundWorker.CancellationPending)
                    {
                        result = IngestionResult.ING_CANCELED;
                        break;
                    }
                    try
                    {
                        CMAnnotation cma = new CMAnnotation(backgroundWorker, logger, dbConnection, batchID, jobName, isCaseLevelConfig, ceInfo);
                        cma.SetSource(rowNode);
                        result = cma.IngestTaskAnnotation(convBatch, ceTaskGUID, caseGUID, depthFirst);
                    }
                    catch (Exception ex)
                    {
                        LogError("Exception processing annotation: " + ex.Message);
                        result = IngestionResult.ING_ERRORED;
                    }
                    totalCount++;
                    if ((result == IngestionResult.ING_DONE) || (result == IngestionResult.ING_COMPLETE)) successCount++;
                    else if (result == IngestionResult.ING_ERRORED) errorCount++;
                }
            }
            return result;
        }

        public int GetTotalCount()
        {
            return totalCount;
        }

        public int GetSuccessCount()
        {
            return successCount;
        }

        public int GetErrorCount()
        {
            return errorCount;
        }

        protected XmlDocument GetAnnotations(string convBatch, string annType, string annotatedObjectMMID, Boolean depthFirst)
        {
            string condition = (depthFirst) ? "ObjectID = " + annotatedObjectMMID + "AND AnnType = '" + annType + "' AND BatchID = " + convBatch : 
                                                "AnnType = '" + annType + "' AND BatchID = " + convBatch;
            return DBAdapter.DBAdapter.GetProcessingRecs(dbConnection, jobName + "_CEAnnotations", "MigrationID", batchID, condition, "Status = 'ING_NEW'", !depthFirst);
        }   
    }
}
