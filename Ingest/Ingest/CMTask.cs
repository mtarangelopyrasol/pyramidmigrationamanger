﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Xml;

using FileNet.Api.Collection;
using FileNet.Api.Constants;
using FileNet.Api.Core;

namespace Ingest
{
    class CMTask : ClassBase
    {
        static Regex rgxGUID = new Regex("[{}]");
        const string repGUID = "";

        static Regex rgxSingleQuote = new Regex("'");
        const string repSingleQuote = "";

        static Regex rgxQuestMark = new Regex("\\?");
        const string repQuestMark = "";
        
        string mmCaseID = string.Empty;
        string mmID = string.Empty;
        string mmAccountID = string.Empty;
        string taskType = string.Empty;
        string taskDesc = string.Empty;
        string ceGUID = string.Empty;
        string rawCaseGUID = string.Empty;
        string strippedCaseGUID = string.Empty;
        Boolean isDocInitiated = false;
        CMAnnotations cmAnnotations = null;

        public CMTask(BackgroundWorker bw, Logger logr, string dbCnxn, string batch, string job, Boolean caseLevel, CommunicationInfo info) : base(bw, logr, dbCnxn, batch, job, caseLevel, info)
        {
        }

        public IngestionResult PerformIngestion(string convBatch, string caseID, string caseGUID, Boolean depthFirst)
        {
            IngestionResult result = IngestionResult.ING_NEW;

            mmCaseID = caseID;
            
            if (backgroundWorker.CancellationPending)
            {
                result = IngestionResult.ING_CANCELED;
            }
            try
            {
                LogMessage("Task: " + mmID + " (Case: " + mmAccountID + ")");

                rawCaseGUID = (depthFirst) ? caseGUID : PopulateCaseInformation();
                if (rawCaseGUID == string.Empty) throw new ArgumentException("Case not yet processed");
                else strippedCaseGUID = rgxGUID.Replace(rawCaseGUID, repGUID);   // get rid of the bounding curly braces of the GUID
                
                if (!isDocInitiated)
                {
                    string taskInfo = GetTaskTypeInfo();
                    string updatedInfo = UpdateProperties(taskInfo);
                    ceGUID = CreateTaskInstance(updatedInfo);

                    if (depthFirst)
                    {
                        result = AddAnnotations(convBatch, ceGUID, strippedCaseGUID, depthFirst);
                        if (result == IngestionResult.ING_COMPLETE)
                        {
                            if (cmAnnotations.ItemsProcessed)
                            {
                                LogMessage("Successfully Ingested Task Annotations: " + cmAnnotations.GetSuccessCount());
                                LogMessage("Errored Task Annotations: " + cmAnnotations.GetErrorCount());
                                LogMessage("Total Target Task Annotations: " + cmAnnotations.GetTotalCount());
                            }
                        }
                    }
                }
                result = IngestionResult.ING_DONE;
            }
            catch (Exception ex)
            {
                result = IngestionResult.ING_ERRORED;
                LogError("Task: " + mmID + " (Case: " + mmAccountID + ") " + ex.Message);
                entityStatus = EntityStatus.DISCARD;
            }
            finally
            {
                UpdateTaskStatus(result);
            }
            return result;
        }

        protected IngestionResult AddAnnotations(string convBatch, string ceGUID, string caseGUID, Boolean depthFirst)
        {
            cmAnnotations = new CMAnnotations(backgroundWorker, logger, dbConnection, batchID, jobName, isCaseLevelConfig, ceInfo);
            return cmAnnotations.IngestTaskAnnotations(convBatch, mmID, ceGUID, caseGUID, depthFirst);
        }

        public void SetSource(XmlElement xml)
        {
            mmID = xml.GetAttribute("ID");
            taskType = xml.GetAttribute("TaskName");
            taskDesc = xml.GetAttribute("TaskDesc");
            isDocInitiated = (xml.GetAttribute("IsDocInitiated") == "1") ? true : false;
            mmAccountID = xml.GetAttribute("MMAccountID");
        }

        protected void UpdateTaskStatus(IngestionResult migResult)
        {
            string sql = (ceGUID != string.Empty) ?
                    "UPDATE " + jobName + "_CETasks" + " SET Status = '" + migResult.ToString() + "', CE_GUID = '" + ceGUID + "' WHERE ID = " + mmID :
                    "UPDATE " + jobName + "_CETasks" + " SET Status = '" + migResult.ToString() + "' WHERE ID = " + mmID;

            DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
        }

        protected string PopulateCaseInformation()
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT c.CE_GUID FROM " + jobName + "_CECases c, " + jobName + "_CETasks t WHERE t.ID = " + mmID + " AND t.CaseID = c.ID");
            return Ingestor.GetAttributeValue(xml, "CE_GUID");
        }

        protected string GetTaskTypeInfo()
        {
            string result = string.Empty;

            try
            {
                ServicePointManager.ServerCertificateValidationCallback = Ingestor.RemoteCertValidate;

                string url = (ceInfo.useSSL) ? "https" : "http";

                url += "://" + ceInfo.CMServer + "/CaseManager/CASEREST/v1/case/" + strippedCaseGUID + "/tasktype/" + taskType + "?TargetObjectStore=" + ceInfo.CEOS;
                LogDebugMessage("GetTaskTypeInfo: " + url);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.KeepAlive = false;
                request.Method = "GET";
                request.ReadWriteTimeout = 10000;
                request.ContentType = "text/html";
                request.AllowAutoRedirect = false;
                CredentialCache wrCache = new CredentialCache();
                wrCache.Add(new Uri(url), "Basic", new NetworkCredential(ceInfo.CEUid, ceInfo.CEPwd));
                request.Credentials = wrCache;

                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();

                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader loResponseStream = new StreamReader(response.GetResponseStream(), enc);

                result = loResponseStream.ReadToEnd();

                loResponseStream.Close();
                response.Close();
            }
            catch (System.Exception ex)
            {
                throw new Exception("Exception getting task properties for task: " + taskType + " " + ex.Message);
            }
            return result;
        }

        protected string UpdateProperties(string properties)
        {
            string result = string.Empty;

            JavaScriptSerializer jss = new JavaScriptSerializer();

            Dictionary<string, Object> jsonResponse = jss.Deserialize<Dictionary<string, Object>>(properties);

            if (jsonResponse.ContainsKey("dataFields"))
            {
                Dictionary<string, Object> jsonDataFields = (Dictionary<string, Object>)jsonResponse["dataFields"];

                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM " + jobName + "_CETaskProperties WHERE TaskID = " + mmID);

                foreach (string k in jsonDataFields.Keys)
                {
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        string name = rowNode.GetAttribute("Name");
                        string value = rowNode.GetAttribute("Value");
                        string dataType = rowNode.GetAttribute("Type");

                        if (k == name)
                        {
                            Dictionary<string, Object> fldProperties = (Dictionary<string, Object>)jsonDataFields[k];

                            if (fldProperties.ContainsKey("value"))
                            {
                                switch (dataType)
                                {
                                    case "SingletonBoolean":
                                        fldProperties["value"] = (value.ToUpper() == "T") ? true : (value.ToUpper() == "F") ? false : Convert.ToBoolean(value);
                                        break;

                                    case "SingletonFloat64":
                                        fldProperties["value"] = Convert.ToDouble(value);
                                        break;

                                    case "SingletonInt32":
                                        fldProperties["value"] = Convert.ToInt32(value);
                                        break;

                                    case "SingletonDateTime":
                                        fldProperties["value"] = value;
                                        break;

                                    case "SingletonString":
                                        fldProperties["value"] = HandleSpecialCharacters(value);
                                        break;

                                    default:
                                        throw new ArgumentException("Property: " + name + " has unsupported data type " + dataType);
                                }
                            }
                            break;
                        }
                    }
                }
                JavaScriptSerializer jss2 = new JavaScriptSerializer();
                result = jss2.Serialize(jsonResponse);
            }
            return result;
        }

        protected string CreateTaskInstance(string properties)
        {
            string result = string.Empty;
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = Ingestor.RemoteCertValidate;

                string url = (ceInfo.useSSL) ? "https" : "http";

                url += "://" + ceInfo.CMServer + "/CaseManager/CASEREST/v1/case/" + strippedCaseGUID + "/tasks/?TargetObjectStore=" + ceInfo.CEOS;
                LogDebugMessage("CreateTaskInstance: " + url);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.KeepAlive = false;
                request.Method = "POST";
                request.ReadWriteTimeout = 10000;
                request.AllowAutoRedirect = false;
                
                CredentialCache wrCache = new CredentialCache();
                wrCache.Add(new Uri(url), "Basic", new NetworkCredential(ceInfo.CEUid, ceInfo.CEPwd));
                request.Credentials = wrCache;

                string content = "{\"TaskTypeName\":\"" + taskType + "\"," + "\"TaskName\":\"" + HandleSpecialCharacters(taskDesc) + "\"," + "\"StepElement\":" + properties + "}";
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] buf = encoding.GetBytes(content);
                request.ContentLength = buf.Length;
                request.ContentType = "application/json";
                Stream stream = request.GetRequestStream();
                stream.Write(buf, 0, buf.Length);

                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();

                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader loResponseStream = new StreamReader(response.GetResponseStream(), enc);

                result = loResponseStream.ReadToEnd();

                loResponseStream.Close();
                response.Close();

                Int32 index = result.LastIndexOf('{');
                result = result.Substring(index);
                index = result.IndexOf('}');
                result = result.Substring(0, index + 1);
            }
            catch (System.Exception ex)
            {
                throw new Exception("Exception creating instance of task: " + taskType + " " + ex.Message);
            }
            return result;
        }

        protected static string HandleSpecialCharacters(string input)
        {
            string temp1 = rgxQuestMark.Replace(input, repQuestMark);
            return rgxSingleQuote.Replace(temp1, repSingleQuote);
        }
    }
}
