﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml;

namespace Ingest
{
    class Batch : ClassBase
    {
        Boolean depthFirst = true;
        protected int caseCount = 0;
        protected int documentCount = 0;
        protected int annotationCount = 0;
        protected int taskCount = 0;

        public Batch(BackgroundWorker bw, Logger logr, string dbCnxn, string batch, string job, Boolean caseLevel, CommunicationInfo info) : base(bw, logr, dbCnxn, batch, job, caseLevel, info)
        {
        }

        public IngestionResult PerformIngestion(string convBatch, IngestionOptions options)
        {
            IngestionResult result = IngestionResult.ING_NEW;

            try
            {
                LogMessage("Batch ID: " + batchID.ToString() + " Starting Ingestion " + DateTime.Now.ToString());

                depthFirst = (options == IngestionOptions.DepthFirst);

                if ((result != IngestionResult.ING_CANCELED) && (result != IngestionResult.ING_ERRORED)) result = DoCases(convBatch, options, isCaseLevelConfig);
                if ((result != IngestionResult.ING_CANCELED) && (result != IngestionResult.ING_ERRORED)) result = DoDocuments(convBatch, options, isCaseLevelConfig);
                if ((result != IngestionResult.ING_CANCELED) && (result != IngestionResult.ING_ERRORED)) result = DoAnnotations(convBatch, options, isCaseLevelConfig);
                if ((result != IngestionResult.ING_CANCELED) && (result != IngestionResult.ING_ERRORED)) result = DoTasks(convBatch, options, isCaseLevelConfig);
                
                LogMessage("Batch ID: " + batchID.ToString() + " Complete " + DateTime.Now.ToString());
            }
            catch (Exception ex)
            {
                LogError("Batch ID: " + batchID.ToString() + " " + ex.Message);
                result = IngestionResult.ING_ERRORED;
            }
            finally
            {
                UpdateBatchStatus(result);
            }
            return result;
        }

        protected void UpdateBatchStatus(IngestionResult migResult)
        {
            string migComment = string.Empty;
            if (isCaseLevelConfig)
            {
                migComment = (depthFirst) ? "Processed Cases (with associated documents, annotations and tasks): " + caseCount :
                                "Processed Cases: " + caseCount + "; Documents: " + documentCount + "; Annotations: " + annotationCount + "; Tasks: " + taskCount;
            }
            else
            {
                migComment = (depthFirst) ? "Processed Documents (with associated annotations): " + documentCount :
                                "Processed Documents: " + documentCount + "; Annotations: " + annotationCount;
            }
            DBAdapter.DBAdapter.UpdateDB(dbConnection,
                "UPDATE MgrMigrations SET EndTime = GETDATE(), Status = '" + migResult.ToString() + "', Comment = '" + migComment + "' WHERE ID = " + batchID.ToString());
        }

        protected IngestionResult DoCases(string convBatch, IngestionOptions options, Boolean caselevelConfig)
        {
            IngestionResult result = IngestionResult.ING_DONE;

            if (caselevelConfig)
            {
                Boolean needToDo = ((options & IngestionOptions.Cases) == IngestionOptions.Cases) ? true : false;

                if (depthFirst || needToDo)
                {
                    CMCases cmCases = new CMCases(backgroundWorker, logger, dbConnection, batchID, jobName, isCaseLevelConfig, ceInfo);
                    result = cmCases.PerformIngestion(convBatch, depthFirst);
                    if (result != IngestionResult.ING_CANCELED)
                    {
                        if (cmCases.ItemsProcessed)
                        {
                            LogMessage("Successfully Ingested Cases: " + cmCases.GetSuccessCount());
                            LogMessage("Errored Cases: " + cmCases.GetErrorCount());
                            caseCount = cmCases.GetTotalCount();
                            LogMessage("Total Cases Processed: " + caseCount);
                        }
                    }
                }
            }
            return result;
        }

        protected IngestionResult DoDocuments(string convBatch, IngestionOptions options, Boolean caselevelConfig)
        {
            IngestionResult result = IngestionResult.ING_DONE;

            Boolean needToDo = ((options & IngestionOptions.Documents) == IngestionOptions.Documents) ? true : false;
            if (!caselevelConfig && (options == IngestionOptions.DepthFirst))
            {
                needToDo = true;
            }

            if (needToDo)
            {
                if (caselevelConfig)
                {
                    XmlDocument xml = DBAdapter.DBAdapter.GetProcessingRecs(dbConnection, jobName + "_CECases", "MigrationID", convBatch, "BatchID = " + convBatch, "Status = 'ING_DONE'", false);
                    foreach (XmlElement row in xml.SelectNodes("//row"))
                    {
                        IngestDocuments(convBatch, options, caselevelConfig, row.GetAttribute("ID").Trim(), row.GetAttribute("CaseFolderPath").Trim());
                    }
                }
                else
                {
                    IngestDocuments(convBatch, options, caselevelConfig, null, null);
                }
            }
            return result;
        }

        protected IngestionResult IngestDocuments(string convBatch, IngestionOptions options, Boolean caselevelConfig, string caseID, string ceCaseFolderPath)
        {
            CMDocuments cmDocs = new CMDocuments(backgroundWorker, logger, dbConnection, batchID, jobName, isCaseLevelConfig, ceInfo);
            IngestionResult result = cmDocs.PerformIngestion(convBatch, caseID, ceCaseFolderPath, depthFirst);
            if (result != IngestionResult.ING_CANCELED)
            {
                if (cmDocs.ItemsProcessed)
                {
                    LogMessage("Successfully Ingested Documents: " + cmDocs.GetSuccessCount());
                    LogMessage("Errored Documents: " + cmDocs.GetErrorCount());
                    documentCount = cmDocs.GetTotalCount();
                    LogMessage("Total Documents Processed: " + documentCount);
                }
            }
            return result;
        }

        protected IngestionResult DoAnnotations(string convBatch, IngestionOptions options, Boolean caselevelConfig)
        {
            IngestionResult result = IngestionResult.ING_DONE;

            Boolean needToDo = ((options & IngestionOptions.Annotations) == IngestionOptions.Annotations) ? true : false;
            if (!caselevelConfig && (options == IngestionOptions.DepthFirst))
            {
                needToDo = true;
            }

            if (needToDo)
            {
                CMAnnotations cmAnnotations = new CMAnnotations(backgroundWorker, logger, dbConnection, batchID, jobName, isCaseLevelConfig, ceInfo);
                result = cmAnnotations.PerformIngestion(convBatch, depthFirst);
                if (result != IngestionResult.ING_CANCELED)
                {
                    if (cmAnnotations.ItemsProcessed)
                    {
                        LogMessage("Successfully Ingested Annotations: " + cmAnnotations.GetSuccessCount());
                        LogMessage("Errored Annotations: " + cmAnnotations.GetErrorCount());
                        annotationCount = cmAnnotations.GetTotalCount();
                        LogMessage("Total Annotations Processed: " + annotationCount);
                    }
                }
                return result;
            }
            return result;
        }

        protected IngestionResult DoTasks(string convBatch, IngestionOptions options, Boolean caselevelConfig)
        {
            IngestionResult result = IngestionResult.ING_DONE;

            if (caselevelConfig)
            {
                Boolean needToDo = ((options & IngestionOptions.Tasks) == IngestionOptions.Tasks) ? true : false;

                if (needToDo)
                {
                    CMTasks cmTasks = new CMTasks(backgroundWorker, logger, dbConnection, batchID, jobName, isCaseLevelConfig, ceInfo);
                    result = cmTasks.PerformIngestion(convBatch, string.Empty, string.Empty, false);
                    if (result != IngestionResult.ING_CANCELED)
                    {
                        if (cmTasks.ItemsProcessed)
                        {
                            LogMessage("Successfully Ingested Tasks: " + cmTasks.GetSuccessCount());
                            LogMessage("Errored Tasks: " + cmTasks.GetErrorCount());
                            taskCount = cmTasks.GetTotalCount();
                            LogMessage("Total Tasks Processed: " + taskCount);
                        }
                    }
                }
            }
            return result;
        }
    }
}
