﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml;

namespace Ingest
{
    class CMTasks : ClassBase
    {
        string mmCaseID = string.Empty;
        protected List<CMTask> taskCollection = new List<CMTask>();
        int totalCount = 0;
        int successCount = 0;
        int errorCount = 0;
        
        public CMTasks(BackgroundWorker bw, Logger logr, string dbCnxn, string batch, string job, Boolean caseLevel, CommunicationInfo info) : base(bw, logr, dbCnxn, batch, job, caseLevel, info)
        {
            ItemsProcessed = false;
        }

        public Boolean ItemsProcessed { get; set;  }

        public IngestionResult PerformIngestion(string convBatch, string caseID, string caseGUID, Boolean depthFirst)
        {
            IngestionResult result = IngestionResult.ING_NEW;

            mmCaseID = caseID;

            while (result != IngestionResult.ING_COMPLETE)
            {
                if (backgroundWorker.CancellationPending)
                {
                    result = IngestionResult.ING_CANCELED;
                    break;
                }
                try
                {
                    XmlDocument taskXML = GetTasks(convBatch, depthFirst);
                    if (taskXML.SelectNodes("//row").Count == 0)
                    {
                        result = IngestionResult.ING_COMPLETE;
                        break;
                    }
                    ItemsProcessed = true;
                    foreach (XmlElement rowNode in taskXML.SelectNodes("//row"))
                    {
                        if (backgroundWorker.CancellationPending)
                        {
                            result = IngestionResult.ING_CANCELED;
                            break;
                        }
                        try
                        {
                            CMTask cmt = new CMTask(backgroundWorker, logger, dbConnection, batchID, jobName, isCaseLevelConfig, ceInfo);
                            cmt.SetSource(rowNode);
                            result = cmt.PerformIngestion(convBatch, mmCaseID, caseGUID, depthFirst);
                        }
                        catch (Exception ex)
                        {
                            LogError("Exception: " + ex.Message);
                            result = IngestionResult.ING_ERRORED;
                        }
                        totalCount++;
                        if ((result == IngestionResult.ING_DONE) || (result == IngestionResult.ING_COMPLETE)) successCount++;
                        else if (result == IngestionResult.ING_ERRORED) errorCount++;
                    }
                }
                catch (Exception ex)
                {
                    LogError("Exception: " + ex.Message);
                    result = IngestionResult.ING_ERRORED;
                    errorCount++;
                }
            }
            return result;
        }

        public int GetTotalCount()
        {
            return totalCount;
        }

        public int GetSuccessCount()
        {
            return successCount;
        }

        public int GetErrorCount()
        {
            return errorCount;
        }

        protected XmlDocument GetTasks(string convBatch, Boolean depthFirst)
        {
            string condition = (depthFirst) ? "CaseID = " + mmCaseID + " AND BatchID = " + convBatch : "BatchID = " + convBatch;
            return DBAdapter.DBAdapter.GetProcessingRecs(dbConnection, jobName + "_CETasks", "MigrationID", batchID, condition, "Status = 'ING_NEW'", false);
        } 
    }
}
