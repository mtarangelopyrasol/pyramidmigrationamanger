﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml;
using System.IO;
using FileNet.Api.Constants;
using FileNet.Api.Core;
using FileNet.Api.Util;
using FileNet.Api.Collection;
using FileNet.Api.Meta;
using FileNet.Api.Admin;
using FileNet.Api.Property;

namespace Ingest
{
    class CMAnnotation : ClassBase
    {
        string mmID = string.Empty;
        string annClass = string.Empty;
        string mmAnnotatedObjectID = string.Empty;
        string fileName = string.Empty;

        string ceGUID = string.Empty;

        public CMAnnotation(BackgroundWorker bw, Logger logr, string dbCnxn, string batch, string job, Boolean caseLevel, CommunicationInfo info) : base(bw, logr, dbCnxn, batch, job, caseLevel, info)
        {
        }

        public IngestionResult IngestDocAnnotations(string convBatch, IDocument parentDoc, Boolean depthFirst)
        {
            IngestionResult result = IngestionResult.ING_NEW;

            try
            {
                LogDebugMessage("Document Annotation: " + mmID);

                IDocument doc = GetAnnotatedDocument(parentDoc, depthFirst);
                if (doc != null)
                {
                    LogDebugMessage("Got annotated document");
                    // Get element sequence number of 1st content element of the document.
                    IContentElementList docContentList = doc.ContentElements;
                    int elementSequenceNumber = (int)((IContentElement)docContentList[0]).ElementSequenceNumber;

                    LogDebugMessage("ElementSequenceNumber: " + elementSequenceNumber.ToString());

                    IAnnotation ann = Factory.Annotation.CreateInstance(ceInfo.GetObjectStore(), annClass);
                    ann.AnnotatedObject = doc;

                    // Specify the document's ContentElement to which the annotation applies.
                    // The ContentElement is identified by its element sequence number.
                    ann.AnnotatedContentElement = elementSequenceNumber;
                    ann.Save(RefreshMode.REFRESH);
                    ceGUID = ann.Id.ToString();
                    LogDebugMessage("Annotation saved. GUID: " + ceGUID);
                    if (annClass == "Annotation")
                    {
                        LogDebugMessage("Calling Handle Traditional Annotation");
                        HandleTraditionalAnnotation(ann, elementSequenceNumber);
                        LogDebugMessage("Completed Handling Traditional Annotation");
                    }
                    else
                    {
                        ann.ChangeClass(annClass);
                        LogDebugMessage("Changed Class to: " + annClass);
                        PopulateAnnotationProperties(ann);
                        LogDebugMessage("Populated properties");
                        ann.Save(RefreshMode.REFRESH);
                        LogDebugMessage("Annotation finally saved");
                    } 
                }
                result = IngestionResult.ING_DONE;
            }
            catch (Exception ex)
            {
                result = IngestionResult.ING_ERRORED;
                LogError("Document Annotation: " + mmID + " " + ex.Message);
                LogExceptionDetails(ex);
                entityStatus = EntityStatus.DISCARD;
            }
            finally
            {
                UpdateAnnotationStatus(result);
                LogDebugMessage("Annotation Status updated to: " + result);
            }
            return result;
        }

        public IngestionResult IngestCaseAnnotations(string convBatch, IFolder parentCase, Boolean depthFirst)
        {
            IngestionResult result = IngestionResult.ING_NEW;

            try
            {
                LogDebugMessage("Case Annotation: " + mmID);

                IFolder caseObject = GetAnnotatedCase(parentCase, depthFirst);
                if (caseObject != null)
                {
                    LogDebugMessage("Annotated Case obtained");
                    IAnnotation ann = Factory.Annotation.CreateInstance(ceInfo.GetObjectStore(), annClass);
                    ann.AnnotatedObject = caseObject;
                    LogDebugMessage("Annotation created");

                    ann.Save(RefreshMode.REFRESH);
                    ceGUID = ann.Id.ToString();
                    LogDebugMessage("Annotation saved. GUID: " + ceGUID);
                    ann.ChangeClass(annClass);
                    LogDebugMessage("Changed Class to: " + annClass);
                    PopulateAnnotationProperties(ann);
                    LogDebugMessage("Populated properties");
                }
                result = IngestionResult.ING_DONE;
            }
            catch (Exception ex)
            {
                result = IngestionResult.ING_ERRORED;
                LogError("Case Annotation: " + mmID + " " + ex.Message);
                LogExceptionDetails(ex);
                entityStatus = EntityStatus.DISCARD;
            }
            finally
            {
                UpdateAnnotationStatus(result);
                LogDebugMessage("Annotation Status updated to: " + result);
            }
            return result;
        }

        public IngestionResult IngestTaskAnnotation(string convBatch, string ceTaskGUID, string caseGUID, Boolean depthFirst)
        {
            IngestionResult result = IngestionResult.ING_NEW;

            try
            {
                LogDebugMessage("Task Annotation: " + mmID);

                IFolder caseFolder = GetAnnotatedCase(caseGUID);
                if (caseFolder != null)
                {
                    IAnnotation ann = Factory.Annotation.CreateInstance(ceInfo.GetObjectStore(), annClass);
                    ann.AnnotatedObject = caseFolder;

                    ann.Save(RefreshMode.REFRESH);
                    ceGUID = ann.Id.ToString();
                    LogDebugMessage("Annotation saved. GUID: " + ceGUID);
                    ann.ChangeClass(annClass);
                    LogDebugMessage("Changed Class to: " + annClass);
                    try
                    {
                        Ingest.Ingestor.PopulateProperty(ann, "CmAcmCommentedTask", ceTaskGUID, "GUID");
                        LogDebugMessage("CmAcmCommentedTask property updated");
                    }
                    catch(Exception ex)
                    {
                        LogError("Annotation " + mmID + ": Exception while populating property CmAcmCommentedTask (data type: GUID) " + ex.Message);
                    }
                    PopulateAnnotationProperties(ann);
                    LogDebugMessage("Populated properties");
                }
                result = IngestionResult.ING_DONE;
            }
            catch (Exception ex)
            {
                result = IngestionResult.ING_ERRORED;
                LogError("Task Annotation: " + mmID + " " + ex.Message);
                LogExceptionDetails(ex);
                entityStatus = EntityStatus.DISCARD;
            }
            finally
            {
                UpdateAnnotationStatus(result);
                LogDebugMessage("Annotation Status updated to: " + result);
            }
            return result;
        }

        public void SetSource(XmlElement xml)
        {
            mmID = xml.GetAttribute("ID");
            mmAnnotatedObjectID = xml.GetAttribute("DocumentID");
            fileName = xml.GetAttribute("FilePath");
            annClass = xml.GetAttribute("AnnClass");
        }

        protected void UpdateAnnotationStatus(IngestionResult migResult)
        {
            string sql = (ceGUID != string.Empty) ?
                    "UPDATE " + jobName + "_CEAnnotations" + " SET Status = '" + migResult.ToString() + "', CE_GUID = '" + ceGUID + "' WHERE ID = " + mmID :
                    "UPDATE " + jobName + "_CEAnnotations" + " SET Status = '" + migResult.ToString() + "' WHERE ID = " + mmID;

            DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
        }

        protected MemoryStream FinalizeProperties(string fileName, int seqNumber)
        {
            LogDebugMessage("Finalizing Properties");

            MemoryStream outStream = new MemoryStream();
            LogDebugMessage("Created new memory stream");

            using (Stream fileStream = File.OpenRead(fileName))
            {
                LogDebugMessage("Annotation File opened: " + fileName);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(fileStream);
                LogDebugMessage("Loaded to Xml");

                XmlAttribute xa = xmlDoc.SelectNodes("//PropDesc")[0].Attributes["F_ANNOTATEDID"];
                xa.Value = ceGUID;
                LogDebugMessage("F_ANNOTATEDID attribute updated");

                xa = xmlDoc.SelectNodes("//PropDesc")[0].Attributes["F_ID"];
                xa.Value = ceGUID;
                LogDebugMessage("F_ID attribute updated");

                //xa = xmlDoc.SelectNodes("//PropDesc")[0].Attributes["F_CLASSID"];
                //xa.Value = "GUID of new class";

                outStream.Seek(0, 0);
                xmlDoc.Save(outStream);
                LogDebugMessage("Xml saved to memory stream");
            }
            return outStream;
        }

        protected IFolder GetAnnotatedCase(string caseGUID)
        {
            if (caseGUID == string.Empty) throw new ArgumentException("Case not yet processed");

            // Create property filter for document's content elements, which are needed to get element sequence numbers that identify elements.
            PropertyFilter pf = new PropertyFilter();
            pf.AddIncludeProperty(new FilterElement(null, null, null, PropertyNames.CONTENT_ELEMENTS, null));

            // Fetch Folder object.
            IFolder caseFolder = Factory.Folder.FetchInstance(ceInfo.GetObjectStore(), caseGUID, pf);
            return caseFolder;
        }

        protected IFolder GetAnnotatedCase(IFolder parentCase, Boolean depthFirst)
        {
            IFolder caseFolder = null;

            if (depthFirst && (parentCase != null))
            {
                caseFolder = parentCase;
            }
            else
            {
                return GetAnnotatedCase(GetParentGUID("CECases"));
            }
            return caseFolder;
        }

        protected IDocument GetAnnotatedDocument(IDocument parentDoc, Boolean depthFirst)
        {
            IDocument doc = null;

            if (depthFirst && (parentDoc != null))
            {
                doc = parentDoc;
            }
            else
            {
                string docGUID = GetParentGUID("CEDocuments");
                if (docGUID == string.Empty) throw new ArgumentException("Document not yet processed");

                // Create property filter for document's content elements, which are needed to get element sequence numbers that identify elements.
                PropertyFilter pf = new PropertyFilter();
                pf.AddIncludeProperty(new FilterElement(null, null, null, PropertyNames.CONTENT_ELEMENTS, null));

                // Fetch Document object.
                doc = Factory.Document.FetchInstance(ceInfo.GetObjectStore(), docGUID, pf);
            }
            return doc;
        }

        protected string GetParentGUID(string tableSuffix)
        {
            string parentGUID = string.Empty;

            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT CE_GUID AS ID FROM " + jobName + "_" + tableSuffix + " WHERE ID = " + mmAnnotatedObjectID);

            if (xmlDoc.SelectNodes("//row").Count > 0)
            {
                XmlAttribute xa = xmlDoc.SelectNodes("//row")[0].Attributes["ID"];
                if (xa != null)
                {
                    parentGUID = xmlDoc.SelectNodes("//row")[0].Attributes["ID"].Value;
                }
            }
            return parentGUID;
        }

        protected void PopulateAnnotationProperties(IAnnotation ann)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM " + jobName + "_CEAnnotationProperties WHERE AnnotationID = " + mmID);

            foreach (XmlElement rowNode in xml.SelectNodes("//row"))
            {
                string dataType = rowNode.GetAttribute("Type");
                string value = rowNode.GetAttribute("Value");
                string name = rowNode.GetAttribute("Name");

                try
                {
                    Ingest.Ingestor.PopulateProperty(ann, name, value, dataType);
                }
                catch (Exception ex)
                {
                    LogError("Annotation " + mmID + ": Exception while populating property " + name + "(data type: " + dataType + ") with value " + value + " " + ex.Message);
                }
            }
        }

        protected void HandleTraditionalAnnotation(IAnnotation ann, int elementSequenceNumber)
        {
            LogDebugMessage("Handling Traditional Annotation");
            using (MemoryStream sx = FinalizeProperties(fileName, elementSequenceNumber))
            {
                LogDebugMessage("Properties finalized");
                using (FileStream fsx = File.OpenWrite(fileName))
                {
                    LogDebugMessage("File opened for writing: " + fileName);
                    fsx.Seek(0, 0);
                    sx.WriteTo(fsx);
                    fsx.Flush();
                    LogDebugMessage("File written to and flushed");
                }
            }

            using (Stream fileStream = File.OpenRead(fileName))
            {
                LogDebugMessage("File opened for reading: " + fileName);
                // Create ContentTransfer and ContentElementList objects for the annotation.
                IContentTransfer ctObject = Factory.ContentTransfer.CreateInstance();
                IContentElementList annContentList = Factory.ContentElement.CreateList();
                ctObject.SetCaptureSource(fileStream);
                ctObject.ContentType = "text/xml";
                LogDebugMessage("Content Transfer Object setup");

                // Add ContentTransfer object to list and set the list on the annotation.
                annContentList.Add(ctObject);
                ann.ContentElements = annContentList;
                LogDebugMessage("Content Transfer Object added to Content Element list");
                ann.Save(RefreshMode.REFRESH);
                LogDebugMessage("Annotation saved");
            }
            LogDebugMessage("Returning from Handle Traditional Annotation");
        }
    }
}