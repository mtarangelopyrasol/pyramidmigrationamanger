﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml;
using DBAdapter;

namespace Ingest
{
    class ClassBase
    {
        protected EntityStatus entityStatus = EntityStatus.GOOD;
		protected string dbConnection = null;
        protected Logger logger = null;
        protected BackgroundWorker backgroundWorker = null;
		protected string batchID = string.Empty;
        protected string jobName = string.Empty;
        protected Boolean isCaseLevelConfig = false;
        protected CommunicationInfo ceInfo = null;

        protected ClassBase(BackgroundWorker bw, Logger logr, string dbCnxn, string batch, string job, Boolean caseLevel, CommunicationInfo info)
        {
            backgroundWorker = bw;
			logger = logr;
			dbConnection = dbCnxn;
			batchID = batch;
            jobName = job;
            isCaseLevelConfig = caseLevel;
            ceInfo = info;
        }

        protected void LogMessage(String message)
        {
            logger.LogMessage(batchID, message);
            backgroundWorker.ReportProgress(0, message);
        }

        protected void LogError(String message)
        {
            logger.LogError(batchID, message);
            backgroundWorker.ReportProgress(0, message);
        }

        protected void LogDebugMessage(String message)
        {
            logger.LogDebugMessage(batchID, message);
        }

        protected void LogExceptionDetails(Exception ex)
        {
            LogDebugMessage("Exception details:");
            LogDebugMessage("Message: " + ex.Message);
            LogDebugMessage("Source: " + ex.Source);
            LogDebugMessage("StackTrace: " + ex.StackTrace);
            LogDebugMessage("TargetSite: " + ex.TargetSite);
            LogDebugMessage("Inner Exception Message: " + ex.InnerException.Message);
        }
    }
}
