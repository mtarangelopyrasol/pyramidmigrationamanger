﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

using FileNet.Api;
using FileNet.Api.Authentication;
using FileNet.Api.Core;
using FileNet.Api.Util;

namespace Ingest
{
    class CommunicationInfo
    {
        public string CEUid = string.Empty;
        public string CEPwd = string.Empty;
        public string CEUrl = string.Empty;
        public string CEOS = string.Empty;
        public string solution = string.Empty;
        public string CMServer = string.Empty;
        public Boolean useSSL = false;
        FileNet.Api.Core.IObjectStore targetOS = null;
        
        public CommunicationInfo(string uid, string pwd, string url, string objectStore, string solnName, string cmServer, Boolean sslOption)
        {
            CEUid = uid;
            CEPwd = pwd;
            CEUrl = url;
            CEOS = objectStore;
            solution = solnName;
            CMServer = cmServer;
            useSSL = sslOption;
        }

        protected FileNet.Api.Core.IDomain GetDomain()
        {
            // Get the connection and default domain.
            IConnection conn = Factory.Connection.GetConnection(CEUrl);
            IDomain domain = Factory.Domain.GetInstance(conn, null);
            return domain;
        }

        protected void DetermineObjectStore()
        {
            if (targetOS == null)
            {
                ServicePointManager.ServerCertificateValidationCallback = Ingestor.RemoteCertValidate;

                UsernameCredentials creds = new UsernameCredentials(CEUid, CEPwd);
                ClientContext.SetProcessCredentials(creds);

                // Get the connection and default domain.
                IConnection conn = Factory.Connection.GetConnection(CEUrl);
                IDomain domain = Factory.Domain.GetInstance(conn, null);

                // Get the object store.
                targetOS = Factory.ObjectStore.FetchInstance(domain, CEOS, null);
            }
        }

        public FileNet.Api.Core.IObjectStore GetObjectStore()
        {
            if (targetOS == null) DetermineObjectStore();
            return targetOS;
        }
    }
}
