﻿using System;
using System.ComponentModel;
using System.Xml;
using FileNet.Api.Constants;
using FileNet.Api.Core;
using FileNet.Api.Query;
using FileNet.Api.Collection;

namespace Ingest
{
    class CMCase : ClassBase
    {
        XmlDocument sourceXML;
        string mmID = string.Empty;
        string caseType = string.Empty;
        string ceGUID = string.Empty;
        string ceFolderPath = string.Empty;
        string mmAccountID = string.Empty;
        CMAnnotations cmAnnotations = null;

        CMDocuments cmDocs;
        CMTasks cmTasks;

        public CMCase(BackgroundWorker bw, Logger logr, string dbCnxn, string batch, string job, Boolean caseLevel, CommunicationInfo info) : base(bw, logr, dbCnxn, batch, job, caseLevel, info)
        {
        }

        public IngestionResult PerformIngestion(string convBatch, Boolean depthFirst)
        {
            IngestionResult result = IngestionResult.ING_NEW;

            try
            {
                LogMessage("Case: " + mmID + " (" + mmAccountID + ")");

                IFolder workingFolder = getCase();

                PopulateCaseProperties(workingFolder);
                workingFolder.Save(RefreshMode.REFRESH);
                AddFolders(workingFolder, workingFolder, "Root");
                ceGUID = workingFolder.Id.ToString();
                ceFolderPath = workingFolder.PathName;
                result = IngestionResult.ING_DONE;
                if (depthFirst)
                {
                    result = AddAnnotations(convBatch, workingFolder, depthFirst);
                    if (result == IngestionResult.ING_COMPLETE)
                    {
                        if (cmAnnotations.ItemsProcessed)
                        {
                            LogMessage("Successfully Ingested Case Annotations: " + cmAnnotations.GetSuccessCount());
                            LogMessage("Errored Case Annotations: " + cmAnnotations.GetErrorCount());
                            LogMessage("Total Target Case Annotations: " + cmAnnotations.GetTotalCount());
                        }
                    }
                    result = AddDocuments(convBatch, depthFirst);
                    if (result == IngestionResult.ING_COMPLETE)
                    {
                        if (cmDocs.ItemsProcessed)
                        {
                            LogMessage("Successfully Ingested Documents: " + cmDocs.GetSuccessCount());
                            LogMessage("Errored Documents: " + cmDocs.GetErrorCount());
                            LogMessage("Total Target Documents Ingested: " + cmDocs.GetTotalCount());
                        }
                        result = AddTasks(convBatch, depthFirst);
                        if (result == IngestionResult.ING_COMPLETE)
                        {
                            if (cmTasks.ItemsProcessed)
                            {
                                LogMessage("Successfully Ingested Tasks: " + cmTasks.GetSuccessCount());
                                LogMessage("Errored Tasks: " + cmTasks.GetErrorCount());
                                LogMessage("Total Tasks: " + cmTasks.GetTotalCount());
                            }
                        }
                        LogMessage("Case: " + mmID + " Complete");
                        result = (((cmAnnotations != null) && (cmAnnotations.GetErrorCount() > 0)) ||
                                  ((cmDocs != null) && (cmDocs.GetErrorCount() > 0)) ||
                                  ((cmTasks != null) && (cmTasks.GetErrorCount() > 0))) ? IngestionResult.ING_ERRORED : IngestionResult.ING_DONE;
                    }
                }
            }
            catch (Exception ex)
            {
                result = IngestionResult.ING_ERRORED;
                LogError("Case: " + mmID + " " + ex.Message);
                entityStatus = EntityStatus.DISCARD;
            }
            finally
            {
                UpdateCaseStatus(result);
            }
            return result;
        }

        private IFolder getCase() { 

            FileNet.Api.Core.IObjectStore targetOS = ceInfo.GetObjectStore();
            // create case
            string pathName = "/IBM Case Manager/Solution Deployments/" + ceInfo.solution + "/Case Types/" + caseType;

            //check to see if there is an existing folder with this primary id and case type
            //EDIT
            IFolder newCaseFolder = null;
            if (isCaseLevelConfig)
            {
                newCaseFolder = getExistingFolder();
            }

            if (newCaseFolder == null)
            {
                //Get the parent
                IFolder caseTypeFolder = Factory.Folder.GetInstance(targetOS, null, pathName);

                // Create a new case folder
                newCaseFolder = Factory.Folder.CreateInstance(targetOS, caseType);
                // We must set the case folder name to something, but it will be renamed instantly by ICM
                newCaseFolder.FolderName = "TEMP";
                // We must also set the folder parent, but again ICM will instantly change it
                newCaseFolder.Parent = caseTypeFolder;
            }

            return newCaseFolder;
        }

        private IFolder getExistingFolder()
        {

            //Search for an Existing Case Folder ...
            try
            {
                IObjectStore targetOS = ceInfo.GetObjectStore();
                IFolder caseFolder = null;

                //need second query to pull thecase type and field column name from the mig man tables
                // case type is in case data, but where is the field defined
                //string caseIdentifierFieldName = getCaseIdentifierFieldName();
                string caseIdentifierFieldName = Ingest.CaseInformationService.getCaseKey(caseType);

                if (caseIdentifierFieldName.Length == 0) return null;

                string queryString = "select ID from " + caseType + " where " + caseIdentifierFieldName + "='" + mmAccountID + "'";

                SearchSQL sqlObject = new FileNet.Api.Query.SearchSQL(queryString);
                SearchScope searchScope = new FileNet.Api.Query.SearchScope(targetOS);
                IIndependentObjectSet objectSet = searchScope.FetchObjects(sqlObject, 50, null, true);

                System.Collections.IEnumerator enumerator = objectSet.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    caseFolder = (IFolder)enumerator.Current;
                    LogMessage("Found Existing Case for Case type '"
                        + caseType + "' where Identifier '" + caseIdentifierFieldName
                        + "' is '" + mmAccountID + "'");
                    break;   //Ignore if there are more than one
                }

                return caseFolder;
            }catch(Exception error)
            {
                //whatever this error is, we don't want it to stop processing 
                // this case because there is likely no duplicate...
                return null;
            }
        }

        public void SetSource(XmlDocument source)
        {
            sourceXML = source;
            mmID = sourceXML.SelectNodes("//row")[0].Attributes["ID"].Value;
            caseType = sourceXML.SelectNodes("//row")[0].Attributes["CaseType"].Value;
            mmAccountID = sourceXML.SelectNodes("//row")[0].Attributes["MMAccountID"].Value;
        }

        protected void PopulateCaseProperties(IFolder folder)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM " + jobName + "_CECaseProperties WHERE CaseID = " + mmID);

            foreach (XmlElement rowNode in xml.SelectNodes("//row"))
            {
                try
                {
                    Ingest.Ingestor.PopulateProperty(folder, rowNode.GetAttribute("Name"), rowNode.GetAttribute("Value"), rowNode.GetAttribute("Type"));
                }
                catch (Exception ex)
                {
                    LogError("Case creation: " + mmID + ": " + ex.Message);
                }
            }
        }

        protected void AddFolders(IFolder caseFolder, IFolder parentFolder, string parentName)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM " + jobName + "_CECaseFolders WHERE CaseID = " + mmID + " AND Parent = '" + parentName + "'");
            foreach (XmlElement rowNode in xml.SelectNodes("//row"))
            {
                string name = rowNode.GetAttribute("Name");

                try
                {

                    IFolder subFolder = Factory.Folder.CreateInstance(ceInfo.GetObjectStore(), "CmAcmCaseSubfolder");
                    subFolder.FolderName = name;
                    subFolder.Parent = parentFolder;
                    subFolder.Properties["CmAcmParentCase"] = caseFolder;
                    subFolder.Save(RefreshMode.REFRESH);

                    AddFolders(caseFolder, subFolder, name);
                }
                catch (Exception error)
                {
                    //check for a duplicate folder error. Let it go if it fails on a duplicat folder ... else, throw the exception up ...
                    if ((error.Message.CompareTo(
                        "A uniqueness requirement has been violated. The value for property FolderName of class CmAcmCaseSubfolder is not unique.") == 0))
                    {
                        // log that the folder exists
                        LogError("Case: " + mmID + ", Folder Name '" + name + "' already exists.");
                    }
                    else
                    {
                        throw error;
                    }
                }
            }
        }

        protected IngestionResult AddDocuments(string convBatch, Boolean depthFirst)
        {
            cmDocs = new CMDocuments(backgroundWorker, logger, dbConnection, batchID, jobName, isCaseLevelConfig, ceInfo);
            return cmDocs.PerformIngestion(convBatch, mmID, ceFolderPath, depthFirst);
        }

        protected IngestionResult AddTasks(string convBatch, Boolean depthFirst)
        {
            cmTasks = new CMTasks(backgroundWorker, logger, dbConnection, batchID, jobName, isCaseLevelConfig, ceInfo);
            return cmTasks.PerformIngestion(convBatch, mmID, ceGUID, depthFirst);
        }

        protected IngestionResult AddAnnotations(string convBatch, IFolder caseFolder, Boolean depthFirst)
        {
            cmAnnotations = new CMAnnotations(backgroundWorker, logger, dbConnection, batchID, jobName, isCaseLevelConfig, ceInfo);
            return cmAnnotations.IngestCaseAnnotations(convBatch, mmID, caseFolder, depthFirst);
        }

        protected void UpdateCaseStatus(IngestionResult migResult)
        {
            string sql = (ceGUID != string.Empty) ? 
                    "UPDATE " + jobName + "_CECases" + " SET Status = '" + migResult.ToString() + "', CE_GUID = '" + ceGUID + "', CaseFolderPath = '" + ceFolderPath + "' WHERE ID = " + mmID : 
                    "UPDATE " + jobName + "_CECases" + " SET Status = '" + migResult.ToString() + "' WHERE ID = " + mmID;
            
            DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
        }
    }
}
