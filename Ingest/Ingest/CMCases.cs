﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml;

namespace Ingest
{
    class CMCases : ClassBase
    {
        protected List<CMCase> caseCollection = new List<CMCase>();
        int totalCount = 0;
        int successCount = 0;
        int errorCount = 0;
        string solution = string.Empty;

        public CMCases(BackgroundWorker bw, Logger logr, string dbCnxn, string batch, string job, Boolean caseLevel, CommunicationInfo info) : base(bw, logr, dbCnxn, batch, job, caseLevel, info)
        {
            ItemsProcessed = false;
        }

        public Boolean ItemsProcessed { get; set; }

        public IngestionResult PerformIngestion(string convBatch, Boolean depthFirst)
        {
            IngestionResult result = IngestionResult.ING_NEW;

            do
            {
                if (backgroundWorker.CancellationPending)
                {
                    result = IngestionResult.ING_CANCELED;
                    break;
                }
                try
                {
                    XmlDocument caseXML = GetCECase(convBatch);
                    if (caseXML.SelectNodes("//row").Count == 0)
                    {
                        result = IngestionResult.ING_COMPLETE;
                        break;
                    }
                    ItemsProcessed = true;
                    CMCase cmc = new CMCase(backgroundWorker, logger, dbConnection, batchID, jobName, isCaseLevelConfig, ceInfo);
                    cmc.SetSource(caseXML);
                    result = cmc.PerformIngestion(convBatch, depthFirst);
                    totalCount++;
                    if (result == IngestionResult.ING_DONE) successCount++;
                    else if (result == IngestionResult.ING_ERRORED) errorCount++;
                }
                catch (Exception ex)
                {
                    LogError("Exception ingesting case: " + ex.Message);
                    result = IngestionResult.ING_ERRORED;
                }
            }
            while (result != IngestionResult.ING_COMPLETE);

            return result;
        }

        public int GetTotalCount()
        {
            return totalCount;
        }

        public int GetSuccessCount()
        {
            return successCount;
        }

        public int GetErrorCount()
        {
            return errorCount;
        }

        protected XmlDocument GetCECase(string convBatch)
        {
            return DBAdapter.DBAdapter.GetProcessingRecs(dbConnection, jobName + "_CECases", "MigrationID", batchID, "BatchID = " + convBatch, "Status = 'ING_NEW'", true);
        }
    }
}
