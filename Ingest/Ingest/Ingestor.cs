﻿using System;
using System.ComponentModel;
using System.Xml;

namespace Ingest
{
    public enum IngestionResult
    {
        ING_NEW,
        ING_CANCELED,
        ING_COMPLETE,
        ING_DONE,
        ING_ERRORED
    }

    public enum EntityStatus
    {
        DISCARD,
        ERROR,
        GOOD
    }

    public enum IngestionOptions
    {
        None = 0x0,
        DepthFirst = 0x01,
        Cases = 0x02,
        Documents = 0x04,
        Annotations = 0x08,
        Tasks = 0x10
    }

    public partial class Ingestor
    {
        BackgroundWorker backgroundWorker = null;
        Logger logger = null;

        string dbConnection = string.Empty;
        string jobName = string.Empty;
        Boolean isCaseLevelConfig = false;
        Boolean doTrace = false;
        string[] convBatchID;
        IngestionOptions chosenOptions;
        CaseInformationService caseInformationService = null;

        CommunicationInfo ceInfo = null;
        
        string sessionID = string.Empty;

        public Ingestor(
            string dbCnxn, 
            string job, 
            Boolean caseLevel, 
            string[] batches, 
            IngestionOptions options, 
            string uid, 
            string pwd, 
            string url, 
            string objectStore, 
            string solnName, 
            string cmServer, 
            string p8RestServer,
            Boolean useSSL, 
            Boolean dbg)
        {
            dbConnection = dbCnxn;
            jobName = job;
            isCaseLevelConfig = caseLevel;
            doTrace = dbg;
            convBatchID = new string[batches.Length];

            caseInformationService = new CaseInformationService(uid, pwd, solnName, p8RestServer, objectStore, isCaseLevelConfig);

            for (int i = 0; i < batches.Length; i++)
            {
                convBatchID[i] = batches[i];
            }
            chosenOptions = options;

            ceInfo = new CommunicationInfo(uid, pwd, url, objectStore, solnName, cmServer, useSSL);
            
            logger = new Logger(dbConnection, jobName, dbg);
        }

        public string PerformIngestion(BackgroundWorker bw)
        {
            backgroundWorker = bw;
            if (doTrace) DisplayMessage("Migrator: Starting Migration");
            try
            {
                if (ceInfo.GetObjectStore() != null)
                {
                    DisplayMessage("Connection to the Content Engine successful.");
                }
                else
                {
                    DisplayMessage("Could not connect to the IBM Content Engine.");
                    return "Session Aborted";
                }

            }
            catch (Exception ex)
            {
                DisplayMessage("Could not connect to the IBM Content Engine. " + ex.Message);
                return "Session Aborted";
            }

            string status = string.Empty;
            IngestionResult result = IngestionResult.ING_NEW;

            StartNewSession();

            int batchCount = 0;

            for(int i = 0; i < convBatchID.Length; i++)
            {
                if (bw.CancellationPending)
                {
                    result = IngestionResult.ING_CANCELED;
                    break;
                }
                DisplayMessage("Processing Batch: " + (batchCount + 1).ToString() + " of current session");
                try
                {
                    result = IngestBatch(convBatchID[i]);
                    batchCount++;
                }
                catch (Exception ex)
                {
                    DisplayMessage("Exception during batch conversion: " + ex.Message);
                }
            }

            switch (result)
            {
                case IngestionResult.ING_CANCELED:
                    status = CancelSession();
                    break;

                default:
                    status = EndSession();
                    DisplayMessage("Last batch processed; no more batches to process.");
                    DisplayMessage("Batches Processed to Completion: " + batchCount.ToString());
                    break;
            }
            return status;
        }

        protected void DisplayMessage(String message)
        {
            backgroundWorker.ReportProgress(0, message);
        }

        protected void StartNewSession()
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.FetchInsertedRec(dbConnection, "INSERT INTO MgrSessions (SessionType, UserName, WorkStation) OUTPUT inserted.* VALUES ('INGESTION', '" + Environment.UserName + "', '" + Environment.MachineName + "')");
            sessionID = xmlDoc.SelectNodes("//row")[0].Attributes["ID"].Value;

            DisplayMessage("Session Started; Session ID: " + sessionID);
        }

        protected string EndSession()
        {
            DBAdapter.DBAdapter.UpdateDB(dbConnection, "UPDATE MgrSessions SET EndTime = GETDATE(), Status = 'COMPLETED' WHERE ID = " + sessionID);

            return "Session Complete";
        }

        protected string AbortSession()
        {
            DBAdapter.DBAdapter.UpdateDB(dbConnection, "UPDATE MgrSessions SET EndTime = GETDATE(), Status = 'ERRORED' WHERE ID = " + sessionID);

            return "Session Complete";
        }

        protected string CancelSession()
        {
            DBAdapter.DBAdapter.UpdateDB(dbConnection, "UPDATE MgrSessions SET EndTime = GETDATE(), Status = 'CANCELED' WHERE ID = " + sessionID);

            return "Session Canceled";
        }

        protected IngestionResult IngestBatch(string convBatchID)
        {
            IngestionResult result = IngestionResult.ING_NEW;

            string batchID = StartNewBatch(convBatchID);

            Batch b = new Batch(backgroundWorker, logger, dbConnection, batchID, jobName, isCaseLevelConfig, ceInfo);

            try
            {
                result = b.PerformIngestion(convBatchID, chosenOptions);
            }
            catch (Exception ex)
            {
                DisplayMessage("Exception::Batch: " + batchID.ToString() + ": " + ex.Message);
                result = IngestionResult.ING_ERRORED;
            }
            return result;
        }

        protected string StartNewBatch(string convBatchID)
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.FetchInsertedRec(dbConnection, "INSERT INTO MgrMigrations (SessionID, BatchID, UserName, WorkStation) OUTPUT inserted.* VALUES (" + sessionID + ", " + convBatchID + ", '" + Environment.UserName + "', '" + Environment.MachineName + "')");
            return xmlDoc.SelectNodes("//row")[0].Attributes["ID"].Value;
        } 
    }
}
