﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Xml;

using FileNet.Api;
using FileNet.Api.Authentication;
using FileNet.Api.Collection;
using FileNet.Api.Core;
using FileNet.Api.Property;
using FileNet.Api.Util;

namespace Ingest
{
    public partial class Ingestor
    {
        public static DateTime GetUTCDateTime(string s)
        {
            DateTime result = new DateTime(1970, 1, 1, 4, 0, 0).AddTicks(long.Parse(s)).ToUniversalTime();
            if (result.Year < 1753)
            {
                result = new DateTime(1920, 1, 1, 4, 0, 0).ToUniversalTime();
                throw new ArgumentException("Bad date. Year < 1753");
            }
            return result;
        }

        public static Boolean RemoteCertValidate(Object sender, X509Certificate cert, X509Chain chain, System.Net.Security.SslPolicyErrors error)
        {
            return true;
        }

        public static void PopulateProperty(IEngineObject eObject, string name, string value, string dataType)
        {
            switch (dataType)
            {
                case "SingletonBoolean":
                    eObject.Properties[name] = (value.ToUpper() == "T") ? true : (value.ToUpper() == "F") ? false : Convert.ToBoolean(value);
                    break;

                case "ListOfBoolean":
                    if (eObject.Properties.FindProperty(name) == null)
                    {
                        eObject.Properties[name] = Factory.BooleanList.CreateList();
                    }
                    eObject.Properties.GetBooleanListValue(name).Add((value.ToUpper() == "T") ? true : (value.ToUpper() == "F") ? false : Convert.ToBoolean(value));
                    break;

                case "SingletonDateTime":
                    eObject.Properties[name] = Ingest.Ingestor.GetUTCDateTime(value);
                    break;

                case "ListOfDateTime":
                    if (eObject.Properties.FindProperty(name) == null)
                    {
                        eObject.Properties[name] = Factory.DateTimeList.CreateList();
                    }
                    eObject.Properties.GetDateTimeListValue(name).Add(Ingest.Ingestor.GetUTCDateTime(value));
                    break;

                case "SingletonFloat64":
                    eObject.Properties[name] = Convert.ToDouble(value);
                    break;

                case "ListOfFloat64":
                    if (eObject.Properties.FindProperty(name) == null)
                    {
                        eObject.Properties[name] = Factory.Float64List.CreateList();
                    }
                    eObject.Properties.GetFloat64ListValue(name).Add(Convert.ToDouble(value));
                    break;

                case "SingletonInt32":
                    eObject.Properties[name] = Convert.ToInt32(value);
                    break;

                case "ListOfInteger32":
                    if (eObject.Properties.FindProperty(name) == null)
                    {
                        eObject.Properties[name] = Factory.Integer32List.CreateList();
                    }
                    eObject.Properties.GetInteger32ListValue(name).Add(Convert.ToInt32(value));
                    break;

                case "SingletonString":
                    string pattern = "''";
                    string replacement = "'";
                    Regex rgx = new Regex(pattern);
                    string result = rgx.Replace(value, replacement);
                    eObject.Properties[name] = result;
                    break;

                case "ListOfString":
                    if (eObject.Properties.FindProperty(name) == null)
                    {
                        eObject.Properties[name] = Factory.StringList.CreateList();
                    }
                    eObject.Properties.GetStringListValue(name).Add(value);
                    break;

                case "SingletonId":
                    eObject.Properties[name] = value;
                    break;

                default:
                    throw new ArgumentException("Property: " + name + " has unsupported data type " + dataType);
            }
        }

        public static string GetAttributeValue(XmlDocument xml, string attributeName)
        {
            XmlNode node = xml.SelectNodes("//row")[0];

            string result = string.Empty;
            if (node.Attributes[attributeName] != null)
            {
                result = node.Attributes[attributeName].Value;
            }
            return result;
        }
    }
}
