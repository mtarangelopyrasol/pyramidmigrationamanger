﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml;

namespace Ingest
{
    class CMDocuments : ClassBase
    {
        protected string mmCaseID = string.Empty;
        protected List<CMDocument> docCollection = new List<CMDocument>();
        int totalCount = 0;
        int errorCount = 0;
        int successCount = 0;

        public CMDocuments(BackgroundWorker bw, Logger logr, string dbCnxn, string batch, string job, Boolean caseLevel, CommunicationInfo info) : base(bw, logr, dbCnxn, batch, job, caseLevel, info)
        {
            ItemsProcessed = false;
        }

        public Boolean ItemsProcessed { get; set; }

        public IngestionResult PerformIngestion(string convBatch, string caseID, string caseFolderPath, Boolean depthFirst)
        {
            IngestionResult result = IngestionResult.ING_NEW;
            
            mmCaseID = caseID;

            while (result != IngestionResult.ING_COMPLETE)
            {
                if (backgroundWorker.CancellationPending)
                {
                    result = IngestionResult.ING_CANCELED;
                    break;
                }
                XmlDocument docsXML = GetDocuments(convBatch, depthFirst);
                if (docsXML.SelectNodes("//row").Count == 0)
                {
                    result = IngestionResult.ING_COMPLETE;
                    break;
                }
                ItemsProcessed = true;
                foreach (XmlElement rowNode in docsXML.SelectNodes("//row"))
                {
                    if (backgroundWorker.CancellationPending)
                    {
                        result = IngestionResult.ING_CANCELED;
                        break;
                    }
                    try
                    {
                        CMDocument cmd = new CMDocument(backgroundWorker, logger, dbConnection, batchID, jobName, isCaseLevelConfig, ceInfo);
                        cmd.SetSource(rowNode);
                        result = cmd.PerformIngestion(convBatch, mmCaseID, caseFolderPath, depthFirst);
                    }
                    catch (Exception ex)
                    {
                        LogError("Exception processing document: " + ex.Message);
                        result = IngestionResult.ING_ERRORED;
                    }
                    totalCount++;
                    if ((result == IngestionResult.ING_DONE) || (result == IngestionResult.ING_COMPLETE)) successCount++;
                    else if (result == IngestionResult.ING_ERRORED) errorCount++;
                    if (!(isCaseLevelConfig) && (result == IngestionResult.ING_COMPLETE)) result = IngestionResult.ING_DONE;
                }
            }
            return result;
        }

        public int GetTotalCount()
        {
            return totalCount;
        }

        public int GetSuccessCount()
        {
            return successCount;
        }

        public int GetErrorCount()
        {
            return errorCount;
        }

        protected XmlDocument GetDocuments(string convBatch, Boolean depthFirst)
        {
            string condition = (isCaseLevelConfig) ? "CaseID = " + mmCaseID + " AND BatchID = " + convBatch : "BatchID = " + convBatch;
            Boolean singleOption = (isCaseLevelConfig) ? !depthFirst : true;
            return DBAdapter.DBAdapter.GetProcessingRecs(dbConnection, jobName + "_CEDocuments", "MigrationID", batchID, condition, "Status = 'ING_NEW'", singleOption);
        }   
    }
}
