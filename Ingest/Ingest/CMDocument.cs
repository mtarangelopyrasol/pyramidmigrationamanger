﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Xml;
using System.IO;
using FileNet.Api.Constants;
using FileNet.Api.Core;
using FileNet.Api.Util;
using FileNet.Api.Collection;
using FileNet.Api.Meta;
using FileNet.Api.Admin;
using FileNet.Api.Property;

namespace Ingest
{
    class CMDocument : ClassBase
    {
        string mmCaseID = string.Empty;
        string mmID = string.Empty;
        string containmentName = string.Empty;
        string docClass = string.Empty;
        string docFolder = string.Empty;
        string mimeType = string.Empty;
        string ceGUID = string.Empty;
        string ceCaseFolder = string.Empty;
        CMAnnotations cmAnnotations = null;

        public CMDocument(BackgroundWorker bw, Logger logr, string dbCnxn, string batch, string job, Boolean caseLevel, CommunicationInfo info) : base(bw, logr, dbCnxn, batch, job, caseLevel, info)
        {
        }

        public IngestionResult PerformIngestion(string convBatch, string caseID, string caseFolderPath, Boolean depthFirst)
        {
            IngestionResult result = IngestionResult.ING_NEW;

            mmCaseID = caseID;

            try
            {
                LogMessage("Document: " + mmID);

                if (isCaseLevelConfig)
                {
                    ceCaseFolder = (depthFirst) ? caseFolderPath : PopulateCaseInformation();
                    if (ceCaseFolder == string.Empty) throw new ArgumentException("Case not yet processed");
                }

                FileNet.Api.Core.IObjectStore targetOS = ceInfo.GetObjectStore();
                // create document
                IClassDefinition classDef = Factory.ClassDefinition.FetchInstance(targetOS, docClass, null);
                IDocument doc = Factory.Document.CreateInstance(targetOS, classDef.SymbolicName);

                AddPropertiesAndContent(doc);
                doc.Save(RefreshMode.REFRESH);
              
                // Check in the document.
                doc.Checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
                doc.Save(RefreshMode.REFRESH);
                
                // File the document in the folder.
                if (docFolder.Length > 0)
                {
                    string folderToFile = (isCaseLevelConfig) ? ceCaseFolder + "/" + docFolder : docFolder;
                    bool filedInFolder = false;
                    for (int nAttempts = 0; nAttempts < 3; nAttempts++)
                    {
                        try
                        {
                            IFolder folder = Factory.Folder.GetInstance(targetOS, ClassNames.FOLDER, folderToFile);
                            IReferentialContainmentRelationship rcr = folder.File(doc, AutoUniqueName.AUTO_UNIQUE, containmentName, DefineSecurityParentage.DO_NOT_DEFINE_SECURITY_PARENTAGE);
                            rcr.Save(RefreshMode.REFRESH);
                            filedInFolder = true;
                            break;
                        }
                        catch(Exception ex)
                        {
                            if (isCaseLevelConfig)
                            {
                                LogMessage("Attempt " + (nAttempts + 1).ToString() + ": Failed to file in folder " + folderToFile + " " + ex.Message + "; attempting again");
                                Stopwatch stopwatch = Stopwatch.StartNew();
                                while (true)
                                {
                                    if (stopwatch.ElapsedMilliseconds >= 15)
                                    {
                                        break;
                                    }
                                    Thread.Sleep(1); //so processor can rest for a while
                                }
                            }
                            else
                            {
                                throw;
                            }
                        }
                    }
                    if (!filedInFolder) LogError("Failed to file document " + doc.Id.ToString() + " in folder " + folderToFile);
                }
                ceGUID = doc.Id.ToString();
                result = IngestionResult.ING_DONE;

                if (depthFirst)
                {
                    result = AddAnnotations(convBatch, doc, depthFirst);
                    if (result == IngestionResult.ING_COMPLETE)
                    {
                        if (cmAnnotations.ItemsProcessed)
                        {
                            LogMessage("Successfully Ingested Annotations: " + cmAnnotations.GetSuccessCount());
                            LogMessage("Errored Annotations: " + cmAnnotations.GetErrorCount());
                            LogMessage("Total Target Annotations: " + cmAnnotations.GetTotalCount());
                        }
                    }
                }
                result = ((cmAnnotations != null) && (cmAnnotations.GetErrorCount() > 0)) ? IngestionResult.ING_ERRORED : IngestionResult.ING_DONE;
            }
            catch (Exception ex)
            {
                result = IngestionResult.ING_ERRORED;
                LogError("Document: " + mmID + " " + ex.Message);
                entityStatus = EntityStatus.DISCARD;
            }
            finally
            {
                UpdateDocumentStatus(result);
            }
            return result;
        }

        public void SetSource(XmlElement xml)
        {
            mmID = xml.GetAttribute("ID");
            containmentName = xml.GetAttribute("ContainmentName");
            docClass = xml.GetAttribute("DocClass");
            mimeType = xml.GetAttribute("MimeType");
            docFolder = xml.GetAttribute("Folder");
        }

        protected void AddPropertiesAndContent(IDocument doc)
        {
            AddContent(doc);
            PopulateDocumentProperties(doc);
        }

        protected void AddContent(IDocument doc)
        {
            IContentTransfer ct = Factory.ContentTransfer.CreateInstance();
            IContentElementList celist = Factory.ContentElement.CreateList();

            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM " + jobName + "_CEDocumentPages WHERE DocumentID = " + mmID);

            foreach (XmlElement rowNode in xml.SelectNodes("//row"))
            {
                string filePath = rowNode.GetAttribute("FilePath");
                //string fileName = Path.GetFileName(filePath);
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                ct.SetCaptureSource(fs);
                ct.Properties["MimeType"] = rowNode.GetAttribute("MimeType");
                //ct.Properties["RetrievalName"] = fileName;
                ct.ContentType = rowNode.GetAttribute("MimeType");
                celist.Add(ct);
            }
            doc.ContentElements = celist;
        }

        protected void PopulateDocumentProperties(IDocument doc)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM " + jobName + "_CEDocumentProperties WHERE DocumentID = " + mmID);

            foreach (XmlElement rowNode in xml.SelectNodes("//row"))
            {
                string dataType = rowNode.GetAttribute("Type");
                string value = rowNode.GetAttribute("Value");
                string name = rowNode.GetAttribute("Name");

                try
                {
                    if (dataType == "ATTACH_DOC_GUID")
                    {
                        XmlDocument xmlGUID = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT CE_GUID FROM " + jobName + "_CEDocuments WHERE ID = " + value);
                        value = xmlGUID.SelectNodes("//row")[0].Attributes["CE_GUID"].Value;
                        dataType = "SingletonString";
                    }
                    Ingest.Ingestor.PopulateProperty(doc, name, value, dataType);
                    
                }
                catch (Exception ex)
                {
                    LogError("Document " + mmID + ": Exception while populating property " + name + "(data type: " + dataType + ") with value " + value + " " + ex.Message);
                }
            }
            doc.MimeType = mimeType;
        }

        protected IngestionResult AddAnnotations(string convBatch, IDocument doc, Boolean depthFirst)
        {
            cmAnnotations = new CMAnnotations(backgroundWorker, logger, dbConnection, batchID, jobName, isCaseLevelConfig, ceInfo);
            return cmAnnotations.IngestDocAnnotations(convBatch, mmID, doc, depthFirst);
        }

        protected void UpdateDocumentStatus(IngestionResult migResult)
        {
            string sql = (ceGUID != string.Empty) ?
                    "UPDATE " + jobName + "_CEDocuments" + " SET Status = '" + migResult.ToString() + "', CE_GUID = '" + ceGUID + "' WHERE ID = " + mmID :
                    "UPDATE " + jobName + "_CEDocuments" + " SET Status = '" + migResult.ToString() + "' WHERE ID = " + mmID;

            DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
        }

        protected string PopulateCaseInformation()
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT c.CaseFolderPath FROM " + jobName + "_CECases c, " + jobName + "_CEDocuments d WHERE d.ID = " + mmID + " AND d.CaseID = c.ID");
            return Ingestor.GetAttributeValue(xml, "CaseFolderPath");
        }
    }
}
