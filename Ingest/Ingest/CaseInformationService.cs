﻿using System;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Collections.Generic;

namespace Ingest
{

    class CaseInformationService
    {
        //control variable
        private static bool isInitialized = false;

        //security variables
        private string userName = string.Empty;
        private string password = string.Empty;

        //connection variables
        private string solution = string.Empty;
        private string p8Server = string.Empty;
        private string TOS = string.Empty;

        //cases identifier mappings
        private static List<CaseTypeDef> caseTypes;
        private static Dictionary<string, CaseDef> caseToKeyMap = new Dictionary<string, CaseDef>();

        //constructor
        public CaseInformationService(string userName, string password, string solution, string p8Server, string TOS, bool isCaseLevelConfig)
        {
            try
            {
                this.userName = userName;
                this.password = password;

                this.solution = solution;
                this.p8Server = p8Server;
                this.TOS = TOS;
                
                if (isCaseLevelConfig)
                    Initialize();

            }catch(Exception error)
            {
                throw new Exception("Unexpected error connecting to Case REST service. See nested exception.", error);
            }
        }

        public void Initialize()
        {
            //Only initialized once ...
            if(CaseInformationService.isInitialized)
            {
                return;
            }

            GetSolutionCaseInformationServiceCall().Wait();

            if(CaseInformationService.caseTypes == null)
            {
                throw new Exception("Unable to get Case Data from Solution with REST services.");
            }

            foreach(CaseTypeDef caseType in caseTypes)
            {
                GetCaseTypeInformationServiceCall(caseType.CaseType).Wait();
            }

            CaseInformationService.isInitialized = true;
        }

        private async Task GetSolutionCaseInformationServiceCall()
        {
            string baseAddress = "http://" + this.p8Server + "/";

            //get case types ...
            string uri = "http://" + this.p8Server + "/CaseManager/CASEREST/v1/solution/" +
                          solution + "/casetypes?TargetObjectStore=" + TOS; ;

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            byte[] cred = UTF8Encoding.UTF8.GetBytes(userName + ":" + password);

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(cred));

            HttpResponseMessage response = client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {

                string results = await response.Content.ReadAsStringAsync();

                // Strip the initial object ...
                results = results.Substring(results.IndexOf('['));
                results = results.Substring(0, results.IndexOf(']') + 1);
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                CaseInformationService.caseTypes = javaScriptSerializer.Deserialize<List<CaseTypeDef>>(results);

                //Console.Write(results);
            }
            else
            {
                throw new Exception("Status: " + response.StatusCode + " Unable to connect with Case REST service at uri: " + uri);
            }
        }

        private async Task GetCaseTypeInformationServiceCall(string caseType)
        {

            string baseAddress = "http://" + this.p8Server + "/";

            //get case type ...
            String uri = "http://" + this.p8Server + "/CaseManager/CASEREST/v1/casetype/" +
                         caseType + "?TargetObjectStore=" + TOS;


            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            byte[] cred = UTF8Encoding.UTF8.GetBytes(userName + ":" + password);

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(cred));

            HttpResponseMessage response = client.GetAsync(uri).Result;  
            if (response.IsSuccessStatusCode)
            {
                string results = await response.Content.ReadAsStringAsync();
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

                CaseDef thisDefinition = javaScriptSerializer.Deserialize<CaseDef>(results);
                CaseInformationService.caseToKeyMap.Add(caseType, thisDefinition);
                //Console.Write(results);
            }
            else
            {
                throw new Exception("Status: " + response.StatusCode + " Unable to connect with Case REST service at uri: " + uri );
            }
        }

        public static string getCaseKey(string caseType)
        {
            CaseDef thisCase = CaseInformationService.caseToKeyMap[caseType];

            if(thisCase == null)
            {
                return null;
            }
            else
            {
                return thisCase.CaseTitleProperty;
            }
        }
    }

    class CaseTypeDef
    {
        public string CaseType { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public bool HasInstanceCreationRights { get; set; }
        public bool HasAnnotationRights { get; set; }
    }

    class CaseDef
    {
        public string CaseType { get; set; }
        public string Description { get; set; }
        public string CaseTitleProperty { get; set; }
        public string DisplayName { get; set; }
    }
}
