﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

using DBAdapter;

namespace Ingest
{
    class Logger
    {
        string dbConnection;
        string logTable;
        Boolean debugOption;

        public Logger(string cnxn, string jobName, Boolean dbg)
        {
            dbConnection = cnxn;
            logTable = jobName + "_IngestLog";
            debugOption = dbg;
        }

        public void LogMessage(string batchID, string message)
        {
            string sql = "INSERT INTO " + logTable + "(BatchID, Type, Description) VALUES ('" + batchID + "', 'AUDIT', '" + QuoteFree(message) + "')";
            DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
        }

        public void LogError(string batchID, string message)
        {
            string sql = "INSERT INTO " + logTable + "(BatchID, Type, Description) VALUES ('" + batchID + "', 'ERROR', '" + QuoteFree(message) + "')";
            DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
        }

        public void LogDebugMessage(string batchID, string message)
        {
            if (debugOption)
            {
                string sql = "INSERT INTO " + logTable + "(BatchID, Type, Description) VALUES ('" + batchID + "', 'DEBUG', '" + QuoteFree(message) + "')";
                DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
            }
        }

        private string QuoteFree(string input)
        {
            string result = string.Empty;

            string pattern = "'";
            string replacement = " ";
            Regex rgx = new Regex(pattern);
            result = rgx.Replace(input, replacement);
            return result;
        }
    }
}
