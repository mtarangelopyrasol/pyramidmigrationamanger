﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;

namespace DBAdapter
{
    public class DBAdapter
    {
        const int MAX_ATTEMPTS = 3;

        static Regex rgxAmpersand = new Regex("&");
        const string repAmpersand = "&amp;";

        static Regex rgxSingleQuote = new Regex("'");
        const string repSingleQuote = "&apos;";

        static Regex rgxDoubleQuote = new Regex("\"");
        const string repDoubleQuote = "&quot;";

        static Regex rgxLessThan = new Regex("<");
        const string repLessThan = "&lt;";

        static Regex rgxGreaterThan = new Regex(">");
        const string repGreaterThan = "&gt;"; 
           
        public string ExecuteSelect(string cnxn, string sql)
        {
            string result = "<rows>";
            string xmlSQL = sql + " FOR XML RAW;";

            int nAttempts = 0;
            do
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(cnxn))
                    {
                        using (SqlCommand command = new SqlCommand(xmlSQL, connection))
                        {
                            connection.Open();

                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    result += reader[0].ToString();
                                }
                            }
                        }
                    }
                    break;
                }
                catch (SqlException ex)
                {
                    if ((ex.Number == 1205) && (nAttempts < (MAX_ATTEMPTS - 1)))
                    {
                        // thrown out as a victim of deadlock
                        Stopwatch stopwatch = Stopwatch.StartNew();
                        while (true)
                        {
                            if (stopwatch.ElapsedMilliseconds >= 15)
                            {
                                break;
                            }
                            Thread.Sleep(1); //so processor can rest for a while
                        }
                        nAttempts++;
                    }
                    else throw new Exception("DBException: " + ex.Message + " (SQL: " + sql + ")");
                }
                catch (Exception ex)
                {
                    throw new Exception("DBException: " + ex.Message + " (SQL: " + sql + ")");
                }
            }
            while(nAttempts < MAX_ATTEMPTS);
            result += "</rows>";
            return result;
        }

        public int ExecuteInsert(string cnxn, string sql)
        {
            int nAffected = -1;
            int nAttempts = 0;
            do
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(cnxn))
                    {
                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.Connection.Open();
                            nAffected = command.ExecuteNonQuery();
                        }
                    }
                    break;
                }
                catch (SqlException ex)
                {
                    if ((ex.Number == 1205) && (nAttempts < (MAX_ATTEMPTS - 1)))
                    {
                        // thrown out as a victim of deadlock
                        Stopwatch stopwatch = Stopwatch.StartNew();
                        while (true)
                        {
                            if (stopwatch.ElapsedMilliseconds >= 15)
                            {
                                break;
                            }
                            Thread.Sleep(1); //so processor can rest for a while
                        }
                        nAttempts++;
                    }
                    else throw new Exception("DBException: " + ex.Message + " (SQL: " + sql + ")");
                }
                catch (Exception ex)
                {
                    throw new Exception("DBException: " + ex.Message + " (SQL: " + sql + ")");
                }
            }
            while (nAttempts < MAX_ATTEMPTS);
            return nAffected;
        }

        public int ExecuteUpdate(string cnxn, string sql)
        {
            int nAffected = -1;
            int nAttempts = 0;
            do
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(cnxn))
                    {
                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.Connection.Open();
                            nAffected = command.ExecuteNonQuery();
                        }
                    }
                    break;
                }
                catch (SqlException ex)
                {
                    if ((ex.Number == 1205) && (nAttempts < (MAX_ATTEMPTS - 1)))
                    {
                        // thrown out as a victim of deadlock
                        Stopwatch stopwatch = Stopwatch.StartNew();
                        while (true)
                        {
                            if (stopwatch.ElapsedMilliseconds >= 15)
                            {
                                break;
                            }
                            Thread.Sleep(1); //so processor can rest for a while
                        }
                        nAttempts++;
                    }
                    else throw new Exception("DBException: " + ex.Message + " (SQL: " + sql + ")");
                }
                catch (Exception ex)
                {
                    throw new Exception("DBException: " + ex.Message + " (SQL: " + sql + ")");
                }
            }
            while (nAttempts < MAX_ATTEMPTS);
            return nAffected;
        }

        public int ExecuteDelete(string cnxn, string sql)
        {
            int nAffected = -1;
            int nAttempts = 0;
            do
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(cnxn))
                    {
                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.Connection.Open();
                            nAffected = command.ExecuteNonQuery();
                        }
                    }
                    break;
                }
                catch (SqlException ex)
                {
                    if ((ex.Number == 1205) && (nAttempts < (MAX_ATTEMPTS - 1)))
                    {
                        // thrown out as a victim of deadlock
                        Stopwatch stopwatch = Stopwatch.StartNew();
                        while (true)
                        {
                            if (stopwatch.ElapsedMilliseconds >= 15)
                            {
                                break;
                            }
                            Thread.Sleep(1); //so processor can rest for a while
                        }
                        nAttempts++;
                    }
                    else throw new Exception("DBException: " + ex.Message + " (SQL: " + sql + ")");
                }
                catch (Exception ex)
                {
                    throw new Exception("DBException: " + ex.Message + " (SQL: " + sql + ")");
                }
            }
            while (nAttempts < MAX_ATTEMPTS);
            return nAffected;
        }

        public static XmlDocument GetProcessingRecs(string cnxn, string tableName, string batchColName, string batchId, string whereClause, string statusClause, Boolean singleOption)
        {
            string effectiveWhereClause = (whereClause.Length > 0) ? whereClause + " AND " + statusClause : statusClause;
            string sql = "UPDATE " + tableName + " SET Status = 'PROCESSING', " + batchColName + " = " + batchId + " WHERE " + effectiveWhereClause;
            if (singleOption)
            {
                Regex rgx = new Regex("UPDATE");
                sql = rgx.Replace(sql, "UPDATE TOP(1)");
            }
            DBAdapter dba = new DBAdapter();
            dba.ExecuteUpdate(cnxn, sql);

            sql = "SELECT * FROM " + tableName + " WHERE Status = 'PROCESSING' AND " + batchColName + " = " + batchId;
            if (whereClause.Length > 0)
            {
                sql += " AND ";
                sql += whereClause;
            }
            string result = dba.ExecuteSelect(cnxn, sql);

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(result);
            return xml;
        }

        public static XmlDocument FetchInsertedRec(string cnxn, string insertSQL)
        {
            string result = "<rows>";
            try
            {
                using (SqlConnection connection = new SqlConnection(cnxn))
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand(insertSQL, connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                int fieldCount = reader.FieldCount;

                                result += "<row";
                                for (int i = 0; i < fieldCount; i++)
                                {
                                    result += " ";
                                    result += reader.GetName(i);
                                    result += "=\"";
                                    result += SanitizeXml(reader[i].ToString());
                                    result += "\"";
                                }
                                result += "/>";
                            }
                        }
                    }
                }
                result += "</rows>";
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(result);
                return xml;
            }
            catch (Exception ex)
            {
                throw new Exception("DBException: " + ex.Message + " (SQL: " + insertSQL + ")");
            }
        }

        public static XmlDocument SelectFromDB(string dbCnxn, string sql)
        {
            DBAdapter dba = new DBAdapter();
            string result = dba.ExecuteSelect(dbCnxn, sql);

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(result);
            return xml;
        }

        public static int AddToDB(string dbCnxn, string sql)
        {
            DBAdapter dba = new DBAdapter();
            return dba.ExecuteInsert(dbCnxn, sql);
        }

        public static int UpdateDB(string dbCnxn, string sql)
        {
            DBAdapter dba = new DBAdapter();
            return dba.ExecuteUpdate(dbCnxn, sql);
        }

        public static int DeleteDB(string dbCnxn, string sql)
        {
            DBAdapter dba = new DBAdapter();
            return dba.ExecuteDelete(dbCnxn, sql);
        }

        public static Boolean DoesTableExist(string dbCnxn, string tableName)
        {
            string sql = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" + tableName + "'";

            DBAdapter dba = new DBAdapter();
            string result = dba.ExecuteSelect(dbCnxn, sql);

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(result);
            int count = xml.SelectNodes("//row").Count;
            return (count > 0) ? true : false;
        }

        public static int BulkLoadData(string dbCnxn, string tableName, string dataFile, Boolean isMultiValueFieldLoad)
        {
            string formatFileName = GenerateFormatFileName(tableName, dataFile);
            string columnList = GenerateFormatFile(dbCnxn, tableName, formatFileName, isMultiValueFieldLoad);

            string sql = "INSERT INTO " + tableName + " (" + columnList + ") SELECT " + columnList + " FROM OPENROWSET ( BULK '" + dataFile + "', FORMATFILE='" + formatFileName + "') AS TEMP";
            DBAdapter dba = new DBAdapter();
            // extend timeout for the bulk load
            string extCnxn = dbCnxn + "; Connection Timeout = 600"; // increase from default 15 seconds to 5 minutes
            return dba.ExecuteInsert(extCnxn, sql); 
            //TODO Delete format file
            //Command: bcp MigrationDB.dbo.GrandRapids_DocumentData format nul -c -t, -x -f format.Xml -T
        }

        public static string GenerateFormatFile(string dbCnxn, string tableName, string fmtFileName, Boolean isMultiValueFieldLoad)
        {
            string columnList = string.Empty;

            string sql = (isMultiValueFieldLoad) ? "SELECT COLUMN_NAME, CHARACTER_MAXIMUM_LENGTH, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tableName + "' AND COLUMN_NAME NOT IN ('ID', 'Datatype', 'Format') ORDER BY ORDINAL_POSITION" :
                "SELECT COLUMN_NAME, CHARACTER_MAXIMUM_LENGTH, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tableName + "' AND COLUMN_NAME NOT IN ('ID', 'BatchID', 'Status') ORDER BY ORDINAL_POSITION";

            DBAdapter dba = new DBAdapter();
            string result = dba.ExecuteSelect(dbCnxn, sql);

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(result);

            XmlTextWriter xtw = new XmlTextWriter(fmtFileName, null);
            xtw.Formatting = Formatting.Indented;
            xtw.IndentChar = ' ';
            xtw.Indentation = 4;
            xtw.WriteStartDocument();

            xtw.WriteStartElement("BCPFORMAT");
            xtw.WriteAttributeString("xmlns", "http://schemas.microsoft.com/sqlserver/2004/bulkload/format");
            xtw.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");

            xtw.WriteStartElement("RECORD");

            int i = 1;
            foreach (XmlElement rowNode in xml.SelectNodes("//row"))
            {
                xtw.WriteStartElement("FIELD");
                xtw.WriteAttributeString("ID", i.ToString());
                xtw.WriteAttributeString("xsi:type", "CharTerm");
                if (i == xml.SelectNodes("//row").Count) 
                {
                    xtw.WriteAttributeString("TERMINATOR", "\\r\\n");
                }
                else
                {
                    xtw.WriteAttributeString("TERMINATOR", "|");
                }
                string dType = rowNode.GetAttribute("DATA_TYPE");
                if (dType == "datetime")
                {
                    xtw.WriteAttributeString("MAX_LENGTH", "24");
                }
                else if (dType == "int")
                {
                    xtw.WriteAttributeString("MAX_LENGTH", "12");
                }
                else
                {
                    xtw.WriteAttributeString("MAX_LENGTH", rowNode.GetAttribute("CHARACTER_MAXIMUM_LENGTH"));
                    xtw.WriteAttributeString("COLLATION", "SQL_Latin1_General_CP1_CI_AS");
                }
                xtw.WriteEndElement();
                i++;
            }

            xtw.WriteEndElement();

            xtw.WriteStartElement("ROW");

            i = 1;
            foreach (XmlElement rowNode in xml.SelectNodes("//row"))
            {
                xtw.WriteStartElement("COLUMN");
                xtw.WriteAttributeString("SOURCE", i.ToString());

                string colName = rowNode.GetAttribute("COLUMN_NAME");
                xtw.WriteAttributeString("NAME", colName);

                columnList = (i == 1) ? colName : columnList + ", " + colName;

                string dType = rowNode.GetAttribute("DATA_TYPE");

                if (dType == "varchar")
                {
                    xtw.WriteAttributeString("xsi:type", "SQLVARYCHAR");
                } 
                else if (dType == "datetime")
                {
                    xtw.WriteAttributeString("xsi:type", "SQLDATETIME");
                }
                else if (dType == "int")
                {
                    xtw.WriteAttributeString("xsi:type", "SQLINT");
                }
                else
                {
                    xtw.WriteAttributeString("xsi:type", "CharTerm");
                }
                xtw.WriteEndElement();
                i++;
            }
            xtw.WriteEndElement();

            xtw.WriteEndElement();
            xtw.WriteEndDocument();
            xtw.Flush();
            xtw.Close();

            return columnList;
        }

        public static string GenerateFormatFileName(string tableName, string dataFile)
        {
            string fmtFileName = string.Empty;

            string dirName = Path.GetDirectoryName(dataFile);
            fmtFileName = dirName + "\\" + tableName + "Format.Xml";
            return fmtFileName;
        }

        public static string SanitizeXml(string input)
        {
            string temp1 = rgxAmpersand.Replace(input, repAmpersand);
            string temp2 = rgxDoubleQuote.Replace(temp1, repDoubleQuote);
            string temp3 = rgxLessThan.Replace(temp2, repLessThan);
            string temp4 = rgxGreaterThan.Replace(temp3, repGreaterThan);
            return rgxSingleQuote.Replace(temp4, repSingleQuote);
        }

        public static int ExecuteSPAdjustColumnNumbers(string cnxn, int configID, string tableName, int oldPosition, int newPosition)
        {
            int nAffected = -1;
            try
            {
                using (SqlConnection connection = new SqlConnection(cnxn))
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "AdjustColumnNumbers";
                        command.CommandType = CommandType.StoredProcedure;

                        SqlParameter parameter = new SqlParameter();
                        parameter.ParameterName = "@ConfigID";
                        parameter.SqlDbType = SqlDbType.Int;
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = configID;
                        command.Parameters.Add(parameter);

                        parameter = new SqlParameter();
                        parameter.ParameterName = "@TableName";
                        parameter.SqlDbType = SqlDbType.VarChar;
                        parameter.Size = 64;
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = tableName;
                        command.Parameters.Add(parameter);

                        parameter = new SqlParameter();
                        parameter.ParameterName = "@OldPosition";
                        parameter.SqlDbType = SqlDbType.Int;
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = oldPosition;
                        command.Parameters.Add(parameter);

                        parameter = new SqlParameter();
                        parameter.ParameterName = "@NewPosition";
                        parameter.SqlDbType = SqlDbType.Int;
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = newPosition;
                        command.Parameters.Add(parameter);

                        command.Connection.Open();
                        nAffected = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("DBException: " + ex.Message + " (Stored Procedure: Adjust Column Numbers)");
            }
            return nAffected;
        }

        public static int ExecuteSPAdjustMMAccountID(string cnxn, string tableName, int fieldLength)
        {
            int nAffected = -1;
            try
            {
                using (SqlConnection connection = new SqlConnection(cnxn))
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "AdjustMMAccountID";
                        command.CommandType = CommandType.StoredProcedure;

                        SqlParameter parameter = new SqlParameter();
                        parameter.ParameterName = "@TableName";
                        parameter.SqlDbType = SqlDbType.VarChar;
                        parameter.Size = 64;
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = tableName;
                        command.Parameters.Add(parameter);

                        parameter = new SqlParameter();
                        parameter.ParameterName = "@FieldLength";
                        parameter.SqlDbType = SqlDbType.Int;
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = fieldLength;
                        command.Parameters.Add(parameter);

                        command.Connection.Open();
                        nAffected = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("DBException: " + ex.Message + " (Stored Procedure: Adjust MMAccountID)");
            }
            return nAffected;
        }

        public static int ExecuteSPResetMigrationBatch(string cnxn, int batchID, string jobName, int caseLevel, int errorsOnly)
        {
            int nAffected = -1;
            try
            {
                using (SqlConnection connection = new SqlConnection(cnxn))
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "ResetMigrationBatch";
                        command.CommandType = CommandType.StoredProcedure;

                        SqlParameter parameter = new SqlParameter();
                        parameter.ParameterName = "@BatchID";
                        parameter.SqlDbType = SqlDbType.Int;
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = batchID;
                        command.Parameters.Add(parameter);

                        parameter = new SqlParameter();
                        parameter.ParameterName = "@JobName";
                        parameter.SqlDbType = SqlDbType.VarChar;
                        parameter.Size = 64;
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = jobName;
                        command.Parameters.Add(parameter);

                        parameter = new SqlParameter();
                        parameter.ParameterName = "@CaseLevel";
                        parameter.SqlDbType = SqlDbType.Int;
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = caseLevel;
                        command.Parameters.Add(parameter);

                        parameter = new SqlParameter();
                        parameter.ParameterName = "@ErrorsOnly";
                        parameter.SqlDbType = SqlDbType.Int;
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = errorsOnly;
                        command.Parameters.Add(parameter);

                        command.Connection.Open();
                        nAffected = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("DBException: " + ex.Message + " (Stored Procedure: Reset Migration Batch)");
            }
            return nAffected;
        }
    }
}
