﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml;

namespace PreIngest
{
    public enum ConversionResult
    {
        CONV_CANCELED,
        CONV_COMPLETE,
        CONV_DONE,
        CONV_ERRORED,
        CONV_NEW
    }

    public enum EntityStatus
    {
        DISCARD,
        ERROR,
        GOOD
    }

    public enum AnnotatedObjectType
    {
        CASE,
        DOCUMENT,
        TASK,
        NONE
    }

    public enum AnnotationTargetObjectType
    {
        DOCUMENT,
        ANNOTATION,
        NONE
    }

    public partial class Converter
    {
        Context contxt = new Context();

        int jobID = 0;
        int batchDefnID = 0;
        string sessionID = string.Empty;
        string batchID = string.Empty;
        int maxBatchCount = -1;
        Boolean doTrace = false;
		int leadToolsJPEGPasses = -1;				// LeadTools default
		int leadToolsJPEGQualityFactor = 255;		// Most compression

		public Converter(string cnxn, int job, int batchDefn, int maxBatches, Boolean debugOption, int leadTools_JPEG_Passes, int leadTools_JPEG_QualityFactor, int leadTools_ColorFormat, int leadTools_BWFormat)
        {
            jobID = job;
            batchDefnID = batchDefn;
            maxBatchCount = maxBatches;
            doTrace = debugOption;

            contxt.Initialize(cnxn, job, debugOption, leadTools_JPEG_Passes, leadTools_JPEG_QualityFactor, leadTools_ColorFormat, leadTools_BWFormat);
        }

        public void UnlockLeadTools()
        {
            string licFile = "Leadtools175.lic";
            const string LEADTOOLSKey = "Dw1+3ABGdm8pRLAZ6uBVCGLuEjkllSdsipA2tZEKmuJLVon90zMchbO8SIgTl1fFD5xiwkP38EiJR0akHaicw6ZmIpmiCLZDQGh3CeXTFcVXJ7D1lHfatWe7yaT8BCwTPf8FpVGxaVxhGweP+EJWDVRFxsmZYZKXTYa8YAcpkD4UJUzWb/VdYhC6HOxVpePOs+V8JEKD4OByYfHImmt1uwj85PCJlddaukjOO9P9M5op072QPbowh8pLfYVJkokWDAryAzVxAWk2jg2Hhu5MoFni8DwK/ZPmAuh5fPX3O8tDeqNDhzMHbAX3xqOvUAlL/pQpPtaYHuR/0CveMWtqc4j0IXYSN+4O3qfFy6da39LfliJvFhfKeMqP+lvVpueCsruHi6TOl7FWNv7Fjs3ydNZ6vyLTw9lBLPcEG+L7n1SA/b1sYDc0ojXp5HPUTlcWdZRITuf0xhAlbVmhmxYIzg==";
            PreIngest.LEADTOOLS.LeadToolsHelper.Unlock(this.GetType().Assembly, LEADTOOLSKey, licFile); 
        }

        public string PerformConversion(BackgroundWorker bw)
        {
            contxt.backgroundWorker = bw;

            if(doTrace) DisplayMessage("Converter: Starting Conversion");
            contxt.casePropertyEvaluator = MMFactory.MMFactory.Instance.GetCasePropertyEvaluator();
            contxt.docPropertyEvaluator = MMFactory.MMFactory.Instance.GetDocumentPropertyEvaluator();
            contxt.annPropertyEvaluator = MMFactory.MMFactory.Instance.GetAnnotationPropertyEvaluator();
            contxt.taskPropertyEvaluator = MMFactory.MMFactory.Instance.GetTaskPropertyEvaluator();

            contxt.caseCommentReader = MMFactory.MMFactory.Instance.GetCaseCommentReader();
            contxt.docCommentReader = MMFactory.MMFactory.Instance.GetDocumentCommentReader();
            contxt.taskCommentReader = MMFactory.MMFactory.Instance.GetTaskCommentReader();

            contxt.caseDocGenerator = MMFactory.MMFactory.Instance.GetCaseDocGenerator();
            contxt.docDocGenerator = MMFactory.MMFactory.Instance.GetDocumentDocGenerator();
            contxt.taskDocGenerator = MMFactory.MMFactory.Instance.GetTaskDocGenerator();

            contxt.caseTypeSelector = MMFactory.MMFactory.Instance.GetCaseTypeSelector();
            contxt.docTypeSelector = MMFactory.MMFactory.Instance.GetDocumentTypeSelector();
            contxt.annTypeSelector = MMFactory.MMFactory.Instance.GetAnnotationTypeSelector();
            contxt.taskTypeSelector = MMFactory.MMFactory.Instance.GetTaskTypeSelector();

            contxt.annotationReader = MMFactory.MMFactory.Instance.GetAnnotationReader();
            if (doTrace) DisplayMessage("Converter: Customization Points Initialized");
  
            string status = string.Empty;
            ConversionResult result = ConversionResult.CONV_NEW;

            StartNewSession();
            
            int batchCount = 0;
            
            do
            {
                if (bw.CancellationPending)
                {
                    result = ConversionResult.CONV_CANCELED;
                    break;
                }
                DisplayMessage("Processing Batch: " + (batchCount + 1).ToString() + " of current session");
                try
                {
                    result = ConvertBatch();
                    batchCount++;
                }
                catch(Exception ex)
                {
                    DisplayMessage("Exception during batch conversion: " + ex.Message);
                }
            }
            while ((result != ConversionResult.CONV_COMPLETE) && ((maxBatchCount == -1) || (batchCount < maxBatchCount)));

            switch (result)
            {
                case ConversionResult.CONV_CANCELED:
                    status = CancelSession();
                    break;

                default:
                    status = EndSession();
                    DisplayMessage("Last batch processed; no more batches to process using this batch definition.");
                    DisplayMessage("Batches Processed to Completion: " + batchCount.ToString());
                    break;
            }
            return status;
        }
        
        protected void DisplayMessage(String message)
        {
            contxt.backgroundWorker.ReportProgress(0, message);
        }

        protected void StartNewSession()
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.FetchInsertedRec(contxt.dbConnection, "INSERT INTO MgrSessions (UserName, WorkStation) OUTPUT inserted.* VALUES ('" + Environment.UserName + "', '" + Environment.MachineName + "')");
            sessionID = xmlDoc.SelectNodes("//row")[0].Attributes["ID"].Value;

            DisplayMessage("Session Started; Session ID: " + sessionID);
        }

        protected string EndSession()
        {
            DBAdapter.DBAdapter.UpdateDB(contxt.dbConnection, "UPDATE MgrSessions SET EndTime = GETDATE(), Status = 'COMPLETED' WHERE ID = " + sessionID);

            return "Session Complete";
        }

        protected string AbortSession()
        {
            DBAdapter.DBAdapter.UpdateDB(contxt.dbConnection, "UPDATE MgrSessions SET EndTime = GETDATE(), Status = 'ERRORED' WHERE ID = " + sessionID);

            return "Session Complete";
        }

        protected string CancelSession()
        {
            DBAdapter.DBAdapter.UpdateDB(contxt.dbConnection, "UPDATE MgrSessions SET EndTime = GETDATE(), Status = 'CANCELED' WHERE ID = " + sessionID);

            return "Session Canceled";
        }

        protected ConversionResult ConvertBatch()
        {
            ConversionResult result;

            contxt.batchID = StartNewBatch();
           
            Batch b = new Batch(contxt);

            try
            {
                result = b.PerformConversion();
            }
            catch (Exception ex)
            {
                DisplayMessage("Exception::Batch: " + batchID.ToString() + ": " + ex.Message);
                result = ConversionResult.CONV_ERRORED;
            }
            return result;
        }

        protected string StartNewBatch()
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.FetchInsertedRec(contxt.dbConnection, "INSERT INTO MgrBatches (SessionID, JobID, BatchDefnID, UserName, WorkStation) OUTPUT inserted.* VALUES (" + sessionID + ", " + jobID + ", " + batchDefnID + ", '" + Environment.UserName + "', '" + Environment.MachineName + "')");
            return xmlDoc.SelectNodes("//row")[0].Attributes["ID"].Value;
        }
    }
}
