﻿using System;

namespace PreIngest
{
    class TargetTaskProperty : TargetProperty
    {
        public TargetTaskProperty(Context c, string n, string v, string t) : base(c, n, v, t)
        {
        }

        public override void Serialize(string taskID)
        {
            if (!IsValueNull())
            {
                DBAdapter.DBAdapter.AddToDB(contxt.dbConnection, "INSERT INTO " + contxt.jobName + "_CETaskProperties (TaskID, Name, Value, Type) VALUES ( " + taskID + ", '" + Name + "', '" + Value + "', '" + DataType + "')");
            }
        }
    } 
}
