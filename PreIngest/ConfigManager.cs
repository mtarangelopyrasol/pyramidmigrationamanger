﻿using Leadtools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace PreIngest
{
    public class ConfigManager
    {
        string dbCnxn = string.Empty;
        string configID = string.Empty;

        protected Dictionary<string, ConfigProperties> CaseTypes = null;
        protected Dictionary<string, string> DefaultDocClasses = null;
        protected Dictionary<string, ConfigProperties> DocumentClasses = null;
        protected Dictionary<string, ConfigProperties> AnnotationClasses = null;
        protected Dictionary<string, string> Identifiers = null;
        protected Dictionary<string, ConfigTaskTypes> TaskTypes = null;
        protected Dictionary<string, ConfigFolders> Folders = null;
        protected ConversionMaps convMaps = null;
        protected Dictionary<string, List<string>> MimeTypeExt = null;
        protected Dictionary<string, FileExtMaps> FileExt = null;
        protected Dictionary<string, LeadFmtMaps> LeadFmt = null;
        protected Dictionary<string, int> MinBpp = null;
        protected PromotionRules Promotions = null;
        protected string[] MarshalingIdentifier = null;
        protected Dictionary<string, FontMap> FontAdjustment = null;

        public ConfigManager(string cnxn, string cId)
        {
            dbCnxn = cnxn;
            configID = cId;
            InitializeConfig();
        }

        protected void InitializeConfig()
        {
            InitializeCaseTypes();
            InitializeCaseFolders();
            InitializeDocumentClasses();
            InitializeAnnotationClasses();
            InitializeTasks();
            InitializeIdentifiers();
            InitializeMimeTypeExt();
            InitializeFileExtension();
            InitializeLeadFormats();
            InitializeMinBpp();
            InitializePromotions();
            InitializeFontMaps();
        }

        protected void InitializeCaseTypes()
        {
            if (CaseTypes == null)
            {
                CaseTypes = new Dictionary<string, ConfigProperties>();
                DefaultDocClasses = new Dictionary<string, string>();

                string sqlCaseTypes = "SELECT * FROM CfgCaseTypes WHERE ConfigID = " + configID;
                XmlDocument xmlCaseTypes = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sqlCaseTypes);

                foreach (XmlElement rowCaseTypes in xmlCaseTypes.SelectNodes("//row"))
                {
                    ConfigProperties cps = new ConfigProperties();
                    CaseTypes.Add(rowCaseTypes.GetAttribute("CaseName"), cps);
                    DefaultDocClasses.Add(rowCaseTypes.GetAttribute("CaseName"), rowCaseTypes.GetAttribute("DefaultDocClass"));

                    string sqlCaseProperties = "SELECT * FROM CfgCaseProperties WHERE CaseTypeId = " + rowCaseTypes.GetAttribute("ID");
                    XmlDocument xmlCaseProperties = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sqlCaseProperties);

                    foreach (XmlElement rowCaseProperties in xmlCaseProperties.SelectNodes("//row"))
                    {
                        ConfigProperty cp = new ConfigProperty(rowCaseProperties.GetAttribute("Name"), rowCaseProperties);
                        
                        if (cp.IsMapped())
                        {
                            string sqlMap = "SELECT MgrMappingValues.OldValue, MgrMappingValues.NewValue FROM MgrMappingValues, MgrMappings WHERE MgrMappingValues.MapID = MgrMappings.ID and MgrMappings.MappingName = '" + cp.GetMappingName() + "'";
                            XmlDocument xmlMapping = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sqlMap);
                            cp.LoadValueMapping(xmlMapping);
                        }
                        cps.AddProperty(cp);
                    }
                }
            }
        }

        protected void InitializeCaseFolders()
        {
            if (Folders == null)
            {
                Folders = new Dictionary<string, ConfigFolders>();

                string sql = "SELECT CfgCaseFolders.FolderName, CfgCaseFolders.Parent, CfgCaseTypes.CaseName FROM CfgCaseFolders, CfgCaseTypes WHERE cfgCaseTypes.ConfigID = " + configID + " AND CfgCaseFolders.CaseTypeID = CfgCaseTypes.ID";
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);

                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    string caseTypeName = rowNode.GetAttribute("CaseName");
                    if (!Folders.ContainsKey(caseTypeName))
                    {
                        ConfigFolders cfs = new ConfigFolders();
                        Folders.Add(caseTypeName, cfs);
                    }
                    ConfigFolder cf = new ConfigFolder(rowNode.GetAttribute("FolderName"), rowNode.GetAttribute("Parent"));
                    Folders[caseTypeName].AddStaticFolder(cf);
                }
            }
        }

        protected void InitializeDocumentClasses()
        {
            if (DocumentClasses == null)
            {
                DocumentClasses = new Dictionary<string, ConfigProperties>();

                string sqlDocClass = "SELECT * FROM CfgDocClasses WHERE ConfigID = " + configID;
                XmlDocument xmlDocClass = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sqlDocClass);

                foreach (XmlElement rowDocClass in xmlDocClass.SelectNodes("//row"))
                {
                    ConfigProperties cps = new ConfigProperties();
                    DocumentClasses.Add(rowDocClass.GetAttribute("DocClassName"), cps);

                    string sqlDocClassProperties = "SELECT * FROM CfgDocClassProperties WHERE DocClassID = " + rowDocClass.GetAttribute("ID");
                    XmlDocument xmlDocClassProperties = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sqlDocClassProperties);

                    foreach (XmlElement rowDocClassProperties in xmlDocClassProperties.SelectNodes("//row"))
                    {
                        ConfigProperty cp = new ConfigProperty(rowDocClassProperties.GetAttribute("Name"), rowDocClassProperties);
                        if (cp.IsMapped())
                        {
                            string sqlMap = "SELECT MgrMappingValues.OldValue, MgrMappingValues.NewValue FROM MgrMappingValues, MgrMappings WHERE MgrMappingValues.MapID = MgrMappings.ID and MgrMappings.MappingName = '" + cp.GetMappingName() + "'";
                            XmlDocument xmlMapping = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sqlMap);
                            cp.LoadValueMapping(xmlMapping);
                        }
                        cps.AddProperty(cp);
                    }
                }
            }
        }

        protected void InitializeAnnotationClasses()
        {
            if (AnnotationClasses == null)
            {
                AnnotationClasses = new Dictionary<string, ConfigProperties>();

                string sqlAnnClass = "SELECT * FROM CfgAnnClasses WHERE ConfigID = " + configID;
                XmlDocument xmlAnnClass = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sqlAnnClass);

                foreach (XmlElement annDocClass in xmlAnnClass.SelectNodes("//row"))
                {
                    ConfigProperties cps = new ConfigProperties();
                    AnnotationClasses.Add(annDocClass.GetAttribute("AnnClassName"), cps);

                    string sqlAnnClassProperties = "SELECT * FROM CfgAnnClassProperties WHERE AnnClassID = " + annDocClass.GetAttribute("ID");
                    XmlDocument xmlAnnClassProperties = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sqlAnnClassProperties);

                    foreach (XmlElement rowAnnClassProperties in xmlAnnClassProperties.SelectNodes("//row"))
                    {
                        ConfigProperty cp = new ConfigProperty(rowAnnClassProperties.GetAttribute("Name"), rowAnnClassProperties);
                        if (cp.IsMapped())
                        {
                            string sqlMap = "SELECT MgrMappingValues.OldValue, MgrMappingValues.NewValue FROM MgrMappingValues, MgrMappings WHERE MgrMappingValues.MapID = MgrMappings.ID and MgrMappings.MappingName = '" + cp.GetMappingName() + "'";
                            XmlDocument xmlMapping = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sqlMap);
                            cp.LoadValueMapping(xmlMapping);
                        }
                        cps.AddProperty(cp);
                    }
                }
            }
        }

        protected void InitializeTasks()
        {
            if (TaskTypes == null)
            {
                TaskTypes = new Dictionary<string, ConfigTaskTypes>();

                string sql = "SELECT tt.* FROM CfgTaskTypes tt, CfgCaseTypes ct WHERE tt.CaseID = ct.ID AND ct.ConfigID = " + configID;
                XmlDocument xmlTaskTypes = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);

                foreach (XmlElement rowTaskTypes in xmlTaskTypes.SelectNodes("//row"))
                {
                    string caseID = rowTaskTypes.GetAttribute("CaseID");

                    if (!(TaskTypes.ContainsKey(caseID)))
                    {
                        ConfigTaskTypes ctts = new ConfigTaskTypes();
                        TaskTypes.Add(caseID, ctts);
                    }

                    string sqlTaskProperties = "SELECT * FROM CfgTaskProperties WHERE TaskTypeId = " + rowTaskTypes.GetAttribute("ID");
                    XmlDocument xmlTaskProperties = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sqlTaskProperties);

                    ConfigTaskType ctt = new ConfigTaskType(rowTaskTypes.GetAttribute("TaskName"), rowTaskTypes, xmlTaskProperties, dbCnxn);
                    TaskTypes[caseID].AddTaskType(ctt);
                }
            }
        }

        protected void InitializeIdentifiers()
        {
            if (Identifiers == null)
            {
                Identifiers = new Dictionary<string, string>();

                string sql = "SELECT * FROM CfgIdentifiers WHERE ConfigID = " + configID;
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);

                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    Identifiers.Add(rowNode.GetAttribute("IdentifierName").Trim(), rowNode.GetAttribute("FieldName").Trim());
                }
                MarshalingIdentifier = Identifiers["DocMarshalingIdentifier"].Split('+');
            }
        }

        public void InitializeConversions(string convDefnID)
        {
            if (convMaps == null)
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn,
                                "SELECT MarshalToSingleDoc FROM MgrConvDefns WHERE ID = " + convDefnID);
                string single = xml.SelectNodes("//row")[0].Attributes["MarshalToSingleDoc"].Value;
                Boolean toSingle = (single == "1") ? true : false;

                convMaps = new ConversionMaps(toSingle);

                if (convDefnID.Length > 0)
                {
                    XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbCnxn,
                                "SELECT MgrConvMaps.SourceFormat, MgrConvMaps.TargetFormat, MgrConvMaps.Exempt FROM MgrConvMaps, MgrConversions WHERE MgrConversions.ConvDefnID = " + convDefnID + " AND MgrConversions.ConvMapID = MgrConvMaps.ID");

                    foreach (XmlElement rowNode in xmlDoc.SelectNodes("//row"))
                    {
                        convMaps.Add(rowNode.GetAttribute("SourceFormat"), rowNode.GetAttribute("TargetFormat"), rowNode.GetAttribute("Exempt"));
                    }
                }
            }
        }

        protected void InitializeMimeTypeExt()
        {
            if (MimeTypeExt == null)
            {
                MimeTypeExt = new Dictionary<string, List<string>>();
                
                string sqlMimeTypeExt = "SELECT * FROM MgrMimeTypeExt";
                XmlDocument xmlMimeTypeExt = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sqlMimeTypeExt);

                foreach (XmlElement rowMimeTypeExt in xmlMimeTypeExt.SelectNodes("//row"))
                {
                    string mimeType = rowMimeTypeExt.GetAttribute("P8MimeType");
                    string temp = rowMimeTypeExt.GetAttribute("Extension").Trim();
                    if (temp.Length > 0)
                    {
                        string[] extensions = temp.Split(' ');

                        foreach (string ext in extensions)
                        {
                            if (!MimeTypeExt.Keys.Contains(ext))
                            {
                                MimeTypeExt.Add(ext, new List<string>());
                            }
                            MimeTypeExt[ext].Add(mimeType);
                        }
                    }
                }
            }
        }

        protected void InitializeFileExtension()
        {
            if (FileExt == null)
            {
                FileExt = new Dictionary<string, FileExtMaps>();

                string sql = "SELECT * FROM MgrFileExtension";
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);

                foreach (XmlElement row in xml.SelectNodes("//row"))
                {
                    string compress = row.GetAttribute("Compression").Trim().ToUpper();
                    FileExtMaps fems = null;
                    if (FileExt.ContainsKey(compress))
                    {
                        fems = FileExt[compress];
                    }
                    else
                    {
                        fems = new FileExtMaps();
                        FileExt.Add(compress, fems);
                    }
                    fems.AddFileExtMap(row.GetAttribute("Format").Trim(), row.GetAttribute("Extension").Trim());  
                }
            }
        }

        protected void InitializeLeadFormats()
        {
            if (LeadFmt == null)
            {
                LeadFmt = new Dictionary<string, LeadFmtMaps>();

                string sql = "SELECT * FROM MgrLeadFormat";
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);

                foreach (XmlElement row in xml.SelectNodes("//row"))
                {
                    string compress = row.GetAttribute("Compression").Trim().ToUpper();
                    LeadFmtMaps lfms = null;
                    if (LeadFmt.ContainsKey(compress))
                    {
                        lfms = LeadFmt[compress];
                    }
                    else
                    {
                        lfms = new LeadFmtMaps();
                        LeadFmt.Add(compress, lfms);
                    }
                    lfms.AddLeadFmtMap(row.GetAttribute("Format").Trim(), row.GetAttribute("LeadFormat").Trim());
                }
            }
        }

        protected void InitializeMinBpp()
        {
            if (MinBpp == null)
            {
                MinBpp = new Dictionary<string, int>();

                string sql = "SELECT * FROM MgrMinBitsPerPixel";
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);

                foreach (XmlElement row in xml.SelectNodes("//row"))
                {
                    MinBpp.Add(row.GetAttribute("LeadFormat").Trim(), Convert.ToInt32(row.GetAttribute("MinBpp").Trim()));
                }
            }
        }

        protected void InitializePromotions()
        {
            if (Promotions == null)
            {
                Promotions = new PromotionRules();

                string sql = "SELECT * FROM PromotionRules";
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);

                foreach (XmlElement row in xml.SelectNodes("//row"))
                {
                    Promotions.AddRule(row.GetAttribute("DocFormat").Trim(), row.GetAttribute("PageFormat").Trim(), row.GetAttribute("PageCompression").Trim(), row.GetAttribute("RuleResult").Trim());
                }
            }
        }

        protected void InitializeFontMaps()
        {
            if (FontAdjustment == null)
            {
                FontAdjustment = new Dictionary<string, FontMap>();
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT * FROM MgrFontMaps");

                foreach (XmlElement row in xml.SelectNodes("//row"))
                {
                    FontMap f = new FontMap();
                    f.FontName = row.GetAttribute("FontName").Trim().ToUpper();
                    f.InputSize = row.GetAttribute("InputSize").Trim();
                    f.OutputSize = row.GetAttribute("OutputSize").Trim();
                    f.HeightAdjustment = Convert.ToSingle(row.GetAttribute("HeightFactor").Trim());
                    f.WidthAdjustment = Convert.ToSingle(row.GetAttribute("WidthFactor").Trim());
                    FontAdjustment.Add(f.FontName + "+" + f.InputSize, f);
                }
            }
        }

        public string GetCaseType()
        {
            return (CaseTypes.Keys.Count == 1) ? CaseTypes.Keys.ElementAt(0) : string.Empty;
        }

        public Boolean IsCaseTypeDefined(string caseTypeName)
        {
            return (CaseTypes.ContainsKey(caseTypeName)) ? true : false;
        }

        public string GetDefaultDocClass(string caseTypeName)
        {
            return (DefaultDocClasses.ContainsKey(caseTypeName)) ? DefaultDocClasses[caseTypeName] : string.Empty;
        }

        public string GetCaseTypeIdentifier()
        {
            return Identifiers["CaseTypeIdentifier"];
        }

        public List<ConfigProperty> GetCaseProperties(string caseTypeName)
        {
            return (CaseTypes.ContainsKey(caseTypeName)) ? CaseTypes[caseTypeName].GetProperties() : new List<ConfigProperty>();
        }

        public List<ConfigFolder> GetCaseFolders(string caseTypeName)
        {
            return (Folders.ContainsKey(caseTypeName)) ? Folders[caseTypeName].GetFolders() : new List<ConfigFolder>();
        }

        public string GetCaseAccountIdentifier()
        {
            return Identifiers["CaseAccountIdentifier"];
        }

        public string GetDocAccountIdentifier()
        {
            return Identifiers["DocAccountIdentifier"];
        }

        public string GetDocumentClass()
        {
            return (DocumentClasses.Keys.Count == 1) ? DocumentClasses.Keys.ElementAt(0) : string.Empty;
        }

        public Boolean IsDocumentClassDefined(string docClassName)
        {
            return (DocumentClasses.ContainsKey(docClassName)) ? true : false;
        }

        public string GetAnnotationClass()
        {
            return (AnnotationClasses.Keys.Count == 1) ? AnnotationClasses.Keys.ElementAt(0) : string.Empty;
        }

        public Boolean IsAnnotationClassDefined(string annClassName)
        {
            return (AnnotationClasses.ContainsKey(annClassName)) ? true : false;
        }

        public string GetTaskType()
        {
            return (TaskTypes.Keys.Count == 1) ? TaskTypes.Keys.ElementAt(0) : string.Empty;
        }

        

        public string GetDocClassIdentifier()
        {
            return Identifiers["DocClassIdentifier"];
        }

        public string GetDocumentIdentifier()
        {
            return Identifiers["DocumentIdentifier"];
        }

        public string[] GetDocMarshalingIdentifier()
        {
            return MarshalingIdentifier;
        }

        public string GetDocTypeIdentifier()
        {
            return Identifiers["DocTypeIdentifier"];
        }

        public string GetDocDateIdentifier()
        {
            return Identifiers["DocDateIdentifier"];
        }

        public string GetDocTraceIdentifier()
        {
            return Identifiers["DocTraceIdentifier"];
        }

        public string GetPageIdentifier()
        {
            return Identifiers["DocPageIdentifier"];
        }

        public string GetPageNumberIdentifier()
        {
            return Identifiers["DocPageNumberIdentifier"];
        }

        public string GetFileNameIdentifier()
        {
            return Identifiers["DocPageFileNameIdentifier"];
        }

        public string GetMimeTypeIdentifier()
        {
            return Identifiers["DocMimeTypeIdentifier"];
        }

        public string GetContainmentNameIdentifier()
        {
            return Identifiers["DocContNameIdentifier"];
        }

        public string GetLegacyDocIdentifier()
        {
            return Identifiers["DocumentIdentifier"];
        }

        public string GetPageStatusIdentifier()
        {
            return Identifiers["DocPageStatusIdentifier"];
        }

        public string GetDocFolderIdentifier()
        {
            return Identifiers["DocFolderIdentifier"];
        }

        public string GetDocRotationIdentifier()
        {
            return Identifiers["DocRotationIdentifier"];
        }

        public string GetDocRotationDirectionIdentifier()
        {
            return Identifiers["DocRotationDirectionIdentifier"];
        }

        public string GetDocPreAnnRotationIdentifier()
        {
            return Identifiers["DocPreAnnRotationIdentifier"];
        }

        public string GetDocPreAnnRotationDirectionIdentifier()
        {
            return Identifiers["DocPreAnnRotationDirectionIdentifier"];
        }

        public List<ConfigProperty> GetDocumentProperties(string docClassName)
        {
            return (DocumentClasses.ContainsKey(docClassName)) ? DocumentClasses[docClassName].GetProperties() : new List<ConfigProperty>();
        }

        public List<ConfigProperty> GetAnnotationProperties(string annClassName)
        {
            return (AnnotationClasses.ContainsKey(annClassName)) ? AnnotationClasses[annClassName].GetProperties() : new List<ConfigProperty>();
        }

        public Boolean IsTaskTypeDefined(string caseID, string taskTypeName)
        {
            if (TaskTypes.ContainsKey(caseID))
            {
                ConfigTaskTypes ctts = TaskTypes[caseID];
                ConfigTaskType ctt = ctts.GetTaskType(taskTypeName);
                if (ctt != null)
                {
                    return true;
                }
            }
            return false;
        }

        public ConfigProperties GetTaskProperties(string caseID, string taskTypeName)
        {
            if (TaskTypes.ContainsKey(caseID))
            {
                ConfigTaskTypes ctts = TaskTypes[caseID];
                ConfigTaskType ctt = ctts.GetTaskType(taskTypeName);
                if (ctt != null)
                {
                    return ctt.GetProperties();
                }
                else
                {
                    throw new Exception("Unknown Task: " + taskTypeName);
                }
            }
            else
            {
                throw new Exception("Unknown Case ID: " + caseID);
            }
        }

        public Boolean IsDocInitiated(string caseID, string taskTypeName)
        {
            if (TaskTypes.ContainsKey(caseID))
            {
                ConfigTaskTypes ctts = TaskTypes[caseID];
                ConfigTaskType ctt = ctts.GetTaskType(taskTypeName);
                if (ctt != null)
                {
                    return ctt.IsDocInitiated();
                }
                else
                {
                    throw new Exception("Unknown Task: " + taskTypeName);
                }
            }
            else
            {
                throw new Exception("Unknown Case ID: " + caseID);
            }
        }

        public string GetInitiatingDocClass(string caseID, string taskTypeName)
        {
            if (TaskTypes.ContainsKey(caseID))
            {
                ConfigTaskTypes ctts = TaskTypes[caseID];
                ConfigTaskType ctt = ctts.GetTaskType(taskTypeName);
                if (ctt != null)
                {
                    return ctt.GetInitiatingDocClass();
                }
                else
                {
                    throw new Exception("Unknown Task: " + taskTypeName);
                }
            }
            else
            {
                throw new Exception("Unknown Case ID: " + caseID);
            }
        }

        public string GetInitiatingDocIdSource(string caseID, string taskTypeName)
        {
            if (TaskTypes.ContainsKey(caseID))
            {
                ConfigTaskTypes ctts = TaskTypes[caseID];
                ConfigTaskType ctt = ctts.GetTaskType(taskTypeName);
                if (ctt != null)
                {
                    return ctt.GetInitiatingDocIdSource();
                }
                else
                {
                    throw new Exception("Unknown Task: " + taskTypeName);
                }
            }
            else
            {
                throw new Exception("Unknown Case ID: " + caseID);
            }
        }

        public string GetTargetCaseIdentifier(string caseTypeName)
        {
            string sourceID = GetCaseAccountIdentifier();
            string targetID = string.Empty;

            ConfigProperties cps = CaseTypes[caseTypeName];

            foreach(ConfigProperty cp in cps.GetProperties())
            {
                if(cp.GetSource() == sourceID)
                {
                    targetID = cp.Name;
                    break;
                }
            }
            return targetID;
        }

        public Boolean IsMarshalingExempt(string sourceFormat)
        {
            return convMaps.IsExempt(sourceFormat);
        }

        public Boolean IsConvertableFormat(string pageFormat, string documentFormat)
        {
            return convMaps.IsConvertableFormat(pageFormat, documentFormat);
        }

        public string GetTargetFormat(string pageFormat)
        {
            return convMaps.GetTargetFormat(pageFormat);
        }

        public string GetTaskAccountIdentifier()
        {
            return Identifiers["TaskAccountIdentifier"];
        }

        public string GetTaskTypeIdentifier()
        {
            return Identifiers["TaskTypeIdentifier"];
        }

        public string GetTaskDescIdentifier()
        {
            return Identifiers["TaskDescriptionIdentifier"];
        }

        public string GetAnnotationAccountIdentifier()
        {
            return Identifiers["AnnotationAccountIdentifier"];
        }

        public string GetAnnotationPageIdentifier()
        {
            return Identifiers["AnnotationPageIdentifier"];
        }

        public string GetAnnotatedObjectIdentifier()
        {
            return Identifiers["AnnotatedObjectIdentifier"];
        }

        public string GetAnnotatedObjectTypeIdentifier()
        {
            return Identifiers["AnnotatedObjTypeIdentifier"];
        }

        public string GetAnnotationClassIdentifier()
        {
            return Identifiers["AnnotationClassIdentifier"];
        }

        public string GetAnnotationTargetObjectTypeIdentifier()
        {
            return Identifiers["AnnotationTgtObjTypeIdentifier"];
        }

        public string GetAnnotationFileNameIdentifier()
        {
            return Identifiers["AnnotationFileNameIdentifier"];
        }

        public bool GetAlwaysCreateFoldersIdentifier()
        {
            try
            {
                string alwaysCreateFolders = Identifiers["AlwaysCreateFolders"];
                bool boolAlwaysCreateFolders = false;

                if (bool.TryParse(alwaysCreateFolders, out boolAlwaysCreateFolders)){
                    //if it parsed, then it is what it is, so return it ...
                    return boolAlwaysCreateFolders;
                }
                else
                {
                    //if it did not parse it is unknown, so do not create the folders ...
                    return false;
                }

            }catch(Exception error)
            {
                //we really don't care how it errored, it is not configured correctly so it is false
                return false;
            }
        }

        public string DetermineP8MimeType(string extension)
        {
            string result = string.Empty;

            string ext = extension.ToLower();
            if (MimeTypeExt.Keys.Contains(ext))
            {
                result = MimeTypeExt[ext][0];
            }
            else
            {
                int i = 0;
                string temp = ext.Substring(0, ext.Length - i++);
                while (temp.Length > 0)
                {
                    temp += "*";
                    if (MimeTypeExt.Keys.Contains(temp))
                    {
                        result = MimeTypeExt[temp][0];
                        break;
                    }
                    temp = ext.Substring(0, ext.Length - i++);
                }
            }
            return result;
        }

        public string DetermineExtension(string format, string compression)
        {
            if (compression == "" || compression == null || compression.ToUpper() == "NONE" || compression.ToUpper() == "UNKNOWN")
            {
                compression = "NONE";
            }
            else
            {
                compression = compression.ToUpper();
            }
            if(!FileExt.ContainsKey(compression))
            {
                throw new Exception("DetermineExtension: Compression '" + compression + "' is unrecognized.");
            }
            FileExtMaps fems = FileExt[compression];
            string ext = fems.GetExtension(format.ToUpper());
            if (ext.Length == 0)
            {
                throw new Exception("DetermineExtension: Format: " + format + " Compression: " + compression + " unknown");
            }
            return ext;
        }

        public string DetermineLeadFormat(string format, string compression)
        {
            if (compression == "" || compression == null || compression.ToUpper() == "NONE" || compression.ToUpper() == "UNKNOWN")
            {
                if (format == "" || format == null)
                {
                    throw new Exception("Converter.DetermineLeadFormat: Both compression and format cannot be null");
                }
                compression = "NONE";
            }
            else
            {
                compression = compression.ToUpper();
            }
            format = format.ToUpper();
            if (!LeadFmt.ContainsKey(compression))
            {
                throw new Exception("DetermineLeadFormat: Compression '" + compression + "' is unrecognized.");
            }
            LeadFmtMaps lfms = LeadFmt[compression];
            string fmtLead = lfms.GetLeadFormat(format);
            if (fmtLead.Length == 0)
            {
                throw new Exception("DetermineLeadFormat: Format: " + format + " Compression: " + compression + " unknown");
            }
            return fmtLead;
        }

        public int DetermineMinimumBpp(RasterImageFormat TargetFormat)
        {
            string fmtLead = TargetFormat.ToString().ToUpper();
            if (MinBpp.ContainsKey(fmtLead))
            {
                return MinBpp[fmtLead];
            }
            else
            {
                throw new Exception("DetermineMinimumBpp: Format '" + TargetFormat.ToString() + "' is unrecognized.");
            }
        }

        public RasterImageFormat PromoteLeadFormat(RasterImageFormat docFormat, RasterImageFormat pageFormat, string pageCompression)
        {
            return Promotions.GetPromotedFormat(docFormat, pageFormat, pageCompression);
        }

        public Boolean NeedToMarshalToSingleDoc()
        {
            return convMaps.NeedToMarshalToSingleDoc();
        }

        public Boolean IsADocumentClass(string name)
        {
            return DocumentClasses.ContainsKey(name.Trim());
        }

        public Boolean IsAnAnnotationClass(string name)
        {
            return AnnotationClasses.ContainsKey(name.Trim());
        }

        public Characteristics GetAdjustedCharacteristics(Characteristics input)
        {
            Characteristics output = new Characteristics();
            output.FontName = input.FontName;
            output.FontSize = input.FontSize;
            output.Height = input.Height;
            output.Width = input.Width;

            string fName = input.FontName.ToUpper() + "+" + input.FontSize;
            if (FontAdjustment.ContainsKey(fName))
            {
                FontMap adjust = FontAdjustment[fName];

                output.FontName = "Arial";
                if (adjust.OutputSize != "ANY")
                {
                    output.FontSize = adjust.OutputSize;
                }
                output.Height = Convert.ToSingle(input.Height) * adjust.HeightAdjustment;
                output.Width = Convert.ToSingle(input.Width) * adjust.WidthAdjustment;
            }
            else
            {
                fName = input.FontName.ToUpper() + "+ANY";
                if (FontAdjustment.ContainsKey(fName))
                {
                    FontMap adjust = FontAdjustment[fName];

                    output.FontName = "Arial";
                    if (adjust.OutputSize != "ANY")
                    {
                        output.FontSize = adjust.OutputSize;
                    }
                    output.Height = Convert.ToSingle(input.Height) * adjust.HeightAdjustment;
                    output.Width = Convert.ToSingle(input.Width) * adjust.WidthAdjustment;
                }
            }
            return output;
        }
    }
}
