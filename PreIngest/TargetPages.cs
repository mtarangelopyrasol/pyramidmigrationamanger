﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PreIngest
{
    class TargetPages : ClassBase
    {
        protected List<TargetPage> pageCollection = new List<TargetPage>();

        public TargetPages(Context c) : base(c)
        {
        }

        public TargetPage AddPage(string fileName, Int32 pageNum, string mimeType)
        {
            TargetPage tp = new TargetPage(contxt, fileName, pageNum, mimeType);
            pageCollection.Add(tp);
            return tp;
        }

        public void Serialize(string docID)
        {
            foreach (TargetPage tp in pageCollection)
            {
                tp.Serialize(docID);
            }
        }

        public TargetPage FindPage(string legacyPageID)
        {
            TargetPage result = null;

            foreach (TargetPage tp in pageCollection)
            {
                if (tp.HasSourcePage(legacyPageID))
                {
                    result = tp;
                    break;
                }
            }
            return result;
        }
    }
}
