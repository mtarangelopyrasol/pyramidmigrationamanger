﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml;

namespace PreIngest
{
    class CMDocuments : ClassBase
    {
        protected string targetCaseID = string.Empty;
        protected string caseAccount = string.Empty;
        protected string caseType = string.Empty;
        protected List<CMDocument> docCollection = new List<CMDocument>();
        int docCount = 0;
        int errorCount = 0;
        int successCount = 0;

        public CMDocuments(Context c, string cAccount, string cID, string ct) : base(c)
        {
            caseAccount = cAccount;
            targetCaseID = cID;
            caseType = ct;
            ItemsProcessed = false;
        }

        public Boolean ItemsProcessed { get; set; }

        // Deals with Case Level Configurations
        public ConversionResult PerformConversion(string srcContentPrefixPath, string stagingPath)
        {
            ConversionResult result = ConversionResult.CONV_NEW;

            while (result != ConversionResult.CONV_COMPLETE)
            {
                if (contxt.backgroundWorker.CancellationPending)
                {
                    result = ConversionResult.CONV_CANCELED;
                    break;
                }
                XmlDocument sourceXML = GetDocSourcePages();
                if (sourceXML.SelectNodes("//row").Count == 0)
                {
                    LogDebugMessage("CMDocuments: PerformConversion: No more Source Pages");
                    result = ConversionResult.CONV_COMPLETE;
                    break;
                }
                LogDebugMessage("CMDocuments: PerformConversion: Got Source Pages: " + sourceXML.SelectNodes("//row").Count.ToString());
                ItemsProcessed = true;
                MarshalDocuments(sourceXML, srcContentPrefixPath);
                foreach (CMDocument cmDoc in docCollection)
                {
                    if (contxt.backgroundWorker.CancellationPending)
                    {
                        result = ConversionResult.CONV_CANCELED;
                        break;
                    }
                    try
                    {
                        result = cmDoc.PerformConversion(srcContentPrefixPath, stagingPath);
                    }
                    catch (Exception ex)
                    {
                        LogError("Case: " + caseAccount + " Exception processing document: " + ex.Message);
                        result = ConversionResult.CONV_ERRORED;
                    }
                    docCount++;
                    if ((result == ConversionResult.CONV_DONE) || (result == ConversionResult.CONV_COMPLETE)) successCount++;
                    else if (result == ConversionResult.CONV_ERRORED) errorCount++;
                }
            }
            return result;
        }

        // Deals with Document Level Configurations
        public ConversionResult PerformConversion(int batchSize, string batchCondition, string srcContentPrefixPath, string stagingPath)
        {
            ConversionResult result = ConversionResult.CONV_NEW;

            while (docCount < batchSize)
            {
                if (contxt.backgroundWorker.CancellationPending)
                {
                    result = ConversionResult.CONV_CANCELED;
                    break;
                }

                try
                {
                    XmlDocument sourceXML = GetDocumentData(batchCondition);
                    if (sourceXML.SelectNodes("//row").Count == 0)
                    {
                        LogDebugMessage("CMDocuments: PerformConversion: No more Document Data to fetch...");
                        result = ConversionResult.CONV_COMPLETE;
                        break;
                    }
                    LogDebugMessage("CMDocuments: PerformConversion: Fetched Document Data. Count: " + sourceXML.SelectNodes("//row").Count.ToString() + ". Starting Marshaling...");
                    ItemsProcessed = true;
                    MarshalDocuments(sourceXML, srcContentPrefixPath);
                    LogDebugMessage("CMDocuments: PerformConversion: Marshaling Complete. Document Count: " + docCollection.Count.ToString() + ". Starting Conversion...");
                    foreach (CMDocument cmDoc in docCollection)
                    {
                        if (contxt.backgroundWorker.CancellationPending)
                        {
                            result = ConversionResult.CONV_CANCELED;
                            break;
                        }
                        if (!cmDoc.IsConverted())
                        {
                            try
                            {
                                result = cmDoc.PerformConversion(srcContentPrefixPath, stagingPath);
                            }
                            catch (Exception ex)
                            {
                                LogError("Exception processing document: " + ex.Message);
                                result = ConversionResult.CONV_ERRORED;
                            }
                            docCount++;
                            if ((result == ConversionResult.CONV_DONE) || (result == ConversionResult.CONV_COMPLETE)) successCount++;
                            else if (result == ConversionResult.CONV_ERRORED) errorCount++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogError("Exception processing document: " + ex.Message);
                    result = ConversionResult.CONV_ERRORED;
                }
            }
            return result;
        }

        protected XmlDocument GetDocumentData(string condition)
        {
            string fld = contxt.configMgr.GetDocumentIdentifier();

            string sql = (condition.Length > 0) ? "SELECT TOP(1) * FROM " + contxt.jobName + "_DocumentData WHERE " + condition + " AND Status IN ('CONV_NEW', 'CONV_CANCELED')" :
                            "SELECT TOP(1) * FROM " + contxt.jobName + "_DocumentData WHERE Status IN ('CONV_NEW', 'CONV_CANCELED')";

            while (true)
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(contxt.dbConnection, sql);
                if (xml.SelectNodes("//row").Count > 0)
                {
                    string val = xml.SelectNodes("//row")[0].Attributes[fld].Value;

                    XmlDocument xmlSet = DBAdapter.DBAdapter.GetProcessingRecs(contxt.dbConnection, contxt.jobName + "_DocumentData", "BatchID", contxt.batchID,
                                                                        fld + " = '" + val + "'", "Status IN ('CONV_NEW', 'CONV_CANCELED')", false);
                    if (xmlSet.SelectNodes("//row").Count > 0)
                    {
                        return xmlSet;
                    }
                }
                else
                {
                    return xml;
                }
            }
        }

        public int GetDocCount()
        {
            return docCount;
        }

        public int GetSuccessCount()
        {
            return successCount;
        }

        public int GetErrorCount()
        {
            return errorCount;
        }

        protected XmlDocument GetDocSourcePages()
        {
            return DBAdapter.DBAdapter.GetProcessingRecs(contxt.dbConnection, contxt.jobName + "_DocumentData", "BatchID", contxt.batchID,
                contxt.configMgr.GetDocAccountIdentifier() + " = '" + caseAccount + "'", "Status IN ('CONV_NEW', 'CONV_CANCELED')", false);
        }

        protected void MarshalDocuments(XmlDocument sourcePages, string srcContentPathPrefix)
        {
            Boolean toSingle = contxt.configMgr.NeedToMarshalToSingleDoc();

            LogDebugMessage("CMDocuments: MarshalDocuments: Total number of pages to marshal: " + sourcePages.SelectNodes("//row").Count.ToString());

            int i = 0;
            foreach (XmlElement row in sourcePages.SelectNodes("//row"))
            {
                if (contxt.backgroundWorker.CancellationPending) break;

                try
                {
                    LogDebugMessage("CMDocuments: MarshalDocuments: Creating a new source page..." + i++);
                    SourcePage sp = new SourcePage(contxt, row, srcContentPathPrefix);
                    if (sp != null)
                    {
                        if (sp.GetExtension().Length > 0)
                        {
                            CMDocument cmDoc = GetDocument(sp.GetDocMarshalingIdentifier(), toSingle);
                            if ((cmDoc == null) || (cmDoc.IsMarshalingExempt()) || (!cmDoc.IsConvertableFormat(sp.GetExtension())))
                            {
                                AddDocument(sp);
                            }
                            else
                            {
                                cmDoc.AddSourcePage(sp);
                            }
                        }
                        else
                        {
                            LogError("Discarding source line " + row.Attributes["ID"].Value + " while marshaling documents.");
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogError("Discarding source line " + row.Attributes["ID"].Value + " while marshaling documents. " + ex.Message);
                }
            } 
        }

        protected CMDocument GetDocument(Dictionary<String, String> docId, Boolean toSingle)
        {
            CMDocument result = null;
           
            foreach (CMDocument d in docCollection)
            {
                if (d.IsSameDoc(docId))
                {
                    result = d;
                    if (toSingle && !d.IsMarshalingExempt()) break; // if not toSingle, do not break, as we want the LAST document 
                }
            }
            return result;
        }

        protected void AddDocument(SourcePage firstPage)
        {
            CMDocument d = new CMDocument(contxt, targetCaseID, caseType, caseAccount, (docCollection.Count + 1).ToString());
            docCollection.Add(d);
            d.AddSourcePage(firstPage);
        }

        public string GetDocumentWithSourcePage(Dictionary<string, string> propertyValues)
        {
            string result = string.Empty;

            foreach (CMDocument d in docCollection)
            {
                if (d.HasSourcePage(propertyValues))
                {
                    result = d.GetTargetDocID();
                    break;
                }
            }
            return result;
        }
    }
}
