﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PreIngest
{
    abstract class TargetProperties : ClassBase
    {
        protected List<TargetProperty> properties = new List<TargetProperty>();

        public TargetProperties(Context c) : base(c)
        {
        }

        abstract public TargetProperty AddProperty(string n, string v, string t);

        protected string GetConvertedValue(string n, string v, string t)
        {
            string nv = string.Empty;

            try
            {
                nv = Converter.ConvertValue(v, t);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(v + " is an invalid value for property " + n + " of type " + t + ". (" + ex.Message + ")");
            }
            return nv;
        }

        public void Serialize(string objectID)
        {
            foreach (TargetProperty p in properties)
            {
                p.Serialize(objectID);
            }
        }
    }
}
