﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace PreIngest
{
    public class ConfigTaskType
    {
        Mapping map = null;
        ConfigProperties cps = null;

        public ConfigTaskType(string name, XmlElement rowTaskTypes, XmlDocument xmlTaskProperties, string dbCnxn)
        {
            Name = name;
            map = new Mapping(rowTaskTypes);
            cps = new ConfigProperties();
            foreach (XmlElement rowTaskProperties in xmlTaskProperties.SelectNodes("//row"))
            {
                ConfigProperty cp = new ConfigProperty(rowTaskProperties.GetAttribute("Name"), rowTaskProperties);

                if (cp.IsMapped())
                {
                    string sqlMap = "SELECT MgrMappingValues.OldValue, MgrMappingValues.NewValue FROM MgrMappingValues, MgrMappings WHERE MgrMappingValues.MapID = MgrMappings.ID and MgrMappings.MappingName = '" + cp.GetMappingName() + "'";
                    XmlDocument xmlMapping = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sqlMap);
                    cp.LoadValueMapping(xmlMapping);
                }
                cps.AddProperty(cp);
            }
        }

        public string Name { get; set; }

        public ConfigProperties GetProperties()
        {
            return cps;
        }

        public Boolean IsDocInitiated()
        {
            string value = map.GetValue("IsDocInitiated");
            return (Convert.ToInt32(value) == 1) ? true : false;
        }

        public string GetInitiatingDocClass()
        {
            return map.GetValue("InitiatingDocClass");
        }

        public string GetInitiatingDocIdSource()
        {
            return map.GetValue("InitiatingDocIdSource");
        }

        public string GetAttachingProperty()
        {
            return map.GetValue("AttachingProperty");
        }
    }
}
