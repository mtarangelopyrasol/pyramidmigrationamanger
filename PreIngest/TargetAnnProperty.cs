﻿using System;

namespace PreIngest
{
    class TargetAnnProperty : TargetProperty
    {
        public TargetAnnProperty(Context c, string n, string v, string t) : base(c, n, v, t)
        {
        }

        public override void Serialize(string annID)
        {
            if (!IsValueNull())
            {
                DBAdapter.DBAdapter.AddToDB(contxt.dbConnection, "INSERT INTO " + contxt.jobName + "_CEAnnotationProperties (AnnotationID, Name, Value, Type) VALUES ( " + annID + ", '" + Name + "', '" + Converter.EscapeQuote(Value) + "', '" + DataType + "')");
            }
        }
    }
}
