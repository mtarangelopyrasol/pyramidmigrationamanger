﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

using Leadtools;
using Leadtools.Codecs;

namespace PreIngest
{
    class SourcePages : ClassBase
    {
        protected List<SourcePage> Collection = new List<SourcePage>();
        protected Boolean needToAssignPageNumbers;

        public SourcePages(Context c) : base(c)
        {
            needToAssignPageNumbers = (contxt.configMgr.GetPageNumberIdentifier().Length == 0) ? true : false;
        }

        public void AddPage(SourcePage page)
        {
            if (needToAssignPageNumbers)
            {
                Collection.Add(page);
                page.PageNumber = Collection.Count;
            }
            else
            {
                Boolean inserted = false;

                int index = 0;
                foreach (SourcePage p in Collection)
                {
                    if (page.PageNumber < p.PageNumber)
                    {
                        Collection.Insert(index, page);
                        inserted = true;
                        break;
                    }
                    index++;
                }
                if (!inserted)
                {
                    Collection.Add(page);
                }
            }
        }
        
        public Dictionary<string, string> GetDocIdentifier()
        {
            Dictionary<string, string> docID = null;
            if (Collection.Count > 0)
            {
                SourcePage p = Collection[0]; // the first element
                docID = p.GetDocMarshalingIdentifier();
            }
            return docID;
        }

        public string GetDocumentClass()
        {
            string docClass = string.Empty;
            if (Collection.Count > 0)
            {
                SourcePage p = Collection[0]; // the first element
                docClass = p.GetDocumentClass();
            }
            return docClass;
        }

        public string GetValue(String srcColName)
        {
            string value = string.Empty;
            foreach (SourcePage p in Collection)
            {
                value = p.GetValue(srcColName);
                if ((value != null) && (value.Length > 0))
                    break;
            }
            return value;
        }

        public void CheckConsistency(String prop, String srcColName, String value)
        {
            foreach (SourcePage p in Collection)
            {
                p.CheckConsistency(prop, srcColName, value);
            }
        }

        public void LogDocFormation()
        {
            String pageNums = string.Empty;
            String srcID = string.Empty;
 
            foreach (SourcePage sp in Collection)
            {
                if (pageNums.Length == 0)
                {
                    srcID = sp.GetSourceIdString();
                    pageNums = sp.PageNumber.ToString();
                }
                else
                {
                    pageNums = pageNums + ", " + sp.PageNumber.ToString();
                }  
            }
            string message = "Document (" + srcID + ") formed from pages: " + pageNums;
            if (message.Length >= 4096)
            {
                message = message.Substring(0, 4000);
                message += "...";
            }
            LogMessage(message);
        }

        public void LogMarshalingInfo(string docID)
        {
            foreach (SourcePage sp in Collection)
            {
                sp.LogMarshalingInfo(docID);
            }
        }

        public int GetCount()
        {
            return Collection.Count;
        }

        public SourcePage GetPage(Int64 index)
        {
            if (index > Collection.Count)
            {
                throw new ArgumentException("Index " + index.ToString() + " exceeds size of source page collection " + Collection.Count + ".");
            }
            return Collection[(int)index - 1];
        }

        public SourcePage GetPageWithID(string pageID)
        {
            SourcePage result = null;

            foreach (SourcePage p in Collection)
            {
                if (p.GetPageIdentifier() == pageID)
                {
                    result = p;
                    break;
                }
            }
            return result;
        }

        public SourcePage GetPageWithNum(Int64 pageNum)
        {
            SourcePage result = null;

            foreach (SourcePage p in Collection)
            {
                if (p.PageNumber == pageNum)
                {
                    result = p;
                    break;
                }
            }
            return result;
        }

        public SourcePage GetNextPage(Int64 pageNum)
        {
            SourcePage result = null;
            Int64 target = pageNum;
            Int64 initialSearchValue = pageNum;

            do
            {
                result = GetPageWithNum(target);
                target++;
                if ((target - initialSearchValue) > 10000)
                    throw new Exception("Cannot find page after Page: " + initialSearchValue.ToString());
            }
            while (result == null);

            return result;
        }

        public void UpdateDocStatus(ConversionResult convResult)
        {
            foreach (SourcePage p in Collection)
            {
                p.UpdateDocStatus(convResult);
            }
        }

        public Boolean HasSourcePage(Dictionary<string, string> propertyValues)
        {
            Boolean result = false;

            foreach (SourcePage p in Collection)
            {
                Boolean match = true;
                foreach (string k in propertyValues.Keys)
                {
                    if (propertyValues[k] != p.GetValue(k))
                    {
                        match = false;
                        break;
                    }
                }
                if (match)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        public RasterImageFormat DetermineTargetFormat(string mimeType)
        {
            RasterImageFormat result = (RasterImageFormat) Enum.Parse(typeof(RasterImageFormat), contxt.configMgr.DetermineLeadFormat(mimeType, null), true);

            // We will defer promotion to Save time
            //foreach (SourcePage sp in Collection)
            //{
            //    result = sp.DetermineTargetFormat(result);
            //}
            return result;
        }

        public MMInterfaces.DocumentPages GetSourcePageProperties()
        {
            MMInterfaces.DocumentPages docPages = new MMInterfaces.DocumentPages();
            foreach (SourcePage sp in Collection)
            {
                docPages.Pages.Add(sp.GetColumnValues());
            }
            return docPages;
        }
    }
}
