﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml;
using DBAdapter;

namespace PreIngest
{
    abstract class ClassBase
    {
        protected const double PAGE_WIDTH = 8.50;
        protected const double PAGE_HEIGHT = 11.0;
        protected const int XRESOLUTION = 300;
        protected const int YRESOLUTION = 300;

        protected Context contxt;

        protected ClassBase(Context c)
        {
            contxt = c;
        }

        protected void LogMessage(String message)
        {
            contxt.logger.LogMessage(contxt.batchID, message);
            contxt.backgroundWorker.ReportProgress(0, message);
        }

        protected void LogWarning(String message)
        {
            contxt.logger.LogWarning(contxt.batchID, message);
            contxt.backgroundWorker.ReportProgress(0, message);
        }

        protected void LogError(String message)
        {
            contxt.logger.LogError(contxt.batchID, message);
            contxt.backgroundWorker.ReportProgress(0, message);
        }

        protected void LogDebugMessage(String message)
        {
            contxt.logger.LogDebugMessage(contxt.batchID, message);
        }
    }
}
