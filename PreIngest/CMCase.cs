﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace PreIngest
{
    class CMCase : CMEntity
    {
        string accountID = string.Empty;
        CMDocuments cmDocs = null;
        CMTasks cmTasks = null;
        CMAnnotations cmAnns = null;

        public CMCase(Context c) : base(c)
        {  
        }

        public override void SetSource(XmlElement sourceXML)
        {
            PrepareSourceProperties(sourceXML);
            accountID = srcProperties.Values[contxt.configMgr.GetCaseAccountIdentifier()];
        }

        public ConversionResult PerformConversion(string srcContentPrefixPath, string stagingPath)
        {
            ConversionResult result = ConversionResult.CONV_NEW;

            try
            {
                LogMessage("Case: " + accountID);

                DetermineType();
                LogDebugMessage("CMCase: PerformConversion: Case Type Determined: " + targetType);
                FormulateTargetProperties();
                LogDebugMessage("CMCase: PerformConversion: Target Properties Formulated");
                Serialize();
                List<String> containerFolders = null;
                LogDebugMessage("CMCase: PerformConversion: Serialized");

                if (contxt.configMgr.GetAlwaysCreateFoldersIdentifier())
                {
                    AddStaticFolders();

                }
                else
                {
                    containerFolders = GetContainerFolders();
                    AddStaticFolders(containerFolders);
                }

                LogDebugMessage("CMCase: PerformConversion: Static Folders added");
                result = AddAnnotations(srcContentPrefixPath, stagingPath);
                LogDebugMessage("CMCase: PerformConversion: Case Annotations added: " + result.ToString());
                if (result == ConversionResult.CONV_COMPLETE)
                {
                    LogDebugMessage("CMCase: PerformConversion: Adding Documents");
                    result = AddDocuments(srcContentPrefixPath, stagingPath);
                    if (result == ConversionResult.CONV_COMPLETE)
                    {
                        if (cmDocs.ItemsProcessed)
                        {
                            LogMessage("Successfully Converted Documents: " + cmDocs.GetSuccessCount());
                            LogMessage("Errored Documents: " + cmDocs.GetErrorCount());
                            LogMessage("Total Target Documents Formulated: " + cmDocs.GetDocCount());
                        }
                        result = AddTasks();
                        if (result == ConversionResult.CONV_COMPLETE)
                        {
                            if (cmTasks.ItemsProcessed)
                            {
                                LogMessage("Successfully Converted Tasks: " + cmTasks.GetSuccessCount());
                                LogMessage("Errored Tasks: " + cmTasks.GetErrorCount());
                                LogMessage("Total Tasks: " + cmTasks.GetTaskCount());
                            }
                            LogMessage("Case: " + accountID + " Complete");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = ConversionResult.CONV_ERRORED;
                LogError("Case: " + accountID + " " + ex.Message);
                entityStatus = EntityStatus.DISCARD;
            }
            finally
            {
                result = ((result == ConversionResult.CONV_ERRORED) || ((cmDocs != null) && (cmDocs.GetErrorCount() > 0)) || ((cmTasks != null) && (cmTasks.GetErrorCount() > 0))) ? ConversionResult.CONV_ERRORED : ConversionResult.CONV_DONE;
                UpdateSourceStatus(result, contxt.jobName + "_CaseData");
                LogDebugMessage("CMCase: PerformConversion: Updating Source Status: " + result.ToString());
            }
            return result;
        }

        private List<String> GetContainerFolders()
        {

            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            //Create a database query string that will get the unique folders for this case
            //String containgFolderSqlByCaseId = "Select distinct Folder from " +
            //      contxt.jobName + "_CEDocuments where CaseId = " + mmCaseId;

            //String containgFolderSqlByCaseId = "Select distinct " + contxt.configMgr.GetDocFolderIdentifier() + " from " +
            //   contxt.jobName + "_DocumentData where " + contxt.configMgr.GetDocAccountIdentifier() + " = '" + this.accountID + "'";

            //modifying to reflect changes for Comerica. checking the distinct f_docnumber to select unique ID's for case folders.
            String containgFolderSqlByCaseId = "Select distinct " + contxt.configMgr.GetDocFolderIdentifier() + " from " +
               contxt.jobName + "_DocumentData where " + contxt.configMgr.GetDocAccountIdentifier() + " = '" + this.accountID + "'";

            //Pull the data
            XmlDocument foldersXml = DBAdapter.DBAdapter.SelectFromDB(contxt.dbConnection, containgFolderSqlByCaseId);

            //Move it to a generic list
            List<String> containingFoldersList = new List<string>();

            foreach (XmlElement rowNode in foldersXml.SelectNodes("//row"))
            {
                string folderPath = rowNode.GetAttribute(contxt.configMgr.GetDocFolderIdentifier());

                if (string.IsNullOrEmpty(folderPath)) continue;

                //pull out the root folder
                string[] parts = folderPath.Split('/');
                string rootFolderName = parts[0];

                if (!string.IsNullOrWhiteSpace(rootFolderName) && !containingFoldersList.Contains(rootFolderName))
                {
                    containingFoldersList.Add(rootFolderName);
                }
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            //Create a database query string that will get the unique folders for this case
            //String containgFolderSqlByCaseId = "Select distinct Folder from " +
            //      contxt.jobName + "_CEDocuments where CaseId = " + mmCaseId;

            String containgAnnFolderSqlByCaseId = "Select distinct " + contxt.configMgr.GetAnnotationAccountIdentifier() + " from " +
                  contxt.jobName + "_AnnotationData where " + contxt.configMgr.GetAnnotatedObjectTypeIdentifier() + "  = 'CASE' and " +
                  contxt.configMgr.GetAnnotatedObjectIdentifier() + " = '" + this.accountID + "'";

            ////Pull the data
            XmlDocument annFoldersXml = DBAdapter.DBAdapter.SelectFromDB(contxt.dbConnection, containgAnnFolderSqlByCaseId);

            try
            {
                string annFolderPath = annFoldersXml.SelectNodes("//row")[0].Attributes[contxt.configMgr.GetDocFolderIdentifier()].Value;

                annFolderPath.Split('/');
                string[] annParts = annFolderPath.Split('/');
                string annRootFolderName = annParts[0];

                if (!string.IsNullOrWhiteSpace(annRootFolderName) && !containingFoldersList.Contains(annRootFolderName))
                {
                    containingFoldersList.Add(annRootFolderName);
                }
            }catch(Exception error)
            {
                //do nothing ... this just means there were not filenotes
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////

            return containingFoldersList;
        }

        protected override void DetermineType()
        {
            if (contxt.caseTypeSelector != null)
            {
                targetType = contxt.caseTypeSelector.Select(srcProperties);
            }
            else
            {
                targetType = contxt.configMgr.GetCaseType();
                if (targetType.Length == 0)
                {
                    string identifier = contxt.configMgr.GetCaseTypeIdentifier();
                    if (identifier.Length > 0)
                    {
                        targetType = GetSourceValue(identifier);
                    }
                }
            }
            if (targetType == string.Empty)
            {
                throw new ArgumentException("Could not determine target case type");
            }
            if (!IsTargetTypeDefined())
            {
                throw new ArgumentException("Case Type " + targetType + " is not defined in the configuration");
            }
        }

        protected override Boolean IsTargetTypeDefined()
        {
            return contxt.configMgr.IsCaseTypeDefined(targetType);
        }

        protected void FormulateTargetProperties()
        {
            targetProperties = new TargetCaseProperties(contxt);
            FormulateProperties(contxt.configMgr.GetCaseProperties(targetType), contxt.jobName + "_CaseData_", contxt.casePropertyEvaluator, "Case: " + accountID);
        }

        protected void Serialize()
        {
            XmlDocument xml = DBAdapter.DBAdapter.FetchInsertedRec(contxt.dbConnection, "INSERT INTO " + contxt.jobName + "_CECases (BatchID, CaseType, MMAccountID) OUTPUT inserted.* VALUES ( " + contxt.batchID + ", '" + targetType + "', '" + accountID + "')");
            mmID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
            targetProperties.Serialize(mmID); 
        }

        protected void AddStaticFolders(IList foldersContainingObjects)
        {
            List<ConfigFolder> folders = contxt.configMgr.GetCaseFolders(targetType);

            foreach (ConfigFolder cf in folders)
            {
                if (cf.Parent == "Root")
                {
                    //Create the configured folder only if it is in the list
                    // of folders with objects contained within the folder
                    if (foldersContainingObjects.Contains(cf.Name))
                    {
                        AddSubFolder(cf.Name, cf.Parent, folders);
                        LogDebugMessage("SDJ: folder added to static folder ingest table: " + cf.Name);
                    }
                }
            }
        }

        protected void AddStaticFolders()
        {
            List<ConfigFolder> folders = contxt.configMgr.GetCaseFolders(targetType);

            foreach (ConfigFolder cf in folders)
            {
                if (cf.Parent == "Root")
                {
                    AddSubFolder(cf.Name, cf.Parent, folders);
                }
            }
        }

        protected void AddSubFolder(string folderName, string parentName, List<ConfigFolder> cfs)
        {
            DBAdapter.DBAdapter.AddToDB(contxt.dbConnection, "INSERT INTO " + contxt.jobName + "_CECaseFolders (CaseID, Name, Parent) VALUES ( " + mmID.ToString() + ", '" + Converter.EscapeQuote(folderName) + "', '" + Converter.EscapeQuote(parentName) + "')");
            foreach (ConfigFolder cf in cfs)
            {
                if (cf.Parent == folderName)
                {
                    AddSubFolder(cf.Name, cf.Parent, cfs);
                }
            }
        }

        protected ConversionResult AddAnnotations(string srcContentPrefixPath, string stagingPath)
        {
            cmAnns = new CMAnnotations(contxt, accountID, mmID);
            return (contxt.annDataLoaded) ? cmAnns.PerformConversion(AnnotatedObjectType.CASE, srcContentPrefixPath, stagingPath) : ConversionResult.CONV_COMPLETE;
        }

        protected ConversionResult AddDocuments(string srcContentPrefixPath, string stagingPath)
        {
            cmDocs = new CMDocuments(contxt, accountID, mmID, targetType);
            return (contxt.docDataLoaded) ? cmDocs.PerformConversion(srcContentPrefixPath, stagingPath) : ConversionResult.CONV_COMPLETE;
        }

        protected ConversionResult AddTasks()
        {
            cmTasks = new CMTasks(contxt, accountID, mmID);
            return (contxt.taskDataLoaded) ? cmTasks.PerformConversion(cmDocs, GetCaseTypeID()) : ConversionResult.CONV_COMPLETE;
        }

        protected string GetCaseTypeID()
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(contxt.dbConnection, "SELECT ID FROM CfgCaseTypes WHERE CaseName = '" + targetType + "'");
            return xml.SelectNodes("//row")[0].Attributes["ID"].Value;
        }
    }
}
