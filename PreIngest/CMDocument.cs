﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Leadtools;
using Leadtools.Codecs;
using Leadtools.ImageProcessing;
using MMInterfaces;

namespace PreIngest
{
    class CMDocument : CMEntity
    {
        SourcePages sourcePages;
        Dictionary<String, String> srcDocIdentifier = null;
        TargetPages targetPages = null;
        RasterImageFormat targetFormat;
        int targetFormatMinBpp = 1;

        string mimeType = string.Empty;
        string caseType = string.Empty;
        string docID = string.Empty;

        protected string accountID = string.Empty;
        string mmCaseID = string.Empty;
        string mmSeqNum = string.Empty;
        string containmentName = string.Empty;
        string legacyDocID = string.Empty;
        Int32 srcFirstPageDBID = 0;
        protected Boolean converted = false;
        string docTraceField = string.Empty;
        CMAnnotations cmAnns = null;

        public CMDocument(Context c, string caseID, string ct, string caseAccountID, string seqNum) : base(c)
        {
            sourcePages = new SourcePages(c);
            mmCaseID = caseID;
            caseType = ct;
            accountID = caseAccountID;
            mmSeqNum = seqNum;
            docTraceField = c.configMgr.GetDocTraceIdentifier();
        }

        public override void SetSource(XmlElement sourceXML)
        {
            // there can be multiple source lines for documents - so we don't implement this function
        }

        public ConversionResult PerformConversion(string srcContentPrefixPath, string stagingPath)
        {
            converted = true;

            ConversionResult result = ConversionResult.CONV_NEW;

            DetermineType();
            docID = GetSourceIdentifier();
            LogDebugMessage("CMDocument: PerformConversion: Starting Conversion; SourceIdentifier: " + docID);

            try
            {
                FormulateTargetProperties();
                LogDebugMessage("CMDocument: PerformConversion: TargetProperties formulated");
                FormulateTargetPages(srcContentPrefixPath, stagingPath);
                LogDebugMessage("CMDocument: PerformConversion: TargetPages formulated");
                Serialize();
                LogDebugMessage("CMDocument: PerformConversion: Serialized");
                FileInFolders();
                LogDebugMessage("CMDocument: PerformConversion: Filed in Folders");

                if (entityStatus != EntityStatus.DISCARD)
                {
                    result = AddAnnotations(srcContentPrefixPath, stagingPath);
                    LogDebugMessage("CMDocument: PerformConversion: Annotations added");
                }
            }
            catch (Exception ex)
            {
                entityStatus = EntityStatus.DISCARD;
                result = ConversionResult.CONV_ERRORED;
                LogError("Error: (Document_ID: " + docID + ") " + ex.Message);
            }
            finally
            {
                result = ((entityStatus != EntityStatus.GOOD) || ((cmAnns != null) && (cmAnns.GetErrorCount() > 0))) ? ConversionResult.CONV_ERRORED : ConversionResult.CONV_DONE;
                sourcePages.UpdateDocStatus(result);
                LogDebugMessage("CMDocument: PerformConversion: Convesrion Complete; Source Status updated: " + result.ToString());
            }
            return result;
        }

        protected override void DetermineType()
        {
            if (contxt.docTypeSelector != null)
            {
                targetType = contxt.docTypeSelector.Select(sourcePages.GetSourcePageProperties());
            }
            else
            {
                targetType = contxt.configMgr.GetDocumentClass();
                if (targetType.Length == 0)
                {
                    targetType = sourcePages.GetDocumentClass();
                }
                if ((targetType.Length == 0) && (contxt.configLevel == "CASE"))
                {
                    targetType = contxt.configMgr.GetDefaultDocClass(caseType);
                }
            }
            if (targetType == string.Empty)
            {
                throw new ArgumentException("Could not determine target document class");
            }
            if (!IsTargetTypeDefined())
            {
                throw new ArgumentException("Document Class " + targetType + " is not defined in the configuration");
            }
        }

        protected override Boolean IsTargetTypeDefined()
        {
            return contxt.configMgr.IsDocumentClassDefined(targetType);
        }

        public Boolean IsConverted()
        {
            return converted;
        }

        public Boolean IsSameDoc(Dictionary<String, String> docId)
        {
            Boolean result = false;

            if (srcDocIdentifier == null)
            {
                srcDocIdentifier = sourcePages.GetDocIdentifier();
            }
            if (srcDocIdentifier.Count == docId.Count)
            {
                result = true;
                foreach (String k in srcDocIdentifier.Keys)
                {
                    if ((srcDocIdentifier[k] != null) && (docId[k] != null) && (srcDocIdentifier[k] != docId[k]))
                    {
                        result = false;
                        break;
                    }
                }
            }
            return result;
        }

        public void AddSourcePage(SourcePage sp)
        {
            sourcePages.AddPage(sp);
            if (sourcePages.GetCount() == 1)
            {
                mimeType = contxt.configMgr.GetTargetFormat(sp.GetExtension());
                containmentName = Converter.SanitizeContainmentName(sp.GetContainmentName());
                legacyDocID = sp.GetLegacyDocID();
                srcFirstPageDBID = sp.GetDBID();
                LogDebugMessage("CMDocument: AddSourcePage: Created New Document with Source ID: " + GetSourceIdentifier() + " MimeType: " + mimeType + " PageCount: 1");
            }
            else
            {
                LogDebugMessage("CMDocument: AddSourcePage: Document with Source ID: " + GetSourceIdentifier() + " PageCount: " + sourcePages.GetCount().ToString());
            }
        }

        public Boolean IsMarshalingExempt()
        {
            if (sourcePages.GetCount() == 1)
            {
                SourcePage sp = sourcePages.GetPage(1);
                return sp.IsMarshalingExempt();
            }
            return false;
        }

        public Boolean IsConvertableFormat(string format)
        {
            return contxt.configMgr.IsConvertableFormat(format, mimeType);
        }

        protected void FormulateTargetProperties()
        {
            if (entityStatus == EntityStatus.DISCARD) return;

            targetProperties = new TargetDocProperties(contxt);
            
            List<ConfigProperty> lcps = contxt.configMgr.GetDocumentProperties(targetType);
            foreach (ConfigProperty cp in lcps)
            {
                if (contxt.backgroundWorker.CancellationPending) break;

                if (cp.IsComputed())
                    FormulateComputedProperty(cp, sourcePages.GetSourcePageProperties());
                else
                {
                    string source = cp.GetSource();
                    string fldValue = (source != string.Empty) ? sourcePages.GetValue(source) : string.Empty;

                    List<string> values;

                    if (cp.IsMultiValued())
                    {
                        values = FetchValues(contxt.jobName + "_DocumentData_" + source, fldValue);
                    }
                    else
                    {
                        values = new List<string>();
                        values.Add(fldValue);
                    }
                    
                    foreach(string value in values)
                    {
                        try
                        {
                            if (value == string.Empty)
                            {
                                AddTargetProperty(cp, GetEffectiveValue(cp, value, "Document Property: "), "Document");
                            }
                            else
                            {
                                if (!cp.IsMultiValued()) sourcePages.CheckConsistency(cp.Name, cp.GetSource(), value);
                                AddTargetProperty(cp, GetEffectiveValue(cp, value, "Document Property: "), "Document");
                            }
                        }
                        catch (Exception ex)
                        {
                            LogError("Trouble adding value " + value + " for document property: " + cp.Name + ": " + ex.Message);
                            entityStatus = EntityStatus.ERROR;
                        }
                    }
                }
            }
        }

        protected void FormulateTargetPages(string sourcePrefix, string targetStagingPath)
        {
            if (entityStatus == EntityStatus.DISCARD) return;

            Int32 targetPageNum = 0;

            targetPages = new TargetPages(contxt);

            if (IsConversionExempt())
            {
                LogDebugMessage("CMDocument: FormulateTargetPages: Conversion is exempt");
                string targetFileName = (targetStagingPath.Trim().Length == 0) ? FormulateTargetFileName(1) : targetStagingPath + "\\" + FormulateTargetFileName(1);
                SourcePage p = sourcePages.GetNextPage(0);
                LogDebugMessage("CMDocument: FormulateTargetPages: Source File Path: " + p.GetFilePath());
                if (p.IsPageGood())
                {
                    File.Copy(p.GetFilePath(), targetFileName);
                    TargetPage tp = targetPages.AddPage(targetFileName, ++targetPageNum, mimeType);
                    tp.SetSourceSequence(p.GetPageIdentifier());
                    LogDebugMessage("CMDocument: FormulateTargetPages: Page Added");
                }
                sourcePages.LogDocFormation();
                return;
            }

            LogDebugMessage("CMDocument: FormulateTargetPages: Conversion is not exempt");
            string srcPageIdentifiers = string.Empty;
            int pageCount = sourcePages.GetCount();

            Boolean contentFound = false;
            Boolean missingContent = false;

            RasterCodecs codecs = InitializeCodec();
            targetFormat = DetermineTargetFormat();
            targetFormatMinBpp = contxt.configMgr.DetermineMinimumBpp(targetFormat);
            LogDebugMessage("CMDocument: FormulateTargetPages: Target Format: " + targetFormat.ToString());

            using (MemoryStream sx = new MemoryStream())
            {
                Int64 pageNum = 0;

                LogDebugMessage("CMDocument: FormulateTargetPages: Total Page Count: " + pageCount.ToString());
                for (int i = 1; i <= pageCount; i++)
                {
                    if (contxt.backgroundWorker.CancellationPending) break;

                    SourcePage sp = sourcePages.GetNextPage(pageNum);
                    LogDebugMessage("CMDocument: FormulateTargetPages: Handling Page " + i.ToString() + " File Path: " + sp.GetFilePath());

                    if (srcPageIdentifiers.Length > 0) srcPageIdentifiers += "|";
                    srcPageIdentifiers += sp.GetPageIdentifier();
                    pageNum = sp.PageNumber + 1;
                    if ((sp != null) && (sp.IsPageGood()))
                    {
                        contentFound = true;

                        try
                        {
                            AddPageToImage(sp, codecs, i, sx, false);
                        }
                        catch (Exception e)
                        {
                            try
                            {
                                LogError("Image: " + sp.GetFilePath() + " may be corrupted (" + e.Message + "). Trying a more forgiving load...");
                                AddPageToImage(sp, codecs, i, sx, true);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Page ID: " + sp.GetPageIdentifier() + " - " + ex.Message);
                            }
                        }
                    }
                    else
                    {
                        missingContent = true;
                    }
                }
                if (contentFound)
                {
                    if (missingContent)
                    {
                        LogError("Document: " + legacyDocID + " Document has some pages missing.");
                        entityStatus = EntityStatus.DISCARD;
                    }

                    string targetFileName = FormulateTargetFileName(1);
                    using (FileStream fsx = File.OpenWrite(targetStagingPath + "\\" + targetFileName))
                    {
                        sx.WriteTo(fsx);
                        fsx.Flush();
                    }
                    LogDebugMessage("CMDocument: FormulateTargetPages: Adding Target Page " + targetPageNum.ToString());
                    TargetPage tp = targetPages.AddPage(targetStagingPath + "\\" + targetFileName, ++targetPageNum, mimeType);
                    tp.SetSourceSequence(srcPageIdentifiers);
                    sourcePages.LogDocFormation();
                }
                else
                {
                    entityStatus = EntityStatus.DISCARD;
                    throw new ArgumentException("Document: " + legacyDocID + " No good content found, discarding document.");
                }
            }
        }

        protected void AddPageToImage(SourcePage sp, RasterCodecs codecs, int pageNum, MemoryStream sx, Boolean allowCorrupted)
        {
            codecs.Options.Load.LoadCorrupted = allowCorrupted;

            AdjustRasterizeDocumentLoadOptions(sp, codecs);
            LogDebugMessage("CMDocument: AddPageToImage: Loading file: " + sp.GetFilePath() + " TotalPages: " + sp.TotalPages().ToString());
            using (RasterImage srcImage = codecs.Load(sp.GetFilePath(), 0, CodecsLoadByteOrder.Bgr, 1, sp.TotalPages()))
            {
                LogDebugMessage("CMDocument: AddPageToImage: Loaded successfully");
                SavePageToImage(srcImage, sp, codecs, pageNum, sx);
            }
        }

        protected void SavePageToImage(RasterImage image, SourcePage sp, RasterCodecs codecs, int pageNum, MemoryStream sx)
        {
            ApplyRotation(image, sp.GetRotationAngle(), sp.GetRotationDirection());
            LogDebugMessage("CMDocument: SavePageToImage: Rotation applied");
            CodecsSavePageMode mode = (pageNum == 1) ? CodecsSavePageMode.Overwrite : CodecsSavePageMode.Append;
            
            Boolean done = false;

            RasterImageFormat effectiveTargetFormat = sp.DetermineTargetFormat(targetFormat); // bring about any promotions if needed
            int bitsPerPixel = Math.Max(targetFormatMinBpp, sp.BitsPerPixel());
            
            while (!done)
            {
                try
                {
                    LogDebugMessage("CMDocument: SavePageToImage: Saving with BitsPerPixel: " + bitsPerPixel.ToString());
                    codecs.Save(image, sx, effectiveTargetFormat, bitsPerPixel, 1, -1, 1, mode);
                    done = true;
                }
                catch (RasterException ex)
                {
                    if ((ex.Code == RasterExceptionCode.BitsPerPixel) || (ex.Code == RasterExceptionCode.InvalidParameter))
                    {
                        // try with a lower bpp
                        if (bitsPerPixel >= 24) bitsPerPixel = 8;
                        else if (bitsPerPixel >= 8) bitsPerPixel = 1;
                        else if (bitsPerPixel >= 1) bitsPerPixel = 0;
                        else throw;
                    }
                    else throw;
                }
            }
        }

        protected void ApplyRotation(RasterImage image, int rotationAngle, RotationDirection direction)
        {
            if (rotationAngle > 0)
            {
                RotateCommand command = new RotateCommand();
                command.Angle = rotationAngle * 100;
                if (direction == RotationDirection.CounterClockwise)
                {
                    command.Angle = -(command.Angle);
                }
                command.FillColor = new RasterColor(255, 255, 255);
                command.Flags = RotateCommandFlags.Bicubic | RotateCommandFlags.Resize;
                command.Run(image);
            }
        }

        protected RasterCodecs InitializeCodec()
        {
            RasterCodecs codecs = new RasterCodecs();
            
            codecs.Options.Txt.Load.FontColor = new RasterColor(0, 0, 0);
            codecs.Options.Txt.Load.FontSize = 12;
            codecs.Options.Txt.Load.Bold = true;
            codecs.Options.Txt.Load.Enabled = true;
            codecs.Options.Jpeg.Save.Passes = contxt.leadToolsJPEGPasses;
            codecs.Options.Jpeg.Save.QualityFactor = contxt.leadToolsJPEGQualityFactor;

            switch (mimeType.ToLower())
            {
                case "pdf":
                    codecs.Options.Pdf.Save.Version = CodecsRasterPdfVersion.V16;
                    codecs.Options.Pdf.Save.UseImageResolution = true;
                    break;

                case "tif":
				case "jpg":
				case "jpeg":
					
                    break;

                default:
                    break;
            }
            return codecs;
        }

        protected void AdjustRasterizeDocumentLoadOptions(SourcePage sp, RasterCodecs codecs)
        {
            CodecsRasterizeDocumentLoadOptions rasterizeDocumentLoadOptions = codecs.Options.RasterizeDocument.Load;
            if (sp.IsDocumentFile())
            {
                rasterizeDocumentLoadOptions.LeftMargin = 0;
                rasterizeDocumentLoadOptions.TopMargin = 0;
                rasterizeDocumentLoadOptions.RightMargin = 0;
                rasterizeDocumentLoadOptions.BottomMargin = 0;
                rasterizeDocumentLoadOptions.PageWidth = PAGE_WIDTH;
                rasterizeDocumentLoadOptions.PageHeight = PAGE_HEIGHT;
                rasterizeDocumentLoadOptions.Unit = CodecsRasterizeDocumentUnit.Inch;

                rasterizeDocumentLoadOptions.SizeMode = CodecsRasterizeDocumentSizeMode.None;
                rasterizeDocumentLoadOptions.XResolution = sp.XResolution();
                rasterizeDocumentLoadOptions.YResolution = sp.YResolution();
            }
            if (sp.GetExtension() == "txt")
            {
                rasterizeDocumentLoadOptions.LeftMargin = 1.0;
                rasterizeDocumentLoadOptions.TopMargin = 1.25;
                rasterizeDocumentLoadOptions.RightMargin = 1.25;
                rasterizeDocumentLoadOptions.BottomMargin = 1.0;
            }
        }

        protected Boolean IsConversionExempt()
        {
            Boolean result = false;

            int pageCount = sourcePages.GetCount();
            if (pageCount == 1)
            {
                SourcePage sp = sourcePages.GetNextPage(0);
                if ((mimeType.ToUpper() == sp.GetExtension().ToUpper()) && (sp.GetRotationAngle() == 0))
                {
                    result = true;
                }
            }
            return result;
        }

        RasterImageFormat DetermineTargetFormat()
        {
            return sourcePages.DetermineTargetFormat(mimeType);
        }

        protected void FormulateComputedProperty(ConfigProperty cp, DocumentPages srcPageProperties)
        {
            if (contxt.docPropertyEvaluator == null)
            {
                string src = cp.GetSource();
                if ((src != "MMID") && (src != "ANNOTATION_HISTORY"))
                {
                    entityStatus = EntityStatus.DISCARD;
                    throw new ArgumentException("Could not find evaluator for computed Property " + cp.Name + ". Discarding this document.");
                }
            }
            else
            {
                if (srcPageProperties == null) srcPageProperties = sourcePages.GetSourcePageProperties();

                List<string> valueList = contxt.docPropertyEvaluator.Evaluate(cp.Name, cp.GetDataType(), srcPageProperties);
                foreach (string value in valueList)
                {
                    AddTargetProperty(cp, value, "Document Property: ");
                }
            }
        }

        protected void Serialize()
        {
            if (entityStatus == EntityStatus.DISCARD) return;

            string p8MimeType = contxt.configMgr.DetermineP8MimeType(mimeType);
            string sql = (contxt.configLevel == "CASE") ? "INSERT INTO " + contxt.jobName + "_CEDocuments (BatchID, CaseID, DocClass, MimeType, ContainmentName, MMAccountID) OUTPUT inserted.* VALUES ( " + contxt.batchID + ", " + mmCaseID + ", '" + targetType + "', '" + p8MimeType + "', '" + Converter.EscapeQuote(containmentName) + "', '" + docID + "')" :
                        "INSERT INTO " + contxt.jobName + "_CEDocuments (BatchID, DocClass, MimeType, ContainmentName, MMAccountID) OUTPUT inserted.* VALUES ( " + contxt.batchID + ", '" + targetType + "', '" + p8MimeType + "', '" + Converter.EscapeQuote(containmentName) + "', '" + docID + "')";
            XmlDocument xml = DBAdapter.DBAdapter.FetchInsertedRec(contxt.dbConnection, sql);
            mmID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
            
            if (docTraceField.Length > 0)
            {
                targetProperties.AddProperty(docTraceField, mmID.ToString(), "SingletonInt32");
            }
            sourcePages.LogMarshalingInfo(mmID);
            targetProperties.Serialize(mmID);
            targetPages.Serialize(mmID); 
        }
        
        protected string FormulateTargetFileName(int pageNum)
        {
            return (contxt.configLevel == "DOCUMENT") ? "Doc" + legacyDocID + "_" + srcFirstPageDBID.ToString() + "." + mimeType : "Case" + mmCaseID + "_Doc" + mmSeqNum + "_Page" + pageNum.ToString() + "." + mimeType;
        }

        protected string GetSourceIdentifier()
        {
            string result = string.Empty;

            if (srcDocIdentifier == null)
            {
                srcDocIdentifier = sourcePages.GetDocIdentifier();
            }
            foreach (string k in srcDocIdentifier.Keys)
            {
                if (result == string.Empty)
                {
                    result = srcDocIdentifier[k];
                }
                else
                {
                    result += "+";
                    result += srcDocIdentifier[k];
                }
            }
            return result;
        }

        protected void FileInFolders()
        {
            if (entityStatus == EntityStatus.DISCARD) return;

            string field = contxt.configMgr.GetDocFolderIdentifier();
            if (field.Length > 0)
            {
                string folder = sourcePages.GetValue(field);
                if (folder.Length > 0)
                {
                    if (!string.IsNullOrEmpty(mmCaseID))
                    {
                        string[] parts = folder.Split('/');
                        if (parts.Length > 1)
                        {
                            // check if starts with a Migration Manager created static folder
                            if (DoesFolderExist(parts[0], "Root"))
                            {
                                for (int i = 1; i < parts.Length; i++)
                                {
                                    if (!(DoesFolderExist(parts[i], parts[i - 1])))
                                    {
                                        DBAdapter.DBAdapter.AddToDB(contxt.dbConnection, "INSERT INTO " + contxt.jobName + "_CECaseFolders (CaseID, Name, Parent) VALUES ( " + mmCaseID.ToString() + ", '" + Converter.EscapeQuote(parts[i]) + "', '" + Converter.EscapeQuote(parts[i - 1]) + "')");
                                    }
                                }
                            }
                        }
                    }
                    int nAffected = DBAdapter.DBAdapter.UpdateDB(contxt.dbConnection, "UPDATE " + contxt.jobName + "_CEDocuments SET Folder = '" + Converter.EscapeQuote(folder) + "' WHERE ID = " + mmID);
                }
            }
        }

        public Boolean HasSourcePage(Dictionary<string, string> propertyValues)
        {
            return sourcePages.HasSourcePage(propertyValues);
        }

        public string GetTargetDocID()
        {
            return mmID;
        }

        protected ConversionResult AddAnnotations(string srcContentPrefixPath, string stagingPath)
        {
            cmAnns = new CMAnnotations(contxt, legacyDocID, mmID);
            return (contxt.annDataLoaded) ? 
                    cmAnns.PerformConversion(srcContentPrefixPath, stagingPath, targetProperties, targetType, sourcePages, targetPages) :
                    ConversionResult.CONV_COMPLETE;
        }

        protected bool DoesFolderExist(string folderName, string parent)
        {
             string sql = "SELECT ID FROM " + contxt.jobName + "_CECaseFolders WHERE CaseID = " + mmCaseID + " AND Name = '" + folderName + "' AND Parent = '" + parent + "'";
             XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(contxt.dbConnection, sql);
             return (xml.SelectNodes("//row").Count == 0) ? false : true;
        }
    }
}
