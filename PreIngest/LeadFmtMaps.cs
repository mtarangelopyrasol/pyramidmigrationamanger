﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace PreIngest
{
    public class LeadFmtMaps
    {
        protected Dictionary<string, string> maps = null;

        public LeadFmtMaps()
        {
            maps = new Dictionary<string, string>();
        }

        public void AddLeadFmtMap(string fmt, string fmtLead)
        {
            if (!maps.ContainsKey(fmt))
            {
                maps[fmt] = fmtLead;
            }
        }

        public string GetLeadFormat(string fmt)
        {
            string fmtLead = string.Empty;

            if (maps.ContainsKey(fmt))
            {
                fmtLead = maps[fmt];
            }
            else
            {
                if(maps.ContainsKey("ANY"))
                {
                    fmtLead = maps["ANY"];
                }
            }
            return fmtLead;
        }
    }
}
