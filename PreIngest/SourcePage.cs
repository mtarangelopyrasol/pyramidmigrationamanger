﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

using Leadtools;
using Leadtools.Codecs;
using Leadtools.ImageProcessing.Color;

namespace PreIngest
{
    public enum RotationDirection
    {
        Clockwise,
        CounterClockwise,
        None
    }

    class SourcePage : ClassBase
    {
        static Regex rgxSingleQuote = new Regex("'");
        const string repSingleQuote = "";  

        protected Dictionary<string, string> colValues = new Dictionary<string, string>();
        
        protected string classIdentifier;
        protected string documentIdentifier;
        protected string[] docMarshalingIdentifier;
        protected string docTypeIdentifier;
        protected string docDateIdentifier;
        protected string pageStatusIdentifier;
        protected string legacyDocIdentifier;
        protected string pageNumberIdentifier;
        protected string docPreAnnRotationIdentifier;
        protected Dictionary<string, string> idValues = new Dictionary<string, string>();
        protected Boolean docIdentified = false;
        protected string srcContentPathPrefix = string.Empty;

        protected string effectiveExtension = string.Empty;
        protected string extension = string.Empty;
        protected RasterImageFormat format;
        protected string compression = string.Empty;
        protected Boolean isDocumentFile = false;
        protected Boolean hasContent = true;
        protected int bpp = 0;
        protected int totalPages = 0;
        protected int height = 0;
        protected int width = 0;
        protected int xResolution = 0;
        protected int yResolution = 0;

        public Int64 PageNumber { get; set; }
        protected string FileName = string.Empty;
        protected string PageID = string.Empty;
        protected int RotationAngle = 0;
        protected RotationDirection DirectionToRotate = RotationDirection.None;
        protected RotationDirection PreAnnotationRotationDirection = RotationDirection.None;
        protected string MimeType = string.Empty;
        protected string ContainmentName = string.Empty;

        public SourcePage(Context c, XmlElement srcPage, string pathPrefix) : base(c)
        {
            srcContentPathPrefix = pathPrefix.Trim();
            ParseSourceXML(srcPage);
            try
            {
                AnalyzeContent();
            }
            catch
            {
                UpdateDocStatus(ConversionResult.CONV_ERRORED);
                throw;
            }
        }

        public Dictionary<string, string> GetDocMarshalingIdentifier()
        {
            if (!docIdentified)
            {
                docIdentified = true;

                for (int i = 0; i < docMarshalingIdentifier.Length; i++)
                {
                    string value = GetValue(docMarshalingIdentifier[i]);
                    idValues.Add(docMarshalingIdentifier[i], value);
                }
            }
            return idValues;
        }

        protected void ParseSourceXML(XmlElement srcPage)
        {
            for (int i = 0; i < srcPage.Attributes.Count; i++)
            {
                colValues.Add(srcPage.Attributes[i].Name, srcPage.Attributes[i].Value);
            }
            classIdentifier = contxt.configMgr.GetDocClassIdentifier();
            documentIdentifier = contxt.configMgr.GetDocumentIdentifier();
            docMarshalingIdentifier = contxt.configMgr.GetDocMarshalingIdentifier();
            docTypeIdentifier = contxt.configMgr.GetDocTypeIdentifier();
            docDateIdentifier = contxt.configMgr.GetDocDateIdentifier();
            pageNumberIdentifier = contxt.configMgr.GetPageNumberIdentifier();
            if (pageNumberIdentifier.Length > 0)
            {
                PageNumber = Convert.ToInt32(GetValue(pageNumberIdentifier));
            }
            string identifier = contxt.configMgr.GetPageIdentifier();
            if (identifier.Length > 0)
            {
                PageID = GetValue(identifier).Trim();
            }
            identifier = contxt.configMgr.GetFileNameIdentifier();
            if (identifier.Length > 0)
            {
                FileName = GetValue(identifier).Trim();
            }
            if (FileName.Length == 0)
            {
                UpdateDocStatus(ConversionResult.CONV_ERRORED);
                throw new ArgumentException("File name not specified");
            }
            identifier = contxt.configMgr.GetMimeTypeIdentifier();
            if (identifier.Length > 0)
            {
                MimeType = GetValue(identifier);
            }
            pageStatusIdentifier = contxt.configMgr.GetPageStatusIdentifier();
            identifier = contxt.configMgr.GetContainmentNameIdentifier();
            if (identifier.Length > 0)
            {
                ContainmentName = Sanitize(GetValue(identifier));
            }
            if (ContainmentName.Length == 0)
            {
                UpdateDocStatus(ConversionResult.CONV_ERRORED);
                throw new ArgumentException("Containment name not specified");
            }
            legacyDocIdentifier = contxt.configMgr.GetLegacyDocIdentifier();
            identifier = contxt.configMgr.GetDocRotationIdentifier();
            if (identifier.Length > 0)
            {
                string angle = GetValue(identifier);
                RotationAngle = (angle.Length == 0) ? 0 : Convert.ToInt32(angle);
            }
            identifier = contxt.configMgr.GetDocRotationDirectionIdentifier();
            if (identifier.Length > 0)
            {
                DirectionToRotate = GetDirection(GetValue(identifier));
            }
            docPreAnnRotationIdentifier = contxt.configMgr.GetDocPreAnnRotationIdentifier();
            identifier = contxt.configMgr.GetDocPreAnnRotationDirectionIdentifier();
            if (identifier.Length > 0)
            {
                PreAnnotationRotationDirection = GetDirection(GetValue(identifier));
            }
        }

        protected void AnalyzeContent()
        {
            string filePath = GetFilePath();
            LogDebugMessage("SourcePage: AnalyzeContent: File Path: " + filePath);
            FileInfo fi = new FileInfo(filePath);
            if (fi.Length == 0)
            {
                hasContent = false;
                LogMessage("Empty file: " + filePath);
                extension = GetEffectiveFileExtension();
                UpdateDocStatus(ConversionResult.CONV_DONE);    // so that this is out of the way
            }
            else
            {
                try
                {
                    using (RasterCodecs codecs = new RasterCodecs())
                    {
                        codecs.Options.Txt.Load.FontColor = new RasterColor(0, 0, 0);
                        codecs.Options.Txt.Load.FontSize = 12;
                        codecs.Options.Txt.Load.Bold = true;
                        codecs.Options.Txt.Load.Enabled = true;

                        CodecsImageInfo info = codecs.GetInformation(filePath, true);
                        
                        isDocumentFile = info.Document.IsDocumentFile;
                        format = info.Format;
                        compression = info.Compression;
                        bpp = info.BitsPerPixel;
                        totalPages = info.TotalPages;
                        xResolution = info.XResolution;
                        yResolution = info.YResolution;
                        height = info.Height;
                        width = info.Width;
                        //
                        RasterImageFormat ColorFormat = (RasterImageFormat) contxt.leadToolsColorFormat;
                        RasterImageFormat BWFormat = (RasterImageFormat) contxt.leadToolsBWFormat;
                        bool isColored = false;
                        switch (info.Format)
                        {
                            case Leadtools.RasterImageFormat.ExifJpeg:
                            case Leadtools.RasterImageFormat.ExifJpeg411:
                            //case Leadtools.RasterImageFormat.ExifJpeg422:
                                if (info.Compression == "JPEG")
                                {
                                    if (info.BitsPerPixel == 1)
                                    {
                                        format = BWFormat;
                                    }
                                    else
                                    {
                                        isColored = true;
                                        format = ColorFormat;
                                    }
                                }
                                else
                                {
                                    //coded = false;
                                }

                                break;

                            case Leadtools.RasterImageFormat.Jpeg:
                            case Leadtools.RasterImageFormat.Jpeg411:
                            case Leadtools.RasterImageFormat.Jpeg422:
                                if (info.Compression == "JPEG")
                                {
                                    if (info.BitsPerPixel == 1)
                                    {
                                        format = BWFormat;
                                    }
                                    else
                                    {
                                        isColored = true;
                                        format = ColorFormat;
                                    }
                                }
                                else
                                {
                                    //coded = false;
                                }

                                break;

                            case Leadtools.RasterImageFormat.CcittGroup4:
                                if (info.Compression == "CCITT Group 4 Fax")
                                {
                                    if (info.BitsPerPixel == 1)
                                    {
                                        format = BWFormat;
                                    }
                                    else
                                    {
                                        isColored = true;
                                        format = ColorFormat;
                                    }
                                }
                                else
                                {
                                    //coded = false;
                                }

                                break;

                            case Leadtools.RasterImageFormat.TifJpeg:
                            case Leadtools.RasterImageFormat.TifJpeg411:
                            case Leadtools.RasterImageFormat.TifJpeg422:
                                if (info.Compression == "JPEG")
                                {
                                    if (info.BitsPerPixel == 1)
                                    {
                                        format = BWFormat;
                                    }
                                    else
                                    {
                                        isColored = true;
                                        format = ColorFormat;
                                    }
                                }
                                else
                                {
                                    //coded = false;
                                }

                                break;

                            case Leadtools.RasterImageFormat.TifLzw:
                                if (info.Compression == "LZW")
                                {
                                    if (info.BitsPerPixel == 1)
                                    {
                                        format = BWFormat;
                                    }
                                    else
                                    {
                                        isColored = true;
                                        format = ColorFormat;
                                    }
                                }
                                else
                                {
                                    //coded = false;
                                }

                                break;

                            case Leadtools.RasterImageFormat.Tif:
                                if (info.BitsPerPixel == 1)
                                {
                                    format = BWFormat;
                                }
                                else
                                {
                                    isColored = true;
                                    format = ColorFormat;
                                }

                                break;

                            default:
                                //coded = false;
                                break;

                        }

                        if (isColored)
                        {
                            bpp = 24;
                        }
                        else
                        {
                            bpp = 1;
                        }
                        //
                        LogDebugMessage("SourcePage: AnalyzeContent: Is Document File: " + isDocumentFile.ToString());
                        LogDebugMessage("SourcePage: AnalyzeContent: Format: " + format.ToString());
                        LogDebugMessage("SourcePage: AnalyzeContent: Compression: " + compression.ToString());
                        LogDebugMessage("SourcePage: AnalyzeContent: Bits Per Pixel: " + bpp.ToString());
                        LogDebugMessage("SourcePage: AnalyzeContent: Total Pages: " + totalPages.ToString());
                        effectiveExtension = contxt.configMgr.DetermineExtension(format.ToString(), compression);
                        LogDebugMessage("SourcePage: AnalyzeContent: Effective Extension: " + effectiveExtension.ToString());
                    }
                }
                catch (Exception ex)
                {
                    LogDebugMessage("SourcePage: AnalyzeContent: Leadtools failed, Exception: " + ex.Message);
                    string fileExt = GetEffectiveFileExtension();
                    if (fileExt.Length > 0)
                    {
                        try
                        {
                            effectiveExtension = contxt.configMgr.DetermineExtension(fileExt, null);
                            LogDebugMessage("SourcePage: AnalyzeContent: AnalyzeContent By Extension " + filePath + " MimeType: " + effectiveExtension);
                        }
                        catch (Exception e)
                        {
                            throw new Exception("AnalyzeContent File: " + filePath + " Extension: " + fileExt + "(" + e.Message + ")");
                        }
                    }
                    else
                    {
                        throw new Exception("AnalyzeContent File: " + filePath + " Extension: None (" + ex.Message + ")");
                    }
                }
                finally
                {
                    ApplyExtensionExceptions();
                }
            }
        }

        public void ApplyExtensionExceptions()
        {
            string fileExt = GetEffectiveFileExtension();
            extension = contxt.configMgr.IsMarshalingExempt(fileExt) ? fileExt : effectiveExtension;
            if ((extension == "htm") || (extension == "html"))
            {
                extension = "htm";
            }
            LogDebugMessage("SourcePage: ApplyExtensionExceptions: Extension after applying exceptions: " + extension);
        }

        private string GetEffectiveFileExtension()
        {
            if(string.IsNullOrEmpty(MimeType))
            {
                string ext = (Path.GetExtension(FileName));
                if (ext.Length > 0)
                    return ext.Substring(1).ToLower();
                else
                    return string.Empty;
            }
            else return MimeType.ToLower();
        }

        public string GetValue(string srcColName)
        {
            return (colValues.ContainsKey(srcColName)) ? colValues[srcColName] : string.Empty;
        }

        public Int32 GetDBID()
        {
            return Convert.ToInt32(GetValue("ID"));
        }

        public string GetSourceIdString()
        {
            Dictionary<string, string> docID = GetDocMarshalingIdentifier();

            string result = "";
            Boolean first = true;
            foreach (string k in docID.Keys)
            {
                if (first)
                {
                    result = result + k + ": " + docID[k];
                    first = false;
                }
                else
                {
                    result = result + " + " + k + ": " + docID[k];
                }
            }
            return result;
        }

        public void CheckConsistency(string prop, string srcColName, string refValue)
        {
            if (srcColName != pageNumberIdentifier)
            {
                string value = GetValue(srcColName);
                if ((value.Trim() != refValue) && (srcColName != docDateIdentifier))
                {
                    contxt.logger.LogWarning(contxt.batchID, "Line " + GetDBID().ToString() + ": Page " + PageNumber + " has property " + prop + " set to: " + value + " which is different from that used by the document: " + refValue);
                }
            }
        }

        public string GetPageIdentifier()
        {
            return PageID;
        }

        public string GetFileName()
        {
            return FileName;
        }

        public string GetFileExtension()
        {
            return MimeType;
        }

        public string GetFilePath()
        {
            return (srcContentPathPrefix.Length == 0) ? FileName : srcContentPathPrefix + "\\" + FileName;
        }

        public string GetDocumentClass()
        {
            return (classIdentifier.Length > 0) ? GetValue(classIdentifier) : string.Empty;
        }

        public string GetContainmentName()
        {
            return ContainmentName;
        }

        public string GetLegacyDocID()
        {
            return Sanitize(GetValue(legacyDocIdentifier).Trim());
        }

        public Boolean IsPageGood()
        {
            //return ((colValues[PageStatusIdentifier] == "BadSimple") ? false : true);
            return hasContent;
        }

        public Boolean IsMarshalingExempt()
        {
            return contxt.configMgr.IsMarshalingExempt(extension);
        }

        public int GetRotationAngle()
        {
            return RotationAngle;
        }

        public RotationDirection GetRotationDirection()
        {
            return DirectionToRotate;
        }

        public int GetPreAnnRotationAngle()
        {
            string angle = GetValue(docPreAnnRotationIdentifier).Trim();
            return (angle.Length == 0) ? 0 : Convert.ToInt32(angle);
        }

        public RotationDirection GetPreAnnRotationDirection()
        {
            return PreAnnotationRotationDirection;
        }

        public void UpdateDocStatus(ConversionResult convResult)
        {
            DBAdapter.DBAdapter.UpdateDB(contxt.dbConnection,
                "UPDATE " + contxt.jobName + "_DocumentData" + " SET Status = '" + convResult.ToString() + "' WHERE ID = " + colValues["ID"]);
        }

        public string GetExtension()
        {
            return extension;
        }

        public RasterImageFormat GetFormat()
        {
            return format;
        }

        public string GetCompression()
        {
            return compression;
        }

        public MMInterfaces.SourceProperties GetColumnValues()
        {
            MMInterfaces.SourceProperties result = new MMInterfaces.SourceProperties();
            foreach (string k in colValues.Keys)
            {
                result.Values.Add(k, colValues[k]);
            }
            return result;
        }

        public Boolean IsDocumentFile()
        {
            return isDocumentFile;
        }

        public RasterImageFormat DetermineTargetFormat(RasterImageFormat docFormat)
        {
            RasterImageFormat result = contxt.configMgr.PromoteLeadFormat(docFormat, format, compression);
            return result;
        }

        public int BitsPerPixel()
        {
            return bpp;
        }

        public int TotalPages()
        {
            return totalPages;
        }

        public void LogMarshalingInfo(string docID)
        {
            DBAdapter.DBAdapter.AddToDB(contxt.dbConnection, "INSERT INTO " + contxt.jobName + "_MarshalingInfo (DocumentID, LegacyPageID) VALUES ( " + docID + ", '" + PageID + "')");
        }

        public static string Sanitize(string input)
        {
            return rgxSingleQuote.Replace(input, repSingleQuote);
        }

        public static RotationDirection GetDirection(string input)
        {
            switch (input)
            {
                case "C":
                    return RotationDirection.Clockwise;

                case "CC":
                    return RotationDirection.CounterClockwise;

                default:
                    return RotationDirection.None;
            }
        }

        public int XResolution()
        {
            return xResolution;
        }

        public int YResolution()
        {
            return yResolution;
        }

        public int Width()
        {
            return width;
        }

        public int Height()
        {
            return height;
        }
    }
}
