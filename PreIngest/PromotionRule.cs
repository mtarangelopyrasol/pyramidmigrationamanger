﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Leadtools;
using Leadtools.Codecs;

namespace PreIngest
{
    public class PromotionRule
    {
        public PromotionRule(string docFmt, string pageFmt, string pageCmp, string promotedFmt)
        {
            DocumentFormat = (docFmt == "ANY") ? RasterImageFormat.Unknown : (RasterImageFormat)Enum.Parse(typeof(RasterImageFormat), docFmt, true);
            PageFormat = (pageFmt == "ANY") ? RasterImageFormat.Unknown : (RasterImageFormat)Enum.Parse(typeof(RasterImageFormat), pageFmt, true);
            PageCompression = pageCmp;
            PromotedFormat = (promotedFmt == "ANY") ? RasterImageFormat.Unknown : (RasterImageFormat)Enum.Parse(typeof(RasterImageFormat), promotedFmt, true);
        }

        public RasterImageFormat DocumentFormat { get; set; }
        public RasterImageFormat PageFormat { get; set; }
        public string PageCompression { get; set; }
        public RasterImageFormat PromotedFormat { get; set; }
    }
}
