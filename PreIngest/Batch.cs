﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml;

namespace PreIngest
{
    class Batch : ClassBase
    {
        protected CMCases cmCases = null;
        protected CMDocuments cmDocs = null;
        protected int batchSize = 0;
        protected string batchCondition = string.Empty;
        protected string srcContentPrefixPath = string.Empty;
        protected string stagingPathDefn = string.Empty;
        
        public Batch(Context c) : base(c)
        {
            if (contxt.configLevel == "CASE")
                cmCases = new CMCases(contxt);
            else
                cmDocs = new CMDocuments(contxt, null, null, null);
        }
        
        public ConversionResult PerformConversion()
        {
            ConversionResult result = ConversionResult.CONV_NEW;

            try
            {
                LogMessage("Batch ID: " + contxt.batchID.ToString() + " Starting Conversion " + DateTime.Now.ToString());
                GetBatchDefinition();
                result = (contxt.configLevel == "CASE") ? cmCases.PerformConversion(batchSize, batchCondition, srcContentPrefixPath, FormulateStagingPath()) :
                    cmDocs.PerformConversion(batchSize, batchCondition, srcContentPrefixPath, FormulateStagingPath());
                if (result != ConversionResult.CONV_CANCELED)
                {
                    if (contxt.configLevel == "CASE")
                    {
                        if (cmCases.ItemsProcessed)
                        {
                            LogMessage("Successfully Converted Cases: " + cmCases.GetSuccessCount());
                            LogMessage("Errored Cases: " + cmCases.GetErrorCount());
                            LogMessage("Total Cases Processed: " + cmCases.GetCaseCount());
                        }
                    }
                    else
                    {
                        if (cmDocs.ItemsProcessed)
                        {
                            LogMessage("Successfully Converted Documents: " + cmDocs.GetSuccessCount());
                            LogMessage("Errored Documents: " + cmDocs.GetErrorCount());
                            LogMessage("Total Target Documents Formulated: " + cmDocs.GetDocCount());
                        }
                    }
                    LogMessage("Batch ID: " + contxt.batchID.ToString() + " Complete " + DateTime.Now.ToString());
                }
            }
            catch (Exception ex)
            {
                LogError("Batch ID: " + contxt.batchID.ToString() + " " + ex.Message);
                LogError("Skipping further processing of Batch ID: " + contxt.batchID.ToString());
                result = ConversionResult.CONV_ERRORED;
            }
            finally
            {
                UpdateBatchStatus(result);
            }
            return result;
        }

        protected int GetBatchDefnId()
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(contxt.dbConnection, "SELECT BatchDefnID FROM MgrBatches WHERE ID = " + contxt.batchID);
            
            return Convert.ToInt32(xmlDoc.SelectNodes("//row")[0].Attributes["BatchDefnID"].Value);
        }

        protected void GetBatchDefinition()
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(contxt.dbConnection, 
                "SELECT BatchSize, BatchCondition, SourceContentPathPrefix, StagingPath FROM MgrBatchDefns WHERE ID = " + GetBatchDefnId());
            
            batchSize = Convert.ToInt32(xmlDoc.SelectNodes("//row")[0].Attributes["BatchSize"].Value);
            batchCondition = xmlDoc.SelectNodes("//row")[0].Attributes["BatchCondition"].Value;
            srcContentPrefixPath = xmlDoc.SelectNodes("//row")[0].Attributes["SourceContentPathPrefix"].Value;
            stagingPathDefn = xmlDoc.SelectNodes("//row")[0].Attributes["StagingPath"].Value;
            if (!Directory.Exists(stagingPathDefn))
            {
                throw new Exception("Staging path does not exist: " + stagingPathDefn);
            }
        }

        protected int GetJobId()
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(contxt.dbConnection, "SELECT JobID FROM MgrBatches WHERE ID = " + contxt.batchID);

            return Convert.ToInt32(xmlDoc.SelectNodes("//row")[0].Attributes["JobID"].Value);
        }

        protected string FormulateStagingPath()
        {
            if (stagingPathDefn.Substring((stagingPathDefn.Length - 1), 1) != "\\")
            {
                stagingPathDefn += "\\"; // Add trailing \ if necessary
            }
            string subFolderName = "Batch_" + contxt.batchID.ToString();
            string stagingPath = stagingPathDefn + subFolderName + "\\Images";

            if (Directory.Exists(stagingPath))
            {
                string[] files = Directory.GetFiles(stagingPath, "*.*");
                if (files.Length > 0)
                {
                    throw new ArgumentException("Folder " + stagingPath + " already exists and has files. Skipping conversion");
                }
            }
            else
            {
                try
                {
                    Directory.CreateDirectory(stagingPath);
                }
                catch (Exception ex)
                {
                    throw new ArgumentException("Could not create subfolder " + subFolderName + "\\Images in folder " + stagingPathDefn + ". " + ex.Message);
                }
            }
            return stagingPath;
        }

        protected void UpdateBatchStatus(ConversionResult convResult)
        {
            if (contxt.configLevel == "CASE")
            {
                DBAdapter.DBAdapter.UpdateDB(contxt.dbConnection,
                    "UPDATE MgrBatches SET ActualSize = " + cmCases.GetCaseCount() + ", EndTime = GETDATE(), Status = '" + convResult.ToString() + "', Comment = '" + cmCases.GetSuccessCount() + " cases successfully processed" + "' WHERE ID = " + contxt.batchID.ToString());
            }
            else
            {
                DBAdapter.DBAdapter.UpdateDB(contxt.dbConnection,
                    "UPDATE MgrBatches SET ActualSize = " + cmDocs.GetDocCount() + ", EndTime = GETDATE(), Status = '" + convResult.ToString() + "', Comment = '" + cmDocs.GetSuccessCount() + " documents successfully processed" + "' WHERE ID = " + contxt.batchID.ToString());
            }
        }
    }
}
