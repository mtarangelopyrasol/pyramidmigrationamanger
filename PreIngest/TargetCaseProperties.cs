﻿using System;
using System.Collections.Generic;

namespace PreIngest
{
    class TargetCaseProperties : TargetProperties
    {
        public TargetCaseProperties(Context c) : base(c)
        {
        }

        public override TargetProperty AddProperty(string n, string v, string t)
        {
            TargetCaseProperty tp = new TargetCaseProperty(contxt, n, GetConvertedValue(n, v, t), t);
            properties.Add(tp);
            return tp;
        }
    }
}
