﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PreIngest
{
    abstract class TargetProperty : ClassBase
    {
        public TargetProperty(Context c, string n, string v, string t) : base(c)
        {
            Name = n;
            Value = v;
            DataType = t;
        }

        public string Name { get; set; }
        
        public string Value { get; set; }

        public string DataType { get; set; }

        abstract public void Serialize(string objectID);
        
        protected Boolean IsValueNull()
        {
            return ((Value == null) || (Value.Length == 0) || (Value == "NULL") || (Value.Trim().Length == 0)) ? true : false;
        }
    }
}
