﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using MMInterfaces;

namespace PreIngest
{
    class CMAnnotation : CMEntity
    {
        protected AnnotatedObjectType annotatedObjectType = AnnotatedObjectType.NONE;
        protected AnnotationTargetObjectType targetObjectType = AnnotationTargetObjectType.NONE;
        protected string legacyAnnotatedObjectID = string.Empty;
        protected string legacyFileName = string.Empty;
        protected string legacyPageID = string.Empty;
        protected string annotatedObjectMMID = string.Empty;
        protected int sequenceNumber = 0;
        List<string> targetFileNames;
        string histField = string.Empty;
        string histFieldType = string.Empty;

        public CMAnnotation(Context c, string parentID, int seqNum) : base(c)
        {
            LogDebugMessage("CMAnnotation: New Annotation: Parent: " + parentID + " Sequence Number: " + seqNum);
            annotatedObjectMMID = parentID;
            sequenceNumber = seqNum;
            targetFileNames = new List<string>();
        }

        public override void SetSource(XmlElement sourceXML)
        {
            PrepareSourceProperties(sourceXML);
            legacyPageID = GetSourceValue(contxt.configMgr.GetAnnotationPageIdentifier());
            legacyFileName = GetSourceValue(contxt.configMgr.GetAnnotationFileNameIdentifier());
        }

        public ConversionResult PerformConversion(AnnotatedObjectType aot, string parentLegacyID, string srcContentPrefixPath, string stagingPath, TargetProperties docProperties, string docClass, SourcePages srcPages, TargetPages tgtPages)
        {
            LogDebugMessage("CMAnnotation: PerformConversion: Parent: " + annotatedObjectMMID + " Sequence Number: " + sequenceNumber + " Annotation Legacy Record Number : " + GetSourceValue("ID"));
            legacyAnnotatedObjectID = parentLegacyID;
            annotatedObjectType = aot;

            DetermineType();
            DetermineTargetObjectType();
            
            return (targetType == "Annotation") ?
                ConvertBaseType(srcContentPrefixPath, stagingPath, docProperties, docClass, srcPages, tgtPages) :
                ConvertInheritedType(srcContentPrefixPath, stagingPath, tgtPages);
        }

        protected ConversionResult ConvertBaseType(string srcContentPrefixPath, string stagingPath, TargetProperties docProperties, string docClass, SourcePages srcPages, TargetPages tgtPages)
        {
            LogDebugMessage("CMAnnotation: ConvertBaseType: Parent: " + annotatedObjectMMID + " Sequence Number: " + sequenceNumber + " Annotation Legacy Record Number : " + GetSourceValue("ID"));
            ConversionResult result = ConversionResult.CONV_NEW;

            annotatedObjectType = AnnotatedObjectType.DOCUMENT;
            
            try
            {
                FormulateP8Annotation(srcContentPrefixPath, stagingPath, docClass, docProperties, srcPages.GetPageWithID(legacyPageID), tgtPages.FindPage(legacyPageID));
                Serialize();
                
                result = ConversionResult.CONV_DONE;
            }
            catch (Exception ex)
            {
                result = ConversionResult.CONV_ERRORED;
                string message = (legacyAnnotatedObjectID.Length > 0) ?
                                    "Error: (Account: " + legacyAnnotatedObjectID + " Annotation Source ID: " + GetSourceValue("ID") + ") " : 
                                    "Error: Annotation Source ID: " + GetSourceValue("ID") + ") ";
                LogError(message + ex.Message);
            }
            finally
            {
                LogDebugMessage("CMAnnotation: ConvertBaseType: Updating Source Status: " + result.ToString() + " ID: " + GetSourceValue("ID"));
                UpdateSourceStatus(result, contxt.jobName + "_AnnotationData");
            }
            return result;
        }

        // all other annotations
        protected ConversionResult ConvertInheritedType(string srcContentPrefixPath, string stagingPath, TargetPages tgtPages)
        {
            LogDebugMessage("CMAnnotation: ConvertInheritedType: Parent: " + annotatedObjectMMID + " Sequence Number: " + sequenceNumber + " Annotation Legacy Record Number : " + GetSourceValue("ID"));
            ConversionResult result = ConversionResult.CONV_NEW;

            if (!IsTargetTypeDefined()) return result;

            try
            {
                switch (targetObjectType)
                {
                    case AnnotationTargetObjectType.ANNOTATION:
                        ICommentReader reader = GetCommentReader();
                        if (reader != null)
                            FormulateP8Comments(srcContentPrefixPath, reader);
                        else
                            FormulateTargetAnnotationProperties(tgtPages);
                        result = ConversionResult.CONV_DONE;
                        break;

                    case AnnotationTargetObjectType.DOCUMENT:
                        LogDebugMessage("CMAnnotation: ConvertInheritedType: Calling GenerateP8Document");
                        GenerateP8Document(srcContentPrefixPath, stagingPath);
                        result = ConversionResult.CONV_DONE;
                        break;

                    default:
                        result = ConversionResult.CONV_ERRORED;
                        LogError("Error: Unknown target object type");
                        break;
                }
                if ((result == ConversionResult.CONV_DONE) || (result == ConversionResult.CONV_COMPLETE))
                {
                    LogMessage("Legacy annotation converted to P8 annotation " + targetType);
                }
            }
            catch (Exception ex)
            {
                result = ConversionResult.CONV_ERRORED;
                LogError("Error: (Annotation for: " + legacyAnnotatedObjectID + ") " + ex.Message);
            }
            finally
            {
                LogDebugMessage("CMAnnotation: ConvertInheritedType: Updating Source Status: " + result.ToString() + " ID: " + GetSourceValue("ID"));
                UpdateSourceStatus(result, contxt.jobName + "_AnnotationData");
            }
            return result;
        }

        protected override void DetermineType()
        {
            if (contxt.annTypeSelector != null)
            {
                targetType = contxt.annTypeSelector.Select(srcProperties);
            }
            else
            {
                string identifier = contxt.configMgr.GetAnnotationClassIdentifier();
                if (identifier.Length > 0)
                {
                    targetType = GetSourceValue(identifier);
                }
                if (targetType.Length == 0)
                {
                    targetType = contxt.configMgr.GetAnnotationClass();
                }
            }
            if (targetType == string.Empty)
            {
                targetType = "Annotation";
            }
        }

        protected override Boolean IsTargetTypeDefined()
        {
            if (targetType == "Annotation") return true;
            else if (targetObjectType == AnnotationTargetObjectType.ANNOTATION)
            {
                if (contxt.configMgr.IsAnnotationClassDefined(targetType)) return true;
                else
                {
                    throw new ArgumentException("Annotation Class " + targetType + " is not defined in the configuration");
                }
            }
            else if (targetObjectType == AnnotationTargetObjectType.DOCUMENT)
            {
                if (contxt.configMgr.IsDocumentClassDefined(targetType)) return true;
                else
                {
                    throw new ArgumentException("Document Class " + targetType + " is not defined in the configuration");
                }
            }
            else return false;
        }

        protected void DetermineTargetObjectType()
        {
            try
            {
                targetObjectType = (AnnotationTargetObjectType)Enum.Parse(typeof(AnnotationTargetObjectType),
                                        GetSourceValue(contxt.configMgr.GetAnnotationTargetObjectTypeIdentifier()));
            }
            catch
            {
                targetObjectType = (legacyFileName.Length > 0) ? AnnotationTargetObjectType.ANNOTATION : AnnotationTargetObjectType.NONE;
            }
            if (targetObjectType == AnnotationTargetObjectType.NONE)
            {
                if (contxt.configMgr.IsADocumentClass(targetType)) targetObjectType = AnnotationTargetObjectType.DOCUMENT;
                else if (contxt.configMgr.IsAnAnnotationClass(targetType)) targetObjectType = AnnotationTargetObjectType.ANNOTATION;
                else throw new ArgumentException("Unable to determine the target object type. Specify the AnnotationTargetObjectIdentifer");
            }
        }

        protected void FormulateTargetAnnotationProperties(TargetPages tgtPages)
        {
            targetProperties = new TargetAnnProperties(contxt);
                
            List<ConfigProperty> lcps = contxt.configMgr.GetAnnotationProperties(targetType);
            foreach (ConfigProperty cp in lcps)
            {
                if (contxt.backgroundWorker.CancellationPending) break;

                if (cp.IsComputed())
                {
                    if (cp.GetSource() == "PAGE_NUMBER")
                        AddTargetProperty(cp, tgtPages.FindPage(legacyPageID).GetSourceSequenceNumber(legacyPageID).ToString(), "Annotation: " + GetSourceValue("ID"));
                    else if (cp.GetSource() == "WINDOW")
                        AddTargetProperty(cp, "page=" + tgtPages.FindPage(legacyPageID).GetSourceSequenceNumber(legacyPageID).ToString() + "&amp;zoom=auto", "Annotation: " + GetSourceValue("ID"));
                    else
                        FormulateComputedProperty(cp, contxt.annPropertyEvaluator, "Annotation: " + GetSourceValue("ID"));
                }
                else
                {
                    FormulateProperty(cp, contxt.jobName + "_DocumentData_", "Annotation: " + GetSourceValue("ID"));
                }
            }
            Serialize();
        }

        protected TargetDocProperties FormulateGeneratedDocumentProperties()
        {
            TargetDocProperties properties = new TargetDocProperties(contxt);
            
            List<ConfigProperty> lcps = contxt.configMgr.GetDocumentProperties(targetType);
            foreach (ConfigProperty cp in lcps)
            {
                if (contxt.backgroundWorker.CancellationPending) break;
                
                if (cp.IsComputed())
                    FormulateComputedDocProperty(cp, properties);
                else
                    FormulateDocProperty(cp, properties);
            }
            return properties;
        }

        protected void FormulateComputedDocProperty(ConfigProperty cp, TargetDocProperties properties)
        {
            if (contxt.docPropertyEvaluator == null)
            {
                string src = cp.GetSource();
                if ((src != "MMID") && (src != "ANNOTATION_HISTORY"))
                {
                    entityStatus = EntityStatus.DISCARD;
                    throw new ArgumentException("Could not find evaluator for computed Property " + cp.Name + ". Could not generate document.");
                }
            }
            else
            {
                LogDebugMessage("CMAnnotation: FormulateComputedDocProperty: Calling DocPropertyEvaluator to evaluate " + cp.Name);
                List<string> valueList = contxt.docPropertyEvaluator.Evaluate(cp.Name, cp.GetDataType(), srcProperties);
                LogDebugMessage("CMAnnotation: FormulateComputedDocProperty: Number of values in evaluated valueList " + valueList.Count.ToString());
                foreach (string value in valueList)
                {
                    if (cp.IsRequired())
                    {
                        if ((value == null) || (value.Trim().Length == 0))
                        {
                            entityStatus = EntityStatus.DISCARD;
                            throw new ArgumentException("Annotation: " + legacyAnnotatedObjectID + " Generated document does not have value for required Property " + cp.Name + ". Discarding this case.");
                        }
                    }
                    LogDebugMessage("CMAnnotation: FormulateComputedDocProperty: Adding value " + value + " to list of generated documents properties");
                    properties.AddProperty(cp.Name, value, cp.GetDataType());
                }
            }
        }

        protected void FormulateDocProperty(ConfigProperty cp, TargetDocProperties properties)
        {
            string source = cp.GetSource();
            string fldValue = (source != string.Empty) ? GetSourceValue(source).Trim() : string.Empty;

            List<string> values;

            if (cp.IsMultiValued())
            {
                values = FetchValues(contxt.jobName + "_AnnotationData_" + source, fldValue);
            }
            else
            {
                values = new List<string>();
                values.Add(fldValue);
            }
            foreach (string value in values)
            {
                if (cp.IsRequired())
                {
                    if ((value == null) || (value.Trim().Length == 0))
                    {
                        entityStatus = EntityStatus.DISCARD;
                        throw new ArgumentException("Annotation: " + legacyAnnotatedObjectID + " Generated document does not have value for required Property " + cp.Name + ". Discarding this case.");
                    }
                }
                properties.AddProperty(cp.Name, value, cp.GetDataType());
            }
        }

        protected ICommentReader GetCommentReader()
        {
            ICommentReader reader = null;

            switch (annotatedObjectType)
            {
                case AnnotatedObjectType.CASE:
                    reader = contxt.caseCommentReader;
                    break;

                case AnnotatedObjectType.DOCUMENT:
                    reader = contxt.docCommentReader;
                    break;

                case AnnotatedObjectType.TASK:
                    reader = contxt.taskCommentReader;
                    break;

                default:
                    break;
            }
            return reader;
        }

        protected void FormulateP8Comments(string sourcePrefix, ICommentReader reader)
        {
            if (entityStatus == EntityStatus.DISCARD) return;

            string legacyAnnPath = GetLegacyAnnotationFilePath(sourcePrefix);
            
            if (!(reader.SetCommentFile(legacyAnnPath, srcProperties)))
            {
                throw new ArgumentException("Exception parsing Notes file: " + legacyAnnPath);
            }
            
            long commentCount = reader.GetCommentCount();
            for (int i = 0; i < commentCount; i++)
            {
                targetProperties = new TargetAnnProperties(contxt);

                targetType = reader.GetClassName(i);
                targetProperties.AddProperty("CmAcmAction", reader.GetActionCode(i), "SingletonInteger");
                targetProperties.AddProperty("CmAcmText", reader.GetText(i), "SingletonString");
                Serialize();
            }
        }

        private PageInformation GatherPageInfo(SourcePage sp)
        {
            PageInformation pageInfo = new MMInterfaces.PageInformation();

            pageInfo.PreAnnotationRotation = (sp.GetPreAnnRotationDirection() == RotationDirection.Clockwise) ? -(sp.GetPreAnnRotationAngle()) : sp.GetPreAnnRotationAngle();
            pageInfo.RotationAngle = (sp.GetRotationDirection() == RotationDirection.Clockwise) ? -(sp.GetRotationAngle()) : sp.GetRotationAngle();
            pageInfo.PageWidth = sp.Width(); // in pixels
            pageInfo.PageHeight = sp.Height(); // in pixels
            pageInfo.XResolution = sp.XResolution();
            pageInfo.YResolution = sp.YResolution();
            return pageInfo;
        }

        protected void FormulateP8Annotation(string sourcePrefix, string targetStagingPath, string docClass, TargetProperties docProperties, SourcePage srcPage, TargetPage tgtPage)
        {
            if (entityStatus == EntityStatus.DISCARD) return;

            string legacyAnnPath = GetLegacyAnnotationFilePath(sourcePrefix);
            
            IAnnotationReader reader = contxt.annotationReader;
            if (reader == null)
            {
                entityStatus = EntityStatus.DISCARD;
                throw new ArgumentException("Cannot read Annotation file: " + legacyAnnPath + ". No annotation reader specified in the config file. Discarding this annotation.");
            }

            try
            {
                if (!(reader.SetAnnotationFile(legacyAnnPath, GatherPageInfo(srcPage))))
                {
                    throw new ArgumentException("Exception parsing Annotation file: " + legacyAnnPath);
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Exception processing Annotation file: " + legacyAnnPath + " " + ex.Message);
            }

            int lpsn = 0;

            long annotationCount = contxt.annotationReader.GetAnnotationCount();
            for (int i = 0; i < annotationCount; i++)
            {
                P8AnnotationType annType = reader.GetTargetAnnotationType(i);
                if (annType != P8AnnotationType.None)
                {
                    string targetFileName = FormulateTargetFileName(targetStagingPath, i);
                    targetFileNames.Add(targetFileName);

                    XmlTextWriter xtw = new XmlTextWriter(targetFileName, Encoding.Unicode);
                    xtw.Formatting = Formatting.Indented;
                    xtw.IndentChar = ' ';
                    xtw.Indentation = 4;
                    xtw.WriteStartDocument();

                    xtw.WriteStartElement("FnAnno");
                    xtw.WriteStartElement("PropDesc");

                    lpsn = tgtPage.GetSourceSequenceNumber(legacyPageID);
                    WriteCommonProperties(xtw, reader, i, lpsn);
                    switch (annType)
                    {
                        case P8AnnotationType.Arrow:
                            WriteArrowProperties(xtw, reader, i, lpsn);
                            break;

                        case P8AnnotationType.Highlight:
                            WriteHighlightProperties(xtw, reader, i, lpsn);
                            break;

                        case P8AnnotationType.HighlightPolygon:
                            WriteHighlightPolygonProperties(xtw, reader, i, lpsn);
                            break;

                        case P8AnnotationType.Line:
                            WriteLineProperties(xtw, reader, i, lpsn);
                            break;

                        case P8AnnotationType.OpenPolygon:
                            WriteOpenPolygonProperties(xtw, reader, i, lpsn);
                            break;

                        case P8AnnotationType.Oval:
                            WriteOvalProperties(xtw, reader, i, lpsn);
                            break;

                        case P8AnnotationType.Pen:
                            WritePenProperties(xtw, reader, i, lpsn);
                            break;

                        case P8AnnotationType.Polygon:
                            WritePolygonProperties(xtw, reader, i, lpsn);
                            break;

                        case P8AnnotationType.Rectangle:
                            WriteRectangleProperties(xtw, reader, i, lpsn);
                            break;

                        case P8AnnotationType.SolidText:
                            WriteSolidTextProperties(xtw, reader, i, lpsn);
                            break;

                        case P8AnnotationType.Stamp:
                            WriteStampProperties(xtw, reader, i, lpsn);
                            break;

                        case P8AnnotationType.StickyNote:
                            WriteStickyNoteProperties(xtw, reader, i, lpsn);
                            break;

                        case P8AnnotationType.TransparentText:
                            WriteTransparentTextProperties(xtw, reader, i, lpsn);
                            break;

                        default:
                            throw new Exception("Unknown Annotation Type");
                    }
                    xtw.WriteEndElement(); // PropDesc
                    xtw.WriteEndElement(); // FnAnno
                    xtw.WriteEndDocument();
                    xtw.Flush();
                    xtw.Close();
                    
                    List<string> histList = reader.GetHistory(i);
                    if(histList.Count > 0)
                    {
                        histField = GetHistoryField(docClass);
                        
                        if (histField != String.Empty)
                        {
                            foreach (string hist in histList) {
                                TargetDocProperty tdp = (TargetDocProperty)docProperties.AddProperty(histField, hist, histFieldType);
                                tdp.Serialize(annotatedObjectMMID);
                            }
                        }
                    }
                    
                    LogMessage("Legacy annotation " + reader.GetLegacyName(i) + " converted to P8 annotation " + annType);
                }
                else
                {
                    LogMessage("Legacy annotation " + reader.GetLegacyName(i) + " on page " + lpsn.ToString() + " (legacy Page ID: " + legacyPageID + ") ignored");
                }
            }
        }

        protected void Serialize()
        {
            if (entityStatus == EntityStatus.DISCARD) return;

            if((targetType == "Annotation") || (targetObjectType == AnnotationTargetObjectType.DOCUMENT))
            {
                foreach (string targetFileName in targetFileNames)
                {
                    XmlDocument xml = DBAdapter.DBAdapter.FetchInsertedRec(contxt.dbConnection, "INSERT INTO " + contxt.jobName + "_CEAnnotations (BatchID, AnnType, AnnClass, PageID, ObjectID, FilePath, MMAccountID) OUTPUT inserted.* VALUES ( " + contxt.batchID + ", '" + annotatedObjectType.ToString() + "', '" + targetType + "', 1, '" + annotatedObjectMMID + "', '" + targetFileName + "', '" + legacyAnnotatedObjectID + "')");
                }
            }
            else
            {
                XmlDocument xml = DBAdapter.DBAdapter.FetchInsertedRec(contxt.dbConnection, "INSERT INTO " + contxt.jobName + "_CEAnnotations (BatchID, AnnType, AnnClass, PageID, ObjectID, FilePath, MMAccountID) OUTPUT inserted.* VALUES ( " + contxt.batchID + ", '" + annotatedObjectType.ToString() + "', '" + targetType + "', 1, '" + annotatedObjectMMID + "', '', '" + legacyAnnotatedObjectID + "')");
                mmID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                LogDebugMessage("CMAnnotation: Serialize: Serialized Annotation: " + mmID.ToString() + " Annotated Object ID: " + annotatedObjectMMID + " Annotation Type: " + targetType);
                targetProperties.Serialize(mmID);
            }
        }

        protected string FormulateTargetFileName(string targetStagingPath, int annNumber)
        {
            string fileName = "Doc" + annotatedObjectMMID + "_File" + sequenceNumber.ToString() + "_Annotation" + annNumber.ToString();

            return (targetStagingPath.Trim().Length == 0) ? fileName : targetStagingPath + "\\" + fileName;
        }

        protected string GetHistoryField(string docClass)
        {
            string histField = string.Empty;
            List<ConfigProperty> lcps = contxt.configMgr.GetDocumentProperties(docClass);
            foreach (ConfigProperty cp in lcps)
            {
                if (contxt.backgroundWorker.CancellationPending) break;

                if ((cp.IsComputed()) && (cp.GetSource() == "ANNOTATION_HISTORY"))
                {
                    histField = cp.Name;
                    histFieldType = cp.GetDataType();
                    break;
                }
            }
            return histField;
        }

        protected void WriteCommonProperties(XmlTextWriter xtw, IAnnotationReader reader, int annotationIndex, int legacyPageSequenceNum)
        {
            xtw.WriteAttributeString("F_ANNOTATEDID", "CE_ANNOTATION_GUID");
            xtw.WriteAttributeString("F_CLASSID", "CE_DOCUMENT_CLASS");
            xtw.WriteAttributeString("F_ID", "CE_ANNOTATION_GUID");
            xtw.WriteAttributeString("F_NAME", "-" + legacyPageSequenceNum.ToString() + "-" + (annotationIndex + 1).ToString());
            string creator = reader.GetCreator(annotationIndex);
            if(creator.Length == 0)
            {
                creator = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            }
            xtw.WriteAttributeString("F_CREATOR", creator);
            xtw.WriteAttributeString("F_PAGENUMBER", legacyPageSequenceNum.ToString());
            xtw.WriteAttributeString("F_MULTIPAGETIFFPAGENUMBER", legacyPageSequenceNum.ToString());
            xtw.WriteAttributeString("F_CLASSNAME", reader.GetClassName(annotationIndex));
            xtw.WriteAttributeString("F_ENTRYDATE", reader.GetEntryDate(annotationIndex));
            xtw.WriteAttributeString("F_MODIFYDATE", reader.GetModifyDate(annotationIndex));
        }

        protected void WriteSizeAndPosition(XmlTextWriter xtw, IAnnotationReader reader, int annotationIndex)
        {
            xtw.WriteAttributeString("F_HEIGHT", reader.GetHeight(annotationIndex).ToString("F"));
            xtw.WriteAttributeString("F_WIDTH", reader.GetWidth(annotationIndex).ToString("F"));
            xtw.WriteAttributeString("F_TOP", reader.GetTop(annotationIndex).ToString("F"));
            xtw.WriteAttributeString("F_LEFT", reader.GetLeft(annotationIndex).ToString("F"));
        }

        protected void WriteAdjustedSizeAndPosition(XmlTextWriter xtw, IAnnotationReader reader, int annotationIndex, double height, double width)
        {
            double top = reader.GetTop(annotationIndex);
            xtw.WriteAttributeString("F_TOP", top.ToString("F"));

            double left = reader.GetLeft(annotationIndex);
            xtw.WriteAttributeString("F_LEFT", left.ToString("F"));

            xtw.WriteAttributeString("F_HEIGHT", height.ToString("F"));
            xtw.WriteAttributeString("F_WIDTH", width.ToString("F"));
        }

        protected void WriteLineProperties(XmlTextWriter xtw, IAnnotationReader reader, int annotationIndex, int legacyPageSequenceNum)
        {
            WriteSizeAndPosition(xtw, reader, annotationIndex);
            xtw.WriteAttributeString("F_LINE_COLOR", reader.GetLineColor(annotationIndex));
            xtw.WriteAttributeString("F_LINE_START_X", reader.GetLineStartX(annotationIndex));
            xtw.WriteAttributeString("F_LINE_END_X", reader.GetLineEndX(annotationIndex));
            xtw.WriteAttributeString("F_LINE_START_Y", reader.GetLineStartY(annotationIndex));
            xtw.WriteAttributeString("F_LINE_END_Y", reader.GetLineEndY(annotationIndex));
            xtw.WriteAttributeString("F_LINE_WIDTH", reader.GetLineWidth(annotationIndex));
            xtw.WriteAttributeString("F_SUBCLASS", reader.GetSubClass(annotationIndex));
            xtw.WriteStartElement("F_CUSTOM_BYTES");
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_POINTS");
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_TEXT");
            xtw.WriteEndElement();
        }

        protected void WriteOpenPolygonProperties(XmlTextWriter xtw, IAnnotationReader reader, int annotationIndex, int legacyPageSequenceNum)
        {
            WriteSizeAndPosition(xtw, reader, annotationIndex);
            xtw.WriteAttributeString("F_LINE_COLOR", reader.GetLineColor(annotationIndex));
            xtw.WriteAttributeString("F_LINE_WIDTH", reader.GetLineWidth(annotationIndex));
            xtw.WriteAttributeString("F_SUBCLASS", reader.GetSubClass(annotationIndex));
            xtw.WriteAttributeString("F_TEXT_BACKMODE", reader.GetTextBackMode(annotationIndex));
            xtw.WriteStartElement("F_CUSTOM_BYTES");
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_POINTS");
            xtw.WriteString(reader.GetPoints(annotationIndex));
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_TEXT");
            xtw.WriteEndElement();
        }

        protected void WriteRectangleProperties(XmlTextWriter xtw, IAnnotationReader reader, int annotationIndex, int legacyPageSequenceNum)
        {
            WriteSizeAndPosition(xtw, reader, annotationIndex);
            xtw.WriteAttributeString("F_LINE_COLOR", reader.GetLineColor(annotationIndex));
            string brushColor = reader.GetBrushColor(annotationIndex);
            if (brushColor != "")
            {
                xtw.WriteAttributeString("F_BRUSHCOLOR", brushColor);
            }
            xtw.WriteAttributeString("F_LINE_WIDTH", reader.GetLineWidth(annotationIndex));
            xtw.WriteAttributeString("F_SUBCLASS", reader.GetSubClass(annotationIndex));
            xtw.WriteAttributeString("F_TEXT_BACKMODE", reader.GetTextBackMode(annotationIndex));
            xtw.WriteStartElement("F_CUSTOM_BYTES");
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_POINTS");
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_TEXT");
            xtw.WriteEndElement();
        }

        protected void WriteTransparentTextProperties(XmlTextWriter xtw, IAnnotationReader reader, int annotationIndex, int legacyPageSequenceNum)
        {
            Characteristics inC = new Characteristics();
            inC.FontName = reader.GetFontName(annotationIndex);
            inC.FontSize = reader.GetFontSize(annotationIndex);
            inC.Height = reader.GetHeight(annotationIndex);
            inC.Width = reader.GetWidth(annotationIndex);
            Characteristics outC = contxt.configMgr.GetAdjustedCharacteristics(inC);

            WriteAdjustedSizeAndPosition(xtw, reader, annotationIndex, outC.Height, outC.Width);
            xtw.WriteAttributeString("F_BACKCOLOR", reader.GetBackColor(annotationIndex));
            xtw.WriteAttributeString("F_BORDER_BACKMODE", reader.GetBorderBackMode(annotationIndex));
            xtw.WriteAttributeString("F_BORDER_STYLE", reader.GetBorderStyle(annotationIndex));
            xtw.WriteAttributeString("F_BORDER_COLOR", reader.GetBorderColor(annotationIndex));
            xtw.WriteAttributeString("F_BORDER_WIDTH", reader.GetBorderWidth(annotationIndex));
            xtw.WriteAttributeString("F_FONT_BOLD", reader.GetFontBold(annotationIndex));
            xtw.WriteAttributeString("F_FONT_ITALIC", reader.GetFontItalic(annotationIndex));
            xtw.WriteAttributeString("F_FONT_NAME", outC.FontName);
            xtw.WriteAttributeString("F_FONT_SIZE", outC.FontSize);
            xtw.WriteAttributeString("F_FONT_STRIKETHROUGH", reader.GetFontStrikethrough(annotationIndex));
            xtw.WriteAttributeString("F_FONT_UNDERLINE", reader.GetFontUnderline(annotationIndex));
            string textColor = reader.GetForeColor(annotationIndex);
            if (textColor.Length == 0)
            {
                textColor = "255";
            }
            xtw.WriteAttributeString("F_FORECOLOR", textColor);
            xtw.WriteAttributeString("F_HASBORDER", reader.GetHasBorder(annotationIndex));
            xtw.WriteAttributeString("F_TEXT_BACKMODE", reader.GetTextBackMode(annotationIndex));
            xtw.WriteStartElement("F_CUSTOM_BYTES");
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_POINTS");
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_TEXT");

            xtw.WriteAttributeString("Encoding", "unicode");
            byte[] unicodeBytes = Encoding.Convert(Encoding.ASCII, Encoding.Unicode, Converter.StripCR(reader.GetText(annotationIndex)));
            Converter.Swap(unicodeBytes);
            xtw.WriteBinHex(unicodeBytes, 0, unicodeBytes.Length);
        }

        protected void WriteStampProperties(XmlTextWriter xtw, IAnnotationReader reader, int annotationIndex, int legacyPageSequenceNum)
        {
            Characteristics inC = new Characteristics();
            inC.FontName = reader.GetFontName(annotationIndex);
            inC.FontSize = reader.GetFontSize(annotationIndex);
            inC.Height = reader.GetHeight(annotationIndex);
            inC.Width = reader.GetWidth(annotationIndex);
            Characteristics outC = contxt.configMgr.GetAdjustedCharacteristics(inC);

            WriteAdjustedSizeAndPosition(xtw, reader, annotationIndex, outC.Height, outC.Width);
            xtw.WriteAttributeString("F_BACKCOLOR", reader.GetBackColor(annotationIndex));
            xtw.WriteAttributeString("F_BORDER_BACKMODE", reader.GetBorderBackMode(annotationIndex));
            xtw.WriteAttributeString("F_BORDER_COLOR", reader.GetBorderColor(annotationIndex));
            xtw.WriteAttributeString("F_BORDER_STYLE", reader.GetBorderStyle(annotationIndex));
            xtw.WriteAttributeString("F_BORDER_WIDTH", reader.GetBorderWidth(annotationIndex));
            xtw.WriteAttributeString("F_FONT_BOLD", reader.GetFontBold(annotationIndex));
            xtw.WriteAttributeString("F_FONT_ITALIC", reader.GetFontItalic(annotationIndex));
            xtw.WriteAttributeString("F_FONT_NAME", outC.FontName);
            xtw.WriteAttributeString("F_FONT_SIZE", outC.FontSize);
            xtw.WriteAttributeString("F_FONT_STRIKETHROUGH", reader.GetFontStrikethrough(annotationIndex));
            xtw.WriteAttributeString("F_FONT_UNDERLINE", reader.GetFontUnderline(annotationIndex));
            xtw.WriteAttributeString("F_FORECOLOR", reader.GetForeColor(annotationIndex));
            xtw.WriteAttributeString("F_HASBORDER", reader.GetHasBorder(annotationIndex));
            xtw.WriteAttributeString("F_ROTATION", reader.GetRotation(annotationIndex));
            xtw.WriteAttributeString("F_TEXT_BACKMODE", reader.GetTextBackMode(annotationIndex));
            xtw.WriteStartElement("F_CUSTOM_BYTES");
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_POINTS");
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_TEXT");
            
            xtw.WriteAttributeString("Encoding", "unicode");
            byte[] unicodeBytes = Encoding.Convert(Encoding.ASCII, Encoding.Unicode, Converter.StripCR(reader.GetText(annotationIndex)));
            Converter.Swap(unicodeBytes);
            xtw.WriteBinHex(unicodeBytes, 0, unicodeBytes.Length);
        }

        protected void WriteSolidTextProperties(XmlTextWriter xtw, IAnnotationReader reader, int annotationIndex, int legacyPageSequenceNum)
        {
            Characteristics inC = new Characteristics();
            inC.FontName = reader.GetFontName(annotationIndex);
            inC.FontSize = reader.GetFontSize(annotationIndex);
            inC.Height = reader.GetHeight(annotationIndex);
            inC.Width = reader.GetWidth(annotationIndex);
            Characteristics outC = contxt.configMgr.GetAdjustedCharacteristics(inC);

            WriteAdjustedSizeAndPosition(xtw, reader, annotationIndex, outC.Height, outC.Width);
            xtw.WriteAttributeString("F_BACKCOLOR", reader.GetBackColor(annotationIndex));
            xtw.WriteAttributeString("F_BORDER_BACKMODE", reader.GetBorderBackMode(annotationIndex));
            xtw.WriteAttributeString("F_BORDER_COLOR", reader.GetBorderColor(annotationIndex));
            xtw.WriteAttributeString("F_BORDER_STYLE", reader.GetBorderStyle(annotationIndex));
            xtw.WriteAttributeString("F_BORDER_WIDTH", reader.GetBorderWidth(annotationIndex));
            xtw.WriteAttributeString("F_FONT_BOLD", reader.GetFontBold(annotationIndex));
            xtw.WriteAttributeString("F_FONT_ITALIC", reader.GetFontItalic(annotationIndex));
            xtw.WriteAttributeString("F_FONT_NAME", outC.FontName);
            xtw.WriteAttributeString("F_FONT_SIZE", outC.FontSize);
            xtw.WriteAttributeString("F_FONT_STRIKETHROUGH", reader.GetFontStrikethrough(annotationIndex));
            xtw.WriteAttributeString("F_FONT_UNDERLINE", reader.GetFontUnderline(annotationIndex));
            xtw.WriteAttributeString("F_FORECOLOR", reader.GetForeColor(annotationIndex));
            xtw.WriteAttributeString("F_HASBORDER", reader.GetHasBorder(annotationIndex));
            xtw.WriteAttributeString("F_TEXT_BACKMODE", reader.GetTextBackMode(annotationIndex));
            xtw.WriteStartElement("F_CUSTOM_BYTES");
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_POINTS");
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_TEXT");
            
            xtw.WriteAttributeString("Encoding", "unicode");
            byte[] unicodeBytes = Encoding.Convert(Encoding.ASCII, Encoding.Unicode, Converter.StripCR(reader.GetText(annotationIndex)));
            Converter.Swap(unicodeBytes);
            xtw.WriteBinHex(unicodeBytes, 0, unicodeBytes.Length);
        }

        protected void WriteHighlightProperties(XmlTextWriter xtw, IAnnotationReader reader, int annotationIndex, int legacyPageSequenceNum)
        {
            WriteSizeAndPosition(xtw, reader, annotationIndex);
            xtw.WriteAttributeString("F_BRUSHCOLOR", reader.GetBrushColor(annotationIndex));
            xtw.WriteAttributeString("F_LINE_COLOR", reader.GetLineColor(annotationIndex));
            xtw.WriteAttributeString("F_LINE_WIDTH", reader.GetLineWidth(annotationIndex));
            xtw.WriteAttributeString("F_TEXT_BACKMODE", reader.GetTextBackMode(annotationIndex));
        }

        protected void WritePolygonProperties(XmlTextWriter xtw, IAnnotationReader reader, int annotationIndex, int legacyPageSequenceNum)
        {
            WriteSizeAndPosition(xtw, reader, annotationIndex);
            xtw.WriteAttributeString("F_LINE_COLOR", reader.GetLineColor(annotationIndex));
            string brushColor = reader.GetBrushColor(annotationIndex);
            if (brushColor != "")
            {
                xtw.WriteAttributeString("F_BRUSHCOLOR", brushColor);
            }
            xtw.WriteAttributeString("F_LINE_WIDTH", reader.GetLineWidth(annotationIndex));
            xtw.WriteAttributeString("F_SUBCLASS", reader.GetSubClass(annotationIndex));
            xtw.WriteAttributeString("F_TEXT_BACKMODE", reader.GetTextBackMode(annotationIndex));
            xtw.WriteStartElement("F_CUSTOM_BYTES");
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_POINTS");
            xtw.WriteString(reader.GetPoints(annotationIndex));
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_TEXT");
            xtw.WriteEndElement();
        }

        protected void WriteHighlightPolygonProperties(XmlTextWriter xtw, IAnnotationReader reader, int annotationIndex, int legacyPageSequenceNum)
        {
            WriteSizeAndPosition(xtw, reader, annotationIndex);
            xtw.WriteAttributeString("F_BRUSHCOLOR", reader.GetBrushColor(annotationIndex));
            xtw.WriteAttributeString("F_LINE_COLOR", reader.GetLineColor(annotationIndex));
            xtw.WriteAttributeString("F_LINE_WIDTH", reader.GetLineWidth(annotationIndex));
            xtw.WriteAttributeString("F_SUBCLASS", reader.GetSubClass(annotationIndex));
            xtw.WriteAttributeString("F_TEXT_BACKMODE", reader.GetTextBackMode(annotationIndex));
            xtw.WriteStartElement("F_CUSTOM_BYTES");
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_POINTS");
            xtw.WriteString(reader.GetPoints(annotationIndex));
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_TEXT");
            xtw.WriteEndElement();
        }

        protected void WriteOvalProperties(XmlTextWriter xtw, IAnnotationReader reader, int annotationIndex, int legacyPageSequenceNum)
        {
            WriteSizeAndPosition(xtw, reader, annotationIndex);
            xtw.WriteAttributeString("F_LINE_COLOR", reader.GetLineColor(annotationIndex));
            string brushColor = reader.GetBrushColor(annotationIndex);
            if(brushColor != "")
            {
                xtw.WriteAttributeString("F_BRUSHCOLOR", brushColor);
            }
            xtw.WriteAttributeString("F_LINE_WIDTH", reader.GetLineWidth(annotationIndex));
            xtw.WriteAttributeString("F_SUBCLASS", reader.GetSubClass(annotationIndex));
            xtw.WriteAttributeString("F_TEXT_BACKMODE", reader.GetTextBackMode(annotationIndex));
            xtw.WriteStartElement("F_CUSTOM_BYTES");
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_POINTS");
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_TEXT");
            xtw.WriteEndElement();
        }

        protected void WriteArrowProperties(XmlTextWriter xtw, IAnnotationReader reader, int annotationIndex, int legacyPageSequenceNum)
        {
            WriteSizeAndPosition(xtw, reader, annotationIndex);
            xtw.WriteAttributeString("F_ARROWHEAD_SIZE", reader.GetArrowHeadSize(annotationIndex));
            xtw.WriteAttributeString("F_LINE_BACKMODE", reader.GetLineBackMode(annotationIndex));
            xtw.WriteAttributeString("F_LINE_COLOR", reader.GetLineColor(annotationIndex));
            xtw.WriteAttributeString("F_LINE_START_X", reader.GetLineStartX(annotationIndex));
            xtw.WriteAttributeString("F_LINE_END_X", reader.GetLineEndX(annotationIndex));
            xtw.WriteAttributeString("F_LINE_START_Y", reader.GetLineStartY(annotationIndex));
            xtw.WriteAttributeString("F_LINE_END_Y", reader.GetLineEndY(annotationIndex));
            xtw.WriteAttributeString("F_LINE_STYLE", reader.GetLineStyle(annotationIndex));
            xtw.WriteAttributeString("F_LINE_WIDTH", reader.GetLineWidth(annotationIndex));
            xtw.WriteStartElement("F_CUSTOM_BYTES");
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_POINTS");
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_TEXT");
            xtw.WriteEndElement();
        }

        protected void WritePenProperties(XmlTextWriter xtw, IAnnotationReader reader, int annotationIndex, int legacyPageSequenceNum)
        {
            WriteSizeAndPosition(xtw, reader, annotationIndex);
            xtw.WriteAttributeString("F_LINE_BACKMODE", reader.GetLineBackMode(annotationIndex));
            xtw.WriteAttributeString("F_LINE_STYLE", reader.GetLineStyle(annotationIndex));
            xtw.WriteAttributeString("F_LINE_COLOR", reader.GetLineColor(annotationIndex));
            xtw.WriteAttributeString("F_LINE_WIDTH", reader.GetLineWidth(annotationIndex));
            xtw.WriteStartElement("F_POINTS");
            xtw.WriteString(reader.GetPoints(annotationIndex));
            xtw.WriteEndElement();
            xtw.WriteStartElement("F_TEXT");
            xtw.WriteEndElement();
        }

        protected void WriteStickyNoteProperties(XmlTextWriter xtw, IAnnotationReader reader, int annotationIndex, int legacyPageSequenceNum)
        {
            xtw.WriteAttributeString("F_FORECOLOR", reader.GetForeColor(annotationIndex));
            xtw.WriteAttributeString("F_TOP", reader.GetTop(annotationIndex).ToString());
            xtw.WriteAttributeString("F_LEFT", reader.GetLeft(annotationIndex).ToString());
            xtw.WriteAttributeString("F_WIDTH", reader.GetWidth(annotationIndex).ToString());
            xtw.WriteAttributeString("F_HEIGHT", reader.GetHeight(annotationIndex).ToString());
            xtw.WriteEndElement();

            xtw.WriteStartElement("F_POINTS");
            xtw.WriteEndElement();

            xtw.WriteStartElement("F_TEXT");
           
            xtw.WriteAttributeString("Encoding", "unicode");
            byte[] unicodeBytes = Encoding.Convert(Encoding.ASCII, Encoding.Unicode, Converter.StripCR(reader.GetText(annotationIndex)));
            Converter.Swap(unicodeBytes);
            xtw.WriteBinHex(unicodeBytes, 0, unicodeBytes.Length);
        }

        protected void GenerateP8Document(string sourcePrefix, string targetStagingPath)
        {
            if (entityStatus == EntityStatus.DISCARD) return;

            string legacyAnnPath = GetLegacyAnnotationFilePath(sourcePrefix);
            string filePath = string.Empty;
            
            GeneratedDoc gd = null;
            switch(annotatedObjectType)
            {
                case AnnotatedObjectType.CASE:
                    filePath = targetStagingPath + "\\Case" + annotatedObjectMMID.ToString() + "_Notes" + sequenceNumber.ToString();
                    LogDebugMessage("CMAnnotation: GenerateP8Document: Calling Case Document Generator with " + legacyAnnPath);
                    gd = contxt.caseDocGenerator.Generate(legacyAnnPath, filePath, srcProperties);
                    break;

                case AnnotatedObjectType.DOCUMENT:
                    DocumentPages dps = new DocumentPages();
                    dps.Pages.Add(srcProperties);
                    filePath = targetStagingPath + "\\Doc" + annotatedObjectMMID.ToString() + "_Notes" + sequenceNumber.ToString();
                    LogDebugMessage("CMAnnotation: GenerateP8Document: Calling Doc Document Generator with " + legacyAnnPath);
                    gd = contxt.docDocGenerator.Generate(legacyAnnPath, filePath, dps);
                    break;

                case AnnotatedObjectType.TASK:
                    filePath = targetStagingPath + "\\Task" + annotatedObjectMMID.ToString() + "_Notes" + sequenceNumber.ToString();
                    LogDebugMessage("CMAnnotation: GenerateP8Document: Calling Task Document Generator with " + legacyAnnPath);
                    gd = contxt.taskDocGenerator.Generate(legacyAnnPath, filePath, srcProperties);
                    break;

                default:
                    entityStatus = EntityStatus.DISCARD;
                    LogError("Error: Unknown annotated object type");
                    break;
            }
            SerializeGeneratedDocument(gd, FormulateGeneratedDocumentProperties());
        }

        protected bool DoesFolderExist(string folderName, string parent, string caseID)
        {
            string sql = "SELECT ID FROM " + contxt.jobName + "_CECaseFolders WHERE CaseID = " + caseID + " AND Name = '" + folderName + "' AND Parent = '" + parent + "'";
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(contxt.dbConnection, sql);
            return (xml.SelectNodes("//row").Count == 0) ? false : true;
        }

        protected void CreateFolders(string folder)
        {
            if ((contxt.configLevel == "CASE") && (!string.IsNullOrEmpty(folder)))
            {
                string[] parts = folder.Split('/');
                if (parts.Length > 1)
                {
                    // check if starts with a Migration Manager created static folder
                    if (DoesFolderExist(parts[0], "Root", annotatedObjectMMID))
                    {
                        for (int i = 1; i < parts.Length; i++)
                        {
                            if (!(DoesFolderExist(parts[i], parts[i - 1], annotatedObjectMMID)))
                            {
                                DBAdapter.DBAdapter.AddToDB(contxt.dbConnection, "INSERT INTO " + contxt.jobName + "_CECaseFolders (CaseID, Name, Parent) VALUES ( " + annotatedObjectMMID.ToString() + ", '" + Converter.EscapeQuote(parts[i]) + "', '" + Converter.EscapeQuote(parts[i - 1]) + "')");
                            }
                        }
                    }
                }
            }
        }

        protected void SerializeGeneratedDocument(GeneratedDoc gd, TargetDocProperties properties)
        {
            if (entityStatus == EntityStatus.DISCARD) return;

            string folder = GetSourceValue(contxt.configMgr.GetDocFolderIdentifier());
            CreateFolders(folder);
            string sql = (contxt.configLevel == "CASE") ? "INSERT INTO " + contxt.jobName + "_CEDocuments (BatchID, CaseID, DocClass, MimeType, Folder, ContainmentName) OUTPUT inserted.* VALUES ( " + contxt.batchID + ", " + annotatedObjectMMID + ", '" + targetType + "', '" + gd.MimeType + "', '" + Converter.EscapeQuote(folder) + "', '" + Converter.EscapeQuote(gd.ContainmentName) + "')" :
                        "INSERT INTO " + contxt.jobName + "_CEDocuments (BatchID, DocClass, MimeType, Folder, ContainmentName) OUTPUT inserted.* VALUES ( " + contxt.batchID + ", '" + targetType + "', '" + gd.MimeType + "', '" + folder + "', '" + Converter.EscapeQuote(gd.ContainmentName) + "')";
            XmlDocument xml = DBAdapter.DBAdapter.FetchInsertedRec(contxt.dbConnection, sql);
            string docID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;

            properties.Serialize(docID);
            DBAdapter.DBAdapter.AddToDB(contxt.dbConnection, "INSERT INTO " + contxt.jobName + "_CEDocumentPages (DocumentID, PageNum, FilePath, MimeType) VALUES ( " + docID + ", '" + gd.PageNumber.ToString() + "', '" + gd.FileName + "', '" + gd.MimeType + "')");
        }

        protected string GetLegacyAnnotationFilePath(string sourcePrefix)
        {
            string legacyAnnPath = (sourcePrefix.Trim().Length == 0) ? legacyFileName : sourcePrefix + "\\" + legacyFileName;
            if (!File.Exists(legacyAnnPath))
            {
                entityStatus = EntityStatus.DISCARD;
                throw new ArgumentException("Annotation file not found: " + legacyAnnPath);
            }
            return legacyAnnPath;
        }
    }
}
