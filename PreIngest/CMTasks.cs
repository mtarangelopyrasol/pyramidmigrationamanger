﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml;

namespace PreIngest
{
    class CMTasks : ClassBase
    {
        protected List<CMTask> taskCollection = new List<CMTask>();
        int totalCount = 0;
        int successCount = 0;
        int errorCount = 0;
        string targetCaseID = string.Empty;
        string accountID = string.Empty;

        public CMTasks(Context c, string account, string caseID) : base(c)
        {
            targetCaseID = caseID;
            accountID = account;
            ItemsProcessed = false;
        }

        public Boolean ItemsProcessed { get; set;  }

        public ConversionResult PerformConversion(CMDocuments cmDocs, string caseTypeID)
        {
            ConversionResult result = ConversionResult.CONV_NEW;

            while (result != ConversionResult.CONV_COMPLETE)
            {
                if (contxt.backgroundWorker.CancellationPending)
                {
                    result = ConversionResult.CONV_CANCELED;
                    break;
                }
                try
                {
                    XmlDocument sourceXML = GetTaskData();
                    XmlNodeList xnl = sourceXML.SelectNodes("//row");
                    if (xnl.Count == 0)
                    {
                        result = ConversionResult.CONV_COMPLETE;
                        break;
                    }
                    ItemsProcessed = true;
                    foreach (XmlElement rowNode in xnl)
                    {
                        CMTask cmt = new CMTask(contxt, accountID, targetCaseID, caseTypeID);
                        cmt.SetSource(rowNode);
                        try
                        {
                            result = cmt.PerformConversion(cmDocs);
                        }
                        catch (Exception ex)
                        {
                            LogError("Exception: " + ex.Message);
                            result = ConversionResult.CONV_ERRORED;
                        }
                        totalCount++;
                        if ((result == ConversionResult.CONV_DONE) || (result == ConversionResult.CONV_COMPLETE)) successCount++;
                        else if (result == ConversionResult.CONV_ERRORED) errorCount++;
                    }
                }
                catch (Exception ex)
                {
                    LogError("Exception: " + ex.Message);
                    result = ConversionResult.CONV_ERRORED;
                    errorCount++;
                }
            }
            return result;
        }

        public int GetTaskCount()
        {
            return totalCount;
        }

        public int GetSuccessCount()
        {
            return successCount;
        }

        public int GetErrorCount()
        {
            return errorCount;
        }

        protected XmlDocument GetTaskData()
        {
            return DBAdapter.DBAdapter.GetProcessingRecs(contxt.dbConnection, contxt.jobName + "_TaskData", "BatchID", contxt.batchID,
                contxt.configMgr.GetTaskAccountIdentifier() + " = '" + accountID + "'", "Status IN ('CONV_NEW', 'CONV_CANCELED')", true);
        } 
    }
}
