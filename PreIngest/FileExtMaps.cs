﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace PreIngest
{
    public class FileExtMaps
    {
        protected Dictionary<string, string> maps = null;

        public FileExtMaps()
        {
            maps = new Dictionary<string, string>();
        }

        public void AddFileExtMap(string fmt, string ext)
        {
            if (!maps.ContainsKey(fmt))
            {
                maps[fmt] = ext;
            }
        }

        public string GetExtension(string fmt)
        {
            string ext = string.Empty;

            if (maps.ContainsKey(fmt))
            {
                ext = maps[fmt];
            }
            else
            {
                if(maps.ContainsKey("ANY"))
                {
                    ext = maps["ANY"];
                }
            }
            return ext;
        }
    }
}
