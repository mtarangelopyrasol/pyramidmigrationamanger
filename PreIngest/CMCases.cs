﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml;

namespace PreIngest
{
    class CMCases : ClassBase
    {
        protected List<CMCase> caseCollection = new List<CMCase>();
        int totalCount = 0;
        int successCount = 0;
        int errorCount = 0;

        public CMCases(Context c) : base(c)
        {
            ItemsProcessed = false;
        }

        public Boolean ItemsProcessed { get; set; }

        public ConversionResult PerformConversion(int batchSize, string batchCondition, string srcContentPrefixPath, string stagingPath)
        {
            ConversionResult result = ConversionResult.CONV_NEW;

            while (totalCount < batchSize)
            {
                if (contxt.backgroundWorker.CancellationPending)
                {
                    result = ConversionResult.CONV_CANCELED;
                    break;
                }

                try
                {
                    XmlDocument sourceXML = GetCaseData(batchCondition);
                    XmlNodeList xnl = sourceXML.SelectNodes("//row");
                    if (xnl.Count == 0)
                    {
                        result = ConversionResult.CONV_COMPLETE;
                        break;
                    }
                    foreach (XmlElement rowNode in xnl)
                    {
                        ItemsProcessed = true;
                        CMCase cmc = new CMCase(contxt);
                        cmc.SetSource(rowNode);
                        result = cmc.PerformConversion(srcContentPrefixPath, stagingPath);
                        totalCount++;
                        if (result == ConversionResult.CONV_DONE) successCount++;
                        else if (result == ConversionResult.CONV_ERRORED) errorCount++;
                    }
                }
                catch (Exception ex)
                {
                    LogError("Exception processing case: " + ex.Message);
                    result = ConversionResult.CONV_ERRORED;
                }
            }
            return result;
        }

        public int GetCaseCount()
        {
            return totalCount;
        }

        public int GetSuccessCount()
        {
            return successCount;
        }

        public int GetErrorCount()
        {
            return errorCount;
        }

        protected XmlDocument GetCaseData(string condition)
        {
            return DBAdapter.DBAdapter.GetProcessingRecs(contxt.dbConnection, contxt.jobName + "_CaseData", "BatchID", contxt.batchID,
                (condition.Length > 0) ? condition : "", "Status IN ('CONV_NEW', 'CONV_CANCELED')", true);
        }
    }
}
