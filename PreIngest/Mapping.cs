﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace PreIngest
{
    public class Mapping
    {
        protected Dictionary<string, string> map = new Dictionary<string, string>();

        public Mapping(XmlElement row)
        {
            for (int i = 0; i < row.Attributes.Count; i++)
            {
                map.Add(row.Attributes[i].Name, row.Attributes[i].Value);
            }
        }

        public Mapping(XmlDocument xmlDoc, string keyName, string valName)
        {
            foreach (XmlElement row in xmlDoc.SelectNodes("//row"))
            {
                map.Add(row.GetAttribute(keyName), row.GetAttribute(valName));
            }
        }

        public string GetValue(string key)
        {
            string value = string.Empty;
            if (key.Trim().Length == 0) key = "NULL";
            if (map.ContainsKey(key))
            {
                value = map[key];
            }
            if (value == "NULL") value = "";
            else if ((value.Trim() == string.Empty) && (key != "NULL")) value = key;
            return value;
        }
       
        public Dictionary<string, string> GetMap()
        {
            return map;
        }
    }
}
