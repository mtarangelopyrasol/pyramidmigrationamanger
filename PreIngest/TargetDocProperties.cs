﻿using System;
using System.Collections.Generic;

namespace PreIngest
{
    class TargetDocProperties : TargetProperties
    {
        public TargetDocProperties(Context c) : base(c)
        {
        }

        public override TargetProperty AddProperty(string n, string v, string t)
        {
            TargetDocProperty tp = new TargetDocProperty(contxt, n, GetConvertedValue(n, v, t), t);
            properties.Add(tp);
            return tp;
        }
    }
}
