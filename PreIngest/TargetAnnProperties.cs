﻿using System;
using System.Collections.Generic;

namespace PreIngest
{
    class TargetAnnProperties : TargetProperties
    {
        public TargetAnnProperties(Context c) : base(c)
        {
        }

        public override TargetProperty AddProperty(string n, string v, string t)
        {
            TargetAnnProperty tp = new TargetAnnProperty(contxt, n, GetConvertedValue(n, v, t), t);
            properties.Add(tp);
            return tp;
        }
    }
}
