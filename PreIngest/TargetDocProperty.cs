﻿using System;

namespace PreIngest
{
    class TargetDocProperty : TargetProperty
    {
        public TargetDocProperty(Context c, string n, string v, string t) : base(c, n, v, t)
        {
        }

        public override void Serialize(string docID)
        {
            if (!IsValueNull())
            {
                DBAdapter.DBAdapter.AddToDB(contxt.dbConnection, "INSERT INTO " + contxt.jobName + "_CEDocumentProperties (DocumentID, Name, Value, Type) VALUES ( " + docID + ", '" + Name + "', '" + Converter.EscapeQuote(Value) + "', '" + DataType + "')");
            }
        }
    } 
}
