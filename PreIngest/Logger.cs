﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

using DBAdapter;

namespace PreIngest
{
    class Logger
    {
        string dbConnection;
        string logTable;
        Boolean debugOption;

        public Logger(string cnxn, string jobName, Boolean dbg)
        {
            dbConnection = cnxn;
            logTable = jobName + "_PreIngestLog";
            debugOption = dbg;
        }

        public void LogMessage(string batchID, string message)
        {
            string sql = "INSERT INTO " + logTable + "(BatchID, Type, Description) VALUES ('" + batchID + "', 'AUDIT', '" + Converter.QuoteFree(message) + "')";
            DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
        }

        public void LogWarning(string batchID, string message)
        {
            string sql = "INSERT INTO " + logTable + "(BatchID, Type, Description) VALUES ('" + batchID + "', 'WARN', '" + Converter.QuoteFree(message) + "')";
            DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
        }

        public void LogError(string batchID, string message)
        {
            string sql = "INSERT INTO " + logTable + "(BatchID, Type, Description) VALUES ('" + batchID + "', 'ERROR', '" + Converter.QuoteFree(message) + "')";
            DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
        }

        public void LogDebugMessage(string batchID, string message)
        {
            if (debugOption)
            {
                string sql = "INSERT INTO " + logTable + "(BatchID, Type, Description) VALUES ('" + batchID + "', 'DEBUG', '" + Converter.QuoteFree(message) + "')";
                DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
            }
        }
    }
}
