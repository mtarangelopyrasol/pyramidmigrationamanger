﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace PreIngest
{
    public class ConfigProperty
    {
        Mapping map = null;
        Mapping valueMap = null;

        public ConfigProperty(string name, XmlElement row)
        {
            Name = name;
            map = new Mapping(row);
        }

        public void LoadValueMapping(XmlDocument mapXML)
        {
            valueMap = new Mapping(mapXML, "OldValue", "NewValue");
        }

        public string Name { get; set; }

        public string GetSource()
        {
            return map.GetValue("Source");
        }

        public string GetDataType()
        {
            return map.GetValue("DataType");
        }

        public Boolean IsMultiValued()
        {
            string dataType = GetDataType();
            if ((dataType == "ListOfBoolean") || (dataType == "ListOfDateTime") || (dataType == "ListOfFloat64") || (dataType == "ListOfInteger32") || (dataType == "ListOfString"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean IsRequired()
        {
            return (Convert.ToInt32(map.GetValue("Required")) == 0) ? false : true;
        }

        public Boolean IsMapped()
        {
            return (Convert.ToInt32(map.GetValue("Mapped")) == 0) ? false : true;
        }

        public Boolean IsComputed()
        {
            return (Convert.ToInt32(map.GetValue("Computed")) == 0) ? false : true;
        }

        public string GetMappingName()
        {
            return map.GetValue("MappingName");
        }

        public string GetMappedValue(string key)
        {
            return valueMap.GetValue(key);
        } 
    }
}
