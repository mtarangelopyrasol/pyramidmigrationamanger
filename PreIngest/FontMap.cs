﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PreIngest
{
    public class FontMap
    {
        public string FontName { get; set; }
        public string InputSize { get; set; }
        public string OutputSize { get; set; }
        public float HeightAdjustment { get; set; }
        public float WidthAdjustment { get; set; }
    }
}
