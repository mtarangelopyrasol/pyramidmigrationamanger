﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PreIngest
{
    class TargetPage : ClassBase
    {
        Int32 pageNumber = 0;
        string fileName = string.Empty;
        string mimeType = string.Empty;
        protected Dictionary<string, int> sourceIDs = new Dictionary<string, int>();

        public TargetPage(Context c, string fName, Int32 pageNum, string mt) : base(c)
        {
            fileName = fName;
            pageNumber = pageNum;
            mimeType = mt;
        }

        public void Serialize(string docID)
        {
            string p8MimeType = contxt.configMgr.DetermineP8MimeType(mimeType);
            DBAdapter.DBAdapter.AddToDB(contxt.dbConnection, "INSERT INTO " + contxt.jobName + "_CEDocumentPages (DocumentID, PageNum, FilePath, MimeType) VALUES ( " + docID + ", '" + pageNumber.ToString() + "', '" + fileName + "', '" + p8MimeType + "')");
        }

        public void SetSourceSequence(string seq)
        {
            string[] srcID = seq.Split('|');

            int seqNum = 1;
            foreach (string sID in srcID)
            {
                sourceIDs.Add(sID, seqNum++);
            }
        }

        public int GetSourceSequenceNumber(string legacyPageID)
        {
            if (sourceIDs.ContainsKey(legacyPageID))
                return sourceIDs[legacyPageID];
            else
                return -1;
        }

        public Boolean HasSourcePage(string legacyPageID)
        {
            return (sourceIDs.ContainsKey(legacyPageID)) ? true : false;
        }

        public int GetPageNumber()
        {
            return pageNumber;
        }
    }
}
