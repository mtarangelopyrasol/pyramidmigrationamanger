﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml;

namespace PreIngest
{
    class CMTask : CMEntity
    {
        string accountID = string.Empty;
        string mmCaseID = string.Empty;
        string caseTypeID = string.Empty;
        Boolean isDocInitiated = false;
        CMAnnotations cmAnns = null;

        public CMTask(Context c, string acID, string caseID, string cTypeID) : base(c)
        {
            accountID = acID;
            mmCaseID = caseID;
            caseTypeID = cTypeID;
        }

        public override void SetSource(XmlElement sourceXML)
        {
            PrepareSourceProperties(sourceXML);
            string taskAccountId = srcProperties.Values[contxt.configMgr.GetTaskAccountIdentifier()];
            if (accountID != taskAccountId)
            {
                LogError("Task account " + taskAccountId + " differs from the case account " + accountID + "; ignoring the tasks account identifier");
            }
        }

        public ConversionResult PerformConversion(CMDocuments cmDocs)
        {
            ConversionResult result = ConversionResult.CONV_NEW;

            if (contxt.backgroundWorker.CancellationPending)
            {
                result = ConversionResult.CONV_CANCELED;
            }
            try
            {
                DetermineType();
                LogMessage("Task: " + targetType);
                if (mmID == string.Empty)
                {
                    if (contxt.configMgr.IsDocInitiated(caseTypeID, targetType))
                    {
                        isDocInitiated = true;

                        LogDebugMessage("Task is document initiated; CaseTypeID: " + caseTypeID + " TargetType: " + targetType);
                        // Find document that has properties specified by InitiatingDocIdSource
                        string initiatingDocIdSource = contxt.configMgr.GetInitiatingDocIdSource(caseTypeID, targetType);
                        LogDebugMessage("Initiating Doc Id source: " + initiatingDocIdSource);

                        string[] parts = initiatingDocIdSource.Split('+');
                        Dictionary<string, string> srcParts = new Dictionary<string, string>();
                        foreach (string s in parts)
                        {
                            srcParts.Add(s, GetSourceValue(s));
                        }
                        string attachingDocMMId = cmDocs.GetDocumentWithSourcePage(srcParts);
                        LogDebugMessage("Attaching Doc MMID: " + attachingDocMMId);

                        string initDocMMId = SerializeInitDoc();
                        LogDebugMessage("Serialized initiating Doc MMID" + initDocMMId);
                        FormulateInitiatingDocProperties(initDocMMId, attachingDocMMId);
                        LogDebugMessage("Initiating Documents properties formulated");
                        result = ConversionResult.CONV_COMPLETE;
                    }
                    else
                    {
                        FormulateTargetProperties();
                        result = AddAnnotations();
                    }
                    if (result == ConversionResult.CONV_COMPLETE)
                    {
                        Serialize();
                        result = ConversionResult.CONV_DONE;
                        LogMessage("Completed Task: " + targetType);
                    }
                }
            }
            catch (Exception ex)
            {
                result = ConversionResult.CONV_ERRORED;
                LogError("Case: " + accountID + " Task: " + targetType + " " + ex.Message);
                entityStatus = EntityStatus.DISCARD;
            }
            finally
            {
                UpdateSourceStatus(result, contxt.jobName + "_TaskData");
            }
            return result;
        }

        protected override void DetermineType()
        {
            if (contxt.taskTypeSelector != null)
            {
                targetType = contxt.taskTypeSelector.Select(srcProperties);
            }
            else
            {
                targetType = contxt.configMgr.GetTaskType();
                if (targetType.Length == 0)
                {
                    string identifier = contxt.configMgr.GetTaskTypeIdentifier();
                    if (identifier.Length > 0)
                    {
                        targetType = GetSourceValue(identifier);
                    }
                }
            }
            if (targetType == string.Empty)
            {
                throw new ArgumentException("Could not determine target task type");
            }
            if (!IsTargetTypeDefined())
            {
                throw new ArgumentException("Task Type " + targetType + " is not defined in the configuration");
            }
        }

        protected override Boolean IsTargetTypeDefined()
        {
            return contxt.configMgr.IsTaskTypeDefined(caseTypeID, targetType);
        }

        protected void FormulateTargetProperties()
        {
            targetProperties = new TargetTaskProperties(contxt);
            FormulateProperties(contxt.configMgr.GetTaskProperties(caseTypeID, targetType).GetProperties(), contxt.jobName + "_TaskData_", contxt.taskPropertyEvaluator, "Case: " + accountID + " Task: " + targetType);
        }

        protected void Serialize()
        {
            if (mmID == string.Empty)
            {
                string sql = "SELECT ID FROM " + contxt.jobName + "_CETasks WHERE CaseID = " + mmCaseID + " AND BatchID = " + contxt.batchID + " AND TaskName = '" + targetType + "' AND MMAccountID = '" + accountID + "'";
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(contxt.dbConnection, sql);
                if (xml.SelectNodes("//row").Count == 0)
                {
                    string taskDesc = Converter.EscapeQuote(srcProperties.Values[contxt.configMgr.GetTaskDescIdentifier()]);
                    string docInitiated = (isDocInitiated) ? "1" : "0";
                    xml = DBAdapter.DBAdapter.FetchInsertedRec(contxt.dbConnection, "INSERT INTO " + contxt.jobName + "_CETasks (CaseID, BatchID, TaskName, TaskDesc, IsDocInitiated, MMAccountID) OUTPUT inserted.* VALUES ( " + mmCaseID + ", " + contxt.batchID + ", '" + targetType + "', '" + taskDesc + "', " + docInitiated + ", '" + accountID + "')");
                    mmID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                    if(targetProperties != null) targetProperties.Serialize(mmID);
                }
                else
                {
                    mmID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                }
            }
        }

        protected string SerializeInitDoc()
        {
            string docClass = contxt.configMgr.GetInitiatingDocClass(caseTypeID, targetType);

            XmlDocument xml = DBAdapter.DBAdapter.FetchInsertedRec(contxt.dbConnection, "INSERT INTO " + contxt.jobName + "_CEDocuments (BatchID, CaseID, DocClass, MimeType, ContainmentName, MMAccountID) OUTPUT inserted.* VALUES ( " + contxt.batchID + ", " + mmCaseID + ", '" + docClass + "', 'application/pdf', 'Task Initiating Document', 'System Generated')");
            return xml.SelectNodes("//row")[0].Attributes["ID"].Value;
        }

        protected void FormulateInitiatingDocProperties(string initDocMMId, string attachingDocMMId)
        {
            if (entityStatus == EntityStatus.DISCARD) return;

            List<ConfigProperty> lcps = contxt.configMgr.GetDocumentProperties(contxt.configMgr.GetInitiatingDocClass(caseTypeID, targetType));
            foreach (ConfigProperty cp in lcps)
            {
                if (contxt.backgroundWorker.CancellationPending) break;

                if (cp.IsComputed())
                {
                    LogDebugMessage("Calling DocPropertyEvaluator to evaluate " + cp.Name);
                    //LogDebugMessage("SrcProperties");
                    //foreach (string v in srcProperties.Values.Keys)
                    //{
                    //    LogDebugMessage(v + ": " + srcProperties.Values[v]);
                    //}
                    List<string> valueList = contxt.docPropertyEvaluator.Evaluate(cp.Name, cp.GetDataType(), srcProperties);

                    foreach (string value in valueList)
                    {
                        SaveProperty(initDocMMId, cp, value);
                    }
                }
                else
                {
                    List<string> valueList = null;

                    if (cp.GetSource() == "ATTACH_DOC_ID")
                    {
                        valueList = new List<string>();
                        valueList.Add(attachingDocMMId);
                    }
                    else
                    {
                        LogDebugMessage("Getting values for initiating document property: " + cp.Name);
                        valueList = GetValues(cp, contxt.jobName + "_TaskData_");
                        LogDebugMessage("Number of values for initiating document property: " + cp.Name + " = " + valueList.Count.ToString());
                    }
                    if (valueList.Count == 0)
                    {
                        if (cp.IsRequired())
                        {
                            LogError("Case: " + accountID + " Could not find value for required property: " + cp.Name);
                            entityStatus = EntityStatus.ERROR;
                        }
                    }
                    else
                    {
                        foreach (string value in valueList)
                        {
                            SaveProperty(initDocMMId, cp, value);
                        }
                    }
                }
            }
        }

        protected Boolean IsValueNull(string value)
        {
            return ((value == null) || (value.Length == 0) || (value == "NULL") || (value.Trim().Length == 0)) ? true : false;
        }

        protected string GetConvertedValue(string n, string v, string t)
        {
            string nv = string.Empty;

            try
            {
                nv = Converter.ConvertValue(v, t);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(v + " is an invalid value for property " + n + " of type " + t + ". (" + ex.Message + ")");
            }
            return nv;
        }

        protected void SaveProperty(string initDocMMId, ConfigProperty cp, string rawValue)
        {
            try
            {
                string value = GetConvertedValue(cp.Name, rawValue, cp.GetDataType());
                if (!IsValueNull(value))
                {
                    DBAdapter.DBAdapter.AddToDB(contxt.dbConnection, "INSERT INTO " + contxt.jobName + "_CEDocumentProperties (DocumentID, Name, Value, Type) VALUES ( " + initDocMMId + ", '" + cp.Name + "', '" + value + "', '" + cp.GetDataType() + "')");
                }
            }
            catch (Exception ex)
            {
                LogError("Case: " + accountID + " Could not add property: " + cp.Name + " to initiating document" + ex.Message);
                entityStatus = EntityStatus.ERROR;
            }
        }

        protected ConversionResult AddAnnotations()
        {
            cmAnns = new CMAnnotations(contxt, accountID, mmID);
            return (contxt.taskDataLoaded) ? cmAnns.PerformConversion(AnnotatedObjectType.TASK, string.Empty, string.Empty) : ConversionResult.CONV_COMPLETE;
        }
    }
}
