﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PreIngest
{
    public class ConversionMaps
    {
        protected Boolean marshalToSingleDoc = false;
        protected List<ConversionMap> mapCollection = new List<ConversionMap>();

        public ConversionMaps(Boolean toSingle)
        {
            marshalToSingleDoc = toSingle;
        }

        public void Add(string srcFormat, string tgtFormat, string exemptOption)
        {
            mapCollection.Add(new ConversionMap(srcFormat, tgtFormat, exemptOption));
        }

        public string GetTargetFormat(string sourceFormat)
        {
            string result = string.Empty;
            string catchAllTarget = string.Empty;

            string inputFormat = sourceFormat.ToUpper();
            foreach (ConversionMap cm in mapCollection)
            {
                if (cm.SourceFormat.ToUpper() == inputFormat)
                {
                    result = cm.TargetFormat;
                    break;
                }
                if (cm.SourceFormat == "ALL")
                {
                    catchAllTarget = cm.TargetFormat;
                }
            }
            if (result == string.Empty) result = catchAllTarget;
            return result;
        }

        public Boolean IsExempt(string sourceFormat)
        {
            Boolean result = true;
            Boolean catchAllExempt = false;
            Boolean found = false;

            string inputFormat = sourceFormat.ToUpper();

            foreach (ConversionMap cm in mapCollection)
            {
                if (cm.SourceFormat.ToUpper() == inputFormat)
                {
                    result = cm.Exempt;
                    found = true;
                    break;
                }
                if (cm.SourceFormat == "ALL")
                {
                    catchAllExempt = cm.Exempt;
                }
            }
            if (!found) result = catchAllExempt;
            return result;
        }

        public Boolean IsConvertableFormat(string sourceFormat, string targetFormat)
        {
            if (IsExempt(sourceFormat))
            {
                return false;
            }
            else if ((sourceFormat.ToUpper() == targetFormat.ToUpper()) || (GetTargetFormat(sourceFormat).ToUpper() == targetFormat.ToUpper()))
            {
                return true;
            }
            return false;
        }

        public Boolean NeedToMarshalToSingleDoc()
        {
            return marshalToSingleDoc;
        }
    }
}
