﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PreIngest
{
    public class ConversionMap
    {
        public ConversionMap(string srcFormat, string tgtFormat, string ex)
        {
            SourceFormat = srcFormat;
            TargetFormat = tgtFormat;
            Exempt = (ex == "0") ? false : true;
        }

        public string SourceFormat { get; set; }
        public string TargetFormat { get; set; }
        public Boolean Exempt { get; set; }
    }
}
