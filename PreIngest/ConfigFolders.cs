﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PreIngest
{
    public class ConfigFolders
    {
        public List<ConfigFolder> folders = null;

        public ConfigFolders()
        {
            folders = new List<ConfigFolder>();
        }

        public void AddStaticFolder(ConfigFolder cf)
        {
            foreach (ConfigFolder cfIter in folders)
            {
                cfIter.LeafFolder = false;
            }
            folders.Add(cf);
        }

        public List<ConfigFolder> GetFolders()
        {
            return folders;
        }
    }
}
