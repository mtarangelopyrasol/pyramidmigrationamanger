﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PreIngest
{
    public class Characteristics
    {
        public string FontName { get; set; }
        public string FontSize { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
    }
}
