﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml;

namespace PreIngest
{
    class CMAnnotations : ClassBase
    {
        protected List<CMAnnotation> annCollection = new List<CMAnnotation>();
        string annotatedObjectLegacyID = string.Empty;
        string annotatedObjectMMID = string.Empty;
        int totalCount = 0;
        int errorCount = 0;
        int successCount = 0;

        public CMAnnotations(Context c, string parentLegacyID, string parentMMID) : base(c)
        {
            annotatedObjectLegacyID = parentLegacyID;
            annotatedObjectMMID = parentMMID;
            ItemsProcessed = false;
        }

        public Boolean ItemsProcessed { get; set; }

        // handles document annotations
        public ConversionResult PerformConversion(string srcContentPrefixPath, string stagingPath, TargetProperties docProperties, string docClass, SourcePages srcPages, TargetPages tgtPages)
        {
            ConversionResult result = ConversionResult.CONV_NEW;

            while (result != ConversionResult.CONV_COMPLETE)
            {
                if (contxt.backgroundWorker.CancellationPending)
                {
                    result = ConversionResult.CONV_CANCELED;
                    break;
                }
                XmlDocument sourceXML = GetDocAnnotationSource();
                if (sourceXML.SelectNodes("//row").Count == 0)
                {
                    result = ConversionResult.CONV_COMPLETE;
                }
                else
                {
                    LogDebugMessage("Count of source annotation records: " + sourceXML.SelectNodes("//row").Count.ToString());
                    ItemsProcessed = true;
                    int seqNum = 0;
                    foreach (XmlElement rowNode in sourceXML.SelectNodes("//row"))
                    {
                        if (contxt.backgroundWorker.CancellationPending)
                        {
                            result = ConversionResult.CONV_CANCELED;
                            break;
                        }
                        try
                        {
                            CMAnnotation cma = new CMAnnotation(contxt, annotatedObjectMMID, ++seqNum);
                            LogDebugMessage("Current row: " + rowNode.Attributes["ID"].Value);
                            cma.SetSource(rowNode);
                            result = cma.PerformConversion(AnnotatedObjectType.DOCUMENT, annotatedObjectLegacyID, srcContentPrefixPath, stagingPath, docProperties, docClass, srcPages, tgtPages);
                            totalCount++;
                            if (result == ConversionResult.CONV_DONE) successCount++;
                            else if (result == ConversionResult.CONV_ERRORED) errorCount++;
                            LogDebugMessage("TotalCount: " + totalCount + " SuccessCount: " + successCount + " ErrorCount: " + errorCount + " Result: " + result.ToString());
                        }
                        catch (Exception ex)
                        {
                            LogError("Exception processing annotation: " + ex.Message);
                            result = ConversionResult.CONV_ERRORED;
                        }
                    }
                }
            }
            return result;
        }

        // handles case and task annotations
        public ConversionResult PerformConversion(AnnotatedObjectType aot, string srcContentPrefixPath, string stagingPath)
        {
            ConversionResult result = ConversionResult.CONV_NEW;

            while (result != ConversionResult.CONV_COMPLETE)
            {
                if (contxt.backgroundWorker.CancellationPending)
                {
                    result = ConversionResult.CONV_CANCELED;
                    break;
                }
                XmlDocument sourceXML = GetAnnotationSource(aot);
                if (sourceXML.SelectNodes("//row").Count == 0)
                {
                    result = ConversionResult.CONV_COMPLETE;
                    break;
                }
                LogDebugMessage("Count of source annotation records: " + sourceXML.SelectNodes("//row").Count.ToString());
                ItemsProcessed = true;
                int seqNum = 0;
                foreach (XmlElement rowNode in sourceXML.SelectNodes("//row"))
                {
                    if (contxt.backgroundWorker.CancellationPending)
                    {
                        result = ConversionResult.CONV_CANCELED;
                        break;
                    }
                    try
                    {
                        
                        CMAnnotation cma = new CMAnnotation(contxt, annotatedObjectMMID, ++seqNum);
                        cma.SetSource(rowNode);
                        result = cma.PerformConversion(aot, annotatedObjectLegacyID, srcContentPrefixPath, stagingPath, null, null, null, null);
                        totalCount++;
                        if (result == ConversionResult.CONV_DONE) successCount++;
                        else if (result == ConversionResult.CONV_ERRORED) errorCount++;
                    }
                    catch (Exception ex)
                    {
                        LogError("Exception processing annotation: " + ex.Message);
                        result = ConversionResult.CONV_ERRORED;
                    }
                }
            }
            return result;
        }

        public int GetTotalCount()
        {
            return totalCount;
        }

        public int GetSuccessCount()
        {
            return successCount;
        }

        public int GetErrorCount()
        {
            return errorCount;
        }

        protected XmlDocument GetDocAnnotationSource()
        {
            string pageList = GetPageList();

            if ((pageList == "()") || 
                (contxt.configMgr.GetAnnotatedObjectTypeIdentifier() == string.Empty) ||
                (contxt.configMgr.GetAnnotatedObjectIdentifier() == string.Empty) ||
                (contxt.configMgr.GetAnnotationPageIdentifier() == string.Empty))
            {
                string result = "<rows></rows>";
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(result);
                return xml;
            }
            else
            {
                string condition = contxt.configMgr.GetAnnotatedObjectTypeIdentifier().ToUpper() + " = 'DOCUMENT' AND " +
                                   contxt.configMgr.GetAnnotatedObjectIdentifier() + " = '" + annotatedObjectLegacyID + "' AND " +
                                   contxt.configMgr.GetAnnotationPageIdentifier() + " IN " + pageList;
                LogDebugMessage("Annotation Pick up Condition: " + condition);

                return DBAdapter.DBAdapter.GetProcessingRecs(contxt.dbConnection, contxt.jobName + "_AnnotationData", "BatchID", contxt.batchID, condition, "Status IN ('CONV_NEW', 'CONV_CANCELED')", false);
            }
        }

        protected XmlDocument GetAnnotationSource(AnnotatedObjectType aot)
        {
            if ((contxt.configMgr.GetAnnotatedObjectTypeIdentifier() == string.Empty) ||
                (contxt.configMgr.GetAnnotationTargetObjectTypeIdentifier() == string.Empty) || 
                (contxt.configMgr.GetAnnotatedObjectIdentifier() == string.Empty))
            {
                string result = "<rows></rows>";
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(result);
                return xml;
            }
            else
            {
                string qualifier = (aot == AnnotatedObjectType.CASE) ? "CASE" : "TASK";
                string condition = contxt.configMgr.GetAnnotatedObjectTypeIdentifier().ToUpper() + " = '" + qualifier + "' AND " +
                                   contxt.configMgr.GetAnnotatedObjectIdentifier() + " = '" + annotatedObjectLegacyID + "'";

                return DBAdapter.DBAdapter.GetProcessingRecs(contxt.dbConnection, contxt.jobName + "_AnnotationData", "BatchID", contxt.batchID, condition, "Status IN ('CONV_NEW', 'CONV_CANCELED')", false);
            }
        }

        protected string GetPageList()
        {
            string result = "(";

            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(contxt.dbConnection, "SELECT LegacyPageID FROM " + contxt.jobName + "_MarshalingInfo WHERE DocumentID = " + annotatedObjectMMID);

            Boolean first = true;
            foreach (XmlElement rowNode in xml.SelectNodes("//row"))
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    result += ", ";
                }
                result += "'";
                result += rowNode.GetAttribute("LegacyPageID");
                result += "'";
            }
            result += ")";
            return result;
        }
    }
}
