﻿using System;
using System.Text;

namespace PreIngest
{
    public class ConfigFolder
    {
        public ConfigFolder(string n, string p)
        {
            Name = n;
            Parent = p;
            LeafFolder = true;
        }

        public string Name { get; set; }
        public string Parent { get; set; }
        public Boolean LeafFolder { get; set; }
    }
}
