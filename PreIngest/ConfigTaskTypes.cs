﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PreIngest
{
    public class ConfigTaskTypes
    {
        protected List<ConfigTaskType> taskTypes = null;

        public ConfigTaskTypes()
        {
            taskTypes = new List<ConfigTaskType>();
        }

        public void AddTaskType(ConfigTaskType ctt)
        {
            taskTypes.Add(ctt);
        }

        public ConfigTaskType GetTaskType(string taskTypeName)
        {
            ConfigTaskType result = null;

            foreach (ConfigTaskType ctt in taskTypes)
            {
                if (ctt.Name == taskTypeName)
                {
                    result = ctt;
                    break;
                }
            }

            return result;
        }
    }
}
