﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Leadtools;
using Leadtools.Codecs;

namespace PreIngest
{
    public class PromotionRules
    {
        protected List<PromotionRule> rules = null;

        public PromotionRules()
        {
            rules = new List<PromotionRule>();
        }

        public void AddRule(string fmtDoc, string fmtPage, string cmpPage, string promoted)
        {
            rules.Add(new PromotionRule(fmtDoc, fmtPage, cmpPage, promoted));
        }

        public RasterImageFormat GetPromotedFormat(RasterImageFormat fmtDoc, RasterImageFormat fmtPage, string cmpPage)
        {
            RasterImageFormat result = fmtDoc;

            PromotionRule pr = GetRule(fmtDoc, fmtPage, cmpPage);
            if (pr == null)
            {
                pr = GetRule(fmtDoc, cmpPage);
            }
            if (pr != null)
            {
                result = pr.PromotedFormat;
            }
            return result;
        }

        private PromotionRule GetRule(RasterImageFormat fmtDoc, RasterImageFormat fmtPage, string cmpPage)
        {
            PromotionRule result = null;
            foreach (PromotionRule p in rules)
            {
                if ((p.DocumentFormat == fmtDoc) && (p.PageFormat == fmtPage) && (p.PageCompression == cmpPage))
                {
                    result = p;
                    break;
                }
            }
            return result;
        }

        private PromotionRule GetRule(RasterImageFormat fmtDoc, string cmpPage)
        {
            PromotionRule result = null;
            foreach (PromotionRule p in rules)
            {
                if ((p.DocumentFormat == fmtDoc) && (p.PageCompression == cmpPage))
                {
                    result = p;
                    break;
                }
            }
            return result;
        }
    }
}
