﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace PreIngest
{
	public partial class Converter
	{
        static Regex rgxSQuote = new Regex("'");
        const string repBlank = " ";
        const string escQuote = "''";

        static Regex rgxCR = new Regex("\r\n");
        const string repCR = "\n";

        public static string ConvertValue(string value, string valueType)
        {
            string result = string.Empty;
            if ((valueType == "SingletonDateTime") || (valueType == "ListOfDateTime"))
            {
                result = PreIngest.Converter.ConvertFormattedDate(value);
                if (result == string.Empty)
                {
                    result = PreIngest.Converter.ConvertUnFormattedDate(value);
                }
            }
            else if ((valueType == "SingletonString") || (valueType == "ListOfString"))
            {
                string pattern = "'";
                string replacement = "''";
                Regex rgx = new Regex(pattern);
                result = rgx.Replace(value, replacement);
            }
            else
            {
                result = value;
            }
            return result;
        }

        public static string ConvertFormattedDate(string value)
        {
            // value of type 6/1/2012 12:00:00 AM

            string result = string.Empty;
            if (value != string.Empty)
            {
                try
                {
                    string[] firstSplit = value.Split('/');
                    Int16 mm = Int16.Parse(firstSplit[0]);
                    Int16 dd = Int16.Parse(firstSplit[1]);

                    string[] secondSplit = firstSplit[2].Split(' ');
                    Int16 yy = Int16.Parse(secondSplit[0]);

                    string[] thirdSplit = secondSplit[1].Split(':');

                    Int16 hh = Int16.Parse(thirdSplit[0]);
                    Int16 mi = Int16.Parse(thirdSplit[1]);
                    Int16 ss = Int16.Parse(thirdSplit[2]);

                    string ampm = secondSplit[2];
                    if (((ampm == "PM") || (ampm == "pm")) && (hh < 12))
                    {
                        hh += 12;
                    }
                    DateTime baseDT = new DateTime(1970, 1, 1, 4, 0, 0);
                    DateTime dt = new DateTime(yy, mm, dd, hh, mi, ss);
                    TimeSpan span = dt - baseDT;
                    result = span.Ticks.ToString();
                }
                catch
                {
                }
            }
            return result;
        }

        public static string ConvertUnFormattedDate(string value)
        {
            // value of type 2012-6-1T00:00:00

            string result = string.Empty;
            if (value != string.Empty)
            {
                if (value.Length >= 10)
                {
                    DateTime baseDT = new DateTime(1970, 1, 1, 4, 0, 0);

                    Int16 yy = Int16.Parse(value.Substring(0, 4));
                    Int16 mm = Int16.Parse(value.Substring(5, 2));
                    Int16 dd = Int16.Parse(value.Substring(8, 2));

                    Int16 hh = 0;
                    Int16 mi = 0;
                    Int16 ss = 0;
                    if (value.Length >= 19)
                    {
                        hh = Int16.Parse(value.Substring(11, 2));
                        mi = Int16.Parse(value.Substring(14, 2));
                        ss = Int16.Parse(value.Substring(17, 2));
                    }
                    DateTime dt = new DateTime(yy, mm, dd, hh, mi, ss);
                    TimeSpan span = dt - baseDT;
                    result = span.Ticks.ToString();
                }
                else
                {
                    throw new ArgumentException(value + " is an invalid date. Assuming a null value for this date.");
                }
            }
            return result;
        }
        
        public static string QuoteFree(string input)
        {
            return rgxSQuote.Replace(input, repBlank);
        }

        public static string EscapeQuote(string input)
        {
            return rgxSQuote.Replace(input, escQuote);
        }

        public static string SanitizeContainmentName(string input)
        {
            // remove special characters \  /  :  *  ?  "  <  >  | 

            //return rgxForwardSlash.Replace(input, repBlank);    //TODO
            return input;
        }

        public static string GetDictionaryAsString(Dictionary<string, string> map)
        {
            string result = string.Empty;
            foreach(string k in map.Keys)
            {
                result += k;
                result += ":";
                result += map[k];
                result += "; ";
            }
            return result;
        }

        public static void Swap(byte[] data)
        {
            for (int i = 0; i < data.Length; i += 2)
            {
                byte b = data[i];
                data[i] = data[i + 1];
                data[i + 1] = b;
            }
        }

        public static byte[] StripCR(byte[] data)
        {
            string s = Encoding.UTF8.GetString(data, 0, data.Length);
            string s1 = rgxCR.Replace(s, repCR);
            byte[] buffer = Encoding.ASCII.GetBytes(s1);
            return buffer;
        }
	}
}
