﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml;
using DBAdapter;

namespace PreIngest
{
    class Context
    {
        public string dbConnection = string.Empty;
        public Logger logger = null;
        public ConfigManager configMgr = null;
        public BackgroundWorker backgroundWorker = null;
        public string batchID = string.Empty;
        public string jobName = string.Empty;
        public string configLevel = string.Empty;
		public int leadToolsJPEGPasses = -1;			// Uses LeadTools default
		public int leadToolsJPEGQualityFactor = 255;	// Most compression
        public int leadToolsColorFormat = 152; // AO default
        public int leadToolsBWFormat = 149; // AO default

        public MMInterfaces.ICasePropertyEvaluator casePropertyEvaluator = null;
        public MMInterfaces.IDocumentPropertyEvaluator docPropertyEvaluator = null;
        public MMInterfaces.IAnnotationPropertyEvaluator annPropertyEvaluator = null;
        public MMInterfaces.ITaskPropertyEvaluator taskPropertyEvaluator = null;

        public MMInterfaces.ICaseTypeSelector caseTypeSelector = null;
        public MMInterfaces.IDocumentTypeSelector docTypeSelector = null;
        public MMInterfaces.IAnnotationTypeSelector annTypeSelector = null;
        public MMInterfaces.ITaskTypeSelector taskTypeSelector = null;

        public MMInterfaces.ICaseDocGenerator caseDocGenerator = null;
        public MMInterfaces.IDocumentDocGenerator docDocGenerator = null;
        public MMInterfaces.ITaskDocGenerator taskDocGenerator = null;

        public MMInterfaces.ICaseCommentReader caseCommentReader = null;
        public MMInterfaces.IDocumentCommentReader docCommentReader = null;
        public MMInterfaces.ITaskCommentReader taskCommentReader = null;

        public MMInterfaces.IAnnotationReader annotationReader = null;

        public Boolean caseDataLoaded = false;
        public Boolean docDataLoaded = false;
        public Boolean annDataLoaded = false;
        public Boolean taskDataLoaded = false;

        public Context()
        {
        }

        public Boolean IsCaseLevelConfig()
        {
            return (configLevel == "CASE") ? true : false;
        }

		public void Initialize(string cnxn, int jobID, Boolean debugOption, int _leadToolsJPEGPasses, int _leadToolsJPEGQualityFactor, int _leadToolsColorFormat, int _leadToolsBWFormat)
        {
			leadToolsJPEGPasses = _leadToolsJPEGPasses;
			leadToolsJPEGQualityFactor = _leadToolsJPEGQualityFactor;
            leadToolsColorFormat = _leadToolsColorFormat;
            leadToolsBWFormat = _leadToolsBWFormat;
            dbConnection = cnxn;

            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(cnxn, "SELECT j.ConfigID AS ConfigID, j.ConvDefnID AS ConvDefnID, j.JobName AS JobName, c.ConfigLevel AS Level FROM MgrJobs j, MgrConfigurations c WHERE j.ID = " + jobID + " AND j.ConfigID = c.ID");

            configMgr = new ConfigManager(cnxn, xmlDoc.SelectNodes("//row")[0].Attributes["ConfigID"].Value);
            configMgr.InitializeConversions(xmlDoc.SelectNodes("//row")[0].Attributes["ConvDefnID"].Value);

            jobName = xmlDoc.SelectNodes("//row")[0].Attributes["JobName"].Value;
            configLevel = xmlDoc.SelectNodes("//row")[0].Attributes["Level"].Value;

            logger = new Logger(cnxn, jobName, debugOption);

            docDataLoaded = IsDocumentDataLoaded();
            annDataLoaded = IsAnnotationDataLoaded();
            if (IsCaseLevelConfig())
            {
                caseDataLoaded = IsCaseDataLoaded();
                taskDataLoaded = IsTaskDataLoaded();
            }
        }

        private Boolean IsCaseDataLoaded()
        {
            return (GetRecordCount(jobName + "_CaseData") > 0) ? true : false;
        }

        private Boolean IsDocumentDataLoaded()
        {
            return (GetRecordCount(jobName + "_DocumentData") > 0) ? true : false;
        }

        private Boolean IsAnnotationDataLoaded()
        {
            return (GetRecordCount(jobName + "_AnnotationData") > 0) ? true : false;
        }

        private Boolean IsTaskDataLoaded()
        {
            return (GetRecordCount(jobName + "_TaskData") > 0) ? true : false;
        }

        protected int GetRecordCount(string tableName)
        {
            int count = 0;

            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT COUNT(*) AS Result FROM " + tableName);

            count = Convert.ToInt32(xmlDoc.SelectNodes("//row")[0].Attributes["Result"].Value);
            return count;
        }
    }
}
