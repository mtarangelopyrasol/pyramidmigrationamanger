﻿using System;

namespace PreIngest
{
    class TargetCaseProperty : TargetProperty
    {
        public TargetCaseProperty(Context c, string n, string v, string t) : base(c, n, v, t)
        {
        }

        public override void Serialize(string caseID)
        {
            if (!IsValueNull())
            {
                DBAdapter.DBAdapter.AddToDB(contxt.dbConnection, "INSERT INTO " + contxt.jobName + "_CECaseProperties (CaseID, Name, Value, Type) VALUES ( " + caseID + ", '" + Name + "', '" + Converter.EscapeQuote(Value) + "', '" + DataType + "')");
            }
        }
    } 
}
