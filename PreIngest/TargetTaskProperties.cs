﻿using System;
using System.Collections.Generic;

namespace PreIngest
{
    class TargetTaskProperties : TargetProperties
    {
        public TargetTaskProperties(Context c) : base(c)
        {
        }

        public override TargetProperty AddProperty(string n, string v, string t)
        {
            TargetTaskProperty tp = new TargetTaskProperty(contxt, n, GetConvertedValue(n, v, t), t);
            properties.Add(tp);
            return tp;
        }
    }
}
