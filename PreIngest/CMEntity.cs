﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using MMInterfaces;

namespace PreIngest
{
    class CMEntity : ClassBase
    {
        protected EntityStatus entityStatus = EntityStatus.GOOD;
        protected string mmID = string.Empty;
        protected SourceProperties srcProperties = null;
        protected string targetType = string.Empty;
        protected TargetProperties targetProperties = null;
        
        public CMEntity(Context c) : base(c)
        {
        }

        public void PrepareSourceProperties(XmlElement sourceXML)
        {
            srcProperties = new SourceProperties();
            XmlAttributeCollection propCollection = sourceXML.Attributes;
            foreach (XmlAttribute att in propCollection)
            {
                srcProperties.Values[att.Name] = att.Value;
            }
        }

        public virtual void SetSource(XmlElement sourceXML)
        {
        }

        protected virtual void DetermineType()
        {
        }

        protected virtual Boolean IsTargetTypeDefined()
        {
            return false;
        }

        protected string GetSourceValue(string sourceName)
        {
            return (sourceName != string.Empty) ? (srcProperties.Values.ContainsKey(sourceName)) ? srcProperties.Values[sourceName] : string.Empty : string.Empty;
        }

        protected List<string> FetchValues(string tableName, string sourceKey)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(contxt.dbConnection, "SELECT Value FROM " + tableName + " WHERE Link_Key = '" + sourceKey + "'");

            List<string> values = new List<string>();
            foreach (XmlElement rowValues in xml.SelectNodes("//row"))
            {
                values.Add(rowValues.GetAttribute("Value"));
            }
            return values;
        }

        protected List<string> GetValues(ConfigProperty cp, string sourceTable)
        {
            string source = cp.GetSource();
            string fldValue = (source != string.Empty) ? GetSourceValue(source).Trim() : string.Empty;
            List<string> values;

            if (cp.IsMultiValued())
            {
                values = FetchValues(sourceTable + source, fldValue);
            }
            else
            {
                values = new List<string>();
                values.Add(fldValue);
            }
            return values;
        }

        protected string GetEffectiveValue(ConfigProperty cp, string value, string msgQualifier)
        {
            string effectiveValue = value;
            if (cp.IsMapped())
            {
                string origValue = value;
                string mappedValue = cp.GetMappedValue(value);
                //The next 3 lines are commentes out on request of Comerica as they have the old mapping values same as the new mapping values, and these lines of code throw the error.
                //if (mappedValue == origValue)
                    
                        //LogError(msgQualifier + " Could not find translation for code: " + origValue + " for property " + cp.Name + "; using " + origValue + " as is");
                //else
                    effectiveValue = mappedValue;
            }
            return effectiveValue;
        }

        protected void AddTargetProperty(ConfigProperty cp, string value, string msgQualifier)
        {
            if (cp.IsRequired())
            {
                if ((value == null) || (value.Trim().Length == 0))
                {
                    entityStatus = EntityStatus.DISCARD;
                    throw new ArgumentException(msgQualifier + " does not have value for required Property " + cp.Name + ". Discarding this case.");
                }
            }
            targetProperties.AddProperty(cp.Name, value, cp.GetDataType());
        }

        protected void FormulateProperty(ConfigProperty cp, string tableQualifier, string msgQualifier)
        {
            List<string> values = GetValues(cp, tableQualifier);

            foreach (string value in values)
            {
                try
                {
                    AddTargetProperty(cp, GetEffectiveValue(cp, value, msgQualifier), msgQualifier);
                }
                catch (Exception ex)
                {
                    LogError(msgQualifier + " Could not add property: " + cp.Name + " using " + value + " as is." + ex.Message);
                    entityStatus = EntityStatus.ERROR;
                }
            }
        }

        protected virtual void FormulateComputedProperty(ConfigProperty cp, MMInterfaces.IPropertyEvaluator ipe, string msgQualifier)
        {
            if (entityStatus == EntityStatus.DISCARD) return;

            if (ipe == null)
            {
                entityStatus = EntityStatus.DISCARD;
                throw new ArgumentException(msgQualifier + " Could not find evaluator for computed Property " + cp.Name + ". Discarding this case.");
            }
            else
            {
                List<string> valueList = ipe.Evaluate(cp.Name, cp.GetDataType(), srcProperties);

                foreach (string value in valueList)
                {
                    AddTargetProperty(cp, value, msgQualifier);
                }
            }
        }

        protected void FormulateProperties(List<ConfigProperty> lps, string tableQualifier, IPropertyEvaluator ipe, string msgQualifier)
        {
            foreach (ConfigProperty cp in lps)
            {
                if (contxt.backgroundWorker.CancellationPending) break;

                if (cp.IsComputed())
                    FormulateComputedProperty(cp, ipe, msgQualifier);
                else
                    FormulateProperty(cp, tableQualifier, msgQualifier);
            }
        }

        protected void UpdateSourceStatus(ConversionResult convResult, string tableName)
        {
            DBAdapter.DBAdapter.UpdateDB(contxt.dbConnection,
                "UPDATE " + tableName + " SET Status = '" + convResult.ToString() + "' WHERE ID = " + GetSourceValue("ID"));
        }
    }
}
