﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PreIngest
{
    public class ConfigProperties
    {
        public List<ConfigProperty> properties = null;

        public ConfigProperties()
        {
            properties = new List<ConfigProperty>();
        }

        public void AddProperty(ConfigProperty cp)
        {
            properties.Add(cp);
        }

        public List<ConfigProperty> GetProperties()
        {
            return properties;
        }
    }
}
