﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class LoadDataDialog : Form
    {
        string dbCnxn;
        string tableName;
        string configTable;
        string configID;
        string sourceDir = string.Empty;
        Boolean isMultiValueFieldLoad = false;
        string[] dataFiles = null;

        public LoadDataDialog(string dbConnection, string table, string cfgTable, string cfgID, Boolean isMultiValueLoad)
        {
            dbCnxn = dbConnection;
            tableName = table;
            configTable = cfgTable;
            configID = cfgID;
            isMultiValueFieldLoad = isMultiValueLoad;

            InitializeComponent();

            lblHeader.Text = "Data Load to Table: " + tableName;
            lblWarning.Text = "Note: The data files must be valid path names from the server on which SQL Server is running. If the data files are" + Environment.NewLine + 
                                    "remote files, specify the Universal Naming Convention (UNC) names.";
            lblLoading.Hide();

            if (isMultiValueFieldLoad)
            {
                btnLoadFields.Hide();
                this.BackColor = System.Drawing.Color.CornflowerBlue;
            }
            else if (HasMultiValueFields(table))
            {
                this.BackColor = System.Drawing.Color.LightBlue;
                btnLoadFields.Show();
            }
            else
            {
                this.BackColor = System.Drawing.Color.LightBlue;
                btnLoadFields.Hide();
            }
        }

        private Boolean HasMultiValueFields(string tableName)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT COUNT(*) AS MULTIVALUEFIELDCOUNT FROM " + configTable + " c WHERE c.MultiValued = 'Y' AND ConfigID = " + configID);
            string value = xml.SelectNodes("//row")[0].Attributes["MULTIVALUEFIELDCOUNT"].Value;
            Int32 count = Convert.ToInt32(value);
            return (count > 0) ? true : false;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlgOpen = new OpenFileDialog();

            dlgOpen.InitialDirectory = sourceDir;
            dlgOpen.Filter = ".txt files (*.txt)|*.txt";
            dlgOpen.FilterIndex = 1;
            dlgOpen.RestoreDirectory = true;
            dlgOpen.Multiselect = true;
            dlgOpen.SupportMultiDottedExtensions = true;
            dlgOpen.Title = "Select Metadata File";

            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                dataFiles = dlgOpen.FileNames;
                txtFile.Text = string.Empty;
                for (int i = 0; i < dataFiles.Length; i++)
                {
                    txtFile.Text += dataFiles[i];
                    txtFile.Text += Environment.NewLine;
                }
                sourceDir = Path.GetDirectoryName(dlgOpen.FileNames[0]);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (txtFile.Text.Length == 0)
            {
                MessageBox.Show("Please specify a data file", "Migration Manager", MessageBoxButtons.OK);
                DialogResult = DialogResult.None;
            }
            else
            {
                List<string> passed = new List<string>();
                List<string> failed = new List<string>();
                foreach (string dataFile in dataFiles)
                {
                    try
                    {
                        btnLoad.Enabled = false;
                        btnCancel.Enabled = false;
                        lblLoading.Show();
                        this.Refresh();
                        int nRecs = DBAdapter.DBAdapter.BulkLoadData(dbCnxn, tableName, dataFile, isMultiValueFieldLoad);
                        lblLoading.Hide();
                        this.Refresh();
                        passed.Add(dataFile + ": " + nRecs.ToString() + " records loaded");   
                    }
                    catch (Exception ex)
                    {
                        failed.Add(dataFile + ": Load failed: " + ex.Message);  
                    }
                }
                if (failed.Count > 0)
                {
                    string msg = "The following data files failed to load:\n";
                    foreach(string s in failed)
                    {
                        msg += s;
                        msg += Environment.NewLine;
                        msg += Environment.NewLine;
                    }
                    MessageBox.Show(msg);
                    DialogResult = DialogResult.Cancel;
                }
                if (passed.Count > 0)
                {
                    string msg = "The following data files loaded:\n";
                    foreach (string s in passed)
                    {
                        msg += s;
                        msg += Environment.NewLine;
                    }
                    MessageBox.Show(msg);
                    DialogResult = DialogResult.OK;
                }
                this.Close();
            }
        }

        private void btnLoadFields_Click(object sender, EventArgs e)
        {
            string fieldTableName = tableName + "_Values" + "_DocumentData";

            using (MultiValueFieldPickDialog dlg = new MultiValueFieldPickDialog(dbCnxn, tableName, configTable))
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                }
            }
        }
    }
}
