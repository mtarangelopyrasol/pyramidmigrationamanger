﻿namespace MigrationManager
{
    partial class MigrationManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MigrationManager));
            this.btnExit = new System.Windows.Forms.Button();
            this.lblMigrator = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.pageConfigure = new System.Windows.Forms.TabPage();
            this.tabConfigure = new System.Windows.Forms.TabControl();
            this.createConfig = new System.Windows.Forms.TabPage();
            this.btnDeleteConfig = new System.Windows.Forms.Button();
            this.btnUpdateConfig = new System.Windows.Forms.Button();
            this.btnAddConfiguration = new System.Windows.Forms.Button();
            this.btnExportConfig = new System.Windows.Forms.Button();
            this.btnImportConfig = new System.Windows.Forms.Button();
            this.lvConfigs = new System.Windows.Forms.ListView();
            this.colConfigName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colConfigLevel = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colLastUpdated = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colConfigComment = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblConfig = new System.Windows.Forms.Label();
            this.sourceConfig = new System.Windows.Forms.TabPage();
            this.tabConfigSource = new System.Windows.Forms.TabControl();
            this.sourceCases = new System.Windows.Forms.TabPage();
            this.lvSourceCase = new System.Windows.Forms.ListView();
            this.srcColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.srcName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.srcDataType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.srcLength = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.srcFormat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.srcMultiValued = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblSourceCaseConfig = new System.Windows.Forms.Label();
            this.btnSrcCfgCaseAdd = new System.Windows.Forms.Button();
            this.btnSrcCfgCaseUpd = new System.Windows.Forms.Button();
            this.btnSrcCfgCaseDel = new System.Windows.Forms.Button();
            this.btnSrcCfgCaseExp = new System.Windows.Forms.Button();
            this.btnSrcCfgCaseImp = new System.Windows.Forms.Button();
            this.sourceDocuments = new System.Windows.Forms.TabPage();
            this.lvSourceDocument = new System.Windows.Forms.ListView();
            this.colSrcDocColum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSrcDocName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSrcDocDataType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSrcDocLength = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSrcDocFormat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSrcDocMultiValued = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblSourceDocConfig = new System.Windows.Forms.Label();
            this.btnSrcCfgDocAdd = new System.Windows.Forms.Button();
            this.btnSrcCfgDocUpd = new System.Windows.Forms.Button();
            this.btnSrcCfgDocDel = new System.Windows.Forms.Button();
            this.btnSrcCfgDocExp = new System.Windows.Forms.Button();
            this.btnSrcCfgDocImp = new System.Windows.Forms.Button();
            this.sourceAnnotations = new System.Windows.Forms.TabPage();
            this.lvSourceAnnotation = new System.Windows.Forms.ListView();
            this.colSrcAnnColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSrcAnnName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSrcAnnDataType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSrcAnnLength = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSrcAnnFormat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSrcAnnMultiValued = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblSourceAnnConfig = new System.Windows.Forms.Label();
            this.btnSrcCfgAnnAdd = new System.Windows.Forms.Button();
            this.btnSrcCfgAnnUpd = new System.Windows.Forms.Button();
            this.btnSrcCfgAnnDel = new System.Windows.Forms.Button();
            this.btnSrcCfgAnnExp = new System.Windows.Forms.Button();
            this.btnSrcCfgAnnImp = new System.Windows.Forms.Button();
            this.sourceTasks = new System.Windows.Forms.TabPage();
            this.lvSourceTask = new System.Windows.Forms.ListView();
            this.colSrcTaskColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSrcTaskName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSrcTaskDataType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSrcTaskLength = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSrcTaskFormat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSrcTaskMultiValued = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblSourceTaskConfig = new System.Windows.Forms.Label();
            this.btnSrcCfgTaskAdd = new System.Windows.Forms.Button();
            this.btnSrcCfgTaskUpd = new System.Windows.Forms.Button();
            this.btnSrcCfgTaskDel = new System.Windows.Forms.Button();
            this.btnSrcCfgTaskExp = new System.Windows.Forms.Button();
            this.btnSrcCfgTaskImp = new System.Windows.Forms.Button();
            this.targetConfig = new System.Windows.Forms.TabPage();
            this.tabConfigTarget = new System.Windows.Forms.TabControl();
            this.targetCases = new System.Windows.Forms.TabPage();
            this.lvTargetCaseFolders = new System.Windows.Forms.ListView();
            this.colTgtCaseFolderName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtCaseFolderParent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblTargetCaseFolders = new System.Windows.Forms.Label();
            this.lvTargetCaseProperties = new System.Windows.Forms.ListView();
            this.colCasePropName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCasePropSource = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCasePropType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCasePropReq = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCasePropMapped = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCasePropComputed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCasePropMapName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblTargetCaseProperties = new System.Windows.Forms.Label();
            this.lvTargetCaseTypes = new System.Windows.Forms.ListView();
            this.colTargetCaseType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDefaultDocClass = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblTargetCaseConfig = new System.Windows.Forms.Label();
            this.btnTgtCfgCaseAdd = new System.Windows.Forms.Button();
            this.btnTgtCfgCaseUpd = new System.Windows.Forms.Button();
            this.btnTgtCfgCaseDel = new System.Windows.Forms.Button();
            this.btnTgtCfgCaseExp = new System.Windows.Forms.Button();
            this.btnTgtCfgCaseImp = new System.Windows.Forms.Button();
            this.targetDocuments = new System.Windows.Forms.TabPage();
            this.lvTargetDocProperties = new System.Windows.Forms.ListView();
            this.colTgtDocPropName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtDocPropSource = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtDocPropType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtDocPropReq = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtDocPropMap = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtDocPropComputed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtDocMapName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblTargetDocProperties = new System.Windows.Forms.Label();
            this.lvTargetDocClasses = new System.Windows.Forms.ListView();
            this.colTargetDocClassName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblTargetDocConfig = new System.Windows.Forms.Label();
            this.btnTgtCfgDocAdd = new System.Windows.Forms.Button();
            this.btnTgtCfgDocUpd = new System.Windows.Forms.Button();
            this.btnTgtCfgDocDel = new System.Windows.Forms.Button();
            this.btnTgtCfgDocExp = new System.Windows.Forms.Button();
            this.btnTgtCfgDocImp = new System.Windows.Forms.Button();
            this.targetAnnotations = new System.Windows.Forms.TabPage();
            this.lvTargetAnnProperties = new System.Windows.Forms.ListView();
            this.colTgtAnnPropName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtAnnPropSource = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtAnnPropType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtAnnPropReq = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtAnnPropMap = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtAnnPropComputed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtAnnMapName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblTargetAnnProperties = new System.Windows.Forms.Label();
            this.lvTargetAnnClasses = new System.Windows.Forms.ListView();
            this.colTargetAnnClassName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblTargetAnnConfig = new System.Windows.Forms.Label();
            this.btnTgtCfgAnnAdd = new System.Windows.Forms.Button();
            this.btnTgtCfgAnnUpd = new System.Windows.Forms.Button();
            this.btnTgtCfgAnnDel = new System.Windows.Forms.Button();
            this.btnTgtCfgAnnExp = new System.Windows.Forms.Button();
            this.btnTgtCfgAnnImp = new System.Windows.Forms.Button();
            this.targetTasks = new System.Windows.Forms.TabPage();
            this.lvTargetInitDocs = new System.Windows.Forms.ListView();
            this.colTaskDocInit = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDocInitDocClass = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colInitDocSource = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colAttachProperty = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblTargetInitDocs = new System.Windows.Forms.Label();
            this.lvTargetTaskProperties = new System.Windows.Forms.ListView();
            this.colTgtTaskPropName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtTaskPropSource = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtTaskPropType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtPropTaskReq = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtTaskPropMap = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtTaskPropComputed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtTaskMapName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblTargetTaskProperties = new System.Windows.Forms.Label();
            this.lvTargetTaskTypes = new System.Windows.Forms.ListView();
            this.colTgtTaskTypes = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTgtTaskCaseName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblTargetTaskConfig = new System.Windows.Forms.Label();
            this.btnTgtCfgTaskAdd = new System.Windows.Forms.Button();
            this.btnTgtCfgTaskUpd = new System.Windows.Forms.Button();
            this.btnTgtCfgTaskDel = new System.Windows.Forms.Button();
            this.btnTgtCfgTaskExp = new System.Windows.Forms.Button();
            this.btnTgtCfgTaskImp = new System.Windows.Forms.Button();
            this.defineIdentifiers = new System.Windows.Forms.TabPage();
            this.lvIdentifierDefns = new System.Windows.Forms.ListView();
            this.colIdentifier = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colIdDefn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblIdConfig = new System.Windows.Forms.Label();
            this.btnUpdIdentifier = new System.Windows.Forms.Button();
            this.btnExpIdentifier = new System.Windows.Forms.Button();
            this.btnImpIdentifier = new System.Windows.Forms.Button();
            this.pageDefine = new System.Windows.Forms.TabPage();
            this.tabDefine = new System.Windows.Forms.TabControl();
            this.defineConversions = new System.Windows.Forms.TabPage();
            this.lvConvDefns = new System.Windows.Forms.ListView();
            this.colCDName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSingleDoc = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblConvDefns = new System.Windows.Forms.Label();
            this.btnConvDefnAdd = new System.Windows.Forms.Button();
            this.btnConvDefnUpd = new System.Windows.Forms.Button();
            this.btnConvDefnDel = new System.Windows.Forms.Button();
            this.btnConvExport = new System.Windows.Forms.Button();
            this.btnConvImport = new System.Windows.Forms.Button();
            this.targetMappings = new System.Windows.Forms.TabPage();
            this.lvMapDefns = new System.Windows.Forms.ListView();
            this.colMapName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblMapDefns = new System.Windows.Forms.Label();
            this.btnMapDefnAdd = new System.Windows.Forms.Button();
            this.btnMapDefnUpd = new System.Windows.Forms.Button();
            this.btnMapDefnDel = new System.Windows.Forms.Button();
            this.btnMapDefnExport = new System.Windows.Forms.Button();
            this.btnMapDefnImport = new System.Windows.Forms.Button();
            this.defineJobs = new System.Windows.Forms.TabPage();
            this.btnAddJob = new System.Windows.Forms.Button();
            this.btnEditJob = new System.Windows.Forms.Button();
            this.btnDeleteJob = new System.Windows.Forms.Button();
            this.btnExportJob = new System.Windows.Forms.Button();
            this.btnImportJob = new System.Windows.Forms.Button();
            this.lvJobDefns = new System.Windows.Forms.ListView();
            this.colJobName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colJobConfig = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colJobConv = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colJobCreated = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colJobComment = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblJobDefns = new System.Windows.Forms.Label();
            this.defineBatches = new System.Windows.Forms.TabPage();
            this.btnAddBatchDefn = new System.Windows.Forms.Button();
            this.btnUpdBatchDefn = new System.Windows.Forms.Button();
            this.btnDelBatchDefn = new System.Windows.Forms.Button();
            this.btnExpBatchDefn = new System.Windows.Forms.Button();
            this.btnImpBatchDefn = new System.Windows.Forms.Button();
            this.lvBatchDefns = new System.Windows.Forms.ListView();
            this.colBDName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colBDSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colBDCondition = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colBDSrcPrefix = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colBDStagingPath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblBatchDefns = new System.Windows.Forms.Label();
            this.pageLoad = new System.Windows.Forms.TabPage();
            this.tabLoad = new System.Windows.Forms.TabControl();
            this.JobLoad = new System.Windows.Forms.TabPage();
            this.lvLoadJobs = new System.Windows.Forms.ListView();
            this.colLoadJobID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colLoadJobName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colLoadComment = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblJobs = new System.Windows.Forms.Label();
            this.CaseLoad = new System.Windows.Forms.TabPage();
            this.btnLoadCaseData = new System.Windows.Forms.Button();
            this.btnNextCaseData = new System.Windows.Forms.Button();
            this.btnPrevCaseData = new System.Windows.Forms.Button();
            this.btnFindCaseID = new System.Windows.Forms.Button();
            this.txtCaseID = new System.Windows.Forms.TextBox();
            this.lblCaseID = new System.Windows.Forms.Label();
            this.lblCaseDataCount = new System.Windows.Forms.Label();
            this.lvCaseData = new System.Windows.Forms.ListView();
            this.lblCaseData = new System.Windows.Forms.Label();
            this.DocumentLoad = new System.Windows.Forms.TabPage();
            this.lblDocumentDataCount = new System.Windows.Forms.Label();
            this.btnLoadDocumentData = new System.Windows.Forms.Button();
            this.btnNextDocumentData = new System.Windows.Forms.Button();
            this.btnPrevDocumentData = new System.Windows.Forms.Button();
            this.btnFindDocID = new System.Windows.Forms.Button();
            this.txtDocID = new System.Windows.Forms.TextBox();
            this.lblDocID = new System.Windows.Forms.Label();
            this.lvDocumentData = new System.Windows.Forms.ListView();
            this.lblDocumentData = new System.Windows.Forms.Label();
            this.AnnotationLoad = new System.Windows.Forms.TabPage();
            this.btnLoadAnnotationData = new System.Windows.Forms.Button();
            this.btnNextAnnotationData = new System.Windows.Forms.Button();
            this.btnPrevAnnotationData = new System.Windows.Forms.Button();
            this.btnFindAnnID = new System.Windows.Forms.Button();
            this.txtAnnID = new System.Windows.Forms.TextBox();
            this.lblAnnID = new System.Windows.Forms.Label();
            this.lblAnnotationDataCount = new System.Windows.Forms.Label();
            this.lvAnnotationData = new System.Windows.Forms.ListView();
            this.lblAnnotationData = new System.Windows.Forms.Label();
            this.TaskLoad = new System.Windows.Forms.TabPage();
            this.btnLoadTaskData = new System.Windows.Forms.Button();
            this.btnNextTaskData = new System.Windows.Forms.Button();
            this.btnPrevTaskData = new System.Windows.Forms.Button();
            this.btnFindTaskID = new System.Windows.Forms.Button();
            this.txtTaskID = new System.Windows.Forms.TextBox();
            this.lblTaskID = new System.Windows.Forms.Label();
            this.lblTaskDataCount = new System.Windows.Forms.Label();
            this.lvTaskData = new System.Windows.Forms.ListView();
            this.lblTaskData = new System.Windows.Forms.Label();
            this.pagePrepare = new System.Windows.Forms.TabPage();
            this.tabPrepare = new System.Windows.Forms.TabControl();
            this.ChooseJob = new System.Windows.Forms.TabPage();
            this.listJobs = new System.Windows.Forms.ListView();
            this.colConvJobID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colConvJobName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colConvComment = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblConvJobs = new System.Windows.Forms.Label();
            this.ChooseBatchDefn = new System.Windows.Forms.TabPage();
            this.listBatchDefns = new System.Windows.Forms.ListView();
            this.colConvBatchDefnName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colConvBatchSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colConvCondition = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colConvSrcPrefix = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colConvStagingPath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblConvBatchDefns = new System.Windows.Forms.Label();
            this.PerformConversion = new System.Windows.Forms.TabPage();
            this.txtMaxBatches = new System.Windows.Forms.TextBox();
            this.lblMaxBatches = new System.Windows.Forms.Label();
            this.convStatus = new System.Windows.Forms.ListBox();
            this.btnConvert = new System.Windows.Forms.Button();
            this.btnConvCancel = new System.Windows.Forms.Button();
            this.lblConvBatch = new System.Windows.Forms.Label();
            this.lblConvJob = new System.Windows.Forms.Label();
            this.ConvLogs = new System.Windows.Forms.TabPage();
            this.tabConvLog = new System.Windows.Forms.TabControl();
            this.ConvResultSession = new System.Windows.Forms.TabPage();
            this.listConvSessions = new System.Windows.Forms.ListView();
            this.colConvSessionID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colConvStart = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colConvEnd = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colConvUser = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colConvWkstn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colConvStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblConvSessions = new System.Windows.Forms.Label();
            this.ConvResultBatch = new System.Windows.Forms.TabPage();
            this.listConvBatches = new System.Windows.Forms.ListView();
            this.convLogBatchID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.convBatchLogSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.convLogBatchStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.convLogBatchStart = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.convLogBatchEnd = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.convLogBatchUser = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.convLogBatchWkstn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblConvBatches = new System.Windows.Forms.Label();
            this.ConvResultView = new System.Windows.Forms.TabPage();
            this.btnNextConvLog = new System.Windows.Forms.Button();
            this.btnPrevConvLog = new System.Windows.Forms.Button();
            this.lblConvLogCount = new System.Windows.Forms.Label();
            this.listConvBatchLog = new System.Windows.Forms.ListView();
            this.colCBLogTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCBLogEvent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCBEventDesc = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblConvBatchLog = new System.Windows.Forms.Label();
            this.ConvErrorView = new System.Windows.Forms.TabPage();
            this.listConvBatchErrors = new System.Windows.Forms.ListView();
            this.convErrorTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.convErrorEvent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblConvBatchErrors = new System.Windows.Forms.Label();
            this.ConvResults = new System.Windows.Forms.TabPage();
            this.btnConvResultDocID = new System.Windows.Forms.Button();
            this.txtConvResultDocID = new System.Windows.Forms.TextBox();
            this.lblConvResultDocID = new System.Windows.Forms.Label();
            this.btnConvResultCaseID = new System.Windows.Forms.Button();
            this.txtConvResultCaseID = new System.Windows.Forms.TextBox();
            this.lblConvResultCaseID = new System.Windows.Forms.Label();
            this.txtConvResultBatch = new System.Windows.Forms.TextBox();
            this.lblConvResultBatch = new System.Windows.Forms.Label();
            this.treeConvResult = new System.Windows.Forms.TreeView();
            this.pageMigrate = new System.Windows.Forms.TabPage();
            this.tabMigrate = new System.Windows.Forms.TabControl();
            this.MigrateJob = new System.Windows.Forms.TabPage();
            this.listMigrateJobs = new System.Windows.Forms.ListView();
            this.colMigrateJobID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMigrateJobName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMigrateJobComment = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblMigrateJobs = new System.Windows.Forms.Label();
            this.MigrateBatch = new System.Windows.Forms.TabPage();
            this.listMigrateBatches = new System.Windows.Forms.ListView();
            this.migBatchID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.migBatchSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.migBatchStart = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.migBatchEnd = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.migBatchUser = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.migBatchWkstn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.migBatchStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblMigrateBatches = new System.Windows.Forms.Label();
            this.PerformIngest = new System.Windows.Forms.TabPage();
            this.btnMigrate = new System.Windows.Forms.Button();
            this.btnMigrateCancel = new System.Windows.Forms.Button();
            this.migrateStatus = new System.Windows.Forms.ListBox();
            this.ckTasks = new System.Windows.Forms.CheckBox();
            this.ckAnnotations = new System.Windows.Forms.CheckBox();
            this.ckDocuments = new System.Windows.Forms.CheckBox();
            this.ckCases = new System.Windows.Forms.CheckBox();
            this.ckDepthFirst = new System.Windows.Forms.CheckBox();
            this.lblMigrateBatch = new System.Windows.Forms.Label();
            this.lblMigrateJob = new System.Windows.Forms.Label();
            this.IngestLogs = new System.Windows.Forms.TabPage();
            this.tabIngestLog = new System.Windows.Forms.TabControl();
            this.IngestResultSession = new System.Windows.Forms.TabPage();
            this.listIngestSessions = new System.Windows.Forms.ListView();
            this.ingestSession = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ingestStart = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ingestEnd = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ingestUser = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ingestWkstn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ingestStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblIngestSessions = new System.Windows.Forms.Label();
            this.IngestResultBatch = new System.Windows.Forms.TabPage();
            this.listIngestBatches = new System.Windows.Forms.ListView();
            this.ingBatchID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ingStartTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ingEndTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ingUser = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ingWkstn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ingStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ingComment = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblIngestBatches = new System.Windows.Forms.Label();
            this.IngestResultView = new System.Windows.Forms.TabPage();
            this.btnNextIngestLog = new System.Windows.Forms.Button();
            this.btnPrevIngestLog = new System.Windows.Forms.Button();
            this.lblIngestLogCount = new System.Windows.Forms.Label();
            this.listIngestBatchLog = new System.Windows.Forms.ListView();
            this.ingOccurred = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ingEvent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ingEventDesc = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblIngestBatchLog = new System.Windows.Forms.Label();
            this.IngestErrorView = new System.Windows.Forms.TabPage();
            this.listIngestBatchErrors = new System.Windows.Forms.ListView();
            this.migOccurrence = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.migEventDesc = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblIngestBatchErrors = new System.Windows.Forms.Label();
            this.IngestResults = new System.Windows.Forms.TabPage();
            this.btnMigResultTaskID = new System.Windows.Forms.Button();
            this.btnMigResultAnnID = new System.Windows.Forms.Button();
            this.btnMigResultDocID = new System.Windows.Forms.Button();
            this.btnMigResultCaseID = new System.Windows.Forms.Button();
            this.txtMigResultID = new System.Windows.Forms.TextBox();
            this.lblMigResultID = new System.Windows.Forms.Label();
            this.treeIngestResult = new System.Windows.Forms.TreeView();
            this.pageReset = new System.Windows.Forms.TabPage();
            this.tabReset = new System.Windows.Forms.TabControl();
            this.ResetConversion = new System.Windows.Forms.TabPage();
            this.tabConversionReset = new System.Windows.Forms.TabControl();
            this.JobsForConversionReset = new System.Windows.Forms.TabPage();
            this.btnResetJobConvErrors = new System.Windows.Forms.Button();
            this.btnResetJobs = new System.Windows.Forms.Button();
            this.lblResetJobsConfirm = new System.Windows.Forms.Label();
            this.listConversionResetJobs = new System.Windows.Forms.ListView();
            this.resetJobID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.resetJobName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.resetComment = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblResetJobs = new System.Windows.Forms.Label();
            this.BatchesForConversionReset = new System.Windows.Forms.TabPage();
            this.btnConvResetErrors = new System.Windows.Forms.Button();
            this.btnResetBatches = new System.Windows.Forms.Button();
            this.lblResetBatchConfirm = new System.Windows.Forms.Label();
            this.listResetBatches = new System.Windows.Forms.ListView();
            this.rstBatchID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rstJob = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rstSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rstStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rstStart = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rstEnd = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rstUser = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rstWkstn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblResetbatches = new System.Windows.Forms.Label();
            this.AccountForConversionReset = new System.Windows.Forms.TabPage();
            this.btnResetAccount = new System.Windows.Forms.Button();
            this.txtResetAccount = new System.Windows.Forms.TextBox();
            this.lblResetAccount = new System.Windows.Forms.Label();
            this.lblResetAccountConfirm = new System.Windows.Forms.Label();
            this.listResetAccountJobs = new System.Windows.Forms.ListView();
            this.resetAccountJobID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.resetAccountJobName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.resetAccountJobComment = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblResetAccountJobs = new System.Windows.Forms.Label();
            this.ResetIngestion = new System.Windows.Forms.TabPage();
            this.tabIngestionReset = new System.Windows.Forms.TabControl();
            this.JobsForIngestionReset = new System.Windows.Forms.TabPage();
            this.btnIngestionResetJobs = new System.Windows.Forms.Button();
            this.lblIngestionResetJobsConfirm = new System.Windows.Forms.Label();
            this.listIngestionResetJobs = new System.Windows.Forms.ListView();
            this.IngResetJobID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.IngResetJobName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.IngResetJobComment = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblIngestionResetJobs = new System.Windows.Forms.Label();
            this.ConvBatchesForIngestionReset = new System.Windows.Forms.TabPage();
            this.btnResetConvBatches = new System.Windows.Forms.Button();
            this.lblResetConvBatchConfirm = new System.Windows.Forms.Label();
            this.listResetConvBatches = new System.Windows.Forms.ListView();
            this.cbResetBatchID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cbResetJob = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cbResetSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cbResetStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cbResetStart = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cbResetEnd = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cbResetUser = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cbResetWkstn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblResetConvBatches = new System.Windows.Forms.Label();
            this.MigBatchesForIngestionReset = new System.Windows.Forms.TabPage();
            this.btnResetMigBatches = new System.Windows.Forms.Button();
            this.btnResetErrors = new System.Windows.Forms.Button();
            this.lblResetMigBatchConfirm = new System.Windows.Forms.Label();
            this.listResetMigBatches = new System.Windows.Forms.ListView();
            this.migResetBatches = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.migResetJob = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.migResetStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.migResetStart = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.migResetEnd = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MigResetUser = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.migResetWkstn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.migResetComment = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblResetMigBatches = new System.Windows.Forms.Label();
            this.IdForIngestionReset = new System.Windows.Forms.TabPage();
            this.btnIngestResetTaskID = new System.Windows.Forms.Button();
            this.btnIngestResetAnnID = new System.Windows.Forms.Button();
            this.btnIngestResetDocID = new System.Windows.Forms.Button();
            this.btnIngestResetCaseID = new System.Windows.Forms.Button();
            this.txtIngestResetID = new System.Windows.Forms.TextBox();
            this.lblIngestResetID = new System.Windows.Forms.Label();
            this.lblResetIDConfirm = new System.Windows.Forms.Label();
            this.listIngestResetIDJobs = new System.Windows.Forms.ListView();
            this.resetIDJobID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.resetIDJobName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.resetIDJobComment = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblResetIDJobs = new System.Windows.Forms.Label();
            this.bgConverter = new System.ComponentModel.BackgroundWorker();
            this.bgMigrator = new System.ComponentModel.BackgroundWorker();
            this.tabMain.SuspendLayout();
            this.pageConfigure.SuspendLayout();
            this.tabConfigure.SuspendLayout();
            this.createConfig.SuspendLayout();
            this.sourceConfig.SuspendLayout();
            this.tabConfigSource.SuspendLayout();
            this.sourceCases.SuspendLayout();
            this.sourceDocuments.SuspendLayout();
            this.sourceAnnotations.SuspendLayout();
            this.sourceTasks.SuspendLayout();
            this.targetConfig.SuspendLayout();
            this.tabConfigTarget.SuspendLayout();
            this.targetCases.SuspendLayout();
            this.targetDocuments.SuspendLayout();
            this.targetAnnotations.SuspendLayout();
            this.targetTasks.SuspendLayout();
            this.defineIdentifiers.SuspendLayout();
            this.pageDefine.SuspendLayout();
            this.tabDefine.SuspendLayout();
            this.defineConversions.SuspendLayout();
            this.targetMappings.SuspendLayout();
            this.defineJobs.SuspendLayout();
            this.defineBatches.SuspendLayout();
            this.pageLoad.SuspendLayout();
            this.tabLoad.SuspendLayout();
            this.JobLoad.SuspendLayout();
            this.CaseLoad.SuspendLayout();
            this.DocumentLoad.SuspendLayout();
            this.AnnotationLoad.SuspendLayout();
            this.TaskLoad.SuspendLayout();
            this.pagePrepare.SuspendLayout();
            this.tabPrepare.SuspendLayout();
            this.ChooseJob.SuspendLayout();
            this.ChooseBatchDefn.SuspendLayout();
            this.PerformConversion.SuspendLayout();
            this.ConvLogs.SuspendLayout();
            this.tabConvLog.SuspendLayout();
            this.ConvResultSession.SuspendLayout();
            this.ConvResultBatch.SuspendLayout();
            this.ConvResultView.SuspendLayout();
            this.ConvErrorView.SuspendLayout();
            this.ConvResults.SuspendLayout();
            this.pageMigrate.SuspendLayout();
            this.tabMigrate.SuspendLayout();
            this.MigrateJob.SuspendLayout();
            this.MigrateBatch.SuspendLayout();
            this.PerformIngest.SuspendLayout();
            this.IngestLogs.SuspendLayout();
            this.tabIngestLog.SuspendLayout();
            this.IngestResultSession.SuspendLayout();
            this.IngestResultBatch.SuspendLayout();
            this.IngestResultView.SuspendLayout();
            this.IngestErrorView.SuspendLayout();
            this.IngestResults.SuspendLayout();
            this.pageReset.SuspendLayout();
            this.tabReset.SuspendLayout();
            this.ResetConversion.SuspendLayout();
            this.tabConversionReset.SuspendLayout();
            this.JobsForConversionReset.SuspendLayout();
            this.BatchesForConversionReset.SuspendLayout();
            this.AccountForConversionReset.SuspendLayout();
            this.ResetIngestion.SuspendLayout();
            this.tabIngestionReset.SuspendLayout();
            this.JobsForIngestionReset.SuspendLayout();
            this.ConvBatchesForIngestionReset.SuspendLayout();
            this.MigBatchesForIngestionReset.SuspendLayout();
            this.IdForIngestionReset.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(752, 514);
            this.btnExit.Margin = new System.Windows.Forms.Padding(2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(56, 23);
            this.btnExit.TabIndex = 0;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblMigrator
            // 
            this.lblMigrator.AutoSize = true;
            this.lblMigrator.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.lblMigrator.Font = new System.Drawing.Font("Trebuchet MS", 19.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMigrator.Location = new System.Drawing.Point(15, 16);
            this.lblMigrator.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMigrator.Name = "lblMigrator";
            this.lblMigrator.Size = new System.Drawing.Size(349, 35);
            this.lblMigrator.TabIndex = 1;
            this.lblMigrator.Text = "Pyramid Migration Manager";
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.AutoSize = true;
            this.lblVersion.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.lblVersion.Font = new System.Drawing.Font("Trebuchet MS", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.Location = new System.Drawing.Point(750, 16);
            this.lblVersion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(63, 16);
            this.lblVersion.TabIndex = 3;
            this.lblVersion.Text = "Version 1.1";
            // 
            // tabMain
            // 
            this.tabMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabMain.Controls.Add(this.pageConfigure);
            this.tabMain.Controls.Add(this.pageDefine);
            this.tabMain.Controls.Add(this.pageLoad);
            this.tabMain.Controls.Add(this.pagePrepare);
            this.tabMain.Controls.Add(this.pageMigrate);
            this.tabMain.Controls.Add(this.pageReset);
            this.tabMain.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabMain.ItemSize = new System.Drawing.Size(85, 28);
            this.tabMain.Location = new System.Drawing.Point(9, 59);
            this.tabMain.Margin = new System.Windows.Forms.Padding(2);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(800, 448);
            this.tabMain.TabIndex = 5;
            this.tabMain.SelectedIndexChanged += new System.EventHandler(this.tabMain_SelectedIndexChanged);
            // 
            // pageConfigure
            // 
            this.pageConfigure.Controls.Add(this.tabConfigure);
            this.pageConfigure.Location = new System.Drawing.Point(4, 32);
            this.pageConfigure.Margin = new System.Windows.Forms.Padding(2);
            this.pageConfigure.Name = "pageConfigure";
            this.pageConfigure.Size = new System.Drawing.Size(792, 412);
            this.pageConfigure.TabIndex = 0;
            this.pageConfigure.Text = "Configure";
            this.pageConfigure.UseVisualStyleBackColor = true;
            // 
            // tabConfigure
            // 
            this.tabConfigure.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabConfigure.Controls.Add(this.createConfig);
            this.tabConfigure.Controls.Add(this.sourceConfig);
            this.tabConfigure.Controls.Add(this.targetConfig);
            this.tabConfigure.Controls.Add(this.defineIdentifiers);
            this.tabConfigure.Location = new System.Drawing.Point(0, 0);
            this.tabConfigure.Margin = new System.Windows.Forms.Padding(2);
            this.tabConfigure.Name = "tabConfigure";
            this.tabConfigure.SelectedIndex = 0;
            this.tabConfigure.Size = new System.Drawing.Size(800, 417);
            this.tabConfigure.TabIndex = 0;
            this.tabConfigure.SelectedIndexChanged += new System.EventHandler(this.tabConfigure_SelectedIndexChanged);
            // 
            // createConfig
            // 
            this.createConfig.Controls.Add(this.btnDeleteConfig);
            this.createConfig.Controls.Add(this.btnUpdateConfig);
            this.createConfig.Controls.Add(this.btnAddConfiguration);
            this.createConfig.Controls.Add(this.btnExportConfig);
            this.createConfig.Controls.Add(this.btnImportConfig);
            this.createConfig.Controls.Add(this.lvConfigs);
            this.createConfig.Controls.Add(this.lblConfig);
            this.createConfig.Location = new System.Drawing.Point(4, 27);
            this.createConfig.Margin = new System.Windows.Forms.Padding(2);
            this.createConfig.Name = "createConfig";
            this.createConfig.Padding = new System.Windows.Forms.Padding(2);
            this.createConfig.Size = new System.Drawing.Size(792, 386);
            this.createConfig.TabIndex = 0;
            this.createConfig.Text = "Manage Configurations";
            this.createConfig.UseVisualStyleBackColor = true;
            // 
            // btnDeleteConfig
            // 
            this.btnDeleteConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteConfig.Location = new System.Drawing.Point(600, 10);
            this.btnDeleteConfig.Margin = new System.Windows.Forms.Padding(2);
            this.btnDeleteConfig.Name = "btnDeleteConfig";
            this.btnDeleteConfig.Size = new System.Drawing.Size(60, 23);
            this.btnDeleteConfig.TabIndex = 4;
            this.btnDeleteConfig.Text = "Delete";
            this.btnDeleteConfig.UseVisualStyleBackColor = true;
            this.btnDeleteConfig.Click += new System.EventHandler(this.btnDeleteConfig_Click);
            // 
            // btnUpdateConfig
            // 
            this.btnUpdateConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdateConfig.Location = new System.Drawing.Point(660, 10);
            this.btnUpdateConfig.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdateConfig.Name = "btnUpdateConfig";
            this.btnUpdateConfig.Size = new System.Drawing.Size(60, 23);
            this.btnUpdateConfig.TabIndex = 3;
            this.btnUpdateConfig.Text = "Edit";
            this.btnUpdateConfig.UseVisualStyleBackColor = true;
            this.btnUpdateConfig.Click += new System.EventHandler(this.btnUpdateConfig_Click);
            // 
            // btnAddConfiguration
            // 
            this.btnAddConfiguration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddConfiguration.Location = new System.Drawing.Point(720, 10);
            this.btnAddConfiguration.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddConfiguration.Name = "btnAddConfiguration";
            this.btnAddConfiguration.Size = new System.Drawing.Size(56, 23);
            this.btnAddConfiguration.TabIndex = 2;
            this.btnAddConfiguration.Text = "Add";
            this.btnAddConfiguration.UseVisualStyleBackColor = true;
            this.btnAddConfiguration.Click += new System.EventHandler(this.btnAddConfiguration_Click);
            // 
            // btnExportConfig
            // 
            this.btnExportConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportConfig.Location = new System.Drawing.Point(540, 10);
            this.btnExportConfig.Margin = new System.Windows.Forms.Padding(2);
            this.btnExportConfig.Name = "btnExportConfig";
            this.btnExportConfig.Size = new System.Drawing.Size(60, 23);
            this.btnExportConfig.TabIndex = 5;
            this.btnExportConfig.Text = "Export";
            this.btnExportConfig.UseVisualStyleBackColor = true;
            this.btnExportConfig.Click += new System.EventHandler(this.btnExportConfig_Click);
            // 
            // btnImportConfig
            // 
            this.btnImportConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImportConfig.Location = new System.Drawing.Point(480, 10);
            this.btnImportConfig.Margin = new System.Windows.Forms.Padding(2);
            this.btnImportConfig.Name = "btnImportConfig";
            this.btnImportConfig.Size = new System.Drawing.Size(60, 23);
            this.btnImportConfig.TabIndex = 6;
            this.btnImportConfig.Text = "Import";
            this.btnImportConfig.UseVisualStyleBackColor = true;
            this.btnImportConfig.Click += new System.EventHandler(this.btnImportConfig_Click);
            // 
            // lvConfigs
            // 
            this.lvConfigs.AllowColumnReorder = true;
            this.lvConfigs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvConfigs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvConfigs.CheckBoxes = true;
            this.lvConfigs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colConfigName,
            this.colConfigLevel,
            this.colLastUpdated,
            this.colConfigComment});
            this.lvConfigs.FullRowSelect = true;
            this.lvConfigs.Location = new System.Drawing.Point(8, 34);
            this.lvConfigs.Margin = new System.Windows.Forms.Padding(2);
            this.lvConfigs.MultiSelect = false;
            this.lvConfigs.Name = "lvConfigs";
            this.lvConfigs.Size = new System.Drawing.Size(778, 313);
            this.lvConfigs.TabIndex = 1;
            this.lvConfigs.UseCompatibleStateImageBehavior = false;
            this.lvConfigs.View = System.Windows.Forms.View.Details;
            this.lvConfigs.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvConfigs_ItemChecked);
            // 
            // colConfigName
            // 
            this.colConfigName.Text = "Configuration Name";
            this.colConfigName.Width = 180;
            // 
            // colConfigLevel
            // 
            this.colConfigLevel.Text = "Level";
            this.colConfigLevel.Width = 100;
            // 
            // colLastUpdated
            // 
            this.colLastUpdated.Text = "Last Updated";
            this.colLastUpdated.Width = 180;
            // 
            // colConfigComment
            // 
            this.colConfigComment.Text = "Comment";
            this.colConfigComment.Width = 500;
            // 
            // lblConfig
            // 
            this.lblConfig.AutoSize = true;
            this.lblConfig.Location = new System.Drawing.Point(0, 11);
            this.lblConfig.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblConfig.Name = "lblConfig";
            this.lblConfig.Size = new System.Drawing.Size(227, 18);
            this.lblConfig.TabIndex = 0;
            this.lblConfig.Text = "Existing Configuration Definitions:";
            // 
            // sourceConfig
            // 
            this.sourceConfig.Controls.Add(this.tabConfigSource);
            this.sourceConfig.Location = new System.Drawing.Point(4, 27);
            this.sourceConfig.Margin = new System.Windows.Forms.Padding(2);
            this.sourceConfig.Name = "sourceConfig";
            this.sourceConfig.Padding = new System.Windows.Forms.Padding(2);
            this.sourceConfig.Size = new System.Drawing.Size(792, 386);
            this.sourceConfig.TabIndex = 1;
            this.sourceConfig.Text = "Define Source";
            this.sourceConfig.UseVisualStyleBackColor = true;
            // 
            // tabConfigSource
            // 
            this.tabConfigSource.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabConfigSource.Controls.Add(this.sourceCases);
            this.tabConfigSource.Controls.Add(this.sourceDocuments);
            this.tabConfigSource.Controls.Add(this.sourceAnnotations);
            this.tabConfigSource.Controls.Add(this.sourceTasks);
            this.tabConfigSource.Location = new System.Drawing.Point(0, 0);
            this.tabConfigSource.Margin = new System.Windows.Forms.Padding(2);
            this.tabConfigSource.Name = "tabConfigSource";
            this.tabConfigSource.SelectedIndex = 0;
            this.tabConfigSource.Size = new System.Drawing.Size(800, 439);
            this.tabConfigSource.TabIndex = 0;
            this.tabConfigSource.SelectedIndexChanged += new System.EventHandler(this.tabConfigSource_SelectedIndexChanged);
            // 
            // sourceCases
            // 
            this.sourceCases.Controls.Add(this.lvSourceCase);
            this.sourceCases.Controls.Add(this.lblSourceCaseConfig);
            this.sourceCases.Controls.Add(this.btnSrcCfgCaseAdd);
            this.sourceCases.Controls.Add(this.btnSrcCfgCaseUpd);
            this.sourceCases.Controls.Add(this.btnSrcCfgCaseDel);
            this.sourceCases.Controls.Add(this.btnSrcCfgCaseExp);
            this.sourceCases.Controls.Add(this.btnSrcCfgCaseImp);
            this.sourceCases.Location = new System.Drawing.Point(4, 27);
            this.sourceCases.Margin = new System.Windows.Forms.Padding(2);
            this.sourceCases.Name = "sourceCases";
            this.sourceCases.Padding = new System.Windows.Forms.Padding(2);
            this.sourceCases.Size = new System.Drawing.Size(792, 408);
            this.sourceCases.TabIndex = 0;
            this.sourceCases.Text = "Case Data Format";
            this.sourceCases.UseVisualStyleBackColor = true;
            // 
            // lvSourceCase
            // 
            this.lvSourceCase.AllowColumnReorder = true;
            this.lvSourceCase.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvSourceCase.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvSourceCase.CheckBoxes = true;
            this.lvSourceCase.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.srcColumn,
            this.srcName,
            this.srcDataType,
            this.srcLength,
            this.srcFormat,
            this.srcMultiValued});
            this.lvSourceCase.FullRowSelect = true;
            this.lvSourceCase.Location = new System.Drawing.Point(0, 34);
            this.lvSourceCase.Margin = new System.Windows.Forms.Padding(2);
            this.lvSourceCase.MultiSelect = false;
            this.lvSourceCase.Name = "lvSourceCase";
            this.lvSourceCase.Size = new System.Drawing.Size(780, 307);
            this.lvSourceCase.TabIndex = 1;
            this.lvSourceCase.UseCompatibleStateImageBehavior = false;
            this.lvSourceCase.View = System.Windows.Forms.View.Details;
            this.lvSourceCase.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvSourceCase_ItemChecked);
            // 
            // srcColumn
            // 
            this.srcColumn.Text = "Column";
            this.srcColumn.Width = 68;
            // 
            // srcName
            // 
            this.srcName.Text = "Field Name";
            this.srcName.Width = 200;
            // 
            // srcDataType
            // 
            this.srcDataType.Text = "Data Type";
            this.srcDataType.Width = 88;
            // 
            // srcLength
            // 
            this.srcLength.Text = "Length";
            this.srcLength.Width = 64;
            // 
            // srcFormat
            // 
            this.srcFormat.Text = "Format";
            this.srcFormat.Width = 256;
            // 
            // srcMultiValued
            // 
            this.srcMultiValued.Text = "MultiValued";
            this.srcMultiValued.Width = 320;
            // 
            // lblSourceCaseConfig
            // 
            this.lblSourceCaseConfig.AutoSize = true;
            this.lblSourceCaseConfig.Location = new System.Drawing.Point(0, 11);
            this.lblSourceCaseConfig.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSourceCaseConfig.Name = "lblSourceCaseConfig";
            this.lblSourceCaseConfig.Size = new System.Drawing.Size(99, 18);
            this.lblSourceCaseConfig.TabIndex = 0;
            this.lblSourceCaseConfig.Text = "Configuration:";
            // 
            // btnSrcCfgCaseAdd
            // 
            this.btnSrcCfgCaseAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgCaseAdd.Location = new System.Drawing.Point(720, 11);
            this.btnSrcCfgCaseAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgCaseAdd.Name = "btnSrcCfgCaseAdd";
            this.btnSrcCfgCaseAdd.Size = new System.Drawing.Size(56, 23);
            this.btnSrcCfgCaseAdd.TabIndex = 2;
            this.btnSrcCfgCaseAdd.Text = "Add";
            this.btnSrcCfgCaseAdd.UseVisualStyleBackColor = true;
            this.btnSrcCfgCaseAdd.Click += new System.EventHandler(this.btnSrcCfgCaseAdd_Click);
            // 
            // btnSrcCfgCaseUpd
            // 
            this.btnSrcCfgCaseUpd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgCaseUpd.Location = new System.Drawing.Point(660, 11);
            this.btnSrcCfgCaseUpd.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgCaseUpd.Name = "btnSrcCfgCaseUpd";
            this.btnSrcCfgCaseUpd.Size = new System.Drawing.Size(60, 23);
            this.btnSrcCfgCaseUpd.TabIndex = 3;
            this.btnSrcCfgCaseUpd.Text = "Edit";
            this.btnSrcCfgCaseUpd.UseVisualStyleBackColor = true;
            this.btnSrcCfgCaseUpd.Click += new System.EventHandler(this.btnSrcCfgCaseUpd_Click);
            // 
            // btnSrcCfgCaseDel
            // 
            this.btnSrcCfgCaseDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgCaseDel.Location = new System.Drawing.Point(600, 11);
            this.btnSrcCfgCaseDel.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgCaseDel.Name = "btnSrcCfgCaseDel";
            this.btnSrcCfgCaseDel.Size = new System.Drawing.Size(60, 23);
            this.btnSrcCfgCaseDel.TabIndex = 4;
            this.btnSrcCfgCaseDel.Text = "Delete";
            this.btnSrcCfgCaseDel.UseVisualStyleBackColor = true;
            this.btnSrcCfgCaseDel.Click += new System.EventHandler(this.btnSrcCfgCaseDel_Click);
            // 
            // btnSrcCfgCaseExp
            // 
            this.btnSrcCfgCaseExp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgCaseExp.Location = new System.Drawing.Point(540, 11);
            this.btnSrcCfgCaseExp.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgCaseExp.Name = "btnSrcCfgCaseExp";
            this.btnSrcCfgCaseExp.Size = new System.Drawing.Size(60, 23);
            this.btnSrcCfgCaseExp.TabIndex = 5;
            this.btnSrcCfgCaseExp.Text = "Export";
            this.btnSrcCfgCaseExp.UseVisualStyleBackColor = true;
            this.btnSrcCfgCaseExp.Click += new System.EventHandler(this.btnSrcCfgCaseExp_Click);
            // 
            // btnSrcCfgCaseImp
            // 
            this.btnSrcCfgCaseImp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgCaseImp.Location = new System.Drawing.Point(480, 11);
            this.btnSrcCfgCaseImp.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgCaseImp.Name = "btnSrcCfgCaseImp";
            this.btnSrcCfgCaseImp.Size = new System.Drawing.Size(60, 23);
            this.btnSrcCfgCaseImp.TabIndex = 6;
            this.btnSrcCfgCaseImp.Text = "Import";
            this.btnSrcCfgCaseImp.UseVisualStyleBackColor = true;
            this.btnSrcCfgCaseImp.Click += new System.EventHandler(this.btnSrcCfgCaseImp_Click);
            // 
            // sourceDocuments
            // 
            this.sourceDocuments.Controls.Add(this.lvSourceDocument);
            this.sourceDocuments.Controls.Add(this.lblSourceDocConfig);
            this.sourceDocuments.Controls.Add(this.btnSrcCfgDocAdd);
            this.sourceDocuments.Controls.Add(this.btnSrcCfgDocUpd);
            this.sourceDocuments.Controls.Add(this.btnSrcCfgDocDel);
            this.sourceDocuments.Controls.Add(this.btnSrcCfgDocExp);
            this.sourceDocuments.Controls.Add(this.btnSrcCfgDocImp);
            this.sourceDocuments.Location = new System.Drawing.Point(4, 27);
            this.sourceDocuments.Margin = new System.Windows.Forms.Padding(2);
            this.sourceDocuments.Name = "sourceDocuments";
            this.sourceDocuments.Padding = new System.Windows.Forms.Padding(2);
            this.sourceDocuments.Size = new System.Drawing.Size(792, 408);
            this.sourceDocuments.TabIndex = 1;
            this.sourceDocuments.Text = "Document Data Format";
            this.sourceDocuments.UseVisualStyleBackColor = true;
            // 
            // lvSourceDocument
            // 
            this.lvSourceDocument.AllowColumnReorder = true;
            this.lvSourceDocument.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvSourceDocument.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvSourceDocument.CheckBoxes = true;
            this.lvSourceDocument.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colSrcDocColum,
            this.colSrcDocName,
            this.colSrcDocDataType,
            this.colSrcDocLength,
            this.colSrcDocFormat,
            this.colSrcDocMultiValued});
            this.lvSourceDocument.FullRowSelect = true;
            this.lvSourceDocument.Location = new System.Drawing.Point(0, 34);
            this.lvSourceDocument.Margin = new System.Windows.Forms.Padding(2);
            this.lvSourceDocument.Name = "lvSourceDocument";
            this.lvSourceDocument.Size = new System.Drawing.Size(780, 302);
            this.lvSourceDocument.TabIndex = 1;
            this.lvSourceDocument.UseCompatibleStateImageBehavior = false;
            this.lvSourceDocument.View = System.Windows.Forms.View.Details;
            this.lvSourceDocument.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvSourceDocument_ItemChecked);
            // 
            // colSrcDocColum
            // 
            this.colSrcDocColum.Text = "Column";
            this.colSrcDocColum.Width = 68;
            // 
            // colSrcDocName
            // 
            this.colSrcDocName.Text = "Field Name";
            this.colSrcDocName.Width = 200;
            // 
            // colSrcDocDataType
            // 
            this.colSrcDocDataType.Text = "Data Type";
            this.colSrcDocDataType.Width = 88;
            // 
            // colSrcDocLength
            // 
            this.colSrcDocLength.Text = "Length";
            this.colSrcDocLength.Width = 64;
            // 
            // colSrcDocFormat
            // 
            this.colSrcDocFormat.Text = "Format";
            this.colSrcDocFormat.Width = 256;
            // 
            // colSrcDocMultiValued
            // 
            this.colSrcDocMultiValued.Text = "MultiValued";
            this.colSrcDocMultiValued.Width = 320;
            // 
            // lblSourceDocConfig
            // 
            this.lblSourceDocConfig.AutoSize = true;
            this.lblSourceDocConfig.Location = new System.Drawing.Point(0, 11);
            this.lblSourceDocConfig.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSourceDocConfig.Name = "lblSourceDocConfig";
            this.lblSourceDocConfig.Size = new System.Drawing.Size(99, 18);
            this.lblSourceDocConfig.TabIndex = 0;
            this.lblSourceDocConfig.Text = "Configuration:";
            // 
            // btnSrcCfgDocAdd
            // 
            this.btnSrcCfgDocAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgDocAdd.Location = new System.Drawing.Point(720, 11);
            this.btnSrcCfgDocAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgDocAdd.Name = "btnSrcCfgDocAdd";
            this.btnSrcCfgDocAdd.Size = new System.Drawing.Size(56, 23);
            this.btnSrcCfgDocAdd.TabIndex = 2;
            this.btnSrcCfgDocAdd.Text = "Add";
            this.btnSrcCfgDocAdd.UseVisualStyleBackColor = true;
            this.btnSrcCfgDocAdd.Click += new System.EventHandler(this.btnSrcCfgDocAdd_Click);
            // 
            // btnSrcCfgDocUpd
            // 
            this.btnSrcCfgDocUpd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgDocUpd.Location = new System.Drawing.Point(660, 11);
            this.btnSrcCfgDocUpd.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgDocUpd.Name = "btnSrcCfgDocUpd";
            this.btnSrcCfgDocUpd.Size = new System.Drawing.Size(60, 23);
            this.btnSrcCfgDocUpd.TabIndex = 3;
            this.btnSrcCfgDocUpd.Text = "Edit";
            this.btnSrcCfgDocUpd.UseVisualStyleBackColor = true;
            this.btnSrcCfgDocUpd.Click += new System.EventHandler(this.btnSrcCfgDocUpd_Click);
            // 
            // btnSrcCfgDocDel
            // 
            this.btnSrcCfgDocDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgDocDel.Location = new System.Drawing.Point(600, 11);
            this.btnSrcCfgDocDel.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgDocDel.Name = "btnSrcCfgDocDel";
            this.btnSrcCfgDocDel.Size = new System.Drawing.Size(60, 23);
            this.btnSrcCfgDocDel.TabIndex = 4;
            this.btnSrcCfgDocDel.Text = "Delete";
            this.btnSrcCfgDocDel.UseVisualStyleBackColor = true;
            this.btnSrcCfgDocDel.Click += new System.EventHandler(this.btnSrcCfgDocDel_Click);
            // 
            // btnSrcCfgDocExp
            // 
            this.btnSrcCfgDocExp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgDocExp.Location = new System.Drawing.Point(540, 11);
            this.btnSrcCfgDocExp.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgDocExp.Name = "btnSrcCfgDocExp";
            this.btnSrcCfgDocExp.Size = new System.Drawing.Size(60, 23);
            this.btnSrcCfgDocExp.TabIndex = 5;
            this.btnSrcCfgDocExp.Text = "Export";
            this.btnSrcCfgDocExp.UseVisualStyleBackColor = true;
            this.btnSrcCfgDocExp.Click += new System.EventHandler(this.btnSrcCfgDocExp_Click);
            // 
            // btnSrcCfgDocImp
            // 
            this.btnSrcCfgDocImp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgDocImp.Location = new System.Drawing.Point(480, 11);
            this.btnSrcCfgDocImp.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgDocImp.Name = "btnSrcCfgDocImp";
            this.btnSrcCfgDocImp.Size = new System.Drawing.Size(60, 23);
            this.btnSrcCfgDocImp.TabIndex = 6;
            this.btnSrcCfgDocImp.Text = "Import";
            this.btnSrcCfgDocImp.UseVisualStyleBackColor = true;
            this.btnSrcCfgDocImp.Click += new System.EventHandler(this.btnSrcCfgDocImp_Click);
            // 
            // sourceAnnotations
            // 
            this.sourceAnnotations.Controls.Add(this.lvSourceAnnotation);
            this.sourceAnnotations.Controls.Add(this.lblSourceAnnConfig);
            this.sourceAnnotations.Controls.Add(this.btnSrcCfgAnnAdd);
            this.sourceAnnotations.Controls.Add(this.btnSrcCfgAnnUpd);
            this.sourceAnnotations.Controls.Add(this.btnSrcCfgAnnDel);
            this.sourceAnnotations.Controls.Add(this.btnSrcCfgAnnExp);
            this.sourceAnnotations.Controls.Add(this.btnSrcCfgAnnImp);
            this.sourceAnnotations.Location = new System.Drawing.Point(4, 27);
            this.sourceAnnotations.Margin = new System.Windows.Forms.Padding(2);
            this.sourceAnnotations.Name = "sourceAnnotations";
            this.sourceAnnotations.Size = new System.Drawing.Size(792, 408);
            this.sourceAnnotations.TabIndex = 2;
            this.sourceAnnotations.Text = "Annotation Data Format";
            this.sourceAnnotations.UseVisualStyleBackColor = true;
            // 
            // lvSourceAnnotation
            // 
            this.lvSourceAnnotation.AllowColumnReorder = true;
            this.lvSourceAnnotation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvSourceAnnotation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvSourceAnnotation.CheckBoxes = true;
            this.lvSourceAnnotation.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colSrcAnnColumn,
            this.colSrcAnnName,
            this.colSrcAnnDataType,
            this.colSrcAnnLength,
            this.colSrcAnnFormat,
            this.colSrcAnnMultiValued});
            this.lvSourceAnnotation.Location = new System.Drawing.Point(0, 34);
            this.lvSourceAnnotation.Margin = new System.Windows.Forms.Padding(2);
            this.lvSourceAnnotation.MultiSelect = false;
            this.lvSourceAnnotation.Name = "lvSourceAnnotation";
            this.lvSourceAnnotation.Size = new System.Drawing.Size(780, 302);
            this.lvSourceAnnotation.TabIndex = 1;
            this.lvSourceAnnotation.UseCompatibleStateImageBehavior = false;
            this.lvSourceAnnotation.View = System.Windows.Forms.View.Details;
            this.lvSourceAnnotation.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvSourceAnnotation_ItemChecked);
            // 
            // colSrcAnnColumn
            // 
            this.colSrcAnnColumn.Text = "Column";
            this.colSrcAnnColumn.Width = 68;
            // 
            // colSrcAnnName
            // 
            this.colSrcAnnName.Text = "Field Name";
            this.colSrcAnnName.Width = 200;
            // 
            // colSrcAnnDataType
            // 
            this.colSrcAnnDataType.Text = "Data Type";
            this.colSrcAnnDataType.Width = 88;
            // 
            // colSrcAnnLength
            // 
            this.colSrcAnnLength.Text = "Length";
            this.colSrcAnnLength.Width = 64;
            // 
            // colSrcAnnFormat
            // 
            this.colSrcAnnFormat.Text = "Format";
            this.colSrcAnnFormat.Width = 256;
            // 
            // colSrcAnnMultiValued
            // 
            this.colSrcAnnMultiValued.Text = "MultiValued";
            this.colSrcAnnMultiValued.Width = 320;
            // 
            // lblSourceAnnConfig
            // 
            this.lblSourceAnnConfig.AutoSize = true;
            this.lblSourceAnnConfig.Location = new System.Drawing.Point(0, 11);
            this.lblSourceAnnConfig.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSourceAnnConfig.Name = "lblSourceAnnConfig";
            this.lblSourceAnnConfig.Size = new System.Drawing.Size(99, 18);
            this.lblSourceAnnConfig.TabIndex = 0;
            this.lblSourceAnnConfig.Text = "Configuration:";
            // 
            // btnSrcCfgAnnAdd
            // 
            this.btnSrcCfgAnnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgAnnAdd.Location = new System.Drawing.Point(720, 11);
            this.btnSrcCfgAnnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgAnnAdd.Name = "btnSrcCfgAnnAdd";
            this.btnSrcCfgAnnAdd.Size = new System.Drawing.Size(56, 23);
            this.btnSrcCfgAnnAdd.TabIndex = 2;
            this.btnSrcCfgAnnAdd.Text = "Add";
            this.btnSrcCfgAnnAdd.UseVisualStyleBackColor = true;
            this.btnSrcCfgAnnAdd.Click += new System.EventHandler(this.btnSrcCfgAnnAdd_Click);
            // 
            // btnSrcCfgAnnUpd
            // 
            this.btnSrcCfgAnnUpd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgAnnUpd.Location = new System.Drawing.Point(660, 11);
            this.btnSrcCfgAnnUpd.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgAnnUpd.Name = "btnSrcCfgAnnUpd";
            this.btnSrcCfgAnnUpd.Size = new System.Drawing.Size(60, 23);
            this.btnSrcCfgAnnUpd.TabIndex = 3;
            this.btnSrcCfgAnnUpd.Text = "Edit";
            this.btnSrcCfgAnnUpd.UseVisualStyleBackColor = true;
            this.btnSrcCfgAnnUpd.Click += new System.EventHandler(this.btnSrcCfgAnnUpd_Click);
            // 
            // btnSrcCfgAnnDel
            // 
            this.btnSrcCfgAnnDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgAnnDel.Location = new System.Drawing.Point(600, 11);
            this.btnSrcCfgAnnDel.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgAnnDel.Name = "btnSrcCfgAnnDel";
            this.btnSrcCfgAnnDel.Size = new System.Drawing.Size(60, 23);
            this.btnSrcCfgAnnDel.TabIndex = 4;
            this.btnSrcCfgAnnDel.Text = "Delete";
            this.btnSrcCfgAnnDel.UseVisualStyleBackColor = true;
            this.btnSrcCfgAnnDel.Click += new System.EventHandler(this.btnSrcCfgAnnDel_Click);
            // 
            // btnSrcCfgAnnExp
            // 
            this.btnSrcCfgAnnExp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgAnnExp.Location = new System.Drawing.Point(540, 11);
            this.btnSrcCfgAnnExp.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgAnnExp.Name = "btnSrcCfgAnnExp";
            this.btnSrcCfgAnnExp.Size = new System.Drawing.Size(60, 23);
            this.btnSrcCfgAnnExp.TabIndex = 5;
            this.btnSrcCfgAnnExp.Text = "Export";
            this.btnSrcCfgAnnExp.UseVisualStyleBackColor = true;
            this.btnSrcCfgAnnExp.Click += new System.EventHandler(this.btnSrcCfgAnnExp_Click);
            // 
            // btnSrcCfgAnnImp
            // 
            this.btnSrcCfgAnnImp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgAnnImp.Location = new System.Drawing.Point(480, 11);
            this.btnSrcCfgAnnImp.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgAnnImp.Name = "btnSrcCfgAnnImp";
            this.btnSrcCfgAnnImp.Size = new System.Drawing.Size(60, 23);
            this.btnSrcCfgAnnImp.TabIndex = 6;
            this.btnSrcCfgAnnImp.Text = "Import";
            this.btnSrcCfgAnnImp.UseVisualStyleBackColor = true;
            this.btnSrcCfgAnnImp.Click += new System.EventHandler(this.btnSrcCfgAnnImp_Click);
            // 
            // sourceTasks
            // 
            this.sourceTasks.Controls.Add(this.lvSourceTask);
            this.sourceTasks.Controls.Add(this.lblSourceTaskConfig);
            this.sourceTasks.Controls.Add(this.btnSrcCfgTaskAdd);
            this.sourceTasks.Controls.Add(this.btnSrcCfgTaskUpd);
            this.sourceTasks.Controls.Add(this.btnSrcCfgTaskDel);
            this.sourceTasks.Controls.Add(this.btnSrcCfgTaskExp);
            this.sourceTasks.Controls.Add(this.btnSrcCfgTaskImp);
            this.sourceTasks.Location = new System.Drawing.Point(4, 27);
            this.sourceTasks.Margin = new System.Windows.Forms.Padding(2);
            this.sourceTasks.Name = "sourceTasks";
            this.sourceTasks.Size = new System.Drawing.Size(792, 408);
            this.sourceTasks.TabIndex = 3;
            this.sourceTasks.Text = "Task Data Format";
            this.sourceTasks.UseVisualStyleBackColor = true;
            // 
            // lvSourceTask
            // 
            this.lvSourceTask.AllowColumnReorder = true;
            this.lvSourceTask.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvSourceTask.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvSourceTask.CheckBoxes = true;
            this.lvSourceTask.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colSrcTaskColumn,
            this.colSrcTaskName,
            this.colSrcTaskDataType,
            this.colSrcTaskLength,
            this.colSrcTaskFormat,
            this.colSrcTaskMultiValued});
            this.lvSourceTask.Location = new System.Drawing.Point(0, 34);
            this.lvSourceTask.Margin = new System.Windows.Forms.Padding(2);
            this.lvSourceTask.Name = "lvSourceTask";
            this.lvSourceTask.Size = new System.Drawing.Size(780, 302);
            this.lvSourceTask.TabIndex = 1;
            this.lvSourceTask.UseCompatibleStateImageBehavior = false;
            this.lvSourceTask.View = System.Windows.Forms.View.Details;
            this.lvSourceTask.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvSourceTask_ItemChecked);
            // 
            // colSrcTaskColumn
            // 
            this.colSrcTaskColumn.Text = "Column";
            this.colSrcTaskColumn.Width = 68;
            // 
            // colSrcTaskName
            // 
            this.colSrcTaskName.Text = "Field Name";
            this.colSrcTaskName.Width = 200;
            // 
            // colSrcTaskDataType
            // 
            this.colSrcTaskDataType.Text = "Data Type";
            this.colSrcTaskDataType.Width = 88;
            // 
            // colSrcTaskLength
            // 
            this.colSrcTaskLength.Text = "Length";
            this.colSrcTaskLength.Width = 64;
            // 
            // colSrcTaskFormat
            // 
            this.colSrcTaskFormat.Text = "Format";
            this.colSrcTaskFormat.Width = 256;
            // 
            // colSrcTaskMultiValued
            // 
            this.colSrcTaskMultiValued.Text = "MultiValued";
            this.colSrcTaskMultiValued.Width = 320;
            // 
            // lblSourceTaskConfig
            // 
            this.lblSourceTaskConfig.AutoSize = true;
            this.lblSourceTaskConfig.Location = new System.Drawing.Point(0, 11);
            this.lblSourceTaskConfig.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSourceTaskConfig.Name = "lblSourceTaskConfig";
            this.lblSourceTaskConfig.Size = new System.Drawing.Size(99, 18);
            this.lblSourceTaskConfig.TabIndex = 0;
            this.lblSourceTaskConfig.Text = "Configuration:";
            // 
            // btnSrcCfgTaskAdd
            // 
            this.btnSrcCfgTaskAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgTaskAdd.Location = new System.Drawing.Point(720, 11);
            this.btnSrcCfgTaskAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgTaskAdd.Name = "btnSrcCfgTaskAdd";
            this.btnSrcCfgTaskAdd.Size = new System.Drawing.Size(56, 23);
            this.btnSrcCfgTaskAdd.TabIndex = 2;
            this.btnSrcCfgTaskAdd.Text = "Add";
            this.btnSrcCfgTaskAdd.UseVisualStyleBackColor = true;
            this.btnSrcCfgTaskAdd.Click += new System.EventHandler(this.btnSrcCfgTaskAdd_Click);
            // 
            // btnSrcCfgTaskUpd
            // 
            this.btnSrcCfgTaskUpd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgTaskUpd.Location = new System.Drawing.Point(660, 11);
            this.btnSrcCfgTaskUpd.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgTaskUpd.Name = "btnSrcCfgTaskUpd";
            this.btnSrcCfgTaskUpd.Size = new System.Drawing.Size(60, 23);
            this.btnSrcCfgTaskUpd.TabIndex = 3;
            this.btnSrcCfgTaskUpd.Text = "Edit";
            this.btnSrcCfgTaskUpd.UseVisualStyleBackColor = true;
            this.btnSrcCfgTaskUpd.Click += new System.EventHandler(this.btnSrcCfgTaskUpd_Click);
            // 
            // btnSrcCfgTaskDel
            // 
            this.btnSrcCfgTaskDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgTaskDel.Location = new System.Drawing.Point(600, 11);
            this.btnSrcCfgTaskDel.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgTaskDel.Name = "btnSrcCfgTaskDel";
            this.btnSrcCfgTaskDel.Size = new System.Drawing.Size(60, 23);
            this.btnSrcCfgTaskDel.TabIndex = 4;
            this.btnSrcCfgTaskDel.Text = "Delete";
            this.btnSrcCfgTaskDel.UseVisualStyleBackColor = true;
            this.btnSrcCfgTaskDel.Click += new System.EventHandler(this.btnSrcCfgTaskDel_Click);
            // 
            // btnSrcCfgTaskExp
            // 
            this.btnSrcCfgTaskExp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgTaskExp.Location = new System.Drawing.Point(540, 11);
            this.btnSrcCfgTaskExp.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgTaskExp.Name = "btnSrcCfgTaskExp";
            this.btnSrcCfgTaskExp.Size = new System.Drawing.Size(60, 23);
            this.btnSrcCfgTaskExp.TabIndex = 5;
            this.btnSrcCfgTaskExp.Text = "Export";
            this.btnSrcCfgTaskExp.UseVisualStyleBackColor = true;
            this.btnSrcCfgTaskExp.Click += new System.EventHandler(this.btnSrcCfgTaskExp_Click);
            // 
            // btnSrcCfgTaskImp
            // 
            this.btnSrcCfgTaskImp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSrcCfgTaskImp.Location = new System.Drawing.Point(480, 11);
            this.btnSrcCfgTaskImp.Margin = new System.Windows.Forms.Padding(2);
            this.btnSrcCfgTaskImp.Name = "btnSrcCfgTaskImp";
            this.btnSrcCfgTaskImp.Size = new System.Drawing.Size(60, 23);
            this.btnSrcCfgTaskImp.TabIndex = 6;
            this.btnSrcCfgTaskImp.Text = "Import";
            this.btnSrcCfgTaskImp.UseVisualStyleBackColor = true;
            this.btnSrcCfgTaskImp.Click += new System.EventHandler(this.btnSrcCfgTaskImp_Click);
            // 
            // targetConfig
            // 
            this.targetConfig.Controls.Add(this.tabConfigTarget);
            this.targetConfig.Location = new System.Drawing.Point(4, 27);
            this.targetConfig.Margin = new System.Windows.Forms.Padding(2);
            this.targetConfig.Name = "targetConfig";
            this.targetConfig.Size = new System.Drawing.Size(792, 386);
            this.targetConfig.TabIndex = 2;
            this.targetConfig.Text = "Define Target";
            this.targetConfig.UseVisualStyleBackColor = true;
            // 
            // tabConfigTarget
            // 
            this.tabConfigTarget.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabConfigTarget.Controls.Add(this.targetCases);
            this.tabConfigTarget.Controls.Add(this.targetDocuments);
            this.tabConfigTarget.Controls.Add(this.targetAnnotations);
            this.tabConfigTarget.Controls.Add(this.targetTasks);
            this.tabConfigTarget.Location = new System.Drawing.Point(0, 0);
            this.tabConfigTarget.Margin = new System.Windows.Forms.Padding(2);
            this.tabConfigTarget.Name = "tabConfigTarget";
            this.tabConfigTarget.SelectedIndex = 0;
            this.tabConfigTarget.Size = new System.Drawing.Size(791, 452);
            this.tabConfigTarget.TabIndex = 0;
            this.tabConfigTarget.SelectedIndexChanged += new System.EventHandler(this.tabConfigTarget_SelectedIndexChanged);
            // 
            // targetCases
            // 
            this.targetCases.Controls.Add(this.lvTargetCaseFolders);
            this.targetCases.Controls.Add(this.lblTargetCaseFolders);
            this.targetCases.Controls.Add(this.lvTargetCaseProperties);
            this.targetCases.Controls.Add(this.lblTargetCaseProperties);
            this.targetCases.Controls.Add(this.lvTargetCaseTypes);
            this.targetCases.Controls.Add(this.lblTargetCaseConfig);
            this.targetCases.Controls.Add(this.btnTgtCfgCaseAdd);
            this.targetCases.Controls.Add(this.btnTgtCfgCaseUpd);
            this.targetCases.Controls.Add(this.btnTgtCfgCaseDel);
            this.targetCases.Controls.Add(this.btnTgtCfgCaseExp);
            this.targetCases.Controls.Add(this.btnTgtCfgCaseImp);
            this.targetCases.Location = new System.Drawing.Point(4, 27);
            this.targetCases.Margin = new System.Windows.Forms.Padding(2);
            this.targetCases.Name = "targetCases";
            this.targetCases.Padding = new System.Windows.Forms.Padding(2);
            this.targetCases.Size = new System.Drawing.Size(783, 421);
            this.targetCases.TabIndex = 1;
            this.targetCases.Text = "Case Definitions";
            this.targetCases.UseVisualStyleBackColor = true;
            // 
            // lvTargetCaseFolders
            // 
            this.lvTargetCaseFolders.AllowColumnReorder = true;
            this.lvTargetCaseFolders.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTargetCaseFolders.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvTargetCaseFolders.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTgtCaseFolderName,
            this.colTgtCaseFolderParent});
            this.lvTargetCaseFolders.FullRowSelect = true;
            this.lvTargetCaseFolders.Location = new System.Drawing.Point(4, 295);
            this.lvTargetCaseFolders.Margin = new System.Windows.Forms.Padding(2);
            this.lvTargetCaseFolders.MultiSelect = false;
            this.lvTargetCaseFolders.Name = "lvTargetCaseFolders";
            this.lvTargetCaseFolders.Size = new System.Drawing.Size(776, 69);
            this.lvTargetCaseFolders.TabIndex = 6;
            this.lvTargetCaseFolders.UseCompatibleStateImageBehavior = false;
            this.lvTargetCaseFolders.View = System.Windows.Forms.View.Details;
            // 
            // colTgtCaseFolderName
            // 
            this.colTgtCaseFolderName.Text = "Folder Name";
            this.colTgtCaseFolderName.Width = 300;
            // 
            // colTgtCaseFolderParent
            // 
            this.colTgtCaseFolderParent.Text = "Parent";
            this.colTgtCaseFolderParent.Width = 730;
            // 
            // lblTargetCaseFolders
            // 
            this.lblTargetCaseFolders.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTargetCaseFolders.AutoSize = true;
            this.lblTargetCaseFolders.Location = new System.Drawing.Point(5, 275);
            this.lblTargetCaseFolders.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTargetCaseFolders.Name = "lblTargetCaseFolders";
            this.lblTargetCaseFolders.Size = new System.Drawing.Size(91, 18);
            this.lblTargetCaseFolders.TabIndex = 5;
            this.lblTargetCaseFolders.Text = "Case Folders:";
            // 
            // lvTargetCaseProperties
            // 
            this.lvTargetCaseProperties.AllowColumnReorder = true;
            this.lvTargetCaseProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTargetCaseProperties.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvTargetCaseProperties.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colCasePropName,
            this.colCasePropSource,
            this.colCasePropType,
            this.colCasePropReq,
            this.colCasePropMapped,
            this.colCasePropComputed,
            this.colCasePropMapName});
            this.lvTargetCaseProperties.FullRowSelect = true;
            this.lvTargetCaseProperties.Location = new System.Drawing.Point(8, 135);
            this.lvTargetCaseProperties.Margin = new System.Windows.Forms.Padding(2);
            this.lvTargetCaseProperties.Name = "lvTargetCaseProperties";
            this.lvTargetCaseProperties.Size = new System.Drawing.Size(773, 138);
            this.lvTargetCaseProperties.TabIndex = 4;
            this.lvTargetCaseProperties.UseCompatibleStateImageBehavior = false;
            this.lvTargetCaseProperties.View = System.Windows.Forms.View.Details;
            // 
            // colCasePropName
            // 
            this.colCasePropName.Text = "Name";
            this.colCasePropName.Width = 210;
            // 
            // colCasePropSource
            // 
            this.colCasePropSource.Text = "Source";
            this.colCasePropSource.Width = 170;
            // 
            // colCasePropType
            // 
            this.colCasePropType.Text = "Type";
            this.colCasePropType.Width = 150;
            // 
            // colCasePropReq
            // 
            this.colCasePropReq.Text = "Required";
            this.colCasePropReq.Width = 80;
            // 
            // colCasePropMapped
            // 
            this.colCasePropMapped.Text = "Mapped";
            this.colCasePropMapped.Width = 80;
            // 
            // colCasePropComputed
            // 
            this.colCasePropComputed.Text = "Computed";
            this.colCasePropComputed.Width = 100;
            // 
            // colCasePropMapName
            // 
            this.colCasePropMapName.Text = "Mapping Name";
            this.colCasePropMapName.Width = 217;
            // 
            // lblTargetCaseProperties
            // 
            this.lblTargetCaseProperties.AutoSize = true;
            this.lblTargetCaseProperties.Location = new System.Drawing.Point(4, 113);
            this.lblTargetCaseProperties.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTargetCaseProperties.Name = "lblTargetCaseProperties";
            this.lblTargetCaseProperties.Size = new System.Drawing.Size(111, 18);
            this.lblTargetCaseProperties.TabIndex = 3;
            this.lblTargetCaseProperties.Text = "Case Properties:";
            // 
            // lvTargetCaseTypes
            // 
            this.lvTargetCaseTypes.AllowColumnReorder = true;
            this.lvTargetCaseTypes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTargetCaseTypes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvTargetCaseTypes.CheckBoxes = true;
            this.lvTargetCaseTypes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTargetCaseType,
            this.colDefaultDocClass});
            this.lvTargetCaseTypes.FullRowSelect = true;
            this.lvTargetCaseTypes.Location = new System.Drawing.Point(0, 34);
            this.lvTargetCaseTypes.Margin = new System.Windows.Forms.Padding(2);
            this.lvTargetCaseTypes.MultiSelect = false;
            this.lvTargetCaseTypes.Name = "lvTargetCaseTypes";
            this.lvTargetCaseTypes.Size = new System.Drawing.Size(776, 92);
            this.lvTargetCaseTypes.TabIndex = 2;
            this.lvTargetCaseTypes.UseCompatibleStateImageBehavior = false;
            this.lvTargetCaseTypes.View = System.Windows.Forms.View.Details;
            this.lvTargetCaseTypes.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvTargetCaseTypes_ItemChecked);
            // 
            // colTargetCaseType
            // 
            this.colTargetCaseType.Text = "Case Types";
            this.colTargetCaseType.Width = 200;
            // 
            // colDefaultDocClass
            // 
            this.colDefaultDocClass.Text = "Default Document Class";
            this.colDefaultDocClass.Width = 800;
            // 
            // lblTargetCaseConfig
            // 
            this.lblTargetCaseConfig.AutoSize = true;
            this.lblTargetCaseConfig.Location = new System.Drawing.Point(0, 11);
            this.lblTargetCaseConfig.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTargetCaseConfig.Name = "lblTargetCaseConfig";
            this.lblTargetCaseConfig.Size = new System.Drawing.Size(99, 18);
            this.lblTargetCaseConfig.TabIndex = 0;
            this.lblTargetCaseConfig.Text = "Configuration:";
            // 
            // btnTgtCfgCaseAdd
            // 
            this.btnTgtCfgCaseAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgCaseAdd.Location = new System.Drawing.Point(720, 11);
            this.btnTgtCfgCaseAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgCaseAdd.Name = "btnTgtCfgCaseAdd";
            this.btnTgtCfgCaseAdd.Size = new System.Drawing.Size(56, 23);
            this.btnTgtCfgCaseAdd.TabIndex = 2;
            this.btnTgtCfgCaseAdd.Text = "Add";
            this.btnTgtCfgCaseAdd.UseVisualStyleBackColor = true;
            this.btnTgtCfgCaseAdd.Click += new System.EventHandler(this.btnTgtCfgCaseAdd_Click);
            // 
            // btnTgtCfgCaseUpd
            // 
            this.btnTgtCfgCaseUpd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgCaseUpd.Location = new System.Drawing.Point(660, 11);
            this.btnTgtCfgCaseUpd.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgCaseUpd.Name = "btnTgtCfgCaseUpd";
            this.btnTgtCfgCaseUpd.Size = new System.Drawing.Size(60, 23);
            this.btnTgtCfgCaseUpd.TabIndex = 3;
            this.btnTgtCfgCaseUpd.Text = "Edit";
            this.btnTgtCfgCaseUpd.UseVisualStyleBackColor = true;
            this.btnTgtCfgCaseUpd.Click += new System.EventHandler(this.btnTgtCfgCaseUpd_Click);
            // 
            // btnTgtCfgCaseDel
            // 
            this.btnTgtCfgCaseDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgCaseDel.Location = new System.Drawing.Point(600, 11);
            this.btnTgtCfgCaseDel.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgCaseDel.Name = "btnTgtCfgCaseDel";
            this.btnTgtCfgCaseDel.Size = new System.Drawing.Size(60, 23);
            this.btnTgtCfgCaseDel.TabIndex = 4;
            this.btnTgtCfgCaseDel.Text = "Delete";
            this.btnTgtCfgCaseDel.UseVisualStyleBackColor = true;
            this.btnTgtCfgCaseDel.Click += new System.EventHandler(this.btnTgtCfgCaseDel_Click);
            // 
            // btnTgtCfgCaseExp
            // 
            this.btnTgtCfgCaseExp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgCaseExp.Location = new System.Drawing.Point(540, 11);
            this.btnTgtCfgCaseExp.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgCaseExp.Name = "btnTgtCfgCaseExp";
            this.btnTgtCfgCaseExp.Size = new System.Drawing.Size(60, 23);
            this.btnTgtCfgCaseExp.TabIndex = 5;
            this.btnTgtCfgCaseExp.Text = "Export";
            this.btnTgtCfgCaseExp.UseVisualStyleBackColor = true;
            this.btnTgtCfgCaseExp.Click += new System.EventHandler(this.btnTgtCfgCaseExp_Click);
            // 
            // btnTgtCfgCaseImp
            // 
            this.btnTgtCfgCaseImp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgCaseImp.Location = new System.Drawing.Point(480, 11);
            this.btnTgtCfgCaseImp.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgCaseImp.Name = "btnTgtCfgCaseImp";
            this.btnTgtCfgCaseImp.Size = new System.Drawing.Size(60, 23);
            this.btnTgtCfgCaseImp.TabIndex = 6;
            this.btnTgtCfgCaseImp.Text = "Import";
            this.btnTgtCfgCaseImp.UseVisualStyleBackColor = true;
            this.btnTgtCfgCaseImp.Click += new System.EventHandler(this.btnTgtCfgCaseImp_Click);
            // 
            // targetDocuments
            // 
            this.targetDocuments.Controls.Add(this.lvTargetDocProperties);
            this.targetDocuments.Controls.Add(this.lblTargetDocProperties);
            this.targetDocuments.Controls.Add(this.lvTargetDocClasses);
            this.targetDocuments.Controls.Add(this.lblTargetDocConfig);
            this.targetDocuments.Controls.Add(this.btnTgtCfgDocAdd);
            this.targetDocuments.Controls.Add(this.btnTgtCfgDocUpd);
            this.targetDocuments.Controls.Add(this.btnTgtCfgDocDel);
            this.targetDocuments.Controls.Add(this.btnTgtCfgDocExp);
            this.targetDocuments.Controls.Add(this.btnTgtCfgDocImp);
            this.targetDocuments.Location = new System.Drawing.Point(4, 27);
            this.targetDocuments.Margin = new System.Windows.Forms.Padding(2);
            this.targetDocuments.Name = "targetDocuments";
            this.targetDocuments.Size = new System.Drawing.Size(783, 421);
            this.targetDocuments.TabIndex = 2;
            this.targetDocuments.Text = "Document Class Definitions";
            this.targetDocuments.UseVisualStyleBackColor = true;
            // 
            // lvTargetDocProperties
            // 
            this.lvTargetDocProperties.AllowColumnReorder = true;
            this.lvTargetDocProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTargetDocProperties.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvTargetDocProperties.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTgtDocPropName,
            this.colTgtDocPropSource,
            this.colTgtDocPropType,
            this.colTgtDocPropReq,
            this.colTgtDocPropMap,
            this.colTgtDocPropComputed,
            this.colTgtDocMapName});
            this.lvTargetDocProperties.Location = new System.Drawing.Point(6, 140);
            this.lvTargetDocProperties.Margin = new System.Windows.Forms.Padding(2);
            this.lvTargetDocProperties.Name = "lvTargetDocProperties";
            this.lvTargetDocProperties.Size = new System.Drawing.Size(777, 257);
            this.lvTargetDocProperties.TabIndex = 3;
            this.lvTargetDocProperties.UseCompatibleStateImageBehavior = false;
            this.lvTargetDocProperties.View = System.Windows.Forms.View.Details;
            // 
            // colTgtDocPropName
            // 
            this.colTgtDocPropName.Text = "Name";
            this.colTgtDocPropName.Width = 200;
            // 
            // colTgtDocPropSource
            // 
            this.colTgtDocPropSource.Text = "Source";
            this.colTgtDocPropSource.Width = 200;
            // 
            // colTgtDocPropType
            // 
            this.colTgtDocPropType.Text = "Data Type";
            this.colTgtDocPropType.Width = 100;
            // 
            // colTgtDocPropReq
            // 
            this.colTgtDocPropReq.Text = "Required";
            this.colTgtDocPropReq.Width = 80;
            // 
            // colTgtDocPropMap
            // 
            this.colTgtDocPropMap.Text = "Mapped";
            this.colTgtDocPropMap.Width = 80;
            // 
            // colTgtDocPropComputed
            // 
            this.colTgtDocPropComputed.Text = "Computed";
            this.colTgtDocPropComputed.Width = 100;
            // 
            // colTgtDocMapName
            // 
            this.colTgtDocMapName.Text = "Mapping Name";
            this.colTgtDocMapName.Width = 268;
            // 
            // lblTargetDocProperties
            // 
            this.lblTargetDocProperties.AutoSize = true;
            this.lblTargetDocProperties.Location = new System.Drawing.Point(6, 118);
            this.lblTargetDocProperties.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTargetDocProperties.Name = "lblTargetDocProperties";
            this.lblTargetDocProperties.Size = new System.Drawing.Size(176, 18);
            this.lblTargetDocProperties.TabIndex = 2;
            this.lblTargetDocProperties.Text = "Document Class Properties";
            // 
            // lvTargetDocClasses
            // 
            this.lvTargetDocClasses.AllowColumnReorder = true;
            this.lvTargetDocClasses.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTargetDocClasses.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvTargetDocClasses.CheckBoxes = true;
            this.lvTargetDocClasses.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTargetDocClassName});
            this.lvTargetDocClasses.FullRowSelect = true;
            this.lvTargetDocClasses.Location = new System.Drawing.Point(0, 34);
            this.lvTargetDocClasses.Margin = new System.Windows.Forms.Padding(2);
            this.lvTargetDocClasses.Name = "lvTargetDocClasses";
            this.lvTargetDocClasses.Size = new System.Drawing.Size(768, 70);
            this.lvTargetDocClasses.TabIndex = 1;
            this.lvTargetDocClasses.UseCompatibleStateImageBehavior = false;
            this.lvTargetDocClasses.View = System.Windows.Forms.View.Details;
            this.lvTargetDocClasses.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvTargetDocClasses_ItemChecked);
            // 
            // colTargetDocClassName
            // 
            this.colTargetDocClassName.Text = "Document Class Name";
            this.colTargetDocClassName.Width = 1000;
            // 
            // lblTargetDocConfig
            // 
            this.lblTargetDocConfig.AutoSize = true;
            this.lblTargetDocConfig.Location = new System.Drawing.Point(0, 11);
            this.lblTargetDocConfig.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTargetDocConfig.Name = "lblTargetDocConfig";
            this.lblTargetDocConfig.Size = new System.Drawing.Size(99, 18);
            this.lblTargetDocConfig.TabIndex = 0;
            this.lblTargetDocConfig.Text = "Configuration:";
            // 
            // btnTgtCfgDocAdd
            // 
            this.btnTgtCfgDocAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgDocAdd.Location = new System.Drawing.Point(720, 11);
            this.btnTgtCfgDocAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgDocAdd.Name = "btnTgtCfgDocAdd";
            this.btnTgtCfgDocAdd.Size = new System.Drawing.Size(56, 23);
            this.btnTgtCfgDocAdd.TabIndex = 2;
            this.btnTgtCfgDocAdd.Text = "Add";
            this.btnTgtCfgDocAdd.UseVisualStyleBackColor = true;
            this.btnTgtCfgDocAdd.Click += new System.EventHandler(this.btnTgtCfgDocAdd_Click);
            // 
            // btnTgtCfgDocUpd
            // 
            this.btnTgtCfgDocUpd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgDocUpd.Location = new System.Drawing.Point(660, 11);
            this.btnTgtCfgDocUpd.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgDocUpd.Name = "btnTgtCfgDocUpd";
            this.btnTgtCfgDocUpd.Size = new System.Drawing.Size(60, 23);
            this.btnTgtCfgDocUpd.TabIndex = 3;
            this.btnTgtCfgDocUpd.Text = "Edit";
            this.btnTgtCfgDocUpd.UseVisualStyleBackColor = true;
            this.btnTgtCfgDocUpd.Click += new System.EventHandler(this.btnTgtCfgDocUpd_Click);
            // 
            // btnTgtCfgDocDel
            // 
            this.btnTgtCfgDocDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgDocDel.Location = new System.Drawing.Point(600, 11);
            this.btnTgtCfgDocDel.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgDocDel.Name = "btnTgtCfgDocDel";
            this.btnTgtCfgDocDel.Size = new System.Drawing.Size(60, 23);
            this.btnTgtCfgDocDel.TabIndex = 4;
            this.btnTgtCfgDocDel.Text = "Delete";
            this.btnTgtCfgDocDel.UseVisualStyleBackColor = true;
            this.btnTgtCfgDocDel.Click += new System.EventHandler(this.btnTgtCfgDocDel_Click);
            // 
            // btnTgtCfgDocExp
            // 
            this.btnTgtCfgDocExp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgDocExp.Location = new System.Drawing.Point(540, 11);
            this.btnTgtCfgDocExp.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgDocExp.Name = "btnTgtCfgDocExp";
            this.btnTgtCfgDocExp.Size = new System.Drawing.Size(60, 23);
            this.btnTgtCfgDocExp.TabIndex = 5;
            this.btnTgtCfgDocExp.Text = "Export";
            this.btnTgtCfgDocExp.UseVisualStyleBackColor = true;
            this.btnTgtCfgDocExp.Click += new System.EventHandler(this.btnTgtCfgDocExp_Click);
            // 
            // btnTgtCfgDocImp
            // 
            this.btnTgtCfgDocImp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgDocImp.Location = new System.Drawing.Point(480, 11);
            this.btnTgtCfgDocImp.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgDocImp.Name = "btnTgtCfgDocImp";
            this.btnTgtCfgDocImp.Size = new System.Drawing.Size(60, 23);
            this.btnTgtCfgDocImp.TabIndex = 6;
            this.btnTgtCfgDocImp.Text = "Import";
            this.btnTgtCfgDocImp.UseVisualStyleBackColor = true;
            this.btnTgtCfgDocImp.Click += new System.EventHandler(this.btnTgtCfgDocImp_Click);
            // 
            // targetAnnotations
            // 
            this.targetAnnotations.Controls.Add(this.lvTargetAnnProperties);
            this.targetAnnotations.Controls.Add(this.lblTargetAnnProperties);
            this.targetAnnotations.Controls.Add(this.lvTargetAnnClasses);
            this.targetAnnotations.Controls.Add(this.lblTargetAnnConfig);
            this.targetAnnotations.Controls.Add(this.btnTgtCfgAnnAdd);
            this.targetAnnotations.Controls.Add(this.btnTgtCfgAnnUpd);
            this.targetAnnotations.Controls.Add(this.btnTgtCfgAnnDel);
            this.targetAnnotations.Controls.Add(this.btnTgtCfgAnnExp);
            this.targetAnnotations.Controls.Add(this.btnTgtCfgAnnImp);
            this.targetAnnotations.Location = new System.Drawing.Point(4, 27);
            this.targetAnnotations.Margin = new System.Windows.Forms.Padding(2);
            this.targetAnnotations.Name = "targetAnnotations";
            this.targetAnnotations.Size = new System.Drawing.Size(783, 421);
            this.targetAnnotations.TabIndex = 3;
            this.targetAnnotations.Text = "Annotation Class Definitions";
            this.targetAnnotations.UseVisualStyleBackColor = true;
            // 
            // lvTargetAnnProperties
            // 
            this.lvTargetAnnProperties.AllowColumnReorder = true;
            this.lvTargetAnnProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTargetAnnProperties.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvTargetAnnProperties.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTgtAnnPropName,
            this.colTgtAnnPropSource,
            this.colTgtAnnPropType,
            this.colTgtAnnPropReq,
            this.colTgtAnnPropMap,
            this.colTgtAnnPropComputed,
            this.colTgtAnnMapName});
            this.lvTargetAnnProperties.Location = new System.Drawing.Point(6, 140);
            this.lvTargetAnnProperties.Margin = new System.Windows.Forms.Padding(2);
            this.lvTargetAnnProperties.Name = "lvTargetAnnProperties";
            this.lvTargetAnnProperties.Size = new System.Drawing.Size(776, 218);
            this.lvTargetAnnProperties.TabIndex = 3;
            this.lvTargetAnnProperties.UseCompatibleStateImageBehavior = false;
            this.lvTargetAnnProperties.View = System.Windows.Forms.View.Details;
            // 
            // colTgtAnnPropName
            // 
            this.colTgtAnnPropName.Text = "Name";
            this.colTgtAnnPropName.Width = 200;
            // 
            // colTgtAnnPropSource
            // 
            this.colTgtAnnPropSource.Text = "Source";
            this.colTgtAnnPropSource.Width = 200;
            // 
            // colTgtAnnPropType
            // 
            this.colTgtAnnPropType.Text = "Data Type";
            this.colTgtAnnPropType.Width = 100;
            // 
            // colTgtAnnPropReq
            // 
            this.colTgtAnnPropReq.Text = "Required";
            this.colTgtAnnPropReq.Width = 80;
            // 
            // colTgtAnnPropMap
            // 
            this.colTgtAnnPropMap.Text = "Mapped";
            this.colTgtAnnPropMap.Width = 80;
            // 
            // colTgtAnnPropComputed
            // 
            this.colTgtAnnPropComputed.Text = "Computed";
            this.colTgtAnnPropComputed.Width = 100;
            // 
            // colTgtAnnMapName
            // 
            this.colTgtAnnMapName.Text = "Mapping Name";
            this.colTgtAnnMapName.Width = 268;
            // 
            // lblTargetAnnProperties
            // 
            this.lblTargetAnnProperties.AutoSize = true;
            this.lblTargetAnnProperties.Location = new System.Drawing.Point(6, 118);
            this.lblTargetAnnProperties.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTargetAnnProperties.Name = "lblTargetAnnProperties";
            this.lblTargetAnnProperties.Size = new System.Drawing.Size(183, 18);
            this.lblTargetAnnProperties.TabIndex = 2;
            this.lblTargetAnnProperties.Text = "Annotation Class Properties";
            // 
            // lvTargetAnnClasses
            // 
            this.lvTargetAnnClasses.AllowColumnReorder = true;
            this.lvTargetAnnClasses.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTargetAnnClasses.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvTargetAnnClasses.CheckBoxes = true;
            this.lvTargetAnnClasses.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTargetAnnClassName});
            this.lvTargetAnnClasses.FullRowSelect = true;
            this.lvTargetAnnClasses.Location = new System.Drawing.Point(0, 34);
            this.lvTargetAnnClasses.Margin = new System.Windows.Forms.Padding(2);
            this.lvTargetAnnClasses.Name = "lvTargetAnnClasses";
            this.lvTargetAnnClasses.Size = new System.Drawing.Size(768, 70);
            this.lvTargetAnnClasses.TabIndex = 1;
            this.lvTargetAnnClasses.UseCompatibleStateImageBehavior = false;
            this.lvTargetAnnClasses.View = System.Windows.Forms.View.Details;
            this.lvTargetAnnClasses.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvTargetAnnClasses_ItemChecked);
            // 
            // colTargetAnnClassName
            // 
            this.colTargetAnnClassName.Text = "Annotation Class Name";
            this.colTargetAnnClassName.Width = 1000;
            // 
            // lblTargetAnnConfig
            // 
            this.lblTargetAnnConfig.AutoSize = true;
            this.lblTargetAnnConfig.Location = new System.Drawing.Point(0, 11);
            this.lblTargetAnnConfig.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTargetAnnConfig.Name = "lblTargetAnnConfig";
            this.lblTargetAnnConfig.Size = new System.Drawing.Size(99, 18);
            this.lblTargetAnnConfig.TabIndex = 0;
            this.lblTargetAnnConfig.Text = "Configuration:";
            // 
            // btnTgtCfgAnnAdd
            // 
            this.btnTgtCfgAnnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgAnnAdd.Location = new System.Drawing.Point(720, 11);
            this.btnTgtCfgAnnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgAnnAdd.Name = "btnTgtCfgAnnAdd";
            this.btnTgtCfgAnnAdd.Size = new System.Drawing.Size(56, 23);
            this.btnTgtCfgAnnAdd.TabIndex = 2;
            this.btnTgtCfgAnnAdd.Text = "Add";
            this.btnTgtCfgAnnAdd.UseVisualStyleBackColor = true;
            this.btnTgtCfgAnnAdd.Click += new System.EventHandler(this.btnTgtCfgAnnAdd_Click);
            // 
            // btnTgtCfgAnnUpd
            // 
            this.btnTgtCfgAnnUpd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgAnnUpd.Location = new System.Drawing.Point(660, 11);
            this.btnTgtCfgAnnUpd.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgAnnUpd.Name = "btnTgtCfgAnnUpd";
            this.btnTgtCfgAnnUpd.Size = new System.Drawing.Size(60, 23);
            this.btnTgtCfgAnnUpd.TabIndex = 3;
            this.btnTgtCfgAnnUpd.Text = "Edit";
            this.btnTgtCfgAnnUpd.UseVisualStyleBackColor = true;
            this.btnTgtCfgAnnUpd.Click += new System.EventHandler(this.btnTgtCfgAnnUpd_Click);
            // 
            // btnTgtCfgAnnDel
            // 
            this.btnTgtCfgAnnDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgAnnDel.Location = new System.Drawing.Point(600, 11);
            this.btnTgtCfgAnnDel.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgAnnDel.Name = "btnTgtCfgAnnDel";
            this.btnTgtCfgAnnDel.Size = new System.Drawing.Size(60, 23);
            this.btnTgtCfgAnnDel.TabIndex = 4;
            this.btnTgtCfgAnnDel.Text = "Delete";
            this.btnTgtCfgAnnDel.UseVisualStyleBackColor = true;
            this.btnTgtCfgAnnDel.Click += new System.EventHandler(this.btnTgtCfgAnnDel_Click);
            // 
            // btnTgtCfgAnnExp
            // 
            this.btnTgtCfgAnnExp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgAnnExp.Location = new System.Drawing.Point(540, 11);
            this.btnTgtCfgAnnExp.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgAnnExp.Name = "btnTgtCfgAnnExp";
            this.btnTgtCfgAnnExp.Size = new System.Drawing.Size(60, 23);
            this.btnTgtCfgAnnExp.TabIndex = 5;
            this.btnTgtCfgAnnExp.Text = "Export";
            this.btnTgtCfgAnnExp.UseVisualStyleBackColor = true;
            this.btnTgtCfgAnnExp.Click += new System.EventHandler(this.btnTgtCfgAnnExp_Click);
            // 
            // btnTgtCfgAnnImp
            // 
            this.btnTgtCfgAnnImp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgAnnImp.Location = new System.Drawing.Point(480, 11);
            this.btnTgtCfgAnnImp.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgAnnImp.Name = "btnTgtCfgAnnImp";
            this.btnTgtCfgAnnImp.Size = new System.Drawing.Size(60, 23);
            this.btnTgtCfgAnnImp.TabIndex = 6;
            this.btnTgtCfgAnnImp.Text = "Import";
            this.btnTgtCfgAnnImp.UseVisualStyleBackColor = true;
            this.btnTgtCfgAnnImp.Click += new System.EventHandler(this.btnTgtCfgAnnImp_Click);
            // 
            // targetTasks
            // 
            this.targetTasks.Controls.Add(this.lvTargetInitDocs);
            this.targetTasks.Controls.Add(this.lblTargetInitDocs);
            this.targetTasks.Controls.Add(this.lvTargetTaskProperties);
            this.targetTasks.Controls.Add(this.lblTargetTaskProperties);
            this.targetTasks.Controls.Add(this.lvTargetTaskTypes);
            this.targetTasks.Controls.Add(this.lblTargetTaskConfig);
            this.targetTasks.Controls.Add(this.btnTgtCfgTaskAdd);
            this.targetTasks.Controls.Add(this.btnTgtCfgTaskUpd);
            this.targetTasks.Controls.Add(this.btnTgtCfgTaskDel);
            this.targetTasks.Controls.Add(this.btnTgtCfgTaskExp);
            this.targetTasks.Controls.Add(this.btnTgtCfgTaskImp);
            this.targetTasks.Location = new System.Drawing.Point(4, 27);
            this.targetTasks.Margin = new System.Windows.Forms.Padding(2);
            this.targetTasks.Name = "targetTasks";
            this.targetTasks.Size = new System.Drawing.Size(783, 421);
            this.targetTasks.TabIndex = 4;
            this.targetTasks.Text = "Task Definitions";
            this.targetTasks.UseVisualStyleBackColor = true;
            // 
            // lvTargetInitDocs
            // 
            this.lvTargetInitDocs.AllowColumnReorder = true;
            this.lvTargetInitDocs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTargetInitDocs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvTargetInitDocs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTaskDocInit,
            this.colDocInitDocClass,
            this.colInitDocSource,
            this.colAttachProperty});
            this.lvTargetInitDocs.FullRowSelect = true;
            this.lvTargetInitDocs.Location = new System.Drawing.Point(6, 320);
            this.lvTargetInitDocs.Margin = new System.Windows.Forms.Padding(2);
            this.lvTargetInitDocs.MultiSelect = false;
            this.lvTargetInitDocs.Name = "lvTargetInitDocs";
            this.lvTargetInitDocs.Size = new System.Drawing.Size(772, 66);
            this.lvTargetInitDocs.TabIndex = 5;
            this.lvTargetInitDocs.UseCompatibleStateImageBehavior = false;
            this.lvTargetInitDocs.View = System.Windows.Forms.View.Details;
            // 
            // colTaskDocInit
            // 
            this.colTaskDocInit.Text = "Document Initiated";
            this.colTaskDocInit.Width = 100;
            // 
            // colDocInitDocClass
            // 
            this.colDocInitDocClass.Text = "Document Class";
            this.colDocInitDocClass.Width = 150;
            // 
            // colInitDocSource
            // 
            this.colInitDocSource.Text = "Initiating Document Source";
            this.colInitDocSource.Width = 200;
            // 
            // colAttachProperty
            // 
            this.colAttachProperty.Text = "Attaching Property";
            this.colAttachProperty.Width = 575;
            // 
            // lblTargetInitDocs
            // 
            this.lblTargetInitDocs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTargetInitDocs.AutoSize = true;
            this.lblTargetInitDocs.Location = new System.Drawing.Point(3, 298);
            this.lblTargetInitDocs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTargetInitDocs.Name = "lblTargetInitDocs";
            this.lblTargetInitDocs.Size = new System.Drawing.Size(146, 18);
            this.lblTargetInitDocs.TabIndex = 4;
            this.lblTargetInitDocs.Text = "Initiating Documents:";
            // 
            // lvTargetTaskProperties
            // 
            this.lvTargetTaskProperties.AllowColumnReorder = true;
            this.lvTargetTaskProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTargetTaskProperties.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvTargetTaskProperties.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTgtTaskPropName,
            this.colTgtTaskPropSource,
            this.colTgtTaskPropType,
            this.colTgtPropTaskReq,
            this.colTgtTaskPropMap,
            this.colTgtTaskPropComputed,
            this.colTgtTaskMapName});
            this.lvTargetTaskProperties.FullRowSelect = true;
            this.lvTargetTaskProperties.Location = new System.Drawing.Point(6, 171);
            this.lvTargetTaskProperties.Margin = new System.Windows.Forms.Padding(2);
            this.lvTargetTaskProperties.MultiSelect = false;
            this.lvTargetTaskProperties.Name = "lvTargetTaskProperties";
            this.lvTargetTaskProperties.Size = new System.Drawing.Size(777, 125);
            this.lvTargetTaskProperties.TabIndex = 3;
            this.lvTargetTaskProperties.UseCompatibleStateImageBehavior = false;
            this.lvTargetTaskProperties.View = System.Windows.Forms.View.Details;
            // 
            // colTgtTaskPropName
            // 
            this.colTgtTaskPropName.Text = "Name";
            this.colTgtTaskPropName.Width = 200;
            // 
            // colTgtTaskPropSource
            // 
            this.colTgtTaskPropSource.Text = "Source";
            this.colTgtTaskPropSource.Width = 200;
            // 
            // colTgtTaskPropType
            // 
            this.colTgtTaskPropType.Text = "Type";
            this.colTgtTaskPropType.Width = 100;
            // 
            // colTgtPropTaskReq
            // 
            this.colTgtPropTaskReq.Text = "Required";
            this.colTgtPropTaskReq.Width = 80;
            // 
            // colTgtTaskPropMap
            // 
            this.colTgtTaskPropMap.Text = "Mapped";
            this.colTgtTaskPropMap.Width = 80;
            // 
            // colTgtTaskPropComputed
            // 
            this.colTgtTaskPropComputed.Text = "Computed";
            this.colTgtTaskPropComputed.Width = 80;
            // 
            // colTgtTaskMapName
            // 
            this.colTgtTaskMapName.Text = "Mapping Name";
            this.colTgtTaskMapName.Width = 270;
            // 
            // lblTargetTaskProperties
            // 
            this.lblTargetTaskProperties.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTargetTaskProperties.AutoSize = true;
            this.lblTargetTaskProperties.Location = new System.Drawing.Point(2, 150);
            this.lblTargetTaskProperties.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTargetTaskProperties.Name = "lblTargetTaskProperties";
            this.lblTargetTaskProperties.Size = new System.Drawing.Size(108, 18);
            this.lblTargetTaskProperties.TabIndex = 2;
            this.lblTargetTaskProperties.Text = "Task Properties:";
            // 
            // lvTargetTaskTypes
            // 
            this.lvTargetTaskTypes.AllowColumnReorder = true;
            this.lvTargetTaskTypes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTargetTaskTypes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvTargetTaskTypes.CheckBoxes = true;
            this.lvTargetTaskTypes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTgtTaskTypes,
            this.colTgtTaskCaseName});
            this.lvTargetTaskTypes.FullRowSelect = true;
            this.lvTargetTaskTypes.Location = new System.Drawing.Point(0, 34);
            this.lvTargetTaskTypes.Margin = new System.Windows.Forms.Padding(2);
            this.lvTargetTaskTypes.MultiSelect = false;
            this.lvTargetTaskTypes.Name = "lvTargetTaskTypes";
            this.lvTargetTaskTypes.Size = new System.Drawing.Size(777, 138);
            this.lvTargetTaskTypes.TabIndex = 1;
            this.lvTargetTaskTypes.UseCompatibleStateImageBehavior = false;
            this.lvTargetTaskTypes.View = System.Windows.Forms.View.Details;
            this.lvTargetTaskTypes.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvTargetTaskTypes_ItemChecked);
            // 
            // colTgtTaskTypes
            // 
            this.colTgtTaskTypes.Text = "Task Type";
            this.colTgtTaskTypes.Width = 500;
            // 
            // colTgtTaskCaseName
            // 
            this.colTgtTaskCaseName.Text = "Parent Case Type";
            this.colTgtTaskCaseName.Width = 500;
            // 
            // lblTargetTaskConfig
            // 
            this.lblTargetTaskConfig.AutoSize = true;
            this.lblTargetTaskConfig.Location = new System.Drawing.Point(0, 11);
            this.lblTargetTaskConfig.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTargetTaskConfig.Name = "lblTargetTaskConfig";
            this.lblTargetTaskConfig.Size = new System.Drawing.Size(99, 18);
            this.lblTargetTaskConfig.TabIndex = 0;
            this.lblTargetTaskConfig.Text = "Configuration:";
            // 
            // btnTgtCfgTaskAdd
            // 
            this.btnTgtCfgTaskAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgTaskAdd.Location = new System.Drawing.Point(720, 11);
            this.btnTgtCfgTaskAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgTaskAdd.Name = "btnTgtCfgTaskAdd";
            this.btnTgtCfgTaskAdd.Size = new System.Drawing.Size(56, 23);
            this.btnTgtCfgTaskAdd.TabIndex = 2;
            this.btnTgtCfgTaskAdd.Text = "Add";
            this.btnTgtCfgTaskAdd.UseVisualStyleBackColor = true;
            this.btnTgtCfgTaskAdd.Click += new System.EventHandler(this.btnTgtCfgTaskAdd_Click);
            // 
            // btnTgtCfgTaskUpd
            // 
            this.btnTgtCfgTaskUpd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgTaskUpd.Location = new System.Drawing.Point(660, 11);
            this.btnTgtCfgTaskUpd.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgTaskUpd.Name = "btnTgtCfgTaskUpd";
            this.btnTgtCfgTaskUpd.Size = new System.Drawing.Size(60, 23);
            this.btnTgtCfgTaskUpd.TabIndex = 3;
            this.btnTgtCfgTaskUpd.Text = "Edit";
            this.btnTgtCfgTaskUpd.UseVisualStyleBackColor = true;
            this.btnTgtCfgTaskUpd.Click += new System.EventHandler(this.btnTgtCfgTaskUpd_Click);
            // 
            // btnTgtCfgTaskDel
            // 
            this.btnTgtCfgTaskDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgTaskDel.Location = new System.Drawing.Point(600, 11);
            this.btnTgtCfgTaskDel.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgTaskDel.Name = "btnTgtCfgTaskDel";
            this.btnTgtCfgTaskDel.Size = new System.Drawing.Size(60, 23);
            this.btnTgtCfgTaskDel.TabIndex = 4;
            this.btnTgtCfgTaskDel.Text = "Delete";
            this.btnTgtCfgTaskDel.UseVisualStyleBackColor = true;
            this.btnTgtCfgTaskDel.Click += new System.EventHandler(this.btnTgtCfgTaskDel_Click);
            // 
            // btnTgtCfgTaskExp
            // 
            this.btnTgtCfgTaskExp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgTaskExp.Location = new System.Drawing.Point(540, 11);
            this.btnTgtCfgTaskExp.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgTaskExp.Name = "btnTgtCfgTaskExp";
            this.btnTgtCfgTaskExp.Size = new System.Drawing.Size(60, 23);
            this.btnTgtCfgTaskExp.TabIndex = 5;
            this.btnTgtCfgTaskExp.Text = "Export";
            this.btnTgtCfgTaskExp.UseVisualStyleBackColor = true;
            this.btnTgtCfgTaskExp.Click += new System.EventHandler(this.btnTgtCfgTaskExp_Click);
            // 
            // btnTgtCfgTaskImp
            // 
            this.btnTgtCfgTaskImp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTgtCfgTaskImp.Location = new System.Drawing.Point(480, 11);
            this.btnTgtCfgTaskImp.Margin = new System.Windows.Forms.Padding(2);
            this.btnTgtCfgTaskImp.Name = "btnTgtCfgTaskImp";
            this.btnTgtCfgTaskImp.Size = new System.Drawing.Size(60, 23);
            this.btnTgtCfgTaskImp.TabIndex = 6;
            this.btnTgtCfgTaskImp.Text = "Import";
            this.btnTgtCfgTaskImp.UseVisualStyleBackColor = true;
            this.btnTgtCfgTaskImp.Click += new System.EventHandler(this.btnTgtCfgTaskImp_Click);
            // 
            // defineIdentifiers
            // 
            this.defineIdentifiers.Controls.Add(this.lvIdentifierDefns);
            this.defineIdentifiers.Controls.Add(this.lblIdConfig);
            this.defineIdentifiers.Controls.Add(this.btnUpdIdentifier);
            this.defineIdentifiers.Controls.Add(this.btnExpIdentifier);
            this.defineIdentifiers.Controls.Add(this.btnImpIdentifier);
            this.defineIdentifiers.Location = new System.Drawing.Point(4, 27);
            this.defineIdentifiers.Margin = new System.Windows.Forms.Padding(2);
            this.defineIdentifiers.Name = "defineIdentifiers";
            this.defineIdentifiers.Size = new System.Drawing.Size(792, 386);
            this.defineIdentifiers.TabIndex = 2;
            this.defineIdentifiers.Text = "Define Identifiers";
            this.defineIdentifiers.UseVisualStyleBackColor = true;
            // 
            // lvIdentifierDefns
            // 
            this.lvIdentifierDefns.AllowColumnReorder = true;
            this.lvIdentifierDefns.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvIdentifierDefns.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvIdentifierDefns.CheckBoxes = true;
            this.lvIdentifierDefns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colIdentifier,
            this.colIdDefn});
            this.lvIdentifierDefns.FullRowSelect = true;
            this.lvIdentifierDefns.Location = new System.Drawing.Point(8, 34);
            this.lvIdentifierDefns.Margin = new System.Windows.Forms.Padding(2);
            this.lvIdentifierDefns.MultiSelect = false;
            this.lvIdentifierDefns.Name = "lvIdentifierDefns";
            this.lvIdentifierDefns.Size = new System.Drawing.Size(778, 327);
            this.lvIdentifierDefns.TabIndex = 1;
            this.lvIdentifierDefns.UseCompatibleStateImageBehavior = false;
            this.lvIdentifierDefns.View = System.Windows.Forms.View.Details;
            this.lvIdentifierDefns.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvIdentifierDefns_ItemChecked);
            // 
            // colIdentifier
            // 
            this.colIdentifier.Text = "Identifier";
            this.colIdentifier.Width = 350;
            // 
            // colIdDefn
            // 
            this.colIdDefn.Text = "Definition";
            this.colIdDefn.Width = 650;
            // 
            // lblIdConfig
            // 
            this.lblIdConfig.AutoSize = true;
            this.lblIdConfig.Location = new System.Drawing.Point(0, 11);
            this.lblIdConfig.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIdConfig.Name = "lblIdConfig";
            this.lblIdConfig.Size = new System.Drawing.Size(99, 18);
            this.lblIdConfig.TabIndex = 0;
            this.lblIdConfig.Text = "Configuration:";
            // 
            // btnUpdIdentifier
            // 
            this.btnUpdIdentifier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdIdentifier.Location = new System.Drawing.Point(728, 11);
            this.btnUpdIdentifier.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdIdentifier.Name = "btnUpdIdentifier";
            this.btnUpdIdentifier.Size = new System.Drawing.Size(56, 23);
            this.btnUpdIdentifier.TabIndex = 7;
            this.btnUpdIdentifier.Text = "Edit";
            this.btnUpdIdentifier.UseVisualStyleBackColor = true;
            this.btnUpdIdentifier.Click += new System.EventHandler(this.btnUpdIdentifier_Click);
            // 
            // btnExpIdentifier
            // 
            this.btnExpIdentifier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExpIdentifier.Location = new System.Drawing.Point(668, 11);
            this.btnExpIdentifier.Margin = new System.Windows.Forms.Padding(2);
            this.btnExpIdentifier.Name = "btnExpIdentifier";
            this.btnExpIdentifier.Size = new System.Drawing.Size(60, 23);
            this.btnExpIdentifier.TabIndex = 8;
            this.btnExpIdentifier.Text = "Export";
            this.btnExpIdentifier.UseVisualStyleBackColor = true;
            this.btnExpIdentifier.Click += new System.EventHandler(this.btnExpIdentifier_Click);
            // 
            // btnImpIdentifier
            // 
            this.btnImpIdentifier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImpIdentifier.Location = new System.Drawing.Point(608, 11);
            this.btnImpIdentifier.Margin = new System.Windows.Forms.Padding(2);
            this.btnImpIdentifier.Name = "btnImpIdentifier";
            this.btnImpIdentifier.Size = new System.Drawing.Size(60, 23);
            this.btnImpIdentifier.TabIndex = 9;
            this.btnImpIdentifier.Text = "Import";
            this.btnImpIdentifier.UseVisualStyleBackColor = true;
            this.btnImpIdentifier.Click += new System.EventHandler(this.btnImpIdentifier_Click);
            // 
            // pageDefine
            // 
            this.pageDefine.Controls.Add(this.tabDefine);
            this.pageDefine.Location = new System.Drawing.Point(4, 32);
            this.pageDefine.Margin = new System.Windows.Forms.Padding(2);
            this.pageDefine.Name = "pageDefine";
            this.pageDefine.Size = new System.Drawing.Size(792, 412);
            this.pageDefine.TabIndex = 1;
            this.pageDefine.Text = "Define";
            this.pageDefine.UseVisualStyleBackColor = true;
            // 
            // tabDefine
            // 
            this.tabDefine.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabDefine.Controls.Add(this.defineConversions);
            this.tabDefine.Controls.Add(this.targetMappings);
            this.tabDefine.Controls.Add(this.defineJobs);
            this.tabDefine.Controls.Add(this.defineBatches);
            this.tabDefine.Location = new System.Drawing.Point(0, 0);
            this.tabDefine.Margin = new System.Windows.Forms.Padding(2);
            this.tabDefine.Name = "tabDefine";
            this.tabDefine.SelectedIndex = 0;
            this.tabDefine.Size = new System.Drawing.Size(800, 417);
            this.tabDefine.TabIndex = 0;
            this.tabDefine.SelectedIndexChanged += new System.EventHandler(this.tabDefine_SelectedIndexChanged);
            // 
            // defineConversions
            // 
            this.defineConversions.Controls.Add(this.lvConvDefns);
            this.defineConversions.Controls.Add(this.lblConvDefns);
            this.defineConversions.Controls.Add(this.btnConvDefnAdd);
            this.defineConversions.Controls.Add(this.btnConvDefnUpd);
            this.defineConversions.Controls.Add(this.btnConvDefnDel);
            this.defineConversions.Controls.Add(this.btnConvExport);
            this.defineConversions.Controls.Add(this.btnConvImport);
            this.defineConversions.Location = new System.Drawing.Point(4, 27);
            this.defineConversions.Margin = new System.Windows.Forms.Padding(2);
            this.defineConversions.Name = "defineConversions";
            this.defineConversions.Padding = new System.Windows.Forms.Padding(2);
            this.defineConversions.Size = new System.Drawing.Size(792, 386);
            this.defineConversions.TabIndex = 0;
            this.defineConversions.Text = "Define Conversions";
            this.defineConversions.UseVisualStyleBackColor = true;
            // 
            // lvConvDefns
            // 
            this.lvConvDefns.AllowColumnReorder = true;
            this.lvConvDefns.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvConvDefns.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvConvDefns.CheckBoxes = true;
            this.lvConvDefns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colCDName,
            this.colSingleDoc});
            this.lvConvDefns.FullRowSelect = true;
            this.lvConvDefns.Location = new System.Drawing.Point(8, 32);
            this.lvConvDefns.Margin = new System.Windows.Forms.Padding(2);
            this.lvConvDefns.MultiSelect = false;
            this.lvConvDefns.Name = "lvConvDefns";
            this.lvConvDefns.Size = new System.Drawing.Size(778, 352);
            this.lvConvDefns.TabIndex = 1;
            this.lvConvDefns.UseCompatibleStateImageBehavior = false;
            this.lvConvDefns.View = System.Windows.Forms.View.Details;
            this.lvConvDefns.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvConvDefns_ItemChecked);
            // 
            // colCDName
            // 
            this.colCDName.Text = "Name";
            this.colCDName.Width = 300;
            // 
            // colSingleDoc
            // 
            this.colSingleDoc.Text = "Marshal To Single Document";
            this.colSingleDoc.Width = 225;
            // 
            // lblConvDefns
            // 
            this.lblConvDefns.AutoSize = true;
            this.lblConvDefns.Location = new System.Drawing.Point(4, 10);
            this.lblConvDefns.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblConvDefns.Name = "lblConvDefns";
            this.lblConvDefns.Size = new System.Drawing.Size(209, 18);
            this.lblConvDefns.TabIndex = 0;
            this.lblConvDefns.Text = "Current Conversion Definitions:";
            // 
            // btnConvDefnAdd
            // 
            this.btnConvDefnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConvDefnAdd.Location = new System.Drawing.Point(720, 10);
            this.btnConvDefnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnConvDefnAdd.Name = "btnConvDefnAdd";
            this.btnConvDefnAdd.Size = new System.Drawing.Size(56, 23);
            this.btnConvDefnAdd.TabIndex = 1;
            this.btnConvDefnAdd.Text = "Add";
            this.btnConvDefnAdd.UseVisualStyleBackColor = true;
            this.btnConvDefnAdd.Click += new System.EventHandler(this.btnConvDefnAdd_Click);
            // 
            // btnConvDefnUpd
            // 
            this.btnConvDefnUpd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConvDefnUpd.Location = new System.Drawing.Point(660, 10);
            this.btnConvDefnUpd.Margin = new System.Windows.Forms.Padding(2);
            this.btnConvDefnUpd.Name = "btnConvDefnUpd";
            this.btnConvDefnUpd.Size = new System.Drawing.Size(60, 23);
            this.btnConvDefnUpd.TabIndex = 2;
            this.btnConvDefnUpd.Text = "Edit";
            this.btnConvDefnUpd.UseVisualStyleBackColor = true;
            this.btnConvDefnUpd.Click += new System.EventHandler(this.btnConvDefnUpd_Click);
            // 
            // btnConvDefnDel
            // 
            this.btnConvDefnDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConvDefnDel.Location = new System.Drawing.Point(600, 10);
            this.btnConvDefnDel.Margin = new System.Windows.Forms.Padding(2);
            this.btnConvDefnDel.Name = "btnConvDefnDel";
            this.btnConvDefnDel.Size = new System.Drawing.Size(60, 23);
            this.btnConvDefnDel.TabIndex = 3;
            this.btnConvDefnDel.Text = "Delete";
            this.btnConvDefnDel.UseVisualStyleBackColor = true;
            this.btnConvDefnDel.Click += new System.EventHandler(this.btnConvDefnDel_Click);
            // 
            // btnConvExport
            // 
            this.btnConvExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConvExport.Location = new System.Drawing.Point(540, 10);
            this.btnConvExport.Margin = new System.Windows.Forms.Padding(2);
            this.btnConvExport.Name = "btnConvExport";
            this.btnConvExport.Size = new System.Drawing.Size(60, 23);
            this.btnConvExport.TabIndex = 4;
            this.btnConvExport.Text = "Export";
            this.btnConvExport.UseVisualStyleBackColor = true;
            this.btnConvExport.Click += new System.EventHandler(this.btnConvExport_Click);
            // 
            // btnConvImport
            // 
            this.btnConvImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConvImport.Location = new System.Drawing.Point(480, 10);
            this.btnConvImport.Margin = new System.Windows.Forms.Padding(2);
            this.btnConvImport.Name = "btnConvImport";
            this.btnConvImport.Size = new System.Drawing.Size(60, 23);
            this.btnConvImport.TabIndex = 5;
            this.btnConvImport.Text = "Import";
            this.btnConvImport.UseVisualStyleBackColor = true;
            this.btnConvImport.Click += new System.EventHandler(this.btnConvImport_Click);
            // 
            // targetMappings
            // 
            this.targetMappings.Controls.Add(this.lvMapDefns);
            this.targetMappings.Controls.Add(this.lblMapDefns);
            this.targetMappings.Controls.Add(this.btnMapDefnAdd);
            this.targetMappings.Controls.Add(this.btnMapDefnUpd);
            this.targetMappings.Controls.Add(this.btnMapDefnDel);
            this.targetMappings.Controls.Add(this.btnMapDefnExport);
            this.targetMappings.Controls.Add(this.btnMapDefnImport);
            this.targetMappings.Location = new System.Drawing.Point(4, 27);
            this.targetMappings.Margin = new System.Windows.Forms.Padding(2);
            this.targetMappings.Name = "targetMappings";
            this.targetMappings.Padding = new System.Windows.Forms.Padding(2);
            this.targetMappings.Size = new System.Drawing.Size(792, 386);
            this.targetMappings.TabIndex = 0;
            this.targetMappings.Text = "Define Mappings";
            this.targetMappings.UseVisualStyleBackColor = true;
            // 
            // lvMapDefns
            // 
            this.lvMapDefns.AllowColumnReorder = true;
            this.lvMapDefns.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvMapDefns.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvMapDefns.CheckBoxes = true;
            this.lvMapDefns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colMapName});
            this.lvMapDefns.FullRowSelect = true;
            this.lvMapDefns.Location = new System.Drawing.Point(8, 32);
            this.lvMapDefns.Margin = new System.Windows.Forms.Padding(2);
            this.lvMapDefns.MultiSelect = false;
            this.lvMapDefns.Name = "lvMapDefns";
            this.lvMapDefns.Size = new System.Drawing.Size(780, 362);
            this.lvMapDefns.TabIndex = 1;
            this.lvMapDefns.UseCompatibleStateImageBehavior = false;
            this.lvMapDefns.View = System.Windows.Forms.View.Details;
            this.lvMapDefns.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvMapDefns_ItemChecked);
            // 
            // colMapName
            // 
            this.colMapName.Text = "Name";
            this.colMapName.Width = 450;
            // 
            // lblMapDefns
            // 
            this.lblMapDefns.AutoSize = true;
            this.lblMapDefns.Location = new System.Drawing.Point(4, 10);
            this.lblMapDefns.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMapDefns.Name = "lblMapDefns";
            this.lblMapDefns.Size = new System.Drawing.Size(192, 18);
            this.lblMapDefns.TabIndex = 0;
            this.lblMapDefns.Text = "Current Mapping Definitions:";
            // 
            // btnMapDefnAdd
            // 
            this.btnMapDefnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMapDefnAdd.Location = new System.Drawing.Point(720, 10);
            this.btnMapDefnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnMapDefnAdd.Name = "btnMapDefnAdd";
            this.btnMapDefnAdd.Size = new System.Drawing.Size(56, 23);
            this.btnMapDefnAdd.TabIndex = 1;
            this.btnMapDefnAdd.Text = "Add";
            this.btnMapDefnAdd.UseVisualStyleBackColor = true;
            this.btnMapDefnAdd.Click += new System.EventHandler(this.btnMapDefnAdd_Click);
            // 
            // btnMapDefnUpd
            // 
            this.btnMapDefnUpd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMapDefnUpd.Location = new System.Drawing.Point(660, 10);
            this.btnMapDefnUpd.Margin = new System.Windows.Forms.Padding(2);
            this.btnMapDefnUpd.Name = "btnMapDefnUpd";
            this.btnMapDefnUpd.Size = new System.Drawing.Size(60, 23);
            this.btnMapDefnUpd.TabIndex = 2;
            this.btnMapDefnUpd.Text = "Edit";
            this.btnMapDefnUpd.UseVisualStyleBackColor = true;
            this.btnMapDefnUpd.Click += new System.EventHandler(this.btnMapDefnUpd_Click);
            // 
            // btnMapDefnDel
            // 
            this.btnMapDefnDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMapDefnDel.Location = new System.Drawing.Point(600, 10);
            this.btnMapDefnDel.Margin = new System.Windows.Forms.Padding(2);
            this.btnMapDefnDel.Name = "btnMapDefnDel";
            this.btnMapDefnDel.Size = new System.Drawing.Size(60, 23);
            this.btnMapDefnDel.TabIndex = 3;
            this.btnMapDefnDel.Text = "Delete";
            this.btnMapDefnDel.UseVisualStyleBackColor = true;
            this.btnMapDefnDel.Click += new System.EventHandler(this.btnMapDefnDel_Click);
            // 
            // btnMapDefnExport
            // 
            this.btnMapDefnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMapDefnExport.Location = new System.Drawing.Point(540, 10);
            this.btnMapDefnExport.Margin = new System.Windows.Forms.Padding(2);
            this.btnMapDefnExport.Name = "btnMapDefnExport";
            this.btnMapDefnExport.Size = new System.Drawing.Size(60, 23);
            this.btnMapDefnExport.TabIndex = 4;
            this.btnMapDefnExport.Text = "Export";
            this.btnMapDefnExport.UseVisualStyleBackColor = true;
            this.btnMapDefnExport.Click += new System.EventHandler(this.btnMapDefnExport_Click);
            // 
            // btnMapDefnImport
            // 
            this.btnMapDefnImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMapDefnImport.Location = new System.Drawing.Point(480, 10);
            this.btnMapDefnImport.Margin = new System.Windows.Forms.Padding(2);
            this.btnMapDefnImport.Name = "btnMapDefnImport";
            this.btnMapDefnImport.Size = new System.Drawing.Size(60, 23);
            this.btnMapDefnImport.TabIndex = 5;
            this.btnMapDefnImport.Text = "Import";
            this.btnMapDefnImport.UseVisualStyleBackColor = true;
            this.btnMapDefnImport.Click += new System.EventHandler(this.btnMapDefnImport_Click);
            // 
            // defineJobs
            // 
            this.defineJobs.Controls.Add(this.btnAddJob);
            this.defineJobs.Controls.Add(this.btnEditJob);
            this.defineJobs.Controls.Add(this.btnDeleteJob);
            this.defineJobs.Controls.Add(this.btnExportJob);
            this.defineJobs.Controls.Add(this.btnImportJob);
            this.defineJobs.Controls.Add(this.lvJobDefns);
            this.defineJobs.Controls.Add(this.lblJobDefns);
            this.defineJobs.Location = new System.Drawing.Point(4, 27);
            this.defineJobs.Margin = new System.Windows.Forms.Padding(2);
            this.defineJobs.Name = "defineJobs";
            this.defineJobs.Size = new System.Drawing.Size(792, 386);
            this.defineJobs.TabIndex = 3;
            this.defineJobs.Text = "Define Jobs";
            this.defineJobs.UseVisualStyleBackColor = true;
            // 
            // btnAddJob
            // 
            this.btnAddJob.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddJob.Location = new System.Drawing.Point(720, 10);
            this.btnAddJob.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddJob.Name = "btnAddJob";
            this.btnAddJob.Size = new System.Drawing.Size(56, 23);
            this.btnAddJob.TabIndex = 4;
            this.btnAddJob.Text = "Add";
            this.btnAddJob.UseVisualStyleBackColor = true;
            this.btnAddJob.Click += new System.EventHandler(this.btnAddJob_Click);
            // 
            // btnEditJob
            // 
            this.btnEditJob.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditJob.Location = new System.Drawing.Point(660, 10);
            this.btnEditJob.Margin = new System.Windows.Forms.Padding(2);
            this.btnEditJob.Name = "btnEditJob";
            this.btnEditJob.Size = new System.Drawing.Size(60, 23);
            this.btnEditJob.TabIndex = 5;
            this.btnEditJob.Text = "Edit";
            this.btnEditJob.UseVisualStyleBackColor = true;
            this.btnEditJob.Click += new System.EventHandler(this.btnEditJob_Click);
            // 
            // btnDeleteJob
            // 
            this.btnDeleteJob.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteJob.Location = new System.Drawing.Point(600, 10);
            this.btnDeleteJob.Margin = new System.Windows.Forms.Padding(2);
            this.btnDeleteJob.Name = "btnDeleteJob";
            this.btnDeleteJob.Size = new System.Drawing.Size(60, 23);
            this.btnDeleteJob.TabIndex = 6;
            this.btnDeleteJob.Text = "Delete";
            this.btnDeleteJob.UseVisualStyleBackColor = true;
            this.btnDeleteJob.Click += new System.EventHandler(this.btnDeleteJob_Click);
            // 
            // btnExportJob
            // 
            this.btnExportJob.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportJob.Location = new System.Drawing.Point(540, 10);
            this.btnExportJob.Margin = new System.Windows.Forms.Padding(2);
            this.btnExportJob.Name = "btnExportJob";
            this.btnExportJob.Size = new System.Drawing.Size(60, 23);
            this.btnExportJob.TabIndex = 7;
            this.btnExportJob.Text = "Export";
            this.btnExportJob.UseVisualStyleBackColor = true;
            this.btnExportJob.Click += new System.EventHandler(this.btnExportJob_Click);
            // 
            // btnImportJob
            // 
            this.btnImportJob.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImportJob.Location = new System.Drawing.Point(480, 10);
            this.btnImportJob.Margin = new System.Windows.Forms.Padding(2);
            this.btnImportJob.Name = "btnImportJob";
            this.btnImportJob.Size = new System.Drawing.Size(60, 23);
            this.btnImportJob.TabIndex = 8;
            this.btnImportJob.Text = "Import";
            this.btnImportJob.UseVisualStyleBackColor = true;
            this.btnImportJob.Click += new System.EventHandler(this.btnImportJob_Click);
            // 
            // lvJobDefns
            // 
            this.lvJobDefns.AllowColumnReorder = true;
            this.lvJobDefns.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvJobDefns.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvJobDefns.CheckBoxes = true;
            this.lvJobDefns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colJobName,
            this.colJobConfig,
            this.colJobConv,
            this.colJobCreated,
            this.colJobComment});
            this.lvJobDefns.FullRowSelect = true;
            this.lvJobDefns.Location = new System.Drawing.Point(2, 32);
            this.lvJobDefns.Margin = new System.Windows.Forms.Padding(2);
            this.lvJobDefns.MultiSelect = false;
            this.lvJobDefns.Name = "lvJobDefns";
            this.lvJobDefns.Size = new System.Drawing.Size(782, 327);
            this.lvJobDefns.TabIndex = 1;
            this.lvJobDefns.UseCompatibleStateImageBehavior = false;
            this.lvJobDefns.View = System.Windows.Forms.View.Details;
            this.lvJobDefns.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvJobDefns_ItemChecked);
            // 
            // colJobName
            // 
            this.colJobName.Text = "Job Name";
            this.colJobName.Width = 200;
            // 
            // colJobConfig
            // 
            this.colJobConfig.Text = "Configuration";
            this.colJobConfig.Width = 200;
            // 
            // colJobConv
            // 
            this.colJobConv.Text = "Conversion";
            this.colJobConv.Width = 275;
            // 
            // colJobCreated
            // 
            this.colJobCreated.Text = "Created";
            this.colJobCreated.Width = 225;
            // 
            // colJobComment
            // 
            this.colJobComment.Text = "Comment";
            this.colJobComment.Width = 143;
            // 
            // lblJobDefns
            // 
            this.lblJobDefns.AutoSize = true;
            this.lblJobDefns.Location = new System.Drawing.Point(5, 11);
            this.lblJobDefns.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblJobDefns.Name = "lblJobDefns";
            this.lblJobDefns.Size = new System.Drawing.Size(164, 18);
            this.lblJobDefns.TabIndex = 0;
            this.lblJobDefns.Text = "Existing Job Definitions:";
            // 
            // defineBatches
            // 
            this.defineBatches.Controls.Add(this.btnAddBatchDefn);
            this.defineBatches.Controls.Add(this.btnUpdBatchDefn);
            this.defineBatches.Controls.Add(this.btnDelBatchDefn);
            this.defineBatches.Controls.Add(this.btnExpBatchDefn);
            this.defineBatches.Controls.Add(this.btnImpBatchDefn);
            this.defineBatches.Controls.Add(this.lvBatchDefns);
            this.defineBatches.Controls.Add(this.lblBatchDefns);
            this.defineBatches.Location = new System.Drawing.Point(4, 27);
            this.defineBatches.Margin = new System.Windows.Forms.Padding(2);
            this.defineBatches.Name = "defineBatches";
            this.defineBatches.Padding = new System.Windows.Forms.Padding(2);
            this.defineBatches.Size = new System.Drawing.Size(792, 386);
            this.defineBatches.TabIndex = 1;
            this.defineBatches.Text = "Define Batches";
            this.defineBatches.UseVisualStyleBackColor = true;
            // 
            // btnAddBatchDefn
            // 
            this.btnAddBatchDefn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddBatchDefn.Location = new System.Drawing.Point(720, 10);
            this.btnAddBatchDefn.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddBatchDefn.Name = "btnAddBatchDefn";
            this.btnAddBatchDefn.Size = new System.Drawing.Size(56, 23);
            this.btnAddBatchDefn.TabIndex = 2;
            this.btnAddBatchDefn.Text = "Add";
            this.btnAddBatchDefn.UseVisualStyleBackColor = true;
            this.btnAddBatchDefn.Click += new System.EventHandler(this.btnAddBatchDefn_Click);
            // 
            // btnUpdBatchDefn
            // 
            this.btnUpdBatchDefn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdBatchDefn.Location = new System.Drawing.Point(660, 10);
            this.btnUpdBatchDefn.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdBatchDefn.Name = "btnUpdBatchDefn";
            this.btnUpdBatchDefn.Size = new System.Drawing.Size(60, 23);
            this.btnUpdBatchDefn.TabIndex = 3;
            this.btnUpdBatchDefn.Text = "Edit";
            this.btnUpdBatchDefn.UseVisualStyleBackColor = true;
            this.btnUpdBatchDefn.Click += new System.EventHandler(this.btnUpdBatchDefn_Click);
            // 
            // btnDelBatchDefn
            // 
            this.btnDelBatchDefn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelBatchDefn.Location = new System.Drawing.Point(600, 10);
            this.btnDelBatchDefn.Margin = new System.Windows.Forms.Padding(2);
            this.btnDelBatchDefn.Name = "btnDelBatchDefn";
            this.btnDelBatchDefn.Size = new System.Drawing.Size(60, 23);
            this.btnDelBatchDefn.TabIndex = 4;
            this.btnDelBatchDefn.Text = "Delete";
            this.btnDelBatchDefn.UseVisualStyleBackColor = true;
            this.btnDelBatchDefn.Click += new System.EventHandler(this.btnDelBatchDefn_Click);
            // 
            // btnExpBatchDefn
            // 
            this.btnExpBatchDefn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExpBatchDefn.Location = new System.Drawing.Point(540, 10);
            this.btnExpBatchDefn.Margin = new System.Windows.Forms.Padding(2);
            this.btnExpBatchDefn.Name = "btnExpBatchDefn";
            this.btnExpBatchDefn.Size = new System.Drawing.Size(60, 23);
            this.btnExpBatchDefn.TabIndex = 5;
            this.btnExpBatchDefn.Text = "Export";
            this.btnExpBatchDefn.UseVisualStyleBackColor = true;
            this.btnExpBatchDefn.Click += new System.EventHandler(this.btnExpBatchDefn_Click);
            // 
            // btnImpBatchDefn
            // 
            this.btnImpBatchDefn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImpBatchDefn.Location = new System.Drawing.Point(480, 10);
            this.btnImpBatchDefn.Margin = new System.Windows.Forms.Padding(2);
            this.btnImpBatchDefn.Name = "btnImpBatchDefn";
            this.btnImpBatchDefn.Size = new System.Drawing.Size(60, 23);
            this.btnImpBatchDefn.TabIndex = 6;
            this.btnImpBatchDefn.Text = "Import";
            this.btnImpBatchDefn.UseVisualStyleBackColor = true;
            this.btnImpBatchDefn.Click += new System.EventHandler(this.btnImpBatchDefn_Click);
            // 
            // lvBatchDefns
            // 
            this.lvBatchDefns.AllowColumnReorder = true;
            this.lvBatchDefns.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvBatchDefns.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvBatchDefns.CheckBoxes = true;
            this.lvBatchDefns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colBDName,
            this.colBDSize,
            this.colBDCondition,
            this.colBDSrcPrefix,
            this.colBDStagingPath});
            this.lvBatchDefns.FullRowSelect = true;
            this.lvBatchDefns.Location = new System.Drawing.Point(5, 32);
            this.lvBatchDefns.Margin = new System.Windows.Forms.Padding(2);
            this.lvBatchDefns.Name = "lvBatchDefns";
            this.lvBatchDefns.Size = new System.Drawing.Size(781, 327);
            this.lvBatchDefns.TabIndex = 1;
            this.lvBatchDefns.UseCompatibleStateImageBehavior = false;
            this.lvBatchDefns.View = System.Windows.Forms.View.Details;
            this.lvBatchDefns.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvBatchDefns_ItemChecked);
            // 
            // colBDName
            // 
            this.colBDName.Text = "Name";
            this.colBDName.Width = 200;
            // 
            // colBDSize
            // 
            this.colBDSize.Text = "Size";
            this.colBDSize.Width = 80;
            // 
            // colBDCondition
            // 
            this.colBDCondition.Text = "Condition";
            this.colBDCondition.Width = 200;
            // 
            // colBDSrcPrefix
            // 
            this.colBDSrcPrefix.Text = "Source Prefix";
            this.colBDSrcPrefix.Width = 250;
            // 
            // colBDStagingPath
            // 
            this.colBDStagingPath.Text = "Staging Path";
            this.colBDStagingPath.Width = 312;
            // 
            // lblBatchDefns
            // 
            this.lblBatchDefns.AutoSize = true;
            this.lblBatchDefns.Location = new System.Drawing.Point(5, 6);
            this.lblBatchDefns.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBatchDefns.Name = "lblBatchDefns";
            this.lblBatchDefns.Size = new System.Drawing.Size(175, 18);
            this.lblBatchDefns.TabIndex = 0;
            this.lblBatchDefns.Text = "Current Batch Definitions:";
            // 
            // pageLoad
            // 
            this.pageLoad.Controls.Add(this.tabLoad);
            this.pageLoad.Location = new System.Drawing.Point(4, 32);
            this.pageLoad.Margin = new System.Windows.Forms.Padding(2);
            this.pageLoad.Name = "pageLoad";
            this.pageLoad.Size = new System.Drawing.Size(792, 412);
            this.pageLoad.TabIndex = 2;
            this.pageLoad.Text = "Load";
            this.pageLoad.UseVisualStyleBackColor = true;
            // 
            // tabLoad
            // 
            this.tabLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabLoad.Controls.Add(this.JobLoad);
            this.tabLoad.Controls.Add(this.CaseLoad);
            this.tabLoad.Controls.Add(this.DocumentLoad);
            this.tabLoad.Controls.Add(this.AnnotationLoad);
            this.tabLoad.Controls.Add(this.TaskLoad);
            this.tabLoad.Location = new System.Drawing.Point(0, 0);
            this.tabLoad.Margin = new System.Windows.Forms.Padding(2);
            this.tabLoad.Name = "tabLoad";
            this.tabLoad.SelectedIndex = 0;
            this.tabLoad.Size = new System.Drawing.Size(800, 417);
            this.tabLoad.TabIndex = 0;
            this.tabLoad.SelectedIndexChanged += new System.EventHandler(this.tabLoad_SelectedIndexChanged);
            // 
            // JobLoad
            // 
            this.JobLoad.Controls.Add(this.lvLoadJobs);
            this.JobLoad.Controls.Add(this.lblJobs);
            this.JobLoad.Location = new System.Drawing.Point(4, 27);
            this.JobLoad.Margin = new System.Windows.Forms.Padding(2);
            this.JobLoad.Name = "JobLoad";
            this.JobLoad.Padding = new System.Windows.Forms.Padding(2);
            this.JobLoad.Size = new System.Drawing.Size(792, 386);
            this.JobLoad.TabIndex = 0;
            this.JobLoad.Text = "Choose Job";
            this.JobLoad.UseVisualStyleBackColor = true;
            // 
            // lvLoadJobs
            // 
            this.lvLoadJobs.AllowColumnReorder = true;
            this.lvLoadJobs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvLoadJobs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvLoadJobs.CheckBoxes = true;
            this.lvLoadJobs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colLoadJobID,
            this.colLoadJobName,
            this.colLoadComment});
            this.lvLoadJobs.FullRowSelect = true;
            this.lvLoadJobs.Location = new System.Drawing.Point(2, 27);
            this.lvLoadJobs.Margin = new System.Windows.Forms.Padding(2);
            this.lvLoadJobs.MultiSelect = false;
            this.lvLoadJobs.Name = "lvLoadJobs";
            this.lvLoadJobs.Size = new System.Drawing.Size(784, 355);
            this.lvLoadJobs.TabIndex = 1;
            this.lvLoadJobs.UseCompatibleStateImageBehavior = false;
            this.lvLoadJobs.View = System.Windows.Forms.View.Details;
            this.lvLoadJobs.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvLoadJobs_ItemChecked);
            // 
            // colLoadJobID
            // 
            this.colLoadJobID.Text = "Job ID";
            this.colLoadJobID.Width = 125;
            // 
            // colLoadJobName
            // 
            this.colLoadJobName.Text = "Job Name";
            this.colLoadJobName.Width = 250;
            // 
            // colLoadComment
            // 
            this.colLoadComment.Text = "Comment";
            this.colLoadComment.Width = 659;
            // 
            // lblJobs
            // 
            this.lblJobs.AutoSize = true;
            this.lblJobs.Location = new System.Drawing.Point(3, 6);
            this.lblJobs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblJobs.Name = "lblJobs";
            this.lblJobs.Size = new System.Drawing.Size(96, 18);
            this.lblJobs.TabIndex = 0;
            this.lblJobs.Text = "Existing Jobs:";
            // 
            // CaseLoad
            // 
            this.CaseLoad.Controls.Add(this.btnLoadCaseData);
            this.CaseLoad.Controls.Add(this.btnNextCaseData);
            this.CaseLoad.Controls.Add(this.btnPrevCaseData);
            this.CaseLoad.Controls.Add(this.btnFindCaseID);
            this.CaseLoad.Controls.Add(this.txtCaseID);
            this.CaseLoad.Controls.Add(this.lblCaseID);
            this.CaseLoad.Controls.Add(this.lblCaseDataCount);
            this.CaseLoad.Controls.Add(this.lvCaseData);
            this.CaseLoad.Controls.Add(this.lblCaseData);
            this.CaseLoad.Location = new System.Drawing.Point(4, 27);
            this.CaseLoad.Margin = new System.Windows.Forms.Padding(2);
            this.CaseLoad.Name = "CaseLoad";
            this.CaseLoad.Padding = new System.Windows.Forms.Padding(2);
            this.CaseLoad.Size = new System.Drawing.Size(792, 386);
            this.CaseLoad.TabIndex = 1;
            this.CaseLoad.Text = "Load Case Data";
            this.CaseLoad.UseVisualStyleBackColor = true;
            // 
            // btnLoadCaseData
            // 
            this.btnLoadCaseData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadCaseData.Location = new System.Drawing.Point(729, 371);
            this.btnLoadCaseData.Margin = new System.Windows.Forms.Padding(2);
            this.btnLoadCaseData.Name = "btnLoadCaseData";
            this.btnLoadCaseData.Size = new System.Drawing.Size(56, 23);
            this.btnLoadCaseData.TabIndex = 8;
            this.btnLoadCaseData.Text = "Load";
            this.btnLoadCaseData.UseVisualStyleBackColor = true;
            this.btnLoadCaseData.Click += new System.EventHandler(this.btnLoadCaseData_Click);
            // 
            // btnNextCaseData
            // 
            this.btnNextCaseData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNextCaseData.Location = new System.Drawing.Point(640, 371);
            this.btnNextCaseData.Margin = new System.Windows.Forms.Padding(2);
            this.btnNextCaseData.Name = "btnNextCaseData";
            this.btnNextCaseData.Size = new System.Drawing.Size(56, 23);
            this.btnNextCaseData.TabIndex = 7;
            this.btnNextCaseData.Text = "Next";
            this.btnNextCaseData.UseVisualStyleBackColor = true;
            this.btnNextCaseData.Click += new System.EventHandler(this.btnNextCaseData_Click);
            // 
            // btnPrevCaseData
            // 
            this.btnPrevCaseData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrevCaseData.Location = new System.Drawing.Point(580, 371);
            this.btnPrevCaseData.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrevCaseData.Name = "btnPrevCaseData";
            this.btnPrevCaseData.Size = new System.Drawing.Size(56, 23);
            this.btnPrevCaseData.TabIndex = 6;
            this.btnPrevCaseData.Text = "Prev";
            this.btnPrevCaseData.UseVisualStyleBackColor = true;
            this.btnPrevCaseData.Click += new System.EventHandler(this.btnPrevCaseData_Click);
            // 
            // btnFindCaseID
            // 
            this.btnFindCaseID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFindCaseID.Location = new System.Drawing.Point(474, 371);
            this.btnFindCaseID.Margin = new System.Windows.Forms.Padding(2);
            this.btnFindCaseID.Name = "btnFindCaseID";
            this.btnFindCaseID.Size = new System.Drawing.Size(56, 23);
            this.btnFindCaseID.TabIndex = 5;
            this.btnFindCaseID.Text = "Find";
            this.btnFindCaseID.UseVisualStyleBackColor = true;
            this.btnFindCaseID.Click += new System.EventHandler(this.btnFindCaseID_Click);
            // 
            // txtCaseID
            // 
            this.txtCaseID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCaseID.Location = new System.Drawing.Point(315, 371);
            this.txtCaseID.Margin = new System.Windows.Forms.Padding(2);
            this.txtCaseID.Name = "txtCaseID";
            this.txtCaseID.Size = new System.Drawing.Size(151, 23);
            this.txtCaseID.TabIndex = 4;
            // 
            // lblCaseID
            // 
            this.lblCaseID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCaseID.AutoSize = true;
            this.lblCaseID.Location = new System.Drawing.Point(210, 371);
            this.lblCaseID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCaseID.Name = "lblCaseID";
            this.lblCaseID.Size = new System.Drawing.Size(109, 18);
            this.lblCaseID.TabIndex = 3;
            this.lblCaseID.Text = "Case Account #:";
            // 
            // lblCaseDataCount
            // 
            this.lblCaseDataCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCaseDataCount.AutoSize = true;
            this.lblCaseDataCount.Location = new System.Drawing.Point(4, 371);
            this.lblCaseDataCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCaseDataCount.Name = "lblCaseDataCount";
            this.lblCaseDataCount.Size = new System.Drawing.Size(96, 18);
            this.lblCaseDataCount.TabIndex = 2;
            this.lblCaseDataCount.Text = "Total Records:";
            // 
            // lvCaseData
            // 
            this.lvCaseData.AllowColumnReorder = true;
            this.lvCaseData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvCaseData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvCaseData.Location = new System.Drawing.Point(5, 28);
            this.lvCaseData.Margin = new System.Windows.Forms.Padding(2);
            this.lvCaseData.MultiSelect = false;
            this.lvCaseData.Name = "lvCaseData";
            this.lvCaseData.Size = new System.Drawing.Size(784, 333);
            this.lvCaseData.TabIndex = 1;
            this.lvCaseData.UseCompatibleStateImageBehavior = false;
            this.lvCaseData.View = System.Windows.Forms.View.Details;
            // 
            // lblCaseData
            // 
            this.lblCaseData.AutoSize = true;
            this.lblCaseData.Location = new System.Drawing.Point(5, 6);
            this.lblCaseData.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCaseData.Name = "lblCaseData";
            this.lblCaseData.Size = new System.Drawing.Size(84, 18);
            this.lblCaseData.TabIndex = 0;
            this.lblCaseData.Text = "Table Name:";
            // 
            // DocumentLoad
            // 
            this.DocumentLoad.Controls.Add(this.lblDocumentDataCount);
            this.DocumentLoad.Controls.Add(this.btnLoadDocumentData);
            this.DocumentLoad.Controls.Add(this.btnNextDocumentData);
            this.DocumentLoad.Controls.Add(this.btnPrevDocumentData);
            this.DocumentLoad.Controls.Add(this.btnFindDocID);
            this.DocumentLoad.Controls.Add(this.txtDocID);
            this.DocumentLoad.Controls.Add(this.lblDocID);
            this.DocumentLoad.Controls.Add(this.lvDocumentData);
            this.DocumentLoad.Controls.Add(this.lblDocumentData);
            this.DocumentLoad.Location = new System.Drawing.Point(4, 27);
            this.DocumentLoad.Margin = new System.Windows.Forms.Padding(2);
            this.DocumentLoad.Name = "DocumentLoad";
            this.DocumentLoad.Size = new System.Drawing.Size(792, 386);
            this.DocumentLoad.TabIndex = 2;
            this.DocumentLoad.Text = "Load Document Data";
            this.DocumentLoad.UseVisualStyleBackColor = true;
            // 
            // lblDocumentDataCount
            // 
            this.lblDocumentDataCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDocumentDataCount.AutoSize = true;
            this.lblDocumentDataCount.Location = new System.Drawing.Point(2, 369);
            this.lblDocumentDataCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDocumentDataCount.Name = "lblDocumentDataCount";
            this.lblDocumentDataCount.Size = new System.Drawing.Size(96, 18);
            this.lblDocumentDataCount.TabIndex = 8;
            this.lblDocumentDataCount.Text = "Total Records:";
            // 
            // btnLoadDocumentData
            // 
            this.btnLoadDocumentData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadDocumentData.Location = new System.Drawing.Point(729, 369);
            this.btnLoadDocumentData.Margin = new System.Windows.Forms.Padding(2);
            this.btnLoadDocumentData.Name = "btnLoadDocumentData";
            this.btnLoadDocumentData.Size = new System.Drawing.Size(56, 23);
            this.btnLoadDocumentData.TabIndex = 7;
            this.btnLoadDocumentData.Text = "Load";
            this.btnLoadDocumentData.UseVisualStyleBackColor = true;
            this.btnLoadDocumentData.Click += new System.EventHandler(this.btnLoadDocumentData_Click);
            // 
            // btnNextDocumentData
            // 
            this.btnNextDocumentData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNextDocumentData.Location = new System.Drawing.Point(640, 369);
            this.btnNextDocumentData.Margin = new System.Windows.Forms.Padding(2);
            this.btnNextDocumentData.Name = "btnNextDocumentData";
            this.btnNextDocumentData.Size = new System.Drawing.Size(56, 23);
            this.btnNextDocumentData.TabIndex = 6;
            this.btnNextDocumentData.Text = "Next";
            this.btnNextDocumentData.UseVisualStyleBackColor = true;
            this.btnNextDocumentData.Click += new System.EventHandler(this.btnNextDocumentData_Click);
            // 
            // btnPrevDocumentData
            // 
            this.btnPrevDocumentData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrevDocumentData.Location = new System.Drawing.Point(580, 369);
            this.btnPrevDocumentData.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrevDocumentData.Name = "btnPrevDocumentData";
            this.btnPrevDocumentData.Size = new System.Drawing.Size(56, 23);
            this.btnPrevDocumentData.TabIndex = 5;
            this.btnPrevDocumentData.Text = "Prev";
            this.btnPrevDocumentData.UseVisualStyleBackColor = true;
            this.btnPrevDocumentData.Click += new System.EventHandler(this.btnPrevDocumentData_Click);
            // 
            // btnFindDocID
            // 
            this.btnFindDocID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFindDocID.Location = new System.Drawing.Point(474, 369);
            this.btnFindDocID.Margin = new System.Windows.Forms.Padding(2);
            this.btnFindDocID.Name = "btnFindDocID";
            this.btnFindDocID.Size = new System.Drawing.Size(56, 23);
            this.btnFindDocID.TabIndex = 4;
            this.btnFindDocID.Text = "Find";
            this.btnFindDocID.UseVisualStyleBackColor = true;
            this.btnFindDocID.Click += new System.EventHandler(this.btnFindDocID_Click);
            // 
            // txtDocID
            // 
            this.txtDocID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDocID.Location = new System.Drawing.Point(315, 369);
            this.txtDocID.Margin = new System.Windows.Forms.Padding(2);
            this.txtDocID.Name = "txtDocID";
            this.txtDocID.Size = new System.Drawing.Size(151, 23);
            this.txtDocID.TabIndex = 3;
            // 
            // lblDocID
            // 
            this.lblDocID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDocID.AutoSize = true;
            this.lblDocID.Location = new System.Drawing.Point(210, 369);
            this.lblDocID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDocID.Name = "lblDocID";
            this.lblDocID.Size = new System.Drawing.Size(109, 18);
            this.lblDocID.TabIndex = 2;
            this.lblDocID.Text = "Case Account #:";
            // 
            // lvDocumentData
            // 
            this.lvDocumentData.AllowColumnReorder = true;
            this.lvDocumentData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvDocumentData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvDocumentData.FullRowSelect = true;
            this.lvDocumentData.Location = new System.Drawing.Point(5, 28);
            this.lvDocumentData.Margin = new System.Windows.Forms.Padding(2);
            this.lvDocumentData.MultiSelect = false;
            this.lvDocumentData.Name = "lvDocumentData";
            this.lvDocumentData.Size = new System.Drawing.Size(784, 333);
            this.lvDocumentData.TabIndex = 1;
            this.lvDocumentData.UseCompatibleStateImageBehavior = false;
            this.lvDocumentData.View = System.Windows.Forms.View.Details;
            // 
            // lblDocumentData
            // 
            this.lblDocumentData.AutoSize = true;
            this.lblDocumentData.Location = new System.Drawing.Point(3, 3);
            this.lblDocumentData.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDocumentData.Name = "lblDocumentData";
            this.lblDocumentData.Size = new System.Drawing.Size(84, 18);
            this.lblDocumentData.TabIndex = 0;
            this.lblDocumentData.Text = "Table Name:";
            // 
            // AnnotationLoad
            // 
            this.AnnotationLoad.Controls.Add(this.btnLoadAnnotationData);
            this.AnnotationLoad.Controls.Add(this.btnNextAnnotationData);
            this.AnnotationLoad.Controls.Add(this.btnPrevAnnotationData);
            this.AnnotationLoad.Controls.Add(this.btnFindAnnID);
            this.AnnotationLoad.Controls.Add(this.txtAnnID);
            this.AnnotationLoad.Controls.Add(this.lblAnnID);
            this.AnnotationLoad.Controls.Add(this.lblAnnotationDataCount);
            this.AnnotationLoad.Controls.Add(this.lvAnnotationData);
            this.AnnotationLoad.Controls.Add(this.lblAnnotationData);
            this.AnnotationLoad.Location = new System.Drawing.Point(4, 27);
            this.AnnotationLoad.Margin = new System.Windows.Forms.Padding(2);
            this.AnnotationLoad.Name = "AnnotationLoad";
            this.AnnotationLoad.Size = new System.Drawing.Size(792, 386);
            this.AnnotationLoad.TabIndex = 3;
            this.AnnotationLoad.Text = "Load Annotation Data";
            this.AnnotationLoad.UseVisualStyleBackColor = true;
            // 
            // btnLoadAnnotationData
            // 
            this.btnLoadAnnotationData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadAnnotationData.Location = new System.Drawing.Point(729, 368);
            this.btnLoadAnnotationData.Margin = new System.Windows.Forms.Padding(2);
            this.btnLoadAnnotationData.Name = "btnLoadAnnotationData";
            this.btnLoadAnnotationData.Size = new System.Drawing.Size(56, 23);
            this.btnLoadAnnotationData.TabIndex = 8;
            this.btnLoadAnnotationData.Text = "Load";
            this.btnLoadAnnotationData.UseVisualStyleBackColor = true;
            this.btnLoadAnnotationData.Click += new System.EventHandler(this.btnLoadAnnotationData_Click);
            // 
            // btnNextAnnotationData
            // 
            this.btnNextAnnotationData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNextAnnotationData.Location = new System.Drawing.Point(640, 368);
            this.btnNextAnnotationData.Margin = new System.Windows.Forms.Padding(2);
            this.btnNextAnnotationData.Name = "btnNextAnnotationData";
            this.btnNextAnnotationData.Size = new System.Drawing.Size(56, 23);
            this.btnNextAnnotationData.TabIndex = 7;
            this.btnNextAnnotationData.Text = "Next";
            this.btnNextAnnotationData.UseVisualStyleBackColor = true;
            this.btnNextAnnotationData.Click += new System.EventHandler(this.btnNextAnnotationData_Click);
            // 
            // btnPrevAnnotationData
            // 
            this.btnPrevAnnotationData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrevAnnotationData.Location = new System.Drawing.Point(580, 368);
            this.btnPrevAnnotationData.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrevAnnotationData.Name = "btnPrevAnnotationData";
            this.btnPrevAnnotationData.Size = new System.Drawing.Size(56, 23);
            this.btnPrevAnnotationData.TabIndex = 6;
            this.btnPrevAnnotationData.Text = "Prev";
            this.btnPrevAnnotationData.UseVisualStyleBackColor = true;
            this.btnPrevAnnotationData.Click += new System.EventHandler(this.btnPrevAnnotationData_Click);
            // 
            // btnFindAnnID
            // 
            this.btnFindAnnID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFindAnnID.Location = new System.Drawing.Point(474, 368);
            this.btnFindAnnID.Margin = new System.Windows.Forms.Padding(2);
            this.btnFindAnnID.Name = "btnFindAnnID";
            this.btnFindAnnID.Size = new System.Drawing.Size(56, 23);
            this.btnFindAnnID.TabIndex = 5;
            this.btnFindAnnID.Text = "Find";
            this.btnFindAnnID.UseVisualStyleBackColor = true;
            this.btnFindAnnID.Click += new System.EventHandler(this.btnFindAnnID_Click);
            // 
            // txtAnnID
            // 
            this.txtAnnID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAnnID.Location = new System.Drawing.Point(315, 368);
            this.txtAnnID.Margin = new System.Windows.Forms.Padding(2);
            this.txtAnnID.Name = "txtAnnID";
            this.txtAnnID.Size = new System.Drawing.Size(151, 23);
            this.txtAnnID.TabIndex = 4;
            // 
            // lblAnnID
            // 
            this.lblAnnID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAnnID.AutoSize = true;
            this.lblAnnID.Location = new System.Drawing.Point(210, 368);
            this.lblAnnID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAnnID.Name = "lblAnnID";
            this.lblAnnID.Size = new System.Drawing.Size(109, 18);
            this.lblAnnID.TabIndex = 3;
            this.lblAnnID.Text = "Case Account #:";
            // 
            // lblAnnotationDataCount
            // 
            this.lblAnnotationDataCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblAnnotationDataCount.AutoSize = true;
            this.lblAnnotationDataCount.Location = new System.Drawing.Point(3, 368);
            this.lblAnnotationDataCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAnnotationDataCount.Name = "lblAnnotationDataCount";
            this.lblAnnotationDataCount.Size = new System.Drawing.Size(96, 18);
            this.lblAnnotationDataCount.TabIndex = 2;
            this.lblAnnotationDataCount.Text = "Total Records:";
            // 
            // lvAnnotationData
            // 
            this.lvAnnotationData.AllowColumnReorder = true;
            this.lvAnnotationData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvAnnotationData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvAnnotationData.FullRowSelect = true;
            this.lvAnnotationData.Location = new System.Drawing.Point(5, 28);
            this.lvAnnotationData.Margin = new System.Windows.Forms.Padding(2);
            this.lvAnnotationData.MultiSelect = false;
            this.lvAnnotationData.Name = "lvAnnotationData";
            this.lvAnnotationData.Size = new System.Drawing.Size(784, 333);
            this.lvAnnotationData.TabIndex = 1;
            this.lvAnnotationData.UseCompatibleStateImageBehavior = false;
            this.lvAnnotationData.View = System.Windows.Forms.View.Details;
            // 
            // lblAnnotationData
            // 
            this.lblAnnotationData.AutoSize = true;
            this.lblAnnotationData.Location = new System.Drawing.Point(3, 3);
            this.lblAnnotationData.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAnnotationData.Name = "lblAnnotationData";
            this.lblAnnotationData.Size = new System.Drawing.Size(84, 18);
            this.lblAnnotationData.TabIndex = 0;
            this.lblAnnotationData.Text = "Table Name:";
            // 
            // TaskLoad
            // 
            this.TaskLoad.Controls.Add(this.btnLoadTaskData);
            this.TaskLoad.Controls.Add(this.btnNextTaskData);
            this.TaskLoad.Controls.Add(this.btnPrevTaskData);
            this.TaskLoad.Controls.Add(this.btnFindTaskID);
            this.TaskLoad.Controls.Add(this.txtTaskID);
            this.TaskLoad.Controls.Add(this.lblTaskID);
            this.TaskLoad.Controls.Add(this.lblTaskDataCount);
            this.TaskLoad.Controls.Add(this.lvTaskData);
            this.TaskLoad.Controls.Add(this.lblTaskData);
            this.TaskLoad.Location = new System.Drawing.Point(4, 27);
            this.TaskLoad.Margin = new System.Windows.Forms.Padding(2);
            this.TaskLoad.Name = "TaskLoad";
            this.TaskLoad.Size = new System.Drawing.Size(792, 386);
            this.TaskLoad.TabIndex = 4;
            this.TaskLoad.Text = "Load Task Data";
            this.TaskLoad.UseVisualStyleBackColor = true;
            // 
            // btnLoadTaskData
            // 
            this.btnLoadTaskData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadTaskData.Location = new System.Drawing.Point(729, 368);
            this.btnLoadTaskData.Margin = new System.Windows.Forms.Padding(2);
            this.btnLoadTaskData.Name = "btnLoadTaskData";
            this.btnLoadTaskData.Size = new System.Drawing.Size(56, 23);
            this.btnLoadTaskData.TabIndex = 8;
            this.btnLoadTaskData.Text = "Load";
            this.btnLoadTaskData.UseVisualStyleBackColor = true;
            this.btnLoadTaskData.Click += new System.EventHandler(this.btnLoadTaskData_Click);
            // 
            // btnNextTaskData
            // 
            this.btnNextTaskData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNextTaskData.Location = new System.Drawing.Point(640, 368);
            this.btnNextTaskData.Margin = new System.Windows.Forms.Padding(2);
            this.btnNextTaskData.Name = "btnNextTaskData";
            this.btnNextTaskData.Size = new System.Drawing.Size(56, 23);
            this.btnNextTaskData.TabIndex = 7;
            this.btnNextTaskData.Text = "Next";
            this.btnNextTaskData.UseVisualStyleBackColor = true;
            this.btnNextTaskData.Click += new System.EventHandler(this.btnNextTaskData_Click);
            // 
            // btnPrevTaskData
            // 
            this.btnPrevTaskData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrevTaskData.Location = new System.Drawing.Point(580, 368);
            this.btnPrevTaskData.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrevTaskData.Name = "btnPrevTaskData";
            this.btnPrevTaskData.Size = new System.Drawing.Size(56, 23);
            this.btnPrevTaskData.TabIndex = 6;
            this.btnPrevTaskData.Text = "Prev";
            this.btnPrevTaskData.UseVisualStyleBackColor = true;
            this.btnPrevTaskData.Click += new System.EventHandler(this.btnPrevTaskData_Click);
            // 
            // btnFindTaskID
            // 
            this.btnFindTaskID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFindTaskID.Location = new System.Drawing.Point(474, 368);
            this.btnFindTaskID.Margin = new System.Windows.Forms.Padding(2);
            this.btnFindTaskID.Name = "btnFindTaskID";
            this.btnFindTaskID.Size = new System.Drawing.Size(56, 23);
            this.btnFindTaskID.TabIndex = 5;
            this.btnFindTaskID.Text = "Find";
            this.btnFindTaskID.UseVisualStyleBackColor = true;
            this.btnFindTaskID.Click += new System.EventHandler(this.btnFindTaskID_Click);
            // 
            // txtTaskID
            // 
            this.txtTaskID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTaskID.Location = new System.Drawing.Point(315, 368);
            this.txtTaskID.Margin = new System.Windows.Forms.Padding(2);
            this.txtTaskID.Name = "txtTaskID";
            this.txtTaskID.Size = new System.Drawing.Size(151, 23);
            this.txtTaskID.TabIndex = 4;
            // 
            // lblTaskID
            // 
            this.lblTaskID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTaskID.AutoSize = true;
            this.lblTaskID.Location = new System.Drawing.Point(210, 368);
            this.lblTaskID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTaskID.Name = "lblTaskID";
            this.lblTaskID.Size = new System.Drawing.Size(109, 18);
            this.lblTaskID.TabIndex = 3;
            this.lblTaskID.Text = "Case Account #:";
            // 
            // lblTaskDataCount
            // 
            this.lblTaskDataCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTaskDataCount.AutoSize = true;
            this.lblTaskDataCount.Location = new System.Drawing.Point(3, 368);
            this.lblTaskDataCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTaskDataCount.Name = "lblTaskDataCount";
            this.lblTaskDataCount.Size = new System.Drawing.Size(96, 18);
            this.lblTaskDataCount.TabIndex = 2;
            this.lblTaskDataCount.Text = "Total Records:";
            // 
            // lvTaskData
            // 
            this.lvTaskData.AllowColumnReorder = true;
            this.lvTaskData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTaskData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvTaskData.FullRowSelect = true;
            this.lvTaskData.Location = new System.Drawing.Point(5, 28);
            this.lvTaskData.Margin = new System.Windows.Forms.Padding(2);
            this.lvTaskData.MultiSelect = false;
            this.lvTaskData.Name = "lvTaskData";
            this.lvTaskData.Size = new System.Drawing.Size(784, 333);
            this.lvTaskData.TabIndex = 1;
            this.lvTaskData.UseCompatibleStateImageBehavior = false;
            this.lvTaskData.View = System.Windows.Forms.View.Details;
            // 
            // lblTaskData
            // 
            this.lblTaskData.AutoSize = true;
            this.lblTaskData.Location = new System.Drawing.Point(3, 11);
            this.lblTaskData.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTaskData.Name = "lblTaskData";
            this.lblTaskData.Size = new System.Drawing.Size(84, 18);
            this.lblTaskData.TabIndex = 0;
            this.lblTaskData.Text = "Table Name:";
            // 
            // pagePrepare
            // 
            this.pagePrepare.Controls.Add(this.tabPrepare);
            this.pagePrepare.Location = new System.Drawing.Point(4, 32);
            this.pagePrepare.Margin = new System.Windows.Forms.Padding(2);
            this.pagePrepare.Name = "pagePrepare";
            this.pagePrepare.Size = new System.Drawing.Size(792, 412);
            this.pagePrepare.TabIndex = 3;
            this.pagePrepare.Text = "Prepare";
            this.pagePrepare.UseVisualStyleBackColor = true;
            // 
            // tabPrepare
            // 
            this.tabPrepare.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabPrepare.Controls.Add(this.ChooseJob);
            this.tabPrepare.Controls.Add(this.ChooseBatchDefn);
            this.tabPrepare.Controls.Add(this.PerformConversion);
            this.tabPrepare.Controls.Add(this.ConvLogs);
            this.tabPrepare.Controls.Add(this.ConvResults);
            this.tabPrepare.Location = new System.Drawing.Point(0, 0);
            this.tabPrepare.Margin = new System.Windows.Forms.Padding(2);
            this.tabPrepare.Name = "tabPrepare";
            this.tabPrepare.SelectedIndex = 0;
            this.tabPrepare.Size = new System.Drawing.Size(800, 417);
            this.tabPrepare.TabIndex = 0;
            this.tabPrepare.SelectedIndexChanged += new System.EventHandler(this.tabPrepare_SelectedIndexChanged);
            // 
            // ChooseJob
            // 
            this.ChooseJob.Controls.Add(this.listJobs);
            this.ChooseJob.Controls.Add(this.lblConvJobs);
            this.ChooseJob.Location = new System.Drawing.Point(4, 27);
            this.ChooseJob.Margin = new System.Windows.Forms.Padding(2);
            this.ChooseJob.Name = "ChooseJob";
            this.ChooseJob.Padding = new System.Windows.Forms.Padding(2);
            this.ChooseJob.Size = new System.Drawing.Size(792, 386);
            this.ChooseJob.TabIndex = 0;
            this.ChooseJob.Text = "Choose Job";
            this.ChooseJob.UseVisualStyleBackColor = true;
            // 
            // listJobs
            // 
            this.listJobs.AllowColumnReorder = true;
            this.listJobs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listJobs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listJobs.CheckBoxes = true;
            this.listJobs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colConvJobID,
            this.colConvJobName,
            this.colConvComment});
            this.listJobs.FullRowSelect = true;
            this.listJobs.Location = new System.Drawing.Point(8, 28);
            this.listJobs.Margin = new System.Windows.Forms.Padding(2);
            this.listJobs.MultiSelect = false;
            this.listJobs.Name = "listJobs";
            this.listJobs.Size = new System.Drawing.Size(778, 355);
            this.listJobs.TabIndex = 1;
            this.listJobs.UseCompatibleStateImageBehavior = false;
            this.listJobs.View = System.Windows.Forms.View.Details;
            this.listJobs.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listJobs_ItemChecked);
            // 
            // colConvJobID
            // 
            this.colConvJobID.Text = "Job ID";
            this.colConvJobID.Width = 125;
            // 
            // colConvJobName
            // 
            this.colConvJobName.Text = "Job Name";
            this.colConvJobName.Width = 250;
            // 
            // colConvComment
            // 
            this.colConvComment.Text = "Comment";
            this.colConvComment.Width = 658;
            // 
            // lblConvJobs
            // 
            this.lblConvJobs.AutoSize = true;
            this.lblConvJobs.Location = new System.Drawing.Point(5, 6);
            this.lblConvJobs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblConvJobs.Name = "lblConvJobs";
            this.lblConvJobs.Size = new System.Drawing.Size(96, 18);
            this.lblConvJobs.TabIndex = 0;
            this.lblConvJobs.Text = "Existing Jobs:";
            // 
            // ChooseBatchDefn
            // 
            this.ChooseBatchDefn.Controls.Add(this.listBatchDefns);
            this.ChooseBatchDefn.Controls.Add(this.lblConvBatchDefns);
            this.ChooseBatchDefn.Location = new System.Drawing.Point(4, 27);
            this.ChooseBatchDefn.Margin = new System.Windows.Forms.Padding(2);
            this.ChooseBatchDefn.Name = "ChooseBatchDefn";
            this.ChooseBatchDefn.Padding = new System.Windows.Forms.Padding(2);
            this.ChooseBatchDefn.Size = new System.Drawing.Size(792, 386);
            this.ChooseBatchDefn.TabIndex = 1;
            this.ChooseBatchDefn.Text = "Choose Batch Defn";
            this.ChooseBatchDefn.UseVisualStyleBackColor = true;
            // 
            // listBatchDefns
            // 
            this.listBatchDefns.AllowColumnReorder = true;
            this.listBatchDefns.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBatchDefns.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBatchDefns.CheckBoxes = true;
            this.listBatchDefns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colConvBatchDefnName,
            this.colConvBatchSize,
            this.colConvCondition,
            this.colConvSrcPrefix,
            this.colConvStagingPath});
            this.listBatchDefns.FullRowSelect = true;
            this.listBatchDefns.Location = new System.Drawing.Point(8, 28);
            this.listBatchDefns.Margin = new System.Windows.Forms.Padding(2);
            this.listBatchDefns.MultiSelect = false;
            this.listBatchDefns.Name = "listBatchDefns";
            this.listBatchDefns.Size = new System.Drawing.Size(778, 365);
            this.listBatchDefns.TabIndex = 1;
            this.listBatchDefns.UseCompatibleStateImageBehavior = false;
            this.listBatchDefns.View = System.Windows.Forms.View.Details;
            this.listBatchDefns.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listBatchDefns_ItemChecked);
            // 
            // colConvBatchDefnName
            // 
            this.colConvBatchDefnName.Text = "Name";
            this.colConvBatchDefnName.Width = 200;
            // 
            // colConvBatchSize
            // 
            this.colConvBatchSize.Text = "Size";
            this.colConvBatchSize.Width = 80;
            // 
            // colConvCondition
            // 
            this.colConvCondition.Text = "Condition";
            this.colConvCondition.Width = 200;
            // 
            // colConvSrcPrefix
            // 
            this.colConvSrcPrefix.Text = "Source Prefix";
            this.colConvSrcPrefix.Width = 300;
            // 
            // colConvStagingPath
            // 
            this.colConvStagingPath.Text = "Staging Path";
            this.colConvStagingPath.Width = 253;
            // 
            // lblConvBatchDefns
            // 
            this.lblConvBatchDefns.AutoSize = true;
            this.lblConvBatchDefns.Location = new System.Drawing.Point(5, 6);
            this.lblConvBatchDefns.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblConvBatchDefns.Name = "lblConvBatchDefns";
            this.lblConvBatchDefns.Size = new System.Drawing.Size(177, 18);
            this.lblConvBatchDefns.TabIndex = 0;
            this.lblConvBatchDefns.Text = "Existing Batch Definitions:";
            // 
            // PerformConversion
            // 
            this.PerformConversion.Controls.Add(this.txtMaxBatches);
            this.PerformConversion.Controls.Add(this.lblMaxBatches);
            this.PerformConversion.Controls.Add(this.convStatus);
            this.PerformConversion.Controls.Add(this.btnConvert);
            this.PerformConversion.Controls.Add(this.btnConvCancel);
            this.PerformConversion.Controls.Add(this.lblConvBatch);
            this.PerformConversion.Controls.Add(this.lblConvJob);
            this.PerformConversion.Location = new System.Drawing.Point(4, 27);
            this.PerformConversion.Margin = new System.Windows.Forms.Padding(2);
            this.PerformConversion.Name = "PerformConversion";
            this.PerformConversion.Size = new System.Drawing.Size(792, 386);
            this.PerformConversion.TabIndex = 2;
            this.PerformConversion.Text = "Perform Conversion";
            this.PerformConversion.UseVisualStyleBackColor = true;
            // 
            // txtMaxBatches
            // 
            this.txtMaxBatches.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtMaxBatches.Location = new System.Drawing.Point(165, 371);
            this.txtMaxBatches.Margin = new System.Windows.Forms.Padding(2);
            this.txtMaxBatches.Name = "txtMaxBatches";
            this.txtMaxBatches.Size = new System.Drawing.Size(76, 23);
            this.txtMaxBatches.TabIndex = 7;
            this.txtMaxBatches.Text = "-1";
            // 
            // lblMaxBatches
            // 
            this.lblMaxBatches.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblMaxBatches.AutoSize = true;
            this.lblMaxBatches.Location = new System.Drawing.Point(5, 371);
            this.lblMaxBatches.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMaxBatches.Name = "lblMaxBatches";
            this.lblMaxBatches.Size = new System.Drawing.Size(164, 18);
            this.lblMaxBatches.TabIndex = 6;
            this.lblMaxBatches.Text = "Max Batches To Convert:\r\n";
            // 
            // convStatus
            // 
            this.convStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.convStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.convStatus.FormattingEnabled = true;
            this.convStatus.HorizontalScrollbar = true;
            this.convStatus.ItemHeight = 18;
            this.convStatus.Location = new System.Drawing.Point(5, 61);
            this.convStatus.Margin = new System.Windows.Forms.Padding(2);
            this.convStatus.Name = "convStatus";
            this.convStatus.Size = new System.Drawing.Size(786, 270);
            this.convStatus.TabIndex = 5;
            // 
            // btnConvert
            // 
            this.btnConvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConvert.Location = new System.Drawing.Point(705, 369);
            this.btnConvert.Margin = new System.Windows.Forms.Padding(2);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(75, 23);
            this.btnConvert.TabIndex = 4;
            this.btnConvert.Text = "Convert";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // btnConvCancel
            // 
            this.btnConvCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConvCancel.Enabled = false;
            this.btnConvCancel.Location = new System.Drawing.Point(619, 369);
            this.btnConvCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnConvCancel.Name = "btnConvCancel";
            this.btnConvCancel.Size = new System.Drawing.Size(75, 23);
            this.btnConvCancel.TabIndex = 3;
            this.btnConvCancel.Text = "Cancel";
            this.btnConvCancel.UseVisualStyleBackColor = true;
            this.btnConvCancel.Click += new System.EventHandler(this.btnConvCancel_Click);
            // 
            // lblConvBatch
            // 
            this.lblConvBatch.AutoSize = true;
            this.lblConvBatch.Location = new System.Drawing.Point(4, 41);
            this.lblConvBatch.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblConvBatch.Name = "lblConvBatch";
            this.lblConvBatch.Size = new System.Drawing.Size(49, 18);
            this.lblConvBatch.TabIndex = 1;
            this.lblConvBatch.Text = "Batch:";
            // 
            // lblConvJob
            // 
            this.lblConvJob.AutoSize = true;
            this.lblConvJob.Location = new System.Drawing.Point(4, 16);
            this.lblConvJob.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblConvJob.Name = "lblConvJob";
            this.lblConvJob.Size = new System.Drawing.Size(36, 18);
            this.lblConvJob.TabIndex = 0;
            this.lblConvJob.Text = "Job:";
            // 
            // ConvLogs
            // 
            this.ConvLogs.Controls.Add(this.tabConvLog);
            this.ConvLogs.Location = new System.Drawing.Point(4, 27);
            this.ConvLogs.Margin = new System.Windows.Forms.Padding(2);
            this.ConvLogs.Name = "ConvLogs";
            this.ConvLogs.Size = new System.Drawing.Size(792, 386);
            this.ConvLogs.TabIndex = 3;
            this.ConvLogs.Text = "View Logs";
            this.ConvLogs.UseVisualStyleBackColor = true;
            // 
            // tabConvLog
            // 
            this.tabConvLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabConvLog.Controls.Add(this.ConvResultSession);
            this.tabConvLog.Controls.Add(this.ConvResultBatch);
            this.tabConvLog.Controls.Add(this.ConvResultView);
            this.tabConvLog.Controls.Add(this.ConvErrorView);
            this.tabConvLog.Location = new System.Drawing.Point(0, 0);
            this.tabConvLog.Margin = new System.Windows.Forms.Padding(2);
            this.tabConvLog.Name = "tabConvLog";
            this.tabConvLog.SelectedIndex = 0;
            this.tabConvLog.Size = new System.Drawing.Size(800, 439);
            this.tabConvLog.TabIndex = 0;
            this.tabConvLog.SelectedIndexChanged += new System.EventHandler(this.tabConvLog_SelectedIndexChanged);
            // 
            // ConvResultSession
            // 
            this.ConvResultSession.Controls.Add(this.listConvSessions);
            this.ConvResultSession.Controls.Add(this.lblConvSessions);
            this.ConvResultSession.Location = new System.Drawing.Point(4, 27);
            this.ConvResultSession.Margin = new System.Windows.Forms.Padding(2);
            this.ConvResultSession.Name = "ConvResultSession";
            this.ConvResultSession.Padding = new System.Windows.Forms.Padding(2);
            this.ConvResultSession.Size = new System.Drawing.Size(792, 408);
            this.ConvResultSession.TabIndex = 0;
            this.ConvResultSession.Text = "Choose Conversion Session";
            this.ConvResultSession.UseVisualStyleBackColor = true;
            // 
            // listConvSessions
            // 
            this.listConvSessions.AllowColumnReorder = true;
            this.listConvSessions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listConvSessions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listConvSessions.CheckBoxes = true;
            this.listConvSessions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colConvSessionID,
            this.colConvStart,
            this.colConvEnd,
            this.colConvUser,
            this.colConvWkstn,
            this.colConvStatus});
            this.listConvSessions.FullRowSelect = true;
            this.listConvSessions.Location = new System.Drawing.Point(4, 28);
            this.listConvSessions.Margin = new System.Windows.Forms.Padding(2);
            this.listConvSessions.MultiSelect = false;
            this.listConvSessions.Name = "listConvSessions";
            this.listConvSessions.Size = new System.Drawing.Size(778, 338);
            this.listConvSessions.TabIndex = 1;
            this.listConvSessions.UseCompatibleStateImageBehavior = false;
            this.listConvSessions.View = System.Windows.Forms.View.Details;
            this.listConvSessions.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listConvSessions_ItemChecked);
            // 
            // colConvSessionID
            // 
            this.colConvSessionID.Text = "Session ID";
            this.colConvSessionID.Width = 100;
            // 
            // colConvStart
            // 
            this.colConvStart.Text = "Start Time";
            this.colConvStart.Width = 200;
            // 
            // colConvEnd
            // 
            this.colConvEnd.Text = "End Time";
            this.colConvEnd.Width = 200;
            // 
            // colConvUser
            // 
            this.colConvUser.Text = "User";
            this.colConvUser.Width = 150;
            // 
            // colConvWkstn
            // 
            this.colConvWkstn.Text = "Workstation";
            this.colConvWkstn.Width = 150;
            // 
            // colConvStatus
            // 
            this.colConvStatus.Text = "Status";
            this.colConvStatus.Width = 237;
            // 
            // lblConvSessions
            // 
            this.lblConvSessions.AutoSize = true;
            this.lblConvSessions.Location = new System.Drawing.Point(5, 6);
            this.lblConvSessions.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblConvSessions.Name = "lblConvSessions";
            this.lblConvSessions.Size = new System.Drawing.Size(194, 18);
            this.lblConvSessions.TabIndex = 0;
            this.lblConvSessions.Text = "Existing Conversion Sessions:";
            // 
            // ConvResultBatch
            // 
            this.ConvResultBatch.Controls.Add(this.listConvBatches);
            this.ConvResultBatch.Controls.Add(this.lblConvBatches);
            this.ConvResultBatch.Location = new System.Drawing.Point(4, 27);
            this.ConvResultBatch.Margin = new System.Windows.Forms.Padding(2);
            this.ConvResultBatch.Name = "ConvResultBatch";
            this.ConvResultBatch.Padding = new System.Windows.Forms.Padding(2);
            this.ConvResultBatch.Size = new System.Drawing.Size(792, 408);
            this.ConvResultBatch.TabIndex = 1;
            this.ConvResultBatch.Text = "Choose Conversion Batch";
            this.ConvResultBatch.UseVisualStyleBackColor = true;
            // 
            // listConvBatches
            // 
            this.listConvBatches.AllowColumnReorder = true;
            this.listConvBatches.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listConvBatches.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listConvBatches.CheckBoxes = true;
            this.listConvBatches.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.convLogBatchID,
            this.convBatchLogSize,
            this.convLogBatchStatus,
            this.convLogBatchStart,
            this.convLogBatchEnd,
            this.convLogBatchUser,
            this.convLogBatchWkstn});
            this.listConvBatches.FullRowSelect = true;
            this.listConvBatches.Location = new System.Drawing.Point(5, 28);
            this.listConvBatches.Margin = new System.Windows.Forms.Padding(2);
            this.listConvBatches.MultiSelect = false;
            this.listConvBatches.Name = "listConvBatches";
            this.listConvBatches.Size = new System.Drawing.Size(777, 338);
            this.listConvBatches.TabIndex = 1;
            this.listConvBatches.UseCompatibleStateImageBehavior = false;
            this.listConvBatches.View = System.Windows.Forms.View.Details;
            this.listConvBatches.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listConvBatches_ItemChecked);
            // 
            // convLogBatchID
            // 
            this.convLogBatchID.Text = "Batch ID";
            this.convLogBatchID.Width = 110;
            // 
            // convBatchLogSize
            // 
            this.convBatchLogSize.Text = "Size";
            this.convBatchLogSize.Width = 95;
            // 
            // convLogBatchStatus
            // 
            this.convLogBatchStatus.Text = "Status";
            this.convLogBatchStatus.Width = 150;
            // 
            // convLogBatchStart
            // 
            this.convLogBatchStart.Text = "Start Time";
            this.convLogBatchStart.Width = 200;
            // 
            // convLogBatchEnd
            // 
            this.convLogBatchEnd.Text = "End Time";
            this.convLogBatchEnd.Width = 200;
            // 
            // convLogBatchUser
            // 
            this.convLogBatchUser.Text = "User";
            this.convLogBatchUser.Width = 150;
            // 
            // convLogBatchWkstn
            // 
            this.convLogBatchWkstn.Text = "WorkStation";
            this.convLogBatchWkstn.Width = 106;
            // 
            // lblConvBatches
            // 
            this.lblConvBatches.AutoSize = true;
            this.lblConvBatches.Location = new System.Drawing.Point(5, 6);
            this.lblConvBatches.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblConvBatches.Name = "lblConvBatches";
            this.lblConvBatches.Size = new System.Drawing.Size(210, 18);
            this.lblConvBatches.TabIndex = 0;
            this.lblConvBatches.Text = "Conversion Batches for Session:";
            // 
            // ConvResultView
            // 
            this.ConvResultView.Controls.Add(this.btnNextConvLog);
            this.ConvResultView.Controls.Add(this.btnPrevConvLog);
            this.ConvResultView.Controls.Add(this.lblConvLogCount);
            this.ConvResultView.Controls.Add(this.listConvBatchLog);
            this.ConvResultView.Controls.Add(this.lblConvBatchLog);
            this.ConvResultView.Location = new System.Drawing.Point(4, 27);
            this.ConvResultView.Margin = new System.Windows.Forms.Padding(2);
            this.ConvResultView.Name = "ConvResultView";
            this.ConvResultView.Size = new System.Drawing.Size(792, 408);
            this.ConvResultView.TabIndex = 2;
            this.ConvResultView.Text = "View Batch Log";
            this.ConvResultView.UseVisualStyleBackColor = true;
            // 
            // btnNextConvLog
            // 
            this.btnNextConvLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNextConvLog.Location = new System.Drawing.Point(729, 343);
            this.btnNextConvLog.Margin = new System.Windows.Forms.Padding(2);
            this.btnNextConvLog.Name = "btnNextConvLog";
            this.btnNextConvLog.Size = new System.Drawing.Size(56, 23);
            this.btnNextConvLog.TabIndex = 4;
            this.btnNextConvLog.Text = "Next";
            this.btnNextConvLog.UseVisualStyleBackColor = true;
            this.btnNextConvLog.Click += new System.EventHandler(this.btnNextConvLog_Click);
            // 
            // btnPrevConvLog
            // 
            this.btnPrevConvLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrevConvLog.Location = new System.Drawing.Point(668, 343);
            this.btnPrevConvLog.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrevConvLog.Name = "btnPrevConvLog";
            this.btnPrevConvLog.Size = new System.Drawing.Size(56, 23);
            this.btnPrevConvLog.TabIndex = 3;
            this.btnPrevConvLog.Text = "Prev";
            this.btnPrevConvLog.UseVisualStyleBackColor = true;
            this.btnPrevConvLog.Click += new System.EventHandler(this.btnPrevConvLog_Click);
            // 
            // lblConvLogCount
            // 
            this.lblConvLogCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConvLogCount.AutoSize = true;
            this.lblConvLogCount.Location = new System.Drawing.Point(2, 343);
            this.lblConvLogCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblConvLogCount.Name = "lblConvLogCount";
            this.lblConvLogCount.Size = new System.Drawing.Size(96, 18);
            this.lblConvLogCount.TabIndex = 2;
            this.lblConvLogCount.Text = "Total Records:";
            // 
            // listConvBatchLog
            // 
            this.listConvBatchLog.AllowColumnReorder = true;
            this.listConvBatchLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listConvBatchLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listConvBatchLog.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colCBLogTime,
            this.colCBLogEvent,
            this.colCBEventDesc});
            this.listConvBatchLog.FullRowSelect = true;
            this.listConvBatchLog.Location = new System.Drawing.Point(6, 25);
            this.listConvBatchLog.Margin = new System.Windows.Forms.Padding(2);
            this.listConvBatchLog.MultiSelect = false;
            this.listConvBatchLog.Name = "listConvBatchLog";
            this.listConvBatchLog.Size = new System.Drawing.Size(776, 313);
            this.listConvBatchLog.TabIndex = 1;
            this.listConvBatchLog.UseCompatibleStateImageBehavior = false;
            this.listConvBatchLog.View = System.Windows.Forms.View.Details;
            // 
            // colCBLogTime
            // 
            this.colCBLogTime.Text = "Time Occurred";
            this.colCBLogTime.Width = 200;
            // 
            // colCBLogEvent
            // 
            this.colCBLogEvent.Text = "Event";
            this.colCBLogEvent.Width = 80;
            // 
            // colCBEventDesc
            // 
            this.colCBEventDesc.Text = "Event Description";
            this.colCBEventDesc.Width = 748;
            // 
            // lblConvBatchLog
            // 
            this.lblConvBatchLog.AutoSize = true;
            this.lblConvBatchLog.Location = new System.Drawing.Point(3, 3);
            this.lblConvBatchLog.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblConvBatchLog.Name = "lblConvBatchLog";
            this.lblConvBatchLog.Size = new System.Drawing.Size(135, 18);
            this.lblConvBatchLog.TabIndex = 0;
            this.lblConvBatchLog.Text = "Existing Batch Logs:";
            // 
            // ConvErrorView
            // 
            this.ConvErrorView.Controls.Add(this.listConvBatchErrors);
            this.ConvErrorView.Controls.Add(this.lblConvBatchErrors);
            this.ConvErrorView.Location = new System.Drawing.Point(4, 27);
            this.ConvErrorView.Margin = new System.Windows.Forms.Padding(2);
            this.ConvErrorView.Name = "ConvErrorView";
            this.ConvErrorView.Size = new System.Drawing.Size(792, 408);
            this.ConvErrorView.TabIndex = 3;
            this.ConvErrorView.Text = "View Batch Errors";
            this.ConvErrorView.UseVisualStyleBackColor = true;
            // 
            // listConvBatchErrors
            // 
            this.listConvBatchErrors.AllowColumnReorder = true;
            this.listConvBatchErrors.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listConvBatchErrors.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listConvBatchErrors.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.convErrorTime,
            this.convErrorEvent});
            this.listConvBatchErrors.Location = new System.Drawing.Point(6, 25);
            this.listConvBatchErrors.Margin = new System.Windows.Forms.Padding(2);
            this.listConvBatchErrors.MultiSelect = false;
            this.listConvBatchErrors.Name = "listConvBatchErrors";
            this.listConvBatchErrors.Size = new System.Drawing.Size(776, 341);
            this.listConvBatchErrors.TabIndex = 1;
            this.listConvBatchErrors.UseCompatibleStateImageBehavior = false;
            this.listConvBatchErrors.View = System.Windows.Forms.View.Details;
            // 
            // convErrorTime
            // 
            this.convErrorTime.Text = "Time Occurred";
            this.convErrorTime.Width = 200;
            // 
            // convErrorEvent
            // 
            this.convErrorEvent.Text = "Event Description";
            this.convErrorEvent.Width = 828;
            // 
            // lblConvBatchErrors
            // 
            this.lblConvBatchErrors.AutoSize = true;
            this.lblConvBatchErrors.Location = new System.Drawing.Point(3, 3);
            this.lblConvBatchErrors.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblConvBatchErrors.Name = "lblConvBatchErrors";
            this.lblConvBatchErrors.Size = new System.Drawing.Size(144, 18);
            this.lblConvBatchErrors.TabIndex = 0;
            this.lblConvBatchErrors.Text = "Existing Batch Errors:";
            // 
            // ConvResults
            // 
            this.ConvResults.Controls.Add(this.btnConvResultDocID);
            this.ConvResults.Controls.Add(this.txtConvResultDocID);
            this.ConvResults.Controls.Add(this.lblConvResultDocID);
            this.ConvResults.Controls.Add(this.btnConvResultCaseID);
            this.ConvResults.Controls.Add(this.txtConvResultCaseID);
            this.ConvResults.Controls.Add(this.lblConvResultCaseID);
            this.ConvResults.Controls.Add(this.txtConvResultBatch);
            this.ConvResults.Controls.Add(this.lblConvResultBatch);
            this.ConvResults.Controls.Add(this.treeConvResult);
            this.ConvResults.Location = new System.Drawing.Point(4, 27);
            this.ConvResults.Margin = new System.Windows.Forms.Padding(2);
            this.ConvResults.Name = "ConvResults";
            this.ConvResults.Size = new System.Drawing.Size(792, 386);
            this.ConvResults.TabIndex = 4;
            this.ConvResults.Text = "View Results";
            this.ConvResults.UseVisualStyleBackColor = true;
            // 
            // btnConvResultDocID
            // 
            this.btnConvResultDocID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConvResultDocID.Location = new System.Drawing.Point(728, 368);
            this.btnConvResultDocID.Margin = new System.Windows.Forms.Padding(2);
            this.btnConvResultDocID.Name = "btnConvResultDocID";
            this.btnConvResultDocID.Size = new System.Drawing.Size(56, 23);
            this.btnConvResultDocID.TabIndex = 8;
            this.btnConvResultDocID.Text = "Find";
            this.btnConvResultDocID.UseVisualStyleBackColor = true;
            this.btnConvResultDocID.Click += new System.EventHandler(this.btnConvResultDocID_Click);
            // 
            // txtConvResultDocID
            // 
            this.txtConvResultDocID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConvResultDocID.Location = new System.Drawing.Point(609, 368);
            this.txtConvResultDocID.Margin = new System.Windows.Forms.Padding(2);
            this.txtConvResultDocID.Name = "txtConvResultDocID";
            this.txtConvResultDocID.Size = new System.Drawing.Size(116, 23);
            this.txtConvResultDocID.TabIndex = 7;
            // 
            // lblConvResultDocID
            // 
            this.lblConvResultDocID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblConvResultDocID.AutoSize = true;
            this.lblConvResultDocID.Location = new System.Drawing.Point(510, 368);
            this.lblConvResultDocID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblConvResultDocID.Name = "lblConvResultDocID";
            this.lblConvResultDocID.Size = new System.Drawing.Size(96, 18);
            this.lblConvResultDocID.TabIndex = 6;
            this.lblConvResultDocID.Text = "Marshaling ID:";
            // 
            // btnConvResultCaseID
            // 
            this.btnConvResultCaseID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConvResultCaseID.Location = new System.Drawing.Point(423, 368);
            this.btnConvResultCaseID.Margin = new System.Windows.Forms.Padding(2);
            this.btnConvResultCaseID.Name = "btnConvResultCaseID";
            this.btnConvResultCaseID.Size = new System.Drawing.Size(56, 23);
            this.btnConvResultCaseID.TabIndex = 5;
            this.btnConvResultCaseID.Text = "Find";
            this.btnConvResultCaseID.UseVisualStyleBackColor = true;
            this.btnConvResultCaseID.Click += new System.EventHandler(this.btnConvResultCaseID_Click);
            // 
            // txtConvResultCaseID
            // 
            this.txtConvResultCaseID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConvResultCaseID.Location = new System.Drawing.Point(290, 368);
            this.txtConvResultCaseID.Margin = new System.Windows.Forms.Padding(2);
            this.txtConvResultCaseID.Name = "txtConvResultCaseID";
            this.txtConvResultCaseID.Size = new System.Drawing.Size(129, 23);
            this.txtConvResultCaseID.TabIndex = 4;
            // 
            // lblConvResultCaseID
            // 
            this.lblConvResultCaseID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblConvResultCaseID.AutoSize = true;
            this.lblConvResultCaseID.Location = new System.Drawing.Point(195, 368);
            this.lblConvResultCaseID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblConvResultCaseID.Name = "lblConvResultCaseID";
            this.lblConvResultCaseID.Size = new System.Drawing.Size(95, 18);
            this.lblConvResultCaseID.TabIndex = 3;
            this.lblConvResultCaseID.Text = "Case Number:";
            // 
            // txtConvResultBatch
            // 
            this.txtConvResultBatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtConvResultBatch.Location = new System.Drawing.Point(51, 368);
            this.txtConvResultBatch.Margin = new System.Windows.Forms.Padding(2);
            this.txtConvResultBatch.Name = "txtConvResultBatch";
            this.txtConvResultBatch.Size = new System.Drawing.Size(108, 23);
            this.txtConvResultBatch.TabIndex = 2;
            // 
            // lblConvResultBatch
            // 
            this.lblConvResultBatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConvResultBatch.AutoSize = true;
            this.lblConvResultBatch.Location = new System.Drawing.Point(2, 368);
            this.lblConvResultBatch.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblConvResultBatch.Name = "lblConvResultBatch";
            this.lblConvResultBatch.Size = new System.Drawing.Size(49, 18);
            this.lblConvResultBatch.TabIndex = 1;
            this.lblConvResultBatch.Text = "Batch:";
            // 
            // treeConvResult
            // 
            this.treeConvResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeConvResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeConvResult.Location = new System.Drawing.Point(5, 14);
            this.treeConvResult.Margin = new System.Windows.Forms.Padding(2);
            this.treeConvResult.Name = "treeConvResult";
            this.treeConvResult.Size = new System.Drawing.Size(778, 346);
            this.treeConvResult.TabIndex = 0;
            // 
            // pageMigrate
            // 
            this.pageMigrate.Controls.Add(this.tabMigrate);
            this.pageMigrate.Location = new System.Drawing.Point(4, 32);
            this.pageMigrate.Margin = new System.Windows.Forms.Padding(2);
            this.pageMigrate.Name = "pageMigrate";
            this.pageMigrate.Size = new System.Drawing.Size(792, 412);
            this.pageMigrate.TabIndex = 4;
            this.pageMigrate.Text = "Migrate";
            this.pageMigrate.UseVisualStyleBackColor = true;
            // 
            // tabMigrate
            // 
            this.tabMigrate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabMigrate.Controls.Add(this.MigrateJob);
            this.tabMigrate.Controls.Add(this.MigrateBatch);
            this.tabMigrate.Controls.Add(this.PerformIngest);
            this.tabMigrate.Controls.Add(this.IngestLogs);
            this.tabMigrate.Controls.Add(this.IngestResults);
            this.tabMigrate.Location = new System.Drawing.Point(0, 0);
            this.tabMigrate.Margin = new System.Windows.Forms.Padding(2);
            this.tabMigrate.Name = "tabMigrate";
            this.tabMigrate.SelectedIndex = 0;
            this.tabMigrate.Size = new System.Drawing.Size(800, 417);
            this.tabMigrate.TabIndex = 0;
            this.tabMigrate.SelectedIndexChanged += new System.EventHandler(this.tabMigrate_SelectedIndexChanged);
            // 
            // MigrateJob
            // 
            this.MigrateJob.Controls.Add(this.listMigrateJobs);
            this.MigrateJob.Controls.Add(this.lblMigrateJobs);
            this.MigrateJob.Location = new System.Drawing.Point(4, 27);
            this.MigrateJob.Margin = new System.Windows.Forms.Padding(2);
            this.MigrateJob.Name = "MigrateJob";
            this.MigrateJob.Padding = new System.Windows.Forms.Padding(2);
            this.MigrateJob.Size = new System.Drawing.Size(792, 386);
            this.MigrateJob.TabIndex = 0;
            this.MigrateJob.Text = "Choose Job";
            this.MigrateJob.UseVisualStyleBackColor = true;
            // 
            // listMigrateJobs
            // 
            this.listMigrateJobs.AllowColumnReorder = true;
            this.listMigrateJobs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listMigrateJobs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listMigrateJobs.CheckBoxes = true;
            this.listMigrateJobs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colMigrateJobID,
            this.colMigrateJobName,
            this.colMigrateJobComment});
            this.listMigrateJobs.Location = new System.Drawing.Point(5, 28);
            this.listMigrateJobs.Margin = new System.Windows.Forms.Padding(2);
            this.listMigrateJobs.MultiSelect = false;
            this.listMigrateJobs.Name = "listMigrateJobs";
            this.listMigrateJobs.Size = new System.Drawing.Size(781, 354);
            this.listMigrateJobs.TabIndex = 1;
            this.listMigrateJobs.UseCompatibleStateImageBehavior = false;
            this.listMigrateJobs.View = System.Windows.Forms.View.Details;
            this.listMigrateJobs.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listMigrateJobs_ItemChecked);
            // 
            // colMigrateJobID
            // 
            this.colMigrateJobID.Text = "Job ID";
            this.colMigrateJobID.Width = 125;
            // 
            // colMigrateJobName
            // 
            this.colMigrateJobName.Text = "Job Name";
            this.colMigrateJobName.Width = 200;
            // 
            // colMigrateJobComment
            // 
            this.colMigrateJobComment.Text = "Comment";
            this.colMigrateJobComment.Width = 709;
            // 
            // lblMigrateJobs
            // 
            this.lblMigrateJobs.AutoSize = true;
            this.lblMigrateJobs.Location = new System.Drawing.Point(5, 6);
            this.lblMigrateJobs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMigrateJobs.Name = "lblMigrateJobs";
            this.lblMigrateJobs.Size = new System.Drawing.Size(96, 18);
            this.lblMigrateJobs.TabIndex = 0;
            this.lblMigrateJobs.Text = "Existing Jobs:";
            // 
            // MigrateBatch
            // 
            this.MigrateBatch.Controls.Add(this.listMigrateBatches);
            this.MigrateBatch.Controls.Add(this.lblMigrateBatches);
            this.MigrateBatch.Location = new System.Drawing.Point(4, 27);
            this.MigrateBatch.Margin = new System.Windows.Forms.Padding(2);
            this.MigrateBatch.Name = "MigrateBatch";
            this.MigrateBatch.Padding = new System.Windows.Forms.Padding(2);
            this.MigrateBatch.Size = new System.Drawing.Size(792, 386);
            this.MigrateBatch.TabIndex = 1;
            this.MigrateBatch.Text = "Choose Conversion Batch";
            this.MigrateBatch.UseVisualStyleBackColor = true;
            // 
            // listMigrateBatches
            // 
            this.listMigrateBatches.AllowColumnReorder = true;
            this.listMigrateBatches.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listMigrateBatches.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listMigrateBatches.CheckBoxes = true;
            this.listMigrateBatches.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.migBatchID,
            this.migBatchSize,
            this.migBatchStart,
            this.migBatchEnd,
            this.migBatchUser,
            this.migBatchWkstn,
            this.migBatchStatus});
            this.listMigrateBatches.FullRowSelect = true;
            this.listMigrateBatches.Location = new System.Drawing.Point(8, 28);
            this.listMigrateBatches.Margin = new System.Windows.Forms.Padding(2);
            this.listMigrateBatches.Name = "listMigrateBatches";
            this.listMigrateBatches.Size = new System.Drawing.Size(778, 365);
            this.listMigrateBatches.TabIndex = 1;
            this.listMigrateBatches.UseCompatibleStateImageBehavior = false;
            this.listMigrateBatches.View = System.Windows.Forms.View.Details;
            this.listMigrateBatches.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listMigrateBatches_ItemChecked);
            // 
            // migBatchID
            // 
            this.migBatchID.Text = "Batch ID";
            this.migBatchID.Width = 100;
            // 
            // migBatchSize
            // 
            this.migBatchSize.Text = "Size";
            this.migBatchSize.Width = 100;
            // 
            // migBatchStart
            // 
            this.migBatchStart.Text = "Start Time";
            this.migBatchStart.Width = 200;
            // 
            // migBatchEnd
            // 
            this.migBatchEnd.Text = "End Time";
            this.migBatchEnd.Width = 200;
            // 
            // migBatchUser
            // 
            this.migBatchUser.Text = "User";
            this.migBatchUser.Width = 150;
            // 
            // migBatchWkstn
            // 
            this.migBatchWkstn.Text = "WorkStation";
            this.migBatchWkstn.Width = 150;
            // 
            // migBatchStatus
            // 
            this.migBatchStatus.Text = "Status";
            this.migBatchStatus.Width = 130;
            // 
            // lblMigrateBatches
            // 
            this.lblMigrateBatches.AutoSize = true;
            this.lblMigrateBatches.Location = new System.Drawing.Point(5, 6);
            this.lblMigrateBatches.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMigrateBatches.Name = "lblMigrateBatches";
            this.lblMigrateBatches.Size = new System.Drawing.Size(117, 18);
            this.lblMigrateBatches.TabIndex = 0;
            this.lblMigrateBatches.Text = "Existing Batches:";
            // 
            // PerformIngest
            // 
            this.PerformIngest.Controls.Add(this.btnMigrate);
            this.PerformIngest.Controls.Add(this.btnMigrateCancel);
            this.PerformIngest.Controls.Add(this.migrateStatus);
            this.PerformIngest.Controls.Add(this.ckTasks);
            this.PerformIngest.Controls.Add(this.ckAnnotations);
            this.PerformIngest.Controls.Add(this.ckDocuments);
            this.PerformIngest.Controls.Add(this.ckCases);
            this.PerformIngest.Controls.Add(this.ckDepthFirst);
            this.PerformIngest.Controls.Add(this.lblMigrateBatch);
            this.PerformIngest.Controls.Add(this.lblMigrateJob);
            this.PerformIngest.Location = new System.Drawing.Point(4, 27);
            this.PerformIngest.Margin = new System.Windows.Forms.Padding(2);
            this.PerformIngest.Name = "PerformIngest";
            this.PerformIngest.Size = new System.Drawing.Size(792, 386);
            this.PerformIngest.TabIndex = 2;
            this.PerformIngest.Text = "Ingest";
            this.PerformIngest.UseVisualStyleBackColor = true;
            // 
            // btnMigrate
            // 
            this.btnMigrate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMigrate.Location = new System.Drawing.Point(709, 369);
            this.btnMigrate.Margin = new System.Windows.Forms.Padding(2);
            this.btnMigrate.Name = "btnMigrate";
            this.btnMigrate.Size = new System.Drawing.Size(64, 23);
            this.btnMigrate.TabIndex = 9;
            this.btnMigrate.Text = "Migrate";
            this.btnMigrate.UseVisualStyleBackColor = true;
            this.btnMigrate.Click += new System.EventHandler(this.btnMigrate_Click);
            // 
            // btnMigrateCancel
            // 
            this.btnMigrateCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMigrateCancel.Enabled = false;
            this.btnMigrateCancel.Location = new System.Drawing.Point(638, 369);
            this.btnMigrateCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnMigrateCancel.Name = "btnMigrateCancel";
            this.btnMigrateCancel.Size = new System.Drawing.Size(64, 23);
            this.btnMigrateCancel.TabIndex = 8;
            this.btnMigrateCancel.Text = "Cancel";
            this.btnMigrateCancel.UseVisualStyleBackColor = true;
            this.btnMigrateCancel.Click += new System.EventHandler(this.btnMigrateCancel_Click);
            // 
            // migrateStatus
            // 
            this.migrateStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.migrateStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.migrateStatus.FormattingEnabled = true;
            this.migrateStatus.HorizontalScrollbar = true;
            this.migrateStatus.ItemHeight = 18;
            this.migrateStatus.Location = new System.Drawing.Point(8, 48);
            this.migrateStatus.Margin = new System.Windows.Forms.Padding(2);
            this.migrateStatus.Name = "migrateStatus";
            this.migrateStatus.Size = new System.Drawing.Size(776, 270);
            this.migrateStatus.TabIndex = 7;
            // 
            // ckTasks
            // 
            this.ckTasks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckTasks.AutoSize = true;
            this.ckTasks.Location = new System.Drawing.Point(600, 20);
            this.ckTasks.Margin = new System.Windows.Forms.Padding(2);
            this.ckTasks.Name = "ckTasks";
            this.ckTasks.Size = new System.Drawing.Size(59, 22);
            this.ckTasks.TabIndex = 6;
            this.ckTasks.Text = "Tasks";
            this.ckTasks.UseVisualStyleBackColor = true;
            this.ckTasks.CheckedChanged += new System.EventHandler(this.ckMigrate_CheckedChanged);
            // 
            // ckAnnotations
            // 
            this.ckAnnotations.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckAnnotations.AutoSize = true;
            this.ckAnnotations.Location = new System.Drawing.Point(463, 20);
            this.ckAnnotations.Margin = new System.Windows.Forms.Padding(2);
            this.ckAnnotations.Name = "ckAnnotations";
            this.ckAnnotations.Size = new System.Drawing.Size(105, 22);
            this.ckAnnotations.TabIndex = 5;
            this.ckAnnotations.Text = "Annotations";
            this.ckAnnotations.UseVisualStyleBackColor = true;
            this.ckAnnotations.CheckedChanged += new System.EventHandler(this.ckMigrate_CheckedChanged);
            // 
            // ckDocuments
            // 
            this.ckDocuments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckDocuments.AutoSize = true;
            this.ckDocuments.Location = new System.Drawing.Point(351, 20);
            this.ckDocuments.Margin = new System.Windows.Forms.Padding(2);
            this.ckDocuments.Name = "ckDocuments";
            this.ckDocuments.Size = new System.Drawing.Size(98, 22);
            this.ckDocuments.TabIndex = 4;
            this.ckDocuments.Text = "Documents";
            this.ckDocuments.UseVisualStyleBackColor = true;
            this.ckDocuments.CheckedChanged += new System.EventHandler(this.ckMigrate_CheckedChanged);
            // 
            // ckCases
            // 
            this.ckCases.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckCases.AutoSize = true;
            this.ckCases.Location = new System.Drawing.Point(244, 20);
            this.ckCases.Margin = new System.Windows.Forms.Padding(2);
            this.ckCases.Name = "ckCases";
            this.ckCases.Size = new System.Drawing.Size(62, 22);
            this.ckCases.TabIndex = 3;
            this.ckCases.Text = "Cases";
            this.ckCases.UseVisualStyleBackColor = true;
            this.ckCases.CheckedChanged += new System.EventHandler(this.ckMigrate_CheckedChanged);
            // 
            // ckDepthFirst
            // 
            this.ckDepthFirst.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckDepthFirst.AutoSize = true;
            this.ckDepthFirst.Checked = true;
            this.ckDepthFirst.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckDepthFirst.Location = new System.Drawing.Point(681, 20);
            this.ckDepthFirst.Margin = new System.Windows.Forms.Padding(2);
            this.ckDepthFirst.Name = "ckDepthFirst";
            this.ckDepthFirst.Size = new System.Drawing.Size(98, 22);
            this.ckDepthFirst.TabIndex = 2;
            this.ckDepthFirst.Text = "Depth First";
            this.ckDepthFirst.UseVisualStyleBackColor = true;
            this.ckDepthFirst.CheckedChanged += new System.EventHandler(this.ckDepthFirst_CheckedChanged);
            // 
            // lblMigrateBatch
            // 
            this.lblMigrateBatch.AutoSize = true;
            this.lblMigrateBatch.Location = new System.Drawing.Point(5, 29);
            this.lblMigrateBatch.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMigrateBatch.Name = "lblMigrateBatch";
            this.lblMigrateBatch.Size = new System.Drawing.Size(49, 18);
            this.lblMigrateBatch.TabIndex = 1;
            this.lblMigrateBatch.Text = "Batch:";
            // 
            // lblMigrateJob
            // 
            this.lblMigrateJob.AutoSize = true;
            this.lblMigrateJob.Location = new System.Drawing.Point(5, 8);
            this.lblMigrateJob.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMigrateJob.Name = "lblMigrateJob";
            this.lblMigrateJob.Size = new System.Drawing.Size(36, 18);
            this.lblMigrateJob.TabIndex = 0;
            this.lblMigrateJob.Text = "Job:";
            // 
            // IngestLogs
            // 
            this.IngestLogs.Controls.Add(this.tabIngestLog);
            this.IngestLogs.Location = new System.Drawing.Point(4, 27);
            this.IngestLogs.Margin = new System.Windows.Forms.Padding(2);
            this.IngestLogs.Name = "IngestLogs";
            this.IngestLogs.Size = new System.Drawing.Size(792, 386);
            this.IngestLogs.TabIndex = 3;
            this.IngestLogs.Text = "View Logs";
            this.IngestLogs.UseVisualStyleBackColor = true;
            // 
            // tabIngestLog
            // 
            this.tabIngestLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabIngestLog.Controls.Add(this.IngestResultSession);
            this.tabIngestLog.Controls.Add(this.IngestResultBatch);
            this.tabIngestLog.Controls.Add(this.IngestResultView);
            this.tabIngestLog.Controls.Add(this.IngestErrorView);
            this.tabIngestLog.Location = new System.Drawing.Point(0, 0);
            this.tabIngestLog.Margin = new System.Windows.Forms.Padding(2);
            this.tabIngestLog.Name = "tabIngestLog";
            this.tabIngestLog.SelectedIndex = 0;
            this.tabIngestLog.Size = new System.Drawing.Size(800, 439);
            this.tabIngestLog.TabIndex = 0;
            this.tabIngestLog.SelectedIndexChanged += new System.EventHandler(this.tabIngestLog_SelectedIndexChanged);
            // 
            // IngestResultSession
            // 
            this.IngestResultSession.Controls.Add(this.listIngestSessions);
            this.IngestResultSession.Controls.Add(this.lblIngestSessions);
            this.IngestResultSession.Location = new System.Drawing.Point(4, 27);
            this.IngestResultSession.Margin = new System.Windows.Forms.Padding(2);
            this.IngestResultSession.Name = "IngestResultSession";
            this.IngestResultSession.Padding = new System.Windows.Forms.Padding(2);
            this.IngestResultSession.Size = new System.Drawing.Size(792, 408);
            this.IngestResultSession.TabIndex = 0;
            this.IngestResultSession.Text = "Choose Migration Session";
            this.IngestResultSession.UseVisualStyleBackColor = true;
            // 
            // listIngestSessions
            // 
            this.listIngestSessions.AllowColumnReorder = true;
            this.listIngestSessions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listIngestSessions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listIngestSessions.CheckBoxes = true;
            this.listIngestSessions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ingestSession,
            this.ingestStart,
            this.ingestEnd,
            this.ingestUser,
            this.ingestWkstn,
            this.ingestStatus});
            this.listIngestSessions.FullRowSelect = true;
            this.listIngestSessions.Location = new System.Drawing.Point(5, 28);
            this.listIngestSessions.Margin = new System.Windows.Forms.Padding(2);
            this.listIngestSessions.MultiSelect = false;
            this.listIngestSessions.Name = "listIngestSessions";
            this.listIngestSessions.Size = new System.Drawing.Size(777, 338);
            this.listIngestSessions.TabIndex = 1;
            this.listIngestSessions.UseCompatibleStateImageBehavior = false;
            this.listIngestSessions.View = System.Windows.Forms.View.Details;
            this.listIngestSessions.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listIngestSessions_ItemChecked);
            // 
            // ingestSession
            // 
            this.ingestSession.Text = "Session ID";
            this.ingestSession.Width = 100;
            // 
            // ingestStart
            // 
            this.ingestStart.Text = "Start Time";
            this.ingestStart.Width = 200;
            // 
            // ingestEnd
            // 
            this.ingestEnd.Text = "End Time";
            this.ingestEnd.Width = 200;
            // 
            // ingestUser
            // 
            this.ingestUser.Text = "User";
            this.ingestUser.Width = 150;
            // 
            // ingestWkstn
            // 
            this.ingestWkstn.Text = "WorkStation";
            this.ingestWkstn.Width = 150;
            // 
            // ingestStatus
            // 
            this.ingestStatus.Text = "Status";
            // 
            // lblIngestSessions
            // 
            this.lblIngestSessions.AutoSize = true;
            this.lblIngestSessions.Location = new System.Drawing.Point(5, 6);
            this.lblIngestSessions.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIngestSessions.Name = "lblIngestSessions";
            this.lblIngestSessions.Size = new System.Drawing.Size(120, 18);
            this.lblIngestSessions.TabIndex = 0;
            this.lblIngestSessions.Text = "Existing Sessions:";
            // 
            // IngestResultBatch
            // 
            this.IngestResultBatch.Controls.Add(this.listIngestBatches);
            this.IngestResultBatch.Controls.Add(this.lblIngestBatches);
            this.IngestResultBatch.Location = new System.Drawing.Point(4, 27);
            this.IngestResultBatch.Margin = new System.Windows.Forms.Padding(2);
            this.IngestResultBatch.Name = "IngestResultBatch";
            this.IngestResultBatch.Padding = new System.Windows.Forms.Padding(2);
            this.IngestResultBatch.Size = new System.Drawing.Size(792, 408);
            this.IngestResultBatch.TabIndex = 1;
            this.IngestResultBatch.Text = "Choose Migration Batch";
            this.IngestResultBatch.UseVisualStyleBackColor = true;
            // 
            // listIngestBatches
            // 
            this.listIngestBatches.AllowColumnReorder = true;
            this.listIngestBatches.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listIngestBatches.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listIngestBatches.CheckBoxes = true;
            this.listIngestBatches.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ingBatchID,
            this.ingStartTime,
            this.ingEndTime,
            this.ingUser,
            this.ingWkstn,
            this.ingStatus,
            this.ingComment});
            this.listIngestBatches.Location = new System.Drawing.Point(5, 28);
            this.listIngestBatches.Margin = new System.Windows.Forms.Padding(2);
            this.listIngestBatches.MultiSelect = false;
            this.listIngestBatches.Name = "listIngestBatches";
            this.listIngestBatches.Size = new System.Drawing.Size(777, 338);
            this.listIngestBatches.TabIndex = 1;
            this.listIngestBatches.UseCompatibleStateImageBehavior = false;
            this.listIngestBatches.View = System.Windows.Forms.View.Details;
            this.listIngestBatches.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listIngestBatches_ItemChecked);
            // 
            // ingBatchID
            // 
            this.ingBatchID.Text = "Batch ID";
            this.ingBatchID.Width = 70;
            // 
            // ingStartTime
            // 
            this.ingStartTime.Text = "Start Time";
            this.ingStartTime.Width = 190;
            // 
            // ingEndTime
            // 
            this.ingEndTime.Text = "End Time";
            this.ingEndTime.Width = 190;
            // 
            // ingUser
            // 
            this.ingUser.Text = "User";
            this.ingUser.Width = 95;
            // 
            // ingWkstn
            // 
            this.ingWkstn.Text = "Workstation";
            this.ingWkstn.Width = 110;
            // 
            // ingStatus
            // 
            this.ingStatus.Text = "Status";
            this.ingStatus.Width = 95;
            // 
            // ingComment
            // 
            this.ingComment.Text = "Comment";
            this.ingComment.Width = 280;
            // 
            // lblIngestBatches
            // 
            this.lblIngestBatches.AutoSize = true;
            this.lblIngestBatches.Location = new System.Drawing.Point(5, 6);
            this.lblIngestBatches.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIngestBatches.Name = "lblIngestBatches";
            this.lblIngestBatches.Size = new System.Drawing.Size(200, 18);
            this.lblIngestBatches.TabIndex = 0;
            this.lblIngestBatches.Text = "Migration Batches for Session:";
            // 
            // IngestResultView
            // 
            this.IngestResultView.Controls.Add(this.btnNextIngestLog);
            this.IngestResultView.Controls.Add(this.btnPrevIngestLog);
            this.IngestResultView.Controls.Add(this.lblIngestLogCount);
            this.IngestResultView.Controls.Add(this.listIngestBatchLog);
            this.IngestResultView.Controls.Add(this.lblIngestBatchLog);
            this.IngestResultView.Location = new System.Drawing.Point(4, 27);
            this.IngestResultView.Margin = new System.Windows.Forms.Padding(2);
            this.IngestResultView.Name = "IngestResultView";
            this.IngestResultView.Size = new System.Drawing.Size(792, 408);
            this.IngestResultView.TabIndex = 2;
            this.IngestResultView.Text = "View Batch Log";
            this.IngestResultView.UseVisualStyleBackColor = true;
            // 
            // btnNextIngestLog
            // 
            this.btnNextIngestLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNextIngestLog.Location = new System.Drawing.Point(724, 342);
            this.btnNextIngestLog.Margin = new System.Windows.Forms.Padding(2);
            this.btnNextIngestLog.Name = "btnNextIngestLog";
            this.btnNextIngestLog.Size = new System.Drawing.Size(56, 23);
            this.btnNextIngestLog.TabIndex = 4;
            this.btnNextIngestLog.Text = "Next";
            this.btnNextIngestLog.UseVisualStyleBackColor = true;
            this.btnNextIngestLog.Click += new System.EventHandler(this.btnNextIngestLog_Click);
            // 
            // btnPrevIngestLog
            // 
            this.btnPrevIngestLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrevIngestLog.Location = new System.Drawing.Point(663, 342);
            this.btnPrevIngestLog.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrevIngestLog.Name = "btnPrevIngestLog";
            this.btnPrevIngestLog.Size = new System.Drawing.Size(56, 23);
            this.btnPrevIngestLog.TabIndex = 3;
            this.btnPrevIngestLog.Text = "Prev";
            this.btnPrevIngestLog.UseVisualStyleBackColor = true;
            this.btnPrevIngestLog.Click += new System.EventHandler(this.btnPrevIngestLog_Click);
            // 
            // lblIngestLogCount
            // 
            this.lblIngestLogCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblIngestLogCount.AutoSize = true;
            this.lblIngestLogCount.Location = new System.Drawing.Point(10, 342);
            this.lblIngestLogCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIngestLogCount.Name = "lblIngestLogCount";
            this.lblIngestLogCount.Size = new System.Drawing.Size(96, 18);
            this.lblIngestLogCount.TabIndex = 2;
            this.lblIngestLogCount.Text = "Total Records:";
            // 
            // listIngestBatchLog
            // 
            this.listIngestBatchLog.AllowColumnReorder = true;
            this.listIngestBatchLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listIngestBatchLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listIngestBatchLog.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ingOccurred,
            this.ingEvent,
            this.ingEventDesc});
            this.listIngestBatchLog.Location = new System.Drawing.Point(6, 25);
            this.listIngestBatchLog.Margin = new System.Windows.Forms.Padding(2);
            this.listIngestBatchLog.Name = "listIngestBatchLog";
            this.listIngestBatchLog.Size = new System.Drawing.Size(776, 310);
            this.listIngestBatchLog.TabIndex = 1;
            this.listIngestBatchLog.UseCompatibleStateImageBehavior = false;
            this.listIngestBatchLog.View = System.Windows.Forms.View.Details;
            // 
            // ingOccurred
            // 
            this.ingOccurred.Text = "Time Occurred";
            this.ingOccurred.Width = 200;
            // 
            // ingEvent
            // 
            this.ingEvent.Text = "Event";
            this.ingEvent.Width = 80;
            // 
            // ingEventDesc
            // 
            this.ingEventDesc.Text = "Event Description";
            this.ingEventDesc.Width = 752;
            // 
            // lblIngestBatchLog
            // 
            this.lblIngestBatchLog.AutoSize = true;
            this.lblIngestBatchLog.Location = new System.Drawing.Point(3, 3);
            this.lblIngestBatchLog.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIngestBatchLog.Name = "lblIngestBatchLog";
            this.lblIngestBatchLog.Size = new System.Drawing.Size(135, 18);
            this.lblIngestBatchLog.TabIndex = 0;
            this.lblIngestBatchLog.Text = "Existing Batch Logs:";
            // 
            // IngestErrorView
            // 
            this.IngestErrorView.Controls.Add(this.listIngestBatchErrors);
            this.IngestErrorView.Controls.Add(this.lblIngestBatchErrors);
            this.IngestErrorView.Location = new System.Drawing.Point(4, 27);
            this.IngestErrorView.Margin = new System.Windows.Forms.Padding(2);
            this.IngestErrorView.Name = "IngestErrorView";
            this.IngestErrorView.Size = new System.Drawing.Size(792, 408);
            this.IngestErrorView.TabIndex = 3;
            this.IngestErrorView.Text = "View Batch Errors";
            this.IngestErrorView.UseVisualStyleBackColor = true;
            // 
            // listIngestBatchErrors
            // 
            this.listIngestBatchErrors.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listIngestBatchErrors.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listIngestBatchErrors.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.migOccurrence,
            this.migEventDesc});
            this.listIngestBatchErrors.FullRowSelect = true;
            this.listIngestBatchErrors.Location = new System.Drawing.Point(6, 25);
            this.listIngestBatchErrors.Margin = new System.Windows.Forms.Padding(2);
            this.listIngestBatchErrors.MultiSelect = false;
            this.listIngestBatchErrors.Name = "listIngestBatchErrors";
            this.listIngestBatchErrors.Size = new System.Drawing.Size(776, 341);
            this.listIngestBatchErrors.TabIndex = 1;
            this.listIngestBatchErrors.UseCompatibleStateImageBehavior = false;
            this.listIngestBatchErrors.View = System.Windows.Forms.View.Details;
            // 
            // migOccurrence
            // 
            this.migOccurrence.Text = "Time Occurred";
            this.migOccurrence.Width = 200;
            // 
            // migEventDesc
            // 
            this.migEventDesc.Text = "Event Description";
            this.migEventDesc.Width = 828;
            // 
            // lblIngestBatchErrors
            // 
            this.lblIngestBatchErrors.AutoSize = true;
            this.lblIngestBatchErrors.Location = new System.Drawing.Point(3, 3);
            this.lblIngestBatchErrors.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIngestBatchErrors.Name = "lblIngestBatchErrors";
            this.lblIngestBatchErrors.Size = new System.Drawing.Size(144, 18);
            this.lblIngestBatchErrors.TabIndex = 0;
            this.lblIngestBatchErrors.Text = "Existing Batch Errors:";
            // 
            // IngestResults
            // 
            this.IngestResults.Controls.Add(this.btnMigResultTaskID);
            this.IngestResults.Controls.Add(this.btnMigResultAnnID);
            this.IngestResults.Controls.Add(this.btnMigResultDocID);
            this.IngestResults.Controls.Add(this.btnMigResultCaseID);
            this.IngestResults.Controls.Add(this.txtMigResultID);
            this.IngestResults.Controls.Add(this.lblMigResultID);
            this.IngestResults.Controls.Add(this.treeIngestResult);
            this.IngestResults.Location = new System.Drawing.Point(4, 27);
            this.IngestResults.Margin = new System.Windows.Forms.Padding(2);
            this.IngestResults.Name = "IngestResults";
            this.IngestResults.Size = new System.Drawing.Size(792, 386);
            this.IngestResults.TabIndex = 4;
            this.IngestResults.Text = "View Results";
            this.IngestResults.UseVisualStyleBackColor = true;
            // 
            // btnMigResultTaskID
            // 
            this.btnMigResultTaskID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnMigResultTaskID.Location = new System.Drawing.Point(570, 372);
            this.btnMigResultTaskID.Margin = new System.Windows.Forms.Padding(2);
            this.btnMigResultTaskID.Name = "btnMigResultTaskID";
            this.btnMigResultTaskID.Size = new System.Drawing.Size(120, 23);
            this.btnMigResultTaskID.TabIndex = 6;
            this.btnMigResultTaskID.Text = "Find Task";
            this.btnMigResultTaskID.UseVisualStyleBackColor = true;
            this.btnMigResultTaskID.Click += new System.EventHandler(this.btnMigResultTaskID_Click);
            // 
            // btnMigResultAnnID
            // 
            this.btnMigResultAnnID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnMigResultAnnID.Location = new System.Drawing.Point(442, 372);
            this.btnMigResultAnnID.Margin = new System.Windows.Forms.Padding(2);
            this.btnMigResultAnnID.Name = "btnMigResultAnnID";
            this.btnMigResultAnnID.Size = new System.Drawing.Size(120, 23);
            this.btnMigResultAnnID.TabIndex = 5;
            this.btnMigResultAnnID.Text = "Find Annotation";
            this.btnMigResultAnnID.UseVisualStyleBackColor = true;
            this.btnMigResultAnnID.Click += new System.EventHandler(this.btnMigResultAnnID_Click);
            // 
            // btnMigResultDocID
            // 
            this.btnMigResultDocID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnMigResultDocID.Location = new System.Drawing.Point(315, 372);
            this.btnMigResultDocID.Margin = new System.Windows.Forms.Padding(2);
            this.btnMigResultDocID.Name = "btnMigResultDocID";
            this.btnMigResultDocID.Size = new System.Drawing.Size(120, 23);
            this.btnMigResultDocID.TabIndex = 4;
            this.btnMigResultDocID.Text = "Find Document";
            this.btnMigResultDocID.UseVisualStyleBackColor = true;
            this.btnMigResultDocID.Click += new System.EventHandler(this.btnMigResultDocID_Click);
            // 
            // btnMigResultCaseID
            // 
            this.btnMigResultCaseID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnMigResultCaseID.Location = new System.Drawing.Point(180, 372);
            this.btnMigResultCaseID.Margin = new System.Windows.Forms.Padding(2);
            this.btnMigResultCaseID.Name = "btnMigResultCaseID";
            this.btnMigResultCaseID.Size = new System.Drawing.Size(120, 23);
            this.btnMigResultCaseID.TabIndex = 3;
            this.btnMigResultCaseID.Text = "Find Case";
            this.btnMigResultCaseID.UseVisualStyleBackColor = true;
            this.btnMigResultCaseID.Click += new System.EventHandler(this.btnMigResultCaseID_Click);
            // 
            // txtMigResultID
            // 
            this.txtMigResultID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtMigResultID.Location = new System.Drawing.Point(30, 372);
            this.txtMigResultID.Margin = new System.Windows.Forms.Padding(2);
            this.txtMigResultID.Name = "txtMigResultID";
            this.txtMigResultID.Size = new System.Drawing.Size(136, 23);
            this.txtMigResultID.TabIndex = 2;
            // 
            // lblMigResultID
            // 
            this.lblMigResultID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblMigResultID.AutoSize = true;
            this.lblMigResultID.Location = new System.Drawing.Point(2, 372);
            this.lblMigResultID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMigResultID.Name = "lblMigResultID";
            this.lblMigResultID.Size = new System.Drawing.Size(26, 18);
            this.lblMigResultID.TabIndex = 1;
            this.lblMigResultID.Text = "ID:";
            // 
            // treeIngestResult
            // 
            this.treeIngestResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeIngestResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeIngestResult.Location = new System.Drawing.Point(5, 3);
            this.treeIngestResult.Margin = new System.Windows.Forms.Padding(2);
            this.treeIngestResult.Name = "treeIngestResult";
            this.treeIngestResult.Size = new System.Drawing.Size(786, 356);
            this.treeIngestResult.TabIndex = 0;
            // 
            // pageReset
            // 
            this.pageReset.Controls.Add(this.tabReset);
            this.pageReset.Location = new System.Drawing.Point(4, 32);
            this.pageReset.Margin = new System.Windows.Forms.Padding(2);
            this.pageReset.Name = "pageReset";
            this.pageReset.Size = new System.Drawing.Size(792, 412);
            this.pageReset.TabIndex = 5;
            this.pageReset.Text = "Reset";
            this.pageReset.UseVisualStyleBackColor = true;
            // 
            // tabReset
            // 
            this.tabReset.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabReset.Controls.Add(this.ResetConversion);
            this.tabReset.Controls.Add(this.ResetIngestion);
            this.tabReset.Location = new System.Drawing.Point(0, 0);
            this.tabReset.Margin = new System.Windows.Forms.Padding(2);
            this.tabReset.Name = "tabReset";
            this.tabReset.SelectedIndex = 0;
            this.tabReset.Size = new System.Drawing.Size(800, 417);
            this.tabReset.TabIndex = 0;
            this.tabReset.SelectedIndexChanged += new System.EventHandler(this.tabReset_SelectedIndexChanged);
            // 
            // ResetConversion
            // 
            this.ResetConversion.Controls.Add(this.tabConversionReset);
            this.ResetConversion.Location = new System.Drawing.Point(4, 27);
            this.ResetConversion.Margin = new System.Windows.Forms.Padding(2);
            this.ResetConversion.Name = "ResetConversion";
            this.ResetConversion.Padding = new System.Windows.Forms.Padding(2);
            this.ResetConversion.Size = new System.Drawing.Size(792, 386);
            this.ResetConversion.TabIndex = 0;
            this.ResetConversion.Text = "Reset Status for Conversion";
            this.ResetConversion.UseVisualStyleBackColor = true;
            // 
            // tabConversionReset
            // 
            this.tabConversionReset.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabConversionReset.Controls.Add(this.JobsForConversionReset);
            this.tabConversionReset.Controls.Add(this.BatchesForConversionReset);
            this.tabConversionReset.Controls.Add(this.AccountForConversionReset);
            this.tabConversionReset.Location = new System.Drawing.Point(0, 0);
            this.tabConversionReset.Margin = new System.Windows.Forms.Padding(2);
            this.tabConversionReset.Name = "tabConversionReset";
            this.tabConversionReset.SelectedIndex = 0;
            this.tabConversionReset.Size = new System.Drawing.Size(794, 395);
            this.tabConversionReset.TabIndex = 0;
            this.tabConversionReset.SelectedIndexChanged += new System.EventHandler(this.tabConversionReset_SelectedIndexChanged);
            // 
            // JobsForConversionReset
            // 
            this.JobsForConversionReset.Controls.Add(this.btnResetJobConvErrors);
            this.JobsForConversionReset.Controls.Add(this.btnResetJobs);
            this.JobsForConversionReset.Controls.Add(this.lblResetJobsConfirm);
            this.JobsForConversionReset.Controls.Add(this.listConversionResetJobs);
            this.JobsForConversionReset.Controls.Add(this.lblResetJobs);
            this.JobsForConversionReset.Location = new System.Drawing.Point(4, 27);
            this.JobsForConversionReset.Margin = new System.Windows.Forms.Padding(2);
            this.JobsForConversionReset.Name = "JobsForConversionReset";
            this.JobsForConversionReset.Padding = new System.Windows.Forms.Padding(2);
            this.JobsForConversionReset.Size = new System.Drawing.Size(786, 364);
            this.JobsForConversionReset.TabIndex = 0;
            this.JobsForConversionReset.Text = "Reset Conversion Status By Job";
            this.JobsForConversionReset.UseVisualStyleBackColor = true;
            // 
            // btnResetJobConvErrors
            // 
            this.btnResetJobConvErrors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetJobConvErrors.Location = new System.Drawing.Point(608, 333);
            this.btnResetJobConvErrors.Margin = new System.Windows.Forms.Padding(2);
            this.btnResetJobConvErrors.Name = "btnResetJobConvErrors";
            this.btnResetJobConvErrors.Size = new System.Drawing.Size(96, 23);
            this.btnResetJobConvErrors.TabIndex = 4;
            this.btnResetJobConvErrors.Text = "Reset Errors";
            this.btnResetJobConvErrors.UseVisualStyleBackColor = true;
            this.btnResetJobConvErrors.Click += new System.EventHandler(this.btnResetJobConvErrors_Click);
            // 
            // btnResetJobs
            // 
            this.btnResetJobs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetJobs.Location = new System.Drawing.Point(716, 333);
            this.btnResetJobs.Margin = new System.Windows.Forms.Padding(2);
            this.btnResetJobs.Name = "btnResetJobs";
            this.btnResetJobs.Size = new System.Drawing.Size(56, 23);
            this.btnResetJobs.TabIndex = 3;
            this.btnResetJobs.Text = "Reset";
            this.btnResetJobs.UseVisualStyleBackColor = true;
            this.btnResetJobs.Click += new System.EventHandler(this.btnResetJobs_Click);
            // 
            // lblResetJobsConfirm
            // 
            this.lblResetJobsConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblResetJobsConfirm.AutoSize = true;
            this.lblResetJobsConfirm.Location = new System.Drawing.Point(4, 333);
            this.lblResetJobsConfirm.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblResetJobsConfirm.Name = "lblResetJobsConfirm";
            this.lblResetJobsConfirm.Size = new System.Drawing.Size(83, 18);
            this.lblResetJobsConfirm.TabIndex = 2;
            this.lblResetJobsConfirm.Text = "Resetting...";
            this.lblResetJobsConfirm.Visible = false;
            // 
            // listConversionResetJobs
            // 
            this.listConversionResetJobs.AllowColumnReorder = true;
            this.listConversionResetJobs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listConversionResetJobs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listConversionResetJobs.CheckBoxes = true;
            this.listConversionResetJobs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.resetJobID,
            this.resetJobName,
            this.resetComment});
            this.listConversionResetJobs.FullRowSelect = true;
            this.listConversionResetJobs.Location = new System.Drawing.Point(8, 28);
            this.listConversionResetJobs.Margin = new System.Windows.Forms.Padding(2);
            this.listConversionResetJobs.MultiSelect = false;
            this.listConversionResetJobs.Name = "listConversionResetJobs";
            this.listConversionResetJobs.Size = new System.Drawing.Size(774, 299);
            this.listConversionResetJobs.TabIndex = 1;
            this.listConversionResetJobs.UseCompatibleStateImageBehavior = false;
            this.listConversionResetJobs.View = System.Windows.Forms.View.Details;
            this.listConversionResetJobs.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listResetJobs_ItemChecked);
            // 
            // resetJobID
            // 
            this.resetJobID.Text = "Job ID";
            this.resetJobID.Width = 125;
            // 
            // resetJobName
            // 
            this.resetJobName.Text = "Job Name";
            this.resetJobName.Width = 250;
            // 
            // resetComment
            // 
            this.resetComment.Text = "Comment";
            this.resetComment.Width = 650;
            // 
            // lblResetJobs
            // 
            this.lblResetJobs.AutoSize = true;
            this.lblResetJobs.Location = new System.Drawing.Point(5, 6);
            this.lblResetJobs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblResetJobs.Name = "lblResetJobs";
            this.lblResetJobs.Size = new System.Drawing.Size(42, 18);
            this.lblResetJobs.TabIndex = 0;
            this.lblResetJobs.Text = "Jobs:";
            // 
            // BatchesForConversionReset
            // 
            this.BatchesForConversionReset.Controls.Add(this.btnConvResetErrors);
            this.BatchesForConversionReset.Controls.Add(this.btnResetBatches);
            this.BatchesForConversionReset.Controls.Add(this.lblResetBatchConfirm);
            this.BatchesForConversionReset.Controls.Add(this.listResetBatches);
            this.BatchesForConversionReset.Controls.Add(this.lblResetbatches);
            this.BatchesForConversionReset.Location = new System.Drawing.Point(4, 27);
            this.BatchesForConversionReset.Margin = new System.Windows.Forms.Padding(2);
            this.BatchesForConversionReset.Name = "BatchesForConversionReset";
            this.BatchesForConversionReset.Padding = new System.Windows.Forms.Padding(2);
            this.BatchesForConversionReset.Size = new System.Drawing.Size(786, 364);
            this.BatchesForConversionReset.TabIndex = 1;
            this.BatchesForConversionReset.Text = "Reset Conversion Status By Batch";
            this.BatchesForConversionReset.UseVisualStyleBackColor = true;
            // 
            // btnConvResetErrors
            // 
            this.btnConvResetErrors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConvResetErrors.Location = new System.Drawing.Point(601, 343);
            this.btnConvResetErrors.Margin = new System.Windows.Forms.Padding(2);
            this.btnConvResetErrors.Name = "btnConvResetErrors";
            this.btnConvResetErrors.Size = new System.Drawing.Size(96, 23);
            this.btnConvResetErrors.TabIndex = 4;
            this.btnConvResetErrors.Text = "Reset Errors";
            this.btnConvResetErrors.UseVisualStyleBackColor = true;
            this.btnConvResetErrors.Click += new System.EventHandler(this.btnConvResetErrors_Click);
            // 
            // btnResetBatches
            // 
            this.btnResetBatches.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetBatches.Location = new System.Drawing.Point(716, 343);
            this.btnResetBatches.Margin = new System.Windows.Forms.Padding(2);
            this.btnResetBatches.Name = "btnResetBatches";
            this.btnResetBatches.Size = new System.Drawing.Size(56, 23);
            this.btnResetBatches.TabIndex = 3;
            this.btnResetBatches.Text = "Reset";
            this.btnResetBatches.UseVisualStyleBackColor = true;
            this.btnResetBatches.Click += new System.EventHandler(this.btnResetBatches_Click);
            // 
            // lblResetBatchConfirm
            // 
            this.lblResetBatchConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblResetBatchConfirm.AutoSize = true;
            this.lblResetBatchConfirm.Location = new System.Drawing.Point(4, 343);
            this.lblResetBatchConfirm.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblResetBatchConfirm.Name = "lblResetBatchConfirm";
            this.lblResetBatchConfirm.Size = new System.Drawing.Size(83, 18);
            this.lblResetBatchConfirm.TabIndex = 2;
            this.lblResetBatchConfirm.Text = "Resetting...";
            this.lblResetBatchConfirm.Visible = false;
            // 
            // listResetBatches
            // 
            this.listResetBatches.AllowColumnReorder = true;
            this.listResetBatches.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listResetBatches.CheckBoxes = true;
            this.listResetBatches.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.rstBatchID,
            this.rstJob,
            this.rstSize,
            this.rstStatus,
            this.rstStart,
            this.rstEnd,
            this.rstUser,
            this.rstWkstn});
            this.listResetBatches.Location = new System.Drawing.Point(4, 27);
            this.listResetBatches.Margin = new System.Windows.Forms.Padding(2);
            this.listResetBatches.Name = "listResetBatches";
            this.listResetBatches.Size = new System.Drawing.Size(778, 309);
            this.listResetBatches.TabIndex = 1;
            this.listResetBatches.UseCompatibleStateImageBehavior = false;
            this.listResetBatches.View = System.Windows.Forms.View.Details;
            this.listResetBatches.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listResetBatches_ItemChecked);
            // 
            // rstBatchID
            // 
            this.rstBatchID.Text = "Batch ID";
            this.rstBatchID.Width = 100;
            // 
            // rstJob
            // 
            this.rstJob.Text = "Job ID";
            this.rstJob.Width = 150;
            // 
            // rstSize
            // 
            this.rstSize.Text = "Size";
            this.rstSize.Width = 90;
            // 
            // rstStatus
            // 
            this.rstStatus.Text = "Status";
            this.rstStatus.Width = 200;
            // 
            // rstStart
            // 
            this.rstStart.Text = "Start Time";
            this.rstStart.Width = 200;
            // 
            // rstEnd
            // 
            this.rstEnd.Text = "End Time";
            this.rstEnd.Width = 200;
            // 
            // rstUser
            // 
            this.rstUser.Text = "User";
            this.rstUser.Width = 100;
            // 
            // rstWkstn
            // 
            this.rstWkstn.Text = "WorkStation";
            this.rstWkstn.Width = 106;
            // 
            // lblResetbatches
            // 
            this.lblResetbatches.AutoSize = true;
            this.lblResetbatches.Location = new System.Drawing.Point(5, 6);
            this.lblResetbatches.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblResetbatches.Name = "lblResetbatches";
            this.lblResetbatches.Size = new System.Drawing.Size(209, 18);
            this.lblResetbatches.TabIndex = 0;
            this.lblResetbatches.Text = "Completed Conversion Batches:";
            // 
            // AccountForConversionReset
            // 
            this.AccountForConversionReset.Controls.Add(this.btnResetAccount);
            this.AccountForConversionReset.Controls.Add(this.txtResetAccount);
            this.AccountForConversionReset.Controls.Add(this.lblResetAccount);
            this.AccountForConversionReset.Controls.Add(this.lblResetAccountConfirm);
            this.AccountForConversionReset.Controls.Add(this.listResetAccountJobs);
            this.AccountForConversionReset.Controls.Add(this.lblResetAccountJobs);
            this.AccountForConversionReset.Location = new System.Drawing.Point(4, 27);
            this.AccountForConversionReset.Margin = new System.Windows.Forms.Padding(2);
            this.AccountForConversionReset.Name = "AccountForConversionReset";
            this.AccountForConversionReset.Size = new System.Drawing.Size(786, 364);
            this.AccountForConversionReset.TabIndex = 2;
            this.AccountForConversionReset.Text = "Reset Conversion Status By Account";
            this.AccountForConversionReset.UseVisualStyleBackColor = true;
            // 
            // btnResetAccount
            // 
            this.btnResetAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetAccount.Location = new System.Drawing.Point(678, 343);
            this.btnResetAccount.Margin = new System.Windows.Forms.Padding(2);
            this.btnResetAccount.Name = "btnResetAccount";
            this.btnResetAccount.Size = new System.Drawing.Size(92, 23);
            this.btnResetAccount.TabIndex = 5;
            this.btnResetAccount.Text = "Reset Case";
            this.btnResetAccount.UseVisualStyleBackColor = true;
            this.btnResetAccount.Click += new System.EventHandler(this.btnResetAccount_Click);
            // 
            // txtResetAccount
            // 
            this.txtResetAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtResetAccount.Location = new System.Drawing.Point(498, 343);
            this.txtResetAccount.Margin = new System.Windows.Forms.Padding(2);
            this.txtResetAccount.Name = "txtResetAccount";
            this.txtResetAccount.Size = new System.Drawing.Size(169, 23);
            this.txtResetAccount.TabIndex = 4;
            // 
            // lblResetAccount
            // 
            this.lblResetAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblResetAccount.AutoSize = true;
            this.lblResetAccount.Location = new System.Drawing.Point(382, 343);
            this.lblResetAccount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblResetAccount.Name = "lblResetAccount";
            this.lblResetAccount.Size = new System.Drawing.Size(119, 18);
            this.lblResetAccount.TabIndex = 3;
            this.lblResetAccount.Text = "Account Number:";
            // 
            // lblResetAccountConfirm
            // 
            this.lblResetAccountConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblResetAccountConfirm.AutoSize = true;
            this.lblResetAccountConfirm.Location = new System.Drawing.Point(6, 343);
            this.lblResetAccountConfirm.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblResetAccountConfirm.Name = "lblResetAccountConfirm";
            this.lblResetAccountConfirm.Size = new System.Drawing.Size(83, 18);
            this.lblResetAccountConfirm.TabIndex = 2;
            this.lblResetAccountConfirm.Text = "Resetting...";
            this.lblResetAccountConfirm.Visible = false;
            // 
            // listResetAccountJobs
            // 
            this.listResetAccountJobs.AllowColumnReorder = true;
            this.listResetAccountJobs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listResetAccountJobs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listResetAccountJobs.CheckBoxes = true;
            this.listResetAccountJobs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.resetAccountJobID,
            this.resetAccountJobName,
            this.resetAccountJobComment});
            this.listResetAccountJobs.Location = new System.Drawing.Point(6, 25);
            this.listResetAccountJobs.Margin = new System.Windows.Forms.Padding(2);
            this.listResetAccountJobs.MultiSelect = false;
            this.listResetAccountJobs.Name = "listResetAccountJobs";
            this.listResetAccountJobs.Size = new System.Drawing.Size(776, 314);
            this.listResetAccountJobs.TabIndex = 1;
            this.listResetAccountJobs.UseCompatibleStateImageBehavior = false;
            this.listResetAccountJobs.View = System.Windows.Forms.View.Details;
            this.listResetAccountJobs.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listResetAccountJobs_ItemChecked);
            // 
            // resetAccountJobID
            // 
            this.resetAccountJobID.Text = "Job ID";
            this.resetAccountJobID.Width = 100;
            // 
            // resetAccountJobName
            // 
            this.resetAccountJobName.Text = "Job Name";
            this.resetAccountJobName.Width = 200;
            // 
            // resetAccountJobComment
            // 
            this.resetAccountJobComment.Text = "Comment";
            this.resetAccountJobComment.Width = 735;
            // 
            // lblResetAccountJobs
            // 
            this.lblResetAccountJobs.AutoSize = true;
            this.lblResetAccountJobs.Location = new System.Drawing.Point(3, 3);
            this.lblResetAccountJobs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblResetAccountJobs.Name = "lblResetAccountJobs";
            this.lblResetAccountJobs.Size = new System.Drawing.Size(42, 18);
            this.lblResetAccountJobs.TabIndex = 0;
            this.lblResetAccountJobs.Text = "Jobs:";
            // 
            // ResetIngestion
            // 
            this.ResetIngestion.Controls.Add(this.tabIngestionReset);
            this.ResetIngestion.Location = new System.Drawing.Point(4, 27);
            this.ResetIngestion.Margin = new System.Windows.Forms.Padding(2);
            this.ResetIngestion.Name = "ResetIngestion";
            this.ResetIngestion.Padding = new System.Windows.Forms.Padding(2);
            this.ResetIngestion.Size = new System.Drawing.Size(792, 386);
            this.ResetIngestion.TabIndex = 1;
            this.ResetIngestion.Text = "Reset Status for Ingestion";
            this.ResetIngestion.UseVisualStyleBackColor = true;
            // 
            // tabIngestionReset
            // 
            this.tabIngestionReset.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabIngestionReset.Controls.Add(this.JobsForIngestionReset);
            this.tabIngestionReset.Controls.Add(this.ConvBatchesForIngestionReset);
            this.tabIngestionReset.Controls.Add(this.MigBatchesForIngestionReset);
            this.tabIngestionReset.Controls.Add(this.IdForIngestionReset);
            this.tabIngestionReset.Location = new System.Drawing.Point(0, 0);
            this.tabIngestionReset.Margin = new System.Windows.Forms.Padding(2);
            this.tabIngestionReset.Name = "tabIngestionReset";
            this.tabIngestionReset.SelectedIndex = 0;
            this.tabIngestionReset.Size = new System.Drawing.Size(800, 439);
            this.tabIngestionReset.TabIndex = 0;
            this.tabIngestionReset.SelectedIndexChanged += new System.EventHandler(this.tabIngestionReset_SelectedIndexChanged);
            // 
            // JobsForIngestionReset
            // 
            this.JobsForIngestionReset.Controls.Add(this.btnIngestionResetJobs);
            this.JobsForIngestionReset.Controls.Add(this.lblIngestionResetJobsConfirm);
            this.JobsForIngestionReset.Controls.Add(this.listIngestionResetJobs);
            this.JobsForIngestionReset.Controls.Add(this.lblIngestionResetJobs);
            this.JobsForIngestionReset.Location = new System.Drawing.Point(4, 27);
            this.JobsForIngestionReset.Margin = new System.Windows.Forms.Padding(2);
            this.JobsForIngestionReset.Name = "JobsForIngestionReset";
            this.JobsForIngestionReset.Padding = new System.Windows.Forms.Padding(2);
            this.JobsForIngestionReset.Size = new System.Drawing.Size(792, 408);
            this.JobsForIngestionReset.TabIndex = 0;
            this.JobsForIngestionReset.Text = "Reset By Job";
            this.JobsForIngestionReset.UseVisualStyleBackColor = true;
            // 
            // btnIngestionResetJobs
            // 
            this.btnIngestionResetJobs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIngestionResetJobs.Location = new System.Drawing.Point(716, 344);
            this.btnIngestionResetJobs.Margin = new System.Windows.Forms.Padding(2);
            this.btnIngestionResetJobs.Name = "btnIngestionResetJobs";
            this.btnIngestionResetJobs.Size = new System.Drawing.Size(56, 23);
            this.btnIngestionResetJobs.TabIndex = 3;
            this.btnIngestionResetJobs.Text = "Reset";
            this.btnIngestionResetJobs.UseVisualStyleBackColor = true;
            this.btnIngestionResetJobs.Click += new System.EventHandler(this.btnIngestionResetJobs_Click);
            // 
            // lblIngestionResetJobsConfirm
            // 
            this.lblIngestionResetJobsConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblIngestionResetJobsConfirm.AutoSize = true;
            this.lblIngestionResetJobsConfirm.Location = new System.Drawing.Point(4, 344);
            this.lblIngestionResetJobsConfirm.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIngestionResetJobsConfirm.Name = "lblIngestionResetJobsConfirm";
            this.lblIngestionResetJobsConfirm.Size = new System.Drawing.Size(68, 18);
            this.lblIngestionResetJobsConfirm.TabIndex = 2;
            this.lblIngestionResetJobsConfirm.Text = "Resetting";
            this.lblIngestionResetJobsConfirm.Visible = false;
            // 
            // listIngestionResetJobs
            // 
            this.listIngestionResetJobs.AllowColumnReorder = true;
            this.listIngestionResetJobs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listIngestionResetJobs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listIngestionResetJobs.CheckBoxes = true;
            this.listIngestionResetJobs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.IngResetJobID,
            this.IngResetJobName,
            this.IngResetJobComment});
            this.listIngestionResetJobs.FullRowSelect = true;
            this.listIngestionResetJobs.Location = new System.Drawing.Point(8, 27);
            this.listIngestionResetJobs.Margin = new System.Windows.Forms.Padding(2);
            this.listIngestionResetJobs.MultiSelect = false;
            this.listIngestionResetJobs.Name = "listIngestionResetJobs";
            this.listIngestionResetJobs.Size = new System.Drawing.Size(774, 314);
            this.listIngestionResetJobs.TabIndex = 1;
            this.listIngestionResetJobs.UseCompatibleStateImageBehavior = false;
            this.listIngestionResetJobs.View = System.Windows.Forms.View.Details;
            this.listIngestionResetJobs.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listIngestionResetJobs_ItemChecked);
            // 
            // IngResetJobID
            // 
            this.IngResetJobID.Text = "Job ID";
            this.IngResetJobID.Width = 125;
            // 
            // IngResetJobName
            // 
            this.IngResetJobName.Text = "Job Name";
            this.IngResetJobName.Width = 250;
            // 
            // IngResetJobComment
            // 
            this.IngResetJobComment.Text = "Comment";
            this.IngResetJobComment.Width = 650;
            // 
            // lblIngestionResetJobs
            // 
            this.lblIngestionResetJobs.AutoSize = true;
            this.lblIngestionResetJobs.Location = new System.Drawing.Point(5, 6);
            this.lblIngestionResetJobs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIngestionResetJobs.Name = "lblIngestionResetJobs";
            this.lblIngestionResetJobs.Size = new System.Drawing.Size(42, 18);
            this.lblIngestionResetJobs.TabIndex = 0;
            this.lblIngestionResetJobs.Text = "Jobs:";
            // 
            // ConvBatchesForIngestionReset
            // 
            this.ConvBatchesForIngestionReset.Controls.Add(this.btnResetConvBatches);
            this.ConvBatchesForIngestionReset.Controls.Add(this.lblResetConvBatchConfirm);
            this.ConvBatchesForIngestionReset.Controls.Add(this.listResetConvBatches);
            this.ConvBatchesForIngestionReset.Controls.Add(this.lblResetConvBatches);
            this.ConvBatchesForIngestionReset.Location = new System.Drawing.Point(4, 27);
            this.ConvBatchesForIngestionReset.Margin = new System.Windows.Forms.Padding(2);
            this.ConvBatchesForIngestionReset.Name = "ConvBatchesForIngestionReset";
            this.ConvBatchesForIngestionReset.Padding = new System.Windows.Forms.Padding(2);
            this.ConvBatchesForIngestionReset.Size = new System.Drawing.Size(792, 408);
            this.ConvBatchesForIngestionReset.TabIndex = 1;
            this.ConvBatchesForIngestionReset.Text = "Reset By Conversion Batch";
            this.ConvBatchesForIngestionReset.UseVisualStyleBackColor = true;
            // 
            // btnResetConvBatches
            // 
            this.btnResetConvBatches.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetConvBatches.Location = new System.Drawing.Point(716, 341);
            this.btnResetConvBatches.Margin = new System.Windows.Forms.Padding(2);
            this.btnResetConvBatches.Name = "btnResetConvBatches";
            this.btnResetConvBatches.Size = new System.Drawing.Size(56, 23);
            this.btnResetConvBatches.TabIndex = 3;
            this.btnResetConvBatches.Text = "Reset";
            this.btnResetConvBatches.UseVisualStyleBackColor = true;
            this.btnResetConvBatches.Click += new System.EventHandler(this.btnResetConvBatches_Click);
            // 
            // lblResetConvBatchConfirm
            // 
            this.lblResetConvBatchConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblResetConvBatchConfirm.AutoSize = true;
            this.lblResetConvBatchConfirm.Location = new System.Drawing.Point(5, 341);
            this.lblResetConvBatchConfirm.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblResetConvBatchConfirm.Name = "lblResetConvBatchConfirm";
            this.lblResetConvBatchConfirm.Size = new System.Drawing.Size(68, 18);
            this.lblResetConvBatchConfirm.TabIndex = 2;
            this.lblResetConvBatchConfirm.Text = "Resetting";
            this.lblResetConvBatchConfirm.Visible = false;
            // 
            // listResetConvBatches
            // 
            this.listResetConvBatches.AllowColumnReorder = true;
            this.listResetConvBatches.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listResetConvBatches.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listResetConvBatches.CheckBoxes = true;
            this.listResetConvBatches.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.cbResetBatchID,
            this.cbResetJob,
            this.cbResetSize,
            this.cbResetStatus,
            this.cbResetStart,
            this.cbResetEnd,
            this.cbResetUser,
            this.cbResetWkstn});
            this.listResetConvBatches.FullRowSelect = true;
            this.listResetConvBatches.Location = new System.Drawing.Point(8, 35);
            this.listResetConvBatches.Margin = new System.Windows.Forms.Padding(2);
            this.listResetConvBatches.Name = "listResetConvBatches";
            this.listResetConvBatches.Size = new System.Drawing.Size(774, 297);
            this.listResetConvBatches.TabIndex = 1;
            this.listResetConvBatches.UseCompatibleStateImageBehavior = false;
            this.listResetConvBatches.View = System.Windows.Forms.View.Details;
            this.listResetConvBatches.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listResetConvBatches_ItemChecked);
            // 
            // cbResetBatchID
            // 
            this.cbResetBatchID.Text = "Batch ID";
            this.cbResetBatchID.Width = 100;
            // 
            // cbResetJob
            // 
            this.cbResetJob.Text = "Job";
            this.cbResetJob.Width = 150;
            // 
            // cbResetSize
            // 
            this.cbResetSize.Text = "Size";
            this.cbResetSize.Width = 80;
            // 
            // cbResetStatus
            // 
            this.cbResetStatus.Text = "Status";
            this.cbResetStatus.Width = 145;
            // 
            // cbResetStart
            // 
            this.cbResetStart.Text = "Start Time";
            this.cbResetStart.Width = 180;
            // 
            // cbResetEnd
            // 
            this.cbResetEnd.Text = "End Time";
            this.cbResetEnd.Width = 180;
            // 
            // cbResetUser
            // 
            this.cbResetUser.Text = "User";
            this.cbResetUser.Width = 100;
            // 
            // cbResetWkstn
            // 
            this.cbResetWkstn.Text = "Workstation";
            this.cbResetWkstn.Width = 106;
            // 
            // lblResetConvBatches
            // 
            this.lblResetConvBatches.AutoSize = true;
            this.lblResetConvBatches.Location = new System.Drawing.Point(5, 6);
            this.lblResetConvBatches.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblResetConvBatches.Name = "lblResetConvBatches";
            this.lblResetConvBatches.Size = new System.Drawing.Size(137, 18);
            this.lblResetConvBatches.TabIndex = 0;
            this.lblResetConvBatches.Text = "Conversion Batches:";
            // 
            // MigBatchesForIngestionReset
            // 
            this.MigBatchesForIngestionReset.Controls.Add(this.btnResetMigBatches);
            this.MigBatchesForIngestionReset.Controls.Add(this.btnResetErrors);
            this.MigBatchesForIngestionReset.Controls.Add(this.lblResetMigBatchConfirm);
            this.MigBatchesForIngestionReset.Controls.Add(this.listResetMigBatches);
            this.MigBatchesForIngestionReset.Controls.Add(this.lblResetMigBatches);
            this.MigBatchesForIngestionReset.Location = new System.Drawing.Point(4, 27);
            this.MigBatchesForIngestionReset.Margin = new System.Windows.Forms.Padding(2);
            this.MigBatchesForIngestionReset.Name = "MigBatchesForIngestionReset";
            this.MigBatchesForIngestionReset.Size = new System.Drawing.Size(792, 408);
            this.MigBatchesForIngestionReset.TabIndex = 2;
            this.MigBatchesForIngestionReset.Text = "Reset By Migration Batch";
            this.MigBatchesForIngestionReset.UseVisualStyleBackColor = true;
            // 
            // btnResetMigBatches
            // 
            this.btnResetMigBatches.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetMigBatches.Location = new System.Drawing.Point(660, 343);
            this.btnResetMigBatches.Margin = new System.Windows.Forms.Padding(2);
            this.btnResetMigBatches.Name = "btnResetMigBatches";
            this.btnResetMigBatches.Size = new System.Drawing.Size(94, 23);
            this.btnResetMigBatches.TabIndex = 4;
            this.btnResetMigBatches.Text = "Reset";
            this.btnResetMigBatches.UseVisualStyleBackColor = true;
            this.btnResetMigBatches.Click += new System.EventHandler(this.btnResetMigBatches_Click);
            // 
            // btnResetErrors
            // 
            this.btnResetErrors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetErrors.Location = new System.Drawing.Point(525, 343);
            this.btnResetErrors.Margin = new System.Windows.Forms.Padding(2);
            this.btnResetErrors.Name = "btnResetErrors";
            this.btnResetErrors.Size = new System.Drawing.Size(94, 23);
            this.btnResetErrors.TabIndex = 3;
            this.btnResetErrors.Text = "Reset Errors";
            this.btnResetErrors.UseVisualStyleBackColor = true;
            this.btnResetErrors.Click += new System.EventHandler(this.btnResetErrors_Click);
            // 
            // lblResetMigBatchConfirm
            // 
            this.lblResetMigBatchConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblResetMigBatchConfirm.AutoSize = true;
            this.lblResetMigBatchConfirm.Location = new System.Drawing.Point(3, 343);
            this.lblResetMigBatchConfirm.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblResetMigBatchConfirm.Name = "lblResetMigBatchConfirm";
            this.lblResetMigBatchConfirm.Size = new System.Drawing.Size(83, 18);
            this.lblResetMigBatchConfirm.TabIndex = 2;
            this.lblResetMigBatchConfirm.Text = "Resetting...";
            this.lblResetMigBatchConfirm.Visible = false;
            // 
            // listResetMigBatches
            // 
            this.listResetMigBatches.AllowColumnReorder = true;
            this.listResetMigBatches.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listResetMigBatches.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listResetMigBatches.CheckBoxes = true;
            this.listResetMigBatches.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.migResetBatches,
            this.migResetJob,
            this.migResetStatus,
            this.migResetStart,
            this.migResetEnd,
            this.MigResetUser,
            this.migResetWkstn,
            this.migResetComment});
            this.listResetMigBatches.FullRowSelect = true;
            this.listResetMigBatches.Location = new System.Drawing.Point(6, 24);
            this.listResetMigBatches.Margin = new System.Windows.Forms.Padding(2);
            this.listResetMigBatches.Name = "listResetMigBatches";
            this.listResetMigBatches.Size = new System.Drawing.Size(776, 313);
            this.listResetMigBatches.TabIndex = 1;
            this.listResetMigBatches.UseCompatibleStateImageBehavior = false;
            this.listResetMigBatches.View = System.Windows.Forms.View.Details;
            this.listResetMigBatches.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listResetMigBatches_ItemChecked);
            // 
            // migResetBatches
            // 
            this.migResetBatches.Text = "Batch ID";
            this.migResetBatches.Width = 100;
            // 
            // migResetJob
            // 
            this.migResetJob.Text = "Job";
            this.migResetJob.Width = 180;
            // 
            // migResetStatus
            // 
            this.migResetStatus.Text = "Status";
            this.migResetStatus.Width = 145;
            // 
            // migResetStart
            // 
            this.migResetStart.Text = "Start Time";
            this.migResetStart.Width = 180;
            // 
            // migResetEnd
            // 
            this.migResetEnd.Text = "End Time";
            this.migResetEnd.Width = 180;
            // 
            // MigResetUser
            // 
            this.MigResetUser.Text = "User";
            this.MigResetUser.Width = 130;
            // 
            // migResetWkstn
            // 
            this.migResetWkstn.Text = "WorkStation";
            this.migResetWkstn.Width = 100;
            // 
            // migResetComment
            // 
            this.migResetComment.Text = "Comment";
            this.migResetComment.Width = 84;
            // 
            // lblResetMigBatches
            // 
            this.lblResetMigBatches.AutoSize = true;
            this.lblResetMigBatches.Location = new System.Drawing.Point(3, 3);
            this.lblResetMigBatches.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblResetMigBatches.Name = "lblResetMigBatches";
            this.lblResetMigBatches.Size = new System.Drawing.Size(127, 18);
            this.lblResetMigBatches.TabIndex = 0;
            this.lblResetMigBatches.Text = "Migration Batches:";
            // 
            // IdForIngestionReset
            // 
            this.IdForIngestionReset.Controls.Add(this.btnIngestResetTaskID);
            this.IdForIngestionReset.Controls.Add(this.btnIngestResetAnnID);
            this.IdForIngestionReset.Controls.Add(this.btnIngestResetDocID);
            this.IdForIngestionReset.Controls.Add(this.btnIngestResetCaseID);
            this.IdForIngestionReset.Controls.Add(this.txtIngestResetID);
            this.IdForIngestionReset.Controls.Add(this.lblIngestResetID);
            this.IdForIngestionReset.Controls.Add(this.lblResetIDConfirm);
            this.IdForIngestionReset.Controls.Add(this.listIngestResetIDJobs);
            this.IdForIngestionReset.Controls.Add(this.lblResetIDJobs);
            this.IdForIngestionReset.Location = new System.Drawing.Point(4, 27);
            this.IdForIngestionReset.Margin = new System.Windows.Forms.Padding(2);
            this.IdForIngestionReset.Name = "IdForIngestionReset";
            this.IdForIngestionReset.Size = new System.Drawing.Size(792, 408);
            this.IdForIngestionReset.TabIndex = 3;
            this.IdForIngestionReset.Text = "Reset By ID";
            this.IdForIngestionReset.UseVisualStyleBackColor = true;
            // 
            // btnIngestResetTaskID
            // 
            this.btnIngestResetTaskID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIngestResetTaskID.Location = new System.Drawing.Point(651, 340);
            this.btnIngestResetTaskID.Margin = new System.Windows.Forms.Padding(2);
            this.btnIngestResetTaskID.Name = "btnIngestResetTaskID";
            this.btnIngestResetTaskID.Size = new System.Drawing.Size(131, 23);
            this.btnIngestResetTaskID.TabIndex = 8;
            this.btnIngestResetTaskID.Text = "Reset Task";
            this.btnIngestResetTaskID.UseVisualStyleBackColor = true;
            this.btnIngestResetTaskID.Click += new System.EventHandler(this.btnIngestResetTaskID_Click);
            // 
            // btnIngestResetAnnID
            // 
            this.btnIngestResetAnnID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIngestResetAnnID.Location = new System.Drawing.Point(512, 340);
            this.btnIngestResetAnnID.Margin = new System.Windows.Forms.Padding(2);
            this.btnIngestResetAnnID.Name = "btnIngestResetAnnID";
            this.btnIngestResetAnnID.Size = new System.Drawing.Size(131, 23);
            this.btnIngestResetAnnID.TabIndex = 7;
            this.btnIngestResetAnnID.Text = "Reset Annotation";
            this.btnIngestResetAnnID.UseVisualStyleBackColor = true;
            this.btnIngestResetAnnID.Click += new System.EventHandler(this.btnIngestResetAnnID_Click);
            // 
            // btnIngestResetDocID
            // 
            this.btnIngestResetDocID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIngestResetDocID.Location = new System.Drawing.Point(373, 340);
            this.btnIngestResetDocID.Margin = new System.Windows.Forms.Padding(2);
            this.btnIngestResetDocID.Name = "btnIngestResetDocID";
            this.btnIngestResetDocID.Size = new System.Drawing.Size(131, 23);
            this.btnIngestResetDocID.TabIndex = 6;
            this.btnIngestResetDocID.Text = "Reset Document";
            this.btnIngestResetDocID.UseVisualStyleBackColor = true;
            this.btnIngestResetDocID.Click += new System.EventHandler(this.btnIngestResetDocID_Click);
            // 
            // btnIngestResetCaseID
            // 
            this.btnIngestResetCaseID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIngestResetCaseID.Location = new System.Drawing.Point(235, 340);
            this.btnIngestResetCaseID.Margin = new System.Windows.Forms.Padding(2);
            this.btnIngestResetCaseID.Name = "btnIngestResetCaseID";
            this.btnIngestResetCaseID.Size = new System.Drawing.Size(131, 23);
            this.btnIngestResetCaseID.TabIndex = 5;
            this.btnIngestResetCaseID.Text = "Reset Case";
            this.btnIngestResetCaseID.UseVisualStyleBackColor = true;
            this.btnIngestResetCaseID.Click += new System.EventHandler(this.btnIngestResetCaseID_Click);
            // 
            // txtIngestResetID
            // 
            this.txtIngestResetID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIngestResetID.Location = new System.Drawing.Point(149, 340);
            this.txtIngestResetID.Margin = new System.Windows.Forms.Padding(2);
            this.txtIngestResetID.Name = "txtIngestResetID";
            this.txtIngestResetID.Size = new System.Drawing.Size(76, 23);
            this.txtIngestResetID.TabIndex = 4;
            // 
            // lblIngestResetID
            // 
            this.lblIngestResetID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblIngestResetID.AutoSize = true;
            this.lblIngestResetID.Location = new System.Drawing.Point(119, 340);
            this.lblIngestResetID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIngestResetID.Name = "lblIngestResetID";
            this.lblIngestResetID.Size = new System.Drawing.Size(26, 18);
            this.lblIngestResetID.TabIndex = 3;
            this.lblIngestResetID.Text = "ID:";
            // 
            // lblResetIDConfirm
            // 
            this.lblResetIDConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblResetIDConfirm.AutoSize = true;
            this.lblResetIDConfirm.Location = new System.Drawing.Point(3, 340);
            this.lblResetIDConfirm.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblResetIDConfirm.Name = "lblResetIDConfirm";
            this.lblResetIDConfirm.Size = new System.Drawing.Size(83, 18);
            this.lblResetIDConfirm.TabIndex = 2;
            this.lblResetIDConfirm.Text = "Resetting...";
            this.lblResetIDConfirm.Visible = false;
            // 
            // listIngestResetIDJobs
            // 
            this.listIngestResetIDJobs.AllowColumnReorder = true;
            this.listIngestResetIDJobs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listIngestResetIDJobs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listIngestResetIDJobs.CheckBoxes = true;
            this.listIngestResetIDJobs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.resetIDJobID,
            this.resetIDJobName,
            this.resetIDJobComment});
            this.listIngestResetIDJobs.FullRowSelect = true;
            this.listIngestResetIDJobs.Location = new System.Drawing.Point(2, 24);
            this.listIngestResetIDJobs.Margin = new System.Windows.Forms.Padding(2);
            this.listIngestResetIDJobs.MultiSelect = false;
            this.listIngestResetIDJobs.Name = "listIngestResetIDJobs";
            this.listIngestResetIDJobs.Size = new System.Drawing.Size(780, 306);
            this.listIngestResetIDJobs.TabIndex = 1;
            this.listIngestResetIDJobs.UseCompatibleStateImageBehavior = false;
            this.listIngestResetIDJobs.View = System.Windows.Forms.View.Details;
            this.listIngestResetIDJobs.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listIngestResetIDJobs_ItemChecked);
            // 
            // resetIDJobID
            // 
            this.resetIDJobID.Text = "Job ID";
            this.resetIDJobID.Width = 125;
            // 
            // resetIDJobName
            // 
            this.resetIDJobName.Text = "Job Name";
            this.resetIDJobName.Width = 250;
            // 
            // resetIDJobComment
            // 
            this.resetIDJobComment.Text = "Comment";
            this.resetIDJobComment.Width = 661;
            // 
            // lblResetIDJobs
            // 
            this.lblResetIDJobs.AutoSize = true;
            this.lblResetIDJobs.Location = new System.Drawing.Point(3, 3);
            this.lblResetIDJobs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblResetIDJobs.Name = "lblResetIDJobs";
            this.lblResetIDJobs.Size = new System.Drawing.Size(42, 18);
            this.lblResetIDJobs.TabIndex = 0;
            this.lblResetIDJobs.Text = "Jobs:";
            // 
            // bgConverter
            // 
            this.bgConverter.WorkerReportsProgress = true;
            this.bgConverter.WorkerSupportsCancellation = true;
            this.bgConverter.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgConverter_DoWork);
            this.bgConverter.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgConverter_ProgressChanged);
            this.bgConverter.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgConverter_RunWorkerCompleted);
            // 
            // bgMigrator
            // 
            this.bgMigrator.WorkerReportsProgress = true;
            this.bgMigrator.WorkerSupportsCancellation = true;
            this.bgMigrator.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgMigrator_DoWork);
            this.bgMigrator.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgMigrator_ProgressChanged);
            this.bgMigrator.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgMigrator_RunWorkerCompleted);
            // 
            // MigrationManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(820, 546);
            this.Controls.Add(this.tabMain);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.lblMigrator);
            this.Controls.Add(this.btnExit);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MigrationManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pyramid Migration Manager";
            this.Shown += new System.EventHandler(this.MigrationManager_Shown);
            this.tabMain.ResumeLayout(false);
            this.pageConfigure.ResumeLayout(false);
            this.tabConfigure.ResumeLayout(false);
            this.createConfig.ResumeLayout(false);
            this.createConfig.PerformLayout();
            this.sourceConfig.ResumeLayout(false);
            this.tabConfigSource.ResumeLayout(false);
            this.sourceCases.ResumeLayout(false);
            this.sourceCases.PerformLayout();
            this.sourceDocuments.ResumeLayout(false);
            this.sourceDocuments.PerformLayout();
            this.sourceAnnotations.ResumeLayout(false);
            this.sourceAnnotations.PerformLayout();
            this.sourceTasks.ResumeLayout(false);
            this.sourceTasks.PerformLayout();
            this.targetConfig.ResumeLayout(false);
            this.tabConfigTarget.ResumeLayout(false);
            this.targetCases.ResumeLayout(false);
            this.targetCases.PerformLayout();
            this.targetDocuments.ResumeLayout(false);
            this.targetDocuments.PerformLayout();
            this.targetAnnotations.ResumeLayout(false);
            this.targetAnnotations.PerformLayout();
            this.targetTasks.ResumeLayout(false);
            this.targetTasks.PerformLayout();
            this.defineIdentifiers.ResumeLayout(false);
            this.defineIdentifiers.PerformLayout();
            this.pageDefine.ResumeLayout(false);
            this.tabDefine.ResumeLayout(false);
            this.defineConversions.ResumeLayout(false);
            this.defineConversions.PerformLayout();
            this.targetMappings.ResumeLayout(false);
            this.targetMappings.PerformLayout();
            this.defineJobs.ResumeLayout(false);
            this.defineJobs.PerformLayout();
            this.defineBatches.ResumeLayout(false);
            this.defineBatches.PerformLayout();
            this.pageLoad.ResumeLayout(false);
            this.tabLoad.ResumeLayout(false);
            this.JobLoad.ResumeLayout(false);
            this.JobLoad.PerformLayout();
            this.CaseLoad.ResumeLayout(false);
            this.CaseLoad.PerformLayout();
            this.DocumentLoad.ResumeLayout(false);
            this.DocumentLoad.PerformLayout();
            this.AnnotationLoad.ResumeLayout(false);
            this.AnnotationLoad.PerformLayout();
            this.TaskLoad.ResumeLayout(false);
            this.TaskLoad.PerformLayout();
            this.pagePrepare.ResumeLayout(false);
            this.tabPrepare.ResumeLayout(false);
            this.ChooseJob.ResumeLayout(false);
            this.ChooseJob.PerformLayout();
            this.ChooseBatchDefn.ResumeLayout(false);
            this.ChooseBatchDefn.PerformLayout();
            this.PerformConversion.ResumeLayout(false);
            this.PerformConversion.PerformLayout();
            this.ConvLogs.ResumeLayout(false);
            this.tabConvLog.ResumeLayout(false);
            this.ConvResultSession.ResumeLayout(false);
            this.ConvResultSession.PerformLayout();
            this.ConvResultBatch.ResumeLayout(false);
            this.ConvResultBatch.PerformLayout();
            this.ConvResultView.ResumeLayout(false);
            this.ConvResultView.PerformLayout();
            this.ConvErrorView.ResumeLayout(false);
            this.ConvErrorView.PerformLayout();
            this.ConvResults.ResumeLayout(false);
            this.ConvResults.PerformLayout();
            this.pageMigrate.ResumeLayout(false);
            this.tabMigrate.ResumeLayout(false);
            this.MigrateJob.ResumeLayout(false);
            this.MigrateJob.PerformLayout();
            this.MigrateBatch.ResumeLayout(false);
            this.MigrateBatch.PerformLayout();
            this.PerformIngest.ResumeLayout(false);
            this.PerformIngest.PerformLayout();
            this.IngestLogs.ResumeLayout(false);
            this.tabIngestLog.ResumeLayout(false);
            this.IngestResultSession.ResumeLayout(false);
            this.IngestResultSession.PerformLayout();
            this.IngestResultBatch.ResumeLayout(false);
            this.IngestResultBatch.PerformLayout();
            this.IngestResultView.ResumeLayout(false);
            this.IngestResultView.PerformLayout();
            this.IngestErrorView.ResumeLayout(false);
            this.IngestErrorView.PerformLayout();
            this.IngestResults.ResumeLayout(false);
            this.IngestResults.PerformLayout();
            this.pageReset.ResumeLayout(false);
            this.tabReset.ResumeLayout(false);
            this.ResetConversion.ResumeLayout(false);
            this.tabConversionReset.ResumeLayout(false);
            this.JobsForConversionReset.ResumeLayout(false);
            this.JobsForConversionReset.PerformLayout();
            this.BatchesForConversionReset.ResumeLayout(false);
            this.BatchesForConversionReset.PerformLayout();
            this.AccountForConversionReset.ResumeLayout(false);
            this.AccountForConversionReset.PerformLayout();
            this.ResetIngestion.ResumeLayout(false);
            this.tabIngestionReset.ResumeLayout(false);
            this.JobsForIngestionReset.ResumeLayout(false);
            this.JobsForIngestionReset.PerformLayout();
            this.ConvBatchesForIngestionReset.ResumeLayout(false);
            this.ConvBatchesForIngestionReset.PerformLayout();
            this.MigBatchesForIngestionReset.ResumeLayout(false);
            this.MigBatchesForIngestionReset.PerformLayout();
            this.IdForIngestionReset.ResumeLayout(false);
            this.IdForIngestionReset.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label lblMigrator;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage pageConfigure;
        private System.Windows.Forms.TabPage pageDefine;
        private System.Windows.Forms.TabPage pageLoad;
        private System.Windows.Forms.TabPage pagePrepare;
        private System.Windows.Forms.TabPage pageMigrate;
        private System.Windows.Forms.TabPage pageReset;
        private System.Windows.Forms.TabControl tabConfigure;
        private System.Windows.Forms.TabPage createConfig;
        private System.Windows.Forms.TabPage sourceConfig;
        private System.Windows.Forms.TabPage targetConfig;
        private System.Windows.Forms.Label lblConfig;
        private System.Windows.Forms.ListView lvConfigs;
        private System.Windows.Forms.ColumnHeader colConfigName;
        private System.Windows.Forms.ColumnHeader colLastUpdated;
        private System.Windows.Forms.ColumnHeader colConfigComment;
        private System.Windows.Forms.TabControl tabConfigSource;
        private System.Windows.Forms.TabPage sourceCases;
        private System.Windows.Forms.TabPage sourceDocuments;
        private System.Windows.Forms.TabPage sourceAnnotations;
        private System.Windows.Forms.TabPage sourceTasks;
        private System.Windows.Forms.Label lblSourceCaseConfig;
        private System.Windows.Forms.ListView lvSourceCase;
        private System.Windows.Forms.ColumnHeader srcColumn;
        private System.Windows.Forms.ColumnHeader srcName;
        private System.Windows.Forms.ColumnHeader srcDataType;
        private System.Windows.Forms.ColumnHeader srcLength;
        private System.Windows.Forms.ColumnHeader srcFormat;
        private System.Windows.Forms.ColumnHeader srcMultiValued;
        private System.Windows.Forms.ListView lvSourceDocument;
        private System.Windows.Forms.ColumnHeader colSrcDocColum;
        private System.Windows.Forms.ColumnHeader colSrcDocName;
        private System.Windows.Forms.ColumnHeader colSrcDocDataType;
        private System.Windows.Forms.ColumnHeader colSrcDocLength;
        private System.Windows.Forms.ColumnHeader colSrcDocFormat;
		private System.Windows.Forms.ColumnHeader colSrcDocMultiValued;
        private System.Windows.Forms.Label lblSourceDocConfig;
        private System.Windows.Forms.ListView lvSourceAnnotation;
        private System.Windows.Forms.ColumnHeader colSrcAnnColumn;
        private System.Windows.Forms.ColumnHeader colSrcAnnName;
        private System.Windows.Forms.ColumnHeader colSrcAnnDataType;
        private System.Windows.Forms.ColumnHeader colSrcAnnLength;
        private System.Windows.Forms.ColumnHeader colSrcAnnFormat;
		private System.Windows.Forms.ColumnHeader colSrcAnnMultiValued;
        private System.Windows.Forms.Label lblSourceAnnConfig;
        private System.Windows.Forms.ListView lvSourceTask;
        private System.Windows.Forms.ColumnHeader colSrcTaskColumn;
        private System.Windows.Forms.ColumnHeader colSrcTaskName;
        private System.Windows.Forms.ColumnHeader colSrcTaskDataType;
        private System.Windows.Forms.ColumnHeader colSrcTaskLength;
        private System.Windows.Forms.ColumnHeader colSrcTaskFormat;
		private System.Windows.Forms.ColumnHeader colSrcTaskMultiValued;
        private System.Windows.Forms.Label lblSourceTaskConfig;
        private System.Windows.Forms.TabControl tabConfigTarget;
        private System.Windows.Forms.TabPage targetMappings;
        private System.Windows.Forms.TabPage targetCases;
        private System.Windows.Forms.TabPage targetDocuments;
        private System.Windows.Forms.TabPage targetAnnotations;
        private System.Windows.Forms.TabPage targetTasks;
        private System.Windows.Forms.ListView lvTargetCaseTypes;
        private System.Windows.Forms.Label lblTargetCaseConfig;
        private System.Windows.Forms.ListView lvTargetCaseProperties;
        private System.Windows.Forms.Label lblTargetCaseProperties;
        private System.Windows.Forms.ListView lvTargetCaseFolders;
        private System.Windows.Forms.Label lblTargetCaseFolders;
        private System.Windows.Forms.ColumnHeader colTgtCaseFolderName;
        private System.Windows.Forms.ColumnHeader colTgtCaseFolderParent;
        private System.Windows.Forms.ColumnHeader colCasePropName;
        private System.Windows.Forms.ColumnHeader colCasePropSource;
        private System.Windows.Forms.ColumnHeader colCasePropType;
        private System.Windows.Forms.ColumnHeader colCasePropReq;
        private System.Windows.Forms.ColumnHeader colCasePropMapped;
        private System.Windows.Forms.ColumnHeader colCasePropComputed;
        private System.Windows.Forms.ColumnHeader colCasePropMapName;
        private System.Windows.Forms.ColumnHeader colTargetCaseType;
        private System.Windows.Forms.Label lblTargetDocConfig;
        private System.Windows.Forms.ListView lvTargetDocProperties;
        private System.Windows.Forms.Label lblTargetDocProperties;
        private System.Windows.Forms.ListView lvTargetDocClasses;
        private System.Windows.Forms.ColumnHeader colTargetDocClassName;
        private System.Windows.Forms.ColumnHeader colTgtDocPropName;
        private System.Windows.Forms.ColumnHeader colTgtDocPropSource;
        private System.Windows.Forms.ColumnHeader colTgtDocPropType;
        private System.Windows.Forms.ColumnHeader colTgtDocPropReq;
        private System.Windows.Forms.ColumnHeader colTgtDocPropMap;
        private System.Windows.Forms.ColumnHeader colTgtDocPropComputed;
        private System.Windows.Forms.ColumnHeader colTgtDocMapName;
		private System.Windows.Forms.Label lblTargetAnnConfig;
        private System.Windows.Forms.ListView lvTargetAnnProperties;
        private System.Windows.Forms.Label lblTargetAnnProperties;
        private System.Windows.Forms.ListView lvTargetAnnClasses;
        private System.Windows.Forms.ColumnHeader colTargetAnnClassName;
        private System.Windows.Forms.ColumnHeader colTgtAnnPropName;
        private System.Windows.Forms.ColumnHeader colTgtAnnPropSource;
        private System.Windows.Forms.ColumnHeader colTgtAnnPropType;
        private System.Windows.Forms.ColumnHeader colTgtAnnPropReq;
        private System.Windows.Forms.ColumnHeader colTgtAnnPropMap;
        private System.Windows.Forms.ColumnHeader colTgtAnnPropComputed;
        private System.Windows.Forms.ColumnHeader colTgtAnnMapName;
        private System.Windows.Forms.ListView lvTargetTaskTypes;
        private System.Windows.Forms.ColumnHeader colTgtTaskTypes;
		private System.Windows.Forms.ColumnHeader colTgtTaskCaseName;
        private System.Windows.Forms.Label lblTargetTaskConfig;
        private System.Windows.Forms.ListView lvTargetInitDocs;
        private System.Windows.Forms.Label lblTargetInitDocs;
        private System.Windows.Forms.Label lblTargetTaskProperties;
        private System.Windows.Forms.ListView lvTargetTaskProperties;
        private System.Windows.Forms.ColumnHeader colTgtTaskPropName;
        private System.Windows.Forms.ColumnHeader colTgtTaskPropSource;
        private System.Windows.Forms.ColumnHeader colTgtTaskPropType;
        private System.Windows.Forms.ColumnHeader colTgtPropTaskReq;
        private System.Windows.Forms.ColumnHeader colTgtTaskPropMap;
        private System.Windows.Forms.ColumnHeader colTgtTaskPropComputed;
        private System.Windows.Forms.ColumnHeader colTgtTaskMapName;
        private System.Windows.Forms.ColumnHeader colTaskDocInit;
        private System.Windows.Forms.ColumnHeader colDocInitDocClass;
        private System.Windows.Forms.ColumnHeader colInitDocSource;
        private System.Windows.Forms.TabControl tabDefine;
        private System.Windows.Forms.TabPage defineConversions;
        private System.Windows.Forms.ListView lvConvDefns;
        private System.Windows.Forms.ColumnHeader colCDName;
		private System.Windows.Forms.ColumnHeader colSingleDoc;
        private System.Windows.Forms.Label lblConvDefns;
        private System.Windows.Forms.TabPage defineBatches;
        private System.Windows.Forms.TabPage defineIdentifiers;
        private System.Windows.Forms.TabPage defineJobs;
        private System.Windows.Forms.Button btnAddBatchDefn;
		private System.Windows.Forms.Button btnUpdBatchDefn;
		private System.Windows.Forms.Button btnDelBatchDefn;
		private System.Windows.Forms.Button btnExpBatchDefn;
		private System.Windows.Forms.Button btnImpBatchDefn;
        private System.Windows.Forms.ListView lvBatchDefns;
        private System.Windows.Forms.Label lblBatchDefns;
        private System.Windows.Forms.ColumnHeader colBDName;
        private System.Windows.Forms.ColumnHeader colBDSize;
        private System.Windows.Forms.ColumnHeader colBDCondition;
        private System.Windows.Forms.ColumnHeader colBDSrcPrefix;
        private System.Windows.Forms.ColumnHeader colBDStagingPath;
        private System.Windows.Forms.ListView lvIdentifierDefns;
        private System.Windows.Forms.Label lblIdConfig;
        private System.Windows.Forms.ColumnHeader colIdentifier;
        private System.Windows.Forms.ColumnHeader colIdDefn;
        private System.Windows.Forms.Button btnAddJob;
		private System.Windows.Forms.Button btnEditJob;
		private System.Windows.Forms.Button btnDeleteJob;
		private System.Windows.Forms.Button btnExportJob;
		private System.Windows.Forms.Button btnImportJob;
        private System.Windows.Forms.ListView lvJobDefns;
        private System.Windows.Forms.Label lblJobDefns;
        private System.Windows.Forms.ColumnHeader colJobName;
        private System.Windows.Forms.ColumnHeader colJobConfig;
        private System.Windows.Forms.ColumnHeader colJobConv;
        private System.Windows.Forms.ColumnHeader colJobCreated;
        private System.Windows.Forms.ColumnHeader colJobComment;
        private System.Windows.Forms.TabControl tabLoad;
        private System.Windows.Forms.TabPage JobLoad;
        private System.Windows.Forms.ListView lvLoadJobs;
        private System.Windows.Forms.ColumnHeader colLoadJobID;
        private System.Windows.Forms.ColumnHeader colLoadJobName;
        private System.Windows.Forms.ColumnHeader colLoadComment;
        private System.Windows.Forms.Label lblJobs;
        private System.Windows.Forms.TabPage CaseLoad;
        private System.Windows.Forms.TabPage DocumentLoad;
        private System.Windows.Forms.TabPage AnnotationLoad;
        private System.Windows.Forms.TabPage TaskLoad;
        private System.Windows.Forms.Button btnFindCaseID;
        private System.Windows.Forms.TextBox txtCaseID;
        private System.Windows.Forms.Label lblCaseID;
        private System.Windows.Forms.Label lblCaseDataCount;
        private System.Windows.Forms.ListView lvCaseData;
        private System.Windows.Forms.Label lblCaseData;
        private System.Windows.Forms.Button btnPrevCaseData;
        private System.Windows.Forms.Button btnLoadCaseData;
        private System.Windows.Forms.Button btnNextCaseData;
        private System.Windows.Forms.ListView lvDocumentData;
        private System.Windows.Forms.Label lblDocumentData;
        private System.Windows.Forms.Label lblDocumentDataCount;
        private System.Windows.Forms.Button btnLoadDocumentData;
        private System.Windows.Forms.Button btnNextDocumentData;
        private System.Windows.Forms.Button btnPrevDocumentData;
        private System.Windows.Forms.Button btnFindDocID;
        private System.Windows.Forms.TextBox txtDocID;
        private System.Windows.Forms.Label lblDocID;
        private System.Windows.Forms.Button btnLoadAnnotationData;
        private System.Windows.Forms.Button btnNextAnnotationData;
        private System.Windows.Forms.Button btnPrevAnnotationData;
        private System.Windows.Forms.Button btnFindAnnID;
        private System.Windows.Forms.TextBox txtAnnID;
        private System.Windows.Forms.Label lblAnnID;
        private System.Windows.Forms.Label lblAnnotationDataCount;
        private System.Windows.Forms.ListView lvAnnotationData;
        private System.Windows.Forms.Label lblAnnotationData;
        private System.Windows.Forms.ListView lvTaskData;
        private System.Windows.Forms.Label lblTaskData;
        private System.Windows.Forms.Button btnLoadTaskData;
        private System.Windows.Forms.Button btnNextTaskData;
        private System.Windows.Forms.Button btnPrevTaskData;
        private System.Windows.Forms.Button btnFindTaskID;
        private System.Windows.Forms.TextBox txtTaskID;
        private System.Windows.Forms.Label lblTaskID;
        private System.Windows.Forms.Label lblTaskDataCount;
        private System.Windows.Forms.TabControl tabPrepare;
        private System.Windows.Forms.TabPage ChooseJob;
        private System.Windows.Forms.TabPage ChooseBatchDefn;
        private System.Windows.Forms.TabPage PerformConversion;
        private System.Windows.Forms.TabPage ConvLogs;
        private System.Windows.Forms.TabPage ConvResults;
        private System.Windows.Forms.ListView listJobs;
        private System.Windows.Forms.ColumnHeader colConvJobID;
        private System.Windows.Forms.ColumnHeader colConvJobName;
        private System.Windows.Forms.ColumnHeader colConvComment;
        private System.Windows.Forms.Label lblConvJobs;
        private System.Windows.Forms.ListView listBatchDefns;
        private System.Windows.Forms.ColumnHeader colConvBatchDefnName;
        private System.Windows.Forms.ColumnHeader colConvBatchSize;
        private System.Windows.Forms.ColumnHeader colConvCondition;
        private System.Windows.Forms.ColumnHeader colConvSrcPrefix;
        private System.Windows.Forms.ColumnHeader colConvStagingPath;
        private System.Windows.Forms.Label lblConvBatchDefns;
        private System.ComponentModel.BackgroundWorker bgConverter;
        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.Button btnConvCancel;
        private System.Windows.Forms.Label lblConvBatch;
        private System.Windows.Forms.Label lblConvJob;
        private System.Windows.Forms.ListBox convStatus;
        private System.Windows.Forms.TabControl tabConvLog;
        private System.Windows.Forms.TabPage ConvResultSession;
        private System.Windows.Forms.TabPage ConvResultBatch;
        private System.Windows.Forms.TabPage ConvResultView;
        private System.Windows.Forms.TabPage ConvErrorView;
        private System.Windows.Forms.ListView listConvSessions;
        private System.Windows.Forms.ColumnHeader colConvSessionID;
        private System.Windows.Forms.ColumnHeader colConvStart;
        private System.Windows.Forms.ColumnHeader colConvEnd;
        private System.Windows.Forms.ColumnHeader colConvUser;
        private System.Windows.Forms.ColumnHeader colConvWkstn;
        private System.Windows.Forms.ColumnHeader colConvStatus;
        private System.Windows.Forms.Label lblConvSessions;
        private System.Windows.Forms.ListView listConvBatches;
        private System.Windows.Forms.ColumnHeader convLogBatchID;
        private System.Windows.Forms.ColumnHeader convBatchLogSize;
        private System.Windows.Forms.ColumnHeader convLogBatchStatus;
        private System.Windows.Forms.ColumnHeader convLogBatchStart;
        private System.Windows.Forms.ColumnHeader convLogBatchEnd;
        private System.Windows.Forms.ColumnHeader convLogBatchUser;
        private System.Windows.Forms.ColumnHeader convLogBatchWkstn;
        private System.Windows.Forms.Label lblConvBatches;
        private System.Windows.Forms.Label lblConvBatchLog;
        private System.Windows.Forms.Button btnNextConvLog;
        private System.Windows.Forms.Button btnPrevConvLog;
        private System.Windows.Forms.Label lblConvLogCount;
        private System.Windows.Forms.ListView listConvBatchLog;
        private System.Windows.Forms.ColumnHeader colCBLogTime;
        private System.Windows.Forms.ColumnHeader colCBLogEvent;
        private System.Windows.Forms.ColumnHeader colCBEventDesc;
        private System.Windows.Forms.ListView listConvBatchErrors;
        private System.Windows.Forms.ColumnHeader convErrorTime;
        private System.Windows.Forms.ColumnHeader convErrorEvent;
        private System.Windows.Forms.Label lblConvBatchErrors;
        private System.Windows.Forms.Button btnConvResultDocID;
        private System.Windows.Forms.TextBox txtConvResultDocID;
        private System.Windows.Forms.Label lblConvResultDocID;
        private System.Windows.Forms.Button btnConvResultCaseID;
        private System.Windows.Forms.TextBox txtConvResultCaseID;
        private System.Windows.Forms.Label lblConvResultCaseID;
        private System.Windows.Forms.TextBox txtConvResultBatch;
        private System.Windows.Forms.Label lblConvResultBatch;
        private System.Windows.Forms.TreeView treeConvResult;
        private System.Windows.Forms.TabControl tabMigrate;
        private System.Windows.Forms.TabPage MigrateJob;
        private System.Windows.Forms.TabPage MigrateBatch;
        private System.Windows.Forms.TabPage PerformIngest;
        private System.Windows.Forms.TabPage IngestLogs;
        private System.Windows.Forms.TabPage IngestResults;
        private System.Windows.Forms.ListView listMigrateJobs;
        private System.Windows.Forms.ColumnHeader colMigrateJobID;
        private System.Windows.Forms.ColumnHeader colMigrateJobName;
        private System.Windows.Forms.ColumnHeader colMigrateJobComment;
        private System.Windows.Forms.Label lblMigrateJobs;
        private System.Windows.Forms.ListView listMigrateBatches;
        private System.Windows.Forms.ColumnHeader migBatchID;
        private System.Windows.Forms.ColumnHeader migBatchSize;
        private System.Windows.Forms.ColumnHeader migBatchStart;
        private System.Windows.Forms.ColumnHeader migBatchEnd;
        private System.Windows.Forms.ColumnHeader migBatchUser;
        private System.Windows.Forms.ColumnHeader migBatchWkstn;
        private System.Windows.Forms.ColumnHeader migBatchStatus;
        private System.Windows.Forms.Label lblMigrateBatches;
        private System.ComponentModel.BackgroundWorker bgMigrator;
        private System.Windows.Forms.Label lblMigrateBatch;
        private System.Windows.Forms.Label lblMigrateJob;
        private System.Windows.Forms.Button btnMigrate;
        private System.Windows.Forms.Button btnMigrateCancel;
        private System.Windows.Forms.ListBox migrateStatus;
        private System.Windows.Forms.CheckBox ckTasks;
        private System.Windows.Forms.CheckBox ckAnnotations;
        private System.Windows.Forms.CheckBox ckDocuments;
        private System.Windows.Forms.CheckBox ckCases;
        private System.Windows.Forms.CheckBox ckDepthFirst;
        private System.Windows.Forms.TabControl tabIngestLog;
        private System.Windows.Forms.TabPage IngestResultSession;
        private System.Windows.Forms.ListView listIngestSessions;
        private System.Windows.Forms.ColumnHeader ingestSession;
        private System.Windows.Forms.ColumnHeader ingestStart;
        private System.Windows.Forms.ColumnHeader ingestEnd;
        private System.Windows.Forms.ColumnHeader ingestUser;
        private System.Windows.Forms.ColumnHeader ingestWkstn;
        private System.Windows.Forms.ColumnHeader ingestStatus;
        private System.Windows.Forms.Label lblIngestSessions;
        private System.Windows.Forms.TabPage IngestResultBatch;
        private System.Windows.Forms.TabPage IngestResultView;
        private System.Windows.Forms.TabPage IngestErrorView;
        private System.Windows.Forms.Label lblIngestBatches;
        private System.Windows.Forms.ListView listIngestBatches;
        private System.Windows.Forms.ColumnHeader ingBatchID;
        private System.Windows.Forms.ColumnHeader ingStartTime;
        private System.Windows.Forms.ColumnHeader ingEndTime;
        private System.Windows.Forms.ColumnHeader ingUser;
        private System.Windows.Forms.ColumnHeader ingWkstn;
        private System.Windows.Forms.ColumnHeader ingStatus;
        private System.Windows.Forms.ColumnHeader ingComment;
        private System.Windows.Forms.Button btnNextIngestLog;
        private System.Windows.Forms.Button btnPrevIngestLog;
        private System.Windows.Forms.Label lblIngestLogCount;
        private System.Windows.Forms.ListView listIngestBatchLog;
        private System.Windows.Forms.Label lblIngestBatchLog;
        private System.Windows.Forms.ColumnHeader ingOccurred;
        private System.Windows.Forms.ColumnHeader ingEvent;
        private System.Windows.Forms.ColumnHeader ingEventDesc;
        private System.Windows.Forms.ListView listIngestBatchErrors;
        private System.Windows.Forms.Label lblIngestBatchErrors;
        private System.Windows.Forms.ColumnHeader migOccurrence;
        private System.Windows.Forms.ColumnHeader migEventDesc;
        private System.Windows.Forms.Button btnMigResultTaskID;
        private System.Windows.Forms.Button btnMigResultAnnID;
        private System.Windows.Forms.Button btnMigResultDocID;
        private System.Windows.Forms.Button btnMigResultCaseID;
        private System.Windows.Forms.TextBox txtMigResultID;
        private System.Windows.Forms.Label lblMigResultID;
        private System.Windows.Forms.TreeView treeIngestResult;
        private System.Windows.Forms.TabControl tabReset;
        private System.Windows.Forms.TabPage ResetConversion;
        private System.Windows.Forms.TabPage ResetIngestion;
        private System.Windows.Forms.TabControl tabConversionReset;
        private System.Windows.Forms.TabPage JobsForConversionReset;
        private System.Windows.Forms.TabPage BatchesForConversionReset;
        private System.Windows.Forms.Button btnResetJobs;
        private System.Windows.Forms.Label lblResetJobsConfirm;
        private System.Windows.Forms.ListView listConversionResetJobs;
        private System.Windows.Forms.Label lblResetJobs;
        private System.Windows.Forms.ColumnHeader resetJobID;
        private System.Windows.Forms.ColumnHeader resetJobName;
        private System.Windows.Forms.ColumnHeader resetComment;
        private System.Windows.Forms.Button btnResetBatches;
        private System.Windows.Forms.Label lblResetBatchConfirm;
        private System.Windows.Forms.ListView listResetBatches;
        private System.Windows.Forms.Label lblResetbatches;
        private System.Windows.Forms.ColumnHeader rstBatchID;
        private System.Windows.Forms.ColumnHeader rstJob;
        private System.Windows.Forms.ColumnHeader rstSize;
        private System.Windows.Forms.ColumnHeader rstStatus;
        private System.Windows.Forms.ColumnHeader rstStart;
        private System.Windows.Forms.ColumnHeader rstEnd;
        private System.Windows.Forms.ColumnHeader rstUser;
        private System.Windows.Forms.ColumnHeader rstWkstn;
        private System.Windows.Forms.TabControl tabIngestionReset;
        private System.Windows.Forms.TabPage JobsForIngestionReset;
        private System.Windows.Forms.TabPage ConvBatchesForIngestionReset;
        private System.Windows.Forms.TabPage MigBatchesForIngestionReset;
        private System.Windows.Forms.TabPage IdForIngestionReset;
        private System.Windows.Forms.ListView listIngestionResetJobs;
        private System.Windows.Forms.Label lblIngestionResetJobs;
        private System.Windows.Forms.Button btnIngestionResetJobs;
        private System.Windows.Forms.Label lblIngestionResetJobsConfirm;
        private System.Windows.Forms.ColumnHeader IngResetJobID;
        private System.Windows.Forms.ColumnHeader IngResetJobName;
        private System.Windows.Forms.ColumnHeader IngResetJobComment;
        private System.Windows.Forms.ListView listResetConvBatches;
        private System.Windows.Forms.Label lblResetConvBatches;
        private System.Windows.Forms.Button btnResetConvBatches;
        private System.Windows.Forms.Label lblResetConvBatchConfirm;
        private System.Windows.Forms.ColumnHeader cbResetBatchID;
        private System.Windows.Forms.ColumnHeader cbResetJob;
        private System.Windows.Forms.ColumnHeader cbResetSize;
        private System.Windows.Forms.ColumnHeader cbResetStatus;
        private System.Windows.Forms.ColumnHeader cbResetStart;
        private System.Windows.Forms.ColumnHeader cbResetEnd;
        private System.Windows.Forms.ColumnHeader cbResetUser;
        private System.Windows.Forms.ColumnHeader cbResetWkstn;
        private System.Windows.Forms.Button btnResetMigBatches;
        private System.Windows.Forms.Button btnResetErrors;
        private System.Windows.Forms.Label lblResetMigBatchConfirm;
        private System.Windows.Forms.ListView listResetMigBatches;
        private System.Windows.Forms.Label lblResetMigBatches;
        private System.Windows.Forms.ColumnHeader migResetBatches;
        private System.Windows.Forms.ColumnHeader migResetJob;
        private System.Windows.Forms.ColumnHeader migResetStatus;
        private System.Windows.Forms.ColumnHeader migResetStart;
        private System.Windows.Forms.ColumnHeader migResetEnd;
        private System.Windows.Forms.ColumnHeader MigResetUser;
        private System.Windows.Forms.ColumnHeader migResetWkstn;
        private System.Windows.Forms.ColumnHeader migResetComment;
        private System.Windows.Forms.ListView listIngestResetIDJobs;
        private System.Windows.Forms.Label lblResetIDJobs;
        private System.Windows.Forms.Button btnIngestResetTaskID;
        private System.Windows.Forms.Button btnIngestResetAnnID;
        private System.Windows.Forms.Button btnIngestResetDocID;
        private System.Windows.Forms.Button btnIngestResetCaseID;
        private System.Windows.Forms.TextBox txtIngestResetID;
        private System.Windows.Forms.Label lblIngestResetID;
        private System.Windows.Forms.Label lblResetIDConfirm;
        private System.Windows.Forms.ColumnHeader resetIDJobID;
        private System.Windows.Forms.ColumnHeader resetIDJobName;
        private System.Windows.Forms.ColumnHeader resetIDJobComment;
		private System.Windows.Forms.TextBox txtMaxBatches;
        private System.Windows.Forms.Label lblMaxBatches;
		private System.Windows.Forms.ColumnHeader colAttachProperty;
        private System.Windows.Forms.ColumnHeader colDefaultDocClass;
		private System.Windows.Forms.Button btnConvResetErrors;
        private System.Windows.Forms.TabPage AccountForConversionReset;
        private System.Windows.Forms.ListView listResetAccountJobs;
        private System.Windows.Forms.ColumnHeader resetAccountJobID;
        private System.Windows.Forms.ColumnHeader resetAccountJobName;
        private System.Windows.Forms.ColumnHeader resetAccountJobComment;
        private System.Windows.Forms.Label lblResetAccountJobs;
        private System.Windows.Forms.Button btnResetAccount;
        private System.Windows.Forms.TextBox txtResetAccount;
        private System.Windows.Forms.Label lblResetAccount;
        private System.Windows.Forms.Label lblResetAccountConfirm;
		private System.Windows.Forms.Button btnResetJobConvErrors;
		private System.Windows.Forms.ColumnHeader colConfigLevel;
		private System.Windows.Forms.Button btnDeleteConfig;
        private System.Windows.Forms.Button btnUpdateConfig;
        private System.Windows.Forms.Button btnAddConfiguration;
		private System.Windows.Forms.Button btnExportConfig;
		private System.Windows.Forms.Button btnImportConfig;
		private System.Windows.Forms.Button btnUpdIdentifier;
		private System.Windows.Forms.Button btnExpIdentifier;
		private System.Windows.Forms.Button btnImpIdentifier;
		private System.Windows.Forms.Button btnSrcCfgCaseAdd;
        private System.Windows.Forms.Button btnSrcCfgCaseUpd;
        private System.Windows.Forms.Button btnSrcCfgCaseDel;
		private System.Windows.Forms.Button btnSrcCfgCaseExp;
		private System.Windows.Forms.Button btnSrcCfgCaseImp;
		private System.Windows.Forms.Button btnSrcCfgDocAdd;
        private System.Windows.Forms.Button btnSrcCfgDocUpd;
        private System.Windows.Forms.Button btnSrcCfgDocDel;
		private System.Windows.Forms.Button btnSrcCfgDocExp;
		private System.Windows.Forms.Button btnSrcCfgDocImp;
		private System.Windows.Forms.Button btnSrcCfgAnnAdd;
        private System.Windows.Forms.Button btnSrcCfgAnnUpd;
        private System.Windows.Forms.Button btnSrcCfgAnnDel;
		private System.Windows.Forms.Button btnSrcCfgAnnExp;
		private System.Windows.Forms.Button btnSrcCfgAnnImp;
		private System.Windows.Forms.Button btnSrcCfgTaskAdd;
        private System.Windows.Forms.Button btnSrcCfgTaskUpd;
        private System.Windows.Forms.Button btnSrcCfgTaskDel;
		private System.Windows.Forms.Button btnSrcCfgTaskExp;
		private System.Windows.Forms.Button btnSrcCfgTaskImp;

		private System.Windows.Forms.Button btnTgtCfgCaseAdd;
        private System.Windows.Forms.Button btnTgtCfgCaseUpd;
        private System.Windows.Forms.Button btnTgtCfgCaseDel;
		private System.Windows.Forms.Button btnTgtCfgCaseExp;
		private System.Windows.Forms.Button btnTgtCfgCaseImp;
		private System.Windows.Forms.Button btnTgtCfgDocAdd;
        private System.Windows.Forms.Button btnTgtCfgDocUpd;
        private System.Windows.Forms.Button btnTgtCfgDocDel;
		private System.Windows.Forms.Button btnTgtCfgDocExp;
		private System.Windows.Forms.Button btnTgtCfgDocImp;
		private System.Windows.Forms.Button btnTgtCfgAnnAdd;
        private System.Windows.Forms.Button btnTgtCfgAnnUpd;
        private System.Windows.Forms.Button btnTgtCfgAnnDel;
		private System.Windows.Forms.Button btnTgtCfgAnnExp;
		private System.Windows.Forms.Button btnTgtCfgAnnImp;
		private System.Windows.Forms.Button btnTgtCfgTaskAdd;
        private System.Windows.Forms.Button btnTgtCfgTaskUpd;
        private System.Windows.Forms.Button btnTgtCfgTaskDel;
		private System.Windows.Forms.Button btnTgtCfgTaskExp;
		private System.Windows.Forms.Button btnTgtCfgTaskImp;

		private System.Windows.Forms.Button btnConvDefnAdd;
        private System.Windows.Forms.Button btnConvDefnUpd;
        private System.Windows.Forms.Button btnConvDefnDel;
		private System.Windows.Forms.Button btnConvExport;
		private System.Windows.Forms.Button btnConvImport;

		private System.Windows.Forms.ListView lvMapDefns;
        private System.Windows.Forms.ColumnHeader colMapName;
        private System.Windows.Forms.Label lblMapDefns;
		private System.Windows.Forms.Button btnMapDefnAdd;
        private System.Windows.Forms.Button btnMapDefnUpd;
        private System.Windows.Forms.Button btnMapDefnDel;
		private System.Windows.Forms.Button btnMapDefnExport;
		private System.Windows.Forms.Button btnMapDefnImport;
    }
}

