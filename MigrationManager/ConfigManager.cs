﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace MigrationManager
{
    class ConfigManager
    {
        public ConfigManager()
        {
        }

        public String GetAppSetting(String key)
        {
            try
            {
                return ConfigurationManager.AppSettings[key];
            }
            catch
            {
                throw new ArgumentException(key + " not defined in MigrationManager.exe.config");
            }
        }
    }
}
