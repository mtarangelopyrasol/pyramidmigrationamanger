﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class ConvDefnDialog : Form
    {
        string dbCnxn = string.Empty;
        DialogOperation operation;
        string ID = string.Empty;
        Boolean isDirty = false;

        public ConvDefnDialog(string dbConnection, DialogOperation op)
        {
            dbCnxn = dbConnection;
            operation = op;

            InitializeComponent();

            lblHeader.Text = (op == DialogOperation.Add) ? "Add New Conversion Definition" : "Update Conversion Definiton";
            cmbSingle.SelectedIndex = 0;
        }

        public void Populate(string id, string name, string single)
        {
            ID = id;

            txtName.Text = name;
            for (int i = 0; i < cmbSingle.Items.Count; i++)
            {
                if (cmbSingle.Items[i].ToString() == single)
                {
                    cmbSingle.SelectedItem = cmbSingle.Items[i];
                    break;
                }
            }
        }

        private Boolean HasPassedValidation()
        {
            if (txtName.Text.Length == 0)
            {
                MessageBox.Show("Please specify a Conversion Name", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (txtName.Text.Length >= 64)
            {
                MessageBox.Show("Conversion Name is too long. Conversion Names can be at most 64 characters", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            return true;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (SyncConvDefn())
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }  
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMap_Click(object sender, EventArgs e)
        {
            if (SyncConvDefn())
            {
                using (ListMapDialog dlg = new ListMapDialog(dbCnxn, ID, txtName.Text, DialogOperation.Update))
                {
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                    }
                }
            }
        }

        private Boolean SyncConvDefn()
        {
            if (HasPassedValidation())
            {
                if (isDirty)
                {
                    string name = MigrationManager.EscapeSingleQuote(txtName.Text.Trim());
                    string single = cmbSingle.SelectedItem.ToString();
                    if (operation == DialogOperation.Add)
                    {
                        string sql = "INSERT INTO MgrConvDefns(ConvDefnName, MarshalToSingleDoc) VALUES ('" + name + "', " + single + ")";
                        int nAffected = DBAdapter.DBAdapter.AddToDB(dbCnxn, sql);
                        sql = "SELECT ID FROM MgrConvDefns WHERE ConvDefnName = '" + name + "' AND MarshalToSingleDoc = " + single;
                        XmlDocument xml = DBAdapter.DBAdapter.FetchInsertedRec(dbCnxn, sql);
                        ID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                        operation = DialogOperation.Update;
                    }
                    else
                    {
                        string sql = "UPDATE MgrConvDefns SET ConvDefnName = '" + name + "', MarshalToSingleDoc = " + single + " WHERE ID = " + ID;
                        int nAffected = DBAdapter.DBAdapter.UpdateDB(dbCnxn, sql);
                    }
                    isDirty = false;
                }
                return true;
            }
            return false;
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            isDirty = true;
        }

        private void cmbSingle_SelectedIndexChanged(object sender, EventArgs e)
        {
            isDirty = true;
        }
    }
}
