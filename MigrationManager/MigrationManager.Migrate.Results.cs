﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        private void btnMigResultCaseID_Click(object sender, EventArgs e)
        {
            string id = txtMigResultID.Text.Trim();

            if (id.Length > 0)
            {
                string jobName = GetJobName(listMigrateJobs.Items[currentMigrateJobIndex].Tag.ToString());

                DisplayCaseResult(false, treeIngestResult, jobName, "SELECT ID, MigrationID, CE_GUID, Status, MMAccountID FROM " + jobName + "_CECases WHERE ID = " + id);
            }
            else
            {
                MessageBox.Show("Specify an ID to find", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnMigResultDocID_Click(object sender, EventArgs e)
        {
            string id = txtMigResultID.Text.Trim();

            if (id.Length > 0)
            {
                string jobName = GetJobName(listMigrateJobs.Items[currentMigrateJobIndex].Tag.ToString());

                DisplayDocResult(false, treeIngestResult, jobName, "SELECT * FROM " + jobName + "_CEDocuments WHERE ID = " + id);
            }
            else
            {
                MessageBox.Show("Specify an ID to find", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnMigResultAnnID_Click(object sender, EventArgs e)
        {
            string id = txtMigResultID.Text.Trim();

            if (id.Length > 0)
            {
                string jobName = GetJobName(listMigrateJobs.Items[currentMigrateJobIndex].Tag.ToString());

                DisplayAnnResult(false, treeIngestResult, jobName, "SELECT * FROM " + jobName + "_CEAnnotations WHERE ID = " + id);
            }
            else
            {
                MessageBox.Show("Specify an ID to find", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnMigResultTaskID_Click(object sender, EventArgs e)
        {
            string id = txtMigResultID.Text.Trim();

            if (id.Length > 0)
            {
                string jobName = GetJobName(listMigrateJobs.Items[currentMigrateJobIndex].Tag.ToString());

                DisplayTaskResult(false, treeIngestResult, jobName, "SELECT * FROM " + jobName + "_CETasks WHERE ID = " + id);
            }
            else
            {
                MessageBox.Show("Specify an ID to find", "Migration Manager", MessageBoxButtons.OK);
            }
        } 
    }
}
