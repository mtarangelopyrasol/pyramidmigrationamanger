﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        Boolean loadJobSelected = false;
        int lvIndexLoadJobs = 0;
        string currentLoadJobID = string.Empty;
        Boolean caselevelJob = false;

        const Int32 ROW_SIZE = 30;

        string caseLBound = "1";
        string caseUBound = string.Empty;
        string caseTotals = string.Empty;
        string caseMaxID = string.Empty;

        string docLBound = "1";
        string docUBound = string.Empty;
        string docTotals = string.Empty;
        string docsMaxID = string.Empty;

        string annLBound = "1";
        string annUBound = string.Empty;
        string annTotals = string.Empty;
        string annsMaxID = string.Empty;

        string taskLBound = "1";
        string taskUBound = string.Empty;
        string taskTotals = string.Empty;
        string taskMaxID = string.Empty;

        private void tabLoad_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowLoadSubTab();
        }

        private void ShowLoadSubTab()
        {
            TabPage currentPage = tabLoad.TabPages[tabLoad.SelectedIndex];

            switch (currentPage.Name)
            {
                case "CaseLoad":
                    if (IsLoadJobSelected()) LoadCaseData(currentPage);
                    break;

                case "DocumentLoad":
                    if (IsLoadJobSelected()) LoadDocumentData(currentPage);
                    break;

                case "AnnotationLoad":
                    if (IsLoadJobSelected()) LoadAnnotationData(currentPage);
                    break;

                case "TaskLoad":
                    if (IsLoadJobSelected()) LoadTaskData(currentPage);
                    break;

                default: // Choose Job
                    RefreshLoadJobList();
                    break;
            }
            currentPage.BringToFront();  
        }

        private void RefreshLoadJobList()
        {
            lvLoadJobs.BeginUpdate();
            lvLoadJobs.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM MgrJobs ORDER BY JobName");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = lvLoadJobs.Items.Add(rowNode.GetAttribute("ID"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("JobName"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Comment"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                    lvLoadJobs.Items[lvIndexLoadJobs].Checked = true;
                    UpdateCurrentLoadJob();
                    lvLoadJobs.EnsureVisible(i - 1);
                    loadJobSelected = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Jobs : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                lvLoadJobs.EndUpdate();
            }
        }

        private void lvLoadJobs_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndexLoadJobs))
            {
                lvLoadJobs.Items[lvIndexLoadJobs].Checked = false;
                lvIndexLoadJobs = e.Item.Index;
                UpdateCurrentLoadJob();
            }
        }

        private void UpdateCurrentLoadJob()
        {
            currentLoadJobID = (string)(lvLoadJobs.Items[lvIndexLoadJobs].Tag);
            UpdateLoadTabPages();
        }

        private void UpdateLoadTabPages()
        {
            UpdateJobLevel();
            if (caselevelJob)
            {
                ShowTabPage(tabLoad, "CaseLoad", 1);
                ShowTabPage(tabLoad, "TaskLoad", 4);
                lblDocID.Text = "Case Account #:";
                lblAnnID.Text = "Case Account #:";
            }
            else
            {
                HideTabPage(tabLoad, "CaseLoad");
                HideTabPage(tabLoad, "TaskLoad");
                lblDocID.Text = "Legacy Doc ID:";
                lblAnnID.Text = "Legacy Doc ID:";
            }
        }

        private void UpdateJobLevel()
        {
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT c.ConfigLevel AS Level FROM MgrJobs j, MgrConfigurations c WHERE j.ID = " + currentLoadJobID + " AND j.ConfigID = c.ID");
                string level = xml.SelectNodes("//row")[0].Attributes["Level"].Value;
                caselevelJob = (level == "CASE") ? true : false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while checking Job's config level : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void LoadCaseData(TabPage page)
        {
            string tableName = GetJobName(currentLoadJobID) + "_CaseData";
            lblCaseData.Text = "Table Name: " + tableName;
            caseTotals = GetTotalCount(tableName);
            if (Convert.ToInt64(caseTotals) > 0) caseMaxID = GetMaxID(tableName);
            RefreshData(page, tableName, lvCaseData, caseLBound, caseUBound, caseTotals, lblCaseDataCount); 
        }

        private void btnFindCaseID_Click(object sender, EventArgs e)
        {
            string caseID = txtCaseID.Text.Trim();

            if (caseID.Length > 0)
            {
                string tableName = GetJobName(currentLoadJobID) + "_CaseData";
                caseTotals = GetTotalCount(tableName);

                TabPage currentPage = tabLoad.TabPages[tabLoad.SelectedIndex];
                string id = GetMMID(currentPage, tableName, caseID, currentLoadJobID);
                if (id == string.Empty)
                {
                    MessageBox.Show("No such record", "Migration Manager", MessageBoxButtons.OK);
                }
                else
                {
                    Int64 desired = Convert.ToInt64(id);
                    if ((desired > 0) && (Convert.ToInt64(caseTotals) > 0) && (desired <= Convert.ToInt64(caseMaxID)))
                    {
                        caseLBound = id;
                        caseUBound = string.Empty;

                        RefreshData(currentPage, tableName, lvCaseData, caseLBound, caseUBound, caseTotals, lblCaseDataCount);
                    }
                    else
                    {
                        MessageBox.Show("No such record", "Migration Manager", MessageBoxButtons.OK);
                    }
                }
            }
            else
            {
                MessageBox.Show("Specify a Case Account Number to find", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnPrevCaseData_Click(object sender, EventArgs e)
        {
            caseUBound = caseLBound;
            caseLBound = string.Empty;
            LoadCaseData(tabLoad.TabPages[tabLoad.SelectedIndex]);
        }

        private void btnNextCaseData_Click(object sender, EventArgs e)
        {
            caseLBound = caseUBound;
            caseUBound = string.Empty;
            LoadCaseData(tabLoad.TabPages[tabLoad.SelectedIndex]);
        }

        private void btnLoadCaseData_Click(object sender, EventArgs e)
        {
            string tableName = GetJobName(currentLoadJobID) + "_CaseData";

            using (LoadDataDialog dlg = new LoadDataDialog(dbConnection, tableName, "CfgCaseSource", GetConfigIDForJob(currentLoadJobID), false))
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    TabPage currentPage = tabLoad.TabPages[tabLoad.SelectedIndex];
                    caseLBound = caseTotals;
                    caseUBound = string.Empty;
                    caseTotals = GetTotalCount(tableName);
                    RefreshData(currentPage, tableName, lvCaseData, caseLBound, caseUBound, caseTotals, lblCaseDataCount);
                }
            }
        }

        private void LoadDocumentData(TabPage page)
        {
            string tableName = GetJobName(currentLoadJobID) + "_DocumentData";
            lblDocumentData.Text = "Table Name: " + tableName;
            docTotals = GetTotalCount(tableName);
            RefreshData(page, tableName, lvDocumentData, docLBound, docUBound, docTotals, lblDocumentDataCount);
        }

        private void btnFindDocID_Click(object sender, EventArgs e)
        {
            string caseID = txtDocID.Text.Trim();

            if (caseID.Length > 0)
            {
                string tableName = GetJobName(currentLoadJobID) + "_DocumentData";
                docTotals = GetTotalCount(tableName);
                if (Convert.ToInt64(docTotals) > 0) docsMaxID = GetMaxID(tableName);

                TabPage currentPage = tabLoad.TabPages[tabLoad.SelectedIndex];
                string id = GetMMID(currentPage, tableName, caseID, currentLoadJobID);
                if (id == string.Empty)
                {
                    MessageBox.Show("No such record", "Migration Manager", MessageBoxButtons.OK);
                }
                else
                {
                    Int64 desired = Convert.ToInt64(id);
                    if ((desired > 0) && (Convert.ToInt64(docTotals) > 0) && (desired <= Convert.ToInt64(docsMaxID)))
                    {
                        docLBound = id;
                        docUBound = string.Empty;

                        RefreshData(currentPage, tableName, lvDocumentData, docLBound, docUBound, docTotals, lblDocumentDataCount);
                    }
                    else
                    {
                        MessageBox.Show("No such record", "Migration Manager", MessageBoxButtons.OK);
                    }
                }
            }
            else
            {
                MessageBox.Show("Specify a Case Account Number to find", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnPrevDocumentData_Click(object sender, EventArgs e)
        {
            docUBound = docLBound;
            docLBound = string.Empty;
            LoadDocumentData(tabLoad.TabPages[tabLoad.SelectedIndex]);
        }

        private void btnNextDocumentData_Click(object sender, EventArgs e)
        {
            docLBound = docUBound;
            docUBound = string.Empty;
            LoadDocumentData(tabLoad.TabPages[tabLoad.SelectedIndex]);
        }

        private void btnLoadDocumentData_Click(object sender, EventArgs e)
        {
            string tableName = GetJobName(currentLoadJobID) + "_DocumentData";

            using (LoadDataDialog dlg = new LoadDataDialog(dbConnection, tableName, "CfgDocumentSource", GetConfigIDForJob(currentLoadJobID), false))
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    TabPage currentPage = tabLoad.TabPages[tabLoad.SelectedIndex];
                    docLBound = docTotals;
                    docUBound = string.Empty;
                    docTotals = GetTotalCount(tableName);
                    RefreshData(currentPage, tableName, lvDocumentData, docLBound, docUBound, docTotals, lblDocumentDataCount);
                }
            }
        }

        private void LoadAnnotationData(TabPage page)
        {
            string tableName = GetJobName(currentLoadJobID) + "_AnnotationData";
            lblAnnotationData.Text = "Table Name: " + tableName;
            annTotals = GetTotalCount(tableName);
            if (Convert.ToInt64(annTotals) > 0) annsMaxID = GetMaxID(tableName);
            RefreshData(page, tableName, lvAnnotationData, annLBound, annUBound, annTotals, lblAnnotationDataCount);
        }

        private void btnFindAnnID_Click(object sender, EventArgs e)
        {
            string caseID = txtAnnID.Text.Trim();

            if (caseID.Length > 0)
            {
                string tableName = GetJobName(currentLoadJobID) + "_AnnotationData";
                annTotals = GetTotalCount(tableName);

                TabPage currentPage = tabLoad.TabPages[tabLoad.SelectedIndex];
                string id = GetMMID(currentPage, tableName, caseID, currentLoadJobID);
                if (id == string.Empty)
                {
                    MessageBox.Show("No such record", "Migration Manager", MessageBoxButtons.OK);
                }
                else
                {
                    Int64 desired = Convert.ToInt64(id);
                    if ((desired > 0) && (Convert.ToInt64(annTotals) > 0) && (desired <= Convert.ToInt64(annsMaxID)))
                    {
                        annLBound = id;
                        annUBound = string.Empty;

                        RefreshData(currentPage, tableName, lvAnnotationData, annLBound, annUBound, annTotals, lblAnnotationDataCount);
                    }
                    else
                    {
                        MessageBox.Show("No such record", "Migration Manager", MessageBoxButtons.OK);
                    }
                }
            }
            else
            {
                MessageBox.Show("Specify a Case Account Number to find", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnPrevAnnotationData_Click(object sender, EventArgs e)
        {
            annUBound = annLBound;
            annLBound = string.Empty;
            LoadAnnotationData(tabLoad.TabPages[tabLoad.SelectedIndex]);
        }

        private void btnNextAnnotationData_Click(object sender, EventArgs e)
        {
            annLBound = annUBound;
            annUBound = string.Empty;
            LoadAnnotationData(tabLoad.TabPages[tabLoad.SelectedIndex]);
        }

        private void btnLoadAnnotationData_Click(object sender, EventArgs e)
        {
            string tableName = GetJobName(currentLoadJobID) + "_AnnotationData";

            using (LoadDataDialog dlg = new LoadDataDialog(dbConnection, tableName, "CfgAnnotationSource", GetConfigIDForJob(currentLoadJobID), false))
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    TabPage currentPage = tabLoad.TabPages[tabLoad.SelectedIndex];
                    annLBound = annTotals;
                    annUBound = string.Empty;
                    annTotals = GetTotalCount(tableName);
                    RefreshData(currentPage, tableName, lvAnnotationData, annLBound, annUBound, annTotals, lblAnnotationDataCount);
                }
            }
        }

        private void LoadTaskData(TabPage page)
        {
            string tableName = GetJobName(currentLoadJobID) + "_TaskData";
            lblTaskData.Text = "Table Name: " + tableName;
            taskTotals = GetTotalCount(tableName);
            if (Convert.ToInt64(taskTotals) > 0) taskMaxID = GetMaxID(tableName);
            RefreshData(page, tableName, lvTaskData, taskLBound, taskUBound, taskTotals, lblTaskDataCount);
        }

        private void btnFindTaskID_Click(object sender, EventArgs e)
        {
            string caseID = txtTaskID.Text.Trim();

            if (caseID.Length > 0)
            {
                string tableName = GetJobName(currentLoadJobID) + "_TaskData";
                taskTotals = GetTotalCount(tableName);

                TabPage currentPage = tabLoad.TabPages[tabLoad.SelectedIndex];
                string id = GetMMID(currentPage, tableName, caseID, currentLoadJobID);
                if (id == string.Empty)
                {
                    MessageBox.Show("No such record", "Migration Manager", MessageBoxButtons.OK);
                }
                else
                {
                    Int64 desired = Convert.ToInt64(id);
                    if ((desired > 0) && (Convert.ToInt64(taskTotals) > 0) && (desired <= Convert.ToInt64(taskMaxID)))
                    {
                        taskLBound = id;
                        taskUBound = string.Empty;

                        RefreshData(currentPage, tableName, lvTaskData, taskLBound, taskUBound, taskTotals, lblTaskDataCount);
                    }
                    else
                    {
                        MessageBox.Show("No such record", "Migration Manager", MessageBoxButtons.OK);
                    }
                }
            }
            else
            {
                MessageBox.Show("Specify a Case Account Number to find", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnPrevTaskData_Click(object sender, EventArgs e)
        {
            taskUBound = taskLBound;
            taskLBound = string.Empty;
            LoadTaskData(tabLoad.TabPages[tabLoad.SelectedIndex]);
        }

        private void btnNextTaskData_Click(object sender, EventArgs e)
        {
            taskLBound = taskUBound;
            taskUBound = string.Empty;
            LoadTaskData(tabLoad.TabPages[tabLoad.SelectedIndex]);
        }

        private void btnLoadTaskData_Click(object sender, EventArgs e)
        {
            string tableName = GetJobName(currentLoadJobID) + "_TaskData";

            using (LoadDataDialog dlg = new LoadDataDialog(dbConnection, tableName, "CfgTaskSource", GetConfigIDForJob(currentLoadJobID), false))
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    TabPage currentPage = tabLoad.TabPages[tabLoad.SelectedIndex];
                    taskLBound = taskTotals;
                    taskUBound = string.Empty;
                    taskTotals = GetTotalCount(tableName);
                    RefreshData(currentPage, tableName, lvTaskData, taskLBound, taskUBound, taskTotals, lblTaskDataCount);
                }
            }
        }
        
        private Boolean IsLoadJobSelected()
        {
            if (!loadJobSelected)
            {
                MessageBox.Show("Please choose a job first", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            return true;
        }

        private string GetJobName(string jobID)
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT MgrJobs.JobName AS JobName FROM MgrJobs WHERE ID = " + jobID);

            return xmlDoc.SelectNodes("//row")[0].Attributes["JobName"].Value;
        }

        private string GetConfigIDForJob(string jobID)
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT MgrJobs.ConfigID AS ConfigID FROM MgrJobs WHERE ID = " + jobID);

            return xmlDoc.SelectNodes("//row")[0].Attributes["ConfigID"].Value;
        }

        private string[] GetColumnNames(string tableName)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = '" + tableName + "' ORDER BY ORDINAL_POSITION");

            int count = xml.SelectNodes("//row").Count;

            string[] colNames = new string[count];

            int i = 0;
            foreach (XmlElement rowNode in xml.SelectNodes("//row"))
            {
                colNames[i++] = rowNode.GetAttribute("COLUMN_NAME");
            }
            return colNames;
        }

        private void RefreshData(TabPage page, string tableName, ListView lv, string lowerID, string upperID, string totalCount, Label countLabel)
        {
            string lBound = "1";
            string uBound = "0";

            Int32 pageSize = (lv.Size.Height / ROW_SIZE);

            lv.BeginUpdate();
            lv.Columns.Clear();
            lv.Items.Clear();
            try
            {
                string[] colNames = GetColumnNames(tableName);
                int colCount = colNames.Length;

                foreach (string colName in colNames)
                {
                    lv.Columns.Add(colName, 100, HorizontalAlignment.Left);
                }

                string sql = (lowerID == string.Empty) ? 
                    "SELECT * FROM (SELECT TOP " + pageSize.ToString() + " * FROM " + tableName + " WHERE ID <= " + upperID + " ORDER BY ID DESC) AS P ORDER BY ID" : 
                    "SELECT TOP " + pageSize.ToString() + " * FROM " + tableName + " WHERE ID >= " + lowerID + " ORDER BY ID";

                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
                int count = xml.SelectNodes("//row").Count;

                countLabel.Text = "Total Records: " + totalCount;
                
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        if (i == 0) lBound = rowNode.GetAttribute("ID");
                        uBound = rowNode.GetAttribute("ID");

                        ListViewItem lvi = lv.Items.Add(rowNode.GetAttribute(colNames[0]), i);
                        for (int j = 1; j < colCount; j++)
                        {
                            lvi.SubItems.Add(rowNode.GetAttribute(colNames[j]));
                        }
                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Data : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                lv.EndUpdate();
                UpdateBounds(page, lBound, uBound, pageSize);
            }
        }

        private void AdjustButtons(TabPage page, Int32 pageSize)
        {
            switch (page.Name)
            {
                case "CaseLoad":
                    btnPrevCaseData.Enabled = (caseLBound == "1") ? false : true;
                    btnNextCaseData.Enabled = (caseUBound == caseTotals) ? false : true;
                    btnFindCaseID.Enabled = (Convert.ToInt32(caseTotals) <= pageSize) ? false : true;
                    break;

                case "DocumentLoad":
                    btnPrevDocumentData.Enabled = (docLBound == "1") ? false : true;
                    btnNextDocumentData.Enabled = (docUBound == docTotals) ? false : true;
                    btnFindDocID.Enabled = (Convert.ToInt32(docTotals) <= pageSize) ? false : true;
                    break;

                case "AnnotationLoad":
                    btnPrevAnnotationData.Enabled = (annLBound == "1") ? false : true;
                    btnNextAnnotationData.Enabled = (annUBound == annTotals) ? false : true;
                    btnFindAnnID.Enabled = (Convert.ToInt32(annTotals) <= pageSize) ? false : true;
                    break;

                case "TaskLoad":
                    btnPrevTaskData.Enabled = (taskLBound == "1") ? false : true;
                    btnNextTaskData.Enabled = (taskUBound == taskTotals) ? false : true;
                    btnFindTaskID.Enabled = (Convert.ToInt32(taskTotals) <= pageSize) ? false : true;
                    break;

                default:
                    //TODO: raise exception
                    break;
            }
        }

        private void UpdateBounds(TabPage page, string lBound, string uBound, Int32 pageSize)
        {
            switch (page.Name)
            {
                case "CaseLoad":
                    caseLBound = lBound;
                    caseUBound = uBound;
                    break;

                case "DocumentLoad":
                    docLBound = lBound;
                    docUBound = uBound;
                    break;

                case "AnnotationLoad":
                    annLBound = lBound;
                    annUBound = uBound;
                    break;

                case "TaskLoad":
                    taskLBound = lBound;
                    taskUBound = uBound;
                    break;
            }
            AdjustButtons(page, pageSize);
        }

        private string GetTotalCount(string tableName)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT COUNT(*) AS TOTALCOUNT FROM " + tableName);
            return xml.SelectNodes("//row")[0].Attributes["TOTALCOUNT"].Value;
        }

        private string GetMaxID(string tableName)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT MAX(ID) AS MAXID FROM " + tableName);
            return xml.SelectNodes("//row")[0].Attributes["MAXID"].Value;
        }

        private string GetConfigID(string jobID)
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ConfigID FROM MgrJobs WHERE ID = " + jobID);

            return xmlDoc.SelectNodes("//row")[0].Attributes["ConfigID"].Value;
        }

        private string GetMMID(TabPage page, string tableName, string searchID, string jobID)
        {
            string sql = string.Empty;
            
            PreIngest.ConfigManager cMgr = new PreIngest.ConfigManager(dbConnection, GetConfigID(jobID));

            switch (page.Name)
            {
                case "CaseLoad":
                    sql = "SELECT TOP(1) ID FROM " + tableName + " WHERE " + cMgr.GetCaseAccountIdentifier() + " = '" + searchID + "'";
                    break;

                case "DocumentLoad":
                    sql = (caselevelJob) ? "SELECT TOP(1) ID FROM " + tableName + " WHERE " + cMgr.GetDocAccountIdentifier() + " = '" + searchID + "'" :
                                "SELECT TOP(1) ID FROM " + tableName + " WHERE " + cMgr.GetDocumentIdentifier() + " = '" + searchID + "'";
                    break;

                case "AnnotationLoad":
                    sql = (caselevelJob) ? "SELECT TOP(1) ID FROM " + tableName + " WHERE " + cMgr.GetAnnotationAccountIdentifier() + " = '" + searchID + "'" :
                                "SELECT TOP(1) ID FROM " + tableName + " WHERE " + cMgr.GetAnnotatedObjectTypeIdentifier() + " = '" + searchID + "'";
                    break;

                case "TaskLoad":
                    sql = "SELECT TOP(1) ID FROM " + tableName + " WHERE " + cMgr.GetTaskAccountIdentifier() + " = '" + searchID + "'";
                    break;

                default:
                    //TODO: raise exception
                    break;

            }
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
            XmlNode node = xmlDoc.SelectNodes("//row")[0];
            
            return (node != null) ? MigrationManager.GetAttributeValue(node, "ID") : string.Empty;
        }
    }
}
