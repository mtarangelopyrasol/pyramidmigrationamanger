using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Leadtools.Codecs;
using Leadtools;
using System.Reflection;
using System.IO;

namespace MigrationManager.LEADTOOLS
{

	/// <summary>
	/// Provides methods and properties used to unlock LEADTOOLS features
	/// </summary>
	internal static class LeadToolsHelper
	{

		/// <summary>
		/// The license key used to unlock LEADTOOLS 17.5 for the Jobs System
		/// </summary>
		public const string JobsSystem50LEADTOOLSKey = "zzWhN6o9JyCqrCZLdPIZs2uAZdmUaRJhJhkxvDYKaz7OSGfC5zEpYHw9KIXUSjSck3N3UrV9jPdkf4d8PqOsiCLm07Vmqt+9Y5sb01okHrg8UatMZPxRT0Ig/Zg4xaDQI7tWPetsPqU8zzD1bdjHyLptmx5o2QwGdQJLQ0Qe/rlx2YNZ3Iu3oKFQN9hF/ea8v+qNHVt76sUDzAJU9yIHksoar2U+oXlTOw53EuqauW5f5J89bcbEGLf8fZNkS3L2Ws63FN+Lx5tfjlkjoW519HIYfcIKWIG2HmkbDNfsCtX58qqJadZKmu8I1+4o5Hvb7JRmt+A/Tkqrtjg77dmcNXg+B838o24JLPy9+JSjbUZhwg9QJaS8VHUivtyckdaDj8aOWz+anHSJXW+0uKYQJ4pR4nn071kpVtqioY1LS+KyasSLBPR8GftFQljLLkfwNsWFzvGO0YuS1TIkldPxAw==";
		public const string JobsSystem50LEADTOOLSResourceName = "LEADTOOLS.17.5.JobsSystem.lic";

		/// <summary>
		/// Unlocks all registered features in the LEADTOOLS175.lic embedded resource
		/// </summary>
		/// <exception cref="System.IO.FileNotFoundException">Thrown when the resource is not found in the supplied assembly</exception>
		/// <exception cref="ArgumentNullException">Thrown when any of the parameters are null</exception>
		public static void Unlock(Assembly LicensedAssembly, string LicenseKey, string LicenseResourceName)
		{
			if (LicensedAssembly == null) throw new ArgumentNullException("LicensedAssembly");
			if (LicenseKey == null) throw new ArgumentNullException("LicenseKey");
			if (LicenseResourceName == null) throw new ArgumentNullException("LicenseResourceName");

			var lic = (from resourcename in LicensedAssembly.GetManifestResourceNames()
					   where resourcename.EndsWith(LicenseResourceName)
					  select resourcename).FirstOrDefault();

			if (lic == null)
			{
				throw new FileNotFoundException("Could not find the embedded resource: " + LicenseResourceName);
			}

			RasterSupport.SetLicense(LicensedAssembly.GetManifestResourceStream(lic), LicenseKey);
		}
		
		
		
		/// <summary>
		///  This function detemins the minimum acceptable Bpp for the sepcified target format.
		/// </summary>
		/// <param name="TargetFormat">the format to determine the minimum bpp value for</param>
		/// <returns>the minimum bpp for the specified target</returns>
		/****
		public static int DetermineMinimumBpp(RasterImageFormat TargetFormat)
		{
			switch (TargetFormat.ToString().ToUpper())
			{
				//TIF's
				case "TIF":
					return 1;
				case "TIFABIC":
					return 1;
				case "TIFCMYK":
					return 24;
				case "TIFYCC":
					return 24;
				case "TIFPACKBITS":
					return 1;
				case "TIFPACKBITSCMYK":
					return 24;
				case "TIFPACKBITSYCC":
					return 24;
				case "TIFLZW":
					return 1;
				case "TIFLZWCMYK":
					return 24;
				case "TIFLZWYCC":
					return 24;
				case "TIFJPEG":
					return 8;
				case "TIFJPEG422":
					return 8;
				case "TIFJPEG411":
					return 8;
				case "TIFCMP":
					return 8;
				case "TIFJBIG":
					return 1;
				case "TIFJBIG2":
					return 1;
				case "TIFJ2K":
					return 8;
				case "TIFCMW":
					return 8;
				case "GEOTIFF":
					return 1;
				case "EXIF":
					return 24;
				case "EXIFYCC":
					return 24;
				case "EXIFJPEG422":
					return 24;
				case "EXIFJPEG411":
					return 24;
				case "CCITT":
					return 1;
				case "CCITTGROUP31DIM":
					return 1;
				case "CCITTGROUP32DIM":
					return 1;
				case "CCITTGROUP4":
					return 1;
				case "TIFABC":
					return 1;
				case "TIFMRC":
					return 24;
				case "TIFLEADMRC":
					return 24;

				//PDF's
				case "RASPDF":
					return 1;
				case "RASPDFCMYK":
					return 24;
				case "RASPDFG31DIM":
					return 1;
				case "RASPDFG32DIM":
					return 1;
				case "RASPDFG4":
					return 1;
				case "RASPDFLZW":
					return 1;
				case "RASPDFLZWCMYK":
					return 24;
				case "RASPDFJPEG":
					return 8;
				case "RASPDFJPEG422":
					return 24;
				case "RASPDFJPEG411":
					return 24;
				case "RASPDFJBIG2":
					return 1;

				//JPEG's
				case "CMP":
					return 8;
				case "JPEG":
					return 8;
				case "JPEG411":
					return 8;
				case "JPEG422":
					return 8;
				case "JPEGLAB":
					return 24;
				case "JPEGLAB411":
					return 24;
				case "JPEGLAB422":
					return 24;
				case "JPEGRGB":
					return 24;
				case "J2K":
					return 8;
				case "JP2":
					return 8;
				case "CMW":
					return 8;

				//GIF's
				case "GIF":
					return 1;



				case "XPS":
					return 24;

				//If Compression doesn't work, throw an exception.
				default:
					throw new Exception("Desired format of '" + TargetFormat.ToString() + "' is unrecognized.");
			}

			throw new Exception("Unknown Error occured in LeadTools16MultiPurposeConverter.determineBpp");
		}
		***/
	}
}
