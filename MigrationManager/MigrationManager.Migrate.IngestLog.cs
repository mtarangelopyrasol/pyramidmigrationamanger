﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        int ingestSessionIndex = 0;
        Boolean ingestSessionSelected = false;

        int ingestBatchIndex = 0;
        Boolean ingestBatchSelected = false;

        string migLogLBound = "1";
        string migLogUBound = string.Empty;
        string migLogTotal = string.Empty;
        string migLogLowerID = string.Empty;
        string migLogUpperID = string.Empty;

        private void ViewMigrationLogs()
        {
            tabIngestLog.Show();
            tabIngestLog.BringToFront();
            ShowViewIngestLogSubTab();
        }

        private void HideMigrationLogs()
        {
            tabIngestLog.Hide();
        }

        private void tabIngestLog_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowViewIngestLogSubTab();
        }

        private void ShowViewIngestLogSubTab()
        {
            switch (tabIngestLog.SelectedIndex)
            {
                case 0:
                    RefreshIngestSessionList();
                    break;
                case 1:
                    if (!ingestSessionSelected)
                    {
                        MessageBox.Show("Please choose a session first", "Migration Manager", MessageBoxButtons.OK);
                    }
                    else
                    {
                        RefreshIngestBatchesList();
                    }
                    break;
                case 2:
                    if (!ingestBatchSelected)
                    {
                        MessageBox.Show("Please choose a batch first", "Migration Manager", MessageBoxButtons.OK);
                    }
                    else
                    {
                        RefreshIngestBatchLog();
                    }
                    break;
                default:
                    if (!ingestBatchSelected)
                    {
                        MessageBox.Show("Please choose a batch first", "Migration Manager", MessageBoxButtons.OK);
                    }
                    else
                    {
                        RefreshIngestBatchErrors();
                    }
                    break;
            }
        }

        private void RefreshIngestSessionList()
        {
            listIngestSessions.BeginUpdate();

            listIngestSessions.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID, CAST(StartTime AS nvarchar(30)) AS StartStamp, CAST(EndTime AS nvarchar(30)) AS EndStamp, UserName, WorkStation, Status FROM MgrSessions WHERE SessionType = 'INGESTION' ORDER BY ID DESC");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = listIngestSessions.Items.Add(rowNode.GetAttribute("ID"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("StartStamp"));
                        lvi.SubItems.Add(rowNode.GetAttribute("EndStamp"));
                        lvi.SubItems.Add(rowNode.GetAttribute("UserName"));
                        lvi.SubItems.Add(rowNode.GetAttribute("WorkStation"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Status"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                    listIngestSessions.Items[ingestSessionIndex].Checked = true;
                    listIngestSessions.EnsureVisible(ingestSessionIndex);
                    ingestSessionSelected = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Sessions : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                listIngestSessions.EndUpdate();
            }
        }

        private void listIngestSessions_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if((e.Item.Checked) && (e.Item.Index != ingestSessionIndex))
            {
                listIngestSessions.Items[ingestSessionIndex].Checked = false;
                ingestSessionIndex = e.Item.Index;
                migLogLBound = "1";
                migLogUBound = string.Empty;
            }
        }

        private void RefreshIngestBatchesList()
        {
            listIngestBatches.BeginUpdate();
            listIngestBatches.Items.Clear();
            try
            {
                string sessionID = listIngestSessions.Items[ingestSessionIndex].Tag.ToString();

                TabPage p = tabIngestLog.TabPages[tabIngestLog.SelectedIndex];
                Label label = (Label)p.Controls["lblIngestBatches"];
                label.Text = "Ingestion Batches For Session: " + sessionID;

                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID, CAST(StartTime AS nvarchar(30)) AS StartStamp, CAST(EndTime AS nvarchar(30)) AS EndStamp, UserName, WorkStation, Status, Comment FROM MgrMigrations WHERE SessionID = " + sessionID + " ORDER BY ID DESC");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = listIngestBatches.Items.Add(rowNode.GetAttribute("ID"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("StartStamp"));
                        lvi.SubItems.Add(rowNode.GetAttribute("EndStamp"));
                        lvi.SubItems.Add(rowNode.GetAttribute("UserName"));
                        lvi.SubItems.Add(rowNode.GetAttribute("WorkStation"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Status"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Comment"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                    listIngestBatches.Items[ingestBatchIndex].Checked = true;
                    listIngestBatches.EnsureVisible(i - 1);
                    ingestBatchSelected = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Ingestion Batches : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                listIngestBatches.EndUpdate();
            }
        }

        private void listIngestBatches_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != ingestBatchIndex))
            {
                listMigrateBatches.Items[ingestBatchIndex].Checked = false;
                ingestBatchIndex = e.Item.Index;
                migLogLBound = "1";
                migLogUBound = string.Empty;
            }
        }

        private void btnPrevIngestLog_Click(object sender, EventArgs e)
        {
            migLogUBound = migLogLBound;
            migLogLBound = string.Empty;
            RefreshIngestBatchLog();
        }

        private void btnNextIngestLog_Click(object sender, EventArgs e)
        {
            migLogLBound = migLogUBound;
            migLogUBound = string.Empty;
            RefreshIngestBatchLog();
        }

        private void RefreshIngestBatchLog()
        {
            string batchID = listIngestBatches.Items[ingestBatchIndex].Tag.ToString();

            UpdateMigLogBoundaries(GetMigrateJobName() + "_IngestLog", batchID);

            Int32 pageSize = (listIngestBatchLog.Size.Height / ROW_SIZE);

            listIngestBatchLog.BeginUpdate();
            listIngestBatchLog.Items.Clear();
            try
            {
                lblIngestBatchLog.Text = "Ingestion Batch " + batchID.ToString() + " Log:";
                lblIngestLogCount.Text = "Total Records: " + migLogTotal.ToString();

                string sql = (migLogLBound == string.Empty) ?
                    "SELECT * FROM (SELECT TOP " + pageSize.ToString() + " ID, Type, CAST(TimeStamp AS nvarchar(30)) AS TStamp, Description FROM " + GetMigrateJobName() + "_IngestLog WHERE BatchID = " + batchID + " AND ID <= " + migLogUBound + " ORDER BY ID DESC) AS P ORDER BY ID" :
                    "SELECT TOP " + pageSize.ToString() + " ID, Type, CAST(TimeStamp AS nvarchar(30)) AS TStamp, Description FROM " + GetMigrateJobName() + "_IngestLog WHERE BatchID = " + batchID + " AND ID >= " + migLogLBound + " ORDER BY ID";

                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        if (i == 0) migLogLBound = rowNode.GetAttribute("ID");
                        migLogUBound = rowNode.GetAttribute("ID");

                        ListViewItem lvi = listIngestBatchLog.Items.Add(rowNode.GetAttribute("TStamp"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("Type"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Description"));
                        i++;
                    }
                    listIngestBatchLog.EnsureVisible(i - 1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing log : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                listIngestBatchLog.EndUpdate();
                btnPrevIngestLog.Enabled = (migLogLBound == migLogLowerID) ? false : true;
                btnNextIngestLog.Enabled = (migLogUBound == migLogUpperID) ? false : true; 
            }
        }

        protected string GetMigrateJobName()
        {
            string jobID = listMigrateJobs.Items[currentMigrateJobIndex].Tag.ToString();
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT MgrJobs.JobName AS JobName FROM MgrJobs WHERE ID = " + jobID);

            return xmlDoc.SelectNodes("//row")[0].Attributes["JobName"].Value;
        }

        private void UpdateMigLogBoundaries(string tableName, string batchID)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT COUNT(*) AS TOTALCOUNT FROM " + tableName + " WHERE BatchID = " + batchID);
            migLogTotal = xml.SelectNodes("//row")[0].Attributes["TOTALCOUNT"].Value;

            if (Convert.ToInt64(migLogTotal) > 0)
            {
                xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT MIN(ID) AS LOWER FROM " + tableName + " WHERE BatchID = " + batchID);
                migLogLowerID = xml.SelectNodes("//row")[0].Attributes["LOWER"].Value;

                xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT MAX(ID) AS UPPER FROM " + tableName + " WHERE BatchID = " + batchID);
                migLogUpperID = xml.SelectNodes("//row")[0].Attributes["UPPER"].Value;
            }
        }

        private void RefreshIngestBatchErrors()
        {
            listIngestBatchErrors.BeginUpdate();
            listIngestBatchErrors.Items.Clear();
            try
            {
                int batchID = Convert.ToInt32(listIngestBatches.Items[ingestBatchIndex].Tag.ToString());

                lblIngestBatchErrors.Text = "Batch " + batchID.ToString() + " Errors:";

                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID, Type, CAST(TimeStamp AS nvarchar(30)) AS TStamp, Description FROM " + GetMigrateJobName() + "_IngestLog WHERE BatchID = " + batchID + " AND Type = 'ERROR' ORDER BY ID");
                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = listIngestBatchErrors.Items.Add(rowNode.GetAttribute("TStamp"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("Description"));
                        i++;
                    }
                    listIngestBatchErrors.EnsureVisible(i - 1);
                }
                else
                {
                    lblIngestBatchErrors.Text = "Batch " + batchID.ToString() + ": No Errors";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing errors : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                listIngestBatchErrors.EndUpdate();
            }
        }
    }
}
