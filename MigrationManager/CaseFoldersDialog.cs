﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class CaseFoldersDialog : Form
    {
        string dbCnxn = string.Empty;
        string configID = string.Empty;
        string caseID = string.Empty;
        DialogOperation operation;
        string ID = string.Empty;

        public CaseFoldersDialog(string dbConnection, string cfgID, string cID, DialogOperation op)
        {
            dbCnxn = dbConnection;
            configID = cfgID;
            caseID = cID;
            operation = op;

            InitializeComponent();

            if (op == DialogOperation.Add)
            {
                lblHeader.Text = "Add New Folder";
            }
            else
            {
                lblHeader.Text = "Update Folder Definition";
            }
            PopulateParentCombo();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (HasPassedValidation())
            {
                string folder = txtFolderName.Text.Trim();
                string parent = (cmbParent.Items.Count > 0) ? cmbParent.SelectedItem.ToString() : string.Empty;
                string sql = string.Empty;
                if (operation == DialogOperation.Add)
                {
                    sql = "INSERT INTO CfgCaseFolders (ConfigID, CaseTypeID, FolderName, Parent) VALUES (" + configID + ", " + caseID + ", '" + folder + "', '" + parent + "')";
                    int nAffected = DBAdapter.DBAdapter.AddToDB(dbCnxn, sql);
                }
                else
                {
                    sql = "UPDATE CfgCaseFolders SET FolderName = '" + folder + "', Parent = '" + parent + "' WHERE ID = " + ID;
                    int nAffected = DBAdapter.DBAdapter.UpdateDB(dbCnxn, sql);
                }
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        public void Populate(string id)
        {
            ID = id;

            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT * FROM CfgCaseFolders WHERE ID = " + ID);

            txtFolderName.Text = xml.SelectNodes("//row")[0].Attributes["FolderName"].Value;
            string parentFolder = xml.SelectNodes("//row")[0].Attributes["Parent"].Value;
            for (int i = 0; i < cmbParent.Items.Count; i++)
            {
                if (cmbParent.Items[i].ToString() == parentFolder)
                {
                    cmbParent.SelectedItem = cmbParent.Items[i];
                    break;
                }
            }
        }

        private Boolean HasPassedValidation()
        {
            if (txtFolderName.Text.Length == 0)
            {
                MessageBox.Show("Please specify a Folder Name", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            return true;
        }

        private void PopulateParentCombo()
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT FolderName FROM CfgCaseFolders WHERE CaseTypeID = " + caseID + " AND ConfigID = " + configID);
            
            cmbParent.BeginUpdate();
            cmbParent.Items.Clear();
            cmbParent.Items.Add("Root");
            foreach (XmlElement rowNode in xml.SelectNodes("//row"))
            {
                cmbParent.Items.Add(rowNode.GetAttribute("FolderName"));
            }
            cmbParent.EndUpdate();
            cmbParent.SelectedIndex = 0;
        }
    }
}
