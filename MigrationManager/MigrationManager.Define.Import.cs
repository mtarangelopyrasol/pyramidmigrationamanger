﻿using System;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        private void btnConvImport_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = GetImportXml();
            if (xmlDoc != null)
            {
                foreach (XmlElement conv in xmlDoc.SelectNodes("/mmExport"))
                {
                    ImportConvDefns("mmConversion", conv);
                }

                foreach (XmlElement conv in xmlDoc.SelectNodes("/mmExport/mmJobDefinition"))
                {
                    ImportConvDefns("mmConversion", conv);
                }
            }
        }

        private void btnMapDefnImport_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = GetImportXml();
            if (xmlDoc != null)
            {
                foreach (XmlElement maps in xmlDoc.SelectNodes("/mmExport"))
                {
                    ImportMappings("mmMapping", maps);
                }

                foreach (XmlElement maps in xmlDoc.SelectNodes("/mmExport/mmMappings"))
                {
                    ImportMappings("mmMapping", maps);
                }

                foreach (XmlElement maps in xmlDoc.SelectNodes("/mmExport/mmJobDefinition/mmMappings"))
                {
                    ImportMappings("mmMapping", maps);
                }
            }
        }

        private void btnImportJob_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = GetImportXml();
            if (xmlDoc != null)
            {
                foreach (XmlElement job in xmlDoc.SelectNodes("/mmExport"))
                {
                    ImportJobDefns("mmJobDefinition", job);
                }
            }
        }

        private void btnImpBatchDefn_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = GetImportXml();
            if (xmlDoc != null)
            {
                foreach (XmlElement batch in xmlDoc.SelectNodes("/mmExport/mmBatchDefinitions"))
                {
                    ImportBatchDefns("mmBatchDefinition", batch);
                }
            }
        }

        private ImportAction DetermineImportMappingAction(XmlElement rowToImport)
        {
            ImportAction result = ImportAction.Ignore;

            string mapDefnName = GetAttributeValue(rowToImport, "MappingName");
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID FROM MgrMappings WHERE MappingName = '" + mapDefnName + "'");

            if (xml.SelectNodes("//row").Count > 0)
            {
                string existingMapDefnID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                XmlDocument existingValueMaps = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT NewValue FROM MgrMappingValues WHERE MapID = " + existingMapDefnID);
                if (existingValueMaps.SelectNodes("//row").Count != rowToImport.SelectNodes("mmValueMaps/mmValueMap").Count)
                {
                    if (IsMapDefnUsed(mapDefnName))
                    {
                        if (MessageBox.Show("There already is a map definition called " + mapDefnName + "that is already in use. Merge it with the one being imported?", "Import Confirmation", MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
                        {
                            result = ImportAction.Merge;
                        }
                    }
                    else result = ImportAction.Merge;
                }
                else
                {
                    foreach (XmlElement row in rowToImport.SelectNodes("mmValueMaps/mmValueMap"))
                    {
                        string oldValue = row.Attributes["OldValue"].Value;
                        string newValue = row.Attributes["NewValue"].Value;

                        XmlDocument valueXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT NewValue FROM MgrMappingValues WHERE OldValue = '" + oldValue + "' AND MapID = " + existingMapDefnID);
                        if (valueXml.SelectNodes("//row").Count > 0)
                        {
                            string existingNewValue= valueXml.SelectNodes("//row")[0].Attributes["NewValue"].Value;
                            if(newValue != existingNewValue)
                            {
                                string message = (IsMapDefnUsed(mapDefnName)) ? 
                                    "There already is a map definition called " + mapDefnName + "that has been used in configuration definitions. Merge it with the one being imported?" : 
                                    "There already is a map definition called " + mapDefnName + ". Merge it with the one being imported?";
                                
                                if (MessageBox.Show(message, "Import Confirmation", MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
                                {
                                    result = ImportAction.Update;
                                }
                                break;
                            }
                        }
                        else
                        {
                            string message = (IsMapDefnUsed(mapDefnName)) ?
                                    "There already is a map definition called " + mapDefnName + "that has been used in configuration definitions. Merge it with the one being imported?" :
                                    "There already is a map definition called " + mapDefnName + ". Merge it with the one being imported?";

                            if (MessageBox.Show(message, "Import Confirmation", MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
                            {
                                result = ImportAction.Merge;
                            }
                            break;
                        }
                    }
                    if (result == ImportAction.Ignore)
                    {
                        MessageBox.Show("There is already a mapping called " + mapDefnName + " that is identical to the one being imported.", "Import Identical", MessageBoxButtons.OK);
                    }
                }
            }
            else
            {
                result = ImportAction.Insert; // does not already exist
            }
            return result;
        }

        private Boolean IsMapDefnUsed(string mapDefnName)
        {
            Boolean result = true;

            string sql = "SELECT * FROM CfgCaseProperties WHERE MappingName = '" + mapDefnName + "'";
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
            if (xml.SelectNodes("//row").Count == 0)
            {
                sql = "SELECT * FROM CfgDocClassProperties WHERE MappingName = '" + mapDefnName + "'";
                xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
                if (xml.SelectNodes("//row").Count == 0)
                {
                    sql = "SELECT * FROM CfgTaskProperties WHERE MappingName = '" + mapDefnName + "'";
                    xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
                    result = (xml.SelectNodes("//row").Count == 0) ? false : true;
                }
            }
            return result;
        }

        private void MergeValueMaps(XmlElement mapDefnXml)
        {
            string sql = "SELECT ID FROM MgrMappings WHERE MappingName = '" + GetAttributeValue(mapDefnXml, "MappingName") + "'";
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            string mapDefnID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;

            foreach(XmlNode vMap in mapDefnXml.SelectNodes("mmValueMaps/mmValueMap"))
            {
                string oldValue = GetAttributeValue(vMap, "OldValue");
                string newValue = GetAttributeValue(vMap, "NewValue");

                sql = "SELECT NewValue FROM MgrMappingValues WHERE MapID = " + mapDefnID + " AND OldValue = '" + oldValue + "'";
                XmlDocument newValueXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

                if(newValueXml.SelectNodes("//row").Count > 0)
                {
                    sql = "UPDATE MgrMappingValues SET NewValue = '" + newValue + "' WHERE MapID = " + mapDefnID + " AND OldValue = '" + oldValue + "'";
                    int nAffected = DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
                }
                else
                {
                    sql = "INSERT INTO MgrMappingValues(MapID, OldValue, NewValue) VALUES (" + mapDefnID + ", '" + oldValue + "', '" + newValue + "')";
                    int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                }
            }
        }

        private void ImportMappings(string xpath, XmlElement maps)
        {
            foreach (XmlElement mapNode in maps.SelectNodes(xpath))
            {
                switch(DetermineImportMappingAction(mapNode))
                {
                    case ImportAction.Insert:
                        string sql = "INSERT INTO MgrMappings(MappingName) VALUES ('" + GetAttributeValue(mapNode, "MappingName") + "')";
                        int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                        MergeValueMaps(mapNode);
                        RefreshMapDefinitions();
                        break;

                    case ImportAction.Merge:
                    case ImportAction.Update:
                        MergeValueMaps(mapNode);
                        break;

                    default:
                        break;
                }
            }
        }

        private ImportAction DetermineImportConvDefnAction(XmlElement rowToImport)
        {
            ImportAction result = ImportAction.Ignore;

            string convDefnName = GetAttributeValue(rowToImport, "ConvDefnName");
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM MgrConvDefns WHERE ConvDefnName = '" + convDefnName + "'");

            if (xml.SelectNodes("//row").Count > 0)
            {
                XmlNode existingRow = xml.SelectNodes("//row")[0];
                
                if (GetAttributeValue(rowToImport, "MarshalToSingleDoc") != GetAttributeValue(existingRow, "MarshalToSingleDoc"))
                {
                    result = ImportAction.Update;
                }
                else
                {
                    string existingConvDefnID = GetAttributeValue(existingRow, "ID");
                    XmlDocument countXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ConvMapID FROM MgrConversions WHERE ConvDefnID = " + existingConvDefnID);
                    if (countXml.SelectNodes("//row").Count != rowToImport.SelectNodes("mmConvMaps/mmConvMap").Count)
                    {
                        if (IsConvDefnUsed(existingConvDefnID))
                        {
                            if (MessageBox.Show("There already is a Conversion Map definition called " + convDefnName + ". Merge it with the one being imported?", "Import Confirmation", MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
                            {
                                result = ImportAction.Merge;
                            }
                        }
                        else result = ImportAction.Merge;
                    }
                    else
                    {
                        string sql = string.Empty;
                        foreach (XmlElement row in rowToImport.SelectNodes("mmConvMaps/mmConvMap"))
                        {
                            string sourceFormat = GetAttributeValue(row, "SourceFormat");
                            string targetFormat = GetAttributeValue(row, "TargetFormat");
                            string exempt = GetAttributeValue(row, "Exempt");
                            string restricted = GetAttributeValue(row, "Restricted");

                            sql = "SELECT c.ID AS Marker, cm.* FROM MgrConvMaps cm, MgrConversions c WHERE c.ConvDefnID = " + existingConvDefnID +
                            " AND c.ConvMapID = cm.ID AND cm.SourceFormat = '" + sourceFormat + "'";
                            XmlDocument existingXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

                            if (existingXml.SelectNodes("//row").Count > 0)
                            {
                                string oldTargetFormat = existingXml.SelectNodes("//row")[0].Attributes["TargetFormat"].Value;
                                string oldExempt = existingXml.SelectNodes("//row")[0].Attributes["Exempt"].Value;
                                string oldRestricted = existingXml.SelectNodes("//row")[0].Attributes["Restricted"].Value;

                                if ((oldTargetFormat != targetFormat) || (oldExempt != exempt) || (oldRestricted != restricted))
                                {
                                    if (MessageBox.Show("There already is a Conversion Map definition called " + convDefnName + ". Merge it with the one being imported?", "Import Confirmation", MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
                                    {
                                        result = ImportAction.Merge; // exists, but does not match
                                    }
                                    break;
                                }
                            }
                            else
                            {
                                if (MessageBox.Show("There already is a Conversion Map definition called " + convDefnName + ". Merge it with the one being imported?", "Import Confirmation", MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
                                {
                                    result = ImportAction.Merge; // does not exist
                                }
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                result = ImportAction.Insert;
            }
            return result;
        }

        private Boolean IsConvDefnUsed(string convDefnName)
        {
            string sql = "SELECT j.ID FROM MgrJobs j, MgrConvDefns cd WHERE j.ConvDefnID = cd.ID AND cd.ConvDefnName = '" + convDefnName + "'";
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
            XmlNodeList xnl = xmlDoc.SelectNodes("//row");
            return (xnl.Count > 0) ? true : false;
        }

        private string GetIDForConvMap(string source, string target, string exempt, string restricted)
        {
            string sql = "SELECT ID FROM MgrConvMaps WHERE SourceFormat = '" + source + "' AND TargetFormat = '" + target + "' AND Exempt = '" + exempt + "' AND Restricted = '" + restricted + "'";
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
            XmlNodeList xnl = xmlDoc.SelectNodes("//row");
            return (xnl.Count > 0) ? xnl[0].Attributes["ID"].Value : string.Empty;
        }

        private string InsertConvMap(string sourceFormat, string targetFormat, string exempt, string restricted)
        {
            string convMapID = GetIDForConvMap(sourceFormat, targetFormat, exempt, restricted);
            if (convMapID.Length == 0)
            {
                string sql = "INSERT INTO MgrConvMaps(SourceFormat, TargetFormat, Exempt, Restricted) VALUES ('" + sourceFormat + "', '" + targetFormat + "', " + exempt + ", " + restricted + ")";
                int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                convMapID = GetIDForConvMap(sourceFormat, targetFormat, exempt, restricted);
            }
            return convMapID;
        }

        private void MergeConversionMaps(string convDefnID, XmlElement convDefnNode)
        {
            foreach (XmlNode convMap in convDefnNode.SelectNodes("mmConvMaps/mmConvMap"))
            {
                string sourceFormat = GetAttributeValue(convMap, "SourceFormat");
                string targetFormat = GetAttributeValue(convMap, "TargetFormat");
                string exempt = GetAttributeValue(convMap, "Exempt");
                string restricted = GetAttributeValue(convMap, "Restricted");

                string sql = "SELECT c.ID AS Marker, cm.* FROM MgrConvMaps cm, MgrConversions c WHERE c.ConvDefnID = " + convDefnID + 
                        " AND c.ConvMapID = cm.ID AND cm.SourceFormat = '" + sourceFormat + "'";
                XmlDocument existingXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

                if (existingXml.SelectNodes("//row").Count > 0)
                {
                    string oldTargetFormat = existingXml.SelectNodes("//row")[0].Attributes["TargetFormat"].Value;
                    string oldExempt = existingXml.SelectNodes("//row")[0].Attributes["Exempt"].Value;
                    string oldRestricted = existingXml.SelectNodes("//row")[0].Attributes["Restricted"].Value;

                    if((oldTargetFormat != targetFormat) || (oldExempt != exempt) || (oldRestricted != restricted))
                    {
                        string newConvMapID = InsertConvMap(sourceFormat, targetFormat, exempt, restricted);
                        string existingConvID = existingXml.SelectNodes("//row")[0].Attributes["Marker"].Value;
                        sql = "UPDATE MgrConversions SET ConvMapID = " + newConvMapID + " WHERE ID = " + existingConvID;
                        int nAffected = DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
                    }
                }
                else
                {
                    // conversion mapping does not exist
                    string newConvMapID = InsertConvMap(sourceFormat, targetFormat, exempt, restricted);
                    sql = "INSERT INTO MgrConversions(ConvDefnID, ConvMapID) VALUES (" + convDefnID + ", " + newConvMapID + ")";
                    int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                }
            }
        }

        private string GetIDForConvDefn(string convDefnName)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID FROM MgrConvDefns WHERE ConvDefnName = '" + convDefnName + "'");
            XmlNodeList xnl = xml.SelectNodes("//row");
            return (xnl.Count > 0) ? xnl[0].Attributes["ID"].Value : string.Empty;
        }

        private string ImportConvDefns(string xpath, XmlElement conv)
        {
            string convDefnID = string.Empty;
          
            foreach (XmlElement convDefnNode in conv.SelectNodes(xpath))
            {
                switch (DetermineImportConvDefnAction(convDefnNode))
                {
                    case ImportAction.Insert:
                        {
                            string sql = "INSERT INTO MgrConvDefns(ConvDefnName, MarshalToSingleDoc) VALUES ('" + GetAttributeValue(convDefnNode, "ConvDefnName") + "', " + GetAttributeValue(convDefnNode, "MarshalToSingleDoc") + ")";
                            int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                            convDefnID = GetIDForConvDefn(GetAttributeValue(convDefnNode, "ConvDefnName"));
                            MergeConversionMaps(convDefnID, convDefnNode);
                            RefreshConversionDefinitions();
                        }
                        break;

                    case ImportAction.Merge:
                        convDefnID = GetIDForConvDefn(GetAttributeValue(convDefnNode, "ConvDefnName"));
                        MergeConversionMaps(convDefnID, convDefnNode);
                        break;

                    case ImportAction.Update:
                        {
                            convDefnID = GetIDForConvDefn(GetAttributeValue(convDefnNode, "ConvDefnName"));
                            string sql = "UPDATE MgrConvDefns SET MarshalToSingleDoc = " + GetAttributeValue(convDefnNode, "MarshalToSingleDoc") + " WHERE ID = " + convDefnID;
                            int nAffected = DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
                            MergeConversionMaps(convDefnID, convDefnNode);
                            RefreshConversionDefinitions();
                        }
                        break;

                    default:
                        convDefnID = GetIDForConvDefn(GetAttributeValue(convDefnNode, "ConvDefnName"));
                        break;
                }
            }
            return convDefnID;
        }

        private ImportAction DetermineImportJobDefnAction(XmlElement rowToImport)
        {
            ImportAction result = ImportAction.Ignore;

            string jobName = GetAttributeValue(rowToImport, "JobName");
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT j.Comment, c.ConfigName, cd.ConvDefnName FROM MgrJobs j, MgrConfigurations c, MgrConvDefns cd WHERE j.JobName = '" + jobName + "' AND j.ConfigID = c.ID AND j.ConvDefnID = cd.ID");

            if (xml.SelectNodes("//row").Count > 0)
            {
                XmlNode existingJob = xml.SelectNodes("//row")[0];
                if ((GetAttributeValue(rowToImport, "Comment") != GetAttributeValue(existingJob, "Comment")) ||
                    (GetAttributeValue(rowToImport, "ConfigName") != GetAttributeValue(existingJob, "ConfigName")) ||
                    (GetAttributeValue(rowToImport, "ConvDefnName") != GetAttributeValue(existingJob, "ConvDefnName")))
                {
                    DialogResult response = MessageBox.Show("There already is a job definition called " + jobName + ". Replace it with the one being imported?", "Import Confirmation", MessageBoxButtons.YesNo);
                    switch (response)
                    {
                        case DialogResult.Yes:
                            result = ImportAction.Update;
                            break;

                        default:
                            result = ImportAction.Ignore;
                            break;
                    }
                }
                else
                {
                    DialogResult response = MessageBox.Show("There already is a job definition called " + jobName + ". Merge it with the one being imported?", "Import Confirmation", MessageBoxButtons.YesNo);
                    switch (response)
                    {
                        case DialogResult.Yes:
                            result = ImportAction.Merge;
                            break;

                        default:
                            result = ImportAction.Ignore;
                            break;
                    }
                }
            }
            else
            {
                result = ImportAction.Insert;
            }
            return result;
        }

        private void ImportJobDefns(string xpath, XmlElement job)
        {
            foreach (XmlElement exportNode in job.SelectNodes(xpath))
            {
                switch (DetermineImportJobDefnAction(exportNode))
                {
                    case ImportAction.Insert:
                        {
                            string configID = ImportConfig("0", "mmConfiguration", exportNode);
                            string convDefnID = ImportConvDefns("mmConversion", exportNode);
                            string sql = "INSERT INTO MgrJobs(ConfigID, JobName, ConvDefnID, Comment) VALUES (" + configID + ", '" +
                                    GetAttributeValue(exportNode, "JobName") + "', " +
                                    convDefnID + ", '" +
                                    GetAttributeValue(exportNode, "Comment") + "')";
                            int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                            RefreshJobDefnList();
                            ImportMappings("mmMappings/mmMapping", exportNode);
                        }
                        break;

                    case ImportAction.Merge:
                        {
                            string configID = ImportConfig("0", "mmConfiguration", exportNode);
                            string convDefnID = ImportConvDefns("mmConversion", exportNode);
                            RefreshJobDefnList();
                            ImportMappings("mmMappings/mmMapping", exportNode);
                        }
                        break;
                    
                    case ImportAction.Update:
                        {
                            string configID = ImportConfig("0", "mmConfiguration", exportNode);
                            string convDefnID = ImportConvDefns("mmConversion", exportNode);
                            string sql = "UPDATE MgrJobs SET ConfigID = " + configID +
                                    ", ConvDefnID = " + convDefnID +
                                    ", Comment = '" + GetAttributeValue(exportNode, "Comment") +
                                    "' WHERE JobName = '" + GetAttributeValue(exportNode, "JobName") + "'";
                            int nAffected = DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
                            RefreshJobDefnList();
                            ImportMappings("mmMappings/mmMapping", exportNode);
                        }
                        break;

                    default:
                        break; // ignore case
                }
            }
        }

        private ImportAction DetermineImportBatchDefnAction(XmlElement rowToImport)
        {
            ImportAction result = ImportAction.Ignore;

            string batchDefnName = GetAttributeValue(rowToImport, "BatchDefnName");
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM MgrBatchDefns WHERE BatchDefnName = '" + batchDefnName + "'");

            if (xml.SelectNodes("//row").Count > 0)
            {
                XmlNode existingRow = xml.SelectNodes("//row")[0];
                string batchDefnID = GetAttributeValue(existingRow, "ID");
                if ((GetAttributeValue(rowToImport, "BatchSize") != GetAttributeValue(existingRow, "BatchSize")) ||
                    (GetAttributeValue(rowToImport, "BatchCondition") != GetAttributeValue(existingRow, "BatchCondition")) ||
                    (GetAttributeValue(rowToImport, "SourceContentPathPrefix") != GetAttributeValue(existingRow, "SourceContentPathPrefix")) ||
                    (GetAttributeValue(rowToImport, "StagingPath") != GetAttributeValue(existingRow, "StagingPath")))
                {
                    string message = DoBatchesExist(batchDefnID) ? 
                            "There already is a batch definition called " + batchDefnName + " with associated batches. Replace it with the one being imported?" : 
                            "There already is a batch definition called " + batchDefnName + ". Replace it with the one being imported?";
                    
                    if (MessageBox.Show(message, "Import Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        result = ImportAction.Update;
                    }
                }
                else
                {
                    MessageBox.Show("There is already a batch definition called " + batchDefnName + " that is identical to the one being imported.", "Import Identical", MessageBoxButtons.OK);
                }
            }
            else
            {
                result = ImportAction.Insert;   // does not already exist
            }
            return result;
        }

        private Boolean DoBatchesExist(string batchDefnID)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM MgrBatches WHERE BatchDefnID = " + batchDefnID + " AND ActualSize > 0");
            return (xml.SelectNodes("//row").Count > 0) ? true : false;
        }

        private void ImportBatchDefns(string xpath, XmlElement batch)
        {
            string sql = string.Empty;
            int nAffected = 0;

            foreach (XmlElement row in batch.SelectNodes(xpath))
            {
                switch (DetermineImportBatchDefnAction(row))
                {
                    case ImportAction.Insert:
                        sql = "INSERT INTO MgrBatchDefns(BatchDefnName, BatchSize, BatchCondition, SourceContentPathPrefix, StagingPath) VALUES ('" +
                                GetAttributeValue(row, "BatchDefnName") + "', " +
                                GetAttributeValue(row, "BatchSize") + ", '" +
                                GetAttributeValue(row, "BatchCondition") + "', '" +
                                GetAttributeValue(row, "SourceContentPathPrefix") + "', '" +
                                GetAttributeValue(row, "StagingPath") + "')";
                        nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                        RefreshBatchDefnList();
                        break;

                    case ImportAction.Update:
                        sql = "UPDATE MgrBatchDefns SET BatchSize = " + GetAttributeValue(row, "BatchSize") +
                                ", BatchCondition = '" + GetAttributeValue(row, "BatchCondition") +
                                "', SourceContentPathPrefix = '" + GetAttributeValue(row, "SourceContentPathPrefix") +
                                "', StagingPath = '" + GetAttributeValue(row, "StagingPath") +
                                "' WHERE BatchDefnName = '" + GetAttributeValue(row, "BatchDefnName") + "'";
                        nAffected = DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
                        RefreshBatchDefnList();
                        break;

                    default:
                        break;
                }
            }
        }
    }
}
