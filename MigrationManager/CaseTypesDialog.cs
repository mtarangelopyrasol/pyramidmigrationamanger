﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class CaseTypesDialog : Form
    {
        string dbCnxn = string.Empty;
        string configID = string.Empty;
        DialogOperation operation;
        string ID = string.Empty;
        Boolean isDirty = false;
        
        public CaseTypesDialog(string dbConnection, string cfgID, DialogOperation op)
        {
            dbCnxn = dbConnection;
            configID = cfgID;
            operation = op;

            InitializeComponent();

            if (op == DialogOperation.Add)
            {
                lblHeader.Text = "Add New Case Type"; 
            }
            else
            {
                lblHeader.Text = "Update Case Type Definition";
            }
            PopulateDocClassCombo();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (SyncCaseType())
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private Boolean IsUniqueCaseTypeName()
        {
            string caseType = txtCaseType.Text.Trim();
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT * FROM CfgCaseTypes WHERE CaseName = '" + caseType + "' AND ConfigID = " + configID);
            if (xml.SelectNodes("//row").Count > 0)
            {
                if (operation == DialogOperation.Add)
                {
                    MessageBox.Show("CaseType " + caseType + " already exists. Please choose another name.", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
                else
                {
                    string temp = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                    if (temp != ID)
                    {
                        MessageBox.Show("CaseType " + caseType + " already exists. Please choose another name.", "Migration Manager", MessageBoxButtons.OK);
                        return false;
                    }
                }
            }
            return true;
        }

        private void btnProperties_Click(object sender, EventArgs e)
        {
            if (SyncCaseType())
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT ID FROM CfgCaseTypes WHERE ConfigID = " + configID + " AND CaseName = '" + txtCaseType.Text.Trim() + "'");
                ID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;

                using (ListPropertiesDialog dlg = new ListPropertiesDialog(dbCnxn, configID, ID, PropertyType.Case))
                {
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        // nothing for now
                    }
                }
            }
        }

        private void btnFolders_Click(object sender, EventArgs e)
        {
            if (SyncCaseType())
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT ID FROM CfgCaseTypes WHERE ConfigID = " + configID + " AND CaseName = '" + txtCaseType.Text.Trim() + "'");
                ID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;

                using (ListFoldersDialog dlg = new ListFoldersDialog(dbCnxn, configID, ID))
                {
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        // nothing for now
                    }
                }
            }
        }

        private void PopulateDocClassCombo()
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT DocClassName FROM CfgDocClasses WHERE ConfigID = " + configID);
            int count = xml.SelectNodes("//row").Count;
            if (count > 0)
            {
                cmbDocClass.BeginUpdate();
                cmbDocClass.Items.Clear();
                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    cmbDocClass.Items.Add(rowNode.GetAttribute("DocClassName"));
                }
                cmbDocClass.EndUpdate();
                cmbDocClass.SelectedIndex = 0;
            }
        }

        public void Populate(string id)
        {
            ID = id;

            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT * FROM CfgCaseTypes WHERE ID = " + ID);

            txtCaseType.Text = xml.SelectNodes("//row")[0].Attributes["CaseName"].Value;
            string docClass = xml.SelectNodes("//row")[0].Attributes["DefaultDocClass"].Value;
            for (int i = 0; i < cmbDocClass.Items.Count; i++)
            {
                if (cmbDocClass.Items[i].ToString() == docClass)
                {
                    cmbDocClass.SelectedItem = cmbDocClass.Items[i];
                    break;
                }
            }
        }

        private Boolean HasPassedValidation()
        {
            if (txtCaseType.Text.Length == 0)
            {
                MessageBox.Show("Please specify a Case Type Name", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            return IsUniqueCaseTypeName();
        }

        private Boolean SyncCaseType()
        {
            if (HasPassedValidation())
            {
                if (isDirty)
                {
                    string caseType = txtCaseType.Text.Trim();
                    string docClass = (cmbDocClass.Items.Count > 0) ? cmbDocClass.SelectedItem.ToString() : string.Empty;
                    string sql = string.Empty;
                    if (operation == DialogOperation.Add)
                    {
                        sql = "INSERT INTO CfgCaseTypes (ConfigID, CaseName, DefaultDocClass) VALUES (" + configID + ", '" + caseType + "', '" + docClass + "')";
                        int nAffected = DBAdapter.DBAdapter.AddToDB(dbCnxn, sql);
                        XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT ID FROM CfgCaseTypes WHERE CaseName = '" + caseType + "'");
                        ID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                        operation = DialogOperation.Update;
                    }
                    else
                    {
                        sql = "UPDATE CfgCaseTypes SET CaseName = '" + caseType + "', DefaultDocClass = '" + docClass + "' WHERE ID = " + ID;
                        int nAffected = DBAdapter.DBAdapter.UpdateDB(dbCnxn, sql);
                    }
                    isDirty = false;
                }
                return true;
            }
            return false;
        }

        private void txtCaseType_TextChanged(object sender, EventArgs e)
        {
            isDirty = true;
        }

        private void cmbDocClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            isDirty = true;
        }
    }
}
