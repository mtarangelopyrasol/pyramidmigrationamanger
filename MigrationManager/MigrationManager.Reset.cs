﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        private void tabReset_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowResetSubTab();
        }

        private void ShowResetSubTab()
        {
            switch (tabReset.SelectedIndex)
            {
                case 0:
                    ViewConversionResetOptions();
                    break;
                default:
                    ViewIngestionResetOptions();
                    break;
            }
        }
    }
}
