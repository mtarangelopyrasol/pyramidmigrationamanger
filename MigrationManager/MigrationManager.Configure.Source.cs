﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        int lvIndexSourceCase = 0;
        int lvIndexSourceDocument = 0;
        int lvIndexSourceAnnotation = 0;
        int lvIndexSourceTask = 0;

        private void tabConfigSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowConfigSourceSubTab();
        }

        private void ShowConfigSourceSubTab()
        {
            switch (tabConfigSource.TabPages[tabConfigSource.SelectedIndex].Name)
            {
                case "sourceCases":
                    RefreshSourceCasePage();
                    break;

                case "sourceDocuments":
                    RefreshSourceDocumentPage();
                    break;

                case "sourceAnnotations":
                    RefreshSourceAnnotationPage();
                    break;

                case "sourceTasks":
                    RefreshSourceTaskPage();
                    break;

                default:
                    throw new Exception("Unknown option");
            }
        }

        private void RefreshSourceData(ListView lv, string sql, string errorID)
        {
            const int nColumns = 6;
            string[] fields = new string[nColumns];
            fields[0] = "ColumnID";
            fields[1] = "Name";
            fields[2] = "DataType";
            fields[3] = "MaxLength";
            fields[4] = "Format";
            fields[5] = "MultiValued";

            MigrationManager.RefreshListView(lv, dbConnection, sql, fields, errorID);
        }

        private void RefreshSourceCasePage()
        {
            if (configSelected)
            {
                lblSourceCaseConfig.Text = "Current Configuration: " + currentConfigName;
                string errorID = "Source Case Columns";
                string sql = "SELECT * FROM CfgCaseSource WHERE ConfigID = " + currentConfigID + " ORDER BY ColumnID";
                RefreshSourceData(lvSourceCase, sql, errorID);
                if (lvSourceCase.Items.Count > 0)
                {
                    lvSourceCase.Items[lvIndexSourceCase].Checked = true;
                    lvSourceCase.EnsureVisible(lvIndexSourceCase);
                }
            }
            else
                ClearDisplay(lblSourceCaseConfig, lvSourceCase);
        }

        private void RefreshSourceDocumentPage()
        {
            if (configSelected)
            {
                lblSourceDocConfig.Text = "Current Configuration: " + currentConfigName;
                string errorID = "Source Document Columns";
                string sql = "SELECT * FROM CfgDocumentSource WHERE ConfigID = " + currentConfigID + " ORDER BY ColumnID";
                RefreshSourceData(lvSourceDocument, sql, errorID);
                if (lvSourceDocument.Items.Count > 0)
                {
                    lvSourceDocument.Items[lvIndexSourceDocument].Checked = true;
                    lvSourceDocument.EnsureVisible(lvIndexSourceDocument);
                }
            }
            else
                ClearDisplay(lblSourceDocConfig, lvSourceDocument);
        }

        private void RefreshSourceAnnotationPage()
        {
            if (configSelected)
            {
                lblSourceAnnConfig.Text = "Current Configuration: " + currentConfigName;
                string errorID = "Source Annotation Columns";
                string sql = "SELECT * FROM CfgAnnotationSource WHERE ConfigID = " + currentConfigID + " ORDER BY ColumnID";
                RefreshSourceData(lvSourceAnnotation, sql, errorID);
                if (lvSourceAnnotation.Items.Count > 0)
                {
                    lvSourceAnnotation.Items[lvIndexSourceAnnotation].Checked = true;
                    lvSourceAnnotation.EnsureVisible(lvIndexSourceAnnotation);
                }
            }
            else
                ClearDisplay(lblSourceAnnConfig, lvSourceAnnotation);
        }

        private void RefreshSourceTaskPage()
        {
            if (configSelected)
            {
                lblSourceTaskConfig.Text = "Current Configuration: " + currentConfigName;
                string errorID = "Source Task Columns";
                string sql = "SELECT * FROM CfgTaskSource WHERE ConfigID = " + currentConfigID + " ORDER BY ColumnID";
                RefreshSourceData(lvSourceTask, sql, errorID);
                if (lvSourceTask.Items.Count > 0)
                {
                    lvSourceTask.Items[lvIndexSourceTask].Checked = true;
                    lvSourceTask.EnsureVisible(lvIndexSourceTask);
                }
            }
            else
                ClearDisplay(lblSourceTaskConfig, lvSourceTask);
        }

        private void btnSrcCfgCaseAdd_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && (IsOKThatJobsExist(dbConnection, currentConfigID)))
            {
                using (SourceConfigDialog dlg = new SourceConfigDialog(dbConnection, currentConfigID, "CfgCaseSource", DialogOperation.Add))
                {
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        RefreshSourceCasePage();
                    }
                }
            }
        }

        private void btnSrcCfgCaseUpd_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && ((lvSourceCase.Items.Count > 0) && (IsOKThatJobsExist(dbConnection, currentConfigID))))
            {
                using (SourceConfigDialog dlg = new SourceConfigDialog(dbConnection, currentConfigID, "CfgCaseSource", DialogOperation.Update))
                {
                    dlg.Populate(lvSourceCase.Items[lvIndexSourceCase].Tag.ToString());
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        RefreshSourceCasePage();
                    }
                }
            }
        }

        private void btnSrcCfgCaseDel_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && ((lvSourceCase.Items.Count > 0) && !(IsASource(dbConnection, currentConfigID, "CfgCaseProperties", lvSourceCase.Items[lvIndexSourceCase].SubItems[1].Text, "Case"))))
            {
                DeleteSourceColumn("CfgCaseSource", lvSourceCase.Items[lvIndexSourceCase].Tag.ToString(), Convert.ToInt32(lvSourceCase.Items[lvIndexSourceCase].SubItems[0].Text));
                lvIndexSourceCase--;
                if (lvIndexSourceCase < 0) lvIndexSourceCase = 0;
                RefreshSourceCasePage();
            }
        }

        private void btnSrcCfgDocAdd_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && (IsOKThatJobsExist(dbConnection, currentConfigID)))
            {
                using (SourceConfigDialog dlg = new SourceConfigDialog(dbConnection, currentConfigID, "CfgDocumentSource", DialogOperation.Add))
                {
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        RefreshSourceDocumentPage();
                    }
                }
            }
        }

        private void btnSrcCfgDocUpd_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && ((lvSourceDocument.Items.Count > 0) && (IsOKThatJobsExist(dbConnection, currentConfigID))))
            {
                using (SourceConfigDialog dlg = new SourceConfigDialog(dbConnection, currentConfigID, "CfgDocumentSource", DialogOperation.Update))
                {
                    dlg.Populate(lvSourceDocument.Items[lvIndexSourceDocument].Tag.ToString());
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        RefreshSourceDocumentPage();
                    }
                }
            }
        }

        private void btnSrcCfgDocDel_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && ((lvSourceDocument.Items.Count > 0) && (!IsASource(dbConnection, currentConfigID, "CfgDocClassProperties", lvSourceDocument.Items[lvIndexSourceDocument].SubItems[1].Text, "Doc"))))
            {
                DeleteSourceColumn("CfgDocumentSource", lvSourceDocument.Items[lvIndexSourceDocument].Tag.ToString(), Convert.ToInt32(lvSourceDocument.Items[lvIndexSourceDocument].SubItems[0].Text));
                lvIndexSourceDocument--;
                if (lvIndexSourceDocument < 0) lvIndexSourceDocument = 0;
                RefreshSourceDocumentPage();
            }
        }

        private void btnSrcCfgAnnAdd_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && (IsOKThatJobsExist(dbConnection, currentConfigID)))
            {
                using (SourceConfigDialog dlg = new SourceConfigDialog(dbConnection, currentConfigID, "CfgAnnotationSource", DialogOperation.Add))
                {
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        RefreshSourceAnnotationPage();
                    }
                }
            }
        }

        private void btnSrcCfgAnnUpd_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && ((lvSourceAnnotation.Items.Count > 0) && (IsOKThatJobsExist(dbConnection, currentConfigID))))
            {
                using (SourceConfigDialog dlg = new SourceConfigDialog(dbConnection, currentConfigID, "CfgAnnotationSource", DialogOperation.Update))
                {
                    dlg.Populate(lvSourceAnnotation.Items[lvIndexSourceAnnotation].Tag.ToString());
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        RefreshSourceAnnotationPage();
                    }
                }
            }
        }

        private void btnSrcCfgAnnDel_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && ((lvSourceAnnotation.Items.Count > 0) && !(IsAnIdentifier(dbConnection, currentConfigID, lvSourceAnnotation.Items[lvIndexSourceAnnotation].SubItems[1].Text, "Ann"))))
            {
                DeleteSourceColumn("CfgAnnotationSource", lvSourceAnnotation.Items[lvIndexSourceAnnotation].Tag.ToString(), Convert.ToInt32(lvSourceAnnotation.Items[lvIndexSourceAnnotation].SubItems[0].Text));
                lvIndexSourceAnnotation--;
                if (lvIndexSourceAnnotation < 0) lvIndexSourceAnnotation = 0;
                RefreshSourceAnnotationPage();
            }
        }

        private void btnSrcCfgTaskAdd_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && (IsOKThatJobsExist(dbConnection, currentConfigID)))
            {
                using (SourceConfigDialog dlg = new SourceConfigDialog(dbConnection, currentConfigID, "CfgTaskSource", DialogOperation.Add))
                {
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        RefreshSourceTaskPage();
                    }
                }
            }
        }

        private void btnSrcCfgTaskUpd_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && ((lvSourceTask.Items.Count > 0) && (IsOKThatJobsExist(dbConnection, currentConfigID))))
            {
                using (SourceConfigDialog dlg = new SourceConfigDialog(dbConnection, currentConfigID, "CfgTaskSource", DialogOperation.Update))
                {
                    dlg.Populate(lvSourceTask.Items[lvIndexSourceTask].Tag.ToString());
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        RefreshSourceTaskPage();
                    }
                }
            }
        }

        private void btnSrcCfgTaskDel_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && ((lvSourceTask.Items.Count > 0) && (!IsASource(dbConnection, currentConfigID, "CfgTaskProperties", lvSourceTask.Items[lvIndexSourceTask].SubItems[1].Text, "Task"))))
            {
                DeleteSourceColumn("CfgTaskSource", lvSourceTask.Items[lvIndexSourceTask].Tag.ToString(), Convert.ToInt32(lvSourceTask.Items[lvIndexSourceTask].SubItems[0].Text));
                lvIndexSourceTask--;
                if (lvIndexSourceTask < 0) lvIndexSourceTask = 0;
                RefreshSourceTaskPage();
            }
        }

        private int DeleteSourceColumn(string tableName, string id, int oldColumn)
        {
            string sql = "DELETE " + tableName + " WHERE ID = " + id;
            int nAffected = DBAdapter.DBAdapter.DeleteDB(dbConnection, sql);
            DBAdapter.DBAdapter.ExecuteSPAdjustColumnNumbers(dbConnection, Convert.ToInt32(currentConfigID), tableName, oldColumn, 0);
            return nAffected;
        }

        private void lvSourceCase_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndexSourceCase))
            {
                lvSourceCase.Items[lvIndexSourceCase].Checked = false;
                lvIndexSourceCase = e.Item.Index;
            }
        }

        private void lvSourceDocument_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndexSourceDocument))
            {
                lvSourceDocument.Items[lvIndexSourceDocument].Checked = false;
                lvIndexSourceDocument = e.Item.Index;
            }
        }

        private void lvSourceAnnotation_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndexSourceAnnotation))
            {
                lvSourceAnnotation.Items[lvIndexSourceAnnotation].Checked = false;
                lvIndexSourceAnnotation = e.Item.Index;
            }
        }

        private void lvSourceTask_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndexSourceTask))
            {
                lvSourceTask.Items[lvIndexSourceTask].Checked = false;
                lvIndexSourceTask = e.Item.Index;
            }
        }
    }
}
