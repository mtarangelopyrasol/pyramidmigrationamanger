﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MapDefnDialog : Form
    {
        string dbCnxn = string.Empty;
        DialogOperation operation;
        string ID = string.Empty;
        Boolean isDirty = false;

        public MapDefnDialog(string dbConnection, DialogOperation op)
        {
            dbCnxn = dbConnection;
            operation = op;

            InitializeComponent();

            lblHeader.Text = (op == DialogOperation.Add) ? "Add New Mapping Definition" : "Update Mapping Definiton";
        }

        public void Populate(string id, string name)
        {
            ID = id;
            txtName.Text = name;
            txtName.Enabled = MigrationManager.IsMappingUsed(dbCnxn, name) ? false : true;
        }

        private Boolean HasPassedValidation()
        {
            string name = txtName.Text.Trim();
            if (name.Length == 0)
            {
                MessageBox.Show("Please specify a Mapping Name", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (name.Length >= 64)
            {
                MessageBox.Show("Mapping Name is too long. Mapping Names can be at most 64 characters", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            return IsUniqueMapName(name);
        }

        private Boolean IsUniqueMapName(string name)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT * FROM MgrMappings WHERE MappingName = '" + name + "'");
            if (xml.SelectNodes("//row").Count > 0)
            {
                if (operation == DialogOperation.Add)
                {
                    MessageBox.Show("Map " + name + " already exists. Please choose another name.", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
                else
                {
                    string temp = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                    if (temp != ID)
                    {
                        MessageBox.Show("Map " + name + " already exists. Please choose another name.", "Migration Manager", MessageBoxButtons.OK);
                        return false;
                    }
                }
            }
            return true;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (SyncMapDefn())
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }  
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnValues_Click(object sender, EventArgs e)
        {
            if (SyncMapDefn())
            {
                using (ListValueMapDialog dlg = new ListValueMapDialog(dbCnxn, ID, txtName.Text, DialogOperation.Update))
                {
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                    }
                }
            }
        }

        private Boolean SyncMapDefn()
        {
            if (HasPassedValidation())
            {
                if (isDirty)
                {
                    string name = MigrationManager.EscapeSingleQuote(txtName.Text.Trim());
                    if (operation == DialogOperation.Add)
                    {
                        string sql = "INSERT INTO MgrMappings(MappingName) VALUES ('" + name + "')";
                        int nAffected = DBAdapter.DBAdapter.AddToDB(dbCnxn, sql);
                        XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT ID FROM MgrMappings WHERE MappingName = '" + name + "'");
                        ID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                        operation = DialogOperation.Update;
                    }
                    else
                    {
                        string sql = "UPDATE MgrMappings SET MappingName = '" + name + "' WHERE ID = " + ID;
                        int nAffected = DBAdapter.DBAdapter.UpdateDB(dbCnxn, sql);
                    }
                    isDirty = false;
                }
                return true;
            }
            return false;
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            isDirty = true;
        }
    }
}
