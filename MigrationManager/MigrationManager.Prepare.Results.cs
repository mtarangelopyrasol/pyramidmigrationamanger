﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        private void btnConvResultCaseID_Click(object sender, EventArgs e)
        {
            string batchId = txtConvResultBatch.Text;
            if (batchId.Length == 0)
            {
                MessageBox.Show("Please specify a conversion batch in which to perform the search", "Migration Manager", MessageBoxButtons.OK);
            }
            else
            {
                string accountId = txtConvResultCaseID.Text;

                string jobName = GetJobName(listJobs.Items[currentPrepareJobIndex].Tag.ToString());

                DisplayCaseResult(true, treeConvResult, jobName, "SELECT ID, BatchID, CaseType, Status, MMAccountID FROM " + jobName + "_CECases WHERE MMAccountID = '" + accountId + "' AND BatchID = " + batchId);
            }
        }

        private void DisplayCaseResult(Boolean convOption, TreeView tv, string jobName, string sql)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            tv.Nodes.Clear();
            string caseRec = "Case: ";
            if (xml.SelectNodes("//row").Count == 0)
            {
                caseRec += " not found";

                List<TreeNode> roots = new List<TreeNode>();
                roots.Add(tv.Nodes.Add(caseRec));
            }
            else
            {
                XmlNode node = xml.SelectNodes("//row")[0];

                string ID = MigrationManager.GetAttributeValue(node, "ID");
                string accountId = MigrationManager.GetAttributeValue(node, "MMAccountID");
                string status = MigrationManager.GetAttributeValue(node, "Status");
                if (convOption)
                {
                    string batch = MigrationManager.GetAttributeValue(node, "BatchID");
                    string caseType = MigrationManager.GetAttributeValue(node, "CaseType");
                    caseRec += accountId + ", ID: " + ID + ", Conversion Batch: " + batch + ", Type: " + caseType + ", Status: " + status;
                }
                else
                {
                    string batch = MigrationManager.GetAttributeValue(node, "MigrationID");
                    string p8GUID = MigrationManager.GetAttributeValue(node, "CE_GUID");
                    caseRec += accountId + ", ID: " + ID + ", Migration Batch: " + batch + ", GUID: " + p8GUID + ", Status: " + status;
                }

                List<TreeNode> listProperties = GetCaseProperties(jobName, ID);
                TreeNode properties = new TreeNode("Properties (" + listProperties.Count.ToString() + ")", listProperties.ToArray());

                List<TreeNode> listFolders = GetCaseFolders(jobName, ID);
                TreeNode folders = new TreeNode("Folders (" + listFolders.Count.ToString() + ")", listFolders.ToArray());

                List<TreeNode> listDocuments = GetCaseDocuments(convOption, jobName, ID);
                TreeNode documents = new TreeNode("Documents (" + listDocuments.Count.ToString() + ")", listDocuments.ToArray());

                List<TreeNode> listTasks = GetCaseTasks(convOption, jobName, ID);
                TreeNode tasks = new TreeNode("Tasks (" + listTasks.Count.ToString() + ")", listTasks.ToArray());

                List<TreeNode> listAnnotations = GetInheritedAnnotations(convOption, jobName, "CASE", ID);
                TreeNode annotations = new TreeNode("Annotations (" + listAnnotations.Count.ToString() + ")", listAnnotations.ToArray());

                TreeNode[] headers = new TreeNode[] { properties, folders, documents, tasks, annotations };
                
                TreeNode root = new TreeNode(caseRec, headers);
                tv.Nodes.Add(root);
            }
        }

        private List<TreeNode> GetCaseProperties(string jobName, string caseID)
        {
            XmlDocument propXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT Name, Value, Type FROM " + jobName + "_CECaseProperties WHERE CaseID = " + caseID);

            List<TreeNode> propList = new List<TreeNode>();
            foreach (XmlElement rowNode in propXml.SelectNodes("//row"))
            {
                string name = rowNode.GetAttribute("Name");
                string value = rowNode.GetAttribute("Value");
                string dType = rowNode.GetAttribute("Type");

                string propRec = name + ": " + value + " (" + dType + ")";
                propList.Add(new TreeNode(propRec));
            }
            return propList;
        }

        private List<TreeNode> GetCaseFolders(string jobName, string caseID)
        {
            XmlDocument foldXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT Name, Parent FROM " + jobName + "_CECaseFolders WHERE CaseID = " + caseID);
            List<TreeNode> folderList = new List<TreeNode>();
            foreach (XmlElement rowNode in foldXml.SelectNodes("//row"))
            {
                string name = rowNode.GetAttribute("Name");
                string parent = rowNode.GetAttribute("Parent");

                string folderRec = parent + " -> " + name;
                folderList.Add(new TreeNode(folderRec));
            }
            return folderList;
        }

        private List<TreeNode> GetCaseDocuments(Boolean convOption, string jobName, string caseID)
        {
            XmlDocument docXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM " + jobName + "_CEDocuments WHERE CaseID = " + caseID);

            List<TreeNode> docList = new List<TreeNode>();
            foreach (XmlElement rowNode in docXml.SelectNodes("//row"))
            {
                string ID = rowNode.GetAttribute("ID");
                string docID = rowNode.GetAttribute("MMAccountID");
                string status = rowNode.GetAttribute("Status");
                string docRec = "Doc: " + docID + ", ID: " + ID;
                if (convOption)
                {
                    string batch = rowNode.GetAttribute("BatchID");
                    string docClass = rowNode.GetAttribute("DocClass");
                    string mimeType = rowNode.GetAttribute("MimeType");
                    string folder = rowNode.GetAttribute("Folder");
                    string cName = rowNode.GetAttribute("ContainmentName");
                    docRec += ", Conversion Batch: " + batch + ", Class: " + docClass + ", MimeType: " + mimeType + ", Folder: " + folder + ", ContainmentName: " + cName + ", Status: " + status;
                }
                else
                {
                    string batch = rowNode.GetAttribute("MigrationID");
                    string p8GUID = rowNode.GetAttribute("CE_GUID");
                    docRec += ", Migration Batch: " + batch + ", GUID: " + p8GUID + ", Status: " + status;
                }

                List<TreeNode> listDocProperties = GetDocProperties(jobName, ID);
                TreeNode properties = new TreeNode("Properties (" + listDocProperties.Count.ToString() + ")", listDocProperties.ToArray());

                List<TreeNode> listDocPages = GetDocPages(convOption, jobName, ID);
                TreeNode pages = new TreeNode("Pages (" + listDocPages.Count.ToString() + ")", listDocPages.ToArray());

                List<TreeNode> listAnnotations = GetInheritedAnnotations(convOption, jobName, "DOCUMENT", ID);
                TreeNode annotations = new TreeNode("Annotations (" + listAnnotations.Count.ToString() + ")", listAnnotations.ToArray());

                TreeNode[] headers = new TreeNode[] { properties, pages, annotations };
                docList.Add(new TreeNode(docRec, headers));
            }
            return docList;
        }

        private List<TreeNode> GetCaseTasks(Boolean convOption, string jobName, string caseID)
        {
            XmlDocument taskXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM " + jobName + "_CETasks WHERE CaseID = " + caseID);

            List<TreeNode> taskList = new List<TreeNode>();
            foreach (XmlElement rowNode in taskXml.SelectNodes("//row"))
            {
                string ID = rowNode.GetAttribute("ID");
                string name = rowNode.GetAttribute("TaskName");
                string desc = rowNode.GetAttribute("TaskDesc");
                string status = rowNode.GetAttribute("Status");
                string docInit = (rowNode.GetAttribute("IsDocInitiated") == "1") ? "YES" : "NO";

                string taskRec = "Task: " + name + ", ID: " + ID + ", Description: " + desc + ", Status: " + status + ", Document Initiated: " + docInit;
                if (convOption)
                {
                    string batch = rowNode.GetAttribute("BatchID");
                    taskRec += ", Conversion Batch: " + batch + ", Status: " + status;
                }
                else
                {
                    string batch = rowNode.GetAttribute("MigrationID");
                    string p8GUID = rowNode.GetAttribute("CE_GUID");
                    taskRec += ", Migration Batch: " + batch + ", GUID: " + p8GUID + ", Status: " + status;
                }
                List<TreeNode> listTaskProperties = GetTaskProperties(jobName, ID);
                TreeNode properties = new TreeNode("Properties (" + listTaskProperties.Count.ToString() + ")", listTaskProperties.ToArray());

                List<TreeNode> listAnnotations = GetInheritedAnnotations(convOption, jobName, "TASK", ID);
                TreeNode annotations = new TreeNode("Annotations (" + listAnnotations.Count.ToString() + ")", listAnnotations.ToArray());

                TreeNode[] headers = new TreeNode[] { properties, annotations };
                taskList.Add(new TreeNode(taskRec, headers));
            }
            return taskList;
        }

        private List<TreeNode> GetDocProperties(string jobName, string docID)
        {
            XmlDocument propXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT Name, Value, Type FROM " + jobName + "_CEDocumentProperties WHERE DocumentID = " + docID);

            List<TreeNode> propList = new List<TreeNode>();
            foreach (XmlElement rowNode in propXml.SelectNodes("//row"))
            {
                string name = rowNode.GetAttribute("Name");
                string value = rowNode.GetAttribute("Value");
                string dType = rowNode.GetAttribute("Type");

                string propRec = name + ": " + value + " (" + dType + ")";
                propList.Add(new TreeNode(propRec));
            }
            return propList;
        }

        private List<TreeNode> GetDocPages(Boolean convOption, string jobName, string docID)
        {
            XmlDocument pageXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT PageNum, FilePath, MimeType FROM " + jobName + "_CEDocumentPages WHERE DocumentID = " + docID);

            List<TreeNode> pageList = new List<TreeNode>();
            foreach (XmlElement rowNode in pageXml.SelectNodes("//row"))
            {
                string pageNum = rowNode.GetAttribute("PageNum");
                string filePath = rowNode.GetAttribute("FilePath");
                string mimeType = rowNode.GetAttribute("MimeType");

                string propRec = "Page Number: " + pageNum + ", File Path: " + filePath + ", MimeType: " + mimeType;
                
                List<TreeNode> annList = GetBaseAnnotations(convOption, jobName, docID);
                if (annList.Count > 0)
                {
                    TreeNode anns = new TreeNode("Annotations (" + annList.Count.ToString() + ")", annList.ToArray());

                    TreeNode[] subheaders = new TreeNode[] { anns };
                    pageList.Add(new TreeNode(propRec, subheaders));
                }
                else
                    pageList.Add(new TreeNode(propRec));
            }
            return pageList;
        }

        private List<TreeNode> GetBaseAnnotations(Boolean convOption, string jobName, string docID)
        {
            XmlDocument annXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM " + jobName + "_CEAnnotations WHERE AnnType = 'DOCUMENT' AND ObjectID = " + docID);

            List<TreeNode> annList = new List<TreeNode>();
            foreach (XmlElement rowNode in annXml.SelectNodes("//row"))
            {
                string ID = rowNode.GetAttribute("ID");
                string annClass = rowNode.GetAttribute("AnnClass");
                string annRec = annClass + ", ID: " + ID;
          
                if (convOption)
                {
                    string batch = rowNode.GetAttribute("BatchID");
                    annRec += ", Conversion Batch: " + batch + ", Status: " + rowNode.GetAttribute("Status");
                }
                else
                {
                    string batch = rowNode.GetAttribute("MigrationID");
                    string p8GUID = rowNode.GetAttribute("CE_GUID");
                    annRec += ", Migration Batch: " + batch + ", GUID: " + p8GUID + ", Status: " + rowNode.GetAttribute("Status");
                }
                if (annClass == "Annotation")
                {
                    string filePath = rowNode.GetAttribute("FilePath");
                    annRec += ", File Path: ";
                    annRec += filePath;
                }
                annList.Add(new TreeNode(annRec));
            }
            return annList;
        }

        private List<TreeNode> GetInheritedAnnotations(Boolean convOption, string jobName, string annType, string objID)
        {
            XmlDocument annXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM " + jobName + "_CEAnnotations WHERE AnnType = '" + annType + "' AND ObjectID = " + objID);

            List<TreeNode> annList = new List<TreeNode>();
            foreach (XmlElement rowNode in annXml.SelectNodes("//row"))
            {
                string annClass = rowNode.GetAttribute("AnnClass");
                string ID = rowNode.GetAttribute("ID");
                string annRec = annClass + ", ID: " + ID;
                if (convOption)
                {
                    string batch = rowNode.GetAttribute("BatchID");
                    annRec += ", Conversion Batch: " + batch + ", Status: " + rowNode.GetAttribute("Status");
                }
                else
                {
                    string batch = rowNode.GetAttribute("MigrationID");
                    string p8GUID = rowNode.GetAttribute("CE_GUID");
                    annRec += ", Migration Batch: " + batch + ", GUID: " + p8GUID + ", Status: " + rowNode.GetAttribute("Status");
                }
                if (annClass == "Annotation")
                {
                    string filePath = rowNode.GetAttribute("FilePath");
                    annRec += ", File Path: ";
                    annRec += filePath;
                }
                List<TreeNode> listAnnProperties = GetAnnProperties(jobName, ID);
                TreeNode properties = new TreeNode("Properties (" + listAnnProperties.Count.ToString() + ")", listAnnProperties.ToArray());

                TreeNode[] headers = new TreeNode[] { properties };
                annList.Add(new TreeNode(annRec, headers));
            }
            return annList;
        }

        private List<TreeNode> GetAnnProperties(string jobName, string annID)
        {
            XmlDocument propXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT Name, Value, Type FROM " + jobName + "_CEAnnotationProperties WHERE AnnotationID = " + annID);

            List<TreeNode> propList = new List<TreeNode>();
            foreach (XmlElement rowNode in propXml.SelectNodes("//row"))
            {
                string name = rowNode.GetAttribute("Name");
                string value = rowNode.GetAttribute("Value");
                string dType = rowNode.GetAttribute("Type");

                string propRec = name + ": " + value + " (" + dType + ")";
                propList.Add(new TreeNode(propRec));
            }
            return propList;
        }

        private void btnConvResultDocID_Click(object sender, EventArgs e)
        {
            string batchId = txtConvResultBatch.Text;
            if (batchId.Length == 0)
            {
                MessageBox.Show("Please specify a conversion batch in which to perform the search", "Migration Manager", MessageBoxButtons.OK);
            }
            else
            {
                string accountId = txtConvResultDocID.Text;

                string jobName = GetJobName(listJobs.Items[currentPrepareJobIndex].Tag.ToString());

                DisplayDocResult(true, treeConvResult, jobName, "SELECT * FROM " + jobName + "_CEDocuments WHERE MMAccountID = '" + accountId + "' AND BatchId = " + batchId);
            }
        }

        private void DisplayDocResult(Boolean convOption, TreeView tv, string jobName, string sql)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            tv.Nodes.Clear();
            if (xml.SelectNodes("//row").Count == 0)
            {
                List<TreeNode> roots = new List<TreeNode>();
                roots.Add(tv.Nodes.Add("Document not found"));
            }
            else
            {
                List<TreeNode> docList = new List<TreeNode>();
                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    string mmID = rowNode.GetAttribute("ID");
                    string status = rowNode.GetAttribute("Status");
                    string accountId = rowNode.GetAttribute("MMAccountID");
                    string docRec = "Document: " + accountId + ", ID: " + mmID;
                    if (convOption)
                    {
                        string batch = rowNode.GetAttribute("BatchID");
                        string docClass = rowNode.GetAttribute("DocClass");
                        string mimeType = rowNode.GetAttribute("MimeType");
                        string folder = rowNode.GetAttribute("Folder");
                        string cName = rowNode.GetAttribute("ContainmentName");
                        docRec += ", Conversion Batch: " + batch + ", Class: " + docClass + ", MimeType: " + mimeType + ", Folder: " + folder + ", ContainmentName: " + cName + ", Status: " + status;
                    }
                    else
                    {
                        string batch = rowNode.GetAttribute("MigrationID");
                        string p8GUID = rowNode.GetAttribute("CE_GUID");
                        docRec += ", Ingestion Batch: " + batch + ", GUID: " + p8GUID + ", Status: " + status;
                    }
                    
                    List<TreeNode> listDocProperties = GetDocProperties(jobName, mmID);
                    TreeNode properties = new TreeNode("Properties (" + listDocProperties.Count.ToString() + ")", listDocProperties.ToArray());

                    List<TreeNode> listDocPages = GetDocPages(convOption, jobName, mmID);
                    TreeNode pages = new TreeNode("Pages (" + listDocPages.Count.ToString() + ")", listDocPages.ToArray());

                    List<TreeNode> listAnnotations = GetInheritedAnnotations(convOption, jobName, "DOCUMENT", mmID);
                    TreeNode annotations = new TreeNode("Annotations (" + listAnnotations.Count.ToString() + ")", listAnnotations.ToArray());

                    TreeNode[] subheaders = new TreeNode[] { properties, pages, annotations };
                    docList.Add(new TreeNode(docRec, subheaders));
                }
                TreeNode root = new TreeNode("Documents (" + docList.Count.ToString() + ")", docList.ToArray());
                tv.Nodes.Add(root);
            }
        }

        private void DisplayTaskResult(Boolean convOption, TreeView tv, string jobName, string sql)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            tv.Nodes.Clear();
            if (xml.SelectNodes("//row").Count == 0)
            {
                List<TreeNode> roots = new List<TreeNode>();
                roots.Add(tv.Nodes.Add("Task not found"));
            }
            else
            {
                List<TreeNode> taskList = new List<TreeNode>();
                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    string mmID = rowNode.GetAttribute("ID");
                    string status = rowNode.GetAttribute("Status");
                    string accountId = rowNode.GetAttribute("MMAccountID");
                    string taskRec = "Task: " + accountId + ", ID: " + mmID;
                    if (convOption)
                    {
                        string batch = rowNode.GetAttribute("BatchID");
                        string taskName = rowNode.GetAttribute("TaskName");
                        string docInitiated = rowNode.GetAttribute("IsDocInitiated");
                        taskRec += ", Conversion Batch: " + batch + ", Name: " + taskName + ", Document Initated: " + docInitiated + ", Status: " + status;
                    }
                    else
                    {
                        string batch = rowNode.GetAttribute("MigrationID");
                        string p8GUID = rowNode.GetAttribute("CE_GUID");
                        taskRec += ", Ingestion Batch: " + batch + ", GUID: " + p8GUID + ", Status: " + status;
                    }

                    List<TreeNode> listTaskProperties = GetTaskProperties(jobName, mmID);
                    TreeNode properties = new TreeNode("Properties (" + listTaskProperties.Count.ToString() + ")", listTaskProperties.ToArray());

                    List<TreeNode> listAnnotations = GetInheritedAnnotations(convOption, jobName, "TASK", mmID);
                    TreeNode annotations = new TreeNode("Annotations (" + listAnnotations.Count.ToString() + ")", listAnnotations.ToArray());

                    TreeNode[] subheaders = new TreeNode[] { properties, annotations };
                    taskList.Add(new TreeNode(taskRec, subheaders));
                }
                TreeNode root = new TreeNode("Documents (" + taskList.Count.ToString() + ")", taskList.ToArray());
                tv.Nodes.Add(root);
            }
        }

        private List<TreeNode> GetTaskProperties(string jobName, string taskID)
        {
            XmlDocument propXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT Name, Value, Type FROM " + jobName + "_CETaskProperties WHERE TaskID = " + taskID);

            List<TreeNode> propList = new List<TreeNode>();
            foreach (XmlElement rowNode in propXml.SelectNodes("//row"))
            {
                string name = rowNode.GetAttribute("Name");
                string value = rowNode.GetAttribute("Value");
                string dType = rowNode.GetAttribute("Type");

                string propRec = name + ": " + value + " (" + dType + ")";
                propList.Add(new TreeNode(propRec));
            }
            return propList;
        }

        private void DisplayAnnResult(Boolean convOption, TreeView tv, string jobName, string sql)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            tv.Nodes.Clear();
            if (xml.SelectNodes("//row").Count == 0)
            {
                List<TreeNode> roots = new List<TreeNode>();
                roots.Add(tv.Nodes.Add("Annotation not found"));
            }
            else
            {
                List<TreeNode> annList = new List<TreeNode>();
                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    string mmID = rowNode.GetAttribute("ID");
                    string status = rowNode.GetAttribute("Status");
                    string accountId = rowNode.GetAttribute("MMAccountID");
                    string docRec = "Document: " + accountId + ", ID: " + mmID;
                    if (convOption)
                    {
                        string batch = rowNode.GetAttribute("BatchID");
                        string docClass = rowNode.GetAttribute("DocClass");
                        string mimeType = rowNode.GetAttribute("MimeType");
                        string folder = rowNode.GetAttribute("Folder");
                        string cName = rowNode.GetAttribute("ContainmentName");
                        docRec += ", Conversion Batch: " + batch + ", Class: " + docClass + ", MimeType: " + mimeType + ", Folder: " + folder + ", ContainmentName: " + cName + ", Status: " + status;
                    }
                    else
                    {
                        string batch = rowNode.GetAttribute("MigrationID");
                        string p8GUID = rowNode.GetAttribute("CE_GUID");
                        docRec += ", Ingestion Batch: " + batch + ", GUID: " + p8GUID + ", Status: " + status;
                    }
                    
                    List<TreeNode> listDocProperties = GetDocProperties(jobName, mmID);
                    TreeNode properties = new TreeNode("Properties (" + listDocProperties.Count.ToString() + ")", listDocProperties.ToArray());

                    List<TreeNode> listDocPages = GetDocPages(convOption, jobName, mmID);
                    TreeNode pages = new TreeNode("Pages (" + listDocPages.Count.ToString() + ")", listDocPages.ToArray());

                    TreeNode[] subheaders = new TreeNode[] { properties, pages };
                    annList.Add(new TreeNode(docRec, subheaders));
                }
                TreeNode root = new TreeNode("Documents (" + annList.Count.ToString() + ")", annList.ToArray());
                tv.Nodes.Add(root);
            }
        }
    }
}
