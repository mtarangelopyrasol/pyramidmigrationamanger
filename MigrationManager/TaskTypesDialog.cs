﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class TaskTypesDialog : Form
    {
        string dbCnxn = string.Empty;
        string configID = string.Empty;
        DialogOperation operation;
        string ID = string.Empty;
        string parentCaseID = string.Empty;
        Dictionary<string, string> caseMap = new Dictionary<string, string>();
        Boolean isDirty = false;

        public TaskTypesDialog(string dbConnection, string cfgID, DialogOperation op)
        {
            dbCnxn = dbConnection;
            configID = cfgID;
            operation = op;

            InitializeComponent();

            if (op == DialogOperation.Add)
            {
                lblHeader.Text = "Add New Task Type";
            }
            else
            {
                lblHeader.Text = "Update Task Type Definition";
            }
            PopulateAllCombos();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (SyncTaskType())
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnProperties_Click(object sender, EventArgs e)
        {
            if (SyncTaskType())
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT ID FROM CfgTaskTypes WHERE CaseID = " + parentCaseID + " AND TaskName = '" + txtTaskName.Text.Trim() + "'");
                ID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;

                using (ListPropertiesDialog dlg = new ListPropertiesDialog(dbCnxn, configID, ID, PropertyType.Task))
                {
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        // nothing for now
                    }
                }
            }
        }

        private void PopulateAllCombos()
        {
            cmbDocInitiated.SelectedIndex = 0;
            PopulateParentCase();
        }

        private void PopulateParentCase()
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT ID, CaseName FROM CfgCaseTypes WHERE ConfigID = " + configID);
            int count = xml.SelectNodes("//row").Count;
            if (count > 0)
            {
                cmbParentCase.BeginUpdate();
                cmbParentCase.Items.Clear();
                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    cmbParentCase.Items.Add(rowNode.GetAttribute("CaseName"));
                    caseMap.Add(rowNode.GetAttribute("CaseName"), rowNode.GetAttribute("ID"));
                }
                cmbParentCase.EndUpdate();
                cmbParentCase.SelectedIndex = 0;
            }
        }

        private void PopulateInitiatingDocClass()
        {
            cmbInitiatingDocClass.BeginUpdate();
            cmbInitiatingDocClass.Items.Clear();
            cmbInitiatingDocClass.Items.Add(" ");
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT DocClassName FROM CfgDocClasses WHERE ConfigID = " + configID);
            if (xml.SelectNodes("//row").Count > 0)
            {
                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    cmbInitiatingDocClass.Items.Add(rowNode.GetAttribute("DocClassName"));
                }
            }
            cmbInitiatingDocClass.EndUpdate();
            cmbInitiatingDocClass.SelectedIndex = 0;
        }

        private string GetDocClassID(string docClassName)
        {
            string docClassID = string.Empty;
            if (docClassName.Trim().Length > 0)
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT ID FROM CfgDocClasses WHERE DocClassName = '" + docClassName + "' AND ConfigID = " + configID);
                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    docClassID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                }
            }
            return docClassID;
        }

        private void PopulateAttachingProperty()
        {
            string docClassName = cmbInitiatingDocClass.SelectedItem.ToString().Trim();
            string docClassID = GetDocClassID(docClassName);
            cmbAttachingProperty.BeginUpdate();
            cmbAttachingProperty.Items.Clear();
            cmbAttachingProperty.Items.Add(" ");
            if (docClassID.Length > 0)
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT Name FROM CfgDocClassProperties WHERE DocClassID = " + docClassID + " AND ConfigID = " + configID);
                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        cmbAttachingProperty.Items.Add(rowNode.GetAttribute("Name"));
                    }
                }
            }
            cmbAttachingProperty.EndUpdate();
            cmbAttachingProperty.SelectedIndex = 0;
        }

        public void Populate(string id)
        {
            ID = id;

            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT * FROM CfgTaskTypes WHERE ID = " + ID);

            txtTaskName.Text = xml.SelectNodes("//row")[0].Attributes["TaskName"].Value;

            string parentCase = string.Empty;
            parentCaseID = xml.SelectNodes("//row")[0].Attributes["CaseID"].Value;
            foreach (string k in caseMap.Keys)
            {
                if (caseMap[k] == parentCaseID)
                {
                    parentCase = k;
                    break;
                }
            }
            SetSelection(cmbParentCase, parentCase);
            SetSelection(cmbDocInitiated, xml.SelectNodes("//row")[0].Attributes["IsDocInitiated"].Value);
            XmlAttribute xa = xml.SelectNodes("//row")[0].Attributes["InitiatingDocClass"];
            if(xa != null)
            {
                SetSelection(cmbInitiatingDocClass, xa.Value);
            }
            
            txtInitiatingDocIdSrc.Text = xml.SelectNodes("//row")[0].Attributes["InitiatingDocIdSource"].Value;

            SetSelection(cmbAttachingProperty, xml.SelectNodes("//row")[0].Attributes["AttachingProperty"].Value);
        }

        private Boolean HasPassedValidation()
        {
            if (txtTaskName.Text.Length == 0)
            {
                MessageBox.Show("Please specify a Task Type Name", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (!IsUniqueTaskName())
            {
                return false;
            }
            string isDocInit = (cmbDocInitiated.Items.Count > 0) ? cmbDocInitiated.SelectedItem.ToString() : "0";
            string initDocClass = (cmbInitiatingDocClass.Items.Count > 0) ? cmbInitiatingDocClass.SelectedItem.ToString().Trim() : string.Empty;
            string initDocIdSrc = txtInitiatingDocIdSrc.Text.Trim();
            string attachProp = (cmbAttachingProperty.Items.Count > 0) ? cmbAttachingProperty.SelectedItem.ToString().Trim() : string.Empty;
            if (isDocInit == "0")
            {
                if (initDocClass.Length > 0)
                {
                    MessageBox.Show("You have specified an initiating document class for a task that is not document initiated", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
                if (initDocIdSrc.Length > 0)
                {
                    MessageBox.Show("You have specified an initiating document ID source for a task that is not document initiated", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
                if (attachProp.Length > 0)
                {
                    MessageBox.Show("You have specified an attaching property for a task that is not document initiated", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
            }
            else
            {
                if (initDocClass.Length == 0)
                {
                    MessageBox.Show("You have not specified an initiating document class for a task that is document initiated", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
                if (initDocIdSrc.Length == 0)
                {
                    MessageBox.Show("You have not specified an initiating document ID source for a task that is document initiated", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
                if (attachProp.Length == 0)
                {
                    MessageBox.Show("You have not specified an attaching property for a task that is document initiated", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
            }
            return true;
        }

        private Boolean IsUniqueTaskName()
        {
            string taskType = txtTaskName.Text.Trim();
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT * FROM CfgTaskTypes WHERE TaskName = '" + taskType + "' AND CaseID = " + parentCaseID);
            if (xml.SelectNodes("//row").Count > 0)
            {
                if (operation == DialogOperation.Add)
                {
                    MessageBox.Show("Task " + taskType + " already exists. Please choose another name.", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
                else
                {
                    string temp = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                    if (temp != ID)
                    {
                        MessageBox.Show("Task " + taskType + " already exists. Please choose another name.", "Migration Manager", MessageBoxButtons.OK);
                        return false;
                    }
                }
            }
            return true;
        }

        private Boolean SyncTaskType()
        {
            string parentCase = (cmbParentCase.Items.Count > 0) ? cmbParentCase.SelectedItem.ToString() : string.Empty;
            parentCaseID = caseMap[parentCase];

            if (HasPassedValidation())
            {
                if (isDirty)
                {
                    string taskType = txtTaskName.Text.Trim();
                    
                    string isDocInit = (cmbDocInitiated.Items.Count > 0) ? cmbDocInitiated.SelectedItem.ToString() : "0";
                    string initDocClass = (cmbInitiatingDocClass.Items.Count > 0) ? cmbInitiatingDocClass.SelectedItem.ToString() : string.Empty;
                    string initDocIdSrc = txtInitiatingDocIdSrc.Text.Trim();
                    string attachProp = (cmbAttachingProperty.Items.Count > 0) ? cmbAttachingProperty.SelectedItem.ToString() : string.Empty;

                    string sql = string.Empty;
                    if (operation == DialogOperation.Add)
                    {
                        sql = "INSERT INTO CfgTaskTypes (CaseID, TaskName, IsDocInitiated, InitiatingDocClass, InitiatingDocIdSource, AttachingProperty) VALUES (" + parentCaseID + ", '" + taskType + "', " + isDocInit + ", '" + initDocClass + "', '" + initDocIdSrc + "', '" + attachProp + "')";
                        int nAffected = DBAdapter.DBAdapter.AddToDB(dbCnxn, sql);
                        XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT ID FROM CfgTaskTypes WHERE TaskName = '" + taskType + "' AND CaseID = " + parentCaseID);
                        ID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                        operation = DialogOperation.Update;
                    }
                    else
                    {
                        sql = "UPDATE CfgTaskTypes SET CaseID = " + parentCaseID + ", TaskName = '" + taskType + "', IsDocInitiated = " + isDocInit + ", InitiatingDocClass = '" + initDocClass + "', InitiatingDocIdSource = '" + initDocIdSrc + "', AttachingProperty = '" + attachProp + "' WHERE ID = " + ID;
                        int nAffected = DBAdapter.DBAdapter.UpdateDB(dbCnxn, sql);
                    }
                    isDirty = false;
                }
                return true;
            }
            return false;
        }

        private void SetSelection(ComboBox cmb, string value)
        {
            for (int i = 0; i < cmb.Items.Count; i++)
            {
                if (cmb.Items[i].ToString() == value)
                {
                    cmb.SelectedItem = cmb.Items[i];
                    break;
                }
            }
        }

        private void cmbParentCase_SelectedIndexChanged(object sender, EventArgs e)
        {
            isDirty = true;
        }

        private void txtTaskName_TextChanged(object sender, EventArgs e)
        {
            isDirty = true;
        }

        private void cmbDocInitiated_SelectedIndexChanged(object sender, EventArgs e)
        {
            isDirty = true;
            PopulateInitiatingDocClass();
        }

        private void cmbInitiatingDocClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            isDirty = true;
            PopulateAttachingProperty();
        }

        private void txtInitiatingDocIdSrc_TextChanged(object sender, EventArgs e)
        {
            isDirty = true;
        }

        private void cmbAttachingProperty_SelectedIndexChanged(object sender, EventArgs e)
        {
            isDirty = true;
        }
    }
}
