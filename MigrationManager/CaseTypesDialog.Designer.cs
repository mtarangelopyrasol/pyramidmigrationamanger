﻿namespace MigrationManager
{
    partial class CaseTypesDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHeader = new System.Windows.Forms.Label();
            this.lblCaseType = new System.Windows.Forms.Label();
            this.lblDocClass = new System.Windows.Forms.Label();
            this.txtCaseType = new System.Windows.Forms.TextBox();
            this.cmbDocClass = new System.Windows.Forms.ComboBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnProperties = new System.Windows.Forms.Button();
            this.btnFolders = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Location = new System.Drawing.Point(186, 33);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(64, 23);
            this.lblHeader.TabIndex = 0;
            this.lblHeader.Text = "Header";
            // 
            // lblCaseType
            // 
            this.lblCaseType.AutoSize = true;
            this.lblCaseType.Location = new System.Drawing.Point(27, 78);
            this.lblCaseType.Name = "lblCaseType";
            this.lblCaseType.Size = new System.Drawing.Size(93, 23);
            this.lblCaseType.TabIndex = 1;
            this.lblCaseType.Text = "Case Type:";
            // 
            // lblDocClass
            // 
            this.lblDocClass.AutoSize = true;
            this.lblDocClass.Location = new System.Drawing.Point(27, 130);
            this.lblDocClass.Name = "lblDocClass";
            this.lblDocClass.Size = new System.Drawing.Size(196, 23);
            this.lblDocClass.TabIndex = 2;
            this.lblDocClass.Text = "Default Document Class:";
            // 
            // txtCaseType
            // 
            this.txtCaseType.Location = new System.Drawing.Point(229, 78);
            this.txtCaseType.Name = "txtCaseType";
            this.txtCaseType.Size = new System.Drawing.Size(327, 27);
            this.txtCaseType.TabIndex = 3;
            this.txtCaseType.TextChanged += new System.EventHandler(this.txtCaseType_TextChanged);
            // 
            // cmbDocClass
            // 
            this.cmbDocClass.FormattingEnabled = true;
            this.cmbDocClass.Location = new System.Drawing.Point(229, 130);
            this.cmbDocClass.Name = "cmbDocClass";
            this.cmbDocClass.Size = new System.Drawing.Size(327, 31);
            this.cmbDocClass.TabIndex = 4;
            this.cmbDocClass.SelectedIndexChanged += new System.EventHandler(this.cmbDocClass_SelectedIndexChanged);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(481, 225);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 32);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(400, 225);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 32);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnProperties
            // 
            this.btnProperties.Location = new System.Drawing.Point(27, 225);
            this.btnProperties.Name = "btnProperties";
            this.btnProperties.Size = new System.Drawing.Size(101, 32);
            this.btnProperties.TabIndex = 7;
            this.btnProperties.Text = "Properties";
            this.btnProperties.UseVisualStyleBackColor = true;
            this.btnProperties.Click += new System.EventHandler(this.btnProperties_Click);
            // 
            // btnFolders
            // 
            this.btnFolders.Location = new System.Drawing.Point(134, 225);
            this.btnFolders.Name = "btnFolders";
            this.btnFolders.Size = new System.Drawing.Size(75, 32);
            this.btnFolders.TabIndex = 8;
            this.btnFolders.Text = "Folders";
            this.btnFolders.UseVisualStyleBackColor = true;
            this.btnFolders.Click += new System.EventHandler(this.btnFolders_Click);
            // 
            // CaseTypesDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(587, 277);
            this.Controls.Add(this.btnFolders);
            this.Controls.Add(this.btnProperties);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.cmbDocClass);
            this.Controls.Add(this.txtCaseType);
            this.Controls.Add(this.lblDocClass);
            this.Controls.Add(this.lblCaseType);
            this.Controls.Add(this.lblHeader);
            this.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "CaseTypesDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "CaseTypesDialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.Label lblCaseType;
        private System.Windows.Forms.Label lblDocClass;
        private System.Windows.Forms.TextBox txtCaseType;
        private System.Windows.Forms.ComboBox cmbDocClass;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnProperties;
        private System.Windows.Forms.Button btnFolders;
    }
}