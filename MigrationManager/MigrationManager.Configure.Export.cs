﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        private void btnExportConfig_Click(object sender, EventArgs e)
        {
            if (configSelected)
            {
                UpdateCurrentConfig();
                string exportFolder = GetDestinationFolder("Select destination folder.");
                if (exportFolder.Length > 0)
                {
                    string exportFile = FormulateExportFileName(exportFolder, "MM_Config", currentConfigName);
                    if (exportFile.Length > 0)
                    {
                        XmlTextWriter xtw = StartExport(exportFile);
                        if (xtw != null)
                        {
                            Dictionary<string, string> mapList = new Dictionary<string, string>();
                            ExportConfig(currentConfigID, mapList, xtw);
                            ExportMappings(mapList, xtw); // export required mappings
                            EndExport(xtw, exportFile);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("You have not chosen any configurations to export.", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void ExportConfig(string configID, Dictionary<string, string> mapList, XmlTextWriter xtw)
        {
            xtw.WriteStartElement("mmConfiguration");

            string sql = "SELECT ConfigName, ConfigLevel, Comment FROM MgrConfigurations WHERE ID = " + configID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            XmlAttributeCollection xac = xml.SelectNodes("//row")[0].Attributes;
            xtw.WriteAttributeString("ConfigName", xac["ConfigName"].Value);
            xtw.WriteAttributeString("ConfigLevel", xac["ConfigLevel"].Value);
            xtw.WriteAttributeString("Comment", xac["Comment"].Value);

            ExportSource(configID, xtw);
            ExportTarget(configID, mapList, xtw);

            // Export Identifiers
            ExportSQLResult("mmIdentifiers", "mmIdentifier", "SELECT IdentifierName, FieldName FROM CfgIdentifiers WHERE ConfigID = " + configID, xtw);

            xtw.WriteEndElement();
        }

        private void ExportSource(string configID, XmlTextWriter xtw)
        {
            ExportSQLResult("mmDocumentSource", "mmSourceColumn", 
                "SELECT ColumnID, Name, DataType, MaxLength, Format, MultiValued FROM CfgDocumentSource WHERE ConfigID = " + configID, xtw);
            ExportSQLResult("mmAnnotationSource", "mmSourceColumn", 
                "SELECT ColumnID, Name, DataType, MaxLength, Format, MultiValued FROM CfgAnnotationSource WHERE ConfigID = " + configID, xtw);
            if (GetConfigLevel(configID) == "CASE")
            {
                ExportSQLResult("mmCaseSource", "mmSourceColumn", 
                    "SELECT ColumnID, Name, DataType, MaxLength, Format, MultiValued FROM CfgCaseSource WHERE ConfigID = " + configID, xtw);
                ExportSQLResult("mmTaskSource", "mmSourceColumn", 
                    "SELECT ColumnID, Name, DataType, MaxLength, Format, MultiValued FROM CfgTaskSource WHERE ConfigID = " + configID, xtw);
            }
        }

        private string GetConfigLevel(string configID)
        {
            string sql = "SELECT ConfigLevel FROM MgrConfigurations WHERE ID = " + configID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
            XmlNodeList xnl = xml.SelectNodes("//row");
            return (xnl.Count > 0) ? xnl[0].Attributes["ConfigLevel"].Value : string.Empty;
        }

        private void ExportTarget(string configID, Dictionary<string, string> mapList, XmlTextWriter xtw)
        {
            if (GetConfigLevel(configID) == "CASE")
            {
                ExportTargetCaseTypes(configID, mapList, xtw);
            }
            ExportTargetDocumentTypes(configID, mapList, xtw);
            ExportTargetAnnotationTypes(configID, mapList, xtw);
        }

        private void ExportTargetCaseTypes(string configID, Dictionary<string, string> mapList, XmlTextWriter xtw)
        {
            xtw.WriteStartElement("mmCaseTypes");

            string sql = "SELECT ID FROM CfgCaseTypes WHERE ConfigID = " + configID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            foreach (XmlElement row in xml.SelectNodes("//row"))
            {
                ExportTargetCaseType(configID, row.GetAttribute("ID"), mapList, xtw);  
            }

            xtw.WriteEndElement();
        }

        private void ExportTargetDocumentTypes(string configID, Dictionary<string, string> mapList, XmlTextWriter xtw)
        {
            xtw.WriteStartElement("mmDocumentTypes");

            string sql = "SELECT ID FROM CfgDocClasses WHERE ConfigID = " + configID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            foreach (XmlElement row in xml.SelectNodes("//row"))
            {
                ExportTargetDocumentType(configID, row.GetAttribute("ID"), mapList, xtw); 
            }

            xtw.WriteEndElement();
        }

        private void ExportTargetAnnotationTypes(string configID, Dictionary<string, string> mapList, XmlTextWriter xtw)
        {
            xtw.WriteStartElement("mmAnnotationTypes");

            string sql = "SELECT ID FROM CfgAnnClasses WHERE ConfigID = " + configID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            foreach (XmlElement row in xml.SelectNodes("//row"))
            {
                ExportTargetAnnotationType(configID, row.GetAttribute("ID"), mapList, xtw);
            }

            xtw.WriteEndElement();
        }

        private void ExportTargetTaskTypes(string configID, string caseTypeID, Dictionary<string, string> mapList, XmlTextWriter xtw)
        {
            xtw.WriteStartElement("mmTaskTypes");

            string sql = "SELECT ID FROM CfgTaskTypes WHERE CaseID = " + caseTypeID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            foreach (XmlElement row in xml.SelectNodes("//row"))
            {
                ExportTargetTaskType(configID, row.GetAttribute("ID"), mapList, xtw);
            }

            xtw.WriteEndElement();
        }

        private void btnSrcCfgCaseExp_Click(object sender, EventArgs e)
        {
            UpdateCurrentConfig();
            string exportFolder = GetDestinationFolder("Select destination folder.");
            if (exportFolder.Length > 0)
            {
                string exportFile = FormulateExportFileName(exportFolder, "MM_CaseSource", currentConfigName);
                if (exportFile.Length > 0)
                {
                    XmlTextWriter xtw = StartExport(exportFile);
                    if (xtw != null)
                    {
                        ExportSQLResult("mmCaseSource", "mmSourceColumn",
                            "SELECT ColumnID, Name, DataType, MaxLength, Format, MultiValued FROM CfgCaseSource WHERE ConfigID = " + currentConfigID, xtw);

                        EndExport(xtw, exportFile);
                    }
                }
            }
        }

        private void btnSrcCfgDocExp_Click(object sender, EventArgs e)
        {
            UpdateCurrentConfig();
            string exportFolder = GetDestinationFolder("Select destination folder.");
            if (exportFolder.Length > 0)
            {
                string exportFile = FormulateExportFileName(exportFolder, "MM_DocSource", currentConfigName);
                if (exportFile.Length > 0)
                {
                    XmlTextWriter xtw = StartExport(exportFile);
                    if (xtw != null)
                    {
                        ExportSQLResult("mmDocumentSource", "mmSourceColumn",
                            "SELECT ColumnID, Name, DataType, MaxLength, Format, MultiValued FROM CfgDocumentSource WHERE ConfigID = " + currentConfigID, xtw);

                        EndExport(xtw, exportFile);
                    }
                }
            }
        }

        private void btnSrcCfgAnnExp_Click(object sender, EventArgs e)
        {
            UpdateCurrentConfig();
            string exportFolder = GetDestinationFolder("Select destination folder.");
            if (exportFolder.Length > 0)
            {
                string exportFile = FormulateExportFileName(exportFolder, "MM_AnnSource", currentConfigName);
                if (exportFile.Length > 0)
                {
                    XmlTextWriter xtw = StartExport(exportFile);
                    if (xtw != null)
                    {
                        ExportSQLResult("mmAnnotationSource", "mmSourceColumn",
                            "SELECT ColumnID, Name, DataType, MaxLength, Format, MultiValued FROM CfgAnnotationSource WHERE ConfigID = " + currentConfigID, xtw);

                        EndExport(xtw, exportFile);
                    }
                }
            }
        }

        private void btnSrcCfgTaskExp_Click(object sender, EventArgs e)
        {
            UpdateCurrentConfig();
            string exportFolder = GetDestinationFolder("Select destination folder.");
            if (exportFolder.Length > 0)
            {
                string exportFile = FormulateExportFileName(exportFolder, "MM_TaskSource", currentConfigName);
                if (exportFile.Length > 0)
                {
                    XmlTextWriter xtw = StartExport(exportFile);
                    if (xtw != null)
                    {
                        ExportSQLResult("mmTaskSource", "mmSourceColumn",
                            "SELECT ColumnID, Name, DataType, MaxLength, Format, MultiValued FROM CfgTaskSource WHERE ConfigID = " + currentConfigID, xtw);

                        EndExport(xtw, exportFile);
                    }
                }
            }
        }

        private void ExportTargetCaseType(string configID, string caseTypeID, Dictionary<string, string> mapList, XmlTextWriter xtw)
        {
            xtw.WriteStartElement("mmCaseType");

            string sql = "SELECT CaseName, DefaultDocClass FROM CfgCaseTypes WHERE ID = " + caseTypeID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            XmlAttributeCollection xac = xml.SelectNodes("//row")[0].Attributes;

            foreach (XmlAttribute xa in xac)
            {
                xtw.WriteAttributeString(xa.Name, xa.Value);
            }

            string condition = "CaseTypeID = " + caseTypeID + " AND ConfigID = " + configID;

            ExportSQLResult("mmProperties", "mmProperty",
                    "SELECT Name, Source, DataType, Required, Mapped, Computed, MappingName FROM CfgCaseProperties WHERE " + condition, xtw); // case properties
            GatherMappings("CfgCaseProperties", condition, mapList);
            ExportSQLResult("mmFolders", "mmFolder", "SELECT FolderName, Parent FROM CfgCaseFolders WHERE " + condition, xtw); // case folders

            ExportTargetTaskTypes(configID, caseTypeID, mapList, xtw);

            xtw.WriteEndElement();
        }

        private void btnTgtCfgCaseExp_Click(object sender, EventArgs e)
        {
            string caseTypeID = lvTargetCaseTypes.Items[lvIndexTargetCaseTypes].Tag.ToString();
            string caseTypeName = lvTargetCaseTypes.Items[lvIndexTargetCaseTypes].SubItems[0].Text;

            string exportFolder = GetDestinationFolder("Select destination folder.");
            if (exportFolder.Length > 0)
            {
                string exportFile = FormulateExportFileName(exportFolder, "MM_TargetCase", caseTypeName);
                if (exportFile.Length > 0)
                {
                    Dictionary<string, string> mapList = new Dictionary<string, string>();

                    XmlTextWriter xtw = StartExport(exportFile);
                    if (xtw != null)
                    {
                        ExportTargetCaseType(currentConfigID, caseTypeID, mapList, xtw);
                        ExportMappings(mapList, xtw);
                        EndExport(xtw, exportFile);
                    }
                }
            }
        }

        private void ExportTargetDocumentType(string configID, string docClassID, Dictionary<string, string> mapList, XmlTextWriter xtw)
        {
            xtw.WriteStartElement("mmDocumentType");

            string sql = "SELECT DocClassName FROM CfgDocClasses WHERE ID = " + docClassID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            XmlAttributeCollection xac = xml.SelectNodes("//row")[0].Attributes;

            foreach (XmlAttribute xa in xac)
            {
                xtw.WriteAttributeString(xa.Name, xa.Value);
            }

            string condition = "DocClassID = " + docClassID + " AND ConfigID = " + configID;

            ExportSQLResult("mmProperties", "mmProperty",
                   "SELECT Name, Source, DataType, Required, Mapped, Computed, MappingName FROM CfgDocClassProperties WHERE " + condition, xtw); // document type properties
            GatherMappings("CfgDocClassProperties", condition, mapList);

            xtw.WriteEndElement();
        }

        private void btnTgtCfgDocExp_Click(object sender, EventArgs e)
        {
            string docClassID = lvTargetDocClasses.Items[lvIndexTargetDocClasses].Tag.ToString();
            string docClassName = lvTargetDocClasses.Items[lvIndexTargetDocClasses].SubItems[0].Text;

            string exportFolder = GetDestinationFolder("Select destination folder.");
            if (exportFolder.Length > 0)
            {
                string exportFile = FormulateExportFileName(exportFolder, "MM_TargetDocType", docClassName);
                if (exportFile.Length > 0)
                {
                    Dictionary<string, string> mapList = new Dictionary<string, string>();

                    XmlTextWriter xtw = StartExport(exportFile);
                    if (xtw != null)
                    {
                        ExportTargetDocumentType(currentConfigID, docClassID, mapList, xtw);
                        ExportMappings(mapList, xtw);
                        EndExport(xtw, exportFile);
                    }
                }
            }
        }

        private void ExportTargetAnnotationType(string configID, string annClassID, Dictionary<string, string> mapList, XmlTextWriter xtw)
        {
            xtw.WriteStartElement("mmAnnotationType");

            string sql = "SELECT AnnClassName FROM CfgAnnClasses WHERE ID = " + annClassID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            XmlAttributeCollection xac = xml.SelectNodes("//row")[0].Attributes;

            foreach (XmlAttribute xa in xac)
            {
                xtw.WriteAttributeString(xa.Name, xa.Value);
            }

            string condition = "AnnClassID = " + annClassID + " AND ConfigID = " + configID;

            ExportSQLResult("mmProperties", "mmProperty",
                   "SELECT Name, Source, DataType, Required, Mapped, Computed, MappingName FROM CfgAnnClassProperties WHERE " + condition, xtw); // annotation type properties
            GatherMappings("CfgAnnClassProperties", condition, mapList);

            xtw.WriteEndElement();
        }

        private void btnTgtCfgAnnExp_Click(object sender, EventArgs e)
        {
            string AnnClassID = lvTargetAnnClasses.Items[lvIndexTargetAnnClasses].Tag.ToString();
            string AnnClassName = lvTargetAnnClasses.Items[lvIndexTargetAnnClasses].SubItems[0].Text;

            string exportFolder = GetDestinationFolder("Select destination folder.");
            if (exportFolder.Length > 0)
            {
                string exportFile = FormulateExportFileName(exportFolder, "MM_TargetAnnType", AnnClassName);
                if (exportFile.Length > 0)
                {
                    Dictionary<string, string> mapList = new Dictionary<string, string>();

                    XmlTextWriter xtw = StartExport(exportFile);
                    if (xtw != null)
                    {
                        ExportTargetAnnotationType(currentConfigID, AnnClassID, mapList, xtw);
                        ExportMappings(mapList, xtw);
                        EndExport(xtw, exportFile);
                    }
                }
            }
        }

        private void ExportTargetTaskType(string configID, string taskTypeID, Dictionary<string, string> mapList, XmlTextWriter xtw)
        {
            xtw.WriteStartElement("mmTaskType");

            string sql = "SELECT tt.TaskName, tt.IsDocInitiated, tt.InitiatingDocClass, tt.InitiatingDocIDSource, tt.AttachingProperty, ct.CaseName FROM CfgTaskTypes tt, CfgCaseTypes ct WHERE tt.ID = " + 
                                                taskTypeID + " AND tt.CaseID = ct.ID";
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            XmlAttributeCollection xac = xml.SelectNodes("//row")[0].Attributes;

            foreach (XmlAttribute xa in xac)
            {
                xtw.WriteAttributeString(xa.Name, xa.Value);
            }

            string condition = "TaskTypeID = " + taskTypeID + " AND ConfigID = " + configID;

            ExportSQLResult("mmProperties", "mmProperty",
                    "SELECT Name, Source, DataType, Required, Mapped, Computed, MappingName FROM CfgTaskProperties WHERE " + condition, xtw); // task properties
            GatherMappings("CfgTaskProperties", condition, mapList);

            xtw.WriteEndElement();
        }

        private void btnTgtCfgTaskExp_Click(object sender, EventArgs e)
        {
            string taskTypeID = lvTargetTaskTypes.Items[lvIndexTargetTaskTypes].Tag.ToString();
            string taskName = lvTargetTaskTypes.Items[lvIndexTargetTaskTypes].SubItems[0].Text;

            string exportFolder = GetDestinationFolder("Select destination folder.");
            if (exportFolder.Length > 0)
            {
                string exportFile = FormulateExportFileName(exportFolder, "MM_TargetTaskType", taskName);
                if (exportFile.Length > 0)
                {
                    Dictionary<string, string> mapList = new Dictionary<string, string>();

                    XmlTextWriter xtw = StartExport(exportFile);
                    if (xtw != null)
                    {
                        ExportTargetTaskType(currentConfigID, taskTypeID, mapList, xtw);
                        ExportMappings(mapList, xtw);
                        EndExport(xtw, exportFile);
                    }
                }
            }
        }

        private void btnExpIdentifier_Click(object sender, EventArgs e)
        {
            UpdateCurrentConfig();
            string exportFolder = GetDestinationFolder("Select destination folder.");
            if (exportFolder.Length > 0)
            {
                string exportFile = FormulateExportFileName(exportFolder, "MM_ConfigIdentifiers", currentConfigName);
                if (exportFile.Length > 0)
                {
                    XmlTextWriter xtw = StartExport(exportFile);
                    if (xtw != null)
                    {
                        ExportSQLResult("mmIdentifiers", "mmIdentifier",
                            "SELECT IdentifierName, FieldName FROM CfgIdentifiers WHERE ConfigID = " + currentConfigID, xtw);
                        EndExport(xtw, exportFile);
                    }
                }
            }
        }

        private void GatherMappings(string propertyTable, string condition, Dictionary<string, string> listMapDefnID)
        {
            string sql = "SELECT Mapped, MappingName FROM " + propertyTable + " WHERE " + condition;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            foreach (XmlElement row in xml.SelectNodes("//row"))
            {
                string isMapped = row.Attributes["Mapped"].Value;
                if (isMapped == "1")
                {
                    string mapName = row.Attributes["MappingName"].Value.Trim();
                    if (!listMapDefnID.ContainsValue(mapName))
                    {
                        sql = "SELECT ID FROM MgrMappings WHERE MappingName = '" + mapName + "'";
                        XmlDocument xmlMap = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
                        string mapID = xmlMap.SelectNodes("//row")[0].Attributes["ID"].Value;
                        listMapDefnID.Add(mapID, mapName);
                    }
                }
            }
        }
    }
}
