﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MultiValueFieldPickDialog : Form
    {
        string dbCnxn;
        string baseTableName;
        string configTable;
        string selectedField = string.Empty;

        public MultiValueFieldPickDialog(string dbConnection, string tableName, string cfgTable)
        {
            InitializeComponent();

            dbCnxn = dbConnection;
            baseTableName = tableName;
            configTable = cfgTable;

            PopulateMultiValueFieldsListBox();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            string fieldTableName = baseTableName + "_" + selectedField;

            // The ConfigID is immaterial here
            using (LoadDataDialog dlg = new LoadDataDialog(dbCnxn, fieldTableName, configTable, "0", true))
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PopulateMultiValueFieldsListBox()
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT Name FROM " + configTable + " c WHERE c.MultiValued = 'Y'");
            int count = xml.SelectNodes("//row").Count;
            if (count > 0)
            {
                cmbFields.BeginUpdate();
                cmbFields.Items.Clear();
                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    cmbFields.Items.Add(rowNode.GetAttribute("Name"));
                }
                cmbFields.EndUpdate();
                cmbFields.SelectedIndex = 0;
                selectedField = cmbFields.SelectedItem.ToString();
            }
        }

        private void cmbFields_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedField = cmbFields.SelectedItem.ToString();
        }
    }
}
