﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class ListMapDialog : Form
    {
        string dbCnxn = string.Empty;
        DialogOperation operation;
        string defnID = string.Empty;
        string ID = string.Empty;
        int lvIndex = 0;

        public ListMapDialog(string dbConnection, string cdID, string cdName, DialogOperation op)
        {
            dbCnxn = dbConnection;
            defnID = cdID;
            operation = op;

            InitializeComponent();

            lblConvMap.Text = "Conversion Definition: " + cdName;

            PopulateListView();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (ConvMapDialog dlg = new ConvMapDialog(dbCnxn, defnID, DialogOperation.Add))
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    PopulateListView();
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT cm.Restricted FROM MgrConvMaps cm, MgrConversions c WHERE cm.ID = c.ConvMapID AND c.ID = " + lvList.Items[lvIndex].Tag.ToString());
            string restricted = xml.SelectNodes("//row")[0].Attributes["Restricted"].Value;
            if (restricted == "1")
            {
                MessageBox.Show("This map entry is restricted and hence cannot be edited", "Migration Manager", MessageBoxButtons.OK);
            }
            else
            {
                using (ConvMapDialog dlg = new ConvMapDialog(dbCnxn, defnID, DialogOperation.Update))
                {
                    ListViewItem lvi = lvList.Items[lvIndex];
                    dlg.Populate(lvi.Tag.ToString(), lvi.SubItems[0].Text, lvi.SubItems[1].Text, lvi.SubItems[2].Text);
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        PopulateListView();
                    }
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = lvList.Items[lvIndex];
            if (lvi.SubItems[0].Text.ToUpper() == "ALL")
            {
                MessageBox.Show("A Conversion Definition must have at least one catch-all conversion map. Hence this map entry cannot be deleted. Edit it if it is not to your liking.", "Migration Manager", MessageBoxButtons.OK);
            }
            else
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT cm.Restricted FROM MgrConvMaps cm, MgrConversions c WHERE cm.ID = c.ConvMapID AND c.ID = " + lvList.Items[lvIndex].Tag.ToString());
                string restricted = xml.SelectNodes("//row")[0].Attributes["Restricted"].Value;
                if (restricted == "1")
                {
                    MessageBox.Show("This map entry is restricted and hence cannot be deleted", "Migration Manager", MessageBoxButtons.OK);
                }
                else
                {
                    int nAffected = DBAdapter.DBAdapter.DeleteDB(dbCnxn, "DELETE MgrConversions WHERE ID = " + lvList.Items[lvIndex].Tag.ToString());
                    if (lvIndex > 0) lvIndex--;
                    PopulateListView();
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PopulateListView()
        {
            string sql = "SELECT c.ID As ID, c.ConvMapID, cm.SourceFormat As Source, cm.TargetFormat As Target, cm.Exempt As Exempt FROM MgrConversions c, MgrConvMaps cm WHERE c.ConvDefnID = " + defnID + " AND c.ConvMapID = cm.ID ORDER BY cm.SourceFormat";

            lvList.BeginUpdate();
            lvList.Items.Clear();

            ListViewItem lvi = null;
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);

                int i = 0;
                XmlElement holdNode = null;
                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    if (rowNode.GetAttribute("Source") == "ALL")
                    {
                        holdNode = rowNode;
                    }
                    else
                    {
                        lvi = lvList.Items.Add(rowNode.GetAttribute("Source"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("Target"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Exempt"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                }
                if (holdNode != null)
                {
                    lvi = lvList.Items.Add(holdNode.GetAttribute("Source"), i);
                    lvi.SubItems.Add(holdNode.GetAttribute("Target"));
                    lvi.SubItems.Add(holdNode.GetAttribute("Exempt"));
                    lvi.Tag = holdNode.GetAttribute("ID");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception populating list view: " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                lvList.EndUpdate();
                if (lvList.Items.Count > 0)
                {
                    lvList.Items[lvIndex].Checked = true;
                    lvList.Items[lvIndex].EnsureVisible();
                }
            }
        }

        private void lvList_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndex))
            {
                lvList.Items[lvIndex].Checked = false;
                lvIndex = e.Item.Index;
            }
        }
    }
}
