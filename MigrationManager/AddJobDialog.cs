﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class AddJobDialog : Form
    {
        string dbCnxn;
        DialogOperation operation;
        string ID = string.Empty;
        Dictionary<string, string> Configs = new Dictionary<string, string>();
        Dictionary<string, string> Conversions = new Dictionary<string, string>();

        string currentConfig = string.Empty;
        string currentConv = string.Empty;
        string jobName = string.Empty;

        public AddJobDialog(string dbConnection, DialogOperation op)
        {
            dbCnxn = dbConnection;
            operation = op;

            InitializeComponent();

            lblHeader.Text = (op == DialogOperation.Add) ? "Add New Job Definition" : "Update Job Definiton";
            PopulateConfigListBox();
            PopulateConvListBox();
        }

        public void Populate(string id, string name, string cfg, string conv, string cmnt)
        {
            ID = id;

            txtJobName.Text = name;
            txtComment.Text = cmnt;

            for (int i = 0; i < cmbConfig.Items.Count; i++)
            {
                if (cmbConfig.Items[i].ToString() == cfg)
                {
                    cmbConfig.SelectedItem = cmbConfig.Items[i];
                    break;
                }
            }

            for (int i = 0; i < cmbConv.Items.Count; i++)
            {
                if (cmbConv.Items[i].ToString() == conv)
                {
                    cmbConv.SelectedItem = cmbConv.Items[i];
                    break;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddJob_Click(object sender, EventArgs e)
        {
            if (HasPassedValidation())
            {
                if (IsGoodJobName())
                {
                    if (operation == DialogOperation.Add)
                    {
                        string sql = "INSERT INTO MgrJobs(ConfigID, JobName, ConvDefnID, Comment) VALUES (" + Configs[currentConfig] + ", '" + jobName + "', " + Conversions[currentConv] + ", '" + txtComment.Text.Trim() + "')";
                        int nAffected = DBAdapter.DBAdapter.AddToDB(dbCnxn, sql);
                    }
                    else
                    {
                        string sql = "UPDATE MgrJobs SET JobName = '" + jobName + "', ConfigID = " + Configs[currentConfig] + ", ConvDefnID = " + Conversions[currentConv] + ", Comment = '" + txtComment.Text + "' WHERE ID = " + ID;
                        int nAffected = DBAdapter.DBAdapter.UpdateDB(dbCnxn, sql);
                    }
                    DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    DialogResult = DialogResult.None;
                    return;
                }
            }
            else
            {
                DialogResult = DialogResult.None;
                return;
            }
        }

        private void PopulateConfigListBox()
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT * FROM MgrConfigurations ORDER BY ConfigName");
            
            cmbConfig.BeginUpdate();
            cmbConfig.Items.Clear();
            foreach (XmlElement rowNode in xml.SelectNodes("//row"))
            {
                Configs.Add(rowNode.GetAttribute("ConfigName"), rowNode.GetAttribute("ID"));
                cmbConfig.Items.Add(rowNode.GetAttribute("ConfigName"));
            }
            cmbConfig.EndUpdate();
            if (cmbConfig.Items.Count > 0)
            {
                cmbConfig.SelectedIndex = 0;
                currentConfig = cmbConfig.SelectedItem.ToString();
            }
        }

        private void PopulateConvListBox()
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT * FROM MgrConvDefns ORDER BY ConvDefnName");
            int count = xml.SelectNodes("//row").Count;
            
            cmbConv.BeginUpdate();
            cmbConv.Items.Clear();
            foreach (XmlElement rowNode in xml.SelectNodes("//row"))
            {
                Conversions.Add(rowNode.GetAttribute("ConvDefnName"), rowNode.GetAttribute("ID"));
                cmbConv.Items.Add(rowNode.GetAttribute("ConvDefnName"));
            }
            cmbConv.EndUpdate();
            if (cmbConv.Items.Count > 0)
            {
                cmbConv.SelectedIndex = 0;
                currentConv = cmbConv.SelectedItem.ToString();
            }
        }

        private Boolean HasPassedValidation()
        {
            if (txtJobName.Text.Length == 0)
            {
                MessageBox.Show("Please specify a Job Name", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (txtJobName.Text.Length >= 64)
            {
                MessageBox.Show("Job Name is too long. Job Names can be at most 64 characters", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (txtComment.Text.Length >= 64)
            {
                MessageBox.Show("Comment is too long. Comments can be at most 64 characters", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (currentConfig.Length == 0)
            {
                MessageBox.Show("Please choose a configuration. Define one if none exist.", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (currentConv.Length == 0)
            {
                MessageBox.Show("Please choose a conversion. Define one if none exist.", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            return true;
        }

        private Boolean IsGoodJobName()
        {
            string oldJob = txtJobName.Text.Trim();
            string pattern = "\\s+";
            string replacement = "_";
            Regex rgx = new Regex(pattern);
            jobName = rgx.Replace(oldJob, replacement);

            if (jobName != oldJob)
            {
                if (!(MessageBox.Show("Job Name has white space. The following Job Name will be used instead: " + jobName + ". Proceed?", "Confirm Job Name", MessageBoxButtons.YesNo) == DialogResult.Yes))
                {
                    return false;
                }
            }
            pattern = "-";
            rgx = new Regex(pattern);
            string temp = rgx.Replace(jobName, replacement);
            if (temp != jobName)
            {
                if (!(MessageBox.Show("Job Name has hyphens. The following Job Name will be used instead: " + temp + ". Proceed?", "Confirm Job Name", MessageBoxButtons.YesNo) == DialogResult.Yes))
                {
                    return false;
                }
                jobName = temp;
            }
            return IsUniqueJobName();
        }

        private Boolean IsUniqueJobName()
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT * FROM MgrJobs WHERE JobName = '" + jobName + "'");
            if (xml.SelectNodes("//row").Count > 0)
            {
                if (operation == DialogOperation.Add)
                {
                    MessageBox.Show("Job " + jobName + " already exists. Please choose another name.", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
                else
                {
                    string temp = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                    if (temp != ID)
                    {
                        MessageBox.Show("Job " + jobName + " already exists. Please choose another name.", "Migration Manager", MessageBoxButtons.OK);
                        return false;
                    }
                }
            }
            return true;
        }

        private void cmbConfig_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentConfig = cmbConfig.SelectedItem.ToString();
        }

        private void cmbConv_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentConv = cmbConv.SelectedItem.ToString();
        }
    }
}
