﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class ValueMapDialog : Form
    {
        string dbCnxn = string.Empty;
        DialogOperation operation;
        string defnID = string.Empty;
        string ID = string.Empty;

        public ValueMapDialog(string dbConnection, string defID, DialogOperation op)
        {
            dbCnxn = dbConnection;
            defnID = defID;
            operation = op;

            InitializeComponent();

            lblHeader.Text = (op == DialogOperation.Add) ? "Add New Value Map" : "Update Value Map";
        }

        public void Populate(string id, string src, string tgt)
        {
            ID = id;

            txtSource.Text = src;
            txtTarget.Text = tgt;
        }

        private Boolean HasPassedValidation()
        {
            if (txtSource.Text.Length == 0)
            {
                if (MessageBox.Show("You have not specified a source value. Assume NULL?", "Confirm NULL source", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    txtSource.Text = "NULL";
                }
                else
                {
                    MessageBox.Show("Please specify a Source value", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
            }
            if (txtSource.Text.Length >= 64)
            {
                MessageBox.Show("Source value is too long. Vales can be at most 64 characters", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (txtTarget.Text.Length == 0)
            {
                if (MessageBox.Show("You have not specified a target value. Assume NULL?", "Confirm NULL target", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    txtTarget.Text = "NULL";
                }
                else
                {
                    MessageBox.Show("Please specify a Target value", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
            }
            if (txtSource.Text.Length >= 64)
            {
                MessageBox.Show("Target value is too long. Values can be at most 64 characters", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            // Check for ambiguous definitons
            string src = txtSource.Text.Trim();
            string tgt = txtTarget.Text.Trim();
            
            string sql = "SELECT ID FROM MgrMappingValues WHERE MapID = " + defnID + " AND OldValue = '" + src + "'";
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);
            Boolean result = true;
            foreach (XmlElement rowNode in xml.SelectNodes("//row"))
            {
                string id = rowNode.GetAttribute("ID");
                if (operation == DialogOperation.Add)
                {
                    MessageBox.Show("Value " + src + " is already mapped.", "Migration Manager", MessageBoxButtons.OK);
                    result = false;
                    break;
                }
                else
                {
                    if (id != ID)
                    {
                        MessageBox.Show("Value " + src + " is already mapped.", "Migration Manager", MessageBoxButtons.OK);
                        result = false;
                        break;
                    }
                }
            }
            return result; ;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (HasPassedValidation())
            {
                string src = txtSource.Text.Trim();
                string tgt = txtTarget.Text.Trim();

                if (operation == DialogOperation.Add)
                {
                    string sql = "INSERT INTO MgrMappingValues(MapID, OldValue, NewValue) VALUES (" + defnID + ", '" + src + "', '" + tgt + "')";
                    int nAffected = DBAdapter.DBAdapter.AddToDB(dbCnxn, sql);
                }
                else
                {
                    string sql = "UPDATE MgrMappingValues SET MapID = " + defnID + ", OldValue = '" + src + "', NewValue = '" + tgt + "' WHERE ID = " + ID;
                    int nAffected = DBAdapter.DBAdapter.UpdateDB(dbCnxn, sql);
                }
                DialogResult = DialogResult.OK;
                this.Close();
            }  
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
