﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        int convSessionIndex = 0;
        Boolean sessionSelected = false;

        int convBatchIndex = 0;
        Boolean batchSelected = false;

        string convLogLBound = "1";
        string convLogUBound = string.Empty;
        string convLogTotal = string.Empty;
        string convLogLowerID = string.Empty;
        string convLogUpperID = string.Empty;

        private void ViewConversionLogs()
        {
            tabConvLog.Show();
            tabConvLog.BringToFront();
            ShowViewConvLogSubTab();
        }

        private void HideConversionLogs()
        {
            tabConvLog.Hide();
        }

        private void tabConvLog_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowViewConvLogSubTab();
        }

        private void ShowViewConvLogSubTab()
        {
            switch (tabConvLog.SelectedIndex)
            {
                case 0:
                    RefreshConvSessionList();
                    convBatchIndex = 0;
                    break;
                case 1:
                    if (!sessionSelected)
                    {
                        MessageBox.Show("Please choose a session first", "Migration Manager", MessageBoxButtons.OK);
                    }
                    else
                    {
                        RefreshConvBatchesList();
                    }
                    break;
                case 2:
                    if (!batchSelected)
                    {
                        MessageBox.Show("Please choose a batch first", "Migration Manager", MessageBoxButtons.OK);
                    }
                    else
                    {
                        RefreshConvBatchLog();
                    }
                    break;
                default:
                    if (!batchSelected)
                    {
                        MessageBox.Show("Please choose a batch first", "Migration Manager", MessageBoxButtons.OK);
                    }
                    else
                    {
                        RefreshConvBatchErrors();
                    }
                    break;
            }
        }

        private void RefreshConvSessionList()
        {
            listConvSessions.BeginUpdate();
            listConvSessions.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID, CAST(StartTime AS nvarchar(30)) AS StartStamp, CAST(EndTime AS nvarchar(30)) AS EndStamp, UserName, WorkStation, Status FROM MgrSessions WHERE SessionType = 'CONVERSION' ORDER BY ID DESC");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    ListViewItem[] items = new ListViewItem[count];

                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = listConvSessions.Items.Add(rowNode.GetAttribute("ID"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("StartStamp"));
                        lvi.SubItems.Add(rowNode.GetAttribute("EndStamp"));
                        lvi.SubItems.Add(rowNode.GetAttribute("UserName"));
                        lvi.SubItems.Add(rowNode.GetAttribute("WorkStation"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Status"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                    listConvSessions.Items[convSessionIndex].Checked = true;
                    listConvSessions.EnsureVisible(convSessionIndex);
                    sessionSelected = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Sessions : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                listConvSessions.EndUpdate();
            }
        }

        private void listConvSessions_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if((e.Item.Checked) && (e.Item.Index != convSessionIndex))
            {
                listConvSessions.Items[convSessionIndex].Checked = false;
                convSessionIndex = e.Item.Index;
                convBatchIndex = 0;
                convLogLBound = "1";
                convLogUBound = string.Empty;
            }
        }

        private void RefreshConvBatchesList()
        {
            listConvBatches.BeginUpdate();
            listConvBatches.Items.Clear();
            try
            {
                string sessionID = listConvSessions.Items[convSessionIndex].Tag.ToString();

                TabPage p = tabConvLog.TabPages[tabConvLog.SelectedIndex];
                Label label = (Label)p.Controls["lblConvBatches"];
                label.Text = "Conversion Batches For Session: " + sessionID;

                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID, ActualSize, CAST(StartTime AS nvarchar(30)) AS StartStamp, CAST(EndTime AS nvarchar(30)) AS EndStamp, UserName, WorkStation, Status FROM MgrBatches WHERE SessionID = " + sessionID + " ORDER BY ID DESC");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = listConvBatches.Items.Add(rowNode.GetAttribute("ID"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("ActualSize"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Status"));
                        lvi.SubItems.Add(rowNode.GetAttribute("StartStamp"));
                        lvi.SubItems.Add(rowNode.GetAttribute("EndStamp"));
                        lvi.SubItems.Add(rowNode.GetAttribute("UserName"));
                        lvi.SubItems.Add(rowNode.GetAttribute("WorkStation"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                    listConvBatches.Items[convBatchIndex].Checked = true;
                    listConvBatches.EnsureVisible(i - 1);
                    batchSelected = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Batches : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                listConvBatches.EndUpdate();
            }
        }

        private void listConvBatches_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != convBatchIndex))
            {
                listConvBatches.Items[convBatchIndex].Checked = false;
                convBatchIndex = e.Item.Index;
                convLogLBound = "1";
                convLogUBound = string.Empty;
            }
        }

        private void btnPrevConvLog_Click(object sender, EventArgs e)
        {
            convLogUBound = convLogLBound;
            convLogLBound = string.Empty;
            RefreshConvBatchLog();
        }

        private void btnNextConvLog_Click(object sender, EventArgs e)
        {
            convLogLBound = convLogUBound;
            convLogUBound = string.Empty;
            RefreshConvBatchLog();
        }

        private void RefreshConvBatchLog()
        {
            string batchID = listConvBatches.Items[convBatchIndex].Tag.ToString();

            UpdateLogBoundaries(GetJobName() + "_PreIngestLog", batchID);

            Int32 pageSize = (listConvBatchLog.Size.Height / ROW_SIZE);

            listConvBatchLog.BeginUpdate();
            listConvBatchLog.Items.Clear();
            try
            {
                lblConvBatchLog.Text = "Conversion Batch " + batchID.ToString() + " Log:";
                lblConvLogCount.Text = "Total Records: " + convLogTotal.ToString();

                string sql = (convLogLBound == string.Empty) ?
                    "SELECT * FROM (SELECT TOP " + pageSize.ToString() + " ID, Type, CAST(TimeStamp AS nvarchar(30)) AS TStamp, Description FROM " + GetJobName() + "_PreIngestLog WHERE BatchID = " + batchID + " AND ID <= " + convLogUBound + " ORDER BY ID DESC) AS P ORDER BY ID" :
                    "SELECT TOP " + pageSize.ToString() + " ID, Type, CAST(TimeStamp AS nvarchar(30)) AS TStamp, Description FROM " + GetJobName() + "_PreIngestLog WHERE BatchID = " + batchID + " AND ID >= " + convLogLBound + " ORDER BY ID";

                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
                
                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        if (i == 0) convLogLBound = rowNode.GetAttribute("ID");
                        convLogUBound = rowNode.GetAttribute("ID");

                        ListViewItem lvi = listConvBatchLog.Items.Add(rowNode.GetAttribute("TStamp"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("Type"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Description"));
                        i++;
                    }
                    listConvBatchLog.EnsureVisible(i - 1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing log : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                listConvBatchLog.EndUpdate();
                btnPrevConvLog.Enabled = (convLogLBound == convLogLowerID) ? false : true;
                btnNextConvLog.Enabled = (convLogUBound == convLogUpperID) ? false : true; 
            }
        }

        private void UpdateLogBoundaries(string tableName, string batchID)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT COUNT(*) AS TOTALCOUNT FROM " + tableName + " WHERE BatchID = " + batchID);
            convLogTotal = xml.SelectNodes("//row")[0].Attributes["TOTALCOUNT"].Value;

            if (Convert.ToInt64(convLogTotal) > 0)
            {
                xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT MIN(ID) AS LOWER FROM " + tableName + " WHERE BatchID = " + batchID);
                convLogLowerID = xml.SelectNodes("//row")[0].Attributes["LOWER"].Value;

                xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT MAX(ID) AS UPPER FROM " + tableName + " WHERE BatchID = " + batchID);
                convLogUpperID = xml.SelectNodes("//row")[0].Attributes["UPPER"].Value;
            }
        }

        private void RefreshConvBatchErrors()
        {
            listConvBatchErrors.BeginUpdate();
            listConvBatchErrors.Items.Clear();
            try
            {
                int batchID = Convert.ToInt32(listConvBatches.Items[convBatchIndex].Tag.ToString());
                lblConvBatchErrors.Text = "Batch " + batchID.ToString() + " Errors:";

                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID, Type, CAST(TimeStamp AS nvarchar(30)) AS TStamp, Description FROM " + GetJobName() + "_PreIngestLog WHERE BatchID = " + batchID + " AND Type = 'ERROR' ORDER BY ID");
                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = listConvBatchErrors.Items.Add(rowNode.GetAttribute("TStamp"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("Description"));
                        i++;
                    }
                    listConvBatchErrors.EnsureVisible(i - 1);
                }
                else
                {
                    lblConvBatchErrors.Text = "Batch " + batchID.ToString() + ": No Errors";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing errors : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                listConvBatchErrors.EndUpdate();
            }
        }

        protected string GetJobName()
        {
            string jobID = listJobs.Items[currentPrepareJobIndex].Tag.ToString();
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT MgrJobs.JobName AS JobName FROM MgrJobs WHERE ID = " + jobID);

            return xmlDoc.SelectNodes("//row")[0].Attributes["JobName"].Value;
        }
    }
}
