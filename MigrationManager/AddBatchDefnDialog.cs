﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MigrationManager
{
    public partial class AddBatchDefnDialog : Form
    {
        string dbCnxn;
        DialogOperation operation;
        string ID = string.Empty;

        public AddBatchDefnDialog(string dbConnection, DialogOperation op)
        {
            dbCnxn = dbConnection;
            operation = op;

            InitializeComponent();
            lblAddBatchDefn.Text = (op == DialogOperation.Add) ? "Add New Batch Definition" : "Update Batch Definiton";
        }

        public void Populate(string id, string name, string size, string condition, string srcPrefix, string stage)
        {
            ID = id;

            txtBatchDefnName.Text = name;
            txtSize.Text = size;
            txtCondition.Text = condition;
            txtSourcePrefix.Text = srcPrefix;
            txtStagingPath.Text = stage;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddBatchDefn_Click(object sender, EventArgs e)
        {
            if (HasPassedValidation())
            {
                string batchDefnName = txtBatchDefnName.Text.Trim();
                string size = txtSize.Text.Trim();
                string condition = MigrationManager.EscapeSingleQuote(txtCondition.Text.Trim());
                string srcPrefix = txtSourcePrefix.Text.Trim();
                string staging = txtStagingPath.Text.Trim();

                if (operation == DialogOperation.Add)
                {
                    string sql = "INSERT INTO MgrBatchDefns(BatchDefnName, BatchSize, BatchCondition, SourceContentPathPrefix, StagingPath) VALUES ('" + batchDefnName + "', " + size + ", '" + condition + "', '" + srcPrefix + "', '" + staging + "')";
                    int nAffected = DBAdapter.DBAdapter.AddToDB(dbCnxn, sql);
                }
                else
                {
                    string sql = "UPDATE MgrBatchDefns SET BatchDefnName = '" + batchDefnName + "', BatchSize = " + size + ", BatchCondition = '" + condition + "', SourceContentPathPrefix = '" + srcPrefix + "', StagingPath = '" + staging + "' WHERE ID = " + ID;
                    int nAffected = DBAdapter.DBAdapter.UpdateDB(dbCnxn, sql);
                }
                DialogResult = DialogResult.OK;
                this.Close();
            }  
        }

        private Boolean HasPassedValidation()
        {
            if (txtBatchDefnName.Text.Trim().Length == 0)
            {
                MessageBox.Show("Please specify a Batch Definition Name", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (txtBatchDefnName.Text.Trim().Length >= 64)
            {
                MessageBox.Show("Batch Definition Name is too long. Batch Definition Names can be at most 64 characters", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (txtSize.Text.Trim().Length == 0)
            {
                MessageBox.Show("Please specify a Size", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (txtSize.Text.Trim().Length >= 64)
            {
                MessageBox.Show("Size is too long. Size can be at most 64 characters", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            try
            {
                Int32 nSize = Convert.ToInt32(txtSize.Text.Trim());
                if (nSize <= 0)
                {
                    MessageBox.Show("Size must be greater than zero", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Invalid Size: " + txtSize.Text.Trim() + " (" + ex.Message + ")", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (txtCondition.Text.Trim().Length >= 64)
            {
                MessageBox.Show("Condition is too long. Condition can be at most 64 characters", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (txtSourcePrefix.Text.Trim().Length >= 64)
            {
                MessageBox.Show("Source Prefix is too long. Source Prefix can be at most 64 characters", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (txtStagingPath.Text.Trim().Length >= 64)
            {
                MessageBox.Show("Staging Path is too long. Staging Path can be at most 64 characters", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            //TODO Validate the staging path - its existence, accessibility etc
            //TODO Validate the source prefix if any - existence, accessibility
            return true;
        }
    }
}
