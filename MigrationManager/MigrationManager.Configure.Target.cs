﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        int lvIndexTargetCaseTypes = 0;
        int currentTargetCaseTypeID = 1;

        int lvIndexTargetDocClasses = 0;
        int currentTargetDocClassID = 0;

        int lvIndexTargetAnnClasses = 0;
        int currentTargetAnnClassID = 0;

        int lvIndexTargetTaskTypes = 0;
        int currentTargetTaskTypeID = 0;
        
        private void tabConfigTarget_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowConfigTargetSubTab();
        }

        private void ShowConfigTargetSubTab()
        {
            switch (tabConfigTarget.TabPages[tabConfigTarget.SelectedIndex].Name)
            {
                case "targetCases":
                    RefreshTargetCasePage();
                    break;

                case "targetDocuments":
                    RefreshTargetDocumentPage();
                    break;

                case "targetAnnotations":
                    RefreshTargetAnnotationPage();
                    break;

                case "targetTasks":
                    RefreshTargetTaskPage();
                    break;

                default:
                    throw new Exception("Unknown option");
            }
        }

        private void RefreshTargetCasePage()
        {
            if (configSelected)
            {
                lblTargetCaseConfig.Text = "Current Configuration: " + currentConfigName;

                RefreshTargetCaseTypes();
                RefreshTargetCaseProperties();
                RefreshTargetCaseFolders();
            }
            else
            {
                ClearDisplay(lblTargetCaseConfig, lvTargetCaseTypes);
                ClearDisplay(lblTargetCaseConfig, lvTargetCaseProperties);
                ClearDisplay(lblTargetCaseConfig, lvTargetCaseFolders);
            }
        }

        private void RefreshTargetCaseTypes()
        {
            lvTargetCaseTypes.BeginUpdate();
            lvTargetCaseTypes.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM CfgCaseTypes WHERE ConfigID = " + currentConfigID + " ORDER BY CaseName");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    ListViewItem[] items = new ListViewItem[count];

                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = lvTargetCaseTypes.Items.Add(rowNode.GetAttribute("CaseName"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("DefaultDocClass"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                    if (lvIndexTargetCaseTypes > (count - 1)) lvIndexTargetCaseTypes = count - 1;
                    lvTargetCaseTypes.Items[lvIndexTargetCaseTypes].Checked = true;
                    currentTargetCaseTypeID = Convert.ToInt32((string)(lvTargetCaseTypes.Items[lvIndexTargetCaseTypes].Tag));
                    lvTargetCaseTypes.EnsureVisible(lvIndexTargetCaseTypes);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Case Types : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                lvTargetCaseTypes.EndUpdate();
            }
        }

        private void DisplayPropertyData(ListView lv, string sql, string errorID)
        {
            const int nColumns = 7;
            string[] fields = new string[nColumns];

            fields[0] = "Name";
            fields[1] = "Source";
            fields[2] = "DataType";
            fields[3] = "Required";
            fields[4] = "Mapped";
            fields[5] = "Computed";
            fields[6] = "MappingName";

            MigrationManager.RefreshListView(lv, dbConnection, sql, fields, errorID);
        }

        private void RefreshTargetCaseProperties()
        {
            string errorID = "Target Case Properties";
            string sql = "SELECT Name, Source, DataType, Required, Mapped, Computed, MappingName FROM CfgCaseProperties WHERE CaseTypeID = " + currentTargetCaseTypeID + " AND ConfigID = " + currentConfigID + " ORDER BY Name";

            DisplayPropertyData(lvTargetCaseProperties, sql, errorID);
        }

        private void RefreshTargetCaseFolders()
        {
            const int nColumns = 2;
            string[] fields = new string[nColumns];

            string errorID = "Target Case Folders";
            string sql = "SELECT FolderName, Parent FROM CfgCaseFolders WHERE CaseTypeID = " + currentTargetCaseTypeID + " AND ConfigID = " + currentConfigID + " ORDER BY FolderName";

            fields[0] = "FolderName";
            fields[1] = "Parent";

            MigrationManager.RefreshListView(lvTargetCaseFolders, dbConnection, sql, fields, errorID);
        }

        private void lvTargetCaseTypes_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndexTargetCaseTypes))
            {
                lvTargetCaseTypes.Items[lvIndexTargetCaseTypes].Checked = false;
                lvIndexTargetCaseTypes = e.Item.Index;
                currentTargetCaseTypeID = Convert.ToInt32((string)(lvTargetCaseTypes.Items[lvIndexTargetCaseTypes].Tag));
                RefreshTargetCaseProperties();
                RefreshTargetCaseFolders();
            }
        }

        private void RefreshTargetDocumentPage()
        {
            if (configSelected)
            {
                lblTargetDocConfig.Text = "Current Configuration: " + currentConfigName;
                RefreshTargetDocClasses();
                RefreshTargetDocProperties();
            }
            else
            {
                ClearDisplay(lblTargetDocConfig, lvTargetDocClasses);
                ClearDisplay(lblTargetDocConfig, lvTargetDocProperties);
            }
        }

        private void RefreshTargetDocClasses()
        {
            lvTargetDocClasses.BeginUpdate();
            lvTargetDocClasses.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID, DocClassName FROM CfgDocClasses WHERE ConfigID = " + currentConfigID + " ORDER BY DocClassName");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = lvTargetDocClasses.Items.Add(rowNode.GetAttribute("DocClassName"), i);
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                    if (lvIndexTargetDocClasses > (count - 1)) lvIndexTargetDocClasses = count - 1;
                    lvTargetDocClasses.Items[lvIndexTargetDocClasses].Checked = true;
                    currentTargetDocClassID = Convert.ToInt32((string)(lvTargetDocClasses.Items[lvIndexTargetDocClasses].Tag));
                    lvTargetDocClasses.EnsureVisible(lvIndexTargetDocClasses);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Document Classes : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                lvTargetDocClasses.EndUpdate();
            }
        }

        private void RefreshTargetDocProperties()
        {
            string errorID = "Target Document Class Properties";
            string sql = "SELECT Name, Source, DataType, Required, Mapped, Computed, MappingName FROM CfgDocClassProperties WHERE DocClassID = " + currentTargetDocClassID + " AND ConfigID = " + currentConfigID + " ORDER BY Name";

            DisplayPropertyData(lvTargetDocProperties, sql, errorID);
        }

        private void lvTargetDocClasses_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndexTargetDocClasses))
            {
                lvTargetDocClasses.Items[lvIndexTargetDocClasses].Checked = false;
                lvIndexTargetDocClasses = e.Item.Index;
                currentTargetDocClassID = Convert.ToInt32((string)(lvTargetDocClasses.Items[lvIndexTargetDocClasses].Tag));
                RefreshTargetDocProperties();
            }
        }

        private void RefreshTargetAnnotationPage()
        {
            if (configSelected)
            {
                lblTargetAnnConfig.Text = "Current Configuration: " + currentConfigName;
                RefreshTargetAnnClasses();
                RefreshTargetAnnProperties();
            }
            else
            {
                ClearDisplay(lblTargetAnnConfig, lvTargetAnnClasses);
                ClearDisplay(lblTargetAnnConfig, lvTargetAnnProperties);
            }
        }

        private void RefreshTargetAnnClasses()
        {
            lvTargetAnnClasses.BeginUpdate();
            lvTargetAnnClasses.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID, AnnClassName FROM CfgAnnClasses WHERE ConfigID = " + currentConfigID + " ORDER BY AnnClassName");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = lvTargetAnnClasses.Items.Add(rowNode.GetAttribute("AnnClassName"), i);
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                    if(lvIndexTargetAnnClasses > (count - 1)) lvIndexTargetAnnClasses = count - 1;
                    lvTargetAnnClasses.Items[lvIndexTargetAnnClasses].Checked = true;
                    currentTargetAnnClassID = Convert.ToInt32((string)(lvTargetAnnClasses.Items[lvIndexTargetAnnClasses].Tag));
                    lvTargetAnnClasses.EnsureVisible(lvIndexTargetAnnClasses);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Annotation Classes : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                lvTargetAnnClasses.EndUpdate();
            }
        }

        private void RefreshTargetAnnProperties()
        {
            string errorID = "Target Annotation Class Properties";
            string sql = "SELECT Name, Source, DataType, Required, Mapped, Computed, MappingName FROM CfgAnnClassProperties WHERE AnnClassID = " + currentTargetAnnClassID + " AND ConfigID = " + currentConfigID + " ORDER BY Name";

            DisplayPropertyData(lvTargetAnnProperties, sql, errorID);
        }

        private void lvTargetAnnClasses_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndexTargetAnnClasses))
            {
                lvTargetAnnClasses.Items[lvIndexTargetAnnClasses].Checked = false;
                lvIndexTargetAnnClasses = e.Item.Index;
                currentTargetAnnClassID = Convert.ToInt32((string)(lvTargetAnnClasses.Items[lvIndexTargetAnnClasses].Tag));
                RefreshTargetAnnProperties();
            }
        }

        private void RefreshTargetTaskPage()
        {
            if (configSelected)
            {
                lblTargetTaskConfig.Text = "Current Configuration: " + currentConfigName;
                RefreshTargetTaskTypes();
                RefreshTargetTaskProperties();
                RefreshTargetInitDocs();
            }
            else
            {
                ClearDisplay(lblTargetTaskConfig, lvTargetTaskTypes);
                ClearDisplay(lblTargetTaskConfig, lvTargetTaskProperties);
                ClearDisplay(lblTargetTaskConfig, lvTargetInitDocs);
            }
        }

        private void RefreshTargetTaskTypes()
        {
            lvTargetTaskTypes.BeginUpdate();
            lvTargetTaskTypes.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT t.ID, t.TaskName, c.CaseName FROM CfgTaskTypes t, CfgCaseTypes c WHERE t.CaseID = c.ID AND ConfigID = " + currentConfigID + " ORDER BY t.TaskName");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = lvTargetTaskTypes.Items.Add(rowNode.GetAttribute("TaskName"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("CaseName"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                    if (lvIndexTargetTaskTypes > (count - 1)) lvIndexTargetTaskTypes = count - 1;
                    lvTargetTaskTypes.Items[lvIndexTargetTaskTypes].Checked = true;
                    currentTargetTaskTypeID = Convert.ToInt32((string)(lvTargetTaskTypes.Items[lvIndexTargetTaskTypes].Tag));
                    lvTargetTaskTypes.EnsureVisible(lvIndexTargetTaskTypes);
                }
                else
                {
                    currentTargetTaskTypeID = -1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Task Types : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                lvTargetTaskTypes.EndUpdate();
            }
        }

        private void RefreshTargetTaskProperties()
        {
            string errorID = "Target Task Properties";
            string sql = "SELECT Name, Source, DataType, Required, Mapped, Computed, MappingName FROM CfgTaskProperties WHERE TaskTypeID = " + currentTargetTaskTypeID + " AND ConfigID = " + currentConfigID + " ORDER BY Name";

            DisplayPropertyData(lvTargetTaskProperties, sql, errorID);
        }

        private void lvTargetTaskTypes_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndexTargetTaskTypes))
            {
                lvTargetTaskTypes.Items[lvIndexTargetTaskTypes].Checked = false;
                lvIndexTargetTaskTypes = e.Item.Index;
                currentTargetTaskTypeID = Convert.ToInt32((string)(lvTargetTaskTypes.Items[lvIndexTargetTaskTypes].Tag));
                RefreshTargetTaskPage();
            }
        }

        private void RefreshTargetInitDocs()
        {
            const int nColumns = 4;
            string[] fields = new string[nColumns];

            string errorID = "Target Task Initiating Document";
            string sql = "SELECT * FROM CfgTaskTypes WHERE ID = " + currentTargetTaskTypeID;

            fields[0] = "IsDocInitiated";
            fields[1] = "InitiatingDocClass";
            fields[2] = "InitiatingDocIdSource";
            fields[3] = "AttachingProperty";

            MigrationManager.RefreshListView(lvTargetInitDocs, dbConnection, sql, fields, errorID);
        }

        private void btnTgtCfgCaseAdd_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && (IsOKThatJobsExist(dbConnection, currentConfigID)))
            {
                using (CaseTypesDialog dlg = new CaseTypesDialog(dbConnection, currentConfigID, DialogOperation.Add))
                {
                    dlg.ShowDialog();
                    RefreshTargetCasePage();
                }
            }
        }

        private void btnTgtCfgCaseUpd_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && (IsOKThatJobsExist(dbConnection, currentConfigID)))
            {
                using (CaseTypesDialog dlg = new CaseTypesDialog(dbConnection, currentConfigID, DialogOperation.Update))
                {
                    string id = lvTargetCaseTypes.Items[lvIndexTargetCaseTypes].Tag.ToString();
                    dlg.Populate(id);
                    dlg.ShowDialog();
                    RefreshTargetCasePage();
                }
            }
        }

        private void btnTgtCfgCaseDel_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && (IsOKThatJobsExist(dbConnection, currentConfigID)))
            {
                string id = lvTargetCaseTypes.Items[lvIndexTargetCaseTypes].Tag.ToString();
                if (MessageBox.Show("Delete Case Type " + lvTargetCaseTypes.Items[lvIndexTargetCaseTypes].SubItems[0].Text + " and all its properties and folders. Proceed?", "Confirm configuration delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    DeleteCaseProperties(dbConnection, currentConfigID, id);
                    DeleteCaseFolders(dbConnection, currentConfigID, id);
                    DeleteRecord(dbConnection, "CfgCaseTypes", id);
                    RefreshTargetCasePage();
                }
            }
        }

        private void btnTgtCfgDocAdd_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && (IsOKThatJobsExist(dbConnection, currentConfigID)))
            {
                using (DocTypesDialog dlg = new DocTypesDialog(dbConnection, currentConfigID, DialogOperation.Add, ClassType.Document, IsCaseLevelConfig()))
                {
                    dlg.ShowDialog();
                    RefreshTargetDocumentPage();
                }
            }
        }

        private void btnTgtCfgDocUpd_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && (IsOKThatJobsExist(dbConnection, currentConfigID)))
            {
                using (DocTypesDialog dlg = new DocTypesDialog(dbConnection, currentConfigID, DialogOperation.Update, ClassType.Document, IsCaseLevelConfig()))
                {
                    string id = lvTargetDocClasses.Items[lvIndexTargetDocClasses].Tag.ToString();
                    dlg.Populate(id);
                    dlg.ShowDialog();
                    RefreshTargetDocumentPage();
                }
            }
        }

        private void btnTgtCfgDocDel_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && (IsOKThatJobsExist(dbConnection, currentConfigID)))
            {
                if(IsOKToDeleteDocClass(lvTargetDocClasses.Items[lvIndexTargetDocClasses].SubItems[0].Text))
                {
                    string id = lvTargetDocClasses.Items[lvIndexTargetDocClasses].Tag.ToString();
                    if (MessageBox.Show("Delete Document Class " + lvTargetDocClasses.Items[lvIndexTargetDocClasses].SubItems[0].Text + " and all its properties. Proceed?", "Confirm configuration delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DeleteDocClassProperties(dbConnection, currentConfigID, id);
                        DeleteRecord(dbConnection, "CfgDocClasses", id);
                        RefreshTargetDocumentPage();
                    }
                }
            }
        }

        private void btnTgtCfgAnnAdd_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && (IsOKThatJobsExist(dbConnection, currentConfigID)))
            {
                using (DocTypesDialog dlg = new DocTypesDialog(dbConnection, currentConfigID, DialogOperation.Add, ClassType.Annotation, IsCaseLevelConfig()))
                {
                    dlg.ShowDialog();
                    RefreshTargetAnnotationPage();
                }
            }
        }

        private void btnTgtCfgAnnUpd_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && (IsOKThatJobsExist(dbConnection, currentConfigID)))
            {
                using (DocTypesDialog dlg = new DocTypesDialog(dbConnection, currentConfigID, DialogOperation.Update, ClassType.Annotation, IsCaseLevelConfig()))
                {
                    string id = lvTargetAnnClasses.Items[lvIndexTargetAnnClasses].Tag.ToString();
                    dlg.Populate(id);
                    dlg.ShowDialog();
                    RefreshTargetAnnotationPage();
                }
            }
        }

        private void btnTgtCfgAnnDel_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && (IsOKThatJobsExist(dbConnection, currentConfigID)))
            {
                string id = lvTargetAnnClasses.Items[lvIndexTargetAnnClasses].Tag.ToString();
                if (MessageBox.Show("Delete Annotation Class " + lvTargetAnnClasses.Items[lvIndexTargetAnnClasses].SubItems[0].Text + " and all its properties. Proceed?", "Confirm configuration delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    DeleteAnnClassProperties(dbConnection, currentConfigID, id);
                    DeleteRecord(dbConnection, "CfgAnnClasses", id);
                    RefreshTargetAnnotationPage();
                }
            }
        }

        private void btnTgtCfgTaskAdd_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && (IsOKThatJobsExist(dbConnection, currentConfigID)))
            {
                using (TaskTypesDialog dlg = new TaskTypesDialog(dbConnection, currentConfigID, DialogOperation.Add))
                {
                    dlg.ShowDialog();
                    RefreshTargetTaskPage();
                }
            }
        }

        private void btnTgtCfgTaskUpd_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && (IsOKThatJobsExist(dbConnection, currentConfigID)))
            {
                using (TaskTypesDialog dlg = new TaskTypesDialog(dbConnection, currentConfigID, DialogOperation.Update))
                {
                    string id = lvTargetTaskTypes.Items[lvIndexTargetTaskTypes].Tag.ToString();
                    dlg.Populate(id);
                    dlg.ShowDialog();
                    RefreshTargetTaskPage();
                }
            }
        }

        private void btnTgtCfgTaskDel_Click(object sender, EventArgs e)
        {
            if ((IsConfigSelected()) && (IsOKThatJobsExist(dbConnection, currentConfigID)))
            {
                string id = lvTargetTaskTypes.Items[lvIndexTargetTaskTypes].Tag.ToString();
                if (MessageBox.Show("Delete Task Type " + lvTargetTaskTypes.Items[lvIndexTargetTaskTypes].SubItems[0].Text + " and all its properties. Proceed?", "Confirm configuration delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    DeleteTaskProperties(dbConnection, currentConfigID, id);
                    DeleteRecord(dbConnection, "CfgTaskTypes", id);
                    RefreshTargetTaskPage();
                }
            }
        }

        private Boolean IsOKToDeleteDocClass(string docClassName)
        {
            Boolean IsOK = true;

            string sql = "SELECT DefaultDocClass As DocClass FROM CfgCaseTypes WHERE ConfigID = " + currentConfigID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            foreach (XmlElement row in xml.SelectNodes("//row"))
            {
                if(docClassName == row.GetAttribute("DocClass"))
                {
                    MessageBox.Show("This is a default document class of existing case types. Hence this document class definition cannot be deleted.", "Cannot Delete Document Class", MessageBoxButtons.OK);
                    IsOK = false;
                    break;
                }
            }

            if (IsOK)
            {
                sql = "SELECT t.InitiatingDocClass As DocClass FROM CfgTaskTypes t, CfgCaseTypes c WHERE c.ID = t.CaseID AND c.ConfigID = " + currentConfigID;
                xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

                foreach (XmlElement row in xml.SelectNodes("//row"))
                {
                    if (docClassName == row.GetAttribute("DocClass"))
                    {
                        MessageBox.Show("This is an initiating document class of existing task types. Hence this document class definition cannot be deleted.", "Cannot Delete Document Class", MessageBoxButtons.OK);
                        IsOK = false;
                        break;
                    }
                }
            }
            return IsOK;
        }
    }
}
