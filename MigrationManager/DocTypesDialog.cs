﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public enum ClassType
    {
        Document,
        Annotation
    }

    public partial class DocTypesDialog : Form
    {
        string dbCnxn = string.Empty;
        string configID = string.Empty;
        DialogOperation operation;
        ClassType classType;
        Boolean isCaseLevelConfig;
        string ID = string.Empty;
        Boolean isDirty = false;
        string qualifier = string.Empty;
        string oldDocClass = string.Empty;

        public DocTypesDialog(string dbConnection, string cfgID, DialogOperation op, ClassType ct, Boolean isCaseLevel)
        {
            dbCnxn = dbConnection;
            configID = cfgID;
            operation = op;
            classType = ct;
            isCaseLevelConfig = isCaseLevel;

            InitializeComponent();

            qualifier = (classType == ClassType.Document) ? "Document" : "Annotation";
            lblName.Text = qualifier + " Class Name:";
            lblHeader.Text = (op == DialogOperation.Add) ? "Add New " + qualifier + " Class" : "Update " + qualifier + " Class Definition";
            PopulatePreDefinedClasses();
        }

        private void PopulatePreDefinedClasses()
        {
            cmbName.Items.Clear();
            if (classType == ClassType.Annotation)
            {
                cmbName.DropDownStyle = ComboBoxStyle.DropDown;
                cmbName.Items.Add("Annotation");
                if (isCaseLevelConfig)
                {
                    cmbName.Items.Add("CmAcmCaseComment");
                    cmbName.Items.Add("CmAcmTaskComment");
                    cmbName.Items.Add("CmAcmVersionSeriesComment");
                }
            }
            else
            {
                cmbName.DropDownStyle = ComboBoxStyle.Simple;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (SyncType())
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnProperties_Click(object sender, EventArgs e)
        {
            if (SyncType())
            {
                string tableName = (classType == ClassType.Document) ? "CfgDocClasses" : "CfgAnnClasses";
                string fieldName = (classType == ClassType.Document) ? "DocClassName" : "AnnClassName";
                PropertyType pType = (classType == ClassType.Document) ? PropertyType.Document : PropertyType.Annotation;

                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT ID FROM " + tableName + " WHERE ConfigID = " + configID + " AND " + fieldName + " = '" + cmbName.Text.Trim() + "'");
                ID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;

                using (ListPropertiesDialog dlg = new ListPropertiesDialog(dbCnxn, configID, ID, pType))
                {
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        // nothing for now
                    }
                }
            }
        }

        public void Populate(string id)
        {
            ID = id;

            string tableName = (classType == ClassType.Document) ? "CfgDocClasses" : "CfgAnnClasses";
            string fieldName = (classType == ClassType.Document) ? "DocClassName" : "AnnClassName";
            
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT * FROM " + tableName + " WHERE ID = " + ID);
            cmbName.Text = xml.SelectNodes("//row")[0].Attributes[fieldName].Value;
            oldDocClass = cmbName.Text;
        }

        private Boolean HasPassedValidation()
        {
            if (cmbName.Text.Length == 0)
            {
                MessageBox.Show("Please specify a " + qualifier + " Class Name", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            return IsClassNameUnique();
        }

        private Boolean IsClassNameUnique()
        {
            string name = cmbName.Text.Trim();

            string sql = (classType == ClassType.Document) ?
                            "SELECT * FROM CfgDocClasses WHERE DocClassName = '" + name + "' AND ConfigID = " + configID :
                            "SELECT * FROM CfgAnnClasses WHERE AnnClassName = '" + name + "' AND ConfigID = " + configID;
            
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);
            if (xml.SelectNodes("//row").Count > 0)
            {
                if (operation == DialogOperation.Add)
                {
                    MessageBox.Show(qualifier + " Class " + name + " already exists. Please choose another name.", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
                else
                {
                    string temp = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                    if (temp != ID)
                    {
                        MessageBox.Show(qualifier + " Class " + name + " already exists. Please choose another name.", "Migration Manager", MessageBoxButtons.OK);
                        return false;
                    }
                }
            }
            return true;
        }

        private Boolean SyncType()
        {
            if (HasPassedValidation())
            {
                if (isDirty)
                {
                    string name = cmbName.Text.Trim();
                    string sql = string.Empty;
                    if (operation == DialogOperation.Add)
                    {
                        sql = (classType == ClassType.Document) ? 
                                    "INSERT INTO CfgDocClasses (ConfigID, DocClassName) VALUES (" + configID + ", '" + name + "')" : 
                                    "INSERT INTO CfgAnnClasses (ConfigID, AnnClassName) VALUES (" + configID + ", '" + name + "')";
                        int nAffected = DBAdapter.DBAdapter.AddToDB(dbCnxn, sql);

                        sql = (classType == ClassType.Document) ? 
                                "SELECT ID FROM CfgDocClasses WHERE DocClassName = '" + name + "'" : 
                                "SELECT ID FROM CfgAnnClasses WHERE AnnClassName = '" + name + "'";
                        XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);
                        ID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                        operation = DialogOperation.Update;
                    }
                    else
                    {
                        sql = (classType == ClassType.Document) ? 
                                    "UPDATE CfgDocClasses SET DocClassName = '" + name + "' WHERE ID = " + ID : 
                                    "UPDATE CfgAnnClasses SET AnnClassName = '" + name + "' WHERE ID = " + ID;
                        int nAffected = DBAdapter.DBAdapter.UpdateDB(dbCnxn, sql);
                        UpdateAffectedCaseTypes(name);
                    }
                    isDirty = false;
                }
                return true;
            }
            return false;
        }

        private void UpdateAffectedCaseTypes(string newDocClass)
        {
            if (isCaseLevelConfig && (classType == ClassType.Document))
            {
                if (oldDocClass != newDocClass)
                {
                    string sql = "UPDATE CfgCaseTypes SET DefaultDocClass = '" + newDocClass + "' WHERE DefaultDocClass = '" + oldDocClass + "' AND ConfigID = " + configID;
                    int nAffected = DBAdapter.DBAdapter.UpdateDB(dbCnxn, sql);
                }
            }
        }

        private void cmbName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbName_TextChanged(object sender, EventArgs e)
        {
            isDirty = true;
        }
    }
}
