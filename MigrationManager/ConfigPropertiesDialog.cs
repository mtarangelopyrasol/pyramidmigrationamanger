﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public enum PropertyType
    {
        Case,
        Document,
        Annotation,
        Task
    }

    public partial class ConfigPropertiesDialog : Form
    {
        const string MULTI_VALUE_INDICATOR = "ListOf";

        string dbCnxn = string.Empty;
        string configID = string.Empty;
        DialogOperation operation;
        string parentID = string.Empty;
        string ID = string.Empty;
        PropertyType propertyType;
        Boolean isDocTraceField = false;

        public ConfigPropertiesDialog(string dbConnection, string cfgID, string parent, PropertyType pType, DialogOperation op)
        {
            dbCnxn = dbConnection;
            configID = cfgID;
            parentID = parent;
            operation = op;
            propertyType = pType;

            InitializeComponent();

            if (op == DialogOperation.Add)
            {
                lblHeader.Text = "Add New Property";
            }
            else
            {
                lblHeader.Text = "Update Property Definition";
            }
            PopulateAllCombos();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (HasPassedValidation())
            {
                string name = txtName.Text.Trim();
                string src = (cmbSource.Items.Count > 0) ? cmbSource.SelectedItem.ToString() : string.Empty;
                string dType = cmbDataType.SelectedItem.ToString();
                int req = Convert.ToInt32(cmbRequired.SelectedItem.ToString());
                int map = Convert.ToInt32(cmbMapped.SelectedItem.ToString());
                int comp = Convert.ToInt32(cmbComputed.SelectedItem.ToString());
                string mapName = ((map != 0) && (cmbMappingName.Items.Count > 0)) ? cmbMappingName.SelectedItem.ToString() : string.Empty;
                string sql = string.Empty;
                if (operation == DialogOperation.Add)
                {
                    sql = "INSERT INTO " + GetTargetTable() + " (ConfigID, " + GetParentField() + ", Name, Source, DataType, Required, Mapped, Computed, MappingName) VALUES (" + configID + ", " + parentID + ", '" + name + "', '" + src + "', '" + dType + "', " + req + ", " + map + ", " + comp + ", '" + mapName + "')";
                    int nAffected = DBAdapter.DBAdapter.AddToDB(dbCnxn, sql);
                }
                else
                {
                    sql = "UPDATE " + GetTargetTable() + " SET Name = '" + name + "', Source = '" + src + "', DataType = '" + dType + "', Required = " + req + ", Mapped = " + map + ", Computed = " + comp + ", MappingName = '" + mapName + "' WHERE ID = " + ID;
                    int nAffected = DBAdapter.DBAdapter.UpdateDB(dbCnxn, sql);

                    if (isDocTraceField)
                    {
                        sql = "UPDATE CfgIdentifiers SET FieldName = '" + name + "' WHERE IdentifierName = 'DocTraceIdentifier' AND ConfigID = " + configID;
                        nAffected = DBAdapter.DBAdapter.UpdateDB(dbCnxn, sql);
                    }
                }

                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PopulateAllCombos()
        {
            PopulateSourceCombo();
            cmbDataType.SelectedIndex = 0;
            cmbRequired.SelectedIndex = 0;
            cmbMapped.SelectedIndex = 0;
            cmbComputed.SelectedIndex = 0;
            if (cmbMapped.SelectedItem.ToString() == "0")
            {
                cmbMappingName.Enabled = false;
            }
            else
            {
                cmbMappingName.Enabled = true;
            }
            PopulateMappingNameCombo();
        }

        private void PopulateSourceCombo()
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT Name FROM " + GetSourceTable() + " WHERE ConfigID = " + configID);

            cmbSource.BeginUpdate();
            cmbSource.Items.Clear();
            cmbSource.Items.Add(" ");    // to accomodate the no source selection case
            foreach (XmlElement rowNode in xml.SelectNodes("//row"))
            {
                cmbSource.Items.Add(rowNode.GetAttribute("Name"));
            }
            if (propertyType == PropertyType.Document)
            {
                cmbSource.Items.Add("ANNOTATION_HISTORY");
                if (IsCaseLevelConfig())
                {
                    cmbSource.Items.Add("ATTACH_DOC_ID");
                }
                cmbSource.Items.Add("MMID");
            }
            else if (propertyType == PropertyType.Annotation)
            {
                cmbSource.Items.Add("PAGE_NUMBER");
                cmbSource.Items.Add("WINDOW");
            }
            cmbSource.EndUpdate();
            cmbSource.SelectedIndex = 0;
        }

        private Boolean IsCaseLevelConfig()
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT ConfigLevel FROM MgrConfigurations WHERE ID = " + configID);
            XmlNodeList xnl = xml.SelectNodes("//row");
            string level = (xnl.Count > 0) ? xnl[0].Attributes["ConfigLevel"].Value : string.Empty;
            return (level == "CASE") ? true : false;
        }

        private void PopulateMappingNameCombo()
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT MappingName FROM MgrMappings");
            int count = xml.SelectNodes("//row").Count;
            if (count > 0)
            {
                cmbMappingName.BeginUpdate();
                cmbMappingName.Items.Clear();
                cmbMappingName.Items.Add("");    // to accomodate the no mapping selection case
                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    cmbMappingName.Items.Add(rowNode.GetAttribute("MappingName"));
                }
                cmbMappingName.EndUpdate();
                cmbMappingName.SelectedIndex = 0;
            }
        }

        public void Populate(string table, string id)
        {
            ID = id;

            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT * FROM " + table + " WHERE ID = " + ID);

            txtName.Text = xml.SelectNodes("//row")[0].Attributes["Name"].Value;
            SetSelection(cmbSource, xml.SelectNodes("//row")[0].Attributes["Source"].Value);
            SetSelection(cmbDataType, xml.SelectNodes("//row")[0].Attributes["DataType"].Value);

            SetSelection(cmbRequired, xml.SelectNodes("//row")[0].Attributes["Required"].Value);
            SetSelection(cmbMapped, xml.SelectNodes("//row")[0].Attributes["Mapped"].Value);
            SetSelection(cmbComputed, xml.SelectNodes("//row")[0].Attributes["Computed"].Value);
            SetSelection(cmbMappingName, xml.SelectNodes("//row")[0].Attributes["MappingName"].Value);

            if ((propertyType == PropertyType.Document) && (xml.SelectNodes("//row")[0].Attributes["Source"].Value == "MMID"))
            {
                isDocTraceField = true;
            }
        }

        private void SetSelection(ComboBox cmb, string value)
        {
            for (int i = 0; i < cmb.Items.Count; i++)
            {
                if (cmb.Items[i].ToString() == value)
                {
                    cmb.SelectedItem = cmb.Items[i];
                    break;
                }
            }
        }

        private Boolean HasPassedValidation()
        {
            string name = txtName.Text.Trim();
            if (name.Length == 0)
            {
                MessageBox.Show("Please specify a Property Name", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            
            string src = (cmbSource.SelectedItem != null) ? cmbSource.SelectedItem.ToString().Trim() : string.Empty;
            int map = Convert.ToInt32(cmbMapped.SelectedItem.ToString());
            int comp = Convert.ToInt32(cmbComputed.SelectedItem.ToString());
            if (comp == 1) 
            {
                if ((src.Length > 0) && ((propertyType == PropertyType.Document) && (src != "ANNOTATION_HISTORY") && (src != "ATTACH_DOC_ID") && (src != "MMID") || 
                                        ((propertyType == PropertyType.Annotation) && (src != "PAGE_NUMBER")) && (src != "WINDOW")||
                                        (propertyType == PropertyType.Case) ||
                                        (propertyType == PropertyType.Task)))
                {
                    MessageBox.Show("Property is a computed field, hence cannot have a source specified", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
                if (map == 1)
                {
                    MessageBox.Show("Property is a computed field, hence cannot be mapped", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
            }
            else
            {
                if (src.Length == 0)
                {
                    MessageBox.Show("Property is not a computed field, hence must have a source specified", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
                if (map == 1)
                {
                    string mapName = (cmbMappingName.Items.Count > 0) ? cmbMappingName.SelectedItem.ToString().Trim() : string.Empty;
                    if (mapName == string.Empty)
                    {
                        MessageBox.Show("Property is mapped, hence must have a mapping specified", "Migration Manager", MessageBoxButtons.OK);
                        return false;
                    }
                }
            }
            string targetType = (cmbDataType.Items.Count > 0) ? cmbDataType.SelectedItem.ToString().Trim() : string.Empty;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT DataType, MultiValued FROM " + GetSourceTable() + " WHERE ConfigID = " + configID + " AND Name = '" + src +"'");
            if (xml.SelectNodes("//row").Count > 0)
            {
                string sourceType = xml.SelectNodes("//row")[0].Attributes["DataType"].Value;
                string sourceMultiValued = xml.SelectNodes("//row")[0].Attributes["MultiValued"].Value;
                string tgtPrefix = targetType.Substring(0, 6);

                if (((sourceMultiValued == "Y") && (tgtPrefix != MULTI_VALUE_INDICATOR)) || ((tgtPrefix == MULTI_VALUE_INDICATOR) && (sourceMultiValued == "N")))
                {
                    MessageBox.Show("Warning: The " + name + " property mapping mixes multi-valued and single valued characteristics", "Migration Manager", MessageBoxButtons.OK);
                }
                if (IsDateTimeType(sourceType) && !(IsDateTimeType(targetType)))
                {
                    MessageBox.Show(name + " is mapped to field of type DateTime", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
                if (IsDateTimeType(targetType) && !(IsDateTimeType(sourceType)))
                {
                    MessageBox.Show("DateTime Property " + name + " is not mapped to field of type DateTime", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
            }
            return true;
        }

        private Boolean IsDateTimeType(string typeName)
        {
            return (typeName.IndexOf("DateTime") >= 0) ? true : false;
        }

        private string GetSourceTable()
        {
            string tableName = string.Empty;

            switch (propertyType)
            {
                case PropertyType.Case:
                    tableName = "CfgCaseSource";
                    break;

                case PropertyType.Document:
                    tableName = "CfgDocumentSource";
                    break;

                case PropertyType.Annotation:
                    tableName = "CfgAnnotationSource";
                    break;

                case PropertyType.Task:
                    tableName = "CfgTaskSource";
                    break;

                default:
                    break;
            }
            return tableName;
        }

        private string GetTargetTable()
        {
            string tableName = string.Empty;

            switch (propertyType)
            {
                case PropertyType.Case:
                    tableName = "CfgCaseProperties";
                    break;

                case PropertyType.Document:
                    tableName = "CfgDocClassProperties";
                    break;

                case PropertyType.Annotation:
                    tableName = "CfgAnnClassProperties";
                    break;

                case PropertyType.Task:
                    tableName = "CfgTaskProperties";
                    break;

                default:
                    break;
            }
            return tableName;
        }

        private string GetParentField()
        {
            string parentField = string.Empty;

            switch (propertyType)
            {
                case PropertyType.Case:
                    parentField = "CaseTypeID";
                    break;

                case PropertyType.Document:
                    parentField = "DocClassID";
                    break;

                case PropertyType.Annotation:
                    parentField = "AnnClassID";
                    break;

                case PropertyType.Task:
                    parentField = "TaskTypeID";
                    break;

                default:
                    break;
            }
            return parentField;
        }

        private void cmbMapped_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbMapped.SelectedItem.ToString() == "0")
            {
                SetSelection(cmbMappingName, "");
                cmbMappingName.Enabled = false;
            }
            else
            {
                cmbMappingName.Enabled = true;
            }
        }

        private void cmbComputed_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void cmbSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbSource.SelectedItem.ToString() == "ANNOTATION_HISTORY")
            {
                SetSelection(cmbDataType, "ListOfString");
                cmbDataType.Enabled = false;
                SetSelection(cmbRequired, "0");
                cmbRequired.Enabled = false;
                SetSelection(cmbMapped, "0");
                cmbMapped.Enabled = false;
                SetSelection(cmbComputed, "1");
                cmbComputed.Enabled = false;
            }
            else if (cmbSource.SelectedItem.ToString() == "ATTACH_DOC_ID")
            {
                SetSelection(cmbDataType, "SingletonString");
                cmbDataType.Enabled = false;
                SetSelection(cmbMapped, "0");
                cmbMapped.Enabled = false;
                SetSelection(cmbComputed, "1");
                cmbComputed.Enabled = false;
            }
            else if (cmbSource.SelectedItem.ToString() == "PAGE_NUMBER")
            {
                SetSelection(cmbDataType, "SingletonString");
                cmbDataType.Enabled = false;
                SetSelection(cmbMapped, "0");
                cmbMapped.Enabled = false;
                SetSelection(cmbComputed, "1");
                cmbComputed.Enabled = false;
            }
            else if (cmbSource.SelectedItem.ToString() == "WINDOW")
            {
                SetSelection(cmbDataType, "SingletonString");
                cmbDataType.Enabled = false;
                SetSelection(cmbMapped, "0");
                cmbMapped.Enabled = false;
                SetSelection(cmbComputed, "1");
                cmbComputed.Enabled = false;
            }
            else
            {
                cmbDataType.Enabled = true;
                cmbRequired.Enabled = true;
                cmbMapped.Enabled = true;
                cmbComputed.Enabled = true;
            }
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            if ((propertyType == PropertyType.Annotation) && (IsReserved(txtName.Text.Trim())))
            {
                cmbSource.Enabled = false;
                cmbDataType.Enabled = false;
                cmbRequired.Enabled = false;
                cmbMapped.Enabled = false;
                cmbComputed.Enabled = false;
            }
            else
            {
                cmbSource.Enabled = true;
                cmbDataType.Enabled = true;
                cmbRequired.Enabled = true;
                cmbMapped.Enabled = true;
                cmbComputed.Enabled = true;
            }
        }

        private Boolean IsReserved(string name)
        {
            return ((name == "AnnotatedObject") || (name == "CmAcmAction") || (name == "CmAcmCommentedTask") || (name == "CmAcmCommentedVersionSeries")) ? true : false;
        }
    }
}
