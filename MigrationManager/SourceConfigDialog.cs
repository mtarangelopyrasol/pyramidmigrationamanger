﻿using System;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class SourceConfigDialog : Form
    {
        string dbCnxn = string.Empty;
        string configID = string.Empty;
        string tableName;
        DialogOperation operation;
        string ID = string.Empty;
        int maxColumn = 0;
        int oldColumn = 0;
        string oldName = string.Empty;

        public SourceConfigDialog(string dbConnection, string cfgID, string table, DialogOperation op)
        {
            dbCnxn = dbConnection;
            configID = cfgID;
            tableName = table;
            operation = op;

            InitializeComponent();

            maxColumn = DetermineMaxColumn();

            if (op == DialogOperation.Add)
            {
                txtColumn.Text = (maxColumn + 1).ToString();
                txtColumn.Enabled = false;
                lblHeader.Text = "Add New Column Definition";
                cmbDataType.SelectedItem = cmbDataType.Items[0];
                cmbMultiValued.SelectedItem = cmbMultiValued.Items[0];
            }
            else
            {
                lblHeader.Text = "Update Column Definition";
                txtColumn.Enabled = true;
            }
        }

        private int DetermineMaxColumn()
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT MAX(ColumnID) AS LASTCOL FROM " + tableName + " WHERE ConfigID = " + configID);
            XmlAttribute att = xml.SelectNodes("//row")[0].Attributes["LASTCOL"];
            return (att == null) ? 0 : (Convert.ToInt32(att.Value));
        }

        public void Populate(string id)
        {
            ID = id;

            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT * FROM " + tableName + " WHERE ID = " + ID);

            txtColumn.Text = xml.SelectNodes("//row")[0].Attributes["ColumnID"].Value;
            oldColumn = Convert.ToInt32(txtColumn.Text);
            txtFieldName.Text = xml.SelectNodes("//row")[0].Attributes["Name"].Value;
            oldName = txtFieldName.Text;
            string dtype = xml.SelectNodes("//row")[0].Attributes["DataType"].Value;
            for (int i = 0; i < cmbDataType.Items.Count; i++)
            {
                if (cmbDataType.Items[i].ToString() == dtype)
                {
                    cmbDataType.SelectedItem = cmbDataType.Items[i];
                    break;
                }
            }
            txtLength.Text = xml.SelectNodes("//row")[0].Attributes["MaxLength"].Value;
            txtFormat.Text = xml.SelectNodes("//row")[0].Attributes["Format"].Value;
            string mValued = xml.SelectNodes("//row")[0].Attributes["MultiValued"].Value;
            for (int j = 0; j < cmbMultiValued.Items.Count; j++)
            {
                if (cmbMultiValued.Items[j].ToString() == mValued)
                {
                    cmbMultiValued.SelectedItem = cmbMultiValued.Items[j];
                    break;
                }
            }
        }

        private Boolean HasPassedValidation()
        {
            if (operation == DialogOperation.Update)
            {
                try
                {
                    if (Convert.ToInt32(txtColumn.Text.Trim()) > maxColumn)
                    {
                        MessageBox.Show("Column Number cannot exceed " + maxColumn.ToString(), "Migration Manager", MessageBoxButtons.OK);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Invalid column number: " + txtColumn.Text.Trim() + " (" + ex.Message + ") Column Number cannot exceed " + maxColumn.ToString(), "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
            }
            string fieldName = txtFieldName.Text.ToUpper();
            if (fieldName.Length == 0)
            {
                MessageBox.Show("Please specify a Field Name", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (fieldName.Length >= 64)
            {
                MessageBox.Show("Field Name is too long. Field Names can be at most 64 characters", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if((fieldName == "ID") || (fieldName == "STATUS") || (fieldName == "BATCHID"))
            {
                MessageBox.Show("Field Names ID, Status and BatchID are reserved. Please choose another name", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (txtLength.Text.Length == 0)
            {
                MessageBox.Show("Please specify a Field Length", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            string selection = cmbDataType.SelectedItem.ToString();
            if (selection == "DateTime")
            {
                if (txtFormat.Text.Length == 0)
                {
                    MessageBox.Show("Please specify a date format", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
            }
            return IsUniqueName() && PassedIdentifierCheck();
        }

        private Boolean IsUniqueName()
        {
            string name = MigrationManager.EscapeSingleQuote(txtFieldName.Text.Trim());
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT * FROM " + tableName + " WHERE Name = '" + name + "' AND ConfigID = " + configID);
            if (xml.SelectNodes("//row").Count > 0)
            {
                if (operation == DialogOperation.Add)
                {
                    MessageBox.Show("Property " + name + " already exists. Please choose another name.", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
                else
                {
                    string temp = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                    if (temp != ID)
                    {
                        MessageBox.Show("Property " + name + " already exists. Please choose another name.", "Migration Manager", MessageBoxButtons.OK);
                        return false;
                    }
                }
            }
            return true;
        }

        private Boolean PassedIdentifierCheck()
        {
            string selection = cmbMultiValued.SelectedItem.ToString();
            if (selection == "Y")
            {
                string field = txtFieldName.Text.Trim();

                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT IdentifierName FROM CfgIdentifiers WHERE FieldName = '" + field + "' AND ConfigID = " + configID);
                if (xml.SelectNodes("//row").Count > 0)
                {
                    string tablePrefix = tableName.Substring(3, 3);
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        string idName = rowNode.GetAttribute("IdentifierName");
                        string idNamePrefix = idName.Substring(0, 3);
                        if (tablePrefix == idNamePrefix)
                        {
                            MessageBox.Show("Property " + field + " cannot be multi-valued when it is the configuration's " + idName + ".", "Migration Manager", MessageBoxButtons.OK);
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        private string GetIdentifierPrefix()
        {
            string prefix = string.Empty;

            switch (tableName)
            {
                case "CfgCaseSource":
                    prefix = "Cas%";
                    break;

                case "CfgDocumentSource":
                    prefix = "Doc%";
                    break;

                case "CfgAnnotationSource":
                    prefix = "Ann%";
                    break;

                case "CfgTaskSource":
                    prefix = "Tas%";
                    break;

                default:
                    break;
            }
            return prefix;
        }

        private string GetTargetPropertiesTable()
        {
            string targetPropertiesTable = string.Empty;

            switch (tableName)
            {
                case "CfgCaseSource":
                    targetPropertiesTable = "CfgCaseProperties";
                    break;

                case "CfgDocumentSource":
                    targetPropertiesTable = "CfgDocClassProperties";
                    break;

                case "CfgAnnotationSource":
                    targetPropertiesTable = "CfgAnnClassProperties";
                    break;

                case "CfgTaskSource":
                    targetPropertiesTable = "CfgTaskProperties";
                    break;

                default:
                    break;
            }
            return targetPropertiesTable;
        }

        private void UpdateIdentifiers(string newName)
        {
            string sql = "UPDATE CfgIdentifiers SET FieldName = '" + newName + "' WHERE FieldName = '" + oldName + "' AND ConfigID = " + configID + " AND IdentifierName LIKE '" + GetIdentifierPrefix() + "'";
            int nAffected = DBAdapter.DBAdapter.UpdateDB(dbCnxn, sql);
        }

        private void UpdateSourceOfTargetProperties(string newName)
        {
            string sql = "UPDATE " + GetTargetPropertiesTable() + " SET Source = '" + newName + "' WHERE Source = '" + oldName + "' AND ConfigID = " + configID;
            int nAffected = DBAdapter.DBAdapter.UpdateDB(dbCnxn, sql);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (HasPassedValidation())
            {
                string column = txtColumn.Text.Trim();
                string name = MigrationManager.EscapeSingleQuote(txtFieldName.Text.Trim());
                string dType = cmbDataType.SelectedItem.ToString();
                string len = txtLength.Text.Trim();
                string fmt = txtFormat.Text.Trim();
                string mvalued = cmbMultiValued.SelectedItem.ToString();
                string sql = (operation == DialogOperation.Add) ? 
                    "INSERT INTO " + tableName + "(ConfigID, ColumnID, Name, DataType, maxlength, Format, MultiValued) VALUES (" + configID + ", " + column + ", '" + name + "', '" + dType + "', " + len + ", '" + fmt + "', '" + mvalued + "')" :
                    "UPDATE " + tableName + " SET ColumnID = " + column + ", Name = '" + name + "', DataType = '" + dType + "', MaxLength = " + len + ", Format = '" + fmt + "', MultiValued = '" + mvalued + "' WHERE ID = " + ID;

                if ((operation == DialogOperation.Update))
                {
                    if (oldColumn.ToString() != column)
                    {
                        DBAdapter.DBAdapter.ExecuteSPAdjustColumnNumbers(dbCnxn, Convert.ToInt32(configID), tableName, oldColumn, Convert.ToInt32(column));
                    }
                    int nAffected = DBAdapter.DBAdapter.UpdateDB(dbCnxn, sql);
                    UpdateIdentifiers(name);
                    UpdateSourceOfTargetProperties(name);
                }
                else
                {
                    int nAffected = DBAdapter.DBAdapter.AddToDB(dbCnxn, sql);
                }
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbDataType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selection = cmbDataType.SelectedItem.ToString();
            if (selection == "String")
            {
                txtFormat.Text = "";
            }
        }
    }
}
