﻿namespace MigrationManager
{
    partial class ConfigPropertiesDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHeader = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblSource = new System.Windows.Forms.Label();
            this.lblDataType = new System.Windows.Forms.Label();
            this.lblRequired = new System.Windows.Forms.Label();
            this.lblMapped = new System.Windows.Forms.Label();
            this.lblComputed = new System.Windows.Forms.Label();
            this.lblMappingName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.cmbSource = new System.Windows.Forms.ComboBox();
            this.cmbDataType = new System.Windows.Forms.ComboBox();
            this.cmbRequired = new System.Windows.Forms.ComboBox();
            this.cmbMapped = new System.Windows.Forms.ComboBox();
            this.cmbComputed = new System.Windows.Forms.ComboBox();
            this.cmbMappingName = new System.Windows.Forms.ComboBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Location = new System.Drawing.Point(219, 37);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(64, 23);
            this.lblHeader.TabIndex = 0;
            this.lblHeader.Text = "Header";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(20, 80);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(59, 23);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Name:";
            // 
            // lblSource
            // 
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new System.Drawing.Point(20, 130);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(66, 23);
            this.lblSource.TabIndex = 2;
            this.lblSource.Text = "Source:";
            // 
            // lblDataType
            // 
            this.lblDataType.AutoSize = true;
            this.lblDataType.Location = new System.Drawing.Point(20, 180);
            this.lblDataType.Name = "lblDataType";
            this.lblDataType.Size = new System.Drawing.Size(92, 23);
            this.lblDataType.TabIndex = 3;
            this.lblDataType.Text = "Data Type:";
            // 
            // lblRequired
            // 
            this.lblRequired.AutoSize = true;
            this.lblRequired.Location = new System.Drawing.Point(20, 230);
            this.lblRequired.Name = "lblRequired";
            this.lblRequired.Size = new System.Drawing.Size(83, 23);
            this.lblRequired.TabIndex = 4;
            this.lblRequired.Text = "Required:";
            // 
            // lblMapped
            // 
            this.lblMapped.AutoSize = true;
            this.lblMapped.Location = new System.Drawing.Point(20, 280);
            this.lblMapped.Name = "lblMapped";
            this.lblMapped.Size = new System.Drawing.Size(73, 23);
            this.lblMapped.TabIndex = 5;
            this.lblMapped.Text = "Mapped:";
            // 
            // lblComputed
            // 
            this.lblComputed.AutoSize = true;
            this.lblComputed.Location = new System.Drawing.Point(20, 330);
            this.lblComputed.Name = "lblComputed";
            this.lblComputed.Size = new System.Drawing.Size(92, 23);
            this.lblComputed.TabIndex = 6;
            this.lblComputed.Text = "Computed:";
            // 
            // lblMappingName
            // 
            this.lblMappingName.AutoSize = true;
            this.lblMappingName.Location = new System.Drawing.Point(20, 380);
            this.lblMappingName.Name = "lblMappingName";
            this.lblMappingName.Size = new System.Drawing.Size(126, 23);
            this.lblMappingName.TabIndex = 7;
            this.lblMappingName.Text = "Mapping Name:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(145, 80);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(413, 27);
            this.txtName.TabIndex = 8;
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // cmbSource
            // 
            this.cmbSource.FormattingEnabled = true;
            this.cmbSource.Location = new System.Drawing.Point(145, 130);
            this.cmbSource.Name = "cmbSource";
            this.cmbSource.Size = new System.Drawing.Size(413, 31);
            this.cmbSource.TabIndex = 9;
            this.cmbSource.SelectedIndexChanged += new System.EventHandler(this.cmbSource_SelectedIndexChanged);
            // 
            // cmbDataType
            // 
            this.cmbDataType.FormattingEnabled = true;
            this.cmbDataType.Items.AddRange(new object[] {
            "SingletonString",
            "ListOfString",
            "SingletonDateTime",
            "ListOfDateTime",
            "SingletonInt32",
            "ListOfInteger32",
            "SingletonBoolean",
            "ListOfBoolean",
            "SingletonFloat64",
            "ListOfFloat64",
            "",
            ""});
            this.cmbDataType.Location = new System.Drawing.Point(145, 170);
            this.cmbDataType.Name = "cmbDataType";
            this.cmbDataType.Size = new System.Drawing.Size(413, 31);
            this.cmbDataType.TabIndex = 10;
            // 
            // cmbRequired
            // 
            this.cmbRequired.FormattingEnabled = true;
            this.cmbRequired.Items.AddRange(new object[] {
            "0",
            "1"});
            this.cmbRequired.Location = new System.Drawing.Point(145, 230);
            this.cmbRequired.Name = "cmbRequired";
            this.cmbRequired.Size = new System.Drawing.Size(45, 31);
            this.cmbRequired.TabIndex = 11;
            // 
            // cmbMapped
            // 
            this.cmbMapped.FormattingEnabled = true;
            this.cmbMapped.Items.AddRange(new object[] {
            "0",
            "1"});
            this.cmbMapped.Location = new System.Drawing.Point(145, 280);
            this.cmbMapped.Name = "cmbMapped";
            this.cmbMapped.Size = new System.Drawing.Size(45, 31);
            this.cmbMapped.TabIndex = 12;
            this.cmbMapped.SelectedIndexChanged += new System.EventHandler(this.cmbMapped_SelectedIndexChanged);
            // 
            // cmbComputed
            // 
            this.cmbComputed.FormattingEnabled = true;
            this.cmbComputed.Items.AddRange(new object[] {
            "0",
            "1"});
            this.cmbComputed.Location = new System.Drawing.Point(145, 330);
            this.cmbComputed.Name = "cmbComputed";
            this.cmbComputed.Size = new System.Drawing.Size(45, 31);
            this.cmbComputed.TabIndex = 13;
            this.cmbComputed.SelectedIndexChanged += new System.EventHandler(this.cmbComputed_SelectedIndexChanged);
            // 
            // cmbMappingName
            // 
            this.cmbMappingName.FormattingEnabled = true;
            this.cmbMappingName.Location = new System.Drawing.Point(145, 380);
            this.cmbMappingName.Name = "cmbMappingName";
            this.cmbMappingName.Size = new System.Drawing.Size(413, 31);
            this.cmbMappingName.TabIndex = 14;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(478, 443);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 32);
            this.btnOK.TabIndex = 15;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(382, 443);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 32);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ConfigPropertiesDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(580, 498);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.cmbMappingName);
            this.Controls.Add(this.cmbComputed);
            this.Controls.Add(this.cmbMapped);
            this.Controls.Add(this.cmbRequired);
            this.Controls.Add(this.cmbDataType);
            this.Controls.Add(this.cmbSource);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lblMappingName);
            this.Controls.Add(this.lblComputed);
            this.Controls.Add(this.lblMapped);
            this.Controls.Add(this.lblRequired);
            this.Controls.Add(this.lblDataType);
            this.Controls.Add(this.lblSource);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblHeader);
            this.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ConfigPropertiesDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ConfigPropertiesDialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblSource;
        private System.Windows.Forms.Label lblDataType;
        private System.Windows.Forms.Label lblRequired;
        private System.Windows.Forms.Label lblMapped;
        private System.Windows.Forms.Label lblComputed;
        private System.Windows.Forms.Label lblMappingName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.ComboBox cmbSource;
        private System.Windows.Forms.ComboBox cmbDataType;
        private System.Windows.Forms.ComboBox cmbRequired;
        private System.Windows.Forms.ComboBox cmbMapped;
        private System.Windows.Forms.ComboBox cmbComputed;
        private System.Windows.Forms.ComboBox cmbMappingName;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
    }
}