﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml;
using PreIngest;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        int currentPrepareJobIndex = 0;
        Boolean jobSelected = false;

        int currentBatchDefnIndex = 0;
        Boolean batchDefnSelected = false;

        PreIngest.Converter converter;
        
        Boolean converterInitialized = false;
        Boolean caselevelPrepare = false;
        
        private void tabPrepare_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowPrepareSubTab();
        }

        private void ShowPrepareSubTab()
        {
            switch (tabPrepare.TabPages[tabPrepare.SelectedIndex].Name)
            {
                case "ChooseJob":
                    if (busyStatus != MigratorStatus.BusyConverting)
                    {
                        if (IsConfigSelected())
                        {
                            HideConversionLogs();
                            RefreshJobList();
                        }
                    }
                    break;
                case "ChooseBatchDefn":
                    if (busyStatus != MigratorStatus.BusyConverting)
                    {
                        if (!jobSelected)
                        {
                            MessageBox.Show("Please choose a job first", "Migration Manager", MessageBoxButtons.OK);
                        }
                        else
                        {
                            HideConversionLogs();
                            RefreshBatchDefnSelectionList();
                        }
                    }
                    break;
                case "PerformConversion":
                    if (!batchDefnSelected)
                    {
                        MessageBox.Show("Please choose a batch first", "Migration Manager", MessageBoxButtons.OK);
                    }
                    else
                    {
                        HideConversionLogs();
                        RefreshConvertPage();
                    }
                    break;
                case "ConvLogs":
                    ViewConversionLogs();
                    break;
                case "ConvResults":
                    if (busyStatus != MigratorStatus.BusyConverting)
                    {
                        if (!jobSelected)
                        {
                            MessageBox.Show("Please choose a job first", "Migration Manager", MessageBoxButtons.OK);
                        }
                        else
                        {
                            HideConversionLogs();
                            if (caselevelPrepare)
                            {
                                lblConvResultCaseID.Show();
                                txtConvResultCaseID.Show();
                                btnConvResultCaseID.Show();
                            }
                            else
                            {
                                lblConvResultCaseID.Hide();
                                txtConvResultCaseID.Hide();
                                btnConvResultCaseID.Hide();
                            }
                        }
                    }
                    break;
                default:
                    throw new Exception("Unknown option");
            }
        }

        private void RefreshJobList()
        {
            listJobs.BeginUpdate();
            listJobs.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM MgrJobs ORDER BY JobName");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = listJobs.Items.Add(rowNode.GetAttribute("ID"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("JobName"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Comment"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                    listJobs.Items[currentPrepareJobIndex].Checked = true;
                    UpdatePrepareConfigLevel();
                    listJobs.EnsureVisible(i - 1);
                    jobSelected = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Jobs : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                listJobs.EndUpdate();
            }
        }

        private void listJobs_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != currentPrepareJobIndex))
            {
                listJobs.Items[currentPrepareJobIndex].Checked = false;
                currentPrepareJobIndex = e.Item.Index;
                UpdatePrepareConfigLevel();
            }
        }

        private void UpdatePrepareConfigLevel()
        {
            try
            {
                string jobID = listJobs.Items[currentPrepareJobIndex].Tag.ToString();
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT c.ConfigLevel AS Level FROM MgrJobs j, MgrConfigurations c WHERE j.ID = " + jobID + " AND j.ConfigID = c.ID");
                string level = xml.SelectNodes("//row")[0].Attributes["Level"].Value;
                caselevelPrepare = (level == "CASE") ? true : false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while checking Job's config level : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void RefreshBatchDefnSelectionList()
        {
            listBatchDefns.BeginUpdate();
            listBatchDefns.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM MgrBatchDefns ORDER BY BatchDefnName");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = listBatchDefns.Items.Add(rowNode.GetAttribute("BatchDefnName"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("BatchSize"));
                        lvi.SubItems.Add(rowNode.GetAttribute("BatchCondition"));
                        lvi.SubItems.Add(rowNode.GetAttribute("SourceContentPathPrefix"));
                        lvi.SubItems.Add(rowNode.GetAttribute("StagingPath"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                    listBatchDefns.Items[currentBatchDefnIndex].Checked = true;
                    listBatchDefns.EnsureVisible(i - 1);
                    batchDefnSelected = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Batch Definitions : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                listBatchDefns.EndUpdate();
            }
        }

        private void listBatchDefns_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != currentBatchDefnIndex))
            {
                listBatchDefns.Items[currentBatchDefnIndex].Checked = false;
                currentBatchDefnIndex = e.Item.Index;
            }
        }
        
        private string GetBatchDefnName(string batchDefnID)
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT MgrBatchDefns.BatchDefnName AS BatchDefnName FROM MgrBatchDefns WHERE ID = " + batchDefnID);

            return xmlDoc.SelectNodes("//row")[0].Attributes["BatchDefnName"].Value;
        }

        private void RefreshConvertPage()
        {
            string jobID = listJobs.Items[currentPrepareJobIndex].Tag.ToString();
            string batchDefnID = listBatchDefns.Items[currentBatchDefnIndex].Tag.ToString();

            TabPage p = tabPrepare.TabPages[2];
            Label label = (Label)p.Controls["lblConvJob"];
            label.Text = "Job: " + GetJobName(jobID);

            label = (Label)p.Controls["lblConvBatch"];
            label.Text = "Batch Definition: " + GetBatchDefnName(batchDefnID);
        }
        
        private void bgConverter_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bw = sender as BackgroundWorker;

            //Boolean arg = (Boolean)e.Argument;
            if(migManTrace)bgConverter.ReportProgress(0, "Calling on converter to begin conversion");
            e.Result = converter.PerformConversion(bw);

            if (bw.CancellationPending)
            {
                e.Cancel = true;
            }
        }

        private void bgConverter_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            DisplayConversionStatus(e.UserState.ToString());
        }

        private void bgConverter_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            string status;
            if (e.Cancelled)
            {
                status = "Conversion canceled";
            }
            else if (e.Error != null)
            {
                status = "Conversion encountered error: " + e.Error.Message;
            }
            else
            {
                status = e.Result.ToString();
            }
            DisplayConversionStatus(status);
        }

        private void btnConvCancel_Click(object sender, EventArgs e)
        {
            bgConverter.CancelAsync();
            DisplayConversionStatus("Conversion cancel acknowledged; will cancel momentarily");
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            ResetConversionStatusDisplay();
            int maxBatches = -1;
            try
            {
                maxBatches = Convert.ToInt32(txtMaxBatches.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(txtMaxBatches.Text + ": Invalid number of batches to convert: " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
                return;
            }

            int jobID = Convert.ToInt32(listJobs.Items[currentPrepareJobIndex].Tag.ToString());
            int batchDefnID = Convert.ToInt32(listBatchDefns.Items[currentBatchDefnIndex].Tag.ToString());

            try
            {
                LoadCustomizationPoints();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Migration Manager", MessageBoxButtons.OK);
                return;
            }

            DisplayConversionStatus("Validating Job");
            if (!IsValidJob(jobID))
            {
                DisplayConversionStatus("Conversion aborted");
                return;
            }
            if(IsDocumentDataLoaded(jobID.ToString())) AdjustMMAccountID(jobID.ToString());
            DisplayConversionStatus("Validation Complete. Starting Conversion");
            
            Boolean dbg = configManager.GetAppSetting("TRACE_PREINGEST") == "1" ? true : false;

			int leadTools_JPEG_Passes;
			try
			{
				leadTools_JPEG_Passes = Int32.Parse(configManager.GetAppSetting("LeadTools_JPEG_Passes"));
			}
			catch (Exception ex)
			{
				leadTools_JPEG_Passes = -1;	// Uses the LeadTools default, according to the documentation
			}

			int leadTools_JPEG_QualityFactor;
			try
			{
				leadTools_JPEG_QualityFactor = Int32.Parse(configManager.GetAppSetting("LeadTools_JPEG_QualityFactor"));
				if (leadTools_JPEG_QualityFactor > 255)
				{
					leadTools_JPEG_QualityFactor = 255;
				}
			}
			catch (Exception ex)
			{
				leadTools_JPEG_QualityFactor = 255;	// This compresses the file as much as possible.
			}

            int leadTools_ColorFormat;
            try
            {
                leadTools_ColorFormat = Int32.Parse(configManager.GetAppSetting("LeadTools_ColorFormat"));
                if (leadTools_ColorFormat < 0 || leadTools_ColorFormat > 398)
                    leadTools_ColorFormat = 152;
            }
            catch (Exception ex)
            {
                leadTools_ColorFormat = 152; //AO default
            }

            int leadTools_BWFormat;
            try
            {
                leadTools_BWFormat = Int32.Parse(configManager.GetAppSetting("LeadTools_BWFormat"));
                if (leadTools_BWFormat < 0 || leadTools_BWFormat > 398)
                    leadTools_BWFormat = 149;
            }
            catch (Exception ex)
            {
                leadTools_BWFormat = 149; //AO default
            }

            try
            {
				converter = new Converter(dbConnection, jobID, batchDefnID, maxBatches, dbg, leadTools_JPEG_Passes, leadTools_JPEG_QualityFactor, leadTools_ColorFormat, leadTools_BWFormat);
                LogConversionTrace("Converter created");
                if (!converterInitialized)
                {
                    converter.UnlockLeadTools();
                    converterInitialized = true;
                    LogConversionTrace("Converter initialized");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception: " + ex.Message, "PreIngest", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            SetInConversion();
            try
            {
                bgConverter.RunWorkerAsync();
                while (bgConverter.IsBusy)
                {
                    Application.DoEvents();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception encountered during conversion: " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                ClearInConversion();
                //TODO: Dispose the converter
            }
        }

        private void LoadCustomizationPoints()
        {
            MMFactory.MMFactory.Instance.SetAnnotationReader(configManager.GetAppSetting("ANNOTATION_READER"), Application.StartupPath);

            MMFactory.MMFactory.Instance.SetCasePropertyEvaluator(configManager.GetAppSetting("CASE_PROPERTY_EVALUATOR"), Application.StartupPath);
            MMFactory.MMFactory.Instance.SetDocumentPropertyEvaluator(configManager.GetAppSetting("DOCUMENT_PROPERTY_EVALUATOR"), Application.StartupPath);
            MMFactory.MMFactory.Instance.SetAnnotationPropertyEvaluator(configManager.GetAppSetting("ANNOTATION_PROPERTY_EVALUATOR"), Application.StartupPath);
            MMFactory.MMFactory.Instance.SetTaskPropertyEvaluator(configManager.GetAppSetting("TASK_PROPERTY_EVALUATOR"), Application.StartupPath);

            MMFactory.MMFactory.Instance.SetCaseTypeSelector(configManager.GetAppSetting("CASE_TYPE_SELECTOR"), Application.StartupPath);
            MMFactory.MMFactory.Instance.SetDocumentTypeSelector(configManager.GetAppSetting("DOCUMENT_TYPE_SELECTOR"), Application.StartupPath);
            MMFactory.MMFactory.Instance.SetAnnotationTypeSelector(configManager.GetAppSetting("ANNOTATION_TYPE_SELECTOR"), Application.StartupPath);
            MMFactory.MMFactory.Instance.SetTaskTypeSelector(configManager.GetAppSetting("TASK_TYPE_SELECTOR"), Application.StartupPath);

            MMFactory.MMFactory.Instance.SetCaseCommentReader(configManager.GetAppSetting("CASE_COMMENT_READER"), Application.StartupPath);
            MMFactory.MMFactory.Instance.SetDocumentCommentReader(configManager.GetAppSetting("DOCUMENT_COMMENT_READER"), Application.StartupPath);
            MMFactory.MMFactory.Instance.SetTaskCommentReader(configManager.GetAppSetting("TASK_COMMENT_READER"), Application.StartupPath);

            MMFactory.MMFactory.Instance.SetCaseNotesDocGenerator(configManager.GetAppSetting("CASE_NOTES_DOC_GENERATOR"), Application.StartupPath);
            MMFactory.MMFactory.Instance.SetDocumentNotesDocGenerator(configManager.GetAppSetting("DOCUMENT_NOTES_DOC_GENERATOR"), Application.StartupPath);
            MMFactory.MMFactory.Instance.SetTaskNotesDocGenerator(configManager.GetAppSetting("TASK_NOTES_DOC_GENERATOR"), Application.StartupPath);
        }

        private void AdjustMMAccountID(string jobID)
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT j.ConfigID AS ConfigID, j.JobName AS JobName, c.ConfigLevel AS Level FROM MgrJobs j, MgrConfigurations c WHERE j.ID = " + jobID + " AND j.ConfigID = c.ID");

            string jobName = xmlDoc.SelectNodes("//row")[0].Attributes["JobName"].Value;
            string configID = xmlDoc.SelectNodes("//row")[0].Attributes["ConfigID"].Value;
            string configLevel = xmlDoc.SelectNodes("//row")[0].Attributes["Level"].Value;

            int minLength = 0;
            XmlDocument xml = null;

            PreIngest.ConfigManager cMgr = new PreIngest.ConfigManager(dbConnection, configID);
            string[] identifiers = cMgr.GetDocMarshalingIdentifier();
            for (int i = 0; i < identifiers.Length; i++)
            {
                xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT MaxLength FROM CfgDocumentSource WHERE ConfigID = " + configID + " AND Name = '" + identifiers[i] + "'");
                minLength += Convert.ToInt32(xml.SelectNodes("//row")[0].Attributes["MaxLength"].Value);
            }
            if (minLength > 64)
            {
                DBAdapter.DBAdapter.ExecuteSPAdjustMMAccountID(dbConnection, jobName + "_CEDocuments", minLength);
            }
            if (IsAnnotationDataLoaded(jobID))
            {
                xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT MaxLength FROM CfgAnnotationSource WHERE ConfigID = " + configID + " AND Name = '" + cMgr.GetAnnotationAccountIdentifier() + "'");
                minLength = Convert.ToInt32(xml.SelectNodes("//row")[0].Attributes["MaxLength"].Value);
                if (minLength > 64)
                {
                    DBAdapter.DBAdapter.ExecuteSPAdjustMMAccountID(dbConnection, jobName + "_CEAnnotations", minLength);
                }
            }
            if (configLevel == "CASE")
            {
                xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT MaxLength FROM CfgCaseSource WHERE ConfigID = " + configID + " AND Name = '" + cMgr.GetCaseAccountIdentifier() + "'");
                minLength = Convert.ToInt32(xml.SelectNodes("//row")[0].Attributes["MaxLength"].Value);
                if (minLength > 64)
                {
                    DBAdapter.DBAdapter.ExecuteSPAdjustMMAccountID(dbConnection, jobName + "_CECases", minLength);
                }
                if (IsTaskDataLoaded(jobID))
                {
                    xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT MaxLength FROM CfgTaskSource WHERE ConfigID = " + configID + " AND Name = '" + cMgr.GetTaskAccountIdentifier() + "'");
                    minLength = Convert.ToInt32(xml.SelectNodes("//row")[0].Attributes["MaxLength"].Value);
                    if (minLength > 64)
                    {
                        DBAdapter.DBAdapter.ExecuteSPAdjustMMAccountID(dbConnection, jobName + "_CETasks", minLength);
                    }
                }
            }
        }

        private void SetInConversion()
        {
            busyStatus = MigratorStatus.BusyConverting;
            AdjustConvertPageDisplay();
        }

        private void ClearInConversion()
        {
            busyStatus = MigratorStatus.NotBusy;
            AdjustConvertPageDisplay();
        }

        private void AdjustConvertPageDisplay()
        {
            switch (busyStatus)
            {
                case MigratorStatus.BusyLoading:
                    btnExit.Enabled = false;
                    break;

                case MigratorStatus.BusyConverting:
                    btnConvCancel.Enabled = true;
                    btnConvert.Enabled = false;
                    btnExit.Enabled = false;
                    break;

                case MigratorStatus.BusyIngesting:
                    btnExit.Enabled = false;
                    break;

                default:
                    btnConvCancel.Enabled = false;
                    btnConvert.Enabled = true;
                    btnExit.Enabled = true;
                    break;
            }
        }

        private void ResetConversionStatusDisplay()
        {
            convStatus.BeginUpdate();
            convStatus.Items.Clear();
            convStatus.EndUpdate();
        }

        public void DisplayConversionStatus(String status)
        {
            convStatus.BeginUpdate();
            convStatus.Items.Add(status);
            convStatus.EndUpdate();
            convStatus.SelectedItem = convStatus.Items[convStatus.Items.Count - 1];
            Application.DoEvents();
        }

        public void LogConversionTrace(String message)
        {
            if (migManTrace) DisplayConversionStatus(message);
        }
    }
}
