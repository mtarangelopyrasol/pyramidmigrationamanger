﻿namespace MigrationManager
{
    partial class AddBatchDefnDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblBatchDefnName = new System.Windows.Forms.Label();
            this.txtBatchDefnName = new System.Windows.Forms.TextBox();
            this.lblSize = new System.Windows.Forms.Label();
            this.lblCondition = new System.Windows.Forms.Label();
            this.lblSourcePrefix = new System.Windows.Forms.Label();
            this.lblStagingPath = new System.Windows.Forms.Label();
            this.txtSize = new System.Windows.Forms.TextBox();
            this.txtCondition = new System.Windows.Forms.TextBox();
            this.txtSourcePrefix = new System.Windows.Forms.TextBox();
            this.txtStagingPath = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAddBatchDefn = new System.Windows.Forms.Button();
            this.lblAddBatchDefn = new System.Windows.Forms.Label();
            this.ttAddBatchDefn = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // lblBatchDefnName
            // 
            this.lblBatchDefnName.AutoSize = true;
            this.lblBatchDefnName.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBatchDefnName.Location = new System.Drawing.Point(20, 85);
            this.lblBatchDefnName.Name = "lblBatchDefnName";
            this.lblBatchDefnName.Size = new System.Drawing.Size(186, 23);
            this.lblBatchDefnName.TabIndex = 0;
            this.lblBatchDefnName.Text = "Batch Definition Name:";
            // 
            // txtBatchDefnName
            // 
            this.txtBatchDefnName.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBatchDefnName.Location = new System.Drawing.Point(250, 85);
            this.txtBatchDefnName.Name = "txtBatchDefnName";
            this.txtBatchDefnName.Size = new System.Drawing.Size(500, 27);
            this.txtBatchDefnName.TabIndex = 1;
            this.ttAddBatchDefn.SetToolTip(this.txtBatchDefnName, "Enter a name so you can refer to this batch definition");
            // 
            // lblSize
            // 
            this.lblSize.AutoSize = true;
            this.lblSize.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSize.Location = new System.Drawing.Point(20, 155);
            this.lblSize.Name = "lblSize";
            this.lblSize.Size = new System.Drawing.Size(46, 23);
            this.lblSize.TabIndex = 2;
            this.lblSize.Text = "Size:";
            // 
            // lblCondition
            // 
            this.lblCondition.AutoSize = true;
            this.lblCondition.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCondition.Location = new System.Drawing.Point(20, 225);
            this.lblCondition.Name = "lblCondition";
            this.lblCondition.Size = new System.Drawing.Size(88, 23);
            this.lblCondition.TabIndex = 3;
            this.lblCondition.Text = "Condition:";
            // 
            // lblSourcePrefix
            // 
            this.lblSourcePrefix.AutoSize = true;
            this.lblSourcePrefix.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSourcePrefix.Location = new System.Drawing.Point(20, 295);
            this.lblSourcePrefix.Name = "lblSourcePrefix";
            this.lblSourcePrefix.Size = new System.Drawing.Size(116, 23);
            this.lblSourcePrefix.TabIndex = 4;
            this.lblSourcePrefix.Text = "Source Prefix:";
            // 
            // lblStagingPath
            // 
            this.lblStagingPath.AutoSize = true;
            this.lblStagingPath.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStagingPath.Location = new System.Drawing.Point(20, 365);
            this.lblStagingPath.Name = "lblStagingPath";
            this.lblStagingPath.Size = new System.Drawing.Size(111, 23);
            this.lblStagingPath.TabIndex = 5;
            this.lblStagingPath.Text = "Staging Path:";
            // 
            // txtSize
            // 
            this.txtSize.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSize.Location = new System.Drawing.Point(250, 155);
            this.txtSize.Name = "txtSize";
            this.txtSize.Size = new System.Drawing.Size(500, 27);
            this.txtSize.TabIndex = 6;
            this.ttAddBatchDefn.SetToolTip(this.txtSize, "Maximum number of cases that can be prepared for ingestion at a time");
            // 
            // txtCondition
            // 
            this.txtCondition.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCondition.Location = new System.Drawing.Point(250, 225);
            this.txtCondition.Name = "txtCondition";
            this.txtCondition.Size = new System.Drawing.Size(500, 27);
            this.txtCondition.TabIndex = 7;
            this.ttAddBatchDefn.SetToolTip(this.txtCondition, "Enter a condition on the source data. Any valid SQL predicate is allowed");
            // 
            // txtSourcePrefix
            // 
            this.txtSourcePrefix.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSourcePrefix.Location = new System.Drawing.Point(250, 295);
            this.txtSourcePrefix.Name = "txtSourcePrefix";
            this.txtSourcePrefix.Size = new System.Drawing.Size(500, 27);
            this.txtSourcePrefix.TabIndex = 8;
            this.ttAddBatchDefn.SetToolTip(this.txtSourcePrefix, "If your source images need a prefix like a drive letter or a path enter it here");
            // 
            // txtStagingPath
            // 
            this.txtStagingPath.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStagingPath.Location = new System.Drawing.Point(250, 365);
            this.txtStagingPath.Name = "txtStagingPath";
            this.txtStagingPath.Size = new System.Drawing.Size(500, 27);
            this.txtStagingPath.TabIndex = 9;
            this.ttAddBatchDefn.SetToolTip(this.txtStagingPath, "Enter the path to the staging area for images prepared for ingestion");
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(594, 425);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 32);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.ttAddBatchDefn.SetToolTip(this.btnCancel, "Cancel out of this dialog without adding a batch definition");
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAddBatchDefn
            // 
            this.btnAddBatchDefn.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddBatchDefn.Location = new System.Drawing.Point(675, 425);
            this.btnAddBatchDefn.Name = "btnAddBatchDefn";
            this.btnAddBatchDefn.Size = new System.Drawing.Size(75, 32);
            this.btnAddBatchDefn.TabIndex = 11;
            this.btnAddBatchDefn.Text = "OK";
            this.ttAddBatchDefn.SetToolTip(this.btnAddBatchDefn, "Add a new batch definition with content specified in this dialog");
            this.btnAddBatchDefn.UseVisualStyleBackColor = true;
            this.btnAddBatchDefn.Click += new System.EventHandler(this.btnAddBatchDefn_Click);
            // 
            // lblAddBatchDefn
            // 
            this.lblAddBatchDefn.AutoSize = true;
            this.lblAddBatchDefn.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddBatchDefn.Location = new System.Drawing.Point(250, 33);
            this.lblAddBatchDefn.Name = "lblAddBatchDefn";
            this.lblAddBatchDefn.Size = new System.Drawing.Size(204, 23);
            this.lblAddBatchDefn.TabIndex = 12;
            this.lblAddBatchDefn.Text = "Add New Batch Definition";
            // 
            // AddBatchDefnDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(772, 485);
            this.Controls.Add(this.lblAddBatchDefn);
            this.Controls.Add(this.btnAddBatchDefn);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtStagingPath);
            this.Controls.Add(this.txtSourcePrefix);
            this.Controls.Add(this.txtCondition);
            this.Controls.Add(this.txtSize);
            this.Controls.Add(this.lblStagingPath);
            this.Controls.Add(this.lblSourcePrefix);
            this.Controls.Add(this.lblCondition);
            this.Controls.Add(this.lblSize);
            this.Controls.Add(this.txtBatchDefnName);
            this.Controls.Add(this.lblBatchDefnName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddBatchDefnDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add New Batch Definition";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBatchDefnName;
        private System.Windows.Forms.TextBox txtBatchDefnName;
        private System.Windows.Forms.Label lblSize;
        private System.Windows.Forms.Label lblCondition;
        private System.Windows.Forms.Label lblSourcePrefix;
        private System.Windows.Forms.Label lblStagingPath;
        private System.Windows.Forms.TextBox txtSize;
        private System.Windows.Forms.TextBox txtCondition;
        private System.Windows.Forms.TextBox txtSourcePrefix;
        private System.Windows.Forms.TextBox txtStagingPath;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAddBatchDefn;
        private System.Windows.Forms.Label lblAddBatchDefn;
        private System.Windows.Forms.ToolTip ttAddBatchDefn;
    }
}