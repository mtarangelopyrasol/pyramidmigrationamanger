﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        int chosenConversionResetJobIndex = 0;
        int[] chosenResetBatches;
        int chosenResetAccountJobIndex = 0;

        private void ViewConversionResetOptions()
        {
            tabIngestionReset.Hide();
            tabConversionReset.Show();
            tabConversionReset.BringToFront();
            ShowConvResetSubTab();
        }

        private void tabConversionReset_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowConvResetSubTab();
        }

        private void ShowConvResetSubTab()
        {
            switch (tabConversionReset.TabPages[tabConversionReset.SelectedIndex].Name)
            {
                case "JobsForConversionReset":
                    RefreshResetJobsList();
                    break;

                 case "BatchesForConversionReset":
                    RefreshResetBatchesList();
                    break;

                case "AccountForConversionReset":
                    RefreshResetAccountJobsList(chosenResetAccountJobIndex);
                    break;

                default:
                    throw new Exception("Unknown option");
            }
        }

        private void RefreshResetJobsList()
        {
            listConversionResetJobs.BeginUpdate();
            listConversionResetJobs.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM MgrJobs ORDER BY JobName");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    ListViewItem[] items = new ListViewItem[count];

                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = listConversionResetJobs.Items.Add(rowNode.GetAttribute("ID"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("JobName"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Comment"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                    listConversionResetJobs.Items[chosenConversionResetJobIndex].Checked = true;
                    listConversionResetJobs.EnsureVisible(i - 1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Jobs : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                listConversionResetJobs.EndUpdate();
                lblResetJobsConfirm.Text = "";
            }
        }

        private void listResetJobs_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != chosenConversionResetJobIndex))
            {
                listConversionResetJobs.Items[chosenConversionResetJobIndex].Checked = false;
                chosenConversionResetJobIndex = e.Item.Index;
                chosenResetBatches = null;
            }
        }

        private void btnResetJobConvErrors_Click(object sender, EventArgs e)
        {
            string jobID = listConversionResetJobs.Items[chosenConversionResetJobIndex].Tag.ToString();
            string jobName = GetJobName(jobID);

            if (MessageBox.Show("You have chosen to reset errors of the job " + jobName + ". Proceed?", "Confirm job error reset", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                PerformJobErrorReset(jobID, jobName);
            }
        }

        private void btnResetJobs_Click(object sender, EventArgs e)
        {
            string jobID = listConversionResetJobs.Items[chosenConversionResetJobIndex].Tag.ToString();
            string jobName = GetJobName(jobID);

            if (MessageBox.Show("You have chosen to reset statuses of the entire job " + jobName + ". Proceed?", "Confirm job reset", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                PerformJobReset(jobName);
            }
        }

        private void PerformJobReset(string jobName)
        {
            lblResetJobsConfirm.Text = "Resetting...";
            lblResetJobsConfirm.Show();

            btnResetJobs.Enabled = false;
            btnResetJobConvErrors.Enabled = false;

            Boolean caseLevel = IsJobConfigCaseLevel(jobName);

            if (caseLevel)
            {
                DBAdapter.DBAdapter.UpdateDB(dbConnection, "UPDATE " + jobName + "_TaskData" + " SET Status = 'CONV_NEW', BatchID = NULL");
            }
            DBAdapter.DBAdapter.UpdateDB(dbConnection, "UPDATE " + jobName + "_AnnotationData" + " SET Status = 'CONV_NEW', BatchID = NULL");
            DBAdapter.DBAdapter.UpdateDB(dbConnection, "UPDATE " + jobName + "_DocumentData" + " SET Status = 'CONV_NEW', BatchID = NULL");
            if (caseLevel)
            {
                DBAdapter.DBAdapter.UpdateDB(dbConnection, "UPDATE " + jobName + "_CaseData" + " SET Status = 'CONV_NEW', BatchID = NULL");
            }
            lblResetJobsConfirm.Text = "Job " + jobName + " Reset complete";
            lblResetJobsConfirm.Show();

            btnResetJobs.Enabled = true;
            btnResetJobConvErrors.Enabled = true;
        }

        private void PerformJobErrorReset(string jobID, string jobName)
        {
            lblResetJobsConfirm.Text = "Resetting...";
            lblResetJobsConfirm.Show();

            btnResetJobs.Enabled = false;
            btnResetJobConvErrors.Enabled = false;

            PreIngest.ConfigManager cMgr = new PreIngest.ConfigManager(dbConnection, GetConfigID(jobID));

            if (IsJobConfigCaseLevel(jobName))
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT " + cMgr.GetCaseAccountIdentifier() + " AS AccountID FROM " + jobName + "_CaseData WHERE Status IN ('CONV_ERRORED', 'CONV_CANCELED')");

                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    ResetCaseForConversion(jobID, jobName, rowNode.Attributes["AccountID"].Value);
                }
            }
            else
            {
                string[] flds = cMgr.GetDocMarshalingIdentifier();
                string sql = "SELECT ";
                Boolean first = true;
                foreach (string f in flds)
                {
                    if (!first)
                    {
                        sql += " , ";
                    }
                    first = false;
                    sql += f;
                }
                sql +=  " FROM " + jobName + "_DocumentData WHERE Status IN ('CONV_ERRORED', 'CONV_CANCELED')";
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    Dictionary<string, string> map = new Dictionary<string, string>();
                    foreach (XmlAttribute a in rowNode.Attributes)
                    {
                        map.Add(a.Name, a.Value);
                    }
                    ResetDocForConversion(jobID, jobName, map);
                }
            }
            lblResetJobsConfirm.Text = "Job " + jobName + " Error Reset complete";
            lblResetJobsConfirm.Show();

            btnResetJobs.Enabled = true;
            btnResetJobConvErrors.Enabled = true;
        }

        private void RefreshResetBatchesList()
        {
            listResetBatches.BeginUpdate();
            listResetBatches.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT b.ID, j.JobName, b.Status, b.ActualSize, CAST(b.StartTime AS nvarchar(30)) AS StartStamp, CAST(b.EndTime AS nvarchar(30)) AS EndStamp, b.UserName, b.WorkStation FROM MgrBatches b, MgrJobs j WHERE b.JobID = j.ID ORDER BY b.ID DESC");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = listResetBatches.Items.Add(rowNode.GetAttribute("ID"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("JobName"));
                        lvi.SubItems.Add(rowNode.GetAttribute("ActualSize"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Status"));
                        lvi.SubItems.Add(rowNode.GetAttribute("StartStamp"));
                        lvi.SubItems.Add(rowNode.GetAttribute("EndStamp"));
                        lvi.SubItems.Add(rowNode.GetAttribute("UserName"));
                        lvi.SubItems.Add(rowNode.GetAttribute("WorkStation"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        if (chosenResetBatches != null)
                        {
                            for (int j = 0; j < chosenResetBatches.GetLength(0); j++)
                            {
                                if (chosenResetBatches[j].ToString() == rowNode.GetAttribute("ID").Trim())
                                {
                                    lvi.Checked = true;
                                    break;
                                }
                            }
                        }
                        i++;
                    }
                    listResetBatches.EnsureVisible(0);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Batches : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                listResetBatches.EndUpdate();
                lblResetBatchConfirm.Text = "";
            }
        }

        private void listResetBatches_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item.Checked)
            {
                listResetBatches.Items[e.Item.Index].Checked = true;
                listResetBatches.Items[e.Item.Index].Selected = true;
            }
        }

        private void btnConvResetErrors_Click(object sender, EventArgs e)
        {
            CaptureChosenResetBatches();
            if (chosenResetBatches != null)
            {
                string chosenBatchList = GetResetBatchList();
                if (MessageBox.Show("You have chosen to reset errors of the following batches: " + chosenBatchList + ". Proceed?", "Confirm batch error reset", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    PerformBatchReset(true);
                }
            }
            else
            {
                MessageBox.Show("You have not chosen to reset the error status of any batches", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnResetBatches_Click(object sender, EventArgs e)
        {
            CaptureChosenResetBatches();
            if (chosenResetBatches != null)
            {
                string chosenBatchList = GetResetBatchList();
                if (MessageBox.Show("You have chosen to reset statuses of the following batches: " + chosenBatchList + ". Proceed?", "Confirm batch reset", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    PerformBatchReset(false);
                } 
            }
            else
            {
                MessageBox.Show("You have not chosen to reset the status of any batches", "Migration Manager", MessageBoxButtons.OK);
            }
        }
        
        private void CaptureChosenResetBatches()
        {
            ListView.SelectedListViewItemCollection selectedItems = listResetBatches.SelectedItems;
            if (selectedItems.Count > 0)
            {
                chosenResetBatches = new int[selectedItems.Count];
                int i = 0;
                foreach (ListViewItem item in selectedItems)
                {
                    chosenResetBatches[i] = Convert.ToInt32(item.Tag.ToString());
                    i++;
                }
            }
            else
            {
                chosenResetBatches = null;
            }
        }

        private string GetResetBatchList()
        {
            string batchList = string.Empty;
            Boolean first = true;
            foreach (int b in chosenResetBatches)
            {
                if (!first)
                {
                    batchList += ", ";
                }
                else
                {
                    first = false;
                }
                batchList += b.ToString();
            }
            return batchList;
        }

        private void PerformBatchReset(Boolean errorsOnly)
        {
            btnResetBatches.Enabled = false;

            foreach (int b in chosenResetBatches)
            {
                try
                {
                    Dictionary<string, string> jobDetails = GetJobDetailsForConversionBatch(b.ToString());
                    string jobName = jobDetails["Name"];
                    string jobID = jobDetails["ID"];
                    Boolean caseLevel = IsJobConfigCaseLevel(jobName);
                    if (errorsOnly)
                    {
                        PreIngest.ConfigManager cMgr = new PreIngest.ConfigManager(dbConnection, GetConfigID(jobID));

                        if (caseLevel)
                        {
                            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT " + cMgr.GetCaseAccountIdentifier() + " AS AccountID FROM " + jobName + "_CaseData WHERE Status IN ('CONV_ERRORED', 'CONV_CANCELED') AND BatchID = " + b.ToString());

                            foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                            {
                                string chosenAccount = rowNode.Attributes["AccountID"].Value;
                                ResetCaseForConversion(jobDetails["ID"], jobName, chosenAccount);
                            }
                        }
                        else
                        {
                            string[] flds = cMgr.GetDocMarshalingIdentifier();
                            string sql = "SELECT ";
                            Boolean first = true;
                            foreach (string f in flds)
                            {
                                if (!first)
                                {
                                    sql += " , ";
                                }
                                first = false;
                                sql += f;
                            }
                            sql += " FROM " + jobName + "_DocumentData WHERE Status IN ('CONV_ERRORED', 'CONV_CANCELED') AND BatchID = " + b.ToString();
                            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

                            foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                            {
                                Dictionary<string, string> map = new Dictionary<string, string>();
                                foreach (XmlAttribute a in rowNode.Attributes)
                                {
                                    map.Add(a.Name, a.Value);
                                }
                                ResetDocForConversion(jobID, jobName, map);
                            }
                        }
                    }
                    else
                    {
                        if (caseLevel)
                        {
                            DBAdapter.DBAdapter.UpdateDB(dbConnection,
                                "UPDATE " + jobName + "_TaskData" + " SET Status = 'CONV_NEW', BatchID = NULL WHERE BatchID = " + b.ToString());
                        }
                        DBAdapter.DBAdapter.UpdateDB(dbConnection,
                            "UPDATE " + jobName + "_AnnotationData" + " SET Status = 'CONV_NEW', BatchID = NULL WHERE BatchID = " + b.ToString());
                        DBAdapter.DBAdapter.UpdateDB(dbConnection,
                            "UPDATE " + jobName + "_DocumentData" + " SET Status = 'CONV_NEW', BatchID = NULL WHERE BatchID = " + b.ToString());
                        if (caseLevel)
                        {
                            DBAdapter.DBAdapter.UpdateDB(dbConnection,
                                "UPDATE " + jobName + "_CaseData" + " SET Status = 'CONV_NEW', BatchID = NULL WHERE BatchID = " + b.ToString());
                        }
                    }
                    lblResetBatchConfirm.Text = "Reset complete";
                    lblResetBatchConfirm.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Reset Batch " + b.ToString() + ": " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
                }
            }
            btnResetBatches.Enabled = true;
        }

        private Dictionary<string, string> GetJobDetailsForConversionBatch(string batchID)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT j.ID, j.JobName FROM MgrBatches b, MgrJobs j WHERE b.ID = " + batchID + " AND b.JobID = j.ID");
            Dictionary<string, string> jobDetails = new Dictionary<string, string>();
            XmlAttributeCollection xac = xml.SelectNodes("//row")[0].Attributes;
            jobDetails.Add("ID", xac["ID"].Value);
            jobDetails.Add("Name", xac["JobName"].Value);
            return jobDetails;
        }

        private void btnResetAccount_Click(object sender, EventArgs e)
        {
            string chosenAccount = txtResetAccount.Text.Trim();
            string jobID = listResetAccountJobs.Items[chosenResetAccountJobIndex].Tag.ToString();
            string jobName = GetJobName(jobID);
            Boolean caseLevel = IsJobConfigCaseLevel(jobName);

            if (chosenAccount.Length > 0)
            {
                if (caseLevel)
                {
                    ResetCaseForConversion(jobID, jobName, chosenAccount);
                    lblResetAccountConfirm.Text = "Case " + chosenAccount + " Reset complete";
                }
                else
                {
                    Dictionary<string, string> map = new Dictionary<string, string>();
                    PreIngest.ConfigManager cMgr = new PreIngest.ConfigManager(dbConnection, GetConfigID(jobID));
                    map.Add(cMgr.GetDocumentIdentifier(), chosenAccount);
                    ResetDocForConversion(jobID, jobName, map);
                    lblResetAccountConfirm.Text = "Document " + chosenAccount + " Reset complete";
                }
                lblResetAccountConfirm.Show();
            }
            else
            {
                if(caseLevel)
                    MessageBox.Show("Specify an Account to reset", "Migration Manager", MessageBoxButtons.OK);
                else
                    MessageBox.Show("Specify a Document ID to reset", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void RefreshResetAccountJobsList(int chosenResetAccountJobIndex)
        {
            listResetAccountJobs.BeginUpdate();
            listResetAccountJobs.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM MgrJobs ORDER BY JobName");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    ListViewItem[] items = new ListViewItem[count];

                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = listResetAccountJobs.Items.Add(rowNode.GetAttribute("ID"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("JobName"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Comment"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                    listResetAccountJobs.Items[chosenResetAccountJobIndex].Checked = true;
                    UpdateConversionByAccountDisplay();
                    listResetAccountJobs.EnsureVisible(i - 1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Jobs : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                listResetAccountJobs.EndUpdate();
                lblResetAccountConfirm.Text = "";
            }
        }

        private void listResetAccountJobs_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != chosenResetAccountJobIndex))
            {
                listResetAccountJobs.Items[chosenResetAccountJobIndex].Checked = false;
                chosenResetAccountJobIndex = e.Item.Index;
                UpdateConversionByAccountDisplay();
            }
        }

        private void UpdateConversionByAccountDisplay()
        {
            string jobName = GetJobName(listResetAccountJobs.Items[chosenResetAccountJobIndex].Tag.ToString());
            Boolean caseLevel = IsJobConfigCaseLevel(jobName);
            if (caseLevel)
            {
                lblResetAccount.Text = "Account Number:";
                btnResetAccount.Text = "Reset Case";
            }
            else
            {
                lblResetAccount.Text = "Doc. Identifier:";
                btnResetAccount.Text = "Reset";
            }
        }

        private void ResetCaseForConversion(string jobID, string jobName, string accountID)
        {
            PreIngest.ConfigManager cMgr = new PreIngest.ConfigManager(dbConnection, GetConfigID(jobID));

            if (IsTaskDataLoaded(jobID))
            {    
                DBAdapter.DBAdapter.UpdateDB(dbConnection,
                            "UPDATE " + jobName + "_TaskData" + " SET Status = 'CONV_NEW', BatchID = NULL WHERE " + cMgr.GetTaskAccountIdentifier() + " = '" + accountID + "'");
            }

            if (IsAnnotationDataLoaded(jobID))
            {
                DBAdapter.DBAdapter.UpdateDB(dbConnection,
                    "UPDATE " + jobName + "_AnnotationData" + " SET Status = 'CONV_NEW', BatchID = NULL WHERE " + cMgr.GetAnnotationAccountIdentifier() + " = '" + accountID + "'");
            }

            if (IsDocumentDataLoaded(jobID))
            {
                DBAdapter.DBAdapter.UpdateDB(dbConnection,
                    "UPDATE " + jobName + "_DocumentData" + " SET Status = 'CONV_NEW', BatchID = NULL WHERE " + cMgr.GetDocAccountIdentifier() + " = '" + accountID + "'");
            }

            if (IsCaseDataLoaded(jobID))
            {
                DBAdapter.DBAdapter.UpdateDB(dbConnection,
                    "UPDATE " + jobName + "_CaseData" + " SET Status = 'CONV_NEW' WHERE " + cMgr.GetCaseAccountIdentifier() + " = '" + accountID + "'");
            }
        }

        private void ResetDocForConversion(string jobID, string jobName, Dictionary<string, string>map)
        {
            string condition = string.Empty;
            Boolean first = true;
            foreach (string k in map.Keys)
            {
                if (!first)
                {
                    condition += " AND ";
                }
                first = false;
                condition += k;
                condition += " = '";
                condition += map[k];
                condition += "'";
            }
            DBAdapter.DBAdapter.UpdateDB(dbConnection, "UPDATE " + jobName + "_DocumentData" + " SET Status = 'CONV_NEW', BatchID = NULL WHERE " + condition);

            PreIngest.ConfigManager cMgr = new PreIngest.ConfigManager(dbConnection, GetConfigID(jobID));

            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT " + cMgr.GetPageIdentifier() + " AS PageID FROM " + jobName + "_DocumentData WHERE " + condition);

            if (cMgr.GetAnnotationPageIdentifier() != String.Empty)
            {
                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    string page = rowNode.Attributes["PageID"].Value;
                    DBAdapter.DBAdapter.UpdateDB(dbConnection,
                        "UPDATE " + jobName + "_AnnotationData" + " SET Status = 'CONV_NEW', BatchID = NULL WHERE " + cMgr.GetAnnotationPageIdentifier() + " = '" + page + "'");
                }
            }
        }

        private Boolean IsJobConfigCaseLevel(string jobName)
        {
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT c.ConfigLevel AS Level FROM MgrJobs j, MgrConfigurations c WHERE j.JobName = '" + jobName + "' AND j.ConfigID = c.ID");
                string level = xml.SelectNodes("//row")[0].Attributes["Level"].Value;
                return (level == "CASE") ? true : false;
            }
            catch (Exception ex)
            {
                throw new Exception("IsJobConfigCaseLevel: Exception while checking Job's config level : " + ex.Message);
            }
        }
    }
}
