﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager : Form
    {
        static Regex rgxSingleQuote = new Regex("'");
        const string repSingleQuote = "''";
        static Regex rgxSpace = new Regex(" ");
        const string repSpace = "_";
        static Dictionary<string, TabPage> pageHold = new Dictionary<string, TabPage>();
        static string importDirectory = "C:\\";

        public static Label CreateLabel(string name, string text, Point location, Size size)
        {
            Label label = new Label();
            label.AutoSize = true;
            label.BackColor = SystemColors.Control;
            label.Font = new Font("Trebuchet MS", 10.2F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
            label.Name = name;
            label.Text = text;
            label.Location = location;
            label.Size = size;
            return label;
        }

        public static void RefreshListView(ListView lv, string dbCnxn, string sql, string[] fields, string errorMessage)
        {
            int nColumns = fields.Length;

            lv.BeginUpdate();
            lv.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = lv.Items.Add(rowNode.GetAttribute(fields[0]), i);
                        for (int j = 1; j < nColumns; j++)
                        {
                            lvi.SubItems.Add(rowNode.GetAttribute(fields[j]));
                        }
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception handling " + errorMessage + ": " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                lv.EndUpdate();
            }
        }

        public static void ClearDisplay(Label label, ListView lv)
        {
            label.Text = string.Empty;
            lv.BeginUpdate();
            lv.Items.Clear();
            lv.EndUpdate();
        }

        public static string GetAttributeValue(XmlNode node, string attributeName)
        {
            string result = string.Empty;
            if (node.Attributes[attributeName] != null)
            {
                result = node.Attributes[attributeName].Value;
            }
            return result;
        }

        public static string EscapeSingleQuote(string input)
        {
            return rgxSingleQuote.Replace(input, repSingleQuote);
        }

        private static void HideTabPage(TabControl tc, string pageName)
        {
            TabPage tp = tc.TabPages[pageName];
            if (tp != null)
            {
                if (tc.TabPages.Contains(tp))
                {
                    if (!pageHold.ContainsKey(pageName))
                    {
                        pageHold.Add(pageName, tp);
                    }
                    tc.TabPages.Remove(tp);
                }
            }
        }

        private static void ShowTabPage(TabControl tc, string pageName, int index) 
        {
            if (!tc.TabPages.ContainsKey(pageName))
            {
                if (pageHold.ContainsKey(pageName))
                {
                    InsertTabPage(tc, pageHold[pageName], index);
                }
            }
        }
        
        private static void InsertTabPage(TabControl tc, TabPage tp, int index) 
        { 
            if (index < 0 || index > tc.TabCount)
                throw new ArgumentException("Index out of Range.");
 
            tc.TabPages.Add(tp);
            if (index < tc.TabCount - 1)
            {
                do
                {
                    SwapTabPages(tc, tp, (tc.TabPages[tc.TabPages.IndexOf(tp) - 1]));
                } while (tc.TabPages.IndexOf(tp) != index);
            } 
        }

        private static void SwapTabPages(TabControl tc, TabPage tp1, TabPage tp2)
        {    
            if (tc.TabPages.Contains(tp1) == false || tc.TabPages.Contains(tp2) == false)        
                throw new ArgumentException("TabPages must be in the TabControls TabPageCollection.");               
            
            int Index1 = tc.TabPages.IndexOf(tp1);    
            int Index2 = tc.TabPages.IndexOf(tp2);    
            tc.TabPages[Index1] = tp2;    
            tc.TabPages[Index2] = tp1;    
        }

        private static Boolean IsOKThatJobsExist(string dbCnxn, string configID)
        {
            string sql = "SELECT * FROM MgrJobs WHERE ConfigID = " + configID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);

            int count = xml.SelectNodes("//row").Count;
            if (count > 0)
            {
                return (MessageBox.Show("There are jobs that depend on the current definition of this configuration. These jobs can become unusable. Do you still want to proceed with changes to this configuration?", "Confirm Configuration Modification", MessageBoxButtons.YesNo) == DialogResult.Yes) ? true : false;
            }
            return true;
        }

        private static Boolean IsASource(string dbCnxn, string configID, string tableName, string fieldName, string idPrefix)
        {
            string sql = "SELECT Name FROM " + tableName + " WHERE Source = '" + fieldName + "' AND ConfigID = " + configID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);

            int count = xml.SelectNodes("//row").Count;
            if (count > 0)
            {
                MessageBox.Show(fieldName + " is the source for target properties in the current definition of this configuration. It cannot be deleted.", "Is A Source", MessageBoxButtons.OK);
                return true;
            }
            return IsAnIdentifier(dbCnxn, configID, fieldName, idPrefix);
        }

        private static Boolean IsAnIdentifier(string dbCnxn, string configID, string fieldName, string idPrefix)
        {
            string sql = "SELECT IdentifierName FROM CfgIdentifiers WHERE FieldName = '" + fieldName + "' AND ConfigID = " + configID + " AND IdentifierName LIKE '" + idPrefix + "%'";
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);

            int count = xml.SelectNodes("//row").Count;
            if (count > 0)
            {
                MessageBox.Show(fieldName + " is an identifier in the current definition of this configuration. It cannot be deleted.", "Is An Identifier", MessageBoxButtons.OK);
                return true;
            }
            return false;
        }

        private static int DeleteCaseProperties(string dbCnxn, string configID, string caseTypeID)
        {
            string sql = "DELETE CfgCaseProperties WHERE CaseTypeID = " + caseTypeID + " AND ConfigID = " + configID;
            return DBAdapter.DBAdapter.DeleteDB(dbCnxn, sql);
        }

        private static int DeleteCaseFolders(string dbCnxn, string configID, string caseTypeID)
        {
            string sql = "DELETE CfgCaseFolders WHERE CaseTypeID = " + caseTypeID + " AND ConfigID = " + configID;
            return DBAdapter.DBAdapter.DeleteDB(dbCnxn, sql);
        }

        private static int DeleteDocClassProperties(string dbCnxn, string configID, string docClassID)
        {
            string sql = "DELETE CfgDocClassProperties WHERE DocClassID = " + docClassID + " AND ConfigID = " + configID;
            return DBAdapter.DBAdapter.DeleteDB(dbCnxn, sql);
        }

        private static int DeleteAnnClassProperties(string dbCnxn, string configID, string annClassID)
        {
            string sql = "DELETE CfgAnnClassProperties WHERE AnnClassID = " + annClassID + " AND ConfigID = " + configID;
            return DBAdapter.DBAdapter.DeleteDB(dbCnxn, sql);
        }

        private static int DeleteTaskProperties(string dbCnxn, string configID, string taskTypeID)
        {
            string sql = "DELETE CfgTaskProperties WHERE TaskTypeID = " + taskTypeID + " AND ConfigID = " + configID;
            return DBAdapter.DBAdapter.DeleteDB(dbCnxn, sql);
        }

        private static int DeleteRecord(string dbCnxn, string tableName, string id)
        {
            string sql = "DELETE " + tableName + " WHERE ID = " + id;
            return DBAdapter.DBAdapter.DeleteDB(dbCnxn, sql);
        }

        public static Boolean IsMappingUsed(string dbCnxn, string mappingName)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT * FROM CfgCaseProperties WHERE MappingName = '" + mappingName + "'");
            if (xml.SelectNodes("//row").Count > 0)
            {
                return true;
            }
            else
            {
                xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT * FROM CfgDocClassProperties WHERE MappingName = '" + mappingName + "'");
                if (xml.SelectNodes("//row").Count > 0)
                {
                    return true;
                }
                else
                {
                    xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, "SELECT * FROM CfgTaskProperties WHERE MappingName = '" + mappingName + "'");
                    if (xml.SelectNodes("//row").Count > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static string GetSourceFilePath(string desc)
        {
            string srcFilePath = string.Empty;

            OpenFileDialog dlgOpen = new OpenFileDialog();

            dlgOpen.InitialDirectory = importDirectory;
            dlgOpen.Filter = ".xml files (*.xml)|*.xml";
            dlgOpen.FilterIndex = 1;
            dlgOpen.RestoreDirectory = true;
            dlgOpen.Multiselect = false;
            dlgOpen.SupportMultiDottedExtensions = true;
            dlgOpen.Title = desc;

            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                srcFilePath = dlgOpen.FileName;
                importDirectory = Path.GetDirectoryName(dlgOpen.FileName);
            }
            return srcFilePath;
        }

        public static string GetDestinationFolder(string desc)
        {
            string destFolder = string.Empty;

            FolderBrowserDialog dlgBrowse = new FolderBrowserDialog();

            dlgBrowse.ShowNewFolderButton = false;
            dlgBrowse.Description = desc;
            dlgBrowse.RootFolder = Environment.SpecialFolder.MyComputer;

            if (dlgBrowse.ShowDialog() == DialogResult.OK)
            {
                destFolder = dlgBrowse.SelectedPath;
            }
            return destFolder;
        }

        public static string PrepareArtifactName(string name)
        {
            return rgxSpace.Replace(name.Trim(), repSpace);
        }

        public static string FormulateExportFileName(string folder, string artifactType, string artifactName)
        {
            string exportFileName = string.Empty;

            string fileName = artifactType + "_" + PrepareArtifactName(artifactName) + ".xml";

            string lastChar = folder.Substring(folder.Length - 1);
            string pathName = (lastChar == "\\") ? folder : folder + "\\";
            pathName = pathName + fileName;

            FileInfo fi = new FileInfo(pathName);
            if (fi.Exists)
            {
                DialogResult result = MessageBox.Show("File: " + pathName + " already exists. Overwrite?", "Output File Already Exists", MessageBoxButtons.YesNo);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    exportFileName = pathName;
                }
            }
            else
            {
                exportFileName = pathName;
            }
            return exportFileName;
        }

        private static XmlTextWriter StartExport(string exportFile)
        {
            try
            {
                XmlTextWriter xtw = new XmlTextWriter(exportFile, Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                xtw.IndentChar = ' ';
                xtw.Indentation = 4;
                xtw.WriteStartDocument();
                xtw.WriteStartElement("mmExport");
                xtw.WriteAttributeString("mmVersion", versionInfo);
                xtw.WriteAttributeString("mmExportDate", DateTime.Now.ToString());
                return xtw;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to export to " + exportFile + ": " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
                return null;
            }
        }

        private static void EndExport(XmlTextWriter xtw, string exportFile)
        {
            xtw.WriteEndElement();
            xtw.WriteEndDocument();
            xtw.Flush();
            xtw.Close();

            MessageBox.Show("Exported content is in " + exportFile, "Migration Manager", MessageBoxButtons.OK);
        }

        private void ExportSQLResult(string collectionName, string tagName, string sql, XmlTextWriter xtw)
        {
            xtw.WriteStartElement(collectionName);

            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            foreach (XmlElement row in xml.SelectNodes("//row"))
            {
                xtw.WriteStartElement(tagName);
                foreach (XmlAttribute xa in row.Attributes)
                {
                    xtw.WriteAttributeString(xa.Name, xa.Value);
                }
                xtw.WriteEndElement();
            }
            xtw.WriteEndElement();
        }

        private XmlDocument GetImportXml()
        {
            XmlDocument xmlDoc = null;

            string fileToImport = GetSourceFilePath("File to Import");
            if (fileToImport.Length > 0)
            {
                xmlDoc = new XmlDocument();
                try
                {
                    xmlDoc.Load(fileToImport);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Invalid export file: " + fileToImport + " - " + ex.Message, "Bad File Specified", MessageBoxButtons.OK);
                }
            }
            return xmlDoc;
        }

        private string GetAttributeValue(XmlElement row, string attName)
        {
            return row.GetAttribute(attName).Trim();
        }
    }
}
