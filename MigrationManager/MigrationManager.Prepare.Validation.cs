﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        private Boolean IsValidJob(int jobID)
        {
            return IsValidConfiguration(jobID) && IsValidConversion(jobID);
        }

        private Boolean IsValidConfiguration(int jobID)
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT j.ConfigID AS ConfigID, c.ConfigLevel AS Level FROM MgrJobs j, MgrConfigurations c WHERE j.ID = " + jobID + " AND j.ConfigID = c.ID");

            string job = jobID.ToString();
            string configID = xmlDoc.SelectNodes("//row")[0].Attributes["ConfigID"].Value;
            string configLevel = xmlDoc.SelectNodes("//row")[0].Attributes["Level"].Value;

            return AreSourceFieldsFreeOfReservedNames(configID, configLevel) &&
                    AreTargetsDefined(job, configID, configLevel) &&
                    NeededIdentifiersExist(job, configID, configLevel) &&
                    AreIdentifiersConsistent(job, configID, configLevel) &&
                    AreAllIdentifiersSingleValued(job, configID, configLevel) &&
                    AreSrcAndTgtConsistent(job, configID, configLevel) &&
                    AreAllInitiatingDocClassesDefined(job, configID, configLevel);

            // TO DO: check for Document Title, Annotated Object - warnings
        }

        private Boolean IsValidConversion(int jobID)
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT j.ConvDefnID AS ConvID, cd.MarshalToSingleDoc AS Single FROM MgrJobs j, MgrConvDefns cd WHERE j.ID = " + jobID + " AND j.ConvDefnID = cd.ID");

            string job = jobID.ToString();
            string convDefnID = xmlDoc.SelectNodes("//row")[0].Attributes["ConvID"].Value;
            string marshalToSingle = xmlDoc.SelectNodes("//row")[0].Attributes["Single"].Value;

            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT TargetFormat, Exempt FROM MgrConversions c, MgrConvMaps cm WHERE c.ConvDefnID = " + convDefnID + " AND c.ConvMapID = cm.ID"); 

            Boolean result = true;
            foreach(XmlElement row in xml.SelectNodes("//row"))
            {
                string targetFormat = row.Attributes["TargetFormat"].Value;
                string exempt = row.Attributes["Exempt"].Value;
                if (exempt == "0")
                {
                    if (targetFormat.ToUpper() == "TXT")
                    {
                        MessageBox.Show("The TXT file format does not support progressive loads and saves, so must be marked marshaling exempt.", "Migration Manager", MessageBoxButtons.OK);
                        result = false;
                        break;
                    }
                    if ((targetFormat.ToUpper() == "DOC") || (targetFormat.ToUpper() == "DOCX"))
                    {
                        MessageBox.Show("Currently a target format of " + targetFormat + " is not supported.", "Migration Manager", MessageBoxButtons.OK);
                        result = false;
                        break;
                    }
                }
            }
            return result;
        }

        private Boolean NeededIdentifiersExist(string jobID, string configID, string configLevel)
        {
            List<string> neededList = new List<string>();

            if (configLevel == "CASE")
            {
                if(IsCaseDataLoaded(jobID))
                {
                    neededList.Add("CaseAccountIdentifier");
                    if (IsCaseTypeIdentifierNeeded(configID))
                    {
                        neededList.Add("CaseTypeIdentifier");
                    }
                }
                if(IsDocumentDataLoaded(jobID))
                {
                    neededList.Add("DocAccountIdentifier");
                    neededList.Add("DocMarshalingIdentifier");
                    neededList.Add("DocPageIdentifier");
                    if (IsDocClassIdentifierNeeded(configID, configLevel))
                    {
                        neededList.Add("DocClassIdentifier");
                    }
                }
                if (IsTaskDataLoaded(jobID))
                {
                    neededList.Add("TaskAccountIdentifier");
                    if (IsTaskTypeIdentifierNeeded(configID))
                    {
                        neededList.Add("TaskTypeIdentifier");
                    }
                }
                if (IsAnnotationDataLoaded(jobID))
                {
                    neededList.Add("AnnotationAccountIdentifier");
                    neededList.Add("AnnotatedObjectIdentifier");
                    neededList.Add("AnnotatedObjTypeIdentifier");
                    neededList.Add("AnnotationPageIdentifier");
                    if (IsAnnClassIdentifierNeeded(configID))
                    {
                        neededList.Add("AnnotationClassIdentifier");
                    }
                }
            }
            else
            {
                if (IsDocumentDataLoaded(jobID))
                {
                    neededList.Add("DocumentIdentifier");
                    neededList.Add("DocMarshalingIdentifier");
                    neededList.Add("DocPageFileNameIdentifier");
                    neededList.Add("DocContNameIdentifier");
                    neededList.Add("DocPageIdentifier");
                    if (IsDocClassIdentifierNeeded(configID, configLevel))
                    {
                        neededList.Add("DocClassIdentifier");
                    }
                }
                if (IsAnnotationDataLoaded(jobID))
                {
                    neededList.Add("AnnotationAccountIdentifier");
                    neededList.Add("AnnotatedObjectIdentifier");
                    neededList.Add("AnnotatedObjTypeIdentifier");
                    neededList.Add("AnnotationPageIdentifier");
                    if (IsAnnClassIdentifierNeeded(configID))
                    {
                        neededList.Add("AnnotationClassIdentifier");
                    }
                }
            }
            
            foreach(string neededIdentifier in neededList)
            {
                if (!IsIdentifierDefined(configID, neededIdentifier))
                {
                    MessageBox.Show("Identifier " + neededIdentifier + " is not defined in the configuration. Please define it before attempting conversion", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
                else
                {
                    LogConversionTrace(neededIdentifier + " is defined");
                }
            }
            return true;
        }

        private Boolean AreIdentifiersConsistent(string jobID, string configID, string configLevel)
        {
            if (IsDocumentDataLoaded(jobID))
            {
                string baseIdentifier = (configLevel == "CASE") ? "DocAccountIdentifier" : "DocumentIdentifier";

                string baseField = string.Empty;
                string marshalID = string.Empty;

                string sql = "SELECT IdentifierName, FieldName FROM CfgIdentifiers WHERE ConfigID = " + configID + " AND IdentifierName IN ('" + baseIdentifier + "', 'DocMarshalingIdentifier')";
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

                foreach (XmlElement row in xml.SelectNodes("//row"))
                {
                    if (row.Attributes["IdentifierName"].Value == baseIdentifier)
                    {
                        baseField = row.Attributes["FieldName"].Value;
                    }
                    else
                    {
                        marshalID = row.Attributes["FieldName"].Value;
                    }
                }
                string[] parts = marshalID.Split('+');
                Boolean found = false;
                foreach (string part in parts)
                {
                    if (part.Trim() == baseField)
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    string msg = (configLevel == "CASE") ? "The Document Account Identifier " : "The Document Identifier ";
                    MessageBox.Show(msg + baseField + " must necessarily be part of the Marshaling Identifier", "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
            }
            return true;
        }

        private Boolean AreSourceFieldsFreeOfReservedNames(string configID, string configLevel)
        {
            Boolean result = CheckTableForReservedNames(configID, "CfgDocumentSource", "Document Source Definition") &&
                         CheckTableForReservedNames(configID, "CfgAnnotationSource", "Annotation Source Definition");

            if (result && (configLevel == "CASE"))
            {
                result = CheckTableForReservedNames(configID, "CfgCaseSource", "Case Source Definition") &&
                         CheckTableForReservedNames(configID, "CfgTaskSource", "Task Source Definition");
            }
            return result;
        }

        private Boolean CheckTableForReservedNames(string configID, string tableName, string msg)
        {
            string sql = "SELECT Name FROM " + tableName + " WHERE Name IN ('ID', 'BatchID', 'Status') AND ConfigID = " + configID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            if (xml.SelectNodes("//row").Count > 0)
            {
                MessageBox.Show("The " + msg + " has columns with reserved names ID, BatchID and Status. Please rename and try again", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            return true;
        }

        private Boolean AreAllIdentifiersSingleValued(string jobID, string configID, string configLevel)
        {
            if (configLevel == "CASE")
            {
                return AreIdentifiersSingleValued(jobID, "Cas", configID) &&
                        AreIdentifiersSingleValued(jobID, "Doc", configID) &&
                        AreIdentifiersSingleValued(jobID, "Ann", configID) &&
                        AreIdentifiersSingleValued(jobID, "Tas", configID);
            }
            else
            {
                return AreIdentifiersSingleValued(jobID, "Doc", configID) &&
                        AreIdentifiersSingleValued(jobID, "Ann", configID);
            }
        }

        private Boolean AreIdentifiersSingleValued(string jobID, string prefix, string configID)
        {
            string tableName = string.Empty;
            switch (prefix)
            {
                case "Cas":
                    tableName = (IsCaseDataLoaded(jobID)) ? "CfgCaseSource" : string.Empty;;
                    break;

                case "Doc":
                    tableName = (IsDocumentDataLoaded(jobID)) ? "CfgDocumentSource" : string.Empty;
                    break;

                case "Ann":
                    tableName = (IsAnnotationDataLoaded(jobID)) ? "CfgAnnotationSource" : string.Empty;
                    break;

                case "Tas":
                    tableName = (IsTaskDataLoaded(jobID)) ? "CfgTaskSource" : string.Empty;
                    break;

                default:
                    throw new ArgumentException("AreIdentifiersMultiValued: Invalid prefix - " + prefix);
            }

            if (tableName.Length > 0)
            {
                string sql = "SELECT s.Name, i.IdentifierName FROM " + tableName + " s, CfgIdentifiers i WHERE s.ConfigID = " + configID + " AND s.MultiValued = 'Y' AND s.Name = i.FieldName AND i.ConfigID = " + configID + " AND i.IdentifierName LIKE '" + prefix + "%'";
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

                foreach (XmlElement row in xml.SelectNodes("//row"))
                {
                    MessageBox.Show(row.Attributes["Name"].Value + " is multi-valued and hence cannot be identifier " + row.Attributes["IdentifierName"].Value, "Migration Manager", MessageBoxButtons.OK);
                    return false;
                }
            }
            return true;
        }

        private Boolean IsCaseTypeIdentifierNeeded(string configID)
        {
            Boolean result = false;

            if (MMFactory.MMFactory.Instance.GetCaseTypeSelector() == null)
            {
                XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM CfgCaseTypes WHERE ConfigID = " + configID);
                if (xmlDoc.SelectNodes("//row").Count > 1)
                {
                    result = true;
                }
            }
            return result;
        }

        private Boolean IsDocClassIdentifierNeeded(string configID, string configLevel)
        {
            Boolean result = false;

            if (MMFactory.MMFactory.Instance.GetDocumentTypeSelector() == null)
            {
                XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM CfgDocClasses WHERE ConfigID = " + configID);
                if (xmlDoc.SelectNodes("//row").Count > 1)
                {
                    if(configLevel != "CASE")
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        private Boolean IsAnnClassIdentifierNeeded(string configID)
        {
            Boolean result = false;

            if (MMFactory.MMFactory.Instance.GetAnnotationTypeSelector() == null)
            {
                XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM CfgAnnClasses WHERE ConfigID = " + configID);
                if (xmlDoc.SelectNodes("//row").Count > 1)
                {
                    result = true;
                }
            }
            return result;
        }

        private Boolean IsTaskTypeIdentifierNeeded(string configID)
        {
            Boolean result = false;

            if (MMFactory.MMFactory.Instance.GetTaskTypeSelector() == null)
            {
                XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT tt.* FROM CfgTaskTypes tt, CfgCaseTypes ct WHERE tt.CaseID = ct.ID AND ct.ConfigID = " + configID);
                if (xmlDoc.SelectNodes("//row").Count > 1)
                {
                    result = true;
                }
            }
            return result;
        }

        private Boolean IsIdentifierDefined(string configID, string identifier)
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT FieldName FROM CfgIdentifiers WHERE ConfigID = " + configID + " AND IdentifierName = '" + identifier + "'");
            if (xmlDoc.SelectNodes("//row").Count > 0)
            {
                XmlAttribute xa = xmlDoc.SelectNodes("//row")[0].Attributes["FieldName"];
                if(xa != null)
                {
                    string fieldName = xa.Value.Trim();
                    return (fieldName.Length > 0) ? true : false;
                }
            }
            return false;
        }

        private Boolean IsCaseDataLoaded(string jobID)
        {
            return (GetRecordCount(GetJobName(jobID) + "_CaseData") > 0) ? true : false;
        }

        private Boolean IsDocumentDataLoaded(string jobID)
        {
            return (GetRecordCount(GetJobName(jobID) + "_DocumentData") > 0) ? true : false;
        }

        private Boolean IsAnnotationDataLoaded(string jobID)
        {
            return (GetRecordCount(GetJobName(jobID) + "_AnnotationData") > 0) ? true : false;
        }

        private Boolean IsTaskDataLoaded(string jobID)
        {
            return (GetRecordCount(GetJobName(jobID) + "_TaskData") > 0) ? true : false;
        }

        private int GetRecordCount(string tableName)
        {
            int count = 0;

            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT COUNT(*) AS Result FROM " + tableName);

            count = Convert.ToInt32(xmlDoc.SelectNodes("//row")[0].Attributes["Result"].Value);
            return count;
        }

        private Boolean AreSrcAndTgtConsistent(string jobID, string configID, string configLevel)
        {
            Boolean result = IsDocSrcAndTgtConsistent(jobID, configID) && IsAnnSrcAndTgtConsistent(jobID, configID);
            if ((result) && (configLevel == "CASE"))
            {
                result = IsCaseSrcAndTgtConsistent(jobID, configID) && IsTaskSrcAndTgtConsistent(jobID, configID);
            }
            return result;
        }

        private Boolean IsCaseSrcAndTgtConsistent(string jobID, string configID)
        {
            Boolean result = true;
            if (IsCaseDataLoaded(jobID))
            {
                string sql = "SELECT cp.Name, cp.Source, cp.DataType, ct.CaseName AS ObjectName FROM CfgCaseProperties cp, CfgCaseTypes ct WHERE cp.ConfigID = " + configID + " AND cp.CaseTypeID = ct.ID";
                result = IsSrcAndTgtConsistent(configID, sql, "CfgCaseSource", "Case ");
            }
            return result;
        }

        private Boolean IsDocSrcAndTgtConsistent(string jobID, string configID)
        {
            Boolean result = true;
            if (IsDocumentDataLoaded(jobID))
            {
                string sql = "SELECT cp.Name, cp.Source, cp.DataType, ct.DocClassName AS ObjectName FROM CfgDocClassProperties cp, CfgDocClasses ct WHERE cp.ConfigID = " + configID + " AND cp.DocClassID = ct.ID";
                result = IsSrcAndTgtConsistent(configID, sql, "CfgDocumentSource", "Document Class "); 
            }
            return result;
        }

        private Boolean IsAnnSrcAndTgtConsistent(string jobID, string configID)
        {
            Boolean result = true;
            if (IsAnnotationDataLoaded(jobID))
            {
                string sql = "SELECT cp.Name, cp.Source, cp.DataType, ct.AnnClassName AS ObjectName FROM CfgAnnClassProperties cp, CfgAnnClasses ct WHERE cp.ConfigID = " + configID + " AND cp.AnnClassID = ct.ID";
                result = IsSrcAndTgtConsistent(configID, sql, "CfgAnnotationSource", "Annotation Class "); 
            }
            return result;
        }

        private Boolean IsTaskSrcAndTgtConsistent(string jobID, string configID)
        {
            Boolean result = true;
            if (IsTaskDataLoaded(jobID))
            {
                string sql = "SELECT cp.Name, cp.Source, cp.DataType, ct.TaskName AS ObjectName FROM CfgTaskProperties cp, CfgTaskTypes ct WHERE cp.ConfigID = " + configID + " AND cp.TaskTypeID = ct.ID";
                result = IsSrcAndTgtConsistent(configID, sql, "CfgTaskSource", "Task "); 
            }
            return result;
        }

        private Boolean IsSrcAndTgtConsistent(string configID, string targetSQL, string srcTableName, string msgQualifier)
        {
            Boolean result = true;
           
            XmlDocument targetXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, targetSQL);
            foreach (XmlElement tgtRow in targetXml.SelectNodes("//row"))
            {
                string srcName = tgtRow.GetAttribute("Source");
                string tgtDataType = tgtRow.GetAttribute("DataType");
                
                XmlDocument sourceXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT DataType, MultiValued FROM " + srcTableName + " WHERE Name = '" + srcName + "'");
                foreach (XmlElement srcRow in sourceXml.SelectNodes("//row"))
                {
                    string srcDataType = srcRow.GetAttribute("DataType");
                    string mValued = srcRow.GetAttribute("MultiValued");
                    if (((mValued == "N") && (!IsSingleValued(tgtDataType))) || ((mValued == "Y") && (IsSingleValued(tgtDataType))))
                    {
                        MessageBox.Show(msgQualifier + tgtRow.GetAttribute("ObjectName") + " has property " + tgtRow.GetAttribute("Name") + " (DataType: " + tgtDataType + ") and its source " + srcName + " has differing mutliplicity", "Migration Manager", MessageBoxButtons.OK);
                        result = false;
                        break;
                    }
                    if (((srcDataType == "DateTime") && (!IsTypeDateTime(tgtDataType))) || ((srcDataType != "DateTime") && (IsTypeDateTime(tgtDataType))))
                    {
                        MessageBox.Show(msgQualifier + tgtRow.GetAttribute("ObjectName") + " has property " + tgtRow.GetAttribute("Name") + " (DataType: " + tgtDataType + ") and its source " + srcName + " is of type " + srcDataType, "Migration Manager", MessageBoxButtons.OK);
                        result = false;
                        break;
                    }
                    break;
                }
                if (!result) break;
            }
            return result;
        }

        private Boolean IsSingleValued(string dataTypeName)
        {
            return (dataTypeName.Substring(0, 6) == "Single") ? true : false;
        }

        private Boolean IsTypeDateTime(string dataTypeName)
        {
            return ((dataTypeName == "SingletonDateTime") || (dataTypeName == "ListOfDateTime")) ? true : false;
        }

        private Boolean AreTargetsDefined(string jobID, string configID, string configLevel)
        {
            Boolean result = IsDocTgtDefined(jobID, configID) && IsAnnTgtDefined(jobID, configID);
            if ((result) && (configLevel == "CASE"))
            {
                result = IsCaseTgtDefined(jobID, configID) && IsTaskTgtDefined(jobID, configID);
            }
            return result;
        }

        private Boolean IsCaseTgtDefined(string jobID, string configID)
        {
            Boolean result = true;
            if (IsCaseDataLoaded(jobID))
            {
                if(!(IsTargetDefined(configID, "SELECT * FROM CfgCaseTypes WHERE ConfigID = " + configID)))
                {
                    MessageBox.Show("Configuration has no case types defined", "Migration Manager", MessageBoxButtons.OK);
                    result = false;
                }
            }
            return result;
        }

        private Boolean IsDocTgtDefined(string jobID, string configID)
        {
            Boolean result = true;
            if (IsDocumentDataLoaded(jobID))
            {
                if (!(IsTargetDefined(configID, "SELECT * FROM CfgDocClasses WHERE ConfigID = " + configID)))
                {
                    MessageBox.Show("Configuration has no document classes defined", "Migration Manager", MessageBoxButtons.OK);
                    result = false;
                }
            }
            return result;
        }

        private Boolean IsAnnTgtDefined(string jobID, string configID)
        {
            if (IsAnnotationDataLoaded(jobID))
            {
                if (!(IsTargetDefined(configID, "SELECT * FROM CfgAnnClasses WHERE ConfigID = " + configID)))
                {
                    MessageBox.Show("Configuration has no annotation classes defined, assuming base annotations", "Migration Manager", MessageBoxButtons.OK);
                }
            }
            return true; // always return true
        }

        private Boolean IsTaskTgtDefined(string jobID, string configID)
        {
            Boolean result = true;
            if (IsTaskDataLoaded(jobID))
            {
                if (!(IsTargetDefined(configID, "SELECT tt.TaskName FROM CfgTaskTypes tt, CfgCaseTypes ct WHERE tt.CaseID = ct.ID AND ct.ConfigID = " + configID)))
                {
                    MessageBox.Show("Configuration has no task types defined", "Migration Manager", MessageBoxButtons.OK);
                    result = false;
                }
            }
            return result;
        }

        private Boolean IsTargetDefined(string configID, string sql)
        {
            XmlDocument targetXml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
            return (targetXml.SelectNodes("//row").Count > 0) ? true : false;
        }

        private Boolean AreAllInitiatingDocClassesDefined(string jobID, string configID, string configLevel)
        {
            Boolean result = true;
            if (configLevel == "CASE")
            {
                if (IsCaseDataLoaded(jobID))
                {
                    XmlDocument xmlCase = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID FROM CfgCaseTypes WHERE ConfigID = " + configID);
                    if (xmlCase.SelectNodes("//row").Count > 0)
                    {
                        foreach (XmlElement caseNode in xmlCase.SelectNodes("//row"))
                        {
                            XmlAttribute xaCase = caseNode.Attributes["ID"];
                            if (xaCase != null)
                            {
                                string caseID = xaCase.Value.Trim();
                                XmlDocument xmlTask = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT TaskName, IsDocInitiated, InitiatingDocClass FROM CfgTaskTypes WHERE CaseID = " + caseID);
                                if (xmlTask.SelectNodes("//row").Count > 0)
                                {
                                    foreach (XmlElement taskNode in xmlTask.SelectNodes("//row"))
                                    {
                                        XmlAttribute xaDocInit = taskNode.Attributes["IsDocInitiated"];
                                        if (xaDocInit != null)
                                        {
                                            string docInit = xaDocInit.Value.Trim();
                                            if (docInit == "1")
                                            {
                                                string docClass = string.Empty;
                                                XmlAttribute xaDocClass = taskNode.Attributes["InitiatingDocClass"];
                                                if (xaDocClass != null)
                                                {
                                                    docClass = xaDocClass.Value.Trim();
                                                }
                                                if (string.IsNullOrEmpty(docClass))
                                                {
                                                    string taskName = taskNode.Attributes["TaskName"].Value.Trim();
                                                    MessageBox.Show("Document initiated task type " + taskName + " does not have the document class specified for the initiating document", "Migration Manager", MessageBoxButtons.OK);
                                                    result = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
            return result;
        }
    }
}
