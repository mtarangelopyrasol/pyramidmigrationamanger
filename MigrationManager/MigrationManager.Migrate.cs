﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        int currentMigrateJobIndex = 0;
        Boolean migrateJobSelected = false;
        Boolean caselevelMigrate = false;

        string[] chosenBatches;
        Boolean migrationBatchesSelected = false;

        Ingest.Ingestor ingestor;
        
        private void tabMigrate_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowMigrateSubTab();
        }

        private void ShowMigrateSubTab()
        {
            switch (tabMigrate.TabPages[tabMigrate.SelectedIndex].Name)
            {
                case "MigrateJob":
                    if (busyStatus != MigratorStatus.BusyIngesting)
                    {
                        if (IsConfigSelected())
                        {
                            HideMigrationLogs();
                            //tabMigrate.TabPages[tabMigrate.SelectedIndex].BringToFront();
                            RefreshMigrateJobList();
                        }
                    }
                    break;
                case "MigrateBatch":
                    if (busyStatus != MigratorStatus.BusyIngesting)
                    {
                        if (!migrateJobSelected)
                        {
                            MessageBox.Show("Please choose a job first", "Migration Manager", MessageBoxButtons.OK);
                        }
                        else
                        {
                            HideMigrationLogs();
                            //tabMigrate.TabPages[tabMigrate.SelectedIndex].BringToFront();
                            RefreshMigrateBatchSelectionList();
                        }
                    }
                    break;
                case "PerformIngest":
                    if (!migrationBatchesSelected)
                    {
                        MessageBox.Show("Please choose batches you wish to migrate first", "Migration Manager", MessageBoxButtons.OK);
                    }
                    else
                    {
                        HideMigrationLogs();
                        //tabMigrate.TabPages[tabMigrate.SelectedIndex].BringToFront();
                        RefreshMigratePage();
                    }
                    break;
                case "IngestLogs":
                    //tabMigrate.TabPages[tabMigrate.SelectedIndex].BringToFront();
                    ViewMigrationLogs();
                    break;
                case "IngestResults":
                    HideMigrationLogs();
                    //tabMigrate.TabPages[tabMigrate.SelectedIndex].BringToFront();
                    if (caselevelMigrate)
                    {
                        btnMigResultCaseID.Show();
                        btnMigResultTaskID.Show();
                    }
                    else
                    {
                        btnMigResultCaseID.Hide();
                        btnMigResultTaskID.Hide();
                    }
                    break;
                default:
                    throw new Exception("Unknown option");
            }
        }

        private void RefreshMigrateJobList()
        {
            listMigrateJobs.BeginUpdate();
            listMigrateJobs.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM MgrJobs ORDER BY JobName");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = listMigrateJobs.Items.Add(rowNode.GetAttribute("ID"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("JobName"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Comment"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                    listMigrateJobs.Items[currentMigrateJobIndex].Checked = true;
                    UpdateMigrateConfigLevel();
                    listMigrateJobs.EnsureVisible(i - 1);
                    migrateJobSelected = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Jobs : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                listMigrateJobs.EndUpdate();
            }
        }

        private void listMigrateJobs_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != currentMigrateJobIndex))
            {
                listMigrateJobs.Items[currentMigrateJobIndex].Checked = false;
                currentMigrateJobIndex = e.Item.Index;
                UpdateMigrateConfigLevel();
            }
        }

        private void UpdateMigrateConfigLevel()
        {
            try
            {
                string jobID = listMigrateJobs.Items[currentMigrateJobIndex].Tag.ToString();
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT c.ConfigLevel AS Level FROM MgrJobs j, MgrConfigurations c WHERE j.ID = " + jobID + " AND j.ConfigID = c.ID");
                string level = xml.SelectNodes("//row")[0].Attributes["Level"].Value;
                caselevelMigrate = (level == "CASE") ? true : false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while checking Job's config level : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void RefreshMigrateBatchSelectionList()
        {
            listMigrateBatches.BeginUpdate();
            listMigrateBatches.Items.Clear();
            try
            {
                string jobID = listMigrateJobs.Items[currentMigrateJobIndex].Tag.ToString();
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID, ActualSize, CAST(StartTime AS nvarchar(30)) AS StartStamp, CAST(EndTime AS nvarchar(30)) AS EndStamp, userName, WorkStation, Status FROM MgrBatches WHERE JobID = " + jobID + " ORDER BY ID");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = listMigrateBatches.Items.Add(rowNode.GetAttribute("ID"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("ActualSize"));
                        lvi.SubItems.Add(rowNode.GetAttribute("StartStamp"));
                        lvi.SubItems.Add(rowNode.GetAttribute("EndStamp"));
                        lvi.SubItems.Add(rowNode.GetAttribute("UserName"));
                        lvi.SubItems.Add(rowNode.GetAttribute("WorkStation"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Status"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        if (chosenBatches != null)
                        {
                            for (int j = 0; j < chosenBatches.GetLength(0); j++)
                            {
                                if (chosenBatches[j] == rowNode.GetAttribute("ID").Trim())
                                {
                                    lvi.Checked = true;
                                    break;
                                }
                            }
                        }
                        i++;
                    }
                    listMigrateBatches.EnsureVisible(i - 1);
                    migrationBatchesSelected = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Batches : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                listMigrateBatches.EndUpdate();
            }
        }

        private void listMigrateBatches_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item.Checked)
            {
                listMigrateBatches.Items[e.Item.Index].Checked = true;
                listMigrateBatches.Items[e.Item.Index].Selected = true;
                migrateStatus.Items.Clear();
            }
        }

        private void AdjustMigrateCheckBoxes()
        {
            if (caselevelMigrate)
            {
                if (ckDepthFirst.Checked)
                {
                    ckCases.Hide();
                    ckDocuments.Hide();
                    ckAnnotations.Hide();
                    ckTasks.Hide();
                    ckDepthFirst.Show();
                }
                else
                {
                    ckCases.Show();
                    ckDocuments.Show();
                    ckAnnotations.Show();
                    ckTasks.Show();
                    ckDepthFirst.Hide();
                }
            }
            else
            {
                if (ckDepthFirst.Checked)
                {
                    ckCases.Hide();
                    ckDocuments.Hide();
                    ckAnnotations.Hide();
                    ckTasks.Hide();
                    ckDepthFirst.Show();
                }
                else
                {
                    ckDocuments.Show();
                    ckAnnotations.Show();
                    ckDepthFirst.Hide();
                }
            }
        }

        private void ckDepthFirst_CheckedChanged(object sender, EventArgs e)
        {
            AdjustMigrateCheckBoxes();
        }

        private void ckMigrate_CheckedChanged(object sender, EventArgs e)
        {
            if (caselevelMigrate)
            {
                if ((!ckCases.Checked) && (!ckDocuments.Checked) && (!ckAnnotations.Checked) && (!ckTasks.Checked))
                {
                    ckCases.Hide();
                    ckDocuments.Hide();
                    ckAnnotations.Hide();
                    ckTasks.Hide();
                    ckDepthFirst.Show();
                }
                else if ((ckCases.Checked) && (ckDocuments.Checked) && (ckAnnotations.Checked) && (ckTasks.Checked))
                {
                    ckDepthFirst.Checked = true;
                    ckDepthFirst.Show();
                }
                else
                {
                    ckCases.Show();
                    ckDocuments.Show();
                    ckAnnotations.Show();
                    ckTasks.Show();
                    ckDepthFirst.Hide();
                }
            }
            else
            {
                if ((!ckDocuments.Checked) && (!ckAnnotations.Checked))
                {
                    ckCases.Hide();
                    ckDocuments.Hide();
                    ckAnnotations.Hide();
                    ckTasks.Hide();
                    ckDepthFirst.Show();
                }
                else if ((ckDocuments.Checked) && (ckAnnotations.Checked))
                {
                    ckDepthFirst.Checked = true;
                    ckDepthFirst.Show();
                }
                else
                {
                    ckDocuments.Show();
                    ckAnnotations.Show();
                    ckDepthFirst.Hide();
                }
            }
        } 

        private void RefreshMigratePage()
        {
            AdjustMigrateCheckBoxes();
            string jobID = listMigrateJobs.Items[currentMigrateJobIndex].Tag.ToString();
            string jobName = GetJobName(jobID);
            ListView.SelectedListViewItemCollection selectedItems = listMigrateBatches.SelectedItems;
            if (selectedItems.Count > 0)
            {
                chosenBatches = new string[selectedItems.Count];
                int i = 0;
                foreach (ListViewItem item in selectedItems)
                {
                    chosenBatches[i] = item.Tag.ToString();
                    i++;
                }

                lblMigrateJob.Text = "Job: " + jobName;
                lblMigrateBatch.Text = "Batches Selected: ";

                Boolean first = true;
                foreach (string b in chosenBatches)
                {
                    if (!first)
                    {
                        lblMigrateBatch.Text += ", ";
                    }
                    else
                    {
                        first = false;
                    }
                    lblMigrateBatch.Text += b;
                }
            }
            else
            {
                MessageBox.Show("Please choose a batch first", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void bgMigrator_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bw = sender as BackgroundWorker;

            //Boolean arg = (Boolean)e.Argument;
            if (migManTrace) bgMigrator.ReportProgress(0, "Calling on migrator to perform migration");
            e.Result = ingestor.PerformIngestion(bw);

            if (bw.CancellationPending)
            {
                e.Cancel = true;
            }
        }

        private void bgMigrator_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            DisplayMigrationStatus(e.UserState.ToString());
        }

        private void bgMigrator_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            string status;
            if (e.Cancelled)
            {
                status = "Migration canceled";
            }
            else if (e.Error != null)
            {
                status = "Migration encountered error: " + e.Error.Message;
            }
            else
            {
                status = e.Result.ToString();
            }
            DisplayMigrationStatus(status); 
        }

        private void btnMigrateCancel_Click(object sender, EventArgs e)
        {
            bgMigrator.CancelAsync();
            DisplayMigrationStatus("Migration cancel acknowledged; will cancel momentarily");
        }

        private void btnMigrate_Click(object sender, EventArgs e)
        {
            ResetMigrationStatusDisplay();
            LogMigrationTrace("Starting migration");
            TabPage p = tabMigrate.TabPages[2];

            Ingest.IngestionOptions options = DetermineIngestionOptions();
            if (options == Ingest.IngestionOptions.None)
            {
                MessageBox.Show("You have not chosen any Migration options", "Migration Manager", MessageBoxButtons.OK);
            }
            else
            {
                LogMigrationTrace("Migration options determined");
                Boolean dbg = configManager.GetAppSetting("TRACE_INGEST") == "1" ? true : false;
                LogMigrationTrace("Trace option: " + dbg.ToString());
                Boolean useSSL = configManager.GetAppSetting("UseSSL").ToUpper() == "TRUE" ? true : false;
                LogMigrationTrace("UseSSL: " + useSSL.ToString());
                ingestor = new Ingest.Ingestor(
                    dbConnection, GetJobName(listMigrateJobs.Items[currentMigrateJobIndex].Tag.ToString()), 
                    caselevelMigrate, chosenBatches, 
                    DetermineIngestionOptions(),
                    configManager.GetAppSetting("CEAPI_Uid"), 
                    configManager.GetAppSetting("CEAPI_Pwd"), 
                    configManager.GetAppSetting("CEAPI_Url"), 
                    configManager.GetAppSetting("CE_OS"), 
                    configManager.GetAppSetting("Solution"), 
                    configManager.GetAppSetting("CMServer"),
                    configManager.GetAppSetting("CE_REST_SERVER"),
                    useSSL, 
                    dbg);

                LogMigrationTrace("Ingestor created");
                SetInMigration();
                LogMigrationTrace("Set in migration");
                try
                {
                    bgMigrator.RunWorkerAsync();
                    while (bgMigrator.IsBusy)
                    {
                        Application.DoEvents();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Exception encountered during migration: " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
                }
                finally
                {
                    ClearInMigration();
                    //TODO: Dispose the ingestor
                }
            }
        }

        private void SetInMigration()
        {
            busyStatus = MigratorStatus.BusyConverting;
            AdjustMigratePageDisplay();
        }

        private void ClearInMigration()
        {
            busyStatus = MigratorStatus.NotBusy;
            AdjustMigratePageDisplay();
        }

        private void AdjustMigratePageDisplay()
        {
            switch (busyStatus)
            {
                case MigratorStatus.BusyLoading:
                    btnExit.Enabled = false;
                    break;

                case MigratorStatus.BusyConverting:
                    btnMigrateCancel.Enabled = true;
                    btnMigrate.Enabled = false;
                    btnExit.Enabled = false;
                    break;

                case MigratorStatus.BusyIngesting:
                    btnExit.Enabled = false;
                    break;

                default:
                    btnMigrateCancel.Enabled = false;
                    btnMigrate.Enabled = true;
                    btnExit.Enabled = true;
                    break;
            }
        }

        private void ResetMigrationStatusDisplay()
        {
            migrateStatus.BeginUpdate();
            migrateStatus.Items.Clear();
            migrateStatus.EndUpdate();
        }

        public void DisplayMigrationStatus(String status)
        {
            migrateStatus.BeginUpdate();
            migrateStatus.Items.Add(status);
            migrateStatus.EndUpdate();
            migrateStatus.SelectedItem = migrateStatus.Items[migrateStatus.Items.Count - 1];
            Application.DoEvents();
        }

        protected Ingest.IngestionOptions DetermineIngestionOptions()
        {
            Ingest.IngestionOptions result = Ingest.IngestionOptions.None;

            if (ckDepthFirst.Checked)
            {
                result = Ingest.IngestionOptions.DepthFirst;
            }
            else
            {
                if (ckCases.Checked) result |= Ingest.IngestionOptions.Cases;
                if (ckDocuments.Checked) result |= Ingest.IngestionOptions.Documents;
                if (ckAnnotations.Checked) result |= Ingest.IngestionOptions.Annotations;
                if (ckTasks.Checked) result |= Ingest.IngestionOptions.Tasks;
            }
            return result;
        }

        public void LogMigrationTrace(String message)
        {
            if (migManTrace) DisplayMigrationStatus(message);
        }
    }
}
