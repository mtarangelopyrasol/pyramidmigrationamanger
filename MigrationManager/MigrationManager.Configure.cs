﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        int lvIndexConfig = 0;
        int lvIndexIdentifier = 0;
        string currentConfigID = string.Empty;
        string currentConfigName = string.Empty;
        string currentConfigLevel = string.Empty;
        string currentConfigComment = string.Empty;
        Boolean configSelected = false;

        private void tabConfigure_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowConfigureSubTab(); 
        }

        private void ShowConfigureSubTab()
        {
            switch (tabConfigure.SelectedIndex)
            {
                case 0:
                    RefreshConfigList();
                    break;

                case 1:
                    ShowConfigSourceSubTab();
                    break;

                case 2:
                    ShowConfigTargetSubTab();
                    break;

                default:
                    RefreshIdentifierList();
                    break;
            }
        }

        private void lvConfigs_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndexConfig))
            {
                lvConfigs.Items[lvIndexConfig].Checked = false;
                lvIndexConfig = e.Item.Index;
                UpdateCurrentConfig();
                UpdateConfigTabPages();
            }
        }

        private void RefreshConfigList()
        {
            lvConfigs.BeginUpdate();
            lvConfigs.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID, ConfigName, ConfigLevel, CAST(LastUpdated AS nvarchar(30)) AS tStamp, Comment FROM MgrConfigurations ORDER BY ConfigName");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = lvConfigs.Items.Add(rowNode.GetAttribute("ConfigName"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("ConfigLevel"));
                        lvi.SubItems.Add(rowNode.GetAttribute("tStamp"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Comment"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                    lvConfigs.Items[lvIndexConfig].Checked = true;
                    UpdateCurrentConfig();
                    UpdateConfigTabPages();
                    lvConfigs.EnsureVisible(lvIndexConfig);
                    configSelected = true;
                }
                else configSelected = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Configurations : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                lvConfigs.EndUpdate();
            }
        }

        private Boolean IsConfigSelected()
        {
            if (!configSelected)
            {
                MessageBox.Show("Please choose a configuration first", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            return true;
        }

        private void UpdateCurrentConfig()
        {
            currentConfigID = (string)(lvConfigs.Items[lvIndexConfig].Tag);
            currentConfigName = lvConfigs.Items[lvIndexConfig].Text;
            currentConfigLevel = lvConfigs.Items[lvIndexConfig].SubItems[1].Text;
            currentConfigComment = lvConfigs.Items[lvIndexConfig].SubItems[3].Text;
        }

        private void UpdateConfigTabPages()
        {
            if (IsCaseLevelConfig())
            {
                ShowTabPage(tabConfigSource, "sourceCases", 0);
                ShowTabPage(tabConfigSource, "sourceTasks", 3);
                ShowTabPage(tabConfigTarget, "targetCases", 0);
                ShowTabPage(tabConfigTarget, "targetTasks", 3);
                lvIndexSourceCase = 0;
                lvIndexSourceTask = 0;
            }
            else
            {
                HideTabPage(tabConfigSource, "sourceCases");
                HideTabPage(tabConfigSource, "sourceTasks");
                HideTabPage(tabConfigTarget, "targetCases");
                HideTabPage(tabConfigTarget, "targetTasks");
            }
            lvIndexSourceDocument = 0;
            lvIndexSourceAnnotation = 0;
        }

        private Boolean IsCaseLevelConfig()
        {
            return (currentConfigLevel == "CASE") ? true : false;
        }

        private void btnAddConfiguration_Click(object sender, EventArgs e)
        {
            using (ConfigDialog dlg = new ConfigDialog(dbConnection, DialogOperation.Add))
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    RefreshConfigList();
                }
            }
        }

        private void btnUpdateConfig_Click(object sender, EventArgs e)
        {
            if (lvConfigs.Items.Count > 0)
            {
                using (ConfigDialog dlg = new ConfigDialog(dbConnection, DialogOperation.Update))
                {
                    dlg.Populate(currentConfigID, currentConfigName, currentConfigLevel, currentConfigComment);
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        RefreshConfigList();
                    }
                }
            }
            else
            {
                MessageBox.Show("There is nothing to edit", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnDeleteConfig_Click(object sender, EventArgs e)
        {
            if (lvConfigs.Items.Count > 0)
            {
                string sql = "SELECT JobName FROM MgrJobs WHERE ConfigID = " + currentConfigID;
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    MessageBox.Show("There are jobs defined based on this configuration; hence this configuration cannot be deleted.", "Migration Manager", MessageBoxButtons.OK);
                }
                else
                {
                    if (MessageBox.Show("You have chosen to delete the entire configuration " + currentConfigName + ". Proceed?", "Confirm configuration delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        PerformConfigDelete(currentConfigID);
                    }
                }
            }
            else
            {
                MessageBox.Show("There is nothing to delete", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void PerformConfigDelete(string configID)
        {
            DeleteConfig("CfgIdentifiers", "ConfigID", currentConfigID);

            if (IsCaseLevelConfig())
            {
                DeleteConfig("CfgTaskProperties", "ConfigID", configID);

                string sql = "SELECT ID as CASEID FROM CfgCaseTypes WHERE ConfigID = " + currentConfigID;
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

                foreach (XmlElement row in xml.SelectNodes("//row"))
                {
                    string caseID = row.Attributes["CASEID"].Value;
                        
                    DeleteConfig("CfgTaskTypes", "CaseID", caseID);
                        
                }
                DeleteConfig("CfgTaskSource", "ConfigID", currentConfigID);
            }
            DeleteConfig("CfgAnnotationSource", "ConfigID", currentConfigID);

            DeleteConfig("CfgDocClassProperties", "ConfigID", currentConfigID);
            DeleteConfig("CfgDocClasses", "ConfigID", currentConfigID);
            DeleteConfig("CfgDocumentSource", "ConfigID", currentConfigID);

            if (IsCaseLevelConfig())
            {
                DeleteConfig("CfgCaseFolders", "ConfigID", currentConfigID);
                DeleteConfig("CfgCaseProperties", "ConfigID", currentConfigID);
                DeleteConfig("CfgCaseTypes", "ConfigID", currentConfigID);
                DeleteConfig("CfgCaseSource", "ConfigID", currentConfigID);
            }

            if(DeleteConfig("MgrConfigurations", "ID", currentConfigID) > 0)
            {
                if(lvIndexConfig > 0) lvIndexConfig--;
                UpdateCurrentConfig();
                RefreshConfigList();
            }
        }

        private int DeleteConfig(string tableName, string fieldName, string id)
        {
            string sql = "DELETE " + tableName + " WHERE " + fieldName + " = " + id;
            return DBAdapter.DBAdapter.DeleteDB(dbConnection, sql);
        }

        
    }
}
