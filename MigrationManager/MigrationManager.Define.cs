﻿using System;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        int lvIndexConvDefns = 0;
        int lvIndexMapDefns = 0;
        int lvIndexJobDefns = 0;
        int lvIndexBatchDefns = 0;

        private void tabDefine_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowDefineSubTab();
        }

        private void ShowDefineSubTab()
        {
            TabPage currentPage = tabDefine.TabPages[tabDefine.SelectedIndex];
            
            switch (tabDefine.TabPages[tabDefine.SelectedIndex].Name)
            {
                case "defineConversions":
                    RefreshConversionDefinitions();
                    break;

                case "targetMappings":
                    RefreshMapDefinitions();
                    break;

                case "defineJobs":
                    RefreshJobDefnList();
                    break;

                case "defineBatches":
                    RefreshBatchDefnList();
                    break;

                default:
                    throw new Exception("Unknown option");
            }
            currentPage.BringToFront();
        }

        private void RefreshConversionDefinitions()
        {
            lvConvDefns.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID, ConvDefnName, MarshalToSingleDoc FROM MgrConvDefns ORDER BY ConvDefnName");

                int i = 0;
                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    ListViewItem lvi = lvConvDefns.Items.Add(rowNode.GetAttribute("ConvDefnName"), i++);
                    lvi.SubItems.Add(rowNode.GetAttribute("MarshalToSingleDoc"));
                    lvi.Tag = rowNode.GetAttribute("ID");
                }
                if (lvConvDefns.Items.Count > 0)
                {
                    lvConvDefns.Items[lvIndexConvDefns].Checked = true;
                    lvConvDefns.EnsureVisible(lvIndexConvDefns);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Conversion Definitions : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void lvConvDefns_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndexConvDefns))
            {
                lvConvDefns.Items[lvIndexConvDefns].Checked = false;
                lvIndexConvDefns = e.Item.Index;
            }
        }

        private void btnConvDefnAdd_Click(object sender, EventArgs e)
        {
            using (ConvDefnDialog dlg = new ConvDefnDialog(dbConnection, DialogOperation.Add))
            {
                dlg.ShowDialog();
                RefreshConversionDefinitions();
            }
        }

        private void btnConvDefnUpd_Click(object sender, EventArgs e)
        {
            if (lvConvDefns.Items.Count > 0)
            {
                using (ConvDefnDialog dlg = new ConvDefnDialog(dbConnection, DialogOperation.Update))
                {
                    dlg.Populate(lvConvDefns.Items[lvIndexConvDefns].Tag.ToString(),
                                                                    lvConvDefns.Items[lvIndexConvDefns].SubItems[0].Text,
                                                                    lvConvDefns.Items[lvIndexConvDefns].SubItems[1].Text);
                    dlg.ShowDialog();
                    RefreshConversionDefinitions();
                }
            }
            else
            {
                MessageBox.Show("There is nothing to edit", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnConvDefnDel_Click(object sender, EventArgs e)
        {
            if (lvConvDefns.Items.Count > 0)
            {
                string sql = "SELECT JobName FROM MgrJobs WHERE ConvDefnID = " + lvConvDefns.Items[lvIndexConvDefns].Tag.ToString();
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    MessageBox.Show("There are jobs defined based on this conversion definition; hence this conversion definition cannot be deleted.", "Migration Manager", MessageBoxButtons.OK);
                }
                else
                {
                    if (MessageBox.Show("You have chosen to delete the entire conversion definition " + lvConvDefns.Items[lvIndexConvDefns].SubItems[0].Text + ". Proceed?", "Confirm configuration delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (PerformConvDelete(lvConvDefns.Items[lvIndexConvDefns].Tag.ToString()))
                        {
                            if (lvIndexConvDefns > 0) lvIndexConvDefns--;
                            RefreshConversionDefinitions();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("There is nothing to delete", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private Boolean PerformConvDelete(string id)
        {
            try
            {
                int nAffected = DBAdapter.DBAdapter.DeleteDB(dbConnection, "DELETE MgrConversions WHERE ConvDefnID = " + id);
                nAffected = DBAdapter.DBAdapter.DeleteDB(dbConnection, "DELETE MgrConvDefns WHERE ID = " + id);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while deleting conversion definition. " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
        }

        private void RefreshBatchDefnList()
        {
            const int nColumns = 5;
            string[] fields = new string[nColumns];

            string errorID = "Batch Definitions";
            string sql = "SELECT * FROM MgrBatchDefns ORDER BY BatchDefnName";

            fields[0] = "BatchDefnName";
            fields[1] = "BatchSize";
            fields[2] = "BatchCondition";
            fields[3] = "SourceContentPathPrefix";
            fields[4] = "StagingPath";

            MigrationManager.RefreshListView(lvBatchDefns, dbConnection, sql, fields, errorID);
            if (lvBatchDefns.Items.Count > 0)
            {
                lvBatchDefns.Items[lvIndexBatchDefns].Checked = true;
                lvBatchDefns.EnsureVisible(lvIndexBatchDefns);
            }
        }

        private void lvBatchDefns_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndexBatchDefns))
            {
                lvBatchDefns.Items[lvIndexBatchDefns].Checked = false;
                lvIndexBatchDefns = e.Item.Index;
            }
        }

        private void btnAddBatchDefn_Click(object sender, EventArgs e)
        {
            using (AddBatchDefnDialog dlg = new AddBatchDefnDialog(dbConnection, DialogOperation.Add))
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    RefreshBatchDefnList();
                }
            }
        }

        private void btnUpdBatchDefn_Click(object sender, EventArgs e)
        {
            if (lvBatchDefns.Items.Count > 0)
            {
                using (AddBatchDefnDialog dlg = new AddBatchDefnDialog(dbConnection, DialogOperation.Update))
                {
                    ListViewItem lvi = lvBatchDefns.Items[lvIndexBatchDefns];
                    dlg.Populate(lvi.Tag.ToString(), lvi.SubItems[0].Text, lvi.SubItems[1].Text, lvi.SubItems[2].Text, lvi.SubItems[3].Text, lvi.SubItems[4].Text);
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        RefreshBatchDefnList();
                    }
                }
            }
            else
            {
                MessageBox.Show("There is nothing to edit", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnDelBatchDefn_Click(object sender, EventArgs e)
        {
            if (lvBatchDefns.Items.Count > 0)
            {
                string sql = "SELECT * FROM MgrBatches WHERE BatchDefnID = " + lvBatchDefns.Items[lvIndexBatchDefns].Tag.ToString() + " AND ActualSize > 0";
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    MessageBox.Show("There are batches existing based on this batch definition; hence this batch definition cannot be deleted.", "Migration Manager", MessageBoxButtons.OK);
                }
                else
                {
                    if (MessageBox.Show("You have chosen to delete the Batch definition " + lvBatchDefns.Items[lvIndexBatchDefns].SubItems[0].Text + ". Proceed?", "Confirm Job Definition delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (PerformBatchDefnDelete(lvBatchDefns.Items[lvIndexBatchDefns].Tag.ToString()))
                        {
                            if (lvIndexBatchDefns > 0) lvIndexBatchDefns--;
                            RefreshBatchDefnList();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("There is nothing to delete", "Migration Manager", MessageBoxButtons.OK);
            }
        }
        
        private void RefreshIdentifierList()
        {
            if (configSelected)
            {
                lblIdConfig.Text = "Current Configuration: " + currentConfigName;

                const int nColumns = 2;
                string[] fields = new string[nColumns];

                string errorID = "Identifier Definitions";
                string sql = "SELECT * FROM CfgIdentifiers WHERE ConfigID = " + currentConfigID + " ORDER BY IdentifierName";

                fields[0] = "IdentifierName";
                fields[1] = "FieldName";

                MigrationManager.RefreshListView(lvIdentifierDefns, dbConnection, sql, fields, errorID);
                lvIdentifierDefns.Items[lvIndexIdentifier].Checked = true;
                lvIdentifierDefns.Items[lvIndexIdentifier].EnsureVisible();
            }
            else
                ClearDisplay(lblIdConfig, lvIdentifierDefns);
        }

        private void lvIdentifierDefns_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if(e.Item.Checked)
            {
                if (e.Item.Index != lvIndexIdentifier)
                {
                    lvIdentifierDefns.Items[lvIndexIdentifier].Checked = false;
                    lvIndexIdentifier = e.Item.Index;
                }
            }
        }

        private void btnUpdIdentifier_Click(object sender, EventArgs e)
        {
            if (IsConfigSelected())
            {
                string identifier = lvIdentifierDefns.Items[lvIndexIdentifier].Text;
                string value = lvIdentifierDefns.Items[lvIndexIdentifier].SubItems[1].Text;

                using (EditIdentifierDialog dlg = new EditIdentifierDialog(dbConnection, currentConfigID, identifier, value, IsCaseLevelConfig()))
                {
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        RefreshIdentifierList();
                    }
                }
            }
        }

        private void RefreshJobDefnList()
        {
            const int nColumns = 5;
            string[] fields = new string[nColumns];

            string errorID = "Job Definitions";
            string sql = "SELECT j.ID AS ID, j.JobName, MgrConfigurations.ConfigName, MgrConvDefns.ConvDefnName, CAST(j.LastUpdated AS nvarchar(30)) AS tStamp, j.Comment FROM MgrJobs j, MgrConfigurations, MgrConvDefns WHERE j.ConfigID = MgrConfigurations.ID AND j.ConvDefnID = MgrConvDefns.ID ORDER BY j.JobName";

            fields[0] = "JobName";
            fields[1] = "ConfigName";
            fields[2] = "ConvDefnName";
            fields[3] = "tStamp";
            fields[4] = "Comment";

            MigrationManager.RefreshListView(lvJobDefns, dbConnection, sql, fields, errorID);
            if (lvJobDefns.Items.Count > 0)
            {
                lvJobDefns.Items[lvIndexJobDefns].Checked = true;
                lvJobDefns.EnsureVisible(lvIndexJobDefns);
            }
        }

        private void lvJobDefns_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndexJobDefns))
            {
                lvJobDefns.Items[lvIndexJobDefns].Checked = false;
                lvIndexJobDefns = e.Item.Index;
            }
        }

        private void btnAddJob_Click(object sender, EventArgs e)
        {
            using (AddJobDialog dlg = new AddJobDialog(dbConnection, DialogOperation.Add))
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    RefreshJobDefnList();
                }
            }
        }

        private void btnEditJob_Click(object sender, EventArgs e)
        {
            if (lvJobDefns.Items.Count > 0)
            {
                if (DoBatchesDependOnJob(lvJobDefns.Items[lvIndexJobDefns].Tag.ToString()))
                {
                    MessageBox.Show("There are batches existing based on this job definition; hence this job definition cannot be updated.", "Migration Manager", MessageBoxButtons.OK);
                }
                else
                {
                    using (AddJobDialog dlg = new AddJobDialog(dbConnection, DialogOperation.Update))
                    {
                        ListViewItem lvi = lvJobDefns.Items[lvIndexJobDefns];
                        dlg.Populate(lvi.Tag.ToString(), lvi.SubItems[0].Text, lvi.SubItems[1].Text, lvi.SubItems[2].Text, lvi.SubItems[4].Text);
                        if (dlg.ShowDialog() == DialogResult.OK)
                        {
                            RefreshJobDefnList();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("There is nothing to edit", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnDeleteJob_Click(object sender, EventArgs e)
        {
            if (lvJobDefns.Items.Count > 0)
            {
                if (DoBatchesDependOnJob(lvJobDefns.Items[lvIndexJobDefns].Tag.ToString()))
                {
                    MessageBox.Show("There are batches existing based on this job definition; hence this job definition cannot be deleted.", "Migration Manager", MessageBoxButtons.OK);
                }
                else
                {
                    if (MessageBox.Show("You have chosen to delete the job definition " + lvJobDefns.Items[lvIndexJobDefns].SubItems[0].Text + ". Proceed?", "Confirm Job Definition delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (PerformJobDelete(lvJobDefns.Items[lvIndexJobDefns].Tag.ToString()))
                        {
                            if ((lvIndexLoadJobs == lvIndexJobDefns) && (lvIndexJobDefns > 0))
                            {
                                lvIndexLoadJobs--;
                                lvLoadJobs.Items[lvIndexLoadJobs].Checked = true;
                                UpdateCurrentLoadJob();
                            }
                            if (lvIndexJobDefns > 0) lvIndexJobDefns--;
                            RefreshJobDefnList();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("There is nothing to delete", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private Boolean DoBatchesDependOnJob(string jobID)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM MgrBatches WHERE JobID = " + jobID + " AND ActualSize > 0");

            int count = xml.SelectNodes("//row").Count;
            return (count > 0) ? true : false;
        }

        private Boolean PerformJobDelete(string id)
        {
            try
            {
                int nAffected = DBAdapter.DBAdapter.DeleteDB(dbConnection, "DELETE MgrJobs WHERE ID = " + id);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while deleting job definition. " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
        }

        private void RefreshMapDefinitions()
        {
            lvMapDefns.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID, MappingName FROM MgrMappings ORDER BY MappingName");

                int i = 0;
                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    ListViewItem lvi = lvMapDefns.Items.Add(rowNode.GetAttribute("MappingName"), i++);
                    lvi.Tag = rowNode.GetAttribute("ID");
                }
                if (lvMapDefns.Items.Count > 0)
                {
                    lvMapDefns.Items[lvIndexMapDefns].Checked = true;
                    lvMapDefns.EnsureVisible(lvIndexMapDefns);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Mapping Definitions : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnMapDefnAdd_Click(object sender, EventArgs e)
        {
            using (MapDefnDialog dlg = new MapDefnDialog(dbConnection, DialogOperation.Add))
            {
                dlg.ShowDialog();
                RefreshMapDefinitions();
            }
        }

        private void btnMapDefnUpd_Click(object sender, EventArgs e)
        {
            if (lvMapDefns.Items.Count > 0)
            {
                using (MapDefnDialog dlg = new MapDefnDialog(dbConnection, DialogOperation.Update))
                {
                    dlg.Populate(lvMapDefns.Items[lvIndexMapDefns].Tag.ToString(), lvMapDefns.Items[lvIndexMapDefns].SubItems[0].Text);
                    dlg.ShowDialog();
                    RefreshMapDefinitions();
                }
            }
            else
            {
                MessageBox.Show("There is nothing to edit", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnMapDefnDel_Click(object sender, EventArgs e)
        {
            if (lvMapDefns.Items.Count > 0)
            {
                if(IsMappingUsed(dbConnection, lvMapDefns.Items[lvIndexMapDefns].SubItems[0].Text))
                {
                    MessageBox.Show("This mapping definition is used; hence cannot be deleted.", "Migration Manager", MessageBoxButtons.OK);
                }
                else
                {
                    if (MessageBox.Show("You have chosen to delete the entire mapping definition " + lvMapDefns.Items[lvIndexMapDefns].SubItems[0].Text + ". Proceed?", "Confirm mapping delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (PerformMapDelete(lvMapDefns.Items[lvIndexMapDefns].Tag.ToString()))
                        {
                            if (lvIndexMapDefns > 0) lvIndexMapDefns--;
                            RefreshMapDefinitions();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("There is nothing to delete", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void lvMapDefns_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndexMapDefns))
            {
                lvMapDefns.Items[lvIndexMapDefns].Checked = false;
                lvIndexMapDefns = e.Item.Index;
            }
        }

        private Boolean PerformMapDelete(string id)
        {
            try
            {
                int nAffected = DBAdapter.DBAdapter.DeleteDB(dbConnection, "DELETE MgrMappingValues WHERE MapID = " + id);
                nAffected = DBAdapter.DBAdapter.DeleteDB(dbConnection, "DELETE MgrMappings WHERE ID = " + id);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while deleting mapping definition. " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
        }

        private Boolean PerformBatchDefnDelete(string id)
        {
            try
            {
                int nAffected = DBAdapter.DBAdapter.DeleteDB(dbConnection, "DELETE MgrBatchDefns WHERE ID = " + id);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while deleting batch definition. " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
        }
    }
}
