﻿namespace MigrationManager
{
    partial class TaskTypesDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnProperties = new System.Windows.Forms.Button();
            this.lblHeader = new System.Windows.Forms.Label();
            this.lblTaskName = new System.Windows.Forms.Label();
            this.lblDocInitiated = new System.Windows.Forms.Label();
            this.lblInitiatingDocClass = new System.Windows.Forms.Label();
            this.lblInitiatingDocIdSrc = new System.Windows.Forms.Label();
            this.lblAttachingProperty = new System.Windows.Forms.Label();
            this.txtTaskName = new System.Windows.Forms.TextBox();
            this.cmbDocInitiated = new System.Windows.Forms.ComboBox();
            this.cmbInitiatingDocClass = new System.Windows.Forms.ComboBox();
            this.txtInitiatingDocIdSrc = new System.Windows.Forms.TextBox();
            this.cmbAttachingProperty = new System.Windows.Forms.ComboBox();
            this.lblParentCase = new System.Windows.Forms.Label();
            this.cmbParentCase = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(481, 380);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 32);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(376, 380);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 32);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnProperties
            // 
            this.btnProperties.Location = new System.Drawing.Point(20, 380);
            this.btnProperties.Name = "btnProperties";
            this.btnProperties.Size = new System.Drawing.Size(104, 32);
            this.btnProperties.TabIndex = 2;
            this.btnProperties.Text = "Properties";
            this.btnProperties.UseVisualStyleBackColor = true;
            this.btnProperties.Click += new System.EventHandler(this.btnProperties_Click);
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Location = new System.Drawing.Point(210, 33);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(64, 23);
            this.lblHeader.TabIndex = 3;
            this.lblHeader.Text = "Header";
            // 
            // lblTaskName
            // 
            this.lblTaskName.AutoSize = true;
            this.lblTaskName.Location = new System.Drawing.Point(20, 80);
            this.lblTaskName.Name = "lblTaskName";
            this.lblTaskName.Size = new System.Drawing.Size(59, 23);
            this.lblTaskName.TabIndex = 4;
            this.lblTaskName.Text = "Name:";
            // 
            // lblDocInitiated
            // 
            this.lblDocInitiated.AutoSize = true;
            this.lblDocInitiated.Location = new System.Drawing.Point(20, 180);
            this.lblDocInitiated.Name = "lblDocInitiated";
            this.lblDocInitiated.Size = new System.Drawing.Size(161, 23);
            this.lblDocInitiated.TabIndex = 5;
            this.lblDocInitiated.Text = "Document Initiated:";
            // 
            // lblInitiatingDocClass
            // 
            this.lblInitiatingDocClass.AutoSize = true;
            this.lblInitiatingDocClass.Location = new System.Drawing.Point(20, 230);
            this.lblInitiatingDocClass.Name = "lblInitiatingDocClass";
            this.lblInitiatingDocClass.Size = new System.Drawing.Size(211, 23);
            this.lblInitiatingDocClass.TabIndex = 6;
            this.lblInitiatingDocClass.Text = "Initiating Document Class:";
            // 
            // lblInitiatingDocIdSrc
            // 
            this.lblInitiatingDocIdSrc.AutoSize = true;
            this.lblInitiatingDocIdSrc.Location = new System.Drawing.Point(20, 280);
            this.lblInitiatingDocIdSrc.Name = "lblInitiatingDocIdSrc";
            this.lblInitiatingDocIdSrc.Size = new System.Drawing.Size(241, 23);
            this.lblInitiatingDocIdSrc.TabIndex = 7;
            this.lblInitiatingDocIdSrc.Text = "Initiating Document ID Source:";
            // 
            // lblAttachingProperty
            // 
            this.lblAttachingProperty.AutoSize = true;
            this.lblAttachingProperty.Location = new System.Drawing.Point(20, 330);
            this.lblAttachingProperty.Name = "lblAttachingProperty";
            this.lblAttachingProperty.Size = new System.Drawing.Size(160, 23);
            this.lblAttachingProperty.TabIndex = 8;
            this.lblAttachingProperty.Text = "Attaching Property:";
            // 
            // txtTaskName
            // 
            this.txtTaskName.Location = new System.Drawing.Point(265, 80);
            this.txtTaskName.Name = "txtTaskName";
            this.txtTaskName.Size = new System.Drawing.Size(301, 27);
            this.txtTaskName.TabIndex = 9;
            this.txtTaskName.TextChanged += new System.EventHandler(this.txtTaskName_TextChanged);
            // 
            // cmbDocInitiated
            // 
            this.cmbDocInitiated.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDocInitiated.FormattingEnabled = true;
            this.cmbDocInitiated.Items.AddRange(new object[] {
            "0",
            "1"});
            this.cmbDocInitiated.Location = new System.Drawing.Point(265, 180);
            this.cmbDocInitiated.Name = "cmbDocInitiated";
            this.cmbDocInitiated.Size = new System.Drawing.Size(296, 31);
            this.cmbDocInitiated.TabIndex = 10;
            this.cmbDocInitiated.SelectedIndexChanged += new System.EventHandler(this.cmbDocInitiated_SelectedIndexChanged);
            // 
            // cmbInitiatingDocClass
            // 
            this.cmbInitiatingDocClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbInitiatingDocClass.FormattingEnabled = true;
            this.cmbInitiatingDocClass.Location = new System.Drawing.Point(265, 230);
            this.cmbInitiatingDocClass.Name = "cmbInitiatingDocClass";
            this.cmbInitiatingDocClass.Size = new System.Drawing.Size(301, 31);
            this.cmbInitiatingDocClass.TabIndex = 11;
            this.cmbInitiatingDocClass.SelectedIndexChanged += new System.EventHandler(this.cmbInitiatingDocClass_SelectedIndexChanged);
            // 
            // txtInitiatingDocIdSrc
            // 
            this.txtInitiatingDocIdSrc.Location = new System.Drawing.Point(265, 280);
            this.txtInitiatingDocIdSrc.Name = "txtInitiatingDocIdSrc";
            this.txtInitiatingDocIdSrc.Size = new System.Drawing.Size(301, 27);
            this.txtInitiatingDocIdSrc.TabIndex = 12;
            this.txtInitiatingDocIdSrc.TextChanged += new System.EventHandler(this.txtInitiatingDocIdSrc_TextChanged);
            // 
            // cmbAttachingProperty
            // 
            this.cmbAttachingProperty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAttachingProperty.FormattingEnabled = true;
            this.cmbAttachingProperty.Location = new System.Drawing.Point(265, 330);
            this.cmbAttachingProperty.Name = "cmbAttachingProperty";
            this.cmbAttachingProperty.Size = new System.Drawing.Size(301, 31);
            this.cmbAttachingProperty.TabIndex = 13;
            this.cmbAttachingProperty.SelectedIndexChanged += new System.EventHandler(this.cmbAttachingProperty_SelectedIndexChanged);
            // 
            // lblParentCase
            // 
            this.lblParentCase.AutoSize = true;
            this.lblParentCase.Location = new System.Drawing.Point(20, 130);
            this.lblParentCase.Name = "lblParentCase";
            this.lblParentCase.Size = new System.Drawing.Size(107, 23);
            this.lblParentCase.TabIndex = 14;
            this.lblParentCase.Text = "Parent Case:";
            // 
            // cmbParentCase
            // 
            this.cmbParentCase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbParentCase.FormattingEnabled = true;
            this.cmbParentCase.Location = new System.Drawing.Point(265, 130);
            this.cmbParentCase.Name = "cmbParentCase";
            this.cmbParentCase.Size = new System.Drawing.Size(296, 31);
            this.cmbParentCase.TabIndex = 15;
            this.cmbParentCase.SelectedIndexChanged += new System.EventHandler(this.cmbParentCase_SelectedIndexChanged);
            // 
            // TaskTypesDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(584, 436);
            this.Controls.Add(this.cmbParentCase);
            this.Controls.Add(this.lblParentCase);
            this.Controls.Add(this.cmbAttachingProperty);
            this.Controls.Add(this.txtInitiatingDocIdSrc);
            this.Controls.Add(this.cmbInitiatingDocClass);
            this.Controls.Add(this.cmbDocInitiated);
            this.Controls.Add(this.txtTaskName);
            this.Controls.Add(this.lblAttachingProperty);
            this.Controls.Add(this.lblInitiatingDocIdSrc);
            this.Controls.Add(this.lblInitiatingDocClass);
            this.Controls.Add(this.lblDocInitiated);
            this.Controls.Add(this.lblTaskName);
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.btnProperties);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "TaskTypesDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "TaskTypesDialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnProperties;
        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.Label lblTaskName;
        private System.Windows.Forms.Label lblDocInitiated;
        private System.Windows.Forms.Label lblInitiatingDocClass;
        private System.Windows.Forms.Label lblInitiatingDocIdSrc;
        private System.Windows.Forms.Label lblAttachingProperty;
        private System.Windows.Forms.TextBox txtTaskName;
        private System.Windows.Forms.ComboBox cmbDocInitiated;
        private System.Windows.Forms.ComboBox cmbInitiatingDocClass;
        private System.Windows.Forms.TextBox txtInitiatingDocIdSrc;
        private System.Windows.Forms.ComboBox cmbAttachingProperty;
        private System.Windows.Forms.Label lblParentCase;
        private System.Windows.Forms.ComboBox cmbParentCase;
    }
}