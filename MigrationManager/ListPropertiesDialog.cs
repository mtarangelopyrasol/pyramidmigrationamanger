﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class ListPropertiesDialog : Form
    {
        int lvIndex = 0;
        string dbCnxn = string.Empty;
        string configID = string.Empty;
        string parentID = string.Empty;
        PropertyType propertyType;
        Boolean alreadyWarned = false;

        public ListPropertiesDialog(string dbConnection, string cfgID, string parent, PropertyType pType)
        {
            dbCnxn = dbConnection;
            configID = cfgID;
            parentID = parent;
            propertyType = pType;

            InitializeComponent();

            PopulateListView();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (ConfigPropertiesDialog dlg = new ConfigPropertiesDialog(dbCnxn, configID, parentID, propertyType, DialogOperation.Add))
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    PopulateListView();
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            using (ConfigPropertiesDialog dlg = new ConfigPropertiesDialog(dbCnxn, configID, parentID, propertyType, DialogOperation.Update))
            {
                dlg.Populate(GetTargetTable(), lvList.Items[lvIndex].Tag.ToString());
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    PopulateListView();
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string sql = "SELECT JobName FROM MgrJobs WHERE ConfigID = " + configID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);

            int count = xml.SelectNodes("//row").Count;
            if ((count > 0) && (!alreadyWarned))
            {
                alreadyWarned = true;
                if (MessageBox.Show("There are jobs defined based on this configuration; Do you still want to proceed with deleting this property?", "Migration Manager", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    DBAdapter.DBAdapter.DeleteDB(dbCnxn, "DELETE " + GetTargetTable() + " WHERE ID = " + lvList.Items[lvIndex].Tag.ToString());
                    PopulateListView();
                }
            }
            else
            {
                if ((propertyType == PropertyType.Annotation) && IsReserved(lvList.Items[lvIndex].Text))
                {
                    MessageBox.Show("Cannot delete reserved property " + lvList.Items[lvIndex].Text, "Delete denied", MessageBoxButtons.OK);
                    return;
                }

                if (MessageBox.Show("Delete property " + lvList.Items[lvIndex].Text + ". Proceed?", "Confirm configuration delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    DBAdapter.DBAdapter.DeleteDB(dbCnxn, "DELETE " + GetTargetTable() + " WHERE ID = " + lvList.Items[lvIndex].Tag.ToString());
                    PopulateListView();
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PopulateListView()
        {
            string sql = "SELECT ID, Name, Source, DataType, Required, Mapped, Computed, MappingName FROM " + GetTargetTable() + " WHERE " + GetParentLink() + " = " + parentID + " AND ConfigID = " + configID + " ORDER BY Name";

            lvList.BeginUpdate();
            lvList.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = lvList.Items.Add(rowNode.GetAttribute("Name"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("Source"));
                        lvi.SubItems.Add(rowNode.GetAttribute("DataType"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Required"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Mapped"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Computed"));
                        lvi.SubItems.Add(rowNode.GetAttribute("MappingName"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                    if (lvIndex > (count - 1)) lvIndex = count - 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception populating list view: " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                lvList.EndUpdate();
                if (lvList.Items.Count > 0)
                {
                    lvList.Items[0].Checked = true;
                }
            }
        }

        private string GetSourceTable()
        {
            string tableName = string.Empty;

            switch (propertyType)
            {
                case PropertyType.Case:
                    tableName = "CfgCaseSource";
                    break;

                case PropertyType.Document:
                    tableName = "CfgDocumentSource";
                    break;

                case PropertyType.Task:
                    tableName = "CfgTaskSource";
                    break;

                default:
                    break;
            }
            return tableName;
        }

        private string GetTargetTable()
        {
            string tableName = string.Empty;

            switch (propertyType)
            {
                case PropertyType.Case:
                    tableName = "CfgCaseProperties";
                    break;

                case PropertyType.Document:
                    tableName = "CfgDocClassProperties";
                    break;

                case PropertyType.Annotation:
                    tableName = "CfgAnnClassProperties";
                    break;

                case PropertyType.Task:
                    tableName = "CfgTaskProperties";
                    break;

                default:
                    break;
            }
            return tableName;
        }

        private string GetParentLink()
        {
            string link = string.Empty;

            switch (propertyType)
            {
                case PropertyType.Case:
                    link = "CaseTypeID";
                    break;

                case PropertyType.Document:
                    link = "DocClassID";
                    break;

                case PropertyType.Annotation:
                    link = "AnnClassID";
                    break;

                case PropertyType.Task:
                    link = "TaskTypeID";
                    break;

                default:
                    break;
            }
            return link;
        }

        private void lvList_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndex))
            {
                lvList.Items[lvIndex].Checked = false;
                lvIndex = e.Item.Index;
            }
        }

        private Boolean IsReserved(string name)
        {
            return ((name == "AnnotatedObject") || (name == "CmAcmAction") || (name == "CmAcmCommentedTask") || (name == "CmAcmCommentedVersionSeries")) ? true : false;
        }
    }
}
