﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class ListFoldersDialog : Form
    {
        int lvIndex = 0;
        string dbCnxn = string.Empty;
        string configID = string.Empty;
        string parentID = string.Empty;
        Boolean alreadyWarned = false;

        public ListFoldersDialog(string dbConnection, string cfgID, string parent)
        {
            dbCnxn = dbConnection;
            configID = cfgID;
            parentID = parent;

            InitializeComponent();

            PopulateListView();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (CaseFoldersDialog dlg = new CaseFoldersDialog(dbCnxn, configID, parentID, DialogOperation.Add))
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    PopulateListView();
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            using (CaseFoldersDialog dlg = new CaseFoldersDialog(dbCnxn, configID, parentID, DialogOperation.Update))
            {
                dlg.Populate(lvList.Items[lvIndex].Tag.ToString());
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    PopulateListView();
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string sql = "SELECT JobName FROM MgrJobs WHERE ConfigID = " + configID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);

            int count = xml.SelectNodes("//row").Count;
            if ((count > 0) && (!alreadyWarned))
            {
                alreadyWarned = true;
                if (MessageBox.Show("There are jobs defined based on this configuration; Do you still want to proceed with deleting this folder?", "Migration Manager", MessageBoxButtons.OK) == DialogResult.Yes)
                {
                    DBAdapter.DBAdapter.DeleteDB(dbCnxn, "DELETE CfgCaseFolders WHERE ID = " + lvList.Items[lvIndex].Tag.ToString());
                    PopulateListView();
                }
            }
            else
            {
                if (MessageBox.Show("Delete folder " + lvList.Items[lvIndex].Text + ". Proceed?", "Confirm configuration delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    DBAdapter.DBAdapter.DeleteDB(dbCnxn, "DELETE CfgCaseFolders WHERE ID = " + lvList.Items[lvIndex].Tag.ToString());
                    PopulateListView();
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PopulateListView()
        {
            string sql = "SELECT ID, FolderName, Parent FROM CfgCaseFolders WHERE CaseTypeID = " + parentID + " AND ConfigID = " + configID + " ORDER BY FolderName";

            lvList.BeginUpdate();
            lvList.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = lvList.Items.Add(rowNode.GetAttribute("FolderName"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("Parent"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception populating list view: " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                lvList.EndUpdate();
                if (lvList.Items.Count > 0)
                {
                    lvList.Items[0].Checked = true;
                    btnDelete.Enabled = true;
                    btnEdit.Enabled = true;
                }
                else
                {
                    btnDelete.Enabled = false;
                    btnEdit.Enabled = false;
                }
            }
        }

        private void lvList_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndex))
            {
                lvList.Items[lvIndex].Checked = false;
                lvIndex = e.Item.Index;
            }
        }
    }
}
