﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class EditIdentifierDialog : Form
    {
        string dbCnxn = string.Empty;
        string configID = string.Empty;
        string fieldName = string.Empty;
        string fieldValue = string.Empty;
        Boolean isCaseLevelConfig = false;

        public EditIdentifierDialog(string dbConnection, string cfgID, string name, string value, Boolean isCaseLevel)
        {
            InitializeComponent();

            dbCnxn = dbConnection;
            configID = cfgID;
            fieldName = name;
            fieldValue = value;
            isCaseLevelConfig = isCaseLevel;

            lblIdentifier.Text = name + ":";

            PopulateDropDown();
        }

        private void PopulateDropDown()
        {
            cmbValues.BeginUpdate();
            cmbValues.Items.Clear();
            cmbValues.Items.Add(" ");
            try
            {
                string prefix = fieldName.Substring(0, 3);
                string table = string.Empty;
                switch (prefix)
                {
                    case "Ann":
                        table = "CfgAnnotationSource";
                        break;

                    case "Cas":
                        table = "CfgCaseSource";
                        break;

                    case "Doc":
                        table = "CfgDocumentSource";
                        break;

                    case "Tas":
                        table = "CfgTaskSource";
                        break;

                    default:
                        throw new Exception("Unknown prefix");
                }

                string sql = (fieldName == "DocTraceIdentifier") ? 
                                "SELECT Name FROM CfgDocClassProperties WHERE ConfigID = " + configID :
                                "SELECT Name FROM " + table + " WHERE ConfigID = " + configID + " AND MultiValued = 'N' ORDER BY Name";
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);
 
                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    string colName = rowNode.GetAttribute("Name");
                    cmbValues.Items.Add(colName);
                }
                cmbValues.Text = fieldValue;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing potential values : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                cmbValues.EndUpdate();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string newValue = cmbValues.Text;

            if (HasPassedValidation(fieldName, newValue))
            {
                string sql = "UPDATE CfgIdentifiers SET FieldName = '" + newValue + "' WHERE IdentifierName = '" + fieldName + "' AND ConfigID = " + configID;
                int nAffected = DBAdapter.DBAdapter.UpdateDB(dbCnxn, sql);
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private Boolean HasPassedValidation(string fieldName, string fieldValue)
        {
            if (fieldValue.Length > 0)
            {
                if (fieldName == "DocTraceIdentifier")
                {
                    if (MessageBox.Show("The Source of " + fieldValue + " will be set to MMID. Continue?", "Migration Manager", MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
