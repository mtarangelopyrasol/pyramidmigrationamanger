﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        int chosenIngestionResetJobIndex = 0;
        int[] chosenResetConvBatches;
        int[] chosenResetMigBatches;
        int chosenIngestionResetIDJobIndex = 0;
        
        private void ViewIngestionResetOptions()
        {
            tabConversionReset.Hide();
            tabIngestionReset.Show();
            tabIngestionReset.BringToFront();
            ShowIngestionResetSubTab();
        }

        private void HideIngestionResetOptions()
        {
            tabIngestionReset.Hide();
        }

        private void tabIngestionReset_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowIngestionResetSubTab();
        }

        private void ShowIngestionResetSubTab()
        {
            switch (tabIngestionReset.TabPages[tabIngestionReset.SelectedIndex].Name)
            {
                case "JobsForIngestionReset":
                    RefreshIngestionResetJobsList(listIngestionResetJobs, chosenIngestionResetJobIndex);
                    break;

                case "ConvBatchesForIngestionReset":
                    RefreshResetConvBatchesList();
                    break;

                case "MigBatchesForIngestionReset":
                    RefreshResetMigBatchesList();
                    break;

                case "IdForIngestionReset":
                    RefreshIngestionResetJobsList(listIngestResetIDJobs, chosenIngestionResetIDJobIndex);
                    UpdateIngestResetIDDisplay();
                    break;

                default:
                    throw new Exception("Unknown option");
            }
        }

        private void RefreshIngestionResetJobsList(ListView lv, int chosenIndex)
        {
            lv.BeginUpdate();
            lv.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM MgrJobs ORDER BY JobName");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = lv.Items.Add(rowNode.GetAttribute("ID"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("JobName"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Comment"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        i++;
                    }
                    lv.Items[chosenIndex].Checked = true;
                    lv.EnsureVisible(i - 1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Jobs : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                lv.EndUpdate();
            }
        }

        private void listIngestionResetJobs_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != chosenIngestionResetJobIndex))
            {
                listIngestionResetJobs.Items[chosenIngestionResetJobIndex].Checked = false;
                chosenIngestionResetJobIndex = e.Item.Index;
            }
        }

        private void btnIngestionResetJobs_Click(object sender, EventArgs e)
        {
            string jobID = listIngestionResetJobs.Items[chosenIngestionResetJobIndex].Tag.ToString();
            string jobName = GetJobName(jobID);

            if (MessageBox.Show("You have chosen to reset ingestion statuses of the entire job " + jobName + ". Proceed?", "Confirm ingestion reset", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                PerformJobIngestionReset(jobName);
            }
        }

        private void PerformJobIngestionReset(string jobName)
        {
            lblIngestionResetJobsConfirm.Show();

            btnIngestionResetJobs.Enabled = false;

            Boolean caseLevel = IsJobConfigCaseLevel(jobName);

            if (caseLevel)
            {
                DBAdapter.DBAdapter.UpdateDB(dbConnection, "UPDATE " + jobName + "_CETasks" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL");
            }
            DBAdapter.DBAdapter.UpdateDB(dbConnection, "UPDATE " + jobName + "_CEAnnotations" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL");
            DBAdapter.DBAdapter.UpdateDB(dbConnection, "UPDATE " + jobName + "_CEDocuments" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL");
            if (caseLevel)
            {
                DBAdapter.DBAdapter.UpdateDB(dbConnection, "UPDATE " + jobName + "_CECases" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL");
            }

            lblIngestionResetJobsConfirm.Text = "Job " + jobName + " Reset complete";

            btnIngestionResetJobs.Enabled = true;
        }

        private void RefreshResetConvBatchesList()
        {
            listResetConvBatches.BeginUpdate();
            listResetConvBatches.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT b.ID, j.JobName, b.Status, b.ActualSize, CAST(b.StartTime AS nvarchar(30)) AS StartStamp, CAST(b.EndTime AS nvarchar(30)) AS EndStamp, b.UserName, b.WorkStation FROM MgrBatches b, MgrJobs j WHERE b.JobID = j.ID ORDER BY b.ID DESC");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi = listResetConvBatches.Items.Add(rowNode.GetAttribute("ID"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("JobName"));
                        lvi.SubItems.Add(rowNode.GetAttribute("ActualSize"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Status"));
                        lvi.SubItems.Add(rowNode.GetAttribute("StartStamp"));
                        lvi.SubItems.Add(rowNode.GetAttribute("EndStamp"));
                        lvi.SubItems.Add(rowNode.GetAttribute("UserName"));
                        lvi.SubItems.Add(rowNode.GetAttribute("WorkStation"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        if (chosenResetConvBatches != null)
                        {
                            for (int j = 0; j < chosenResetConvBatches.GetLength(0); j++)
                            {
                                if (chosenResetConvBatches[j].ToString() == rowNode.GetAttribute("ID").Trim())
                                {
                                    lvi.Checked = true;
                                    break;
                                }
                            }
                        }
                        i++;
                    }
                    listResetConvBatches.EnsureVisible(0);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Conversion Batches : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                listResetConvBatches.EndUpdate();
            }
        }

        private void listResetConvBatches_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item.Checked)
            {
                listResetConvBatches.Items[e.Item.Index].Checked = true;
                listResetConvBatches.Items[e.Item.Index].Selected = true;
            }
        }

        private void btnResetConvBatches_Click(object sender, EventArgs e)
        {
            CaptureChosenResetConvBatches();
            if (chosenResetConvBatches != null)
            {
                string chosenBatchList = GetResetConvBatchList();
                if (MessageBox.Show("You have chosen to reset statuses of the following batches: " + chosenBatchList + ". Proceed?", "Confirm batch reset", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    PerformConvBatchReset();
                }
            }
            else
            {
                MessageBox.Show("You have not chosen to reset the status of any batches", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void CaptureChosenResetConvBatches()
        {
            ListView.SelectedListViewItemCollection selectedItems = listResetConvBatches.SelectedItems;
            if (selectedItems.Count > 0)
            {
                chosenResetConvBatches = new int[selectedItems.Count];
                int i = 0;
                foreach (ListViewItem item in selectedItems)
                {
                    chosenResetConvBatches[i] = Convert.ToInt32(item.Tag.ToString());
                    i++;
                }
            }
            else
            {
                chosenResetConvBatches = null;
            }
        }

        private string GetResetConvBatchList()
        {
            string batchList = string.Empty;
            Boolean first = true;
            foreach (int b in chosenResetConvBatches)
            {
                if (!first)
                {
                    batchList += ", ";
                }
                else
                {
                    first = false;
                }
                batchList += b.ToString();
            }
            return batchList;
        }

        private void PerformConvBatchReset()
        {
            lblResetConvBatchConfirm.Show();
            btnResetConvBatches.Enabled = false;

            foreach (int b in chosenResetConvBatches)
            {
                try
                {
                    Dictionary<string, string> jobDetails = GetJobDetailsForConversionBatch(b.ToString());
                    string jobName = jobDetails["Name"];
                    Boolean caseLevel = IsJobConfigCaseLevel(jobName);
                    if (caseLevel)
                    {
                        DBAdapter.DBAdapter.UpdateDB(dbConnection,
                            "UPDATE " + jobName + "_CETasks" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE BatchID = " + b.ToString());
                    }
                    DBAdapter.DBAdapter.UpdateDB(dbConnection,
                        "UPDATE " + jobName + "_CEAnnotations" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE BatchID = " + b.ToString());
                    DBAdapter.DBAdapter.UpdateDB(dbConnection,
                        "UPDATE " + jobName + "_CEDocuments" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE BatchID = " + b.ToString());
                    if (caseLevel)
                    {
                        DBAdapter.DBAdapter.UpdateDB(dbConnection,
                            "UPDATE " + jobName + "_CECases" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE BatchID = " + b.ToString());
                    }
                    lblResetConvBatchConfirm.Text = "Batch " + b.ToString() + "Reset successfully"; 
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Reset Batch " + b.ToString() + ": " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
                }
            }
            lblResetConvBatchConfirm.Text = "Reset complete";
            btnResetConvBatches.Enabled = true;
        }

        private void RefreshResetMigBatchesList()
        {
            listResetMigBatches.BeginUpdate();
            listResetMigBatches.Items.Clear();
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT m.ID, j.JobName, m.Status, CAST(m.StartTime AS nvarchar(30)) AS StartStamp, CAST(m.EndTime AS nvarchar(30)) AS EndStamp, m.UserName, m.WorkStation, m.Comment FROM MgrMigrations m, MgrBatches b, MgrJobs j WHERE m.BatchID = b.ID AND b.JobID = j.ID ORDER BY m.ID DESC");

                int count = xml.SelectNodes("//row").Count;
                if (count > 0)
                {
                    int i = 0;
                    foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                    {
                        ListViewItem lvi= listResetMigBatches.Items.Add(rowNode.GetAttribute("ID"), i);
                        lvi.SubItems.Add(rowNode.GetAttribute("JobName")); 
                        lvi.SubItems.Add(rowNode.GetAttribute("Status"));
                        lvi.SubItems.Add(rowNode.GetAttribute("StartStamp"));
                        lvi.SubItems.Add(rowNode.GetAttribute("EndStamp"));
                        lvi.SubItems.Add(rowNode.GetAttribute("UserName"));
                        lvi.SubItems.Add(rowNode.GetAttribute("WorkStation"));
                        lvi.SubItems.Add(rowNode.GetAttribute("Comment"));
                        lvi.Tag = rowNode.GetAttribute("ID");
                        if (chosenResetMigBatches != null)
                        {
                            for (int j = 0; j < chosenResetMigBatches.GetLength(0); j++)
                            {
                                if (chosenResetMigBatches[j].ToString() == rowNode.GetAttribute("ID").Trim())
                                {
                                    lvi.Checked = true;
                                    break;
                                }
                            }
                        }
                        i++;
                    }
                    listResetMigBatches.EnsureVisible(i - 1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception while listing Migration Batches : " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                listResetMigBatches.EndUpdate();
            }
        }

        private void listResetMigBatches_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item.Checked)
            {
                listResetMigBatches.Items[e.Item.Index].Checked = true;
                listResetMigBatches.Items[e.Item.Index].Selected = true;
            }
        }

        private void btnResetErrors_Click(object sender, EventArgs e)
        {
            CaptureChosenResetMigBatches();
            if (chosenResetMigBatches != null)
            {
                string chosenBatchList = GetResetMigBatchList();
                if (MessageBox.Show("You have chosen to reset statuses of the errored records of the following batches: " + chosenBatchList + ". Proceed?", "Confirm batch reset", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    PerformMigBatchReset(true);
                }
            }
            else
            {
                MessageBox.Show("You have not chosen to reset the status of any batches", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnResetMigBatches_Click(object sender, EventArgs e)
        {
            CaptureChosenResetMigBatches();
            if (chosenResetMigBatches != null)
            {
                string chosenBatchList = GetResetMigBatchList();
                if (MessageBox.Show("You have chosen to reset statuses of the following batches: " + chosenBatchList + ". Proceed?", "Confirm batch reset", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    PerformMigBatchReset(false);
                }
            }
            else
            {
                MessageBox.Show("You have not chosen to reset the status of any batches", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void CaptureChosenResetMigBatches()
        {
            ListView.SelectedListViewItemCollection selectedItems = listResetMigBatches.SelectedItems;
            if (selectedItems.Count > 0)
            {
                chosenResetMigBatches = new int[selectedItems.Count];
                int i = 0;
                foreach (ListViewItem item in selectedItems)
                {
                    chosenResetMigBatches[i] = Convert.ToInt32(item.Tag.ToString());
                    i++;
                }
            }
            else
            {
                chosenResetMigBatches = null;
            }
        }

        private string GetResetMigBatchList()
        {
            string batchList = string.Empty;
            Boolean first = true;
            foreach (int b in chosenResetMigBatches)
            {
                if (!first)
                {
                    batchList += ", ";
                }
                else
                {
                    first = false;
                }
                batchList += b.ToString();
            }
            return batchList;
        }

        private void ResetTable(string tableName, string batchID, Boolean errorsOnly)
        {
            string sql = "UPDATE " + tableName + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE MigrationID = " + batchID.ToString();
            if (errorsOnly)
            {
                sql += " AND Status IN ('ING_ERRORED', 'ING_CANCELED')";
            }
            DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
        }

        private void PerformMigBatchReset(Boolean errorsOnly)
        {
            lblResetMigBatchConfirm.Show();
            btnResetMigBatches.Enabled = false;

            foreach (int b in chosenResetMigBatches)
            {
                try
                {
                    string jobName = GetJobNameForIngestionBatch(b.ToString());
                    Boolean caseLevel = IsJobConfigCaseLevel(jobName);
                    int cLevel = (caseLevel) ? 1 : 0;
                    int eLevel = (errorsOnly) ? 1 : 0;
                    DBAdapter.DBAdapter.ExecuteSPResetMigrationBatch(dbConnection, b, jobName, cLevel, eLevel);


                    //if (caseLevel) ResetTable(jobName + "_CETasks", b.ToString(), errorsOnly);
                    //ResetTable(jobName + "_CEAnnotations", b.ToString(), errorsOnly);
                    //ResetTable(jobName + "_CEDocuments", b.ToString(), errorsOnly);
                    //if (caseLevel) ResetTable(jobName + "_CECases", b.ToString(), errorsOnly);
                    //lblResetMigBatchConfirm.Text = "Batch " + b.ToString() + "Reset successfully";  
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Reset Batch " + b.ToString() + ": " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
                }
            }
            lblResetMigBatchConfirm.Text = "Reset complete";
            btnResetMigBatches.Enabled = true;
        }

        private string GetJobNameForIngestionBatch(string batchID)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT j.JobName FROM MgrMigrations m, MgrBatches b, MgrJobs j WHERE m.ID = " + batchID + " AND m.BatchID = b.ID AND b.JobID = j.ID");
            return xml.SelectNodes("//row")[0].Attributes["JobName"].Value;
        }

        private void listIngestResetIDJobs_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != chosenIngestionResetIDJobIndex))
            {
                listIngestResetIDJobs.Items[chosenIngestionResetIDJobIndex].Checked = false;
                chosenIngestionResetIDJobIndex = e.Item.Index;
                UpdateIngestResetIDDisplay();
            }
        }

        private void UpdateIngestResetIDDisplay()
        {
            string jobName = GetJobName(listIngestResetIDJobs.Items[chosenIngestionResetIDJobIndex].Tag.ToString());
            Boolean caseLevel = IsJobConfigCaseLevel(jobName);

            if (caseLevel)
            {
                btnIngestResetCaseID.Show();
                btnIngestResetTaskID.Show();
            }
            else
            {
                btnIngestResetCaseID.Hide();
                btnIngestResetTaskID.Hide();
            }
        }

        private void btnIngestResetCaseID_Click(object sender, EventArgs e)
        {
            string chosenID = txtIngestResetID.Text.Trim();

            if (chosenID.Length > 0)
            {
                string jobName = GetJobName(listIngestResetIDJobs.Items[chosenIngestionResetIDJobIndex].Tag.ToString());

                EnableIngestionResetButtons(false);

                // task annotations
                DBAdapter.DBAdapter.UpdateDB(dbConnection,
                    "UPDATE " + jobName + "_CEAnnotations" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE AnnType = 'TASK' AND ObjectID IN (SELECT ID FROM " + jobName + "_CETasks WHERE CaseID = " + chosenID + ") ");
                
                // tasks
                DBAdapter.DBAdapter.UpdateDB(dbConnection,
                            "UPDATE " + jobName + "_CETasks" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE CaseID = " + chosenID);
                
                // document annotations
                DBAdapter.DBAdapter.UpdateDB(dbConnection,
                    "UPDATE " + jobName + "_CEAnnotations" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE AnnType = 'DOCUMENT' AND ObjectID IN (SELECT ID FROM " + jobName + "_CEDocuments WHERE CaseID = " + chosenID + ") ");
                
                // documents
                DBAdapter.DBAdapter.UpdateDB(dbConnection,
                    "UPDATE " + jobName + "_CEDocuments" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE CaseID = " + chosenID);
                
                // case annotations
                DBAdapter.DBAdapter.UpdateDB(dbConnection,
                    "UPDATE " + jobName + "_CEAnnotations" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE AnnType = 'CASE' AND ObjectID = " + chosenID);

                // case
                DBAdapter.DBAdapter.UpdateDB(dbConnection,
                    "UPDATE " + jobName + "_CECases" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE ID = " + chosenID);

                lblResetIDConfirm.Text = "Case " + chosenID + " Reset";
                lblResetIDConfirm.Show();

                EnableIngestionResetButtons(true);
            }
            else
            {
                MessageBox.Show("Specify an ID to reset", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnIngestResetDocID_Click(object sender, EventArgs e)
        {
            string chosenID = txtIngestResetID.Text.Trim();

            if (chosenID.Length > 0)
            {
                string jobName = GetJobName(listIngestResetIDJobs.Items[chosenIngestionResetIDJobIndex].Tag.ToString());

                EnableIngestionResetButtons(false);

                // document annotations
                DBAdapter.DBAdapter.UpdateDB(dbConnection,
                   "UPDATE " + jobName + "_CEAnnotations" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE AnnType = 'DOCUMENT' AND ObjectID = " + chosenID);
                
                // document
                DBAdapter.DBAdapter.UpdateDB(dbConnection,
                    "UPDATE " + jobName + "_CEDocuments" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE ID = " + chosenID);
                
                lblResetIDConfirm.Text = "Document " + chosenID + " Reset";
                lblResetIDConfirm.Show();

                EnableIngestionResetButtons(true);
            }
            else
            {
                MessageBox.Show("Specify an ID to reset", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnIngestResetAnnID_Click(object sender, EventArgs e)
        {
            string chosenID = txtIngestResetID.Text.Trim();

            if (chosenID.Length > 0)
            {
                string jobName = GetJobName(listIngestResetIDJobs.Items[chosenIngestionResetIDJobIndex].Tag.ToString());

                EnableIngestionResetButtons(false);

                DBAdapter.DBAdapter.UpdateDB(dbConnection,
                   "UPDATE " + jobName + "_CEAnnotations" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE ID = " + chosenID);
               
                lblResetIDConfirm.Text = "Annotation " + chosenID + " Reset";
                lblResetIDConfirm.Show();

                EnableIngestionResetButtons(true);
            }
            else
            {
                MessageBox.Show("Specify an ID to reset", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void btnIngestResetTaskID_Click(object sender, EventArgs e)
        {
            string chosenID = txtIngestResetID.Text.Trim();

            if (chosenID.Length > 0)
            {
                string jobName = GetJobName(listIngestResetIDJobs.Items[chosenIngestionResetIDJobIndex].Tag.ToString());

                EnableIngestionResetButtons(false);

                // task annotations
                DBAdapter.DBAdapter.UpdateDB(dbConnection,
                    "UPDATE " + jobName + "_CEAnnotations" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE AnnType = 'TASK' AND ObjectID = " + chosenID);

                // task
                DBAdapter.DBAdapter.UpdateDB(dbConnection,
                   "UPDATE " + jobName + "_CETasks" + " SET Status = 'ING_NEW', MigrationID = NULL, CE_GUID = NULL WHERE ID = " + chosenID);
                
                lblResetIDConfirm.Text = "Task " + chosenID + " Reset";
                lblResetIDConfirm.Show();

                EnableIngestionResetButtons(true);
            }
            else
            {
                MessageBox.Show("Specify an ID to reset", "Migration Manager", MessageBoxButtons.OK);
            }
        }

        private void EnableIngestionResetButtons(Boolean enable)
        {
            btnIngestResetCaseID.Enabled = enable;
            btnIngestResetDocID.Enabled = enable;
            btnIngestResetAnnID.Enabled = enable;
            btnIngestResetTaskID.Enabled = enable;
        }
    }
}
