﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MigrationManager
{
    public enum DialogOperation
    {
        Add,
        Update
    }

    public partial class ConfigDialog : Form
    {
        string dbCnxn;
        DialogOperation operation;
        string configID = string.Empty;

        public ConfigDialog(string dbConnection, DialogOperation op)
        {
            dbCnxn = dbConnection;
            operation = op;

            InitializeComponent();

            if (op == DialogOperation.Add)
            {
                txtName.Enabled = true;
                cmbLevel.Enabled = true;
                txtComment.Enabled = true;
                lblHeader.Text = "Add New Configuration";
                cmbLevel.SelectedItem = cmbLevel.Items[0];
                btnExecute.Text = "Add";
            }
            else
            {
                txtName.Enabled = false;
                cmbLevel.Enabled = false;
                txtComment.Enabled = true;
                lblHeader.Text = "Update Configuration";
                btnExecute.Text = "Update";
            }
        }

        public void Populate(string id, string name, string level, string comment)
        {
            configID = id;
            txtName.Text = name;
            for (int i = 0; i < cmbLevel.Items.Count; i++)
            {
                if (cmbLevel.Items[i].ToString() == level)
                {
                    cmbLevel.SelectedItem = cmbLevel.Items[i];
                    break;
                }
            }
            txtComment.Text = comment;
        }

        private Boolean HasPassedValidation()
        {
            if (txtName.Text.Length == 0)
            {
                MessageBox.Show("Please specify a Configuration Name", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (txtName.Text.Length >= 64)
            {
                MessageBox.Show("Configuration Name is too long. Configuration Names can be at most 64 characters", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (txtComment.Text.Length >= 64)
            {
                MessageBox.Show("Length of Comment is too long. Comments can be at most 64 characters", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            return true;
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            if (HasPassedValidation())
            {
                string sql = string.Empty;
                string comment = MigrationManager.EscapeSingleQuote(txtComment.Text.Trim());
                if (operation == DialogOperation.Add)
                {
                    string name = MigrationManager.EscapeSingleQuote(txtName.Text.Trim());
                    string level = cmbLevel.SelectedItem.ToString();

                    sql = "INSERT INTO MgrConfigurations(ConfigName, ConfigLevel, Comment) VALUES ('" + name + "', '" + level + "', '" + comment + "')";
                }
                else
                {
                    sql = "UPDATE MgrConfigurations SET Comment = '" + comment + "' WHERE ID = " + configID;
                }
                int nAffected = DBAdapter.DBAdapter.AddToDB(dbCnxn, sql);
                DialogResult = DialogResult.OK;
                this.Close();
            }  
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
