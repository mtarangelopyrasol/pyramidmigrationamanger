﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class ConvMapDialog : Form
    {
        string dbCnxn = string.Empty;
        DialogOperation operation;
        string defnID = string.Empty;
        string ID = string.Empty;

        public ConvMapDialog(string dbConnection, string defID, DialogOperation op)
        {
            dbCnxn = dbConnection;
            defnID = defID;
            operation = op;

            InitializeComponent();

            lblHeader.Text = (op == DialogOperation.Add) ? "Add New Conversion Map" : "Update Conversion Map";
            cmbExempt.SelectedIndex = 0;
        }

        public void Populate(string id, string src, string tgt, string ex)
        {
            ID = id;

            txtSource.Text = src;
            txtTarget.Text = tgt;
            for (int i = 0; i < cmbExempt.Items.Count; i++)
            {
                if (cmbExempt.Items[i].ToString() == ex)
                {
                    cmbExempt.SelectedItem = cmbExempt.Items[i];
                    break;
                }
            }
        }

        private Boolean HasPassedValidation()
        {
            string src = txtSource.Text.Trim();
            string tgt = txtTarget.Text.Trim();
            string ex = cmbExempt.SelectedItem.ToString();

            if (src.Length == 0)
            {
                MessageBox.Show("Please specify a Source format", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (src.Length >= 16)
            {
                MessageBox.Show("Source Format Name is too long. Format Names can be at most 16 characters", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (tgt.Length == 0)
            {
                MessageBox.Show("Please specify a Target format", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            if (tgt.Length >= 16)
            {
                MessageBox.Show("Target Format Name is too long. Format Names can be at most 16 characters", "Migration Manager", MessageBoxButtons.OK);
                return false;
            }
            // Check for ambiguous definitons
            string sql = "SELECT c.ID as ID, cm.TargetFormat as Target, cm.Exempt as Exempt FROM MgrConversions c, MgrConvMaps cm WHERE c.ConvDefnID = " + defnID + " AND c.ConvMapID = cm.ID AND cm.SourceFormat = '" + src + "'";
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);
            Boolean result = true;
            foreach (XmlElement rowNode in xml.SelectNodes("//row"))
            {
                string id = rowNode.GetAttribute("ID");
                if (operation == DialogOperation.Add)
                {
                    MessageBox.Show("Source Format " + src + " is already mapped.", "Migration Manager", MessageBoxButtons.OK);
                    result = false;
                    break;
                }
                else
                {
                    if (id != ID)
                    {
                        string oldTarget = rowNode.GetAttribute("Target");
                        string exempt = rowNode.GetAttribute("Exempt");
                        if ((oldTarget == tgt) && (exempt == ex))
                        {
                            MessageBox.Show("Source Format " + src + " is already mapped.", "Migration Manager", MessageBoxButtons.OK);
                            result = false;
                            break;
                        }
                    }
                }
            }
            if((src.ToUpper() == "ALL") && (ex == "1"))
            {
                MessageBox.Show("Source Format " + src + " is the catch-all case. It cannot be marked marshaling exempt", "Migration Manager", MessageBoxButtons.OK);
                result = false;
            }
            return result;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (HasPassedValidation())
            {
                string src = txtSource.Text.Trim();
                string tgt = txtTarget.Text.Trim();
                string ex = cmbExempt.SelectedItem.ToString();

                string sql = "SELECT * FROM MgrConvMaps WHERE SourceFormat = '" + src + "' AND TargetFormat = '" + tgt + "' AND Exempt = " + ex;
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);
                int count = xml.SelectNodes("//row").Count;
                if (count == 0)
                {
                    sql = "INSERT INTO MgrConvMaps(SourceFormat, TargetFormat, Exempt) VALUES ('" + src + "', '" + tgt + "', " + ex + ")";
                    int nAffected = DBAdapter.DBAdapter.AddToDB(dbCnxn, sql);
                    sql = "SELECT * FROM MgrConvMaps WHERE SourceFormat = '" + src + "' AND TargetFormat = '" + tgt + "' AND Exempt = " + ex;
                    xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);
                }
                string mapID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                if (operation == DialogOperation.Add)
                {
                    sql = "INSERT INTO MgrConversions(ConvDefnID, ConvMapID) VALUES (" + defnID + ", " + mapID + ")";
                    int nAffected = DBAdapter.DBAdapter.AddToDB(dbCnxn, sql);
                }
                else
                {
                    sql = "UPDATE MgrConversions SET ConvMapID = " + mapID + " WHERE ID = " + ID;
                    int nAffected = DBAdapter.DBAdapter.UpdateDB(dbCnxn, sql);
                }
                DialogResult = DialogResult.OK;
                this.Close();
            }  
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
