﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DBAdapter;

namespace MigrationManager
{
    public partial class LoginDialog : Form
    {
        string dbConnection;
        Boolean winAuth = false;

        public LoginDialog(string userId, string passwd, string instance, string database, string useWinAuth)
        {
            InitializeComponent();

            txtDBInstance.Text = instance;
            txtDatabase.Text = database;
            winAuth = (useWinAuth.ToLower() == "true") ? true : false;

            if (winAuth)
            {
                lblUserID.Hide();
                txtUserID.Hide();
                lblPassword.Hide();
                txtPassword.Hide();
                lblWinAuth.Show(); 
            }
            else
            {
                lblUserID.Show();
                txtUserID.Show();
                lblPassword.Show();
                txtPassword.Show();
                lblWinAuth.Hide();

                txtUserID.Text = userId;
                txtPassword.Text = passwd;
            }

            this.DialogResult = DialogResult.Cancel;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            btnCancel.Enabled = false;
            btnConnect.Enabled = false;

            if (winAuth)
            {
                dbConnection = "Data Source = " + txtDBInstance.Text + "; Initial Catalog = " + txtDatabase.Text + "; Integrated Security = SSPI";
            }
            else
            {
                dbConnection = "Data Source = " + txtDBInstance.Text + "; Initial Catalog = " + txtDatabase.Text + "; User Id = " + txtUserID.Text + "; Password = " + txtPassword.Text;
            }

            string sql = "SELECT ID, ConfigName FROM [dbo].[MgrConfigurations]"; // just some sample SQL
            try
            {
                DBAdapter.DBAdapter dba = new DBAdapter.DBAdapter();
                dba.ExecuteSelect(dbConnection, sql);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not connect to the database: " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
                btnCancel.Enabled = true;
                btnConnect.Enabled = true;
            }
        }

        public string GetDBConnectionString()
        {
            return dbConnection;
        }
    }
}
