﻿namespace MigrationManager
{
    partial class SourceConfigDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHeader = new System.Windows.Forms.Label();
            this.lblColumn = new System.Windows.Forms.Label();
            this.lblFieldName = new System.Windows.Forms.Label();
            this.lblDataType = new System.Windows.Forms.Label();
            this.lblLength = new System.Windows.Forms.Label();
            this.lblFormat = new System.Windows.Forms.Label();
            this.lblMultiValued = new System.Windows.Forms.Label();
            this.txtColumn = new System.Windows.Forms.TextBox();
            this.txtFieldName = new System.Windows.Forms.TextBox();
            this.cmbDataType = new System.Windows.Forms.ComboBox();
            this.txtLength = new System.Windows.Forms.TextBox();
            this.txtFormat = new System.Windows.Forms.TextBox();
            this.cmbMultiValued = new System.Windows.Forms.ComboBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Location = new System.Drawing.Point(232, 26);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(64, 23);
            this.lblHeader.TabIndex = 0;
            this.lblHeader.Text = "Header";
            // 
            // lblColumn
            // 
            this.lblColumn.AutoSize = true;
            this.lblColumn.Location = new System.Drawing.Point(20, 70);
            this.lblColumn.Name = "lblColumn";
            this.lblColumn.Size = new System.Drawing.Size(72, 23);
            this.lblColumn.TabIndex = 1;
            this.lblColumn.Text = "Column:";
            // 
            // lblFieldName
            // 
            this.lblFieldName.AutoSize = true;
            this.lblFieldName.Location = new System.Drawing.Point(20, 120);
            this.lblFieldName.Name = "lblFieldName";
            this.lblFieldName.Size = new System.Drawing.Size(101, 23);
            this.lblFieldName.TabIndex = 2;
            this.lblFieldName.Text = "Field Name:";
            // 
            // lblDataType
            // 
            this.lblDataType.AutoSize = true;
            this.lblDataType.Location = new System.Drawing.Point(20, 170);
            this.lblDataType.Name = "lblDataType";
            this.lblDataType.Size = new System.Drawing.Size(92, 23);
            this.lblDataType.TabIndex = 3;
            this.lblDataType.Text = "Data Type:";
            // 
            // lblLength
            // 
            this.lblLength.AutoSize = true;
            this.lblLength.Location = new System.Drawing.Point(20, 220);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(68, 23);
            this.lblLength.TabIndex = 4;
            this.lblLength.Text = "Length:";
            // 
            // lblFormat
            // 
            this.lblFormat.AutoSize = true;
            this.lblFormat.Location = new System.Drawing.Point(20, 270);
            this.lblFormat.Name = "lblFormat";
            this.lblFormat.Size = new System.Drawing.Size(71, 23);
            this.lblFormat.TabIndex = 5;
            this.lblFormat.Text = "Format:";
            // 
            // lblMultiValued
            // 
            this.lblMultiValued.AutoSize = true;
            this.lblMultiValued.Location = new System.Drawing.Point(20, 320);
            this.lblMultiValued.Name = "lblMultiValued";
            this.lblMultiValued.Size = new System.Drawing.Size(110, 23);
            this.lblMultiValued.TabIndex = 6;
            this.lblMultiValued.Text = "Multi Valued:";
            // 
            // txtColumn
            // 
            this.txtColumn.Enabled = false;
            this.txtColumn.Location = new System.Drawing.Point(140, 70);
            this.txtColumn.Name = "txtColumn";
            this.txtColumn.Size = new System.Drawing.Size(89, 27);
            this.txtColumn.TabIndex = 7;
            // 
            // txtFieldName
            // 
            this.txtFieldName.Location = new System.Drawing.Point(140, 120);
            this.txtFieldName.Name = "txtFieldName";
            this.txtFieldName.Size = new System.Drawing.Size(397, 27);
            this.txtFieldName.TabIndex = 8;
            // 
            // cmbDataType
            // 
            this.cmbDataType.FormattingEnabled = true;
            this.cmbDataType.Items.AddRange(new object[] {
            "String",
            "DateTime"});
            this.cmbDataType.Location = new System.Drawing.Point(140, 170);
            this.cmbDataType.Name = "cmbDataType";
            this.cmbDataType.Size = new System.Drawing.Size(156, 31);
            this.cmbDataType.TabIndex = 9;
            this.cmbDataType.SelectedIndexChanged += new System.EventHandler(this.cmbDataType_SelectedIndexChanged);
            // 
            // txtLength
            // 
            this.txtLength.Location = new System.Drawing.Point(140, 220);
            this.txtLength.Name = "txtLength";
            this.txtLength.Size = new System.Drawing.Size(100, 27);
            this.txtLength.TabIndex = 10;
            // 
            // txtFormat
            // 
            this.txtFormat.Location = new System.Drawing.Point(140, 270);
            this.txtFormat.Name = "txtFormat";
            this.txtFormat.Size = new System.Drawing.Size(397, 27);
            this.txtFormat.TabIndex = 11;
            // 
            // cmbMultiValued
            // 
            this.cmbMultiValued.FormattingEnabled = true;
            this.cmbMultiValued.Items.AddRange(new object[] {
            "N",
            "Y"});
            this.cmbMultiValued.Location = new System.Drawing.Point(140, 320);
            this.cmbMultiValued.Name = "cmbMultiValued";
            this.cmbMultiValued.Size = new System.Drawing.Size(54, 31);
            this.cmbMultiValued.TabIndex = 12;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(462, 365);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 32);
            this.btnOK.TabIndex = 13;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(379, 365);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 32);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // SourceConfigDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(561, 419);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.cmbMultiValued);
            this.Controls.Add(this.txtFormat);
            this.Controls.Add(this.txtLength);
            this.Controls.Add(this.cmbDataType);
            this.Controls.Add(this.txtFieldName);
            this.Controls.Add(this.txtColumn);
            this.Controls.Add(this.lblMultiValued);
            this.Controls.Add(this.lblFormat);
            this.Controls.Add(this.lblLength);
            this.Controls.Add(this.lblDataType);
            this.Controls.Add(this.lblFieldName);
            this.Controls.Add(this.lblColumn);
            this.Controls.Add(this.lblHeader);
            this.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "SourceConfigDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add/Edit Columns";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.Label lblColumn;
        private System.Windows.Forms.Label lblFieldName;
        private System.Windows.Forms.Label lblDataType;
        private System.Windows.Forms.Label lblLength;
        private System.Windows.Forms.Label lblFormat;
        private System.Windows.Forms.Label lblMultiValued;
        private System.Windows.Forms.TextBox txtColumn;
        private System.Windows.Forms.TextBox txtFieldName;
        private System.Windows.Forms.ComboBox cmbDataType;
        private System.Windows.Forms.TextBox txtLength;
        private System.Windows.Forms.TextBox txtFormat;
        private System.Windows.Forms.ComboBox cmbMultiValued;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
    }
}