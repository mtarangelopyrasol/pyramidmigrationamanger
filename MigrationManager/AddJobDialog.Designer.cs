﻿namespace MigrationManager
{
    partial class AddJobDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblJobName = new System.Windows.Forms.Label();
            this.txtJobName = new System.Windows.Forms.TextBox();
            this.lblConfig = new System.Windows.Forms.Label();
            this.lblConversion = new System.Windows.Forms.Label();
            this.lblComment = new System.Windows.Forms.Label();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAddJob = new System.Windows.Forms.Button();
            this.lblHeader = new System.Windows.Forms.Label();
            this.ttAddNewJob = new System.Windows.Forms.ToolTip(this.components);
            this.cmbConfig = new System.Windows.Forms.ComboBox();
            this.cmbConv = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblJobName
            // 
            this.lblJobName.AutoSize = true;
            this.lblJobName.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJobName.Location = new System.Drawing.Point(20, 86);
            this.lblJobName.Name = "lblJobName";
            this.lblJobName.Size = new System.Drawing.Size(90, 23);
            this.lblJobName.TabIndex = 0;
            this.lblJobName.Text = "Job Name:";
            // 
            // txtJobName
            // 
            this.txtJobName.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJobName.Location = new System.Drawing.Point(170, 86);
            this.txtJobName.Name = "txtJobName";
            this.txtJobName.Size = new System.Drawing.Size(539, 27);
            this.txtJobName.TabIndex = 2;
            this.ttAddNewJob.SetToolTip(this.txtJobName, "Enter name by which you would like to refer to this job");
            // 
            // lblConfig
            // 
            this.lblConfig.AutoSize = true;
            this.lblConfig.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConfig.Location = new System.Drawing.Point(20, 157);
            this.lblConfig.Name = "lblConfig";
            this.lblConfig.Size = new System.Drawing.Size(119, 23);
            this.lblConfig.TabIndex = 3;
            this.lblConfig.Text = "Configuration:";
            // 
            // lblConversion
            // 
            this.lblConversion.AutoSize = true;
            this.lblConversion.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConversion.Location = new System.Drawing.Point(20, 220);
            this.lblConversion.Name = "lblConversion";
            this.lblConversion.Size = new System.Drawing.Size(100, 23);
            this.lblConversion.TabIndex = 4;
            this.lblConversion.Text = "Conversion:";
            // 
            // lblComment
            // 
            this.lblComment.AutoSize = true;
            this.lblComment.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComment.Location = new System.Drawing.Point(20, 282);
            this.lblComment.Name = "lblComment";
            this.lblComment.Size = new System.Drawing.Size(88, 23);
            this.lblComment.TabIndex = 6;
            this.lblComment.Text = "Comment:";
            // 
            // txtComment
            // 
            this.txtComment.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComment.Location = new System.Drawing.Point(170, 282);
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(539, 27);
            this.txtComment.TabIndex = 7;
            this.ttAddNewJob.SetToolTip(this.txtComment, "Enter a descriptive comment for this job definition");
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(553, 338);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 32);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.ttAddNewJob.SetToolTip(this.btnCancel, "Cancel out of this dialog without adding a new job definition");
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAddJob
            // 
            this.btnAddJob.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAddJob.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddJob.Location = new System.Drawing.Point(634, 338);
            this.btnAddJob.Name = "btnAddJob";
            this.btnAddJob.Size = new System.Drawing.Size(75, 32);
            this.btnAddJob.TabIndex = 9;
            this.btnAddJob.Text = "OK";
            this.ttAddNewJob.SetToolTip(this.btnAddJob, "Add a new job definition using content entered in this dialog");
            this.btnAddJob.UseVisualStyleBackColor = true;
            this.btnAddJob.Click += new System.EventHandler(this.btnAddJob_Click);
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeader.Location = new System.Drawing.Point(268, 33);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(108, 23);
            this.lblHeader.TabIndex = 10;
            this.lblHeader.Text = "Add New Job";
            // 
            // cmbConfig
            // 
            this.cmbConfig.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbConfig.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbConfig.FormattingEnabled = true;
            this.cmbConfig.Location = new System.Drawing.Point(170, 157);
            this.cmbConfig.Name = "cmbConfig";
            this.cmbConfig.Size = new System.Drawing.Size(539, 31);
            this.cmbConfig.TabIndex = 11;
            this.cmbConfig.SelectedIndexChanged += new System.EventHandler(this.cmbConfig_SelectedIndexChanged);
            // 
            // cmbConv
            // 
            this.cmbConv.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbConv.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbConv.FormattingEnabled = true;
            this.cmbConv.Location = new System.Drawing.Point(170, 220);
            this.cmbConv.Name = "cmbConv";
            this.cmbConv.Size = new System.Drawing.Size(539, 31);
            this.cmbConv.TabIndex = 12;
            this.cmbConv.SelectedIndexChanged += new System.EventHandler(this.cmbConv_SelectedIndexChanged);
            // 
            // AddJobDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(744, 398);
            this.Controls.Add(this.cmbConv);
            this.Controls.Add(this.cmbConfig);
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.btnAddJob);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtComment);
            this.Controls.Add(this.lblComment);
            this.Controls.Add(this.lblConversion);
            this.Controls.Add(this.lblConfig);
            this.Controls.Add(this.txtJobName);
            this.Controls.Add(this.lblJobName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddJobDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add New Job";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblJobName;
        private System.Windows.Forms.TextBox txtJobName;
        private System.Windows.Forms.Label lblConfig;
        private System.Windows.Forms.Label lblConversion;
        private System.Windows.Forms.Label lblComment;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAddJob;
        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.ToolTip ttAddNewJob;
        private System.Windows.Forms.ComboBox cmbConfig;
        private System.Windows.Forms.ComboBox cmbConv;
    }
}