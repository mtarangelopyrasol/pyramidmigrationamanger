﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class ListValueMapDialog : Form
    {
        string dbCnxn = string.Empty;
        DialogOperation operation;
        string defnID = string.Empty;
        string ID = string.Empty;
        int lvIndex = 0;

        public ListValueMapDialog(string dbConnection, string mdID, string mdName, DialogOperation op)
        {
            dbCnxn = dbConnection;
            defnID = mdID;
            operation = op;

            InitializeComponent();

            lblHeader.Text = mdName;

            PopulateListView();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (ValueMapDialog dlg = new ValueMapDialog(dbCnxn, defnID, DialogOperation.Add))
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    PopulateListView();
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            using (ValueMapDialog dlg = new ValueMapDialog(dbCnxn, defnID, DialogOperation.Update))
            {
                ListViewItem lvi = lvList.Items[lvIndex];
                dlg.Populate(lvi.Tag.ToString(), lvi.SubItems[0].Text, lvi.SubItems[1].Text);
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    PopulateListView();
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int nAffected = DBAdapter.DBAdapter.DeleteDB(dbCnxn, "DELETE MgrMappingValues WHERE ID = " + lvList.Items[lvIndex].Tag.ToString());
            if (lvIndex > 0) lvIndex--;
            PopulateListView();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PopulateListView()
        {
            string sql = "SELECT ID, OldValue, NewValue FROM MgrMappingValues WHERE MapID = " + defnID + " ORDER BY OldValue";

            lvList.BeginUpdate();
            lvList.Items.Clear();

            ListViewItem lvi = null;
            try
            {
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbCnxn, sql);

                int i = 0;
                foreach (XmlElement rowNode in xml.SelectNodes("//row"))
                {
                    lvi = lvList.Items.Add(rowNode.GetAttribute("OldValue"), i);
                    lvi.SubItems.Add(rowNode.GetAttribute("NewValue"));
                    lvi.Tag = rowNode.GetAttribute("ID");
                    i++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception populating list view: " + ex.Message, "Migration Manager", MessageBoxButtons.OK);
            }
            finally
            {
                lvList.EndUpdate();
                if (lvList.Items.Count > 0)
                {
                    lvList.Items[lvIndex].Checked = true;
                    lvList.Items[lvIndex].EnsureVisible();
                }
            }
        }

        private void lvList_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if ((e.Item.Checked) && (e.Item.Index != lvIndex))
            {
                lvList.Items[lvIndex].Checked = false;
                lvIndex = e.Item.Index;
            }
        }
    }
}
