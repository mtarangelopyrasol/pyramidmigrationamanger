﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        private void btnConvExport_Click(object sender, EventArgs e)
        {
            if (lvConvDefns.Items.Count > 0)
            {
                string convDefnID = lvConvDefns.Items[lvIndexConvDefns].Tag.ToString();

                if (convDefnID.Length > 0)
                {
                    string exportFolder = GetDestinationFolder("Select destination folder.");
                    if (exportFolder.Length > 0)
                    {
                        string convDefnName = lvConvDefns.Items[lvIndexConvDefns].SubItems[0].Text;

                        string exportFile = FormulateExportFileName(exportFolder, "MM_Conv", convDefnName);
                        if (exportFile.Length > 0)
                        {
                            XmlTextWriter xtw = StartExport(exportFile);
                            if (xtw != null)
                            {
                                ExportConvDefn(convDefnID, xtw);
                                EndExport(xtw, exportFile);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("You have not chosen any conversions to export.", "Migration Manager", MessageBoxButtons.OK);
                }
            }
        }

        private void ExportConvDefn(string convDefnID, XmlTextWriter xtw)
        {
            xtw.WriteStartElement("mmConversion");

            string sql = "SELECT ConvDefnName, MarshalToSingleDoc FROM MgrConvDefns WHERE ID = " + convDefnID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            XmlAttributeCollection xac = xml.SelectNodes("//row")[0].Attributes;

            foreach (XmlAttribute xa in xac)
            {
                xtw.WriteAttributeString(xa.Name, xa.Value);
            }

            ExportConvMaps(convDefnID, xtw);

            xtw.WriteEndElement();
        }

        private void ExportConvMaps(string convDefnID, XmlTextWriter xtw)
        {
            xtw.WriteStartElement("mmConvMaps");

            string sql = "SELECT cm.SourceFormat, cm.TargetFormat, cm.Exempt, cm.Restricted FROM MgrConvDefns cd, MgrConversions c, MgrConvMaps cm WHERE cd.ID = " + convDefnID + " AND cd.ID = c.ConvDefnID AND c.ConvMapID = cm.ID";
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            foreach (XmlElement row in xml.SelectNodes("//row"))
            {
                xtw.WriteStartElement("mmConvMap");
                foreach (XmlAttribute xa in row.Attributes)
                {
                    xtw.WriteAttributeString(xa.Name, xa.Value);
                }
                xtw.WriteEndElement();
            }

            xtw.WriteEndElement();
        }

        private void btnMapDefnExport_Click(object sender, EventArgs e)
        {
            if (lvMapDefns.Items.Count > 0)
            {
                string mapDefnID = lvMapDefns.Items[lvIndexMapDefns].Tag.ToString();

                if (mapDefnID.Length > 0)
                {
                    string exportFolder = GetDestinationFolder("Select destination folder.");
                    if (exportFolder.Length > 0)
                    {
                        string mapDefnName = lvMapDefns.Items[lvIndexMapDefns].SubItems[0].Text;

                        string exportFile = FormulateExportFileName(exportFolder, "MM_Map", mapDefnName);
                        if (exportFile.Length > 0)
                        {
                            XmlTextWriter xtw = StartExport(exportFile);
                            if (xtw != null)
                            {
                                ExportMapping(mapDefnID, xtw);
                                EndExport(xtw, exportFile);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("You have not chosen any maps to export.", "Migration Manager", MessageBoxButtons.OK);
                }
            }
        }

        private void ExportMapping(string mapDefnID, XmlTextWriter xtw)
        {
            xtw.WriteStartElement("mmMapping");

            string sql = "SELECT MappingName FROM MgrMappings WHERE ID = " + mapDefnID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            XmlAttributeCollection xac = xml.SelectNodes("//row")[0].Attributes;

            foreach (XmlAttribute xa in xac)
            {
                xtw.WriteAttributeString(xa.Name, xa.Value);
            }

            ExportValueMaps(mapDefnID, xtw);

            xtw.WriteEndElement();
        }

        private void ExportMappings(Dictionary<string, string> mapList, XmlTextWriter xtw)
        {
            xtw.WriteStartElement("mmMappings");

            foreach (string k in mapList.Keys)
            {
                ExportMapping(k, xtw);
            }

            xtw.WriteEndElement();
        }

        private void ExportValueMaps(string mapDefnID, XmlTextWriter xtw)
        {
            xtw.WriteStartElement("mmValueMaps");

            string sql = "SELECT OldValue, NewValue FROM MgrMappingValues WHERE MapID = " + mapDefnID;
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            foreach (XmlElement row in xml.SelectNodes("//row"))
            {
                xtw.WriteStartElement("mmValueMap");
                foreach (XmlAttribute xa in row.Attributes)
                {
                    xtw.WriteAttributeString(xa.Name, xa.Value);
                }
                xtw.WriteEndElement();
            }

            xtw.WriteEndElement();
        }

        private void btnExportJob_Click(object sender, EventArgs e)
        {
            if (lvJobDefns.Items.Count > 0)
            {
                ListViewItem lvi = lvJobDefns.Items[lvIndexJobDefns];
                string jobDefnID = lvi.Tag.ToString();

                if (jobDefnID.Length > 0)
                {
                    string exportFolder = GetDestinationFolder("Select destination folder.");
                    if (exportFolder.Length > 0)
                    {
                        string jobDefnName = lvi.SubItems[0].Text;

                        string exportFile = FormulateExportFileName(exportFolder, "MM_JobDefn", jobDefnName);
                        if (exportFile.Length > 0)
                        {
                            XmlTextWriter xtw = StartExport(exportFile);
                            if (xtw != null)
                            {
                                ExportJobDefn(jobDefnID, xtw);
                                EndExport(xtw, exportFile);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("You have not chosen any job definitions to export.", "Migration Manager", MessageBoxButtons.OK);
                }
            }
        }

        private void ExportJobDefn(string jobDefnID, XmlTextWriter xtw)
        {
            xtw.WriteStartElement("mmJobDefinition");

            string sql = "SELECT j.ConfigID, j.ConvDefnID, j.JobName, j.Comment, c.ConfigName, cd.ConvDefnName FROM MgrJobs j, MgrConvDefns cd, MgrConfigurations c WHERE j.ID = " + jobDefnID + " AND c.ID = j.ConfigID AND j.ConvDefnID = cd.ID";
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);

            XmlAttributeCollection xac = xml.SelectNodes("//row")[0].Attributes;

            string configID = string.Empty;
            string convDefnID = string.Empty;

            foreach (XmlAttribute xa in xac)
            {
                if (xa.Name == "ConfigID")
                {
                    configID = xa.Value;
                }
                else if (xa.Name == "ConvDefnID")
                {
                    convDefnID = xa.Value;
                }
                else
                {
                    xtw.WriteAttributeString(xa.Name, xa.Value);
                }
            }
            Dictionary<string, string> mapList = new Dictionary<string, string>();
            ExportConfig(configID, mapList, xtw);
            ExportMappings(mapList, xtw);
            ExportConvDefn(convDefnID, xtw);

            xtw.WriteEndElement();
        }

        private void btnExpBatchDefn_Click(object sender, EventArgs e)
        {
            if (lvBatchDefns.Items.Count > 0)
            {
                ListViewItem lvi = lvBatchDefns.Items[lvIndexBatchDefns];
                string batchDefnID = lvi.Tag.ToString();

                if (batchDefnID.Length > 0)
                {
                    string exportFolder = GetDestinationFolder("Select destination folder.");
                    if (exportFolder.Length > 0)
                    {
                        string batchDefnName = lvi.SubItems[0].Text;

                        string exportFile = FormulateExportFileName(exportFolder, "MM_BatchDefn", batchDefnName);
                        if (exportFile.Length > 0)
                        {
                            XmlTextWriter xtw = StartExport(exportFile);
                            if (xtw != null)
                            {
                                ExportBatchDefn(xtw, batchDefnID);
                                EndExport(xtw, exportFile);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("You have not chosen any batch definitions to export.", "Migration Manager", MessageBoxButtons.OK);
                }
            }
        }

        private void ExportBatchDefn(XmlTextWriter xtw, string batchDefnID)
        {
            ExportSQLResult("mmBatchDefinitions", "mmBatchDefinition", 
                "SELECT BatchDefnName, BatchSize, BatchCondition, SourceContentPathPrefix, StagingPath FROM MgrBatchDefns WHERE ID = " + batchDefnID, xtw);
        }
    }
}
