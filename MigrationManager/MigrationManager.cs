﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public enum MigratorStatus
    {
        NotBusy,
        BusyLoading,
        BusyConverting,
        BusyIngesting
    }

    public enum ImportAction
    {
        Ignore,
        Update,
        Merge,
        Insert
    }

    public partial class MigrationManager : Form
    {
        ConfigManager configManager;
        string dbConnection;
        const string versionInfo = "Version 2.0.0";
        Boolean migManTrace = false;

        MigratorStatus busyStatus = MigratorStatus.NotBusy;

        public MigrationManager()
        {
            InitializeComponent();
            lblVersion.Text = versionInfo;
            configManager = new ConfigManager();
            migManTrace = configManager.GetAppSetting("TRACE_MMANAGER") == "1" ? true : false;
            if (configManager.GetAppSetting("ALLOW_RESET") != "1") tabMain.TabPages.Remove(pageReset);
        }

        private void MigrationManager_Shown(object sender, EventArgs e)
        {
            tabMain.Hide();
            using (LoginDialog dlg = new LoginDialog(configManager.GetAppSetting("DBUserId"), configManager.GetAppSetting("DBPassword"), configManager.GetAppSetting("DBInstance"), configManager.GetAppSetting("DefaultDB"), configManager.GetAppSetting("UseWindowsAuth")))
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    dbConnection = dlg.GetDBConnectionString();
                    tabMain.Show();
                    DisplayTab();
                }
                else
                    Application.Exit();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tabMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisplayTab();
        }

        private void DisplayTab()
        {
            AdjustTabs();

            switch (tabMain.SelectedIndex)
            {
                case 0:
                    ShowConfigureSubTab();
                    break;
                case 1:
                    ShowDefineSubTab();
                    break;
                case 2:
                    ShowLoadSubTab();
                    break;
                case 3:
                    ShowPrepareSubTab();
                    break;
                case 4:
                    ShowMigrateSubTab();
                    break;
                case 5:
                    ShowResetSubTab();
                    break;
                default:
                    break;
            }
        }

        private void DisplayNotImplemented(TabPage page)
        {
            if (page.Controls["NotImplemented"] == null)
            {
                page.Controls.Add(MigrationManager.CreateLabel("NotImplemented", page.Text + ": Not Implemented Yet", new System.Drawing.Point(380, 200), new System.Drawing.Size(270, 60)));
            }
            page.BringToFront();
        }

        private Boolean AreJobsDefined()
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID FROM MgrJobs");
            return (xml.SelectNodes("//row").Count > 0) ? true : false;
        }

        private void AdjustTabs()
        {
            if (AreJobsDefined())
            {
                tabLoad.Show();
                tabPrepare.Show();
                tabMigrate.Show();
                tabReset.Show();
            }
            else
            {
                tabLoad.Hide();
                tabPrepare.Hide();
                tabMigrate.Hide();
                tabReset.Hide();
            }
        }
    }
}
