﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MigrationManager
{
    public partial class MigrationManager
    {
        private void btnImportConfig_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = GetImportXml();
            if (xmlDoc != null)
            {
                foreach (XmlElement exportNode in xmlDoc.SelectNodes("/mmExport"))
                {
                    ImportConfig(currentConfigID, "mmConfiguration", exportNode);
                    ImportMappings("mmMappings/mmMapping", exportNode);
                }

                foreach (XmlElement exportNode in xmlDoc.SelectNodes("/mmExport/mmJobDefinition"))
                {
                    ImportConfig(currentConfigID, "mmConfiguration", exportNode);
                    ImportMappings("mmMappings/mmMapping", exportNode);
                }
            }
        }

        private void btnSrcCfgCaseImp_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = GetImportXml();
            if (xmlDoc != null)
            {
                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport"))
                {
                    ImportCfgCaseSrc(currentConfigID, "mmCaseSource/mmSourceColumn", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmConfiguration"))
                {
                    ImportCfgCaseSrc(currentConfigID, "mmCaseSource/mmSourceColumn", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmJobDefinition/mmConfiguration"))
                {
                    ImportCfgCaseSrc(currentConfigID, "mmCaseSource/mmSourceColumn", config);
                }
            }
        }

        private void btnSrcCfgDocImp_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = GetImportXml();
            if (xmlDoc != null)
            {
                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport"))
                {
                    ImportCfgDocSrc(currentConfigID, "mmDocumentSource/mmSourceColumn", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmConfiguration"))
                {
                    ImportCfgDocSrc(currentConfigID, "mmDocumentSource/mmSourceColumn", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmJobDefinition/mmConfiguration"))
                {
                    ImportCfgDocSrc(currentConfigID, "mmDocumentSource/mmSourceColumn", config);
                }
            }
        }

        private void btnSrcCfgAnnImp_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = GetImportXml();
            if (xmlDoc != null)
            {
                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport"))
                {
                    ImportCfgAnnSrc(currentConfigID, "mmAnnotationSource/mmSourceColumn", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmConfiguration"))
                {
                    ImportCfgAnnSrc(currentConfigID, "mmAnnotationSource/mmSourceColumn", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmJobDefinition/mmConfiguration"))
                {
                    ImportCfgAnnSrc(currentConfigID, "mmAnnotationSource/mmSourceColumn", config);
                }
            }
        }

        private void btnSrcCfgTaskImp_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = GetImportXml();
            if (xmlDoc != null)
            {
                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport"))
                {
                    ImportCfgTaskSrc(currentConfigID, "mmTaskSource/mmSourceColumn", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmConfiguration"))
                {
                    ImportCfgTaskSrc(currentConfigID, "mmTaskSource/mmSourceColumn", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmJobDefinition/mmConfiguration"))
                {
                    ImportCfgTaskSrc(currentConfigID, "mmTaskSource/mmSourceColumn", config);
                }
            }
        }

        private void btnTgtCfgCaseImp_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = GetImportXml();
            if (xmlDoc != null)
            {
                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport"))
                {
                    ImportCfgCaseTgt(currentConfigID, "mmCaseType", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmConfiguration"))
                {
                    ImportCfgCaseTgt(currentConfigID, "mmCaseTypes/mmCaseType", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmJobDefinition/mmConfiguration"))
                {
                    ImportCfgCaseTgt(currentConfigID, "mmCaseTypes/mmCaseType", config);
                }
            }
        }

        private void btnTgtCfgDocImp_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = GetImportXml();
            if (xmlDoc != null)
            {
                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport"))
                {
                    ImportCfgDocTgt(currentConfigID, "mmDocumentType", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmConfiguration"))
                {
                    ImportCfgDocTgt(currentConfigID, "mmDocumentTypes/mmDocumentType", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmJobDefinition/mmConfiguration"))
                {
                    ImportCfgDocTgt(currentConfigID, "mmDocumentTypes/mmDocumentType", config);
                }
            }
        }

        private void btnTgtCfgAnnImp_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = GetImportXml();
            if (xmlDoc != null)
            {
                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport"))
                {
                    ImportCfgAnnTgt(currentConfigID, "mmAnnotationType", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmConfiguration"))
                {
                    ImportCfgAnnTgt(currentConfigID, "mmAnnotationTypes/mmAnnotationType", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmJobDefinition/mmConfiguration"))
                {
                    ImportCfgAnnTgt(currentConfigID, "mmAnnotationTypes/mmAnnotationType", config);
                }
            }
        }

        private void btnTgtCfgTaskImp_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = GetImportXml();
            if (xmlDoc != null)
            {
                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport"))
                {
                    ImportCfgTaskTgt(currentConfigID, "mmTaskType", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmConfiguration"))
                {
                    ImportCfgTaskTgt(currentConfigID, "mmCaseTypes/mmCaseType/mmTaskTypes/mmTaskType", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmJobDefinition/mmConfiguration"))
                {
                    ImportCfgTaskTgt(currentConfigID, "mmCaseTypes/mmCaseType/mmTaskTypes/mmTaskType", config);
                }
            }
        }

        private void btnImpIdentifier_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = GetImportXml();
            if (xmlDoc != null)
            {
                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport"))
                {
                    ImportIdentifiers(currentConfigID, "mmIdentifiers/mmIdentifier", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmConfiguration"))
                {
                    ImportIdentifiers(currentConfigID, "mmIdentifiers/mmIdentifier", config);
                }

                foreach (XmlElement config in xmlDoc.SelectNodes("/mmExport/mmJobDefinition/mmConfiguration"))
                {
                    ImportIdentifiers(currentConfigID, "mmIdentifiers/mmIdentifier", config);
                }
            }
        }

        private ImportAction DetermineImportConfigAction(XmlElement rowToImport)
        {
            ImportAction result = ImportAction.Ignore;

            string configName = GetAttributeValue(rowToImport, "ConfigName");
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT * FROM MgrConfigurations WHERE ConfigName = '" + configName + "'");

            if (xml.SelectNodes("//row").Count > 0)
            {
                XmlNode existingRow = xml.SelectNodes("//row")[0];
                if ((GetAttributeValue(rowToImport, "ConfigLevel") != GetAttributeValue(existingRow, "ConfigLevel")) ||
                    (GetAttributeValue(rowToImport, "Comment") != GetAttributeValue(existingRow, "Comment")))
                {
                    DialogResult response = MessageBox.Show("There already is a configuration called " + configName + ". Replace it with the one being imported?", "Import Confirmation", MessageBoxButtons.YesNo);
                    switch (response)
                    {
                        case DialogResult.Yes:
                            result = ImportAction.Update;
                            break;

                        default:
                            result = ImportAction.Ignore;
                            break;
                    }
                }
                else
                {
                    DialogResult response = MessageBox.Show("There already is a configuration called " + configName + ". Merge it with the one being imported?", "Import Confirmation", MessageBoxButtons.YesNo);
                    switch (response)
                    {
                        case DialogResult.Yes:
                            result = ImportAction.Update;
                            break;

                        default:
                            result = ImportAction.Ignore;
                            break;
                    }
                }
            }
            else
            {
                result = ImportAction.Insert;
            }
            return result;
        }

        private void ImportSource(string configID, XmlElement config)
        {
            ImportCfgDocSrc(configID, "mmDocumentSource/mmSourceColumn", config);
            ImportCfgAnnSrc(configID, "mmAnnotationSource/mmSourceColumn", config);
            ImportCfgCaseSrc(configID, "mmCaseSource/mmSourceColumn", config);
            ImportCfgTaskSrc(configID, "mmTaskSource/mmSourceColumn", config);
        }

        private void ImportTarget(string configID, XmlElement config)
        {
            ImportCfgCaseTgt(configID, "mmCaseTypes/mmCaseType", config);
            ImportCfgDocTgt(configID, "mmDocumentTypes/mmDocumentType", config);
            ImportCfgAnnTgt(configID, "mmAnnotationTypes/mmAnnotationType", config);
        }

        private string GetIDForConfigName(string configName)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID FROM MgrConfigurations WHERE ConfigName = '" + configName + "'");
            XmlNodeList xnl = xml.SelectNodes("//row");
            return (xnl.Count > 0) ? xnl[0].Attributes["ID"].Value : string.Empty;
        }

        private string ImportConfig(string existingConfigID, string xpath, XmlElement config)
        {
            string configID = existingConfigID;

            foreach (XmlElement row in config.SelectNodes(xpath))
            {
                switch (DetermineImportConfigAction(row))
                {
                    case ImportAction.Insert:
                        {
                            string sql = "INSERT INTO MgrConfigurations(ConfigName, ConfigLevel, Comment) VALUES ('" +
                                    GetAttributeValue(row, "ConfigName") + "', '" +
                                    GetAttributeValue(row, "ConfigLevel") + "', '" +
                                    GetAttributeValue(row, "Comment") + "')";
                            int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                            configID = GetIDForConfigName(GetAttributeValue(row, "ConfigName"));
                            RefreshConfigList();
                            ImportSource(configID, row);
                            ImportTarget(configID, row);
                            ImportIdentifiers(configID, "mmIdentifiers/mmIdentifier", row);
                        }
                        break;

                    case ImportAction.Update:
                        {
                            string sql = "UPDATE MgrConfigurations SET " +
                                    "ConfigLevel = '" + GetAttributeValue(row, "ConfigLevel") + "', " +
                                    "Comment = '" + GetAttributeValue(row, "Comment") + "' " +
                                    "WHERE ConfigName = '" + GetAttributeValue(row, "ConfigName") + "'";
                            int nAffected = DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
                            configID = GetIDForConfigName(GetAttributeValue(row, "ConfigName"));
                            RefreshConfigList();
                            ImportSource(configID, row);
                            ImportTarget(configID, row);
                            ImportIdentifiers(configID, "mmIdentifiers/mmIdentifier", row);
                        }
                        break;

                    default:
                        configID = GetIDForConfigName(GetAttributeValue(row, "ConfigName"));
                        break;
                }
            }
            return configID;
        }

        private ImportAction DetermineImportCaseSrcAction(string configID, XmlElement rowToImport)
        {
            ImportAction result = ImportAction.Ignore;

            
            string columnID = GetAttributeValue(rowToImport, "ColumnID");
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT Name, DataType, MaxLength, Format, MultiValued FROM CfgCaseSource WHERE ColumnID = " + columnID + " AND ConfigID = " + configID);
            if (xml.SelectNodes("//row").Count > 0)
            {
                XmlNode existingRow = xml.SelectNodes("//row")[0];
                if ((GetAttributeValue(rowToImport, "Name") != GetAttributeValue(existingRow, "Name")) ||
                    (GetAttributeValue(rowToImport, "DataType") != GetAttributeValue(existingRow, "DataType")) ||
                    (GetAttributeValue(rowToImport, "MaxLength") != GetAttributeValue(existingRow, "MaxLength")) ||
                    (GetAttributeValue(rowToImport, "Format") != GetAttributeValue(existingRow, "Format")) ||
                    (GetAttributeValue(rowToImport, "MultiValued") != GetAttributeValue(existingRow, "MultiValued")))
                {
                    result = ImportAction.Update;
                }
            }
            else
            {
                result = ImportAction.Insert;
            }
            return result;
        }

        private void ImportCfgCaseSrc(string configID, string xpath, XmlElement config)
        {
            foreach (XmlElement row in config.SelectNodes(xpath))
            {
                DeleteIfExistsInAnotherColumn(configID, "CfgCaseSource", row);
                switch (DetermineImportCaseSrcAction(configID, row))
                {
                    case ImportAction.Insert:
                        {
                            string sql = "INSERT INTO CfgCaseSource(ConfigID, ColumnID, Name, DataType, MaxLength, Format, MultiValued) VALUES (" +
                                            configID + ", " +
                                            GetAttributeValue(row, "ColumnID") + ", '" +
                                            GetAttributeValue(row, "Name") + "', '" +
                                            GetAttributeValue(row, "DataType") + "', " +
                                            GetAttributeValue(row, "MaxLength") + ", '" +
                                            GetAttributeValue(row, "Format") + "', '" +
                                            GetAttributeValue(row, "MultiValued") + "')";
                            int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                            RefreshSourceCasePage();
                        }
                        break;

                    case ImportAction.Update:
                        {
                            string sql = "UPDATE CfgCaseSource SET " +
                                            "Name = '" + GetAttributeValue(row, "Name") + "', " +
                                            "DataType = '" + GetAttributeValue(row, "DataType") + "', " +
                                            "MaxLength = " + GetAttributeValue(row, "MaxLength") + ", " +
                                            "Format = '" + GetAttributeValue(row, "Format") + "', " +
                                            "MultiValued = '" + GetAttributeValue(row, "MultiValued") + "' " +
                                            "WHERE ColumnID = " + GetAttributeValue(row, "ColumnID") + " AND ConfigID = " + configID;
                            int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                            RefreshSourceCasePage();
                        }
                        break;

                    default:
                        break;
                }
                
            }
        }

        private ImportAction DetermineImportDocSrcAction(string configID, XmlElement rowToImport)
        {
            ImportAction result = ImportAction.Ignore;

            string columnID = GetAttributeValue(rowToImport, "ColumnID");
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT Name, DataType, MaxLength, Format, MultiValued FROM CfgDocumentSource WHERE ColumnID = " + columnID + " AND ConfigID = " + configID);
            if (xml.SelectNodes("//row").Count > 0)
            {
                XmlNode existingRow = xml.SelectNodes("//row")[0];
                if ((GetAttributeValue(rowToImport, "Name") != GetAttributeValue(existingRow, "Name")) ||
                    (GetAttributeValue(rowToImport, "DataType") != GetAttributeValue(existingRow, "DataType")) ||
                    (GetAttributeValue(rowToImport, "MaxLength") != GetAttributeValue(existingRow, "MaxLength")) ||
                    (GetAttributeValue(rowToImport, "Format") != GetAttributeValue(existingRow, "Format")) ||
                    (GetAttributeValue(rowToImport, "MultiValued") != GetAttributeValue(existingRow, "MultiValued")))
                {
                    result = ImportAction.Update;
                }
            }
            else
            {
                result = ImportAction.Insert;
            }
            return result;
        }

        private void ImportCfgDocSrc(string configID, string xpath, XmlElement config)
        {
            foreach (XmlElement row in config.SelectNodes(xpath))
            {
                DeleteIfExistsInAnotherColumn(configID, "CfgDocumentSource", row);
                switch (DetermineImportDocSrcAction(configID, row))
                {
                    case ImportAction.Insert:
                        {
                            string sql = "INSERT INTO CfgDocumentSource(ConfigID, ColumnID, Name, DataType, MaxLength, Format, MultiValued) VALUES (" +
                                            configID + ", " +
                                            GetAttributeValue(row, "ColumnID") + ", '" +
                                            GetAttributeValue(row, "Name") + "', '" +
                                            GetAttributeValue(row, "DataType") + "', " +
                                            GetAttributeValue(row, "MaxLength") + ", '" +
                                            GetAttributeValue(row, "Format") + "', '" +
                                            GetAttributeValue(row, "MultiValued") + "')";
                            int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                            RefreshSourceDocumentPage();
                        }
                        break;

                    case ImportAction.Update:
                        {
                            string sql = "UPDATE CfgDocumentSource SET " +
                                            "Name = '" + GetAttributeValue(row, "Name") + "', " +
                                            "DataType = '" + GetAttributeValue(row, "DataType") + "', " +
                                            "MaxLength = " + GetAttributeValue(row, "MaxLength") + ", " +
                                            "Format = '" + GetAttributeValue(row, "Format") + "', " +
                                            "MultiValued = '" + GetAttributeValue(row, "MultiValued") + "' " +
                                            "WHERE ColumnID = " + GetAttributeValue(row, "ColumnID") + " AND ConfigID = " + configID;
                            int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                            RefreshSourceDocumentPage();
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        private ImportAction DetermineImportAnnSrcAction(string configID, XmlElement rowToImport)
        {
            ImportAction result = ImportAction.Ignore;

            string columnID = GetAttributeValue(rowToImport, "ColumnID");
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT Name, DataType, MaxLength, Format, MultiValued FROM CfgAnnotationSource WHERE ColumnID = " + columnID + " AND ConfigID = " + configID);
            if (xml.SelectNodes("//row").Count > 0)
            {
                XmlNode existingRow = xml.SelectNodes("//row")[0];
                if ((GetAttributeValue(rowToImport, "Name") != GetAttributeValue(existingRow, "Name")) ||
                    (GetAttributeValue(rowToImport, "DataType") != GetAttributeValue(existingRow, "DataType")) ||
                    (GetAttributeValue(rowToImport, "MaxLength") != GetAttributeValue(existingRow, "MaxLength")) ||
                    (GetAttributeValue(rowToImport, "Format") != GetAttributeValue(existingRow, "Format")) ||
                    (GetAttributeValue(rowToImport, "MultiValued") != GetAttributeValue(existingRow, "MultiValued")))
                {
                    result = ImportAction.Update;
                }
            }
            else
            {
                result = ImportAction.Insert;
            }
            return result;
        }

        private void ImportCfgAnnSrc(string configID, string xpath, XmlElement config)
        {
            foreach (XmlElement row in config.SelectNodes(xpath))
            {
                DeleteIfExistsInAnotherColumn(configID, "CfgAnnotationSource", row);
                switch (DetermineImportAnnSrcAction(configID, row))
                {
                    case ImportAction.Insert:
                        {
                            string sql = "INSERT INTO CfgAnnotationSource(ConfigID, ColumnID, Name, DataType, MaxLength, Format, MultiValued) VALUES (" +
                                            configID + ", " +
                                            GetAttributeValue(row, "ColumnID") + ", '" +
                                            GetAttributeValue(row, "Name") + "', '" +
                                            GetAttributeValue(row, "DataType") + "', " +
                                            GetAttributeValue(row, "MaxLength") + ", '" +
                                            GetAttributeValue(row, "Format") + "', '" +
                                            GetAttributeValue(row, "MultiValued") + "')";
                            int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                            RefreshSourceAnnotationPage();
                        }
                        break;

                    case ImportAction.Update:
                        {
                            string sql = "UPDATE CfgAnnotationSource SET " +
                                            "Name = '" + GetAttributeValue(row, "Name") + "', " +
                                            "DataType = '" + GetAttributeValue(row, "DataType") + "', " +
                                            "MaxLength = " + GetAttributeValue(row, "MaxLength") + ", " +
                                            "Format = '" + GetAttributeValue(row, "Format") + "', " +
                                            "MultiValued = '" + GetAttributeValue(row, "MultiValued") + "' " +
                                            "WHERE ColumnID = " + GetAttributeValue(row, "ColumnID") + " AND ConfigID = " + configID;
                            int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                            RefreshSourceAnnotationPage();
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        private ImportAction DetermineImportTaskSrcAction(string configID, XmlElement rowToImport)
        {
            ImportAction result = ImportAction.Ignore;

            string columnID = GetAttributeValue(rowToImport, "ColumnID");
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT Name, DataType, MaxLength, Format, MultiValued FROM CfgTaskSource WHERE ColumnID = " + columnID + " AND ConfigID = " + configID);
            if (xml.SelectNodes("//row").Count > 0)
            {
                XmlNode existingRow = xml.SelectNodes("//row")[0];
                if ((GetAttributeValue(rowToImport, "Name") != GetAttributeValue(existingRow, "Name")) ||
                    (GetAttributeValue(rowToImport, "DataType") != GetAttributeValue(existingRow, "DataType")) ||
                    (GetAttributeValue(rowToImport, "MaxLength") != GetAttributeValue(existingRow, "MaxLength")) ||
                    (GetAttributeValue(rowToImport, "Format") != GetAttributeValue(existingRow, "Format")) ||
                    (GetAttributeValue(rowToImport, "MultiValued") != GetAttributeValue(existingRow, "MultiValued")))
                {
                    result = ImportAction.Update;
                }
            }
            else
            {
                result = ImportAction.Insert;
            }
            return result;
        }

        private void ImportCfgTaskSrc(string configID, string xpath, XmlElement config)
        {
            foreach (XmlElement row in config.SelectNodes(xpath))
            {
                DeleteIfExistsInAnotherColumn(configID, "CfgTaskSource", row);
                switch (DetermineImportTaskSrcAction(configID, row))
                {
                    case ImportAction.Insert:
                        {
                            string sql = "INSERT INTO CfgTaskSource(ConfigID, ColumnID, Name, DataType, MaxLength, Format, MultiValued) VALUES (" +
                                            configID + ", " +
                                            GetAttributeValue(row, "ColumnID") + ", '" +
                                            GetAttributeValue(row, "Name") + "', '" +
                                            GetAttributeValue(row, "DataType") + "', " +
                                            GetAttributeValue(row, "MaxLength") + ", '" +
                                            GetAttributeValue(row, "Format") + "', '" +
                                            GetAttributeValue(row, "MultiValued") + "')";
                            int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                            RefreshSourceTaskPage();
                        }
                        break;

                    case ImportAction.Update:
                        {
                            string sql = "UPDATE CfgTaskSource SET " +
                                            "Name = '" + GetAttributeValue(row, "Name") + "', " +
                                            "DataType = '" + GetAttributeValue(row, "DataType") + "', " +
                                            "MaxLength = " + GetAttributeValue(row, "MaxLength") + ", " +
                                            "Format = '" + GetAttributeValue(row, "Format") + "', " +
                                            "MultiValued = '" + GetAttributeValue(row, "MultiValued") + "' " +
                                            "WHERE ColumnID = " + GetAttributeValue(row, "ColumnID") + " AND ConfigID = " + configID;
                            int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                            RefreshSourceTaskPage();
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        private ImportAction DetermineImportCaseTgtAction(string configID, XmlNodeList caseTypes)
        {
            ImportAction action = ImportAction.Ignore;

            foreach (XmlElement ct in caseTypes)
            {
                string sql = "SELECT * FROM CfgCaseTypes WHERE CaseName = '" + GetAttributeValue(ct, "CaseName") + "' AND ConfigID = " + configID;
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
                if (xml.SelectNodes("//row").Count > 0)
                {
                    action = ImportAction.Merge;
                    break;
                }
                else
                {
                    action = ImportAction.Insert;
                }
            }
            return action;
        }

        private void InsertCaseProperty(string configID, string caseTypeID, XmlElement casePropertyNode)
        {
            string sql = "INSERT INTO CfgCaseProperties(ConfigID, CaseTypeID, Name, Source, DataType, Required, Mapped, Computed, MappingName) VALUES (" +
                                configID + ", " +
                                caseTypeID + ", '" +
                                GetAttributeValue(casePropertyNode, "Name") + "', '" +
                                GetAttributeValue(casePropertyNode, "Source") + "', '" +
                                GetAttributeValue(casePropertyNode, "DataType") + "', " +
                                GetAttributeValue(casePropertyNode, "Required") + ", " +
                                GetAttributeValue(casePropertyNode, "Mapped") + ", " +
                                GetAttributeValue(casePropertyNode, "Computed") + ", '" +
                                GetAttributeValue(casePropertyNode, "MappingName") + "')";
            int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
        }

        private void InsertCaseFolder(string configID, string caseTypeID, XmlElement caseFolderNode)
        {
            string sql = "INSERT INTO CfgCaseFolders(ConfigID, CaseTypeID, FolderName, Parent) VALUES (" +
                                configID + ", " +
                                caseTypeID + ", '" +
                                GetAttributeValue(caseFolderNode, "FolderName") + "', '" +
                                GetAttributeValue(caseFolderNode, "Parent") + "')";
            int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
        }

        private void UpdateCaseProperty(string configID, string caseTypeID, XmlElement casePropertyNode)
        {
            string sql = "UPDATE CfgCaseProperties SET " +
                                "Source = '" + GetAttributeValue(casePropertyNode, "Source") + "', " +
                                "DataType = '" + GetAttributeValue(casePropertyNode, "DataType") + "', " +
                                "Required = " + GetAttributeValue(casePropertyNode, "Required") + ", " +
                                "Mapped = " + GetAttributeValue(casePropertyNode, "Mapped") + ", " +
                                "Computed = " + GetAttributeValue(casePropertyNode, "Computed") + ", " +
                                "MappingName = '" + GetAttributeValue(casePropertyNode, "MappingName") + "' " +
                                "WHERE CaseTypeID = " + caseTypeID + " AND ConfigID = " + configID + " AND Name = '" + GetAttributeValue(casePropertyNode, "Name") + "'";
            int nAffected = DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
        }

        private void UpdateCaseFolder(string configID, string caseTypeID, XmlElement caseFolderNode)
        {
            string sql = "UPDATE CfgCaseFolders SET " +
                                "Parent = '" + GetAttributeValue(caseFolderNode, "Parent") + "' " +
                                "WHERE CaseTypeID = " + caseTypeID + " AND ConfigID = " + configID + " AND FolderName = '" + GetAttributeValue(caseFolderNode, "FolderName") + "'";
            int nAffected = DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
        }

        private void InsertCaseProperties(string configID, string caseTypeID, string xPath, XmlElement caseTypeNode)
        {
            foreach (XmlElement cp in caseTypeNode.SelectNodes(xPath))
            {
                InsertCaseProperty(configID, caseTypeID, cp);
            }
        }

        private void InsertCaseFolders(string configID, string caseTypeID, string xPath, XmlElement caseTypeNode)
        {
            foreach (XmlElement cf in caseTypeNode.SelectNodes(xPath))
            {
                InsertCaseFolder(configID, caseTypeID, cf);
            }
        }

        private void MergeCaseProperties(string configID, string caseTypeID, string xPath, XmlElement caseTypeNode)
        {
            string sql = string.Empty;

            foreach (XmlElement cp in caseTypeNode.SelectNodes(xPath))
            {
                sql = "SELECT * FROM CfgCaseProperties WHERE CaseTypeID = " + caseTypeID + " AND ConfigID = " + configID + " AND Name = '" + GetAttributeValue(cp, "Name") + "'";
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
                if (xml.SelectNodes("//row").Count > 0)
                {
                    UpdateCaseProperty(configID, caseTypeID, cp);
                }
                else
                {
                    InsertCaseProperty(configID, caseTypeID, cp);
                }
                
            }
        }

        private void MergeCaseFolders(string configID, string caseTypeID, string xPath, XmlElement caseTypeNode)
        {
            string sql = string.Empty;

            foreach (XmlElement cp in caseTypeNode.SelectNodes(xPath))
            {
                sql = "SELECT * FROM CfgCaseFolders WHERE CaseTypeID = " + caseTypeID + " AND ConfigID = " + configID + " AND FolderName = '" + GetAttributeValue(cp, "FolderName") + "'";
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
                if (xml.SelectNodes("//row").Count > 0)
                {
                    UpdateCaseFolder(configID, caseTypeID, cp);
                }
                else
                {
                    InsertCaseFolder(configID, caseTypeID, cp);
                }
            }
        }

        private string GetIDForCaseName(string configID, string caseName)
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID FROM CfgCaseTypes WHERE ConfigID = " + configID + " AND CaseName = '" + caseName + "'");
            XmlNodeList xnl = xmlDoc.SelectNodes("//row");
            return (xnl.Count > 0) ? xnl[0].Attributes["ID"].Value : string.Empty;
        }

        private void InsertCaseTypes(string configID, XmlNodeList caseTypes)
        {
            foreach (XmlElement ct in caseTypes)
            {
                string sql = "INSERT INTO CfgCaseTypes(ConfigID, CaseName, DefaultDocClass) VALUES (" + configID + ", '" +
                                GetAttributeValue(ct, "CaseName") + "', '" + 
                                GetAttributeValue(ct, "DefaultDocClass") + "')";
                int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                string caseTypeID = GetIDForCaseName(configID, GetAttributeValue(ct, "CaseName"));
                InsertCaseProperties(configID, caseTypeID, "mmProperties/mmProperty", ct);
                InsertCaseFolders(configID, caseTypeID, "mmFolders/mmFolder", ct);
                ImportCfgTaskTgt(configID, "mmTaskTypes/mmTaskType", ct);
            }
        }

        private void MergeCaseTypes(string configID, XmlNodeList caseTypes)
        {
            foreach (XmlElement ct in caseTypes)
            {
                string caseTypeID = GetIDForCaseName(configID, GetAttributeValue(ct, "CaseName"));
                if (caseTypeID.Length > 0)
                {
                    string sql = "UPDATE CfgCaseTypes SET " +
                                "DefaultDocClass = '" + GetAttributeValue(ct, "DefaultDocClass") + "' " +
                                "WHERE ID = " + caseTypeID;
                    int nAffected = DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
                }
                else
                {
                    string sql = "INSERT INTO CfgCaseTypes(ConfigID, CaseName, DefaultDocClass) VALUES (" + configID + ", '" +
                                GetAttributeValue(ct, "CaseName") + "', '" +
                                GetAttributeValue(ct, "DefaultDocClass") + "')";
                    int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                    caseTypeID = GetIDForCaseName(configID, GetAttributeValue(ct, "CaseName"));
                }
                MergeCaseProperties(configID, caseTypeID, "mmProperties/mmProperty", ct);
                MergeCaseFolders(configID, caseTypeID, "mmFolders/mmFolder", ct);
                ImportCfgTaskTgt(configID, "mmTaskTypes/mmTaskType", ct);
            }
        }

        private void ImportCfgCaseTgt(string configID, string xpath, XmlElement node)
        {
            switch (DetermineImportCaseTgtAction(configID, node.SelectNodes(xpath)))
            {
                case ImportAction.Insert:
                    InsertCaseTypes(configID, node.SelectNodes(xpath));
                    RefreshTargetCasePage();
                    break;

                case ImportAction.Merge:
                case ImportAction.Update:
                    MergeCaseTypes(configID, node.SelectNodes(xpath));
                    RefreshTargetCasePage();
                    break;

                default:
                    break;  // ignore
            }
        }

        private ImportAction DetermineImportDocTgtAction(string configID, XmlNodeList docTypes)
        {
            ImportAction action = ImportAction.Ignore;

            foreach (XmlElement dt in docTypes)
            {
                string sql = "SELECT * FROM CfgDocClasses WHERE DocClassName = '" + GetAttributeValue(dt, "DocClassName") + "' AND ConfigID = " + configID;
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
                if (xml.SelectNodes("//row").Count > 0)
                {
                    action = ImportAction.Merge;
                    break;
                }
                else
                {
                    action = ImportAction.Insert;
                }
            }
            return action;
        }

        private void InsertDocProperty(string configID, string docClassID, XmlElement docClassProperty)
        {
            string sql = "INSERT INTO CfgDocClassProperties(ConfigID, DocClassID, Name, Source, DataType, Required, Mapped, Computed, MappingName) VALUES (" +
                                configID + ", " +
                                docClassID + ", '" +
                                GetAttributeValue(docClassProperty, "Name") + "', '" +
                                GetAttributeValue(docClassProperty, "Source") + "', '" +
                                GetAttributeValue(docClassProperty, "DataType") + "', " +
                                GetAttributeValue(docClassProperty, "Required") + ", " +
                                GetAttributeValue(docClassProperty, "Mapped") + ", " +
                                GetAttributeValue(docClassProperty, "Computed") + ", '" +
                                GetAttributeValue(docClassProperty, "MappingName") + "')";
            int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
        }

        private void UpdateDocProperty(string configID, string docClassID, XmlElement docClassProperties)
        {
            string sql = "UPDATE CfgDocClassProperties SET " +
                                "Source = '" + GetAttributeValue(docClassProperties, "Source") + "', " +
                                "DataType = '" + GetAttributeValue(docClassProperties, "DataType") + "', " +
                                "Required = " + GetAttributeValue(docClassProperties, "Required") + ", " +
                                "Mapped = " + GetAttributeValue(docClassProperties, "Mapped") + ", " +
                                "Computed = " + GetAttributeValue(docClassProperties, "Computed") + ", " +
                                "MappingName = '" + GetAttributeValue(docClassProperties, "MappingName") + "' " +
                                "WHERE DocClassID = " + docClassID + " AND ConfigID = " + configID + " AND Name = '" + GetAttributeValue(docClassProperties, "Name") + "'";
            int nAffected = DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
        }

        private void InsertDocProperties(string configID, string docClassID, string xPath, XmlElement docClassProperties)
        {
            foreach (XmlElement dp in docClassProperties.SelectNodes(xPath))
            {
                if (GetAttributeValue(dp, "Name") == "DocumentTitle")
                {
                    UpdateDocProperty(configID, docClassID, dp);
                }
                else
                {
                    InsertDocProperty(configID, docClassID, dp);
                }
            }
        }

        private void MergeDocProperties(string configID, string docClassID, string xPath, XmlElement docClassProperties)
        {
            string sql = string.Empty;

            foreach (XmlElement dp in docClassProperties.SelectNodes(xPath))
            {
                sql = "SELECT * FROM CfgDocClassProperties WHERE DocClassID = " + docClassID + " AND ConfigID = " + configID + " AND Name = '" + GetAttributeValue(dp, "Name") + "'";
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
                if (xml.SelectNodes("//row").Count > 0)
                {
                    UpdateDocProperty(configID, docClassID, dp);
                }
                else
                {
                    InsertDocProperty(configID, docClassID, dp);
                }
            }
        }

        private string GetIDForDocClassName(string configID, string docClassName)
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID FROM CfgDocClasses WHERE ConfigID = " + configID + " AND DocClassName = '" + docClassName + "'");
            XmlNodeList xnl = xmlDoc.SelectNodes("//row");
            return (xnl.Count > 0) ? xnl[0].Attributes["ID"].Value : string.Empty;
        }

        private void InsertDocTypes(string configID, XmlNodeList docTypes)
        {
            foreach (XmlElement dt in docTypes)
            {
                string sql = "INSERT INTO CfgDocClasses(ConfigID, DocClassName) VALUES (" + configID + ", '" + GetAttributeValue(dt, "DocClassName") + "')";
                int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                string docClassID = GetIDForDocClassName(configID, GetAttributeValue(dt, "DocClassName"));
                InsertDocProperties(configID, docClassID, "mmProperties/mmProperty", dt);
            }
        }

        private void MergeDocTypes(string configID, XmlNodeList docTypes)
        {
            foreach (XmlElement dt in docTypes)
            {
                string docClassID = GetIDForDocClassName(configID, GetAttributeValue(dt, "DocClassName"));
                if (docClassID.Length == 0)
                {
                    string sql = "INSERT INTO CfgDocClasses(ConfigID, DocClassName) VALUES (" + configID + ", '" + GetAttributeValue(dt, "DocClassName") + "')";
                    int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                    docClassID = GetIDForDocClassName(configID, GetAttributeValue(dt, "DocClassName"));
                }
                MergeDocProperties(configID, docClassID, "mmProperties/mmProperty", dt);
            }
        }

        private void ImportCfgDocTgt(string configID, string xpath, XmlElement node)
        {
            switch (DetermineImportDocTgtAction(configID, node.SelectNodes(xpath)))
            {
                case ImportAction.Insert:
                    InsertDocTypes(configID, node.SelectNodes(xpath));
                    RefreshTargetDocumentPage();
                    break;

                case ImportAction.Merge:
                case ImportAction.Update:
                    MergeDocTypes(configID, node.SelectNodes(xpath));
                    RefreshTargetDocumentPage();
                    break;

                default:
                    break;  // ignore
            }
        }

        private ImportAction DetermineImportAnnTgtAction(string configID, XmlNodeList annTypes)
        {
            ImportAction action = ImportAction.Ignore;

            foreach (XmlElement dt in annTypes)
            {
                string sql = "SELECT * FROM CfgAnnClasses WHERE AnnClassName = '" + GetAttributeValue(dt, "AnnClassName") + "' AND ConfigID = " + configID;
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
                if (xml.SelectNodes("//row").Count > 0)
                {
                    action = ImportAction.Merge;
                    break;
                }
                else
                {
                    action = ImportAction.Insert;
                }
            }
            return action;
        }

        private void InsertAnnProperty(string configID, string annClassID, XmlElement annClassProperty)
        {
            string sql = "INSERT INTO CfgAnnClassProperties(ConfigID, AnnClassID, Name, Source, DataType, Required, Mapped, Computed, MappingName) VALUES (" +
                                configID + ", " +
                                annClassID + ", '" +
                                GetAttributeValue(annClassProperty, "Name") + "', '" +
                                GetAttributeValue(annClassProperty, "Source") + "', '" +
                                GetAttributeValue(annClassProperty, "DataType") + "', " +
                                GetAttributeValue(annClassProperty, "Required") + ", " +
                                GetAttributeValue(annClassProperty, "Mapped") + ", " +
                                GetAttributeValue(annClassProperty, "Computed") + ", '" +
                                GetAttributeValue(annClassProperty, "MappingName") + "')";
            int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
        }

        private void UpdateAnnProperty(string configID, string annClassID, XmlElement annClassProperties)
        {
            string sql = "UPDATE CfgAnnClassProperties SET " +
                                "Source = '" + GetAttributeValue(annClassProperties, "Source") + "', " +
                                "DataType = '" + GetAttributeValue(annClassProperties, "DataType") + "', " +
                                "Required = " + GetAttributeValue(annClassProperties, "Required") + ", " +
                                "Mapped = " + GetAttributeValue(annClassProperties, "Mapped") + ", " +
                                "Computed = " + GetAttributeValue(annClassProperties, "Computed") + ", " +
                                "MappingName = '" + GetAttributeValue(annClassProperties, "MappingName") + "' " +
                                "WHERE AnnClassID = " + annClassID + " AND ConfigID = " + configID + " AND Name = '" + GetAttributeValue(annClassProperties, "Name") + "'";
            int nAffected = DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
        }

        private void InsertAnnProperties(string configID, string annClassID, string xPath, XmlElement annClassProperties)
        {
            foreach (XmlElement dp in annClassProperties.SelectNodes(xPath))
            {
                string name = GetAttributeValue(dp, "Name");
                if ((name == "AnnotatedObject") || (name == "CmAcmAction") || (name == "CmAcmCommentText") || (name == "CmAcmCommentedTask") || (name == "CmAcmCommentedVersionSeries"))
                {
                    UpdateAnnProperty(configID, annClassID, dp);
                }
                else
                {
                    InsertAnnProperty(configID, annClassID, dp);
                }
            }
        }

        private void MergeAnnProperties(string configID, string annClassID, string xPath, XmlElement annClassProperties)
        {
            string sql = string.Empty;

            foreach (XmlElement dp in annClassProperties.SelectNodes(xPath))
            {
                sql = "SELECT * FROM CfgAnnClassProperties WHERE AnnClassID = " + annClassID + " AND ConfigID = " + configID + " AND Name = '" + GetAttributeValue(dp, "Name") + "'";
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
                if (xml.SelectNodes("//row").Count > 0)
                {
                    UpdateAnnProperty(configID, annClassID, dp);
                }
                else
                {
                    InsertAnnProperty(configID, annClassID, dp);
                }
            }
        }

        private string GetIDForAnnClassName(string configID, string annClassName)
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID FROM CfgAnnClasses WHERE ConfigID = " + configID + " AND AnnClassName = '" + annClassName + "'");
            XmlNodeList xnl = xmlDoc.SelectNodes("//row");
            return (xnl.Count > 0) ? xnl[0].Attributes["ID"].Value : string.Empty;
        }

        private void InsertAnnTypes(string configID, XmlNodeList annTypes)
        {
            foreach (XmlElement dt in annTypes)
            {
                string sql = "INSERT INTO CfgAnnClasses(ConfigID, AnnClassName) VALUES (" + configID + ", '" + GetAttributeValue(dt, "AnnClassName") + "')";
                int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                string annClassID = GetIDForAnnClassName(configID, GetAttributeValue(dt, "AnnClassName"));
                InsertAnnProperties(configID, annClassID, "mmProperties/mmProperty", dt);
            }
        }

        private void MergeAnnTypes(string configID, XmlNodeList annTypes)
        {
            foreach (XmlElement dt in annTypes)
            {
                string annClassID = GetIDForAnnClassName(configID, GetAttributeValue(dt, "AnnClassName"));
                if (annClassID.Length == 0)
                {
                    string sql = "INSERT INTO CfgAnnClasses(ConfigID, AnnClassName) VALUES (" + configID + ", '" + GetAttributeValue(dt, "AnnClassName") + "')";
                    int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                    annClassID = GetIDForAnnClassName(configID, GetAttributeValue(dt, "AnnClassName"));
                }
                MergeAnnProperties(configID, annClassID, "mmProperties/mmProperty", dt);
            }
        }

        private void ImportCfgAnnTgt(string configID, string xpath, XmlElement node)
        {
            switch (DetermineImportAnnTgtAction(configID, node.SelectNodes(xpath)))
            {
                case ImportAction.Insert:
                    InsertAnnTypes(configID, node.SelectNodes(xpath));
                    RefreshTargetAnnotationPage();
                    break;

                case ImportAction.Merge:
                case ImportAction.Update:
                    MergeAnnTypes(configID, node.SelectNodes(xpath));
                    RefreshTargetAnnotationPage();
                    break;

                default:
                    break;  // ignore
            }
        }

        private ImportAction DetermineImportTaskTgtAction(string configID, XmlNodeList taskTypes)
        {
            ImportAction action = ImportAction.Ignore;

            foreach (XmlElement tt in taskTypes)
            {
                string caseID = GetIDForCaseName(configID, GetAttributeValue(tt, "CaseName"));

                string sql = "SELECT * FROM CfgTaskTypes WHERE TaskName = '" + GetAttributeValue(tt, "TaskName") + "' AND CaseID = " + caseID;
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
                if (xml.SelectNodes("//row").Count > 0)
                {
                    action = ImportAction.Merge;
                    break;
                }
                else
                {
                    action = ImportAction.Insert;
                }
            }
            return action;
        }

        private void InsertTaskProperty(string configID, string taskTypeID, XmlElement taskType)
        {
            string sql = "INSERT INTO CfgTaskProperties(ConfigID, TaskTypeID, Name, Source, DataType, Required, Mapped, Computed, MappingName) VALUES (" +
                                configID + ", " +
                                taskTypeID + ", '" +
                                GetAttributeValue(taskType, "Name") + "', '" +
                                GetAttributeValue(taskType, "Source") + "', '" +
                                GetAttributeValue(taskType, "DataType") + "', " +
                                GetAttributeValue(taskType, "Required") + ", " +
                                GetAttributeValue(taskType, "Mapped") + ", " +
                                GetAttributeValue(taskType, "Computed") + ", '" +
                                GetAttributeValue(taskType, "MappingName") + "')";
            int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
        }

        private void UpdateTaskProperty(string configID, string taskTypeID, XmlElement taskType)
        {
            string sql = "UPDATE CfgTaskProperties SET " +
                                "Source = '" + GetAttributeValue(taskType, "Source") + "', " +
                                "DataType = '" + GetAttributeValue(taskType, "DataType") + "', " +
                                "Required = " + GetAttributeValue(taskType, "Required") + ", " +
                                "Mapped = " + GetAttributeValue(taskType, "Mapped") + ", " +
                                "Computed = " + GetAttributeValue(taskType, "Computed") + ", " +
                                "MappingName = '" + GetAttributeValue(taskType, "MappingName") + "' " +
                                "WHERE TaskTypeID = " + taskTypeID + " AND ConfigID = " + configID + " AND Name = '" + GetAttributeValue(taskType, "Name") + "'";
            int nAffected = DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
        }

        private void InsertTaskProperties(string configID, string taskTypeID, string xPath, XmlElement taskType)
        {
            foreach (XmlElement tp in taskType.SelectNodes(xPath))
            {
                InsertTaskProperty(configID, taskTypeID, tp);
            }
        }

        private void MergeTaskProperties(string configID, string taskTypeID, string xPath, XmlElement taskType)
        {
            string sql = string.Empty;

            foreach (XmlElement tp in taskType.SelectNodes(xPath))
            {
                sql = "SELECT * FROM CfgTaskProperties WHERE taskTypeID = " + taskTypeID + " AND ConfigID = " + configID + " AND Name = '" + GetAttributeValue(tp, "Name") + "'";             
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
                if (xml.SelectNodes("//row").Count > 0)
                {
                    UpdateTaskProperty(configID, taskTypeID, tp);
                }
                else
                {
                    InsertTaskProperty(configID, taskTypeID, tp);
                }
            }
        }

        private string GetIDForTaskName(string caseID, string taskName)
        {
            XmlDocument xmlDoc = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ID FROM CfgTaskTypes WHERE CaseID = " + caseID + " AND TaskName = '" + taskName + "'");
            XmlNodeList xnl = xmlDoc.SelectNodes("//row");
            return (xnl.Count > 0) ? xnl[0].Attributes["ID"].Value : string.Empty;
        }

        private void InsertTaskTypes(string configID, XmlNodeList taskTypes)
        {
            foreach (XmlElement tt in taskTypes)
            {
                string caseID = GetIDForCaseName(configID, GetAttributeValue(tt, "CaseName"));
  
                string sql = "INSERT INTO CfgTaskTypes(CaseID, TaskName, IsDocInitiated, InitiatingDocIDSource, AttachingProperty) VALUES (" + caseID + ", '" +
                                GetAttributeValue(tt, "TaskName") + "', '" +
                                GetAttributeValue(tt, "IsDocInitiated") + "', '" +
                                GetAttributeValue(tt, "InitiatingDocIDSource") + "', '" +
                                GetAttributeValue(tt, "AttachingProperty") + "')";
                int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                string taskTypeID = GetIDForTaskName(caseID, GetAttributeValue(tt, "TaskName"));
                InsertTaskProperties(configID, taskTypeID, "mmProperties/mmProperty", tt);
            }
        }

        private void MergeTaskTypes(string configID, XmlNodeList taskTypes)
        {
            foreach (XmlElement tt in taskTypes)
            {
                string caseID = GetIDForCaseName(configID, GetAttributeValue(tt, "CaseName"));

                string sql = "SELECT ID FROM CfgTaskTypes WHERE CaseID = " + caseID + " AND TaskName = '" + GetAttributeValue(tt, "TaskName") + "'";
                XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, sql);
                string taskTypeID = string.Empty;
                if (xml.SelectNodes("//row").Count > 0)
                {
                    taskTypeID = xml.SelectNodes("//row")[0].Attributes["ID"].Value;
                    sql = "UPDATE CfgTaskTypes SET " +
                                "IsDocInitiated = '" + GetAttributeValue(tt, "IsDocInitiated") + "', " +
                                "InitiatingDocIDSource = '" + GetAttributeValue(tt, "InitiatingDocIDSource") + "', " +
                                "AttachingProperty = '" + GetAttributeValue(tt, "AttachingProperty") + "' " +
                                "WHERE CaseID = " + caseID + " AND TaskName = '" + GetAttributeValue(tt, "TaskName") + "'";   
                    int nAffected = DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
                }
                else
                {
                    sql = "INSERT INTO CfgTaskTypes(CaseID, TaskName, IsDocInitiated, InitiatingDocIDSource, AttachingProperty) VALUES (" + caseID + ", '" +
                                GetAttributeValue(tt, "TaskName") + "', '" +
                                GetAttributeValue(tt, "IsDocInitiated") + "', '" +
                                GetAttributeValue(tt, "InitiatingDocIDSource") + "', '" +
                                GetAttributeValue(tt, "AttachingProperty") + "')";
                    int nAffected = DBAdapter.DBAdapter.AddToDB(dbConnection, sql);
                    taskTypeID = GetIDForTaskName(caseID, GetAttributeValue(tt, "TaskName"));
                }
                MergeTaskProperties(configID, taskTypeID, "mmProperties/mmProperty", tt);
            }
        }

        private void ImportCfgTaskTgt(string configID, string xpath, XmlElement node)
        {
            switch (DetermineImportTaskTgtAction(configID, node.SelectNodes(xpath)))
            {
                case ImportAction.Insert:
                    InsertTaskTypes(configID, node.SelectNodes(xpath));
                    RefreshTargetTaskPage();
                    break;

                case ImportAction.Merge:
                case ImportAction.Update:
                    MergeTaskTypes(configID, node.SelectNodes(xpath));
                    RefreshTargetTaskPage();
                    break;

                default:
                    break;  // ignore
            }
        }

        private ImportAction DetermineImportIdentifiersAction(string configID, XmlNodeList rowsToImport)
        {
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT IdentifierName, FieldName FROM CfgIdentifiers WHERE ConfigID = " + configID);

            int existingRowCount = xml.SelectNodes("//row").Count;
            if (existingRowCount == rowsToImport.Count)
            {
                foreach (XmlElement rowI in rowsToImport)
                {
                    string identifier = rowI.GetAttribute("IdentifierName");
                    string fieldName = rowI.GetAttribute("FieldName");

                    string value = string.Empty;
                    foreach (XmlElement rowE in xml.SelectNodes("//row"))
                    {
                        if (rowE.GetAttribute("IdentifierName") == identifier)
                        {
                            value = rowE.GetAttribute("FieldName");
                            break;
                        }
                    }
                    if (fieldName != value)
                    {
                        if (value.Length > 0)
                        {
                            if (MessageBox.Show("Update current configuration's identifiers with those being imported?", "Import Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            {
                                return ImportAction.Update;
                            }
                        }
                        else return ImportAction.Update;
                    }
                }
                MessageBox.Show("The identifiers for this configuration are already identical to that being imported.", "Identifiers Identical", MessageBoxButtons.OK);
            }
            else
            {
                return ImportAction.Update;
            }
            return ImportAction.Ignore;
        }

        private void ImportIdentifiers(string configID, string xpath, XmlElement config)
        {
            if (DetermineImportIdentifiersAction(configID, config.SelectNodes(xpath)) == ImportAction.Update)
            {
                string sql = string.Empty;
                int nAffected = 0;

                foreach (XmlElement row in config.SelectNodes(xpath))
                {
                    string fieldName = row.Attributes["IdentifierName"].Value;
                    if (fieldName == "AnnotationDocumentIdentifier") fieldName = "AnnotatedObjectIdentifier";
                    string newValue = (row.Attributes["FieldName"] != null) ? row.Attributes["FieldName"].Value : string.Empty;

                    sql = "UPDATE CfgIdentifiers SET FieldName = '" + newValue + "' WHERE IdentifierName = '" + fieldName + "' AND ConfigID = " + configID;
                    nAffected = DBAdapter.DBAdapter.UpdateDB(dbConnection, sql);
                }    
                RefreshIdentifierList();
            }
        }

        private void DeleteIfExistsInAnotherColumn(string configID, string tableName, XmlElement rowToImport)
        {
            string fldName = GetAttributeValue(rowToImport, "Name");
            XmlDocument xml = DBAdapter.DBAdapter.SelectFromDB(dbConnection, "SELECT ColumnID FROM " + tableName + " WHERE Name = '" + fldName + "' AND ConfigID = " + configID);
            if (xml.SelectNodes("//row").Count > 0)
            {
                XmlNode existingRow = xml.SelectNodes("//row")[0];
                string oldColumn = GetAttributeValue(existingRow, "ColumnID");
                string newColumn = GetAttributeValue(rowToImport, "ColumnID");
                if (oldColumn != newColumn)
                {
                    int nAffected = DBAdapter.DBAdapter.DeleteDB(dbConnection, "DELETE " + tableName + " WHERE ColumnID = " + oldColumn);
                }
            }
        }
    }
}
